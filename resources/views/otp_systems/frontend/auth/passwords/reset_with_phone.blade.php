@extends('layouts.blank')

@section('content')
    <div class="cls-content-sm panel">
        <div class="panel-body">
        @if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
            <h1 class="h3">{{ __('Reset Password') }}</h1>
            <p class="pad-btm">{{__('Enter code (OTP), new password and confirm password.')}} </p>
            <form id="resetform" method="POST" action="{{ route('password.reset.mobile') }}">
                @csrf
                <div class="form-group">
                    <input  type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone"  value="{{ old('phone', session('login_phone')) }}" placeholder="Phone" disabled>
                </div>
                <div class="form-group">
                    <input  type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('code')}}" placeholder="Code" required autofocus>
                </div>
                <div class="form-group">
                    <input  type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="New Password" required>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                        {{ __('Reset Password') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
    
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
   
    <script>
    
    $('#resetform').submit(function(e) {
        e.preventDefault();
        var data = $('#resetform').serialize();
        $.post('{{ route('password.reset.mobile') }}', data, function(data){
            if(data.result == true) {
                showAlert('success', 'Password has been reset successful.');
                $.cookie("name", data.name, { expires: 1 , path: '/' });
                window.location = data.redirectTo;
            } else {
                showAlert('warning', data.message);
                
            }
        }).fail(function(xhr, status, error) {
            if(xhr.status == 422) {
                var m = '';
                data = JSON.parse(xhr.responseText);
                $(data.errors).each(function(i,v) {
                    $.each(v, function(x,y) {
                        m += y[0]+"<br />"; 
                    });
                });
                showAlert('warning', m);
            }
        });
    });
    
    
    </script>
@endsection
