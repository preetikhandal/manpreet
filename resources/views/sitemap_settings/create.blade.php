@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Sitemap')}}</h3>
        </div>
<script>
function check(v) {
    if(v == 'link') {
        $('#datadiv').slideDown();
        $('#data').prop('required','required');
    } else {
        $('#data').prop('required',false);
        $('#datadiv').slideUp();
    }
}
</script>
        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('sitemap.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">Setting Type</label>
                    <div class="col-sm-9">
                        <select name="type" onchange="check(this.value)" required class="form-control demo-select2-placeholder">
                                <option value="">Select</option>
                                <option value="Blog">Blog</option>
                                <option value="Brand">Brand</option>
                                <option value="link">Link</option>
                                <option value="Product">Product</option>
                                <option value="Category">Category</option>
                                <option value="SubCategory">Sub Category</option>
                                <option value="SubSubCategory">Sub Sub Category</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" id="datadiv">
                    <label class="col-sm-3 control-label" for="data">Link Data</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Link Data" id="data" name="data" value="{{old('data')}}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="changefreq">Frequency</label>
                    <div class="col-sm-9">
                        <select placeholder="changefreq" id="changefreq" name="changefreq" value="{{old('changefreq')}}" class="form-control">
                            <option value="">Select</option>
                            <option value="always">Always</option>
                            <option value="hourly">Hourly</option>
                            <option value="daily">Daily</option>
                            <option value="weekly">Weekly</option>
                            <option value="monthly">Monthly</option>
                            <option value="yearly">Yearly</option>
                            <option value="never">Never</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="priority">Priority</label>
                    <div class="col-sm-9">
                        <input type="number" placeholder="priority" id="priority" name="priority" max="1" step="0.1" value="{{old('priority')}}" class="form-control">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>


@endsection
