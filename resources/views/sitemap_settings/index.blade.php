@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('sitemap.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Sitemap Data')}}</a> &nbsp;  &nbsp; 
        <a href="{{ route('sitemap.generate')}}" onclick="return showmodal()" class="btn btn-rounded btn-info pull-right">{{__('Re-Generate Sitemap')}}</a> &nbsp;  &nbsp; 
        <a href="{{ url('sitemap.xml')}}" target="_blank" class="btn btn-rounded btn-info pull-right">{{__('View Sitemap')}}</a> &nbsp;  &nbsp; 
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Sitemaps')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10%">#</th>
                    <th>{{__('Type')}}</th>
                    <th>{{__('Data')}}</th>
                    <th>{{__('Frequency')}}</th>
                    <th>{{__('Priority')}}</th>
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sitemaps as $key => $sitemap)
                    <tr>
                        <td>{{1 + $key}}</td>
                        <td>{{$sitemap->type}}</td>
                        <td>@if($sitemap->data != null) {{url($sitemap->data)}} @else Not Application @endif</td>
                        <td>{{$sitemap->changefreq}}</td>
                        <td>{{$sitemap->priority}}</td>
                        <td>
                            <a href="{{route('sitemap.edit', encrypt($sitemap->id))}}" class="btn btn-primary">{{__('Edit')}}</a>
                            <button onclick="confirm_modal('{{route('sitemap.destroy', $sitemap->id)}}');" class="btn btn-danger">{{__('Delete')}}</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

    
	<!-- The Modal -->
	<div class="modal" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<!-- Modal body -->
				<div class="modal-body">
				    <center>
					 <i class="fa fa-spin fa-spinner fa-4x"></i>
					 <br />
					 Processing...
					 </center>
				</div>
			</div>
		</div>
	</div>
	
	<script>
	function showmodal() {
	    $('#myModal').modal('show');
	    setTimeout(function() {return true;}, 300);
	}
	</script>

@endsection
