@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Sitemap Update</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('sitemap.update', $sitemap->id) }}" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="type">Setting Type</label>
                    <div class="col-sm-9">
                        <input type="text" value="{{ $sitemap->type }}" class="form-control" readonly disabled>
                    </div>
                </div>
                @if($sitemap->type == "link")
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="data">Link Data</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Link Data" id="data" name="data" value="{{old('data', url($sitemap->data))}}" class="form-control" required>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="changefreq">Frequency</label>
                    <div class="col-sm-9">
                        <select placeholder="changefreq" id="changefreq" name="changefreq" class="form-control">
                            <option value="">Select</option>
                            <option value="always" @if($sitemap->changefreq == "always") selected @endif >Always</option>
                            <option value="hourly" @if($sitemap->changefreq == "hourly") selected @endif >Hourly</option>
                            <option value="daily" @if($sitemap->changefreq == "daily") selected @endif >Daily</option>
                            <option value="weekly" @if($sitemap->changefreq == "weekly") selected @endif >Weekly</option>
                            <option value="monthly" @if($sitemap->changefreq == "monthly") selected @endif >Monthly</option>
                            <option value="yearly" @if($sitemap->changefreq == "yearly") selected @endif >Yearly</option>
                            <option value="never" @if($sitemap->changefreq == "never") selected @endif >Never</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="priority">Priority</label>
                    <div class="col-sm-9">
                        <input type="number" placeholder="priority" id="priority" name="priority" max="1" step="0.1" value="{{old('priority', $sitemap->priority)}}" class="form-control">
                    </div>
                </div>
                
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">Update</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
