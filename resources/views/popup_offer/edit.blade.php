<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">{{__('Offer Information')}}</h3>
	</div>

	<!--Horizontal Form-->
	<!--===================================================-->
	<form class="form-horizontal" action="{{ route('popup_offer.update', $popup_offer->id) }}" method="POST" enctype="multipart/form-data">
		@csrf
		<input type="hidden" name="_method" value="PATCH">
		<div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3" for="url">{{__('URL')}}</label>
                <div class="col-sm-9">
                    <input type="text" placeholder="{{__('URL')}}" id="url" name="url" class="form-control" value="{{$popup_offer->url}}" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label">{{__('Offer Image')}}</label>
                    <strong>(800px*400px)</strong>
                </div>
                <div class="col-sm-9">
                    <div id="photo">
						@if ($popup_offer->photo != null)
							<div class="col-md-4 col-sm-4 col-xs-6">
								<div class="img-upload-preview">
									<img loading="lazy"  src="{{ asset($popup_offer->photo) }}" alt="" class="img-responsive">
									<input type="hidden" name="previous_thumbnail_img" value="{{ $popup_offer->photo }}">
									<button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>
								</div>
							</div>
						@endif
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
			<button class="btn btn-purple" type="submit">{{__('Update')}}</button>
		</div>
	</form>
	<!--===================================================-->
	<!--End Horizontal Form-->

</div>

<script type="text/javascript">

    $(document).ready(function(){

        $('.demo-select2').select2();

        $("#photo").spartanMultiImagePicker({
            fieldName:        'photo',
            maxCount:         1,
            rowHeight:        '200px',
            groupClassName:   'col-md-4 col-sm-9 col-xs-6',
            maxFileSize:      '',
            dropFileLabel : "Drop Here",
            onExtensionErr : function(index, file){
                console.log(index, file,  'extension err');
                alert('Please only input png or jpg type file')
            },
			onExtensionErr : function(index, file){
				console.log(index, file);
				alert('Please only input jpg type file');
			},
            onSizeErr : function(index, file){
                console.log(index, file,  'file size too big');
                alert('File size too big');
            }
        });
    });

</script>
