@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <!-- <a href="{{ route('sellers.create')}}" class="btn btn-info pull-right">{{__('add_new')}}</a> -->
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Customers')}}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_customers" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder=" Type email or name & Enter">
                    </div>
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                    
                    <div class="" style="min-width: 100px;display:inline-block;">
                        <a href="{{route('customers.create')}}" class="btn btn-success">Add Customer</a>
                        <a href="{{route('customers.export')}}" class="btn btn-success">Export Customer</a>
                        <a href="{{route('userimoprt')}}" class="btn btn-success">Import Customer</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    
                    <th>{{__('Align Book Id')}}</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Email Address')}}</th>
                    <th>{{__('Phone')}}</th>
                    <th>{{__('Date')}}</th>
                    <th>{{__('Wallet Balance')}}</th>
                    <th>{{__('Cancel order credit limit')}}</th>
                    <th>{{__('No Of bookings')}}</th>
                    
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($customers as $key => $customer)
                    <tr>
                        <td>{{ ($key+1) + ($customers->currentPage() - 1)*$customers->perPage() }}</td>
                        @if($customer->user != null)
                        <td>{{$customer->user->alignbook_customer_id}}</td>
                        <td>{{$customer->user->name}}</td>
                        <td>{{$customer->user->email}}</td>
                        <td>{{$customer->user->phone}}</td>
                        <td> {{ date('d-M-y', strtotime($customer->user->created_at)) }}</td>
                        <td>{{single_price($customer->user->balance)}}<a href="{{route('wallet-history',encrypt($customer->id))}}">&nbsp;&nbsp;View History</a></td>
                        <td>{{$customer->user->order_cancel_credit}}</td>
                        <td><a target="_blank" href="{{route('orders.index.admin',['key' => encrypt($customer->user_id)])}}">{{customerBookingsCount($customer->user_id)}}</a></td>
                        
                        <td>
                            <a class="btn btn-primary" href="{{route('customers.edit', encrypt($customer->id))}}">{{__('Edit')}}</a>
                            <a class="btn btn-danger" onclick="confirm_modal('{{route('customers.destroy', $customer->id)}}');">{{__('Delete')}}</a>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $customers->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script type="text/javascript">
        function sort_customers(el){
            $('#sort_customers').submit();
        }
    </script>
@endsection
