@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Customer Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('customers.update', $customer->id) }}" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" value="{{ $customer->user->name }}" maxlegnth="190" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="email">{{__('Email')}}</label>
                    <div class="col-sm-9">
                        <input type="email" placeholder="{{__('Email')}}" id="email" name="email" value="{{ $customer->user->email }}" maxlegnth="30" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="mobile">{{__('Phone')}}</label>
                    <div class="col-sm-9">
                        <input type="tel" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10" placeholder="{{__('Phone')}}" id="mobile" name="mobile" value="{{ $customer->user->phone }}" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="password">{{__('Password')}}</label>
                    <div class="col-sm-9">
                        <input type="password" placeholder="{{__('Password')}}" id="password" name="password" class="form-control">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Address">{{__('Address')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Address')}}" id="address" name="address" value="{{ $customer->user->address }}" maxlegnth="300"  class="form-control">
                    </div>
                </div>
               
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Address"></label>
                    <div class="col-sm-3"> 
                        City <br />
                        <input type="text" placeholder="{{__('City')}}" id="city" name="city" value="{{ $customer->user->city }}" maxlegnth="30"  class="form-control">
                    </div>
                    <div class="col-sm-3"> 
                        State <br />
                        <input type="text" placeholder="{{__('State')}}" id="state" name="state" value="{{ $customer->user->state }}" maxlegnth="30" class="form-control">
                    </div>
                    <div class="col-sm-3"> 
                        Pincode <br />
                        <input type="text" placeholder="{{__('Pincode')}}" id="postal_code" name="postal_code" minlength="6" maxlength="6" value="{{ $customer->user->postal_code }}"  class="form-control" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">
                    </div>
                </div>
                
                
                <hr />
                
                
                
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{__('Current Wallet Balance')}}</label>
                    <div class="col-sm-9">
                        <input type="text"  name="balance" @if($customer->user->balance == null) value="0.00" @else value="{{ $customer->user->balance }}" @endif  class="form-control" readonly disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="balance_type">{{__('Wallet Balance')}}</label>
                    <div class="col-sm-3">
                        <select name="balance_type" id="balance_type" class="form-control">
                            <option value="add">Credit (+)</option>
                            <option value="remove">Debit (-)</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" placeholder="{{__('balance')}}" id="balance" name="balance" value="{{ old('balance') }}" max="10000" class="form-control">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="payment_details">{{__('Balance Details')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Balance Details')}}" id="payment_details" name="payment_details" value="{{ old('payment_details') }}" maxlegnth="500"  class="form-control">
                        <em>Will show to User Dashboard in Wallet History</em>
                    </div>
                </div>
                
               
                
            </div>
            
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
