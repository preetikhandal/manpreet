@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Add Customer Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('customers.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" value="{{old('name')}}" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="email">{{__('Email')}}</label>
                    <div class="col-sm-9">
                        <input type="email" placeholder="{{__('Email')}}" id="email" name="email" value="{{old('email')}}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="mobile">{{__('Phone')}}</label>
                    <div class="col-sm-9">
                        <input type="tel" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" value="{{old('mobile')}}" minlength="10" maxlength="10" placeholder="{{__('Phone')}}" id="mobile" name="mobile" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="password">{{__('Password')}}</label>
                    <div class="col-sm-9">
                        <input type="password" placeholder="{{__('Password')}}" id="password" name="password" class="form-control">
                    </div>
                </div>
                
                
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Address">{{__('Address')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Address')}}" id="address" name="address" value="{{ old('address') }}" maxlegnth="300"  class="form-control">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Address"></label>
                    <div class="col-sm-3"> 
                        City <br />
                        <input type="text" placeholder="{{__('City')}}" id="city" name="city" value="{{ old('city') }}" maxlegnth="30"  class="form-control">
                    </div>
                    <div class="col-sm-3"> 
                        State <br />
                        <input type="text" placeholder="{{__('State')}}" id="state" name="state" value="{{ old('state') }}" maxlegnth="30" class="form-control">
                    </div>
                    <div class="col-sm-3"> 
                        Pincode <br />
                        <input type="text" placeholder="{{__('Pincode')}}" id="postal_code" name="postal_code" minlength="6" maxlength="6" value="{{ old('postal_code') }}"  class="form-control" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">
                    </div>
                </div>
                
                <hr />
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Address">{{__('Wallet Balance')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('balance')}}" id="balance" name="balance" value="{{ old('balance') }}" maxlegnth="100"  class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Address">{{__('Balance Details')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Balance Details')}}" id="payment_details" name="payment_details" value="{{ old('payment_details') }}" maxlegnth="500"  class="form-control">
                        <em>Will show to User Dashboard in Wallet History</em>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
