@extends('layouts.app')

@section('content')

@php
    if(isset($seller_id)) {
    $admin_user_id = $seller_id;
} else {
$admin_user_id = \App\User::where('user_type', 'admin')->first()->id;
}
@endphp
    <div class="panel">
    	<div class="panel-body">
    		<div class="invoice-masthead">
    			<div class="invoice-text">
    				<h3 class="h1 text-thin mar-no text-primary">{{ __('Order Details') }}</h3>
    			</div>
    		</div>
            <div class="row">
                @php
             // dd(Route::currentRouteAction()." ~ ".Route::currentRouteName() );
                    $otc_confirmation = count($order->orderDetails->where('seller_id', $admin_user_id)->where('delivery_status', 'confirmation_pending'));
                    $delivery_status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->delivery_status??'NA';
                    $payment_status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->payment_status??'NA';
                @endphp
                @if($otc_confirmation == 0)
                <div class="col-lg-3">
                    <h3>Payment Status: @if($payment_status == 'unpaid') <span style="color:red">{{ucfirst($payment_status)}}</span> @else <span style="color:green">{{ucfirst($payment_status)}}</span></h3>@endif
                </div>
                    @if((Auth::user()->user_type == 'admin' || in_array('22', json_decode(Auth::user()->staff->role->permissions))))
                    <div class="col-lg-offset-3 col-lg-3">
                        <label for=update_payment_status"">{{__('Payment Status')}}</label>
                        <select class="form-control demo-select2"  data-minimum-results-for-search="Infinity" id="update_payment_status">
                            <option value="paid" @if ($payment_status == 'paid') selected @endif>{{__('Paid')}}</option>
                            <option value="unpaid" @if ($payment_status == 'unpaid') selected @endif>{{__('Unpaid')}}</option>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <label for=update_delivery_status"">{{__('Delivery Status')}}</label>
                        <select class="form-control demo-select2"  data-minimum-results-for-search="Infinity" id="update_delivery_status">
                            <option value="confirmation_pending" @if ($delivery_status == 'confirmation_pending') selected @endif>{{__('Confirmation Pending')}}</option>
                            <option value="hold" @if ($delivery_status == 'hold') selected @endif>{{__('Hold')}}</option>
                            <option value="pending" @if ($delivery_status == 'pending') selected @endif>{{__('Pending')}}</option>
                            <option value="on_review" @if ($delivery_status == 'on_review') selected @endif>{{__('On review')}}</option>
                            <option value="ready_to_ship" @if ($delivery_status == 'ready_to_ship') selected @endif>{{__('Ready to Ship')}}</option>
                            <option value="ready_to_delivery" @if ($delivery_status == 'ready_to_delivery') selected @endif>{{__('Ready to Delivery')}}</option>
                            <option value="shipped" @if ($delivery_status == 'shipped') selected @endif>{{__('Shipped')}}</option>
                            <option value="on_delivery" @if ($delivery_status == 'on_delivery') selected @endif>{{__('On delivery')}}</option>
                            <option value="delivered" @if ($delivery_status == 'delivered') selected @endif>{{__('Delivered')}}</option>
                            <option value="processing_refund" @if ($delivery_status == 'processing_refund') selected @endif>{{__('Processing Refund')}}</option>
                            <option value="refunded" @if ($delivery_status == 'refunded') selected @endif>{{__('Refunded')}}</option>
                            <option value="cancel" @if ($delivery_status == 'cancel') selected @endif>{{__('Cancel')}}</option>
                        </select>
                    </div>
                    @endif
                @else
                <div class="col-lg-3">
                    <h3>Payment Status: @if($payment_status == 'unpaid') <span style="color:red">{{ucfirst($payment_status)}}</span> @else <span style="color:green">{{ucfirst($payment_status)}}</span></h3>@endif
                </div>
                <div clas="col-md-6">
                    <div class=" pull-right">
                    @if($payment_status == 'unpaid')
                        <button class="btn btn-primary" onclick="confirm_payment('paid')">Mark Paid</button>
                    @else 
                        <button class="btn btn-danger" onclick="confirm_payment('unpaid')">Mark Unpaid</button>
                    @endif
                    <button class="btn btn-primary" onclick="confirm_order('pending')">Confirm Order</button>
                    <button class="btn btn-primary" onclick="confirm_order('hold')">Mark Hold</button>
                        @if($order->prescription_file != null)<a  class="btn btn-primary" href="{{ asset('prescription/'.$order->prescription_file) }}" target="_blank">{{__('Download Prescription')}}</a> @endif
                    </div>
                </div>
                @endif
            </div>
            <hr>
    		<div class="invoice-bill row">
    			<div class="col-sm-6 text-xs-center">
    				<address>
        				{{ json_decode($order->shipping_address)->name }}<br>
						@if(!empty(json_decode($order->shipping_address)->email))
						{{ json_decode($order->shipping_address)->email }} <br>
						@endif
						{{ json_decode($order->shipping_address)->phone }}<br>
						{{ json_decode($order->shipping_address)->address }},
						{{ json_decode($order->shipping_address)->city }},
					
						    @if(isset(json_decode($order->shipping_address)->state))
						       {{ json_decode($order->shipping_address)->state }},
						    @else
						    NA
						    @endif
						<br>
						{{ json_decode($order->shipping_address)->postal_code }}
						  
                    </address>

                    @if ($order->manual_payment && is_array(json_decode($order->manual_payment_data, true)))
                        <br>
                        <strong class="text-main">{{ __('Payment Information') }}</strong><br>
                        Name: {{ json_decode($order->manual_payment_data)->name }}, Amount: {{ single_price(json_decode($order->manual_payment_data)->amount) }}, TRX ID: {{ json_decode($order->manual_payment_data)->trx_id }}
                        <br>
                        <a href="{{ asset(json_decode($order->manual_payment_data)->photo) }}" target="_blank"><img src="{{ asset(json_decode($order->manual_payment_data)->photo) }}" alt="" height="100"></a>
                    @endif

    			</div>

    			<div class="col-sm-6 text-xs-center">
    				<table class="invoice-details">
    				<tbody>
    				<tr>
    					<td class="text-main text-bold">
    						{{__('Order #')}}
    					</td>
    					<td class="text-right text-info text-bold">
    						{{ $order->code }}
    					</td>
    				</tr>
    				<tr>
    					<td class="text-main text-bold">
    						{{__('Order Status')}}
    					</td>
                        @php
                            $status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->delivery_status??'NA';
                        @endphp
    					<td class="text-right">
                            @if($status == 'delivered')
                                <span class="badge badge-success">{{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                            @else
                                <span class="badge badge-info">{{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                            @endif
    					</td>
    				</tr>

    				<tr>
    					<td class="text-main text-bold">
    						{{__('Order Date')}}
    					</td>
    					<td class="text-right">
    						{{ date('d-m-Y h:i A', $order->date) }} (UTC)
    					</td>
    				</tr>

                    <tr>
    					<td class="text-main text-bold">
    						{{__('Total amount')}}
    					</td>
    					<td class="text-right">
    						{{ single_price($order->orderDetails->where('seller_id', $admin_user_id)->sum('price') + $order->orderDetails->where('seller_id', $admin_user_id)->sum('tax')) }}
    					</td>
    				</tr>
                    <tr>
    					<td class="text-main text-bold">
    						{{__('Payment method')}}
    					</td>
    					<td class="text-right">
    						{{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}
    					</td>
    				</tr>
    				@if($order->payment_type == 'razorpay')
    				    @if($order->payment_details != null)
    				    <tr>
        					<td class="text-main text-bold">
        						{{__('Payment ID')}}
        					</td>
        					<td class="text-right">
        						{{ json_decode($order->payment_details)->id }}
        					</td>
    				    </tr>
						
    				    @endif
    				@endif
    				
    				@if($order->payment_type == 'paytm')
    				    @if($order->payment_details != null)
    				    <tr>
        					<td class="text-main text-bold">
        						{{__('Payment ID')}}
        					</td>
        					<td class="text-right">
        						{{ json_decode($order->payment_details)->TXNID }}
        					</td>
    				    </tr>
						
    				    @endif
    				@endif
    				
					<?php $total = $order->orderDetails->sum('price')+$order->orderDetails->sum('tax')?>
				
    				</tbody>
    				</table>
    			</div>
    		</div>

			<div class="row">
			<button type="button" class="btn btn-primary" id="showUpdate">Update order</button>
			</div>
			<div class="clearfix">&nbsp;&nbsp;</div>
			<div id="update_order" style="display:none;">
				<div class="row">
					<div class="col-md-2">
							<label>Search product</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" id="searchProduct" placeholder="Enter product name/sku-code">
					</div>
					<div class="col-md-2">
					
					<button type="button" class="btn btn-primary" id="searchProductDetails">Search Product</button>
					</div>	
				</div>
				
				<div class="clearfix">&nbsp;&nbsp;</div>
				<div class="row" id="show_product" style="display:none;">
					
					<div class="col-md-2">
						<div class="product-decs text-center">
							<p>Product Title:-<span id="productTitle"></span></p>
							
							<p>Product price:-<span id="producPrice"></span></p>
								
						</div>
					</div>
					<div class="col-md-2">
					<button type="button" class="btn btn-primary" id="addOrder">Add Product</button>
					</div>

					<div class="col-md-4">
						<span id="show_error" style="color:red;"></span>
					</div>
				</div>
				
			</div>
			<div class="col-md-4">
						<span id="show_error1" style="color:red;margin-left: 173px;"></span>
					</div>
			<input type="hidden" id="currentOrderId" value="{{$order->id}}">
			


    		<hr class="new-section-sm bord-no">
    		<form action="{{route('orders.update',['id' => $order->id])}}" method="post">
    		    @csrf
    		<div class="row">
    			<div class="col-lg-12 table-responsive">
    				<table class="table table-bordered invoice-summary">
        				<thead>
            				<tr class="bg-trans-dark">
                                <th class="min-col text-center">#</th>
                                <th width="10%">
            						{{__('Photo')}}
            					</th>
            					<th class="text-uppercase">
            						{{__('Description')}}
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Qty')}} <br />
            						A
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Unit Price')}}<br /> B
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Unit Discount')}}<br /> C
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Effective Unit Price')}}<br /> 
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Total Price')}}<br /> D = A * B
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Total Discount')}} <br /> F = A * C
            					</th>
            					<th class="min-col text-right text-uppercase">
            						{{__('Total')}} <br /> E = D - F
            					</th>
            					<th class="min-col text-right text-uppercase">
            						{{__('Exclusive ')}} 
            					</th>
            					<th class="min-col text-right text-uppercase">
            						{{__('Inclusive  ')}} 
            					</th>
								
            				</tr>
        				</thead>
        				<tbody>
                            @foreach ($order->orderDetails->where('seller_id', $admin_user_id) as $key => $orderDetail)
                                <tr>
                                    <input type="hidden" name="order_detail[{{$orderDetail->id}}]" value="{{$orderDetail->id}}">
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td>
                                        @if ($orderDetail->product != null)
                    						<a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank"><img height="50" src="{{ asset($orderDetail->product->thumbnail_img) }}"></a>
                                        @else
                                            <strong>{{ __('N/A') }}</strong>
                                        @endif
                                    </td>
                					<td>
                					    @php
                					    $return_request=\App\ReturnProduct::where('order_id',$orderDetail->order_id)->where('product_id',$orderDetail->product_id)->first();
                					   // dd($return_request);
                					    @endphp
                                        @if ($orderDetail->product != null)
                    						<strong><a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank">{{ $orderDetail->product->name }}</a> @if($return_request!=null) <span style="color:red">{{__('Return Requested')}}</span> @endif</strong>
                                        @else
                                            <strong>{{ __('Product Unavailable') }}</strong>
                                        @endif
                					</td>
                					<td class="text-center">
                						<input type="text" name="quantity[{{$orderDetail->id}}]" value="{{ $orderDetail->quantity }}" onkeyup="checkthis({{$orderDetail->id}}, this)" class="form-control">
                					</td>
            					    <td class="text-center del_{{$orderDetail->id}}" colspan="5" style="display:none;">
            					        <em style="color:red">This product will remove on update.</em>
            					    </td>
                					<td class="text-center show_{{$orderDetail->id}}">
                						<input type="text" name="unit_price[{{$orderDetail->id}}]" value="{{ $orderDetail->price/$orderDetail->quantity + $orderDetail->discount/$orderDetail->quantity}}" class="form-control">
                					</td>
                					<td class="text-center show_{{$orderDetail->id}}"><input type="text" name="discount[{{$orderDetail->id}}]" value="{{ $orderDetail->discount/$orderDetail->quantity }}" class="form-control">
                					</td>
                					<td class="text-center show_{{$orderDetail->id}}">{{ single_price($orderDetail->price/$orderDetail->quantity) }}</td>
                					<td class="text-center show_{{$orderDetail->id}}">
                						{{ single_price($orderDetail->price + $orderDetail->discount) }}
                					</td>
                					<td class="text-center show_{{$orderDetail->id}}" @if($orderDetail->discount > 0) style="color:red" @endif> {{ single_price( $orderDetail->discount) }}
                					</td>
                                    <td class="text-center show_{{$orderDetail->id}}">
                						{{ single_price($orderDetail->price) }}
                					</td>
                					@php
                               $getPrice = \App\OrderDetail::where('order_id', $order->id)->get(); 
                               if(isset($getPrice[0]->inclusive_price)){
                                   $comissionselleralde = single_price($order->seller_commission);
                                }else{
                                    $comissionselleralde = 0;
                                }
                               
                                if(isset($getPrice[0]->exclusive_price)){
                                   $comissionselleraldeex = single_price($order->seller_commission);
                                }else{
                                    $comissionselleraldeex = 0;
                                }
                            @endphp
                					
                					 <td class="text-center show_{{$orderDetail->id}}">
                					       {{$comissionselleraldeex}}
                					</td>
                					
                					 <td class="text-center show_{{$orderDetail->id}}">
                					  {{$comissionselleralde}}
                					</td>
									
                				</tr>
                            @endforeach
        				</tbody>
    				</table>
    			<script>
    			function checkthis(id, ele) {
    			    v = $(ele).val();
    			    if(v == 0) {
    			        $('.show_'+id).hide();
    			        $('.del_'+id).show();
    			    } else {
    			        $('.show_'+id).show();
    			        $('.del_'+id).hide();
    			    }
    			}
    			</script>
    		<button class="btn btn-primary">Update Invoice Value</button>
    			</div>
    		</div>
    		</form>
    		<div class="clearfix">
    			<table class="table invoice-total">
    			<tbody>
    			<tr>
    				<td>
    					<strong>{{__('Sub Total')}} :</strong>
    				</td>
    				<td>
    					{{ single_price($order->orderDetails->where('seller_id', $admin_user_id)->sum('price')) }}
    				</td>
    			</tr>
    			<tr>
    				<td>
    					<strong>{{__('Tax')}} :</strong>
    				</td>
    				<td>
    					{{ single_price($order->orderDetails->where('seller_id', $admin_user_id)->sum('tax')) }}
    				</td>
    			</tr>
                <tr>
    				<td>
    					<strong>{{__('Shipping')}} :</strong>
    				</td>
    				<td>
    					{{ single_price($order->orderDetails->where('seller_id', $admin_user_id)->sum('shipping_cost')) }}
    				</td>
    			</tr>
    			
    			@if($order->cart_shipping > 0)
    			<tr>
    				<td>
    					<strong>{{__('Delivery Charges')}} :</strong>
    				</td>
    				<td class="text-bold h4">
    					{{ single_price($order->cart_shipping) }}
    				</td>
    			</tr>
    			@endif
				<tr>
    				<td>
    					<strong>{{__('TOTAL')}} :</strong>
    				</td>
    				<td class="text-bold h4">
    					{{ single_price($order->grand_total) }}
    				</td>
    			</tr>
    			</tbody>
    			</table>
    		</div>
    		 @if((Auth::user()->user_type == 'admin' || in_array('22', json_decode(Auth::user()->staff->role->permissions))))
    		<div class="col-md-12">
    		    <form action="{{route('orders.deliveryupdate')}}" method="post">
    		        <input name="order_id" type="hidden" value="{{$order->id}}">
        	            @csrf
    		       <div class="col-md-1 text-right">
    		           Delivery Charges &nbsp; &nbsp;
    		       </div>
    		       <div class="col-md-3">
    		           <input type="text" class="form-control" name="delivery_charges" value="{{$order->cart_shipping}}" >
    		       </div>
		           <div class="col-md-2">
		               <button type="submit" class="btn btn-primary">Update</button>
		           </div>
		       </form>
    		</div>
    		@endif
    		<div class="col-md-12">
    		    <br />
    		  Total Order Value : {{ single_price($order->grand_total + $order->coupon_discount + $order->wallet_credit) }} &nbsp; &nbsp; &nbsp; &nbsp;
               Discount :  {{ single_price($order->coupon_discount) }} &nbsp; &nbsp; &nbsp; &nbsp;
               Use Wallet Credit :  {{ single_price($order->wallet_credit) }} &nbsp; &nbsp; &nbsp; &nbsp;
               Total Amount : {{ single_price($order->grand_total) }}
    		</div>
    		<div class="text-right no-print">
    			<a href="{{ route('customer.invoice.download', $order->id) }}" class="btn btn-default"><i class="demo-pli-printer icon-lg"></i></a>
    		</div>
    	</div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#update_delivery_status').on('change', function(){
            var order_id = {{ $order->id }};
            var status = $('#update_delivery_status').val();
            
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                showAlert('success', 'Delivery status has been updated');
            });
        
        });
        function confirm_payment(status) {
            var order_id = {{ $order->id }};
            var setFlash = 'Payment status has been updated';
             $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                location.reload();
            });
        }

        function confirm_order(status) {
            var order_id = {{ $order->id }};
            var setFlash = 'Order status has been updated';
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                  location.reload();
                });
        }

        $('#update_payment_status').on('change', function(){
            var order_id = {{ $order->id }};
            var status = $('#update_payment_status').val();
            $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                showAlert('success', 'Payment status has been updated');
            });
		});
		

		//Select Category
		$(function(){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
		});

		$('#showUpdate').on('click',function(){
			
			$('#update_order').show();
		});

		$("#searchProductDetails").on('click',function(){

		var productId = $('#searchProduct').val(); 
		var orderId =  $('#currentOrderId').val();
		if(productId == ''){
			
			$('#show_error1').html('please Enter product id/Sku-code');
			return false;
		}
		$('#show_error1').html('');
			$.ajax({
				url:"{{route('ajax.searchProduct')}}",
				type: 'POST',
				dataType:'json',
				data:{'productId':productId,'order_id':orderId},
				success: function(data) {
					console.log(data.product_name);
					if(data.status == 200){
						$('#show_product').show();
						$('#productTitle').html(data.product_name);
						$('#productDesc').html(data.description);
						$('#producPrice').html(data.price);
						$('#producphotos').html(data.photos);
					}else{
						$('#show_error1').html(data.product_name);
					}
					
				}
			});	
		});






		$("#addOrder").on('click',function(){

			var productId = $('#searchProduct').val(); 
			var orderId =  $('#currentOrderId').val();
			if(productId == ''){
				
				$('#show_error').html('please Enter product id/Sku-code');
				return false;
			}
			$('#show_error').html('');
				$.ajax({
					url:"{{route('ajax.updatedOrder')}}",
					type: 'POST',
					dataType:'json',
					data:{'productId':productId,'order_id':orderId},
					success: function(data) {
						console.log(data.product_name);
						
						$('#show_error').html('product added sucessfully');
						$('#show_error').css("color", "green");
						setTimeout(function(){ 
							location.reload();
							}, 1000);
					}
				});	
		});
		
		

    </script>
@endsection
