@extends('layouts.app')

@section('content')
@php
    $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
@endphp

<!-- Basic Data Tables -->
<!--===================================================-->
<style>
    .ordcode{
        color:#282563;
    }
    .ordcode:hover{
        color:#EB3038;
    }
</style>
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <!-- <h3 class="panel-title pull-left pad-no">{{__('Orders')}}</h3> -->
        <div class="pull-right clearfix">
            <form class="" id="sort_orders" action="" method="GET">
                <!-- <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 300px;">
                        <select class="form-control demo-select2" name="payment_type" id="payment_type" onchange="sort_orders()">
                            <option value="">{{__('Filter by Payment Status')}}</option>
                            <option value="paid"  @isset($payment_status) @if($payment_status == 'paid') selected @endif @endisset>{{__('Paid')}}</option>
                            <option value="unpaid"  @isset($payment_status) @if($payment_status == 'unpaid') selected @endif @endisset>{{__('Un-Paid')}}</option>
                        </select>
                    </div>
                </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 300px;">
                        <select class="form-control demo-select2" name="delivery_status" id="delivery_status" onchange="sort_orders()">
                            <option value="">{{__('Filter by Deliver Status')}}</option>
                            <option value="confirmation_pending"   @isset($delivery_status) @if($delivery_status == 'confirmation_pending') selected @endif @endisset>{{__('Confirmation Pending')}}</option>
                            <option value="hold"   @isset($delivery_status) @if($delivery_status == 'hold') selected @endif @endisset>{{__('Hold')}}</option>
                            <option value="pending"   @isset($delivery_status) @if($delivery_status == 'pending') selected @endif @endisset>{{__('Pending')}}</option>
                            <option value="on_review"   @isset($delivery_status) @if($delivery_status == 'on_review') selected @endif @endisset>{{__('On review')}}</option>
                            <option value="ready_to_ship"   @isset($delivery_status) @if($delivery_status == 'ready_to_ship') selected @endif @endisset>{{__('Ready to Ship')}}</option>
                            <option value="ready_to_delivery"   @isset($delivery_status) @if($delivery_status == 'ready_to_delivery') selected @endif @endisset>{{__('Ready to Delivery')}}</option>
                            <option value="shipped"   @isset($delivery_status) @if($delivery_status == 'shipped') selected @endif @endisset>{{__('Shipped')}}</option>
                            <option value="on_delivery"   @isset($delivery_status) @if($delivery_status == 'on_delivery') selected @endif @endisset>{{__('On delivery')}}</option>
                            <option value="delivered"   @isset($delivery_status) @if($delivery_status == 'delivered') selected @endif @endisset>{{__('Delivered')}}</option>
                            <option value="processing_refund"   @isset($delivery_status) @if($delivery_status == 'processing_refund') selected @endif @endisset>{{__('Processing Refund')}}</option>
                            <option value="refunded"   @isset($delivery_status) @if($delivery_status == 'refunded') selected @endif @endisset>{{__('Refunded')}}</option>
                            <option value="cancel"   @isset($delivery_status) @if($delivery_status == 'cancel') selected @endif @endisset>{{__('Cancel')}}</option>
                        </select>
                    </div>
                </div> -->
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Order Code')}}</th>
                    <th>{{__('Num. of Products')}}</th>
                    <th>{{__('Customer')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Order Status')}}</th>
                    <th>{{__('Pay Method')}}</th>
                    <th>{{__('Pay Status')}}</th>
                    <th>{{__('Seller Amount')}}</th>
                    <th>{{__('Service Provider Comission')}}</th>
                    <th>{{__('Inclusive Comission')}}</th>
                    <th>{{__('Exclusive Comission')}}</th>
                    <th>{{__('Credit to seller wallet')}}</th>
                    <th>{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $key => $order_id)
                    @php
                        $seller_id = $order_id->seller_id;
                        $order = \App\Order::find($order_id->id);
                         $orderDetails = \App\OrderDetail::find($order_id->id);
                    @endphp
                    @if($order != null)
                        @php
                        if ($order->orderDetails->where('seller_id',  $seller_id)->first()->payment_status == 'paid') {
                            $payment_status = "paid";
                        } else {
                            $payment_status = "unpaid";
                        }
                        @endphp
                        <tr>
                            <td>
                                {{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }} <input type="checkbox" data-ordercode="{{$order->code}}" data-order-seller_id="{{$seller_id}}" data-payment="{{$payment_status}}" name="order_id[]" value="{{$order->id}}" id="check_order_id" class="check_order_id" />
                            </td>
                            <td>
                                <a href="{{ route('orders.seller.show', [$seller_id, encrypt($order->id)]) }}" class="ordcode"> {{ $order->code }}</a>@if($order->viewed == 0) <span class="pull-right badge badge-info">{{ __('New') }}</span> @endif
                            </td>
                            
                            <td>{{ \App\Product::where('user_id', $seller_id)->count() }}</td>
                            <td>
                                @if ($order->user_id != null)
                                    {{ $order->user->name??' ' }}
                                @else
                                    Guest ({{ $order->guest_id }})
                                @endif
                            </td>
                            <td>
								{{ single_price($order->orderDetails->where('seller_id', $seller_id)->sum('price') + $order->orderDetails->where('seller_id', $seller_id)->sum('tax')) }}
		
                            </td>
                            <td>
                                @php
                                    $status = $order->orderDetails->where('seller_id', $seller_id)->first()->delivery_status;
                                @endphp
                                @if($status == "Confirmation Pending")
                                <label class="label label-danger">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @elseif($status == "pending")
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @else
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @endif
                            </td>
                            <td>
                                {{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}
                            </td>
                            <td>
                                <span class="badge badge--2 mr-4">
                                    @if ($payment_status == 'paid')
                                        <i class="bg-green"></i> Paid
                                    @else
                                        <i class="bg-red"></i> Unpaid
                                    @endif
                                </span>
                            </td>
                            <td>{{single_price($order->service_provider)}}</td>
                            <td>{{$order->seller_commission}}</td>
                            @php
                               $getPrice = \App\OrderDetail::where('order_id', $order_id->id)->get(); 
                               if(isset($getPrice[0]->inclusive_price)){
                                   $comissionselleralde = single_price($order->seller_commission);
                                }else{
                                    $comissionselleralde = 0;
                                }
                               
                                if(isset($getPrice[0]->exclusive_price)){
                                   $comissionselleraldeex = single_price($order->seller_commission);
                                }else{
                                    $comissionselleraldeex = 0;
                                }
                            @endphp
                           <td>{{$comissionselleralde}}</td>
                           
                            <td>{{$comissionselleraldeex}}</td>
                            <td>{{$order->paid_to_seller_account}}</td>
                            <td>
                                @if(count($order->orderDetails->where('seller_id', $seller_id)->where('delivery_status', 'confirmation_pending')) > 0)
                                        <a  href="javascript:confirm_order({{$order->id}}, {{$seller_id}}, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a>
                                        @if($order->prescription_file != null) 
                                            <a href="{{ asset('prescription/'.$order->prescription_file) }}" class="btn btn-primary">{{__('Download Prescription')}}</a>
                                        @endif
                                        @if($payment_status == "paid")
                                            <a href="javascript:confirm_order({{$order->id}}, {{$seller_id}}, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                        @else
                                            <a href="javascript:confirm_order({{$order->id}}, {{$seller_id}}, 'cancel')" class="btn btn-primary">Cancel Order</a>
                                        @endif
                                @else
                                    @if($status == "delivered")
                                    @elseif($status == "pending")
                                        <a  href="javascript:confirm_order({{$order->id}}, {{$seller_id}}, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a><a href="javascript:trash_order({{$order->id}}, {{$seller_id}}, 'trash')" class="btn btn-primary">Trash Order</a>
                                        @if($order->prescription_file != null) 
                                            <a href="{{ asset('prescription/'.$order->prescription_file) }}" class="btn btn-primary">{{__('Download Prescription')}}</a>
                                        @endif
                                        <!--a onclick="confirm_modal('{{route('orders.destroy', $order->id)}}');" class="btn btn-primary">{{__('Delete')}}</a-->
                                        @if($payment_status == "paid")
                                            <a href="javascript:confirm_order({{$order->id}}, {{$seller_id}}, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                        @else
                                            <a href="javascript:confirm_order({{$order->id}}, {{$seller_id}}, 'cancel')" class="btn btn-primary">Cancel Order</a>
                                        @endif
                                    @elseif($status == "ready_to_ship") 
                                        <a href="javascript:order_shipping_info({{ $order->id }}, {{$seller_id}})"  class="btn btn-primary">{{__('Add Shipping info')}}</a>
                                        <a href="{{ route('orders.printlabel.seller', [$seller_id, $order->id]) }}" target="_blank" class="btn btn-primary">{{__('Print Label')}}</a>
                                    @elseif($status == "shipped") 
                                        @if($payment_status == 'unpaid')
                                            <a href="javascript:confirm_payment({{$order->id}}, {{$seller_id}}, 'paid')" class="btn btn-primary">Mark Paid</a>
                                            <span id="orderbtn_{{$order->id}}"><a href="javascript:void(0)" class="btn btn-primary" disabled>{{__('Mark Delivered')}}</a></span>
                                        @else
                                        <span id="orderbtn_{{$order->id}}"><a href="javascript:confirm_order({{$order->id}}, {{$seller_id}}, 'delivered')" class="btn btn-primary">{{__('Mark Delivered')}}</a></span>
                                        @endif
                                    @elseif($status == "processing_refund") 
                                        <a href="javascript:confirm_order({{$order->id}}, {{$seller_id}}, 'refunded')" class="btn btn-primary">Refunded to Wallet</a>
                                    @elseif($status == "on_review" or $status == "hold")
                                        <a href="javascript:confirm_order({{$order->id}}, {{$seller_id}}, 'pending')" class="btn btn-primary">Move to Pending</a>
                                    @else
                                    <button class="btn btn-primary" disabled>{{__('No Action')}}</button>
                                    @endif
                                @endif
                            </td>
                        </tr>
                     @endif
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="col-md-12">
                <br />&nbsp;
                @if(\Route::currentRouteName() == 'orders.index.shipped.admin.seller' )
                <button class="btn btn-success" id="markdeliverybtn" onclick="showmodal('mark_delivery_modal')" disabled>Mark Selected to Delivered</button>
                <button class="btn btn-primary" id="markpaidbtn" onclick="showmodal('payment_status_modal');" disabled>Mark Selected to Paid</button>
                @endif
                
                <br />&nbsp;
                <br />&nbsp;
            </div>
        </div>
        <div class="clearfix">
            <div class="pull-right">
                {{ $orders->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>
<script>
    function showmodal(modalid) {
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
        productArray.push($(v).val());
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        $('#'+modalid).modal();
    }
</script>

<div class="modal fade" id="payment_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Changing of Payment Status')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 text-right pt-2">
                       <center><strong>Do you want to continue ?</strong></center>
                    </div>
                        <input type="hidden" value="paid" id="modal_payment_status">
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="change_status('payment')"  data-dismiss="modal">Continue</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mark_delivery_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Changing of Delivery Status')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 text-right pt-2">
                       <center><strong>Do you want to continue ?</strong></center>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="mark_delivered()"  data-dismiss="modal">Continue</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="shipping_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Shipping Info')}}</h4>
            </div>
            <form action="{{route('orders.shipping')}}" method="post">
            @csrf
            <div class="modal-body">
            <input type="hidden" name="order_id" id="modal_order_id">
            <input type="hidden" name="seller_id" id="modal_seller_id">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Courier &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name='courier' placeholder="Courier Company Name" >
                    </div>
                </div>
                <div class="form-group row" style="margin-top:5px;">
                    <div class="col-sm-3 text-right pt-2">
                        AWB/Ref/Tracking No. &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="awb" class="form-control" placeholder="AWB Number" >
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                  <button class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')

    <script type="text/javascript">
        
        $('#undelivery_Form').submit(function(e){
            e.preventDefault();
            formdata =  $('#undelivery_Form').serialize();
            $('#undelivery_status_modal').modal('hide');
            $.ajax({
                type: 'POST',
                url: '{{ route('orders.update_status') }}',
                data:formdata,
                processData: false,
                success: function(result){
                 location.reload();
                }
            });
        });
    @if(\Route::currentRouteName() == 'orders.index.shipped.admin' )
    
    function checkdeliverypayButton() {
        productunpaidArray = [];
        productpaidArray = [];
        $('#check_order_id:checked').each(function(i,v) {
            if($(v).data('payment') == 'unpaid') {
                productunpaidArray.push($(v).val());
            } else {
                productpaidArray.push($(v).val());
            }
        });
        console.log(productunpaidArray.length);
        console.log(productpaidArray.length);
        
        if(productunpaidArray.length == 0) {
            $('#markdeliverybtn').prop('disabled', false);
        } else {
            $('#markdeliverybtn').prop('disabled', 'disabled');
        }
        
        if(productpaidArray.length == 0) {
            $('#markpaidbtn').prop('disabled', false);
        } else {
            $('#markpaidbtn').prop('disabled', 'disabled');
        }
        if(productpaidArray.length == 0 && productunpaidArray.length == 0 ) {
            $('#markpaidbtn').prop('disabled', 'disabled');
            $('#markdeliverybtn').prop('disabled', 'disabled');
        }
    }
    
    $('.check_order_id').click(function() {
        checkdeliverypayButton();
    });
    
    @endif
    
    function mark_delivered() {
        productArray = [];
        seller_id = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
           seller_id.push($(v).data('order-seller_id'));
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "delivered";
        var setFlash = 'Order set for delivered status.';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, seller_ids : seller_id }, function(data){
             location.reload();
        });
    }
        
    function change_status(action) {
        if(action == 'delivery') {
            status = $('#modal_delivery_status').val();
            $('#modal_delivery_status').val("");
            $('#modal_delivery_status').trigger('change');
        }
        if(action == 'payment') {
            status = $('#modal_payment_status').val();
            $('#modal_payment_status').val("");
            $('#modal_payment_status').trigger('change');
        }
        productArray = [];
        seller_id = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
           seller_id.push($(v).data('order-seller_id'));
        }); 
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        
        var setFlash = 'Order '+action+' status has been updated';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, status:status, seller_ids : seller_id}, function(data){
             location.reload();
        });
    }
        
    function order_shipping_info(order_id, seller_id)
    {
        $('#modal_order_id').val(order_id);
        $('#modal_seller_id').val(seller_id);
        $('#shipping_info').modal();
    }
        function sort_orders(el){
            $('#sort_orders').submit();
        }
    
        function confirm_payment(order_id, seller_id, status) {
            var setFlash = 'Payment status has been updated';
            $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id, status:status, seller_id : seller_id }, function(data){
                location.reload();
            });
        }

        function confirm_order(order_id, seller_id, status) {
            var setFlash = 'Order status has been updated';
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, seller_id : seller_id }, function(data){
                 location.reload();
            });
        }
        
         function trash_order(order_id, seller_id,status) {
            var setFlash = 'Order status has been updated';
             $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, seller_id : seller_id }, function(data){
                 location.reload();
            });
        }
    </script>
@endsection