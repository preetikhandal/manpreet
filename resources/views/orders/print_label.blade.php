<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Order Label Print</title>
    <meta http-equiv="Content-Type" content="text/html;"/>
    <meta charset="UTF-8">
	<style media="all">
		*{
			margin: 0;
			padding: 0;
			line-height: 1.3;
			font-family: sans-serif;
			color: #333542;
		}
		body{
			font-size: .875rem;
		}
		.gry-color *,
		.gry-color{
			color:#878f9c;
		}
		table{
			width: 100%;
		}
		table th{
			font-weight: normal;
		}
		table.padding th{
			padding: .5rem .7rem;
		}
		table.padding td{
			padding: .7rem;
		}
		table.sm-padding td{
			padding: .2rem .7rem;
		}
		.border-bottom td,
		.border-bottom th{
			border-bottom:1px solid #eceff4;
		}
		.text-left{
			text-align:left;
		}
		.text-right{
			text-align:right;
		}
		.small{
			font-size: .85rem;
		}
		.currency{

		}
	</style>
</head>
<body>
	<div>

		@php
			$generalsetting = \App\GeneralSetting::first();
		@endphp

		<div style="background: #eceff4;padding: 1.5rem;">
			<table>
				<tr>
					<td>
						@if($generalsetting->logo != null)
							<img loading="lazy"  src="{{ asset($generalsetting->logo) }}" height="80" style="display:inline-block;">
						@else
							<img loading="lazy"  src="{{ asset('frontend/images/logo/logo.png') }}" height="80" style="display:inline-block;">
						@endif
					</td>
                    <td style="font-size: 1.5rem;" class="text-right strong">@if($order->payment_type == "cash_on_delivery") <strong>COD<br /> Collect {{single_price($COD)}}</strong> @else <strong>Paid</strong>@endif</td>
				</tr>
			</table>
			<table>
				<tr>
					<td class="text-right small"><span class="gry-color small">Order ID:</span> <span class="strong">{{ $order->code }}</span></td>
				</tr>
				<tr>
					<td class="text-right small"><span class="gry-color small">Order Date:</span> <span class=" strong">{{ date('d-m-Y', $order->date) }}</span></td>
				</tr>
			</table>

		</div>

		<div style="padding: 1.5rem;padding-bottom: 0">
            <table><tr><td>
			     <table>
				@php
					$shipping_address = json_decode($order->shipping_address);
				@endphp
				<tr><td class="strong small gry-color">Delivery To:</td></tr>
				<tr><td class="strong">{{ $shipping_address->name }}</td></tr>
				<tr><td class="gry-color small">{{ $shipping_address->address }}, {{ $shipping_address->city }}, {{ $shipping_address->country }}</td></tr>
				<tr><td class="gry-color small">Email: {{ $shipping_address->email }}</td></tr>
				<tr><td class="gry-color small">Phone: {{ $shipping_address->phone }}</td></tr>
                </table>
                </td>
                <td>
                    
                    @if($User->user_type == "admin" || $User->user_type == "staff")
                    <table><tr><td class="strong small gry-color">From:</td></tr>
                    <tr><td class="strong">{{ $generalsetting->site_name }}</td></tr>
                    <tr><td class="gry-color small"> {{ $generalsetting->address }}</td></tr>
                    <tr><td class="gry-color small">Email: {{ $generalsetting->email }}</td></tr>
                    <tr><td class="gry-color small">Phone: {{ $generalsetting->phone }}</td></tr>
                    </table>
                    @elseif($User->user_type == "vendor")
                    @php
                    $Vendor = \App\Vendor::where('user_id', $User->id)->first();
                    @endphp
                    <table><tr><td class="strong small gry-color">From:</td></tr>
                    <tr><td class="strong">{{ $User->name }}</td></tr>
                    <tr><td class="gry-color small"> {{$Vendor->address_1}} {{$Vendor->address_2}} {{$Vendor->city}} {{$Vendor->state}} {{$Vendor->pincode}}</td></tr>
                    <tr><td class="gry-color small">Email: {{ $User->email }}</td></tr>
                    <tr><td class="gry-color small">Phone: {{ $Vendor->contact_number }}</td></tr>
                    </table>
                    @elseif($User->user_type == "seller") 
                    <table><tr><td class="strong small gry-color">From:</td></tr>
                    <tr><td class="strong">{{ $User->shop->name }}</td></tr>
                    <tr><td class="gry-color small"> {{ $User->shop->address }}</td></tr>
                    <tr><td class="gry-color small">Email: {{ $User->email }}</td></tr>
                    <tr><td class="gry-color small">Phone: {{ $User->phone }}</td></tr>
                    </table>
                    @endif
                    
                </td>
                </tr>
            </table>
		</div>

	    <div style="padding: 1.5rem;">
			<table class="padding text-left small border-bottom">
				<thead>
	                <tr class="gry-color" style="background: #eceff4;">
	                    <th width="35%">Category</th>
	                    <th width="10%">Qty</th>
	                </tr>
				</thead>
				<tbody class="strong">
	                @foreach ($Products as $key => $P)
		                @if ($P != null)
							<tr class="">
								<td>{{ $P->product->category->name }}</td>
								<td class="gry-color">{{ $P->quantity }}</td>
							</tr>
		                @endif
					@endforeach
	            </tbody>
			</table>
		</div>

		<div style="background: #eceff4;padding: 1.5rem;">
			<table>
                <tr>
					<td class="gry-color small"><strong>Registered Address:</strong> {{ $generalsetting->site_name }} {{ $generalsetting->address }}</td>
				</tr>
				<tr>
					<td class="gry-color small">Email: {{ $generalsetting->email }}, Phone: {{ $generalsetting->phone }}</td>
				</tr>
			</table>

		</div>

	</div>
</body>
</html>