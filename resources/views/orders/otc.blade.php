@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<style>
    .ordcode{
        color:#282563;
    }
    .ordcode:hover{
        color:#EB3038;
    }
</style>
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Orders')}}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_orders" action="" method="GET">
                <!--<div class="box-inline pad-rgt pull-left">-->
                <!--    <div class="select" style="min-width: 300px;">-->
                <!--        <select class="form-control demo-select2" name="payment_type" id="payment_type" onchange="sort_orders()">-->
                <!--            <option value="">{{__('Filter by Payment Status')}}</option>-->
                <!--            <option value="paid"  @isset($payment_status) @if($payment_status == 'paid') selected @endif @endisset>{{__('Paid')}}</option>-->
                <!--            <option value="unpaid"  @isset($payment_status) @if($payment_status == 'unpaid') selected @endif @endisset>{{__('Un-Paid')}}</option>-->
                <!--        </select>-->
                <!--    </div>-->
                <!--</div>-->
                <!--<div class="box-inline pad-rgt pull-left">-->
                <!--    <div class="select" style="min-width: 300px;">-->
                <!--        <select class="form-control demo-select2" name="delivery_status" id="delivery_status" onchange="sort_orders()">-->
                <!--            <option value="">{{__('Filter by Deliver Status')}}</option>-->
                <!--            <option value="confirmation_pending"   @isset($delivery_status) @if($delivery_status == 'confirmation_pending') selected @endif @endisset>{{__('Confirmation Pending')}}</option>-->
                <!--            <option value="hold"   @isset($delivery_status) @if($delivery_status == 'hold') selected @endif @endisset>{{__('Hold')}}</option>-->
                <!--            <option value="pending"   @isset($delivery_status) @if($delivery_status == 'pending') selected @endif @endisset>{{__('Pending')}}</option>-->
                <!--            <option value="on_review"   @isset($delivery_status) @if($delivery_status == 'on_review') selected @endif @endisset>{{__('On review')}}</option>-->
                <!--            <option value="ready_to_ship"   @isset($delivery_status) @if($delivery_status == 'ready_to_ship') selected @endif @endisset>{{__('Ready to Ship')}}</option>-->
                <!--            <option value="ready_to_delivery"   @isset($delivery_status) @if($delivery_status == 'ready_to_delivery') selected @endif @endisset>{{__('Ready to Delivery')}}</option>-->
                <!--            <option value="shipped"   @isset($delivery_status) @if($delivery_status == 'shipped') selected @endif @endisset>{{__('Shipped')}}</option>-->
                <!--            <option value="on_delivery"   @isset($delivery_status) @if($delivery_status == 'on_delivery') selected @endif @endisset>{{__('On delivery')}}</option>-->
                <!--            <option value="delivered"   @isset($delivery_status) @if($delivery_status == 'delivered') selected @endif @endisset>{{__('Delivered')}}</option>-->
                <!--            <option value="processing_refund"   @isset($delivery_status) @if($delivery_status == 'processing_refund') selected @endif @endisset>{{__('Processing Refund')}}</option>-->
                <!--            <option value="refunded"   @isset($delivery_status) @if($delivery_status == 'refunded') selected @endif @endisset>{{__('Refunded')}}</option>-->
                <!--            <option value="cancel"   @isset($delivery_status) @if($delivery_status == 'cancel') selected @endif @endisset>{{__('Cancel')}}</option>-->
                <!--        </select>-->
                <!--    </div>-->
                <!--</div>-->
                <div class="box-inline pad-rgt pull-left">
                    <!--<div class="" style="min-width: 200px;display:inline-block;">-->
                    <!--    <input type="text" class="form-control" id="search" name="cust_search"@isset($cust_search) value="{{ $cust_search }}" @endisset placeholder="Cust Name">-->
                    <!--</div>-->
                    <a href="{{route('order.export')}}" class="btn btn-success">Export Orders</a>
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                    
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Order Code')}}</th>
                    <th>{{__('Order Date')}}</th>
                    <th>{{__('Num. of Products')}}</th>
                    <th>{{__('Customer')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Coupen Discount')}}</th>
                    <th>{{__('Order Status')}}</th>
                    <th>{{__('Pay Method')}}</th>
                    <th>{{__('Pay Status')}}</th>
                    <th>{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $key => $order_id)
                    @php
                        $order = \App\Order::find($order_id->id);
                    @endphp
                    @if($order != null)
                    @php
                    if ($order->orderDetails->where('seller_id',  $admin_user_id)->first()->payment_status == 'paid') {
                        $payment_status = "paid";
                    } else {
                        $payment_status = "unpaid";
                    }
                    @endphp
                        <tr>
                            <td>
                                {{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }} <input type="checkbox" data-ordercode="{{$order->code}}" data-payment="{{$payment_status}}" name="order_id[]" value="{{$order->id}}" id="check_order_id" class="check_order_id" />
                            </td>
                            <td>
                                <a href="{{ route('orders.show', encrypt($order->id)) }}" class="ordcode"> {{ $order->code }}</a> @if($order->viewed == 0) <span class="pull-right badge badge-info">{{ __('New') }}</span> @endif
                            </td>
                            <td>
                                {{ date('d-m-Y h:i A', $order->date) }}
                            </td>
                             @php
								$return_product_id=\App\ReturnProduct::where('order_id',$order_id->id)->pluck('product_id');
								//dd(\Route::currentRouteName());
							@endphp
                            <td class="text-center">
                                @if(count($return_product_id)>0 && (\Route::currentRouteName() == 'orders.index.refunded.admin' || \Route::currentRouteName() == 'orders.index.processing_refund.admin'))
								 Return Request of {{count($return_product_id)}} out of {{ count($order->orderDetails->where('seller_id', $admin_user_id)) }} 
							@else
                                {{ count($order->orderDetails->where('seller_id', $admin_user_id)) }}
							@endif
                            </td>
                            <td>
                                
                                @if(\Route::currentRouteName() == 'orders.index.ready_to_delivery.admin')
                                
                                  @php $shipping_address = json_decode($order->shipping_address,true); @endphp
                                    {{$shipping_address['name']}}
                                    
                                @else
                                @if ($order->user_id != null)
                                      
                                    {{ $order->user->name??' ' }}@if(customerBookingsCount($order->user_id) == 1) <span class="pull-right badge badge-info">{{ __('New Customer') }}</span> @endif
                                @else
                                    Guest ({{ $order->guest_id }})
                                @endif
                                @endif
                            </td>
                            <td>
                               
						
							{{ single_price($order->grand_total ) }}
						
                            </td>
                            <td>
                               
								{{ single_price($order->coupon_discount ) }}
						
                            </td>
                            <td>
                                @php
                                    $status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->delivery_status;
                                @endphp
                                @if($status == "Confirmation Pending")
                                <label class="label label-danger">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @elseif($status == "pending")
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @else
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @endif
                            </td>
                            <td>
                                {{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}
                            </td>
                            <td>
                                <span class="badge badge--2 mr-4">
                                    @if ($payment_status == 'paid')
                                        <i class="bg-green"></i> Paid
                                    @else
                                        <i class="bg-red"></i> Unpaid
                                    @endif
                                </span>
                            </td>
                            <td>
                                @if(count($order->orderDetails->where('seller_id', $admin_user_id)->where('delivery_status', 'confirmation_pending')) > 0)
                                        <a  href="javascript:confirm_order({{$order->id}}, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a>
                                        @if($order->prescription_file != null) 
                                            <a href="{{ asset('prescription/'.$order->prescription_file) }}" class="btn btn-primary">{{__('Download Prescription')}}</a>
                                        @endif
                                        @if($payment_status == "paid")
                                            <a href="javascript:confirm_order({{$order->id}}, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                      
                                           
                                        @endif
                                       
                                @else
                                    @if($status == "delivered")
                                     @if(count($return_product_id)>0 && (\Route::currentRouteName() == 'orders.index.refunded.admin' || \Route::currentRouteName() == 'orders.index.processing_refund.admin'))
                                         <a href="{{ route('returnorders.show', encrypt($order->id)) }}" class="btn btn-primary">{{__('View Return')}}</a>
                                     @endif
                                    @elseif($status == "pending")
                                        <a  href="javascript:confirm_order({{$order->id}}, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a>
                                        @if($order->prescription_file != null) 
                                            <a href="{{ asset('prescription/'.$order->prescription_file) }}" class="btn btn-primary">{{__('Download Prescription')}}</a>
                                        @endif
                                        <!--a onclick="confirm_modal('{{route('orders.destroy', $order->id)}}');" class="btn btn-primary">{{__('Delete')}}</a-->
                                        @if($payment_status == "paid")
                                            <a href="javascript:confirm_order({{$order->id}}, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                        
                                        @endif
                                    @elseif($status == "ready_to_ship") 
                                        <a href="javascript:order_shipping_info({{ $order->id }})"  class="btn btn-primary">{{__('Add Shipping info')}}</a>
                                        <a href="javascript:confirm_order({{$order->id}}, 'ready_to_delivery')" class="btn btn-primary">Ready to Delivery</a>
                                        <a href="{{ route('orders.print_label', $order->id) }}" target="_blank" class="btn btn-primary">{{__('Print Label')}}</a>
                                    @elseif($status == "shipped" or $status == "on_delivery") 
                                        @if($payment_status == 'unpaid')
                                            <a href="javascript:confirm_payment({{$order->id}}, 'paid')" class="btn btn-primary">Mark Paid</a>
                                            <span id="orderbtn_{{$order->id}}"><a href="javascript:void(0)" class="btn btn-primary" disabled>{{__('Mark Delivered')}}</a></span>
                                        @else
                                        <span id="orderbtn_{{$order->id}}"><a href="javascript:confirm_order({{$order->id}}, 'delivered')" class="btn btn-primary">{{__('Mark Delivered')}}</a></span>
                                        @endif
                                        
                                        @if($payment_status == "paid")
                                            <a href="javascript:confirm_order({{$order->id}}, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                       
                                        @endif
                                        
                                    @elseif($status == "processing_refund") 
                                        <a href="javascript:confirm_order({{$order->id}}, 'refunded')" class="btn btn-primary">Refunded to Wallet</a>
                                    @elseif($status == "ready_to_delivery")
                                        <a href="{{ route('orders.print_label', $order->id) }}" target="_blank" class="btn btn-primary">{{__('Print Label')}}</a>
                                        <button class="btn btn-primary" disabled>{{__('User below button to assign Agent')}}</button>
                                    @elseif($status == "on_review" or $status == "hold")
                                        <a href="javascript:confirm_order({{$order->id}}, 'pending')" class="btn btn-primary">Move to Pending</a>   
                                    @else
                                    <button class="btn btn-primary" disabled>{{__('No Action')}}</button>
                                    @endif
                                @endif
                                
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="col-md-12">
                <br />&nbsp;
                @if(\Route::currentRouteName() == 'orders.index.on_delivery.admin')
                <button class="btn btn-danger" onclick="mark_undelivered()">Unable to Delivered</button>
                @endif
                @if(\Route::currentRouteName() == 'orders.index.ready_to_delivery.admin')
                <button class="btn btn-primary" onclick="showmodal('assign_status_modal');">Assign Delivery Agent</button>
                @endif
                
                @if(\Route::currentRouteName() == 'orders.index.on_delivery.admin' || \Route::currentRouteName() == 'orders.index.shipped.admin' )
                <button class="btn btn-success" id="markdeliverybtn" onclick="showmodal('mark_delivery_modal')" disabled>Mark Selected to Delivered</button>
                <button class="btn btn-primary" id="markpaidbtn" onclick="showmodal('payment_status_modal');" disabled>Mark Selected to Paid</button>
                @endif
                
                <br />&nbsp;
                <br />&nbsp;
            </div>
        </div>
        <div class="clearfix">
            <div class="pull-right">
                {{ $orders->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>
<script>
    function showmodal(modalid) {
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
        productArray.push($(v).val());
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        $('#'+modalid).modal();
    }
function mark_undelivered() {
    productArray = [];
    productCodeArray = [];
    html = '<table class="table table-bordred table-hover">';
    html +='<thead>';
    html +='    <tr>';
    html +='    <th class="text-center">Order No.</th>';
    html +='    <th class="text-center">Order Code</th>';
    html +='    <th class="text-center">Undelivery Note</th>';
    html +='    </tr>';
    html +='</thead>';
    html +='<tbody>';
    $('#check_order_id:checked').each(function(i,v) {
        productArray.push($(v).val());
        productCodeArray.push($(v).data('ordercode')); 
        html +='    <tr>';
        html +='        <td class="text-center">'+ $(v).val() +'<input type="hidden" name="order_ids[]" value="'+ $(v).val() +'"  class="form-control"></td>';
        html +='        <td class="text-center">'+ $(v).data('ordercode') +'<input type="hidden" name="order_code[]" value="'+ $(v).data('ordercode') +'" class="form-control"></td>';
        html +='        <td><input type="text" name="undelivery_notes[]" class="form-control"></td>';
        html +='    </tr>';
    });
    if(productCodeArray.length == 0) {
        alert("Kindly select order.");
        return false;
    }
    html +='</tbody>';
    html +='</table>';
    $('#undelivery_div').html(html);
   // console.log(productCodeArray);
    $('#undelivery_status_modal').modal();
}
</script>

<div class="modal fade" id="undelivery_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="undelivery_Form">
                @csrf
                @if(isset($seller_id)) 
                    <input type="hidden" name="seller_id" value="{{$seller_id}}">
                @endif
                    <input type="hidden" name="action" value="undelivery">
                    <input type="hidden" name="setFlash" value="Order set for ready for delivery status.">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Update Info')}}</h4>
            </div>
            <div class="modal-body" id="undelivery_div">
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="delivery_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Delivery Status')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Status &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control demo-select2"  id="modal_delivery_status">
                            <option value="">{{__('Select Deliver Status')}}</option>
                            <option value="confirmation_pending">{{__('Confirmation Pending')}}</option>
                            <option value="hold">{{__('Hold')}}</option>
                            <option value="pending">{{__('Pending')}}</option>
                            <option value="on_review">{{__('On review')}}</option>
                            <option value="ready_to_ship">{{__('Ready to Ship')}}</option>
                            <option value="ready_to_delivery">{{__('Ready to Delivery')}}</option>
                            <option value="shipped">{{__('Shipped')}}</option>
                            <option value="on_delivery">{{__('On delivery')}}</option>
                            <option value="delivered" >{{__('Delivered')}}</option>
                            <option value="processing_refund">{{__('Processing Refund')}}</option>
                            <option value="refunded">{{__('Refunded')}}</option>
                            <option value="cancel">{{__('Cancel')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="change_status('delivery')"  data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="assign_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Assign to delivery')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Status &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control demo-select2"  id="modal_delivery_agent_id">
                            <option value="">{{__('Select Agent')}}</option>
                            @foreach($delivery_agent as $d)
                            <option value="{{$d->id}}">{{$d->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="assign_delivery()"  data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="payment_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Changing of Payment Status')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 text-right pt-2">
                       <center><strong>Do you want to continue ?</strong></center>
                    </div>
                        <input type="hidden" value="paid" id="modal_payment_status">
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="change_status('payment')"  data-dismiss="modal">Continue</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mark_delivery_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Changing of Delivery Status')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 text-right pt-2">
                       <center><strong>Do you want to continue ?</strong></center>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="mark_delivered()"  data-dismiss="modal">Continue</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="shipping_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Shipping Info')}}</h4>
            </div>
            <form action="{{route('orders.shipping')}}" method="post">
            @csrf
            <div class="modal-body">
            <input type="hidden" name="order_id" id="modal_order_id">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Courier &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name='courier' placeholder="Courier Company Name" >
                    </div>
                </div>
                <div class="form-group row" style="margin-top:5px;">
                    <div class="col-sm-3 text-right pt-2">
                        AWB/Ref/Tracking No. &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="awb" class="form-control" placeholder="AWB Number" >
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                  <button class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')

    <script type="text/javascript">
        
        $('#undelivery_Form').submit(function(e){
            e.preventDefault();
            formdata =  $('#undelivery_Form').serialize();
            $('#undelivery_status_modal').modal('hide');
            $.ajax({
                type: 'POST',
                url: '{{ route('orders.update_status') }}',
                data:formdata,
                processData: false,
                success: function(result){
                 location.reload();
                }
            });
        });
    @if(\Route::currentRouteName() == 'orders.index.on_delivery.admin' || \Route::currentRouteName() == 'orders.index.shipped.admin' )
    
    function checkdeliverypayButton() {
        productunpaidArray = [];
        productpaidArray = [];
        $('#check_order_id:checked').each(function(i,v) {
            if($(v).data('payment') == 'unpaid') {
                productunpaidArray.push($(v).val());
            } else {
                productpaidArray.push($(v).val());
            }
        });
        console.log(productunpaidArray.length);
        console.log(productpaidArray.length);
        
        if(productunpaidArray.length == 0) {
            $('#markdeliverybtn').prop('disabled', false);
        } else {
            $('#markdeliverybtn').prop('disabled', 'disabled');
        }
        
        if(productpaidArray.length == 0) {
            $('#markpaidbtn').prop('disabled', false);
        } else {
            $('#markpaidbtn').prop('disabled', 'disabled');
        }
        if(productpaidArray.length == 0 && productunpaidArray.length == 0 ) {
            $('#markpaidbtn').prop('disabled', 'disabled');
            $('#markdeliverybtn').prop('disabled', 'disabled');
        }
    }
    
    $('.check_order_id').click(function() {
        checkdeliverypayButton();
    });
    
    @endif
    
    function mark_delivered() {
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "delivered";
        var setFlash = 'Order set for delivered status.';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
             location.reload();
        });
    }
        
    function assign_delivery() {
        agent_id = $('#modal_delivery_agent_id').val();
        $('#modal_delivery_agent_id').val("");
        $('#modal_delivery_agent_id').trigger('change');
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "assign";
        var setFlash = 'Order set for out for delivery status.';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, agent_id:agent_id, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
             location.reload();
        });
    }
        
    function change_status(action) {
        if(action == 'delivery') {
            status = $('#modal_delivery_status').val();
            $('#modal_delivery_status').val("");
            $('#modal_delivery_status').trigger('change');
        }
        if(action == 'payment') {
            status = $('#modal_payment_status').val();
            $('#modal_payment_status').val("");
            $('#modal_payment_status').trigger('change');
        }
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        }); 
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        
        var setFlash = 'Order '+action+' status has been updated';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
             location.reload();
        });
    }
        
    function order_shipping_info(order_id)
    {
        $('#modal_order_id').val(order_id);
        $('#shipping_info').modal();
    }
        function sort_orders(el){
            $('#sort_orders').submit();
        }
    
        function confirm_payment(order_id, status) {
            var setFlash = 'Payment status has been updated';
            $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                location.reload();
            });
        }

        function confirm_order(order_id, status) {
            var setFlash = 'Order status has been updated';
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                 location.reload();
            });
        }

        function trash_order(order_id, status) {
            var setFlash = 'Order status has been updated';
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                 location.reload();
            });
        }
    </script>
@endsection