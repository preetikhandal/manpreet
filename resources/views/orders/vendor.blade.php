@extends('layouts.app')

@section('content')
@php
    $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
@endphp
<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Orders')}}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_orders" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 300px;">
                        <select class="form-control demo-select2" name="payment_type" id="payment_type" onchange="sort_orders()">
                            <option value="">{{__('Filter by Payment Status')}}</option>
                            <option value="paid"  @isset($payment_status) @if($payment_status == 'paid') selected @endif @endisset>{{__('Paid')}}</option>
                            <option value="unpaid"  @isset($payment_status) @if($payment_status == 'unpaid') selected @endif @endisset>{{__('Un-Paid')}}</option>
                        </select>
                    </div>
                </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 300px;">
                        <select class="form-control demo-select2" name="delivery_status" id="delivery_status" onchange="sort_orders()">
                            <option value="">{{__('Filter by Deliver Status')}}</option>
                            <option value="pending"   @isset($delivery_status) @if($delivery_status == 'pending') selected @endif @endisset>{{__('Pending')}}</option>
                            <option value="on_review"   @isset($delivery_status) @if($delivery_status == 'on_review') selected @endif @endisset>{{__('On review')}}</option>
                            <option value="shipped"   @isset($delivery_status) @if($delivery_status == 'shipped') selected @endif @endisset>{{__('Shipped')}}</option>
                            <option value="on_delivery"   @isset($delivery_status) @if($delivery_status == 'on_delivery') selected @endif @endisset>{{__('On delivery')}}</option>
                            <option value="delivered"   @isset($delivery_status) @if($delivery_status == 'delivered') selected @endif @endisset>{{__('Delivered')}}</option>
                            <option value="cancel"   @isset($delivery_status) @if($delivery_status == 'cancel') selected @endif @endisset>{{__('Cancel')}}</option>
                        </select>
                    </div>
                </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Order Code')}}</th>
                    <th>{{__('Num. of Products')}}</th>
                    <th>{{__('Customer')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Delivery Status')}}</th>
                    <th>{{__('Payment Method')}}</th>
                    <th>{{__('Payment Status')}}</th>
                    @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
                        <th>{{__('Refund')}}</th>
                    @endif
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $key => $order_id)
                @php $order_id =  $order_id->id; @endphp
                    @if(isset($OrderData[$order_id]))
                        <tr>
                            <td>
                                {{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }}
                            </td>
                            <td>
                                {{ $OrderData[$order_id]['order']->code }} @if($OrderData[$order_id]['order']->viewed == 0) <span class="pull-right badge badge-info">{{ __('New') }}</span> @endif
                            </td>
                            <td>
                                {{ $OrderData[$order_id]['items'] }}
                            </td>
                            <td>
                                @if ($OrderData[$order_id]['order']->user_id != null)
                                    {{ $OrderData[$order_id]['order']->user->name }}
                                @else
                                    Guest ({{ $OrderData[$order_id]['order']->guest_id }})
                                @endif
                            </td>
                            <td>
                                {{ $OrderData[$order_id]['price'] }}
                            </td>
                            <td>
                                @php
                                    $status = $OrderData[$order_id]['delivery_status'];
                                @endphp
                                {{ ucfirst(str_replace('_', ' ', $status)) }}
                            </td>
                            <td>
                                {{ ucfirst(str_replace('_', ' ', $OrderData[$order_id]['order']->payment_type)) }}
                            </td>
                            <td>
                                <span class="badge badge--2 mr-4">
                                    @if ($OrderData[$order_id]['payment_status'] == 'paid')
                                        <i class="bg-green"></i> Paid
                                    @else
                                        <i class="bg-red"></i> Unpaid
                                    @endif
                                </span>
                            </td>
                            @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
                                <td>
                                    @if (count($OrderData[$order_id]['order']->refund_requests) > 0)
                                        {{ count($OrderData[$order_id]['order']->refund_requests) }} Refund
                                    @else
                                        No Refund
                                    @endif
                                </td>
                            @endif
                            <td>
                                <div class="btn-group dropdown">
                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                        {{__('Actions')}} <i class="dropdown-caret"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{ route('orders.vendor.show', [ $OrderData[$order_id]['seller_id'], encrypt($order_id)]) }}">{{__('View')}}</a></li>
                                        <li><a href="{{ route('orders.print_label', $order_id) }}" target="_blank">{{__('Print Label')}}</a></li>
                                        @if($OrderData[$order_id]['order']->prescription_file != null) <li><a href="{{ asset('prescription/'.$OrderData[$order_id]['order']->prescription_file) }}" target="_blank">{{__('Download Prescription')}}</a></li> @endif
                                        <li><a href="{{ route('admin.invoice.download', $order_id) }}">{{__('Download Invoice')}}</a></li>
                                        <li><a onclick="confirm_modal('{{route('orders.destroy', $order_id)}}');">{{__('Delete')}}</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $orders->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="shipping_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Shipping Info')}}</h4>
            </div>
            <form action="{{route('orders.shipping')}}" method="post">
            @csrf
            <div class="modal-body">
            <input type="hidden" name="order_id" id="modal_order_id">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Courier &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name='courier' placeholder="Courier Company Name" >
                    </div>
                </div>
                <div class="form-group row" style="margin-top:5px;">
                    <div class="col-sm-3 text-right pt-2">
                        AWB/Ref/Tracking No. &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="awb" class="form-control" placeholder="AWB Number" >
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                  <button class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection


@section('script')

    <script type="text/javascript">
        
    function order_shipping_info(order_id)
    {
        $('#modal_order_id').val(order_id);
        $('#shipping_info').modal();
    }
        function sort_orders(el){
            $('#sort_orders').submit();
        }
    </script>
@endsection
