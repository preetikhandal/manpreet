@extends('layouts.app')
176217

@section('content')
<div class="row">
  
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title text-center">{{__('USER CASHBACK CONFIGURATION')}}</h3>
            </div>

            @if(count($cashBackConfig) >= 1)
           
            <div class="panel-body">
                <form class="form-horizontal" action="{{ route('update-cashback-config') }}" method="POST">
                    @csrf
                    <input type="hidden" value="{{$cashBackConfig[0]->id}}" name="updateid">
                    <div id="smtp">
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">{{__('Category')}}</label>
                            <div class="col-sm-6">
                                <select name="category_id" required class="form-control demo-select2-placeholder">
                                    @foreach($categories as $category)
                                <option value="{{$category->id}}" <?php if($cashBackConfig[0]->category_id == $category->id) echo "selected";?> >{{__($category->name)}}</option>
                            @endforeach
                                </select>
                            </div>
                        </div>
                
                        <div class="form-group">
                            <div class="col-lg-3">
                                <label class="control-label">{{__('CASH BACK AMOUNT PERCENTAGE')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="CASHBACK_AMOUNT" value="{{$cashBackConfig[0]->casback_amount}}" placeholder="ENTER CASHBACK AMOUNT" require>
                            </div>%
                        </div>
                        
                        <div class="form-group">
                        
                            <div class="col-lg-3">
                                <label class="control-label">{{__('MINIMUM SHOPPING AMOUNT')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="MINIMUM_SHOPPING_AMOUNT" value="{{$cashBackConfig[0]->casback_minimum_amount_limit}}" placeholder="ENTER MINIMUM SHOPPING AMOUNT" require>
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label">{{__('SHOPPING USER PERCENTAGE(Next Shopping)')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="SHOPPING_percentage" value="{{$cashBackConfig[0]->casback_use_percentage}}" placeholder="Shopping Order percentage" require>
                            </div>%
                        </div>

                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label">{{__('NEXT MINIMUM SHOPPING AMOUNT')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="CASHBACK_CREDIT_AMT" value="{{$cashBackConfig[0]->cashback_credit_amount}}" placeholder="ENTER CASHBACK CREDIT AMOUNT" require>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label">{{__('USER PRODUCT CANCELLATION CHARGE')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="user_cancel_charge" value="{{$cashBackConfig[0]->user_cancel_charge}}" placeholder="ENTER USER PRODUCT CANCELLATION CHARGE">
                            </div>%
                        </div>
                       
                       
                    </div>
                   
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-purple" type="submit">{{__('update')}}</button>
                        </div>
                    </div>
                </form>
            </div>
                   @else
                   <div class="panel-body">
                <form class="form-horizontal" action="{{ route('add-cashback-config') }}" method="POST">
                    @csrf
                    <div id="smtp">
                        <div class="form-group">
                           
                            <div class="col-lg-3">
                                <label class="control-label">{{__('CASH BACK AMOUNT')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="CASHBACK_AMOUNT" value="" placeholder="ENTER CASHBACK AMOUNT" require>
                            </div>
                        </div>
                        <div class="form-group">
                        
                            <div class="col-lg-3">
                                <label class="control-label">{{__('MINIMUM SHOPPING AMOUNT')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="MINIMUM_SHOPPING_AMOUNT" value="" placeholder="ENTER MINIMUM SHOPPING AMOUNT" require>
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label">{{__('SHOPPING USER PERCENTAGE(per order)')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="SHOPPING_percentage" value="" placeholder="Shopping Order percentage" require>
                            </div>%
                        </div>

                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label">{{__('CASHBACK CREDIT AMOUNT')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="CASHBACK_CREDIT_AMT" value="" placeholder="ENTER CASHBACK CREDIT AMOUNT" require>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label">{{__('OFFER DAYS')}}</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="OFFER_DAYS" value="" placeholder="MAIL ENCRYPTION">
                            </div>
                        </div>
                       
                       
                    </div>
                   
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
                        </div>
                    </div>
                </form>
            </div>
            @endif

            

        </div>
    
    
</div>

@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function(){
            checkMailDriver();
        });
        function checkMailDriver(){
            if($('select[name=MAIL_DRIVER]').val() == 'mailgun'){
                $('#mailgun').show();
                $('#smtp').hide();
            }
            else{
                $('#mailgun').hide();
                $('#smtp').show();
            }
        }
    </script>

@endsection
