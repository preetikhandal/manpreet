@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col">
    <div class="panel">
        <div class="panel-title">
            <center><h3>{{__('Add City')}}</h3></center>
        </div>
        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('city.store') }}" method="POST" enctype="multipart/form-data" onsubmit="return validate()">
        	@csrf
            <div class="panel-body">
                
                <div class="form-group">
                    <div class="col-sm-12">
                        {{__('Select State')}} <br />
                        <select class="form-control" name="stateid">
                            <option >Please select</option>
                            @foreach($state as $stateList)
                               <option value="{{$stateList->id}}">{{$stateList->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
               
                
                <div class="form-group">
                    <div class="col-sm-12">
                        {{__('City Name')}} <br />
                        <input type="text" placeholder="{{__('City Name')}}" id="BlogTitle" name="city" class="form-control" required value="{{ old('BlogTitle') }}">
                    </div>
                </div>
                
                
                
                <div class="form-group">
                    <div class="col-sm-12">
                        {{__('City code')}} <br />
                        <input type="text" placeholder="{{__('City code')}}" id="BlogTitle" name="code" class="form-control" required value="{{ old('BlogTitle') }}">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection

@section('script')
<style>
    .error-input {
        border: 1px solid red;    
    }
    .success-input {
        border: 1px solid green;
    }
</style>
<!-- Bootstrap WYSIHTML5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="{{asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}" />
<script src="//cdn.ckeditor.com/4.10.1/full/ckeditor.js"></script>
<script>
 /* $(function () {
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5(
	{
		"color": true,
	});
  });
  */
   CKEDITOR.replace( 'textarea', {
        filebrowserUploadUrl: "{{route('EditorUploadImg', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
   $(function () {
      $('#BlogPublishedOn').datetimepicker({
		format: 'YYYY-MM-DD HH:mm:ss'
    });
     });
</script>

@endsection
