@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <a href="{{ route('city.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New City')}}</a>
        </div>
    </div>

    <br>

    <!-- Basic Data Tables -->
    <!--===================================================-->
   <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all h-100">
            <h3 class="panel-title pull-left pad-no">{{__('City')}}</h3>
            <div class="pull-right clearfix">
                <form class="" id="sort_sellers" action="" method="GET">
                    <div class="box-inline pad-rgt pull-left">
                        <div class="" style="min-width: 200px;">
                            <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type City Name">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Alignbook State Parent Id')}}</th>
                    <th>{{__('State Name')}}</th>
                    <th>{{__('City Name')}}</th>
                    <th>{{__('Alignbook City Id')}}</th>
                    <th>{{__('Code')}}</th>
                    
                    
                </tr>
                </thead>
                <tbody>
                    @foreach($citys as $key => $citysList)
                          @php
                          $statteName = App\State::find($citysList->state_id);
                          @endphp
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$citysList->parent_id}}</td>
                            <td>{{$statteName->name}}</td>
                            <td>{{$citysList->name}}</td>
                            <td>{{$citysList->city_id}}</td>
                            <td>{{$citysList->code}}</td>
                        </td>
                    @endforeach
                </tbody>
                
            </table>
            
        </div>
    </div>
        
@endsection

@section('script')
  
@endsection
