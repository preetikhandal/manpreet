@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col">
    <div class="panel">
        <div class="panel-title">
            <center><h3>{{__('Edit Blog Post')}}</h3></center>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('blog.update', $Blog->id) }}" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        {{__('Title')}} <br />
                        <input type="text" placeholder="{{__('Blog Title')}}" id="BlogTitle" name="BlogTitle" class="form-control" required value="{{ $Blog->BlogTitle }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        {{__('Summary')}} <br />
                        <textarea type="text" placeholder="{{__('Blog Summary')}}" id="BlogSummary" name="BlogSummary" class="form-control" required>{{ $Blog->BlogSummary }}</textarea>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-12">
                        {{__('Description')}} <br />
                        <textarea type="text" placeholder="{{__('Your Blog Goes here...')}}"  id="textarea" name="BlogContent" class="form-control" style="height:600px;" required>{{ $Blog->BlogContent }}</textarea>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-12">
                        {{__('Slug')}} <br />
                        <input type="text" placeholder="{{__('URL Slug')}}" id="URLSlug" name="URLSlug" class="form-control" value="{{ $Blog->URLSlug }}">
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-12">
                        {{__('Featured Image')}} <em class="text-danger">Exiting image will be overwritten</em> <br />
                        <input type="file" placeholder="{{__('Featured Image')}}" id="FeaturedImage" name="FeaturedImage" class="form-control" value="{{ old('FeaturedImage') }}">
						
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-12">
                        {{__('Meta Description')}} <br />
                        <input type="text" placeholder="{{__('Meta Description')}}" id="SEO_metaDescription" name="SEO_metaDescription" class="form-control" value="{{ $Blog->SEO_metaDescription }}">
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-12">
                        {{__('Meta Tags')}} <br />
                        <input type="text" placeholder="{{__('Meta Tags')}}" id="SEO_metatags" name="SEO_metatags" class="form-control" value="{{ $Blog->SEO_metatags }}">
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-12">
                        {{__('Featured Image')}} <br />
                        <img src="{{asset($Blog->FeaturedImage)}}" class="img-responsive" style="width:200px">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Update')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
@section('script')

<!-- Bootstrap WYSIHTML5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="{{asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}" />
<script src="//cdn.ckeditor.com/4.10.1/full/ckeditor.js"></script>
<script>
 /* $(function () {
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5(
	{
		"color": true,
	});
  });
  */
   CKEDITOR.replace( 'textarea', {
        filebrowserUploadUrl: "{{route('EditorUploadImg', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
   $(function () {
      $('#BlogPublishedOn').datetimepicker({
		format: 'YYYY-MM-DD HH:mm:ss'
    });
     });
</script>
@endsection
