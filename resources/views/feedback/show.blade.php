@extends('layouts.app')

@section('content')

    <div class="row">
       
    </div>

    <br>

    <!-- Basic Data Tables -->
    <!--===================================================-->
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all h-100">
            <h3 class="panel-title pull-left pad-no">{{__('feedbacks')}}</h3>
            <div class="pull-right clearfix">
            <form class="" id="sort_orders" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <!--<div class="" style="min-width: 200px;display:inline-block;">-->
                    <!--    <input type="text" class="form-control" id="search" name="cust_search"@isset($cust_search) value="{{ $cust_search }}" @endisset placeholder="Cust Name">-->
                    <!--</div>-->
                   
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                    
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                </div>
            </form>
                
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Email')}}</th>
                    <th>{{__('Mobile No')}}</th>
                    <th>{{__('Q1')}}</th>
                    <th >{{__('Q2')}}</th>
                    <th >{{__('Q3')}}</th>
                    <th >{{__('Q4')}}</th>
                    <th>{{__('Q5')}}</th>
                    <th >{{__('Q6')}}</th>
                    <th >{{__('Q7')}}</th>
                    <th >{{__('Q8')}}</th>
                    <th >{{__('Q9')}}</th>
                    <th>{{__('Q10')}}</th>
                    <th >{{__('Q11')}}</th>
                   
                </tr>
                </thead>
                <tbody>
                @foreach($feedbacks as $key => $feedback)
                        <tr>
                            <td>{{ ($key+1) + ($feedbacks->currentPage() - 1)*$feedbacks->perPage() }}</td>
                            <td>{{$feedback->name}}</td>
                            <td width="40%">{{$feedback->email}}</td>
                            <td width="40%">{{$feedback->mobile}}</td>
                            <td width="40%">{{$feedback->Q1}}</td>
                            <td width="40%">{{$feedback->Q2}}</td>
                            <td width="40%">{{$feedback->Q3}}</td>
                            <td width="40%">{{$feedback->Q4}}</td>
                            <td width="40%">{{$feedback->Q5}}</td>
                            <td width="40%">{{$feedback->Q6}}</td>
                            <td width="40%">{{$feedback->Q7}}</td>
                            <td width="40%">{{$feedback->Q8}}</td>
                            <td width="40%">{{$feedback->Q9}}</td>
                            <td width="40%">{{$feedback->Q10}}</td>
                            <td width="40%">{{$feedback->Q12}}</td>           
                        </tr>
                @endforeach
                </tbody>
            </table>
            <div class="clearfix">
                <div class="pull-right">
                    {{ $feedbacks->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
  
@endsection
