@extends('frontend.layouts.app') @section('content')
<div class="pharmacy clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-12 offset-lg-2 offset-md-2 items">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <h2 class="text-center">Feedback Form</h2>
                <form action="{{route('savefeedback.form')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4 col-12">
                            <div class="form-group">
                                <label for="email">Name:</label>
                                <input type="text" class="form-control" value="{{old('name')}}" name="name" required maxlength="10">
                            </div>
                        </div>
                        <div class="col-lg-4 col-12">
                            <div class="form-group">
                                <label for="">Phone:</label>
                                <input type="number" class="form-control" value="{{old('phone')}}" name="phone" required>
                            </div>
                        </div>
                        <div class="col-lg-4 col-12">
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" value="{{old('email')}}" name="email" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <p>1.Considering your complete experience with our company, how likely would you be to recommend us to a friend or colleague ?</p>
                            <div class="radioBtn">
                            <label for="one">
                                <input type="radio" name="q1" id="one" value="Very unlikely"> Very unlikely
                            </label> 
                            <label for="two"><input type="radio" name="q1"  id="two"  value="Likely"> Likely</label>
                            <label for="three"><input type="radio" name="q1"  id="three" value="1"> 1</label>
                            <label for="four"><input type="radio" name="q1" id="four" value="2"> 2</label>
                            <label for="five"><input type="radio" name="q1" id="five" value="3"> 3</label>
                            <label for="six"><input type="radio" name="q1" id="six" value="4"> 4</label>
                            <label for="seven"><input type="radio" name="q1" id="seven" value="5"> 5</label>
                            <label for="eight"> <input type="radio" name="q1" id="eight" value="6"> 6</label>
                            <label for="nine"><input type="radio" name="q1" id="nine" value="7"> 7</label>
                            <label for="ten"><input type="radio" name="q1"  id="ten"value="8"> 8</label>
                            <label for="eleven"> <input type="radio" name="q1" id="eleven" value="9"> 9</label>
                            </div>
                           
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <p>2. How satisfied do you feel based on your overall experience ?</p>
                            <textarea class="form-control answer" id="exampleFormControlTextarea1" rows="3" name="q2" required maxlength="1000">{{old('q2')}}</textarea>
                        </div>
                        <div class="col-lg-12 col-12">
                            <p>3. Please let us know how can we improve your experience ?</p>
                            <textarea class="form-control answer" id="exampleFormControlTextarea1" rows="3" name="q3" required maxlength="1000">{{old('q3')}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <p>4. How did you learn about our website?</p>
                            <div class="radioBtn">
                                <label for="learn1"><input type="radio" name="q4" id="learn1" value="News Paper"> News Paper</label>
                                <label for="learn2"><input type="radio" name="q4"  id="learn2" value="TV ads"> TV ads</label>
                                <label for="learn3"><input type="radio" name="q4" id="learn3" value="Social Media"> Social Media</label>
                                <label for="learn4"><input type="radio" name="q4" id="learn4" value="Hoardings / Unipoles"> Hoardings / Unipoles</label>
                                <label for="learn5"><input type="radio" name="q4" id="learn5" value="E-mails"> E-mails</label>
                                <label for="learn6"><input type="radio" name="q4" id="learn6" value="Recommendation from a friend or relative"> Recommendation from a friend or relative</label><br>
                                <label>Other (Please Specify)</label>
                                <input type="text" name="other_q4" class="form-control" placeholder="please enter">
                               
                            </div>
                           
                        </div>
                        <div class="col-lg-12 col-12">
                            <p>5. What other products would you like to see in our online store (City Haat)?</p>
                            <input type="text" class="form-control answer" name="q5" required value="{{old('q5')}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <p>6. How satisfied are you with the quality of products ?</p>
                            <div class="radioBtn">
                                <label for="product1"><input type="radio" name="q6" id="product1" value="Very Dissatisfied"> Very Dissatisfied</label>
                                <label for="product2"> <input type="radio" name="q6"  id="product2" value="Not Satisfied"> Not Satisfied</label>
                                <label for="product3"><input type="radio" name="q6" id="product3" value="Neutral"> Neutral</label>
                                <label for="product4"><input type="radio" name="q6"  id="product4"value="Satisfied"> Satisfied</label>
                                <label for="product5"><input type="radio" name="q6" id="product5" value="Very Staisfied"> Very Staisfied</label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-12">
                            <p>7. How was the checkout experience overall?</p>
                            <textarea class="form-control answer" id="exampleFormControlTextarea1" rows="3" name="q7" maxlength="1000">{{old('q7')}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <p>8. Did you experience a hassle-free payment experience?</p>
                            <textarea class="form-control answer" id="exampleFormControlTextarea1" rows="3" name="q8" required maxlength="1000">{{old('q8')}}</textarea>
                        </div>
                        <div class="col-lg-12 col-12">
                            <p>9. On a scale of 0-10, how likely are you to buy from us again?</p>
                            <div class="radioBtn">
                                <label for="again1"><input type="radio" name="q9" id="again1" value="Very unlikely"> Very unlikely</label>
                                <label for="again2"><input type="radio" name="q9"   id="again2" value="1"> 1</label>
                                <label for="again3"><input type="radio" name="q9"  id="again3" value="2"> 2</label>
                                <label for="again4"><input type="radio" name="q9"   id="again4" value="3"> 3</label>
                                <label for="again5"><input type="radio" name="q9"  id="again5" value="4"> 4</label>
                                <label for="again6"><input type="radio" name="q9"   id="again6" value="5"> 5</label>
                                <label for="again7"> <input type="radio" name="q9"  id="again7" value="6"> 6</label>
                                <label for="again8"> <input type="radio" name="q9"   id="again8" value="7"> 7</label>
                                <label for="again11"><input type="radio" name="q9"  id="again11" value="8"> 8</label>
                                <label for="again9"><input type="radio" name="q9" id="again9" value="9"> 9</label>
                                <label for="again10"><input type="radio" name="q9"  id="again10" value="Likely"> Likely</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <p>10. Did you receive your product within the expected timeline?</p>
                            <input type="text" class="form-control answer" name="q10" required maxlength="200" value="{{old('q10')}}">
                        </div>
                        <div class="col-lg-12 col-12">
                            <p>11. How helpful was the customer support staff?</p>
                            <div class="radioBtn">
                                <label for="staff1"><input type="radio" name="q12" value="Not at all helpful" id="staff1" name="q12"> Not at all helpful</label>
                                 <label for="staff2"><input type="radio" name="q12" value="Slighty helpful" id="staff2" name="q12"> Slighty helpful</label>
                                 <label for="staff3"><input type="radio" name="q12" value="Moderately helpful" id="staff3" name="q12"> Moderately helpful</label>
                                  <label for="staff4"> <input type="radio" name="q12" value="Very helpful" id="staff4" name="q12"> Very helpful</label>
                                  <label for="staff5"><input type="radio" name="q12" value="Extremely helpful" id="staff5" name="q12"> Extremely helpful </label>
                            </div>
                           
                        </div>
                    </div>
                    <div class="controlBtn text-center mt-3">
                        <input style="padding: 5px 10px;
    color: white;
    background: black;
    border: 1px solid black;" type="submit" value="submit">
    <a href="#" class="float-right" style="padding: 6px 15px; background: #6c6caf; color: #fff; border-radius: 5px;">Share  <i class="fa fa-share"></i></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
@endsection