<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Home Categories')}}</h3>
    </div>
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
    <!--Horizontal Form-->
    <!--===================================================-->
    <form class="form-horizontal" action="{{ route('home_categories.update', $homeCategory->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="PATCH">
        <div class="panel-body">
            
            <div class="form-group mb-3">
				<label class="col-sm-2 control-label" for="products">{{__('Config Category Display Title')}}</label>
				<div class="col-sm-7">
				   <input type="text" class="form-control" name="display_title"  value="{{$homeCategory->display_title}}">
				</div>
			</div>
			
            <div class="form-group" id="category">
                <label class="col-lg-2 control-label">{{__('Category')}}</label>
                <div class="col-lg-7">
                    <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
                        @foreach(\App\Category::all() as $category)
                            <option value="{{$category->id}}" @php if($homeCategory->category_id == $category->id) echo "selected"; @endphp>{{__($category->name)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			<div class="form-group mb-3">
				<label class="col-sm-2 control-label" for="products">{{__('Products')}}</label>
				<div class="col-sm-7">
					<select name="products[]" id="products" class="form-control select2" data-placeholder="Choose Products" multiple required>
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-2 control-label" for="position">{{__('Position')}}</label>
				@php
					$dbposition=\App\HomeCategory::get();
					$allposiotns=array();
					foreach($dbposition as $value){
							array_push($allposiotns,$value->position);
					}						
				@endphp
				<div class="col-lg-7">
					<select name="position" class="form-control demo-select2-placeholder">
						<option value="{{$homeCategory->position}}" selected>{{$homeCategory->position}}</option>
						@for($n=1;$n<=40;$n++)
							@if(!in_array($n,$allposiotns))
								<option value="{{$n}}">{{$n}}</option>
							@endif
						@endfor
					</select>
				</div>
			</div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
        </div>
    </form>
    <!--===================================================-->
    <!--End Horizontal Form-->

</div>

<script type="text/javascript">

    $(document).ready(function(){
        $('#products').select2();
        get_products_by_category();

        $('#category_id').on('change', function() {
            get_products_by_category();
        });
        
        function get_products_by_category(){
            var category_id = $('#category_id').val();
            $.post('{{ route('products.get_products_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                $('#products').html(null);
                 $('#products').append($('<option>', {
                        value: "",
                        text: 'Select Products'
                    }));
                for (var i = 0; i < data.length; i++) {
                    $('#products').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name+" ( #"+data[i].product_id+" )"
                    }));
                }
                $("#products > option").each(function() {
                    @foreach (json_decode($homeCategory->products) as $key => $id)
                        if(this.value == '{{$id}}') {
                            $(this).prop('selected', true);
                        }
                    @endforeach
                    });
                $('#products').trigger("change");
            });
        }


    });
</script>
