@extends('layouts.app')

@section('content')

<br>

<div class="panel">
    <!--Panel heading-->
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{ __('Unlisted Products :: Product not in Alde Bazaar but in Marg') }}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_products" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="panel-body">
        <form action="{{route('unlistedProduct.Map')}}" method="post">
            @csrf
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Product Code')}}</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('MRP')}}</th>
                    <th>{{__('Rate')}}</th>
                    <th>{{__('Stock')}}</th>
                    <th>{{__('Product to Map')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($MargUnlistedProduct as $key => $product)
                    <tr>
                        <td>{{ ($key+1) + ($MargUnlistedProduct->currentPage() - 1)*$MargUnlistedProduct->perPage() }}</td>
                        <td>{{ $product->product_code }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->mrp }}</td>
                        <td>{{ $product->rate }}</td>
                        <td>{{ $product->stock }}</td>
                        <td><input type="hidden" name="MargUnlistedProductID[]" value="{{$product->id}}"><select name="ProductID[]" class="form-control select2"><option value="0">Select Product</option>
                            {!! $ProductsOption !!}</select></td>
                    </tr>
                @endforeach
            </tbody>
            <tfooter>
                <tr>
                    <td colspan="7"><button class="btn btn-success">Assign All</button></td>
                </tr>
            </tfooter>
        </table>
        </form>
        <div class="clearfix">
            <div class="pull-right">
                {{ $MargUnlistedProduct->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
    <script type="text/javascript">

        $(document).ready(function(){
            $('.select2').select2();
        });

    </script>
@endsection
