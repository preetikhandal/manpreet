@extends('layouts.app')

@section('content')

<br>

<div class="panel">
    <!--Panel heading-->
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{ __('Unlisted Customer :: Customer not in Alde Bazaar but in Marg') }}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_products" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="panel-body">
        <form action="{{route('unlistedCustomer.Map')}}" method="post">
            @csrf
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}} (List From Marg)</th>
                    <th>{{__('Customer to Map')}} (List From AldeBazaar)</th>
                </tr>
            </thead>
            <tbody>
                @foreach($MargUnlistedCustomer as $key => $C)
                    <tr>
                        <td>{{ ($key+1) + ($MargUnlistedCustomer->currentPage() - 1)*$MargUnlistedCustomer->perPage() }}</td>
                        <td>{{ $C->name }}</td>
                        <td><input type="hidden" name="MargUnlistedCustomerID[]" value="{{$C->id}}"><select name="UserID[]" class="form-control select2"><option value="0">Select Customer</option>
                            {!! $CustomerOption !!}</select></td>
                    </tr>
                @endforeach
            </tbody>
            <tfooter>
                <tr>
                    <td colspan="7"><button class="btn btn-success">Assign All</button></td>
                </tr>
            </tfooter>
        </table>
        </form>
        <div class="clearfix">
            <div class="pull-right">
                {{ $MargUnlistedCustomer->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
    <script type="text/javascript">

        $(document).ready(function(){
            $('.select2').select2();
        });

    </script>
@endsection
