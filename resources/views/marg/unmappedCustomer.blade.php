@extends('layouts.app')

@section('content')
<br>
<div class="panel">
    <!--Panel heading-->
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{ __('Unmapped Customer :: Customer in Alde Bazaar but not mapped with in Marg') }}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_products" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <form action="{{route('unmappedCustomer.Map')}}" method="post">
            @csrf
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}} (List From AldeBazaar)</th>
                    <th>{{__('Customer to Map')}} (List From Marg)</th>
                </tr>
            </thead>
            <tbody>
                @foreach($User as $key => $U)
                    <tr>
                        <td>{{ ($key+1) + ($User->currentPage() - 1)*$User->perPage() }}</td>
                        <td>{{ $U->name }}</td>
                        <td><input type="hidden" name="UserID[]" value="{{$U->id}}"><select name="MargUnlistedCustomerID[]" class="form-control select2"><option value="0">Select Customer</option>
                            {!! $MargUnlistedCustomerOption !!}</select></td>
                    </tr>
                @endforeach
            </tbody>
            <tfooter>
                <tr>
                    <td colspan="7"><button class="btn btn-success">Assign All</button></td>
                </tr>
            </tfooter>
        </table>
        </form>
        <div class="clearfix">
            <div class="pull-right">
                {{ $User->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
    <script type="text/javascript">

        $(document).ready(function(){
            $('.select2').select2();
        });
    </script>
@endsection
