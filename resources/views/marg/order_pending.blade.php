@extends('layouts.app')

@section('content')
<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Marg Pending Orders :: Order Not send to Marg')}}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_orders" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Order Code')}}</th>
                    <th>{{__('Marg Order ID')}}</th>
                    <th>{{__('Customer')}}</th>
                    <th>{{__('Product(s) not in Marg')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Delivery Status')}}</th>
                    <th>{{__('Payment Method')}}</th>
                    <!--<th width="10%">{{__('Options')}}</th>-->
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $key => $order_id)
                    @php
                        $order = \App\Order::find($order_id->id);
                    @endphp
                    @if($order != null)
                        <tr>
                            <td>
                                {{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }} <input type="checkbox" data-ordercode="{{$order->code}}" name="order_id[]" value="{{$order->id}}" id="check_order_id"/>
                            </td>
                            <td>
                                {{ $order->code }}
                            </td>
                            <td> 
                                @if($order->marg_order_no == null) 
                                <label class="label label-warning">Not Generated</label>
                                @else
                                {{$order->marg_order_no}}
                                @endif
                            </td>
                            <td @if($order->user_id != null) class="marginuser_{{$order->user_id}}" @endif>
                                @if(\Route::currentRouteName() == 'orders.index.ready_to_delivery.admin')
                                  @php $shipping_address = json_decode($order->shipping_address,true); @endphp
                                    {{$shipping_address['address']}}, 
                                    {{$shipping_address['city']}}, 
                                    {{$shipping_address['phone']}}
                                @else
                                @if ($order->user_id != null)
                                    {{ $order->user->name }} 
                                    @if($order->user->marg_rid == null)
                                        <button class="btn btn-danger btn-sm" type="button" onclick="customerMapModal('{{$order->user->name}}', '{{$order->user->email}}', '{{$order->user->phone}}', '{{$order->user->address}}', '{{$order->user->city}}', '{{$order->user->postal_code}}', '{{$order->user_id}}')">Not in Marg</button>
                                    @else
                                        <label class="label label-success">in Marg</label>
                                    @endif
                                @else
                                    Guest ({{ $order->guest_id }})
                                @endif
                                @endif
                            </td>
                            <td class="text-center">
                                @php
                                $PP = \App\Product::select('id','name')->whereIn('id',$order->orderDetails->pluck('product_id'))->where('marg_code',null)->count()
                                @endphp
                                @if($PP == 0) 
                                <label class="label label-success">0</label>
                                @else 
                                <button class="btn btn-danger btn-sm" onclick="productMapModal({{$order->id}})">Map Product ({{$PP}})</button>
                                @endif
                            </td>
                            <td>
                                {{ single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax')) }}
                            </td>
                            <td>
                                @php
                                if($order->orderDetails->first() == null) {
                                $status == "Avoid";
                                }
                                else {
                                $status = $order->orderDetails->first()->delivery_status;
                                }
                                    
                                @endphp
                                @if($status == "Confirmation Pending")
                                <label class="label label-danger">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @elseif($status == "pending")
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @else
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @endif
                            </td>
                            <td>
                                {{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}
                            </td>
                            <!--<td>
                                <div class="btn-group dropdown">
                                    <button class="btn btn-danger dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                        {{__('Actions')}} <i class="dropdown-caret"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{ route('orders.show', encrypt($order->id)) }}">{{__('View')}}</a></li>
                                        
                                        
                                        <li><a href="javascript:confirm_order({{$order->id}}, 'pending')">Confirm Order</a></li>
                                        <li><a href="javascript:confirm_order({{$order->id}}, 'hold')">Mark Hold</a></li>
                                    </ul>
                                </div>
                            </td>-->
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="col-md-12">
                <br />&nbsp;
                <button class="btn btn-success" onclick="send_order()">Send Order to Marg</button>
               
                <br />&nbsp;
                <br />&nbsp;
            </div>
        </div>
        <div class="clearfix">
            <div class="pull-right">
                {{ $orders->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="customer_map_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Map AldeBazaar Customer to Marg Customer')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row" style="padding-bottom:5px;">
                    <div class="col-sm-3 text-right pt-2">
                        Date &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control datepicker" value="{{date('d/m/Y')}}" id="marg_date">
                    </div>
                </div>
                <div class="form-group row" style="padding-bottom:5px;">
                    <div class="col-sm-7 col-sm-offset-3">
                       <strong> Customer Info :</strong>
                    </div>
                    <div class="col-sm-7 col-sm-offset-3">
                    Name : <span id="user_name"></span> <br />
                    Contact :<span id="email"></span>, <span id="phone"></span> <br />
                     Address : <span id="address"></span> <br />
                    </div>             
                    <input type="hidden" class="form-control" name="user_id" id="user_id">
                </div>
                <div class="form-group row" style="padding-bottom:5px;">
                    <div class="col-sm-3 text-right pt-2">
                        Customer(s) in Marg &nbsp;
                    </div>
                    <div class="col-sm-7">
                        <select class="form-control modal_select2"  id="modal_marg_id" required>
                            <option value="">{{__('Select Marg Customer')}}</option>
                        </select>
                    </div>
                    <div class="col-sm-2 "  style="padding-bottom:8px;">
                        <button class="btn btn-success btn-sm" onclick="getMargCustomer()">Relaod</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success"  data-dismiss="modal" onclick="updateUser()">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="product_map_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Map AldeBazaar Product to Marg Product')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row" style="padding-bottom:5px;">
                    <div class="col-sm-2 text-right pt-2">
                        Date &nbsp;
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control datepicker" value="{{date('d/m/Y')}}" id="marg_product_date">
                    </div>
                    <div class="col-sm-4" id="product_modal_btn" style="padding-bottom:8px;">
                    </div>
                </div>
                <div class="row divarea">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')

    <script type="text/javascript">
   $('.modal_select2').select2({
dropdownParent: $('#customer_map_modal')
});
    function send_order() {
        orderArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           orderArray.push($(v).val()); 
        });
        if(orderArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        
        $.post('{{ route('marg.order.send') }}', {_token:'{{ @csrf_token() }}', order_ids:orderArray}, function(data){
            location.reload();
        });
    }
        
    function updateUser() {
        name = $('#user_name').val();
        userid = $('#user_id').val();
        modal_marg_id = $('#modal_marg_id').val();
        $.ajax({
        type: 'POST',
        url: '{{ route('marg.customer.map') }}',
        data: { _token: '{{ @csrf_token() }}', userid : userid, modal_marg_id :modal_marg_id},
        success: function(result){
          if(result.status == true) {
              html = name +' <label class="label label-success">in Marg</label>';
              $('.marginuser_'+userid).html(html);
              $('#user_name').val('');
              $('#user_id').val('');
              $('#modal_marg_rid').val('');
          }
        }
    });
    }
    
    getMargCustomer();
        
    $('.datepicker').datepicker({
            clearBtn: true,
            format: "dd/mm/yyyy",
            autoclose: true,
    });
        
    function customerMapModal(name, email, phone, address, city, postal_code, userid) {
    $('#user_name').html(name);
    $('#email').html(email);
    $('#phone').html(phone);
    $('#address').html(address+" "+city+" "+postal_code);
    $('#user_id').val(userid);
    $('#customer_map_modal').modal('show');
}
        function resync(order_id) {
             $('.divarea').html('Loading ...');
            getMargProduct(order_id);
        }
    
    function productMapModal(order_id) {
        html = '<button class="btn btn-success btn-sm" onclick="resync('+order_id+')">Re-Sync</button>';
        $('#product_modal_btn').html(html);
    $.ajax({
        type: 'POST',
        url: '{{ route('marg.order.product') }}',
        data: { _token: '{{ @csrf_token() }}', order_id : order_id},
        success: function(result){
          if(result.status == true) {
              html = "";
                 html +='<div class="row">';
                 html +='<div class="col-md-12">';
                 html +='<table class="table table-bordered">';
                 html +='<tr><th>Product Code</th><th>Name</th><th>Unit Price</th></tr>';
              if(result.Product.length == 0 ) {
                  html +='<tr><td colspan="4">All product added, <a href="javascript:location.reload()">click here reload the page</a> </td></tr>';
              } else {
                    $.each(result.Product, function(i,v) {
                    html +='<tr>';
                    html +='<input type="hidden" name="product_id[]" value="'+v.id+'">';
                    html +='<td>'+v.product_id+'</td>';
                    html +='<td>'+v.name+'</td>';
                    html +='<td>₹ '+v.unit_price+'</td>';
                    html += '</td>';
                    /*html +='<td>';
                    html +='<select name="marg_product_id[]" class="modal_marg_product_id form-control">';
                      if(MargUnlistedProduct == "") {
                          html += '<option value="">Loading</option>';
                      } else {
                          $.each(MargUnlistedProduct, function(i,v) {
                         html += '<option value="'+v.id+'">'+v.name+'</option>'; 
                      });
                      }
                      html += '</select></td>'; */
                      html += '</tr>';
                  });
              } 
              
                  html += '</table>';
                  html += '</div>';
                  html += '</div>';
              $('.divarea').html(html);
          }
        }
    });
    $('#product_map_modal').modal('show');
}

var MargUnlistedProduct = '';  
getMargProduct();
function getMargProduct(order_id = 0) {
    var marg_date =  $('#marg_product_date').val();
     $('.modal_marg_product_id').html('<option value="">Loading</option>');
    $.ajax({
        type: 'POST',
        url: '{{ route('marg.product') }}',
        data: { _token: '{{ @csrf_token() }}', datetime : marg_date},
        success: function(result){
          if(result.status == true) {
              html = '';
              MargUnlistedProduct = result.MargUnlistedProduct;
              $.each(result.MargUnlistedProduct, function(i,v) {
                 html += '<option value="'+v.id+'">'+v.name+'</option>'; 
              });
              $('.modal_marg_product_id').html(html);
              if(order_id > 0) {
                  productMapModal(order_id);
              }
          }
        }
    });
}

function getMargCustomer() {
    var marg_date =  $('#marg_date').val();
     $('#modal_marg_id').html('<option value="">Loading</option>');
    $.ajax({
        type: 'POST',
        url: '{{ route('marg.customer') }}',
        data: { _token: '{{ @csrf_token() }}', datetime : marg_date},
        success: function(result){
          if(result.status == true) {
              html = '';
              $.each(result.MargUnlistedCustomer, function(i,v) {
                 html += '<option value="'+v.id+'">'+v.name+'</option>'; 
              });
              $('#modal_marg_id').html(html);
          }
        }
    });
}

    </script>
@endsection