@extends('layouts.app')

@section('content')

@php
    if(isset($seller_id)) {
    $admin_user_id = $seller_id;
} else {
$admin_user_id = \App\User::where('user_type', 'admin')->first()->id;
}
@endphp
    <div class="panel">
    	<div class="panel-body">
    		<div class="invoice-masthead">
    			<div class="invoice-text">
    				<h3 class="h1 text-thin mar-no text-primary">{{ __('Order Details') }}</h3>
    			</div>
    		</div>
            <div class="row">
                @php
                    	$return_product_id=\App\ReturnProduct::where('order_id',$order->id)->pluck('product_id');
                    $otc_confirmation = count($order->orderDetails->where('seller_id', $admin_user_id)->where('delivery_status', 'confirmation_pending'));
                    $delivery_status = $order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->first()->delivery_status??'NA';
                    $payment_status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->payment_status??'NA';
                @endphp
                @if($otc_confirmation == 0)
                <!--div class="col-lg-offset-6 col-lg-3">
                    <label for=update_payment_status"">{{__('Payment Status')}}</label>
                    <select class="form-control demo-select2"  data-minimum-results-for-search="Infinity" id="update_payment_status">
                        <option value="paid" @if ($payment_status == 'paid') selected @endif>{{__('Paid')}}</option>
                        <option value="unpaid" @if ($payment_status == 'unpaid') selected @endif>{{__('Unpaid')}}</option>
                    </select>
                </div-->
                <!--div class="col-lg-3">
                    <label for=update_delivery_status"">{{__('Return Status')}}</label>
                    <select class="form-control demo-select2"  data-minimum-results-for-search="Infinity" id="update_delivery_status">

                        <option value="processing_refund" @if ($delivery_status == 'processing_refund') selected @endif>{{__('Processing Refund')}}</option>
                        <option value="refunded" @if ($delivery_status == 'refunded') selected @endif>{{__('Refunded')}}</option>
                      
                    </select>
                </div-->
                <div clas="col-md-6">
                    <div class=" pull-right">
                        @if ($delivery_status == 'return_request') 
                        <button class="btn btn-primary" onclick="update_status('return_accepted')">Accept Request</button>
                        <button class="btn btn-danger" onclick="update_status('return_cancelled')" >Cancel Request</button>
                        @elseif($delivery_status == 'return_accepted')
                        <button class="btn btn-primary" onclick="update_status('pickup_completed')">Pickup Completed</button><!--onclick="confirm_payment('paid')"-->
                         @elseif($delivery_status == 'pickup_completed')
                         <button class="btn btn-primary" onclick="update_status('refunded')">Amount Credited to Wallet</button>
                         @elseif($delivery_status == 'refunded')
                         <label for=update_delivery_status"">{{__('Refund Completed')}}</label>
                         @elseif($delivery_status == 'return_cancelled')
                         <label for=update_delivery_status"">{{__('Reurn Request Cancelled')}}</label>
                        @endif
                        @if($order->prescription_file != null)<a  class="btn btn-primary" href="{{ asset('prescription/'.$order->prescription_file) }}" target="_blank">{{__('Download Prescription')}}</a> @endif
                    </div>
                </div>
                
                @endif
            </div>
            <hr>
    		<div class="invoice-bill row">
    			<div class="col-sm-6 text-xs-center">
    				<address>
        				<strong class="text-main">{{ json_decode($order->shipping_address)->name }}</strong><br>
                         {{ json_decode($order->shipping_address)->email }}<br>
                         {{ json_decode($order->shipping_address)->phone }}<br>
        				 {{ json_decode($order->shipping_address)->address }}, {{ json_decode($order->shipping_address)->city }}, {{ json_decode($order->shipping_address)->country }}
                    </address>
                    @if ($order->manual_payment && is_array(json_decode($order->manual_payment_data, true)))
                        <br>
                        <strong class="text-main">{{ __('Payment Information') }}</strong><br>
                        Name: {{ json_decode($order->manual_payment_data)->name }}, Amount: {{ single_price(json_decode($order->manual_payment_data)->amount) }}, TRX ID: {{ json_decode($order->manual_payment_data)->trx_id }}
                        <br>
                        <a href="{{ asset(json_decode($order->manual_payment_data)->photo) }}" target="_blank"><img src="{{ asset(json_decode($order->manual_payment_data)->photo) }}" alt="" height="100"></a>
                    @endif
    			</div>
    			<div class="col-sm-6 text-xs-center">
    				<table class="invoice-details">
    				<tbody>
    				<tr>
    					<td class="text-main text-bold">
    						{{__('Order #')}}
    					</td>
    					<td class="text-right text-info text-bold">
    						{{ $order->code }}
    					</td>
    				</tr>
    				
    				<tr>
    					<td class="text-main text-bold">
    						{{__('Order Status')}}
    					</td>
                        @php
                            $status = $order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->first()->delivery_status??'NA';
                        @endphp
    					<td class="text-right">
                            @if($status == 'delivered')
                                <span class="badge badge-success">{{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                            @else
                                <span class="badge badge-info">{{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                            @endif
    					</td>
    				</tr>
    				<tr>
    					<td class="text-main text-bold">
    						{{__('Order Date')}}
    					</td>
    					<td class="text-right">
    						{{ date('d-m-Y h:i A', $order->date) }} (UTC)
    					</td>
    				</tr>
                    <tr>
    					<td class="text-main text-bold">
    						{{__('Total amount')}}
    					</td>
					
    					<td class="text-right">
    						{{ single_price($order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->sum('price') + $order->orderDetails->where('seller_id', $admin_user_id)->sum('tax')) }}
    					</td>
    				</tr>
                    <tr>
    					<td class="text-main text-bold">
    						{{__('Payment method')}}
    					</td>
    					<td class="text-right">
    						{{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}
    					</td>
    				</tr>
    				</tbody>
    				</table>
    			</div>
    		</div>
    		<hr class="new-section-sm bord-no">
    		<div class="row">
    			<div class="col-lg-12 table-responsive">
    				<table class="table table-bordered invoice-summary">
        				<thead>
            				<tr class="bg-trans-dark">
                                <th class="min-col text-center">#</th>
                                <th width="10%">
            						{{__('Photo')}}
            					</th>
            					<th class="text-uppercase">
            						{{__('Description')}}
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Qty')}}
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Price')}}
            					</th>
            					<th class="min-col text-right text-uppercase">
            						{{__('Total')}}
            					</th>
            				</tr>
        				</thead>
        				<tbody>
                            @foreach ($order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id) as $key => $orderDetail)
                                <tr>@php
                					    $return_request=\App\ReturnProduct::where('order_id',$orderDetail->order_id)->where('product_id',$orderDetail->product_id)->first();
                					   // dd($return_request);
                					    @endphp
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td>
                                        @if ($orderDetail->product != null )
                    						<a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank"><img height="50" src="{{ asset($orderDetail->product->thumbnail_img) }}"></a>
                                        @else
                                            <strong>{{ __('N/A') }}</strong>
                                        @endif
                                    </td>
                					<td>
                					    
                                        @if ($orderDetail->product != null )
                    						<strong><a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank">{{ $orderDetail->product->name }}</a> @if($return_request!=null) <span style="color:red">{{__('Return Requested')}}</span> @endif</strong>
                    						<small>{{ $orderDetail->variation }}</small>
                                        @else
                                            <strong>{{ __('Product Unavailable') }}</strong>
                                        @endif
                					</td>
                					<td class="text-center">
                						{{ $orderDetail->quantity }}
                					</td>
                					<td class="text-center">
                						{{ single_price($orderDetail->price/$orderDetail->quantity) }}
                					</td>
                                    <td class="text-center">
                						{{ single_price($orderDetail->price) }}
                					</td>
                				</tr>
                            @endforeach
        				</tbody>
    				</table>
    			</div>
    		</div>
    		<div class="clearfix">
    			<table class="table invoice-total">
    			<tbody>
    			<tr>
    				<td>
    					<strong>{{__('Sub Total')}} :</strong>
    				</td>
    				<td>
    					{{ single_price($order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->sum('price')) }}
    				</td>
    			</tr>
    			<tr>
    				<td>
    					<strong>{{__('Tax')}} :</strong>
    				</td>
    				<td>
    					{{ single_price($order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->sum('tax')) }}
    				</td>
    			</tr>
                <tr>
    				<td>
    					<strong>{{__('Shipping')}} :</strong>
    				</td>
    				<td>
    					{{ single_price($order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->sum('shipping_cost')) }}
    				</td>
    			</tr>
    			<tr>
    				<td>
    					<strong>{{__('TOTAL')}} :</strong>
    				</td>
    				<td class="text-bold h4">
    				    <input type="hidden" value="{{ $order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->sum('price') + $order->orderDetails->where('seller_id', $admin_user_id)->sum('tax') + $order->orderDetails->where('seller_id', $admin_user_id)->sum('shipping_cost') }}" id="return_amount"/>
    					{{ single_price($order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->sum('price') + $order->orderDetails->where('seller_id', $admin_user_id)->sum('tax') + $order->orderDetails->where('seller_id', $admin_user_id)->sum('shipping_cost')) }}
    				</td>
    			</tr>
    			</tbody>
    			</table>
    		</div>
    		<div class="text-right no-print">
    			<a href="{{ route('customer.invoice.download', $order->id) }}" class="btn btn-default"><i class="demo-pli-printer icon-lg"></i></a>
    		</div>
    	</div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#update_delivery_status').on('change', function(){
            var order_id = {{ $order->id }};
            var status = $('#update_delivery_status').val();
            
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                showAlert('success', 'Delivery status has been updated');
            });
        
        });
        function update_status(status) {
            var order_id = {{ $order->id }};
            var amount=$('#return_amount').val();;
          // alert(order_id+" ~ "+status+" ~ "+amount);
            var setFlash = 'Return status has been updated';
             $.post('{{ route('returns.update_return_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status,amount:amount @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                location.reload();
            });
        }

        function confirm_order(status) {
            var order_id = {{ $order->id }};
            var setFlash = 'Order status has been updated';
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                  location.reload();
                });
        }

      
    </script>
@endsection
