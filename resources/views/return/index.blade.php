@extends('layouts.app')

@section('content')
@php
    $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
@endphp

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Orders')}}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_orders" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <!--div class="select" style="min-width: 300px;">
                        <select class="form-control demo-select2" name="payment_type" id="payment_type" onchange="sort_orders()">
                            <option value="">{{__('Filter by Payment Status')}}</option>
                            <option value="paid"  @isset($payment_status) @if($payment_status == 'paid') selected @endif @endisset>{{__('Paid')}}</option>
                            <option value="unpaid"  @isset($payment_status) @if($payment_status == 'unpaid') selected @endif @endisset>{{__('Un-Paid')}}</option>
                        </select>
                    </div-->
                </div>
                <div class="box-inline pad-rgt pull-left">
                    <!--div class="select" style="min-width: 300px;">
                        <select class="form-control demo-select2" name="delivery_status" id="delivery_status" onchange="sort_orders()">
                            <option value="">{{__('Filter by Return Status')}}</option>

                            <option value="processing_refund"   @isset($delivery_status) @if($delivery_status == 'processing_refund') selected @endif @endisset>{{__('Processing Refund')}}</option>
                             <option value="return_accepted"   @isset($delivery_status) @if($delivery_status == 'return_accepted') selected @endif @endisset>{{__('Return Accepted')}}</option>
                        <option value="pickup_completed"   @isset($delivery_status) @if($delivery_status == 'return_accepted') selected @endif @endisset>{{__('Return Accepted')}}</option>
                     
                            <option value="refunded"   @isset($delivery_status) @if($delivery_status == 'refunded') selected @endif @endisset>{{__('Refunded')}}</option>
                        </select>
                    </div-->
                </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Order Code')}}</th>
                    <th>{{__('Num. of Products')}}</th>
                    <th>{{__('Customer')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Delivery Status')}}</th>
                    <th>{{__('Payment Method')}}</th>
                    <th>{{__('Payment Status')}}</th>
                    @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
                        <th>{{__('Refund')}}</th>
                    @endif
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $key => $order_id)
                    @php
                        $order = \App\Order::find($order_id->id);
                        $return_product_id=\App\ReturnProduct::where('order_id',$order_id->id)->pluck('product_id');
								//dd(\Route::currentRouteName());
                    @endphp
                    @if($order != null && count($return_product_id) != 0)
                
                        <tr>
                            <td>
                                {{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }} <input type="checkbox" data-ordercode="{{$order->code}}" name="order_id[]" value="{{$order->id}}" id="check_order_id"/>
                            </td>
                            <td>
                                {{ $order->code }} @if($order->viewed == 0) <span class="pull-right badge badge-info">{{ __('New') }}</span> @endif
                            </td>
                            
                            <td>
                               
								 Return Request of {{count($return_product_id)}} out of {{ count($order->orderDetails->where('seller_id', $admin_user_id)) }} 
						
                            </td>
                            <td>
                                @if(\Route::currentRouteName() == 'orders.index.ready_to_delivery.admin')
                                  @php $shipping_address = json_decode($order->shipping_address,true); @endphp
                                    {{$shipping_address['address']}}, 
                                    {{$shipping_address['city']}}, 
                                    {{$shipping_address['phone']}}
                                @else
                                @if ($order->user_id != null)
                                    {{ $order->user->name??' ' }}
                                @else
                                    Guest ({{ $order->guest_id }})
                                @endif
                                @endif
                            </td>
                            <td>
                               
						
                                {{ single_price($order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->sum('price') + $order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->sum('tax')) }}
						
                            </td>
                            <td>
                                @php
                                    $status = $order->orderDetails->whereIn('product_id', $return_product_id)->where('seller_id', $admin_user_id)->first()->delivery_status;
                                @endphp
                                @if($status == "Confirmation Pending")
                                <label class="label label-danger">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @elseif($status == "pending")
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @else
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @endif
                            </td>
                            <td>
                                {{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}
                            </td>
                            <td>
                                <span class="badge badge--2 mr-4">
                                    @if ($order->orderDetails->where('seller_id',  $admin_user_id)->first()->payment_status == 'paid')
                                        <i class="bg-green"></i> Paid
                                    @php $payment_status = "paid"; @endphp
                                    @else
                                        <i class="bg-red"></i> Unpaid
                                    @php $payment_status = "unpaid"; @endphp
                                    @endif
                                </span>
                            </td>
                            @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
                                <td>
                                    @if (count($order->refund_requests) > 0)
                                        {{ count($order->refund_requests) }} Refund
                                    @else
                                        No Refund
                                    @endif
                                </td>
                            @endif
                            <td>
                               
                                <div class="btn-group">
                                   <a href="{{ route('return_orders.show', encrypt($order->id)) }}">
                                        <button class="btn btn-primary" type="button">
                                        {{__('View')}} 
                                    </button>
                                    </a>
                                  
                                </div>
                                    
                               
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        
        <div class="clearfix">
            <div class="pull-right">
                {{ $orders->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>
<script>
    function showmodal(modalid) {
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
        productArray.push($(v).val());
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        $('#'+modalid).modal();
    }
function mark_undelivered() {
    productArray = [];
    productCodeArray = [];
    html = '<table class="table table-bordred table-hover">';
    html +='<thead>';
    html +='    <tr>';
    html +='    <th class="text-center">Order No.</th>';
    html +='    <th class="text-center">Order Code</th>';
    html +='    <th class="text-center">Undelivery Note</th>';
    html +='    </tr>';
    html +='</thead>';
    html +='<tbody>';
    $('#check_order_id:checked').each(function(i,v) {
        productArray.push($(v).val());
        productCodeArray.push($(v).data('ordercode')); 
        html +='    <tr>';
        html +='        <td class="text-center">'+ $(v).val() +'<input type="hidden" name="order_ids[]" value="'+ $(v).val() +'"  class="form-control"></td>';
        html +='        <td class="text-center">'+ $(v).data('ordercode') +'<input type="hidden" name="order_code[]" value="'+ $(v).data('ordercode') +'" class="form-control"></td>';
        html +='        <td><input type="text" name="undelivery_notes[]" class="form-control"></td>';
        html +='    </tr>';
    });
    if(productCodeArray.length == 0) {
        alert("Kindly select order.");
        return false;
    }
    html +='</tbody>';
    html +='</table>';
    $('#undelivery_div').html(html);
   // console.log(productCodeArray);
    $('#undelivery_status_modal').modal();
}
</script>



<div class="modal fade" id="payment_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Payment Status')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Status &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control demo-select2"  id="modal_payment_status">
                            <option value="">{{__('Select Payment Status')}}</option>
                            <option value="paid">{{__('Paid')}}</option>
                            <option value="unpaid">{{__('Unpaid')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="change_status('payment')"  data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="shipping_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Shipping Info')}}</h4>
            </div>
            <form action="{{route('orders.shipping')}}" method="post">
            @csrf
            <div class="modal-body">
            <input type="hidden" name="order_id" id="modal_order_id">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Courier &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name='courier' placeholder="Courier Company Name" >
                    </div>
                </div>
                <div class="form-group row" style="margin-top:5px;">
                    <div class="col-sm-3 text-right pt-2">
                        AWB/Ref/Tracking No. &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="awb" class="form-control" placeholder="AWB Number" >
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                  <button class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')

    <script type="text/javascript">
        
        $('#undelivery_Form').submit(function(e){
            e.preventDefault();
            formdata =  $('#undelivery_Form').serialize();
            $('#undelivery_status_modal').modal('hide');
            $.ajax({
                type: 'POST',
                url: '{{ route('orders.update_status') }}',
                data:formdata,
                processData: false,
                success: function(result){
                 location.reload();
                }
            });
        });
        
    function mark_delivered() {
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "delivered";
        var setFlash = 'Order set for delivered status.';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
             location.reload();
        });
    }
        
    function assign_delivery() {
        agent_id = $('#modal_delivery_agent_id').val();
        $('#modal_delivery_agent_id').val("");
        $('#modal_delivery_agent_id').trigger('change');
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "assign";
        var setFlash = 'Order set for out for delivery status.';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, agent_id:agent_id, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
             location.reload();
        });
    }
        
    function change_status(action) {
        if(action == 'delivery') {
            status = $('#modal_delivery_status').val();
            $('#modal_delivery_status').val("");
            $('#modal_delivery_status').trigger('change');
        }
        if(action == 'payment') {
            status = $('#modal_payment_status').val();
            $('#modal_payment_status').val("");
            $('#modal_payment_status').trigger('change');
        }
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        }); 
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        
        var setFlash = 'Order '+action+' status has been updated';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
             location.reload();
        });
    }
        
    function order_shipping_info(order_id)
    {
        $('#modal_order_id').val(order_id);
        $('#shipping_info').modal();
    }
        function sort_orders(el){
            $('#sort_orders').submit();
        }
    
        function confirm_payment(order_id, status) {
            var setFlash = 'Payment status has been updated';
            $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                location.reload();
            });
        }

        function confirm_order(order_id, status) {
            var setFlash = 'Order status has been updated';
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                 location.reload();
            });
        }
    </script>
@endsection