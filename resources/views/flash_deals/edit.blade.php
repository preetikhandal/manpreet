@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-sm-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Flash Deal Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('flash_deals.update', $flash_deal->id) }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <input type="hidden" name="_method" value="PATCH">
            <span class="err text-danger"></span>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Title')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Title')}}" id="name" name="title" value="{{ $flash_deal->title }}" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="bg_img">{{__('Background Image')}} </label>
                    <div class="col-sm-9">
                        <input type="file" id="bg_img" name="bg_img" class="form-control">
						<small class="text-danger">(222px * 250px)</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="background_color">{{__('Background Color')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('#0000ff')}}" id="background_color" name="background_color" value="{{ $flash_deal->background_color }}" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="name">{{__('Text Color')}}</label>
                    <div class="col-lg-9">
                        <select name="text_color" id="text_color" class="form-control demo-select2" required>
                            <option value="">Select One</option>
                            <option value="white" @if ($flash_deal->text_color == 'white') selected @endif>{{__('White')}}</option>
                            <option value="dark" @if ($flash_deal->text_color == 'dark') selected @endif>{{__('Dark')}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="banner">{{__('Banner')}} <small>(1920x500)</small></label>
                    <div class="col-sm-9">
                        <input type="file" id="banner" name="banner" class="form-control">
						<em class="text-danger" id="error"></em>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="start_date">{{__('Date')}}</label>
                    <div class="col-sm-9">
                        <div id="demo-dp-range">
                            <div class="input-daterange input-group" id="datepicker">
                                <input type="text" class="form-control" name="start_date" value="{{ date('m/d/Y', $flash_deal->start_date) }}">
                                <span class="input-group-addon">{{__('to')}}</span>
                                <input type="text" class="form-control" name="end_date" value="{{ date('m/d/Y', $flash_deal->end_date) }}">
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="form-group mb-3" id="category">
                    <label class="col-lg-3 control-label">{{__('Category')}}</label>
                    <div class="col-lg-9">
                        <select class="form-control demo-select2-placeholder demo-select3" name="category_id" id="category_id">
                            <option>Select Category</option>
                        	@foreach(\App\Category::all() as $category)
                        	    <option value="{{$category->id}}" @if(old('category_id')==$category->id) selected @endif>{{__($category->name)}}</option>
                        	@endforeach
                        </select>
                    </div>
                </div>
	            <div class="form-group mb-3" id="subcategory">
                    <label class="col-lg-3 control-label">{{__('Subcategory')}}</label>
                    <div class="col-lg-9">
                        <select class="form-control demo-select2-placeholder demo-select3" name="subcategory_id" id="subcategory_id">

                        </select>
                    </div>
                </div>
	            <div class="form-group mb-3" id="subsubcategory">
                    <label class="col-lg-3 control-label">{{__('Sub Subcategory')}}</label>
                    <div class="col-lg-9">
                        <select class="form-control demo-select2-placeholder demo-select3" name="subsubcategory_id" id="subsubcategory_id">

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="products">{{__('Products')}}</label>
                    <div class="col-sm-9">
                        <select name="products[]" id="products" class="form-control demo-select2" multiple required data-placeholder="Choose Products">
                            @php
                                 $deal_product = \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->pluck('product_id');
                            @endphp
                            @foreach(\App\Product::whereIn('id',$deal_product)->get() as $product)
                                @php
                                    $flash_deal_product = \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first();
                                @endphp
                                <option value="{{$product->id}}" <?php if($flash_deal_product != null) echo "selected";?> >{{__($product->name)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>
                <div class="form-group" id="discount_table">

                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit" id="submitbtn">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    


function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		    $("#subcategory_id > option").each(function() {
		        
		            $("#subcategory_id").val(this.value).change();
		      
		    });

		   $('.demo-select3').select2();

		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
			$('#subsubcategory_id').append($('<option>', {
			    value: "",
                text: 'Select Sub Categories'
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		/*	
		    $("#subsubcategory_id > option").each(function() {
		      
		            $("#subsubcategory_id").val(this.value).change();
		       
		    });
        */
		   // $('.demo-select3').select2();
           // get_products_by_subsubcategory();
		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}
        $('#subsubcategory_id').on('change', function() {
            get_products_by_subsubcategory();
        });
        
        
        function get_products_by_subsubcategory(){
            var count = $("#products :selected").length;
            var category_id = $('#category_id').val();
            var subcategory_id = $('#subcategory_id').val();
            var subsubcategory_id = $('#subsubcategory_id').val();
            $.post('{{ route('productsstock.get_product_by_subsubcategory') }}',{_token:'{{ csrf_token() }}',category_id:category_id,subcategory_id:subcategory_id, subsubcategory_id:subsubcategory_id}, function(data){
               if(count<=0){
               $('#products').html(null);
                 $('#products').append($('<option>', {
                        value: "",
                        text: 'Select Products'
                    }));
               }
                for (var i = 0; i < data.length; i++) {
                    $('#products').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                }
                $('#products').trigger("change");
            });
        }

	$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

</script>
    <script type="text/javascript">
        $(document).ready(function(){
			//Upload File
			var url = window.URL || window.webkitURL;
			$("#banner").change(function(e) {
				var chosen = this.files[0];
				var type=chosen.type;
				var image = new Image();
				image.onload = function() {
					var imagewidth=this.width;
					var imageheight=this.height;
					if (imagewidth>1920 || imageheight>500)
					{
						$("#error").html("Maximum image width and height should be 1920(W)*500(H)");
						$("#banner").val('');
					}
					else{ $("#error").html(""); }
					
				};
				if(type!="image/jpeg")
				{
					$("#error").html("Not a valid file type: Jpeg or jpg only");
					$("#banner").val('');
					return false;
				}
				else{ $("#error").html(""); }
				image.onerror = function() {
					$("#error").html("Not a valid file type:"+ chosen.type);
					$("#banner").val('');
				};
				image.src = url.createObjectURL(chosen);
			});
			//End Upload File

            get_flash_deal_discount();

            $('#products').on('change', function(){
                get_flash_deal_discount();
            });

            function get_flash_deal_discount(){
                var product_ids = $('#products').val();
                if(product_ids.length > 0){
                    $.post('{{ route('flash_deals.product_discount_edit') }}', {_token:'{{ csrf_token() }}', product_ids:product_ids, flash_deal_id:{{ $flash_deal->id }}}, function(data){
                        $('#discount_table').html(data);
                        $('.demo-select2').select2();
                    });
                }
                else{
                    $('#discount_table').html(null);
                }
            }
        });
    </script>
    <script type="text/javascript">
    	$('INPUT[type="file"]').change(function () {
    		var ext = this.value.match(/\.(.+)$/)[1];
    	    switch (ext) {
    			case 'jpg':
    			case 'jpeg':
    			case 'png':
    			    $(".err").text('');
    				$('#submitbtn').attr('disabled', false);
    				break;
    			default:
    				$(".err").text('This is not an allowed file type.');
    				this.value = '';
    		}
    	});
    </script>
@endsection
