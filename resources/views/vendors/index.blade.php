@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <a href="{{ route('vendors.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Vendor')}}</a>
        </div>
    </div>

    <br>

    <!-- Basic Data Tables -->
    <!--===================================================-->
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all h-100">
            <h3 class="panel-title pull-left pad-no">{{__('Vendors')}}</h3>
            <div class="pull-right clearfix">
                <form class="" id="sort_sellers" action="" method="GET">
                    <div class="box-inline pad-rgt pull-left">
                        <div class="" style="min-width: 200px;">
                            <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type name or email & Enter">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Company Name')}}</th>
                    <th>{{__('User Login')}}</th>
                    <th>{{__('Phone')}}</th>
                    <th>{{__('City - State')}}</th>
                    <th>{{ __('Assgined Products') }}</th>
                    <th width="10%">{{__('Options')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($vendors as $key => $vendor)
                        <tr>
                            <td>{{ ($key+1) + ($vendors->currentPage() - 1)*$vendors->perPage() }}</td>
                            <td>{{$vendor->company_name}}</td>
                            <td>
                                @if($vendor->user_id == 0)
                                <label class="label label-danger">Not Active</label>
                                @else
                                <label class="label label-success">Active</label>
                                @endif
                            </td>
                            <td>{{$vendor->phone}}</td>
                            <td>{{$vendor->city}} {{$vendor->state}}</td>
                            @if($vendor->user_id == 0)
                            <td> 0 </td>
                            @else
                            <td>{{ \App\Product::where('user_id', $vendor->user_id)->count() }}</td>
                            @endif
                            <td>
                                <div class="btn-group dropdown">
                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                        {{__('Actions')}} <i class="dropdown-caret"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{route('vendors.edit', $vendor->id)}}">{{__('Edit Vendor')}}</a></li>
                                        @if($vendor->user_id > 0)
                                        <li><a href="{{route('products.vendor', encrypt($vendor->user_id))}}">{{__('Products')}}</a></li>
                                        @else
                                        <li><a href="javascript:create_login({{$vendor->id}})">{{__('Create User Login')}}</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </table>
            <div class="clearfix">
                <div class="pull-right">
                    {{ $vendors->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="profile_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-content">

            </div>
        </div>
    </div>


<div class="modal fade" id="create_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">{{__('Create Login')}}</h4>
            </div>
            <form action="{{route('vendor.create_login')}}" method="post" onsubmit="return loginValidate()">
            @csrf
            <div class="modal-body">
            <input type="hidden" name="vendor_id" id="vendor_id">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Password &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name='password' id='password' placeholder="Password"  onkeyup="validate_password()" required>
                    </div>
                </div>
                <div class="form-group row" style="margin-top:5px;">
                    <div class="col-sm-3 text-right pt-2">
                        Confirm Password&nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="password" name="password_confirm" id="password_confirm" class="form-control" placeholder="Confirm Password" onkeyup="validate_password()" required>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                  <button class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>



@endsection

@section('script')
    <script type="text/javascript">
var password_error = false;

 function create_login(vendor_id)
    {
        $('#password').val("");
        $('#password_confirm').val("");
        $('#vendor_id').val(vendor_id);
        $('#create_login').modal();
    }
        
        function validate_password() {
            password = $('#password').val();
            password_confirm = $('#password_confirm').val();
            if(password_confirm.length > 0) {
                if(password_confirm != password) {
                    password_error = true;
                    $('#password_confirm').css('border','1px solid red');
                } else {
                    password_error = false;
                    $('#password_confirm').css('border','1px solid green');
                }
            }
        }
        
        
function loginValidate() {
    if(password_error == true) {
        alert('Kindly check error before submit the form');
        return false;
    } else {
        return true;
    }
}
        
        
        function show_vendor_profile(id){
            $.post('{{ route('vendors.profile_modal') }}',{_token:'{{ @csrf_token() }}', id:id}, function(data){
                $('#profile_modal #modal-content').html(data);
                $('#profile_modal').modal('show', {backdrop: 'static'});
            });
        }

        function update_status(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('vendors.activated') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Vendor status updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function sort_vendors(el){
            $('#sort_vendors').submit();
        }
    </script>
@endsection
