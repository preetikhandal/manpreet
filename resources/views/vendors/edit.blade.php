@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-title">
            <center><h3>{{__('Vendor Information')}}</h3></center>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('vendors.update', $vendor->id) }}" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        {{__('Company Name')}} <br />
                        <input type="text" placeholder="{{__('Name')}}" id="company_name" name="company_name" value="{{$vendor->company_name}}" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        {{__('Address')}} <br />
                        <input type="text" placeholder="{{__('Address')}}" id="address" name="address" value="{{$vendor->address}}"  class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        {{__('City')}}<br />
                        <input type="text" placeholder="{{__('City')}}" id="city" name="city" value="{{$vendor->city}}"  class="form-control" required>
                    </div>
                    <div class="col-sm-4">
                        {{__('State')}} <br />
                        <input type="text" placeholder="{{__('State')}}" id="state" name="state" value="{{$vendor->state}}"  class="form-control" required>
                    </div>
                    <div class="col-sm-4">
                        {{__('Pincode')}}<br />
                        <input type="text" placeholder="{{__('Pincode')}}" id="pincode" name="pincode" value="{{$vendor->pincode}}"  class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        {{__('Email Address')}} <br />
                        <input type="text" placeholder="{{__('Email Address')}}" id="email" name="email" value="{{$vendor->email}}"  class="form-control" required>
                    </div>
                    <div class="col-sm-6">
                        {{__('Phone')}} <br />
                        <input type="text" placeholder="{{__('Phone')}}" id="phone" name="phone" class="form-control" value="{{$vendor->phone}}"  required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        {{__('GSTIN')}} <br />
                        <input type="text" placeholder="{{__('GSTIN')}}" id="gstin" name="gstin" class="form-control" value="{{$vendor->gstin}}"  onkeyup="verifygstin(this.value)">
                    </div>
                    <div class="col-sm-6">
                        {{__('PAN')}} <br />
                        <input type="text" placeholder="{{__('PAN')}}" id="pan" name="pan" class="form-control" value="{{$vendor->pan}}"  onkeyup="verifypan(this.value)">
                    </div>
                </div>
                <hr />
                <center><strong>Bank Information</strong></center>
                <hr />
                
                <div class="form-group">
                    <div class="col-sm-6">
                        {{__('Bank Name')}} <br />
                        <input type="text" placeholder="{{__('Bank Name')}}" id="bank_name" name="bank_name" value="{{$vendor->bank_name}}"  class="form-control">
                    </div>
                    <div class="col-sm-6">
                        {{__('Bank A/c Name')}} <br />
                        <input type="text" placeholder="{{__('Bank A/c Name')}}" id="bank_account_name" value="{{$vendor->bank_account_name}}" name="bank_account_name" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        {{__('A/c Number')}}<br />
                        <input type="text" placeholder="{{__('A/c Number')}}" id="bank_account_no" value="{{$vendor->bank_account_no}}" name="bank_account_no" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        {{__('IFSC Code')}} <br />
                        <input type="text" placeholder="{{__('IFSC Code')}}" id="bank_ifsc_code" value="{{$vendor->bank_ifsc_code}}" name="bank_ifsc_code" class="form-control">
                    </div>
                </div>
                @if($vendor->user_id > 0)
                <hr />
                <center>Password Reset (Fill-up only if need to reset.)</center>
                <hr />
                <div class="form-group" id="password_div" style="display:none">
                    <label class="col-sm-3 control-label" for="password">{{__('Password')}}</label>
                    <div class="col-sm-9">
                        <input type="password" placeholder="{{__('Password')}}" id="password" name="password" class="form-control">
                    </div>
                </div>
                @endif
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
@section('script')


<script>
var gstin_error = false;
var pan_error = false;

function verifygstin(gstin) {
    var gstin_pattern = /^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/g;
    if(gstin_pattern.test(gstin) === false) {
        gstin_error = true;
        $('#gstin').css('border','1px solid red');
    } else {
        gstin_error = false;
        $('#gstin').css('border','1px solid green');
    }
}
    
function verifypan(pan) {
    var pan_pattern = /^([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})+$/g;
    if(pan_pattern.test(pan) === false) {
        pan_error = true;
        $('#pan').css('border','1px solid red');
    } else {
        pan_error = false;
        $('#pan').css('border','1px solid green');
    }
}

function validate() {
    if(pan_pattern == true || gstin_pattern == ture) {
        alert('Kindly check error before submit the form');
        return false;
    } else {
        return true;
    }
}

function passwordManage(v) {
    if(v == 1) {
        $('#password_div').slideDown();
        $('#password').attr("required", true);
    } else {
        $('#password').removeAttr("required");
        $('#password_div').slideUp();
    }
}
</script>
@endsection
