@extends('layouts.app')

@section('content')

    <div class="tab-base">

        <!--Nav Tabs-->
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#demo-lft-tab-1" aria-expanded="true">{{ __('Home Menu') }}</a>
            </li>
			<li class="">
                <a data-toggle="tab" href="#demo-lft-tab-2" aria-expanded="true">{{ __('Home slider') }}</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-7" aria-expanded="false">{{ __('Brand banner') }}</a>
            </li>
			<li class="">
                <a data-toggle="tab" href="#demo-lft-tab-3" aria-expanded="false">{{ __('Home banner 1') }}</a>
            </li>
			<li class="">
                <a data-toggle="tab" href="#demo-lft-tab-4" aria-expanded="false">{{ __('Home banner 2') }}</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-10" aria-expanded="false">{{ __('Home banner 3') }}</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-5" aria-expanded="false">{{ __('Home categories') }}</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-9" aria-expanded="false">{{ __('Best Selling') }}</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-6" aria-expanded="false">{{ __('Top 20') }}</a>
            </li>
			<li class="">
                <a data-toggle="tab" href="#demo-lft-tab-8" aria-expanded="false">{{ __('Offer') }}</a>
            </li>
        </ul>

        <!--Tabs Content-->
        <div class="tab-content">
			<div id="demo-lft-tab-1" class="tab-pane fade active in">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_menu()" class="btn btn-rounded btn-info pull-right">{{__('Add New Menu')}}</a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Home Menu')}}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Menu')}}</th>
                                    <th width="50%">{{__('Link')}}</th>
                                    <th>{{__('Position')}}</th>
                                    <th>{{__('Published')}}</th>
                                    <th width="10%">{{__('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Menu::all() as $key => $menu)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$menu->name}}</td>
                                        <td>{{$menu->link}}</td>
										 <td>{{$menu->position}}</td>
                                        <td><label class="switch">
                                            <input onchange="update_menu_published(this)" value="{{ $menu->id }}" type="checkbox" <?php if($menu->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_menu({{ $menu->id }})">{{__('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('menu.destroy', $menu->id)}}');">{{__('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-2" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_slider()" class="btn btn-rounded btn-info pull-right">{{__('Add New Slider')}}</a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Home slider')}}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Photo')}}</th>
                                    <th>{{__('Mobile Photo')}}</th>
                                    <th width="50%">{{__('Link')}}</th>
                                    <th>{{__('Published')}}</th>
                                    <th width="10%">{{__('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Slider::all() as $key => $slider)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td><img loading="lazy"  class="img-md" src="{{ asset($slider->photo)}}" alt="Slider Image"></td>
                                        <td><img loading="lazy"  class="img-md" src="{{ asset($slider->mobile_photo)}}" alt="Slider Image"></td>
                                        <td>{{$slider->link}}</td>
                                        <td><label class="switch">
                                            <input onchange="update_slider_published(this)" value="{{ $slider->id }}" type="checkbox" <?php if($slider->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_slider({{ $slider->id }})">{{__('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('sliders.destroy', $slider->id)}}');">{{__('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-3" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_banner_1()" class="btn btn-rounded btn-info pull-right">{{__('Add New Banner')}}</a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Home banner')}}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Photo')}}</th>
                                    <th>{{__('URL')}}</th>
                                    <th>{{__('Published')}}</th>
                                    <th>{{__('Display Position')}}</th>
                                    <th width="10%">{{__('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Banner::where('position', 1)->get() as $key => $banner)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td><img loading="lazy"  class="img-md" src="{{ asset($banner->photo)}}" alt="banner Image"></td>
                                        <td>{{ $banner->url }}</td>
                                        <td><label class="switch">
                                            <input onchange="update_banner_published(this)" value="{{ $banner->id }}" type="checkbox" <?php if($banner->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>{{ $banner->displayposition }}</td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_home_banner_1({{ $banner->id }})">{{__('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('home_banners.destroy', $banner->id)}}');">{{__('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-4" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_banner_2()" class="btn btn-rounded btn-info pull-right">{{__('Add New Banner')}}</a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Home banner')}}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Photo')}}</th>
                                    <th>{{__('URL')}}</th>
                                    <th>{{__('Published')}}</th>
                                    <th>{{__('Display Position')}}</th>
                                    <th width="10%">{{__('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Banner::where('position', 2)->get() as $key => $banner)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td><img loading="lazy"  class="img-md" src="{{ asset($banner->photo)}}" alt="banner Image"></td>
                                        <td>{{ $banner->url }}</td>
                                        <td><label class="switch">
                                            <input onchange="update_banner_published(this)" value="{{ $banner->id }}" type="checkbox" <?php if($banner->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>{{ $banner->displayposition }}</td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_home_banner_2({{ $banner->id }})">{{__('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('home_banners.destroy', $banner->id)}}');">{{__('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-10" class="tab-pane fade">
                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_banner_3()" class="btn btn-rounded btn-info pull-right">{{__('Add New Banner')}}</a>
                    </div>
                </div>
                <br>
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Home banner')}}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Photo')}}</th>
                                    <th>{{__('URL')}}</th>
                                    <th>{{__('Published')}}</th>
                                    <th>{{__('Display Position')}}</th>
                                    <th width="10%">{{__('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Banner::where('position', 3)->get() as $key => $banner)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td><img loading="lazy"  class="img-md" src="{{ asset($banner->photo)}}" alt="banner Image"></td>
                                        <td>{{ $banner->url }}</td>
                                        <td><label class="switch">
                                            <input onchange="update_banner_published(this)" value="{{ $banner->id }}" type="checkbox" <?php if($banner->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>{{ $banner->displayposition }}</td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_home_banner_3({{ $banner->id }})">{{__('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('home_banners.destroy', $banner->id)}}');">{{__('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-5" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-6">
                        <a onclick="add_home_category()" class="btn btn-rounded btn-info pull-right">{{__('Add New Category')}}</a>
                    </div>
                    
                    <div class="col-sm-6">
                        <a onclick="add_home_categoryConfig()" class="btn btn-rounded btn-info pull-right">{{__('Add New Home Category configuration with Position')}}</a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Home Categories')}}</h3>
                    </div>
                    <div class="panel-body">
<div class="table-responsive">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Category Display Name')}}</th>
                                    <th>{{__('Category')}}</th>
                                    <th>{{__('Products')}}</th>
                                    <th>{{__('Position')}}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th width="10%">{{__('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\HomeCategory::all() as $key => $home_category)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$home_category->display_title}}</td>
                                        <td>{{$home_category->category->name}}</td>
                                        <td>
                                            @foreach (json_decode($home_category->products) as $key => $product_id)
                                                @if (\App\Product::find($product_id) != null)
                                                    <span class="badge badge-info">{{\App\Product::find($product_id)->name}}</span>
                                                @endif
                                            @endforeach
                                        </td>
										<td>{{ $home_category->position }}</td>
                                        <td><label class="switch">
                                            <input onchange="update_home_category_status(this)" value="{{ $home_category->id }}" type="checkbox" <?php if($home_category->status == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_home_category({{ $home_category->id }})">{{__('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('home_categories.destroy', $home_category->id)}}');">{{__('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
</div>
                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-6" class="tab-pane fade">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Top 20 Information')}}</h3>
                    </div>

                    <!--Horizontal Form-->
                    <!--===================================================-->
                    <form class="form-horizontal" action="{{ route('top_10_settings.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3" for="url">{{__('Top Categories (Max 20)')}}</label>
                                <div class="col-sm-9">
                                    <select class="form-control demo-select2-max-10" name="top_categories[]" multiple required>
                                        @foreach (\App\Category::all() as $key => $category)
                                            <option value="{{ $category->id }}" @if($category->top == 1) selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                           <div class="form-group" style="display:none">
                                <label class="col-sm-3" for="url">{{__('Top Brands (Max 20)')}}</label>
                                <div class="col-sm-9">
                                    <select class="form-control demo-select2-max-10" name="top_brands[]" multiple required>
                                        @foreach (\App\Brand::all() as $key => $brand)
                                            <option value="{{ $brand->id }}" @if($brand->top == 1) selected @endif>{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-right">
                            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
                        </div>
                    </form>
                    <!--===================================================-->
                    <!--End Horizontal Form-->

                </div>
            </div>
			<div id="demo-lft-tab-7" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_brand_banner()" class="btn btn-rounded btn-info pull-right">{{__('Add New Brand Banner')}}</a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Home Brand Banner')}}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Photo')}}</th>
                                    <th width="50%">{{__('Link')}}</th>
                                    <th>{{__('Published')}}</th>
                                    <th width="10%">{{__('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Brandbanner::all() as $key => $Bbanner)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td><img loading="lazy"  class="img-md" src="{{ asset($Bbanner->photo)}}" alt="Slider Image"></td>
                                        <td>{{$Bbanner->link}}</td>
                                        <td><label class="switch">
                                            <input onchange="update_Bbanner_published(this)" value="{{ $Bbanner->id }}" type="checkbox" <?php if($Bbanner->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
													<li><a onclick="edit_brandbanner({{ $Bbanner->id }})">{{__('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('brandbanner.destroy', $Bbanner->id)}}');">{{__('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div id="demo-lft-tab-8" class="tab-pane fade">
				<div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_popup_offer()" class="btn btn-rounded btn-info pull-right">{{__('Add offer')}}</a>
                    </div>
                </div>
                <br>
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Popup Offer')}}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Photo')}}</th>
                                    <th>{{__('URL')}}</th>
                                    <th>{{__('Published')}}</th>
                                    <th width="10%">{{__('Options')}}</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach(\App\popup_offer::all() as $key => $offer)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td><img loading="lazy"  class="img-md" src="{{ asset($offer->photo)}}" alt="Popup Offer Image"></td>
                                        <td>{{$offer->url}}</td>
                                        <td><label class="switch">
                                            <input onchange="update_popup_offer_published(this)" value="{{ $offer->id }}" type="checkbox" <?php if($offer->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_popup_offer({{ $offer->id }})">{{__('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('popup_offer.destroy', $offer->id)}}');">{{__('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            
            <div id="demo-lft-tab-9" class="tab-pane fade">
                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_best_selling()" class="btn btn-rounded btn-info pull-right">{{__('Add New Product')}}</a>
                    </div>
                </div>
                <br>
                
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{__('Best Selling Products')}}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Category')}}</th>
                                    <th>{{__('Products')}}</th>
                                    <!--<th>{{__('Position')}}</th>-->
                                    <th>{{ __('Status') }}</th>
                                    <th width="10%">{{__('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\BestSelling::all() as $key => $best_selling)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$best_selling->category->name}}</td>
                                        <td>
                                            @foreach (json_decode($best_selling->products) as $key => $product_id)
                                                @if (\App\Product::find($product_id) != null)
                                                    <span class="badge badge-info">{{\App\Product::find($product_id)->name}}</span>
                                                @endif
                                            @endforeach
                                        </td>
										<!--<td>{{ $best_selling->position }}</td>-->
                                        <td><label class="switch">
                                            <input onchange="update_best_selling_status(this)" value="{{ $best_selling->id }}" type="checkbox" <?php if($best_selling->status == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_best_selling({{ $best_selling->id }})">{{__('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('best_selling.destroy', $best_selling->id)}}');">{{__('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
	   </div>
    </div>
@endsection
@section('script')

<script type="text/javascript">

    function updateSettings(el, type){
        if($(el).is(':checked')){
            var value = 1;
        }
        else{
            var value = 0;
        }
        $.post('{{ route('business_settings.update.activation') }}', {_token:'{{ csrf_token() }}', type:type, value:value}, function(data){
            if(data == 1){
                showAlert('success', 'Settings updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }
	
	function add_menu(){
        $.get('{{ route('menu.create')}}', {}, function(data){
            $('#demo-lft-tab-1').html(data);
        });
    }
	
	function edit_menu(id){
        var url = '{{ route("menu.edit", "menu_id") }}';
        url = url.replace('menu_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-1').html(data);
        });
    }


    function add_slider(){
        $.get('{{ route('sliders.create')}}', {}, function(data){
            $('#demo-lft-tab-2').html(data);
        });
    }
	function edit_slider(id){
        var url = '{{ route("sliders.edit", "home_slider_id") }}';
        url = url.replace('home_slider_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-2').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
    function update_slider_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
       // var url = '{{ route('sliders.update_status', 'slider_id') }}';
        //url = url.replace('slider_id', el.value);
		$.post('{{ route('sliders.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
			
        //$.post(url, {_token:'{{ csrf_token() }}', status:status, _method:'PATCH'}, function(data){
            if(data == 1){
                showAlert('success', 'Published sliders updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }
	function add_brand_banner(){
        $.get('{{ route('brandbanner.create')}}', {}, function(data){
            $('#demo-lft-tab-7').html(data);
        });
    }
	
	function edit_brandbanner(id){
        var url = '{{ route("brandbanner.edit", "Brandbanner") }}';
        url = url.replace('Brandbanner', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-7').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
	
	function update_Bbanner_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('{{ route('brandbanner.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Brand Banner status updated successfully');
            }
            else{
                showAlert('danger', 'There is some error occurred. Please try again.');
            }
        });
    }
	
	/*Start popup offer*/
	function add_popup_offer(){
        $.get('{{ route('popup_offer.create')}}', {}, function(data){
            $('#demo-lft-tab-8').html(data);
        });
    }
	function edit_popup_offer(id){
        var url = '{{ route("popup_offer.edit", "popup_offer_id") }}';
        url = url.replace('popup_offer_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-8').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
	function update_popup_offer_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('{{ route('popup_offer.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Popup Offer status updated successfully');
            }
            else{
                showAlert('danger', 'There is some error occurred. Please try again.');
            }
        });
    }
	/*End popup offer*/

	function add_banner_1(){
        $.get('{{ route('home_banners.create', 1)}}', {}, function(data){
            $('#demo-lft-tab-3').html(data);
        });
    }

    function add_banner_2(){
        $.get('{{ route('home_banners.create', 2)}}', {}, function(data){
            $('#demo-lft-tab-4').html(data);
        });
    }
    function add_banner_3(){
        $.get('{{ route('home_banners.create', 3)}}', {}, function(data){
            $('#demo-lft-tab-10').html(data);
        });
    }
   
    function edit_home_banner_1(id){
        var url = '{{ route("home_banners.edit", "home_banner_id") }}';
        url = url.replace('home_banner_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-3').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
	

    function edit_home_banner_2(id){
        var url = '{{ route("home_banners.edit", "home_banner_id") }}';
        url = url.replace('home_banner_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-4').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
    
    function edit_home_banner_3(id){
        var url = '{{ route("home_banners.edit", "home_banner_id") }}';
        url = url.replace('home_banner_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-10').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
    
    function add_home_category(){
        $.get('{{ route('home_categories.create')}}', {}, function(data){
            $('#demo-lft-tab-5').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
    
    function add_home_categoryConfig(){
        $.get('{{ route('home_categories.create')}}', {'config':true}, function(data){
            $('#demo-lft-tab-5').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

    function edit_home_category(id){
        var url = '{{ route("home_categories.edit", "home_category_id") }}';
        url = url.replace('home_category_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-5').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

    function update_home_category_status(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('{{ route('home_categories.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Home Page Category status updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }

    function update_banner_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('{{ route('home_banners.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Banner status updated successfully');
                location.reload();
            }
            else{
                showAlert('danger', 'Maximum 4 banners to be published.');
            }
        });
    }
	

    function update_menu_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('{{ route('menu.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Published Menu updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }
	
    
    function add_best_selling(){
        $.get('{{ route('best_selling.create')}}', {}, function(data){
            $('#demo-lft-tab-9').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
	
	function edit_best_selling(id){
        var url = '{{ route("best_selling.edit", "best_selling_id") }}';
        url = url.replace('best_selling_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-9').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

	function update_best_selling_status(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('{{ route('best_selling.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Best Selling Category status updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }

</script>

@endsection
