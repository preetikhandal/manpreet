<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
   	<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"/>
    <!--<![endif]-->
	<title>Alde Bazaar</title>
	<!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
	
<style>
    table tr td, table { text-align: left; }
</style>
	
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
			<td align="left" valign="top">
			
				<!-- Header -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						<td align="left" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="img-center" style="padding: 45px 15px; font-size:0pt; line-height:0pt; text-align:center;">
									<a href="{{url('/')}}" target="_blank"><img src="{{asset('email/logo_2.png')}}" width="225" border="0" alt="" /></a></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Header -->

				<!-- Nav -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						<td align="left" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="5" class="mobile-shell" bgcolor="#ffffff">
								<tr>
									<td class="text-nav fw-medium" style="padding: 20px 15px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:23px; line-height:21px; text-align:center; font-weight:500;">
										 <span class="link-2" style="color:#4a4a4a; text-decoration:none;">Your question and answers </span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Nav -->
        <div class="form-group">
            <label for="email">Name: {{$data['name']}}</label>
        </div>
        <div class="form-group">
            <label for="email">Email:{{$data['email']}}</label>
        </div>
       <p><strong>1.Considering your complete experience with our company, how likely would you be to recommend us to a friend or colleague ?</strong></p>
	   <strong>Answer:- </strong>{{$data['q1']}}
       <p><strong>2. How satisfied do you feel based on your overall experience ?</strong></p>
       <strong>Answer:-</strong> {{$data['q2']}}
       <p><strong>3. Please let us know how can we improve your experience ?</strong></p>
       <strong>Answer:-</strong> {{$data['q3']}}
       <p><strong>4. How did you learn about our website?</strong></p>
       <strong>Answer:- </strong>{{$data['q4']}}
       <p><strong>5. What other products would you like to see in our online store (City Haat)?</strong></p>
       <strong>Answer:- </strong>{{$data['q5']}}
    
       <p><strong>6. How satisfied are you with the quality of products ?</strong></p>
       <strong>Answer:-</strong> {{$data['q6']}}
       
        <p><strong>7. How was the checkout experience overall?</strong></p>
        <strong>Answer:-</strong> {{$data['q7']}}
        <p><strong>8. Did you experience a hassle-free payment experience?</strong></p>
        <strong>Answer:-</strong> {{$data['q8']}}
        <p><strong>9. On a scale of 0-10, how likely are you to buy from us again?</strong></p>
        <strong>Answer:- </strong>{{$data['q9']}}
        <p><strong>10. Did you receive your product within the expected timeline?</strong></p>
        <strong>Answer:- </strong>{{$data['q10']}}
        <p><strong>11. How helpful was the customer support staff?</strong></p>
        <strong>Answer:- </strong>{{$data['q12']}}
      </body>
</html>
