@php 
$order = $array['order'];
@endphp
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
   	<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"/>
    <!--<![endif]-->
	<title>Alde Bazaar</title>
	<!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
	

	<style type="text/css" media="screen">
table{border:0px solid black;}
		/* Linked Styles */
		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none }
		a { color:#66b7f0; text-decoration:none }
		p { padding:0 !important; margin:0 !important } 
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }
		
		/* Mobile styles */
		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			.mobile-shell { width: 100% !important; min-width: 100% !important; }
			
			.m-center { text-align: center !important; }
			
			.center { margin: 0 auto !important; }
			
			.td { width: 100% !important; min-width: 100% !important; }

			.m-br-3 { height: 3px !important; }
			.m-br-4 { height: 4px !important; background: #f4f4f4 !important; }
			.m-br-15 { height: 15px !important; }
			.m-br-25 { height: 25px !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

			.column-top,
			.column { float: left !important; width: 100% !important; display: block !important; }

			.content-spacing { width: 15px !important; }

			.m-bg { display: block !important; width: 100% !important; height: auto !important; background-position: center center !important; }

			.h-auto { height: auto !important; }

			.plr-15 { padding-left: 15px !important; padding-right: 15px !important; }

			.w-2 { width: 2% !important; }
			.w-49 { width: 49% !important; }

			.pb-4 { padding-bottom: 4px !important; }
			.pb-15 { padding-bottom: 15px !important; }

			.pt-0 { padding-top: 0 !important; }

			.h1,
			.h1-white { font-size: 20px !important; line-height: 28px !important; }
			.h2 { font-size: 18px !important; line-height: 28px !important; }

			.text-btn-large,
			.text-btn-large-white { padding: 15px 25px !important; }
			.text-btn,
			.text-btn-1,
			.text-btn-1-white { padding: 12px 15px !important; }
		}
	</style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
			<td align="center" valign="top">
			
				<!-- Header -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						<td align="center" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="img-center" style="padding: 45px 15px; font-size:0pt; line-height:0pt; text-align:center;">
									<a href="{{url('/')}}" target="_blank"><img src="{{asset('email/logo_2.png')}}" width="225" border="0" alt="" /></a></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Header -->

				<!-- Nav -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						<td align="center" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="5" class="mobile-shell" bgcolor="#ffffff">
								<tr>
									<td class="text-nav fw-medium" style="padding:20px 15px 0 15px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:23px; line-height:21px; text-align:center; font-weight:500;">
										 <span class="link-2" style="color:#4a4a4a; text-decoration:none;">Order Confirmation</span>
									</td>
								</tr>
								<tr>
									<td class="text-nav fw-medium" style="padding-bottom: 20px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:16px; text-align:center; font-weight:500;">
										 <span style="color:#4a4a4a; text-decoration:none;">{{__('Order Number:')}} {{ $order->code }}</span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Nav -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="min-height:100px;">
					<tr>
						<td align="center" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell" bgcolor="#ffffff">
								<tr>
                                    <td class="h1 fw-medium" style="padding-right:15px;padding-bottom: 10px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:15px; line-height:30px; text-align:left; font-weight:500;">
                                        Dear  @if ($order->user_id != null) {{ $order->user->name }} @else Guest ({{ $order->guest_id }}) @endif
                                    </td>
                                </tr>

                                <tr>
                                    <td class="text-1" style="padding-right:15px;padding-bottom: 30px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:16px; line-height:30px; text-align:justify;">
                                        We are preparing your order. We are trying our best to deliver your order at the earliest. However, due to the current restrictions, deliveries are likely to take longer than usual. Thank you for your patience and understanding.
                                    </td>
                                </tr>
							
							

							</table>
						</td>
					</tr>
				</table>
                <!-- Main -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<!-- Section - 2 Cols -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
								<tr>
									<td align="center" valign="top">
										<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
											<tr>
												<td class="plr-15" style="padding-bottom: 50px;">
													<!-- Section Title -->
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="h3 fw-medium" style="padding-bottom: 20px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:18px; line-height:26px; text-align:Left; font-weight:500;">
																Your Order Details :-
																<br/>
																Order No.:- {{$order->code}}
															</td>
															<td>Track Your Order click this link:- <a href="{{url('track_your_order')}}">Track Order</a></td>
														</tr>

													</table>
													<!-- END Section Title -->
												
													<!-- 2 Cols -->
													<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
														<tr>
															<td style="padding: 10px 10px 0 10px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr mc:repeatable>
																		<td style="padding-bottom: 10px;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                               
                                                                                    @php $schema = ''; 
                                                                                    $orderCount = $order->orderDetails->count();
                                                                                    @endphp
                                                                                     
																				    @php $n = 1; @endphp
                                                                                    @foreach($order->orderDetails as $OD)
                                                                                    @if($n == 1)
                                                                                        <tr>
                                                                                    @endif
                                                                                    
																				    @php 
																				    $photo = json_decode($OD->product->photos);
																				    if(isset($photo[0])) {
																				    $photo = $photo[0];
																				    } else {
																				    $photo = 'frontend/images/placeholder.jpg';
																				    }
																				    
                                                                    			    $home_base_price = home_base_price($OD->product->id);
                                                                    			    $home_discounted_base_price = home_discounted_base_price($OD->product->id);
																				    
                                                                                    $schema .= '<div itemprop="acceptedOffer" itemscope itemtype="http://schema.org/Offer">';
                                                                                    $schema .= '<div itemprop="itemOffered" itemscope itemtype="http://schema.org/Product">';
                                                                                    $schema .= '<meta itemprop="name" content="'.$OD->product->name.'"/>';
                                                                                    $schema .= '<link itemprop="url" href="'.route('product',$OD->product->slug).'"/>';
                                                                                    $schema .= '<link itemprop="image" href="'.asset($photo).'"/>';
                                                                                    $schema .= '<meta itemprop="sku" content="'.$OD->product->product_id.'"/>';
                                                                                    $schema .= '</div>';
                                                                                    $schema .= '<meta itemprop="price" content="'.trim(str_replace('₹','', $home_discounted_base_price)).'"/>';
                                                                                    $schema .= '<meta itemprop="priceCurrency" content="INR"/>';
                                                                                    $schema .= '<div itemprop="eligibleQuantity" itemscope itemtype="http://schema.org/QuantitativeValue">';
                                                                                    $schema .= '<meta itemprop="value" content="'.$OD->quantity.'"/>';
                                                                                    $schema .= '</div>';
                                                                                    $schema .= '</div>';
                                                                                      @endphp
																					<th class="column-top" @if($orderCount == 1) colspan="2" @endif valign="top" width="310" bgcolor="#ffffff" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;border:1px solid #dcdcdc">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="plr-15" style="padding: 50px 15px;">
																									<table width="100%" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="fluid-img centered" style="padding-bottom: 25px; font-size:0pt; line-height:0pt; text-align:center;"><a href="#" target="_blank"><img src="{{asset($photo)}}" width="260" border="0" alt="" /></a></td>
																										</tr>
																										<tr>
																											<td class="h3 fw-light" style="padding-bottom: 15px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:16px; line-height:20px; text-align:center; font-weight:300;">
																												{{$OD->product->name}}
																											</td>
																										</tr>
																										<tr>
																											<td class="h4-2 fw-semibold" style="padding-bottom: 20px; color:#979797; font-family:'Poppins', Arial,sans-serif; font-size:16px; line-height:20px; text-align:center; font-weight:600;">
																												Product Price &nbsp; <span class="link" style="color:#66b7f0; text-decoration:none;"><del style="color:red">₹{{$OD->discount +$OD->price + $OD->tax + $OD->shipping_cost}}</del> ₹{{$OD->price + $OD->tax + $OD->shipping_cost}}</span>
																											</td>
																										</tr>
																										<tr>
																											<td class="h4-2 fw-semibold" style="padding-bottom: 20px; color:#979797; font-family:'Poppins', Arial,sans-serif; font-size:16px; line-height:20px; text-align:center; font-weight:600;">
																												Qty &nbsp; <span class="link" style="color:#66b7f0; text-decoration:none;">{{$OD->quantity}}</span>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
                                                                                    @if($n == 2)
                                                                                    </tr>
                                                                                    @endif
                                                                                
                                                                                    @php
                                                                                    $n++;
                                                                                    if($n > 2) {
                                                                                            $n = 1;
                                                                                    }
                                                                                    $orderCount--;
                                                                                    @endphp
                                                                                    @endforeach
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<!-- END 2 Cols -->
												</td>
											</tr>
											<tr>
												<td class="plr-15" style="padding-bottom: 50px;">
													<!-- 2 Cols -->
													<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
														<tr>
															<td style="padding: 10px 10px 0 10px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr mc:repeatable>
																		<td style="padding-bottom: 10px;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-top" valign="top" width="310" bgcolor="#ffffff" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="plr-15" style="padding: 50px 15px;">
																									<table width="100%" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="fluid-img centered" style="padding-bottom: 25px; font-size:20px; line-height:0pt; text-align:Left;">
																											
																											Delivery Address :</td>
																										</tr>
                                                                                                        @php
                                                                                                            $shipping_address = json_decode($order->shipping_address);
                                                                                                        @endphp
																										<tr>
																											<td class="h3 fw-light" style="padding-bottom: 15px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:17px; line-height:26px; text-align:Justify; font-weight:300;">
																												{{ $shipping_address->address }}<br />
                                                                                                                {{ $shipping_address->city }}, {{ $shipping_address->country }}
																												
																											</td>
																										</tr>
																										
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-top" valign="top" width="10" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;"><div style="font-size:0pt; line-height:0pt;" class="m-br-10"></div></th>
																					<th class="column-top" valign="top" width="310" bgcolor="#ffffff" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="plr-15" style="padding: 50px 15px;">
																									<table width="100%" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="fluid-img centered" style="padding-bottom: 25px; font-size:20px; line-height:0pt; text-align:Left;">
																											
																											Belling Details :</td>
																										</tr>
																										<tr>
																											<td class="h3 fw-light" style="padding-bottom: 15px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:17px; line-height:26px; text-align:Justify; font-weight:300;">
																											Total :	₹{{$order->grand_total}}<br/>

																												
																											</td>
																										</tr>
																										<tr>
																											<td class="h4-2 fw-semibold" style="padding-bottom: 10px; color:#979797; font-family:'Poppins', Arial,sans-serif; font-size:17px; line-height:24px; text-align:Justify; font-weight:600;">
																												Mode of Payment	{{ucfirst(str_ireplace("_", " ", $order->payment_type))}}
																											</td>
																										</tr>
																										
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<!-- END 2 Cols -->
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END Section - 2 Cols -->
						</td>
					</tr>

		
				</table>
				<!-- END Main -->
				<!-- Footer -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="plr-15" style="padding: 40px 30px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<th class="column-top" valign="top" width="225" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
													<div class="img-center" style="font-size:0pt; line-height:0pt; text-align:center;"><a href="{{url('/')}}" target="_blank"><img src="{{asset('email/logo_2.png')}}" border="0" width="225" alt="" /></a></div>
												</th>
												<th class="column-top" valign="top" width="15" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;"><div style="font-size:0pt; line-height:0pt;" class="m-br-15"></div>
</th>
												<th class="column-top" valign="top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td align="right" style="padding: 15px 0 25px 0;">
																<!-- Socials -->
																<table border="0" cellspacing="0" cellpadding="0" class="center">
																	<tr>
																		<td class="img" width="11" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://www.facebook.com/Alde-Bazaar-100442855141856/" target="_blank"><img src="{{asset('email/ico_facebook_light.png')}}" width="11" height="22" border="0" alt="" /></a></td>
																		<td class="img" width="40" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																		<td class="img" width="23" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://twitter.com/AldeBazaar" target="_blank"><img src="{{asset('email/ico_twitter_light.png')}}" width="23" height="22" border="0" alt="" /></a></td>
																		<td class="img" width="36" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																		<td class="img" width="22" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://www.instagram.com/aldebazaar/" target="_blank"><img src="{{asset('email/ico_instagram_light.png')}}" width="22" height="22" border="0" alt="" /></a></td>
																	</tr>
																</table>
																<!-- END Socials -->
															</td>
														</tr>
														
													</table>
												</th>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Footer -->

				
			</td>
		</tr>
	</table>
	<div itemscope itemtype="http://schema.org/Order">
      <div itemprop="merchant" itemscope itemtype="http://schema.org/Organization">
        <meta itemprop="name" content="Aldebazaar.com"/>
      </div>
      <link itemprop="orderStatus" href="http://schema.org/OrderProcessing"/>
      <meta itemprop="orderNumber" content="{{$order->code}}"/>
      <meta itemprop="priceCurrency" content="INR"/>
      <meta itemprop="price" content="{{$order->grand_total}}"/>
      <div itemprop="priceSpecification" itemscope itemtype="http://schema.org/PriceSpecification">
        <meta itemprop="price" content="{{$order->grand_total}}"/>
        <meta itemprop="priceCurrency" content="INR"/>
      </div>
      {!! $schema !!}
      <link itemprop="url" href="{{url('purchase_history')}}"/>
      <div itemprop="potentialAction" itemscope itemtype="http://schema.org/ViewAction">
        <link itemprop="url" href="{{url('purchase_history')}}"/>
      </div>
  </div>
                                                                                  
</body>
</html>
