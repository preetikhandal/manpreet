<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Menu Information')}}</h3>
    </div>

    <!--Horizontal Form-->
    <!--===================================================-->
    <form class="form-horizontal" action="{{ route('menu.update', $menu->id) }}" method="POST">
        @csrf
        <input type="hidden" name="_method" value="PATCH">
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3" for="url">{{__('URL')}}</label>
                <div class="col-sm-9">
                    <input type="text" placeholder="{{__('URL')}}" id="url" name="url" value="{{ $menu->link }}" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label">{{__('Name')}}</label>
                </div>
                <div class="col-sm-9">
                   <input type="text" placeholder="{{__('Name')}}" id="menu_name" name="menu_name" value="{{ $menu->name }}" class="form-control" required>
                </div>
            </div>
			@php
				$dbposition=\App\Menu::get();
				$allposiotns=array();
				foreach($dbposition as $value){
						array_push($allposiotns,$value->position);
				}						
			@endphp
			<div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label">{{__('Position')}}</label>
                </div>
                <div class="col-sm-9">
                    <select id="menu_position" name="menu_position" placeholder="Menu Position" class="form-control" required>
						<option value="{{$menu->position}}" selected >{{$menu->position}}</option>
						@for($i=1;$i<=5;$i++)
							@if(!in_array($i,$allposiotns))
								<option value="{{$i}}" >{{$i}}</option>
							@endif
						@endfor
					<select>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
        </div>
    </form>
    <!--===================================================-->
    <!--End Horizontal Form-->

</div>
