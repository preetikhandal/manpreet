<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Menu Information')}}</h3>
    </div>

    <!--Horizontal Form-->
    <!--===================================================-->
    <form class="form-horizontal" action="{{ route('menu.store') }}" method="POST">
        @csrf
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3" for="url">{{__('URL')}}</label>
                <div class="col-sm-9">
                    <input type="text" id="url" name="url" placeholder="http://example.com/" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label">{{__('Menu Name')}}</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" id="menu_name" name="menu_name" placeholder="Menu Name" class="form-control" required>
                </div>
            </div>
			@php
				$dbposition=\App\Menu::get();
				$allposiotns=array();
				foreach($dbposition as $value){
						array_push($allposiotns,$value->position);
				}						
			@endphp
			<div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label">{{__('Position')}}</label>
                </div>
                <div class="col-sm-9">
                    <select id="menu_position" name="menu_position" placeholder="Menu Position" class="form-control" required>
						@for($i=1;$i<=5;$i++)
							@if(!in_array($i,$allposiotns))
								<option value="{{$i}}">{{$i}}</option>
							@endif
						@endfor
					<select>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
        </div>
    </form>
    <!--===================================================-->
    <!--End Horizontal Form-->

</div>
