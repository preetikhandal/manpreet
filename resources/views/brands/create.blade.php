@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-lg-8 col-lg-offset-2">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Brand Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('brands.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="logo">{{__('Logo')}} </label>
                    <div class="col-sm-10">
                        <input type="file" id="logo" name="logo" class="form-control">
						<small class="text-danger">Logo size 240px*120px</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="position">{{__('Discount (%)')}}</label>
                    <div class="col-sm-10">
                        <input type="text" min=0 value="0" class="form-control" data-toggle="tooltip" data-placement="top" title="Please Type ND in capital latter" name="brand_discount" placeholder="{{__('Brand Discount')}}">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Title')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="meta_title" placeholder="{{__('Meta Title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Description')}}</label>
                    <div class="col-sm-10">
                        <textarea name="meta_description" rows="8" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Keywords')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="tags[]" placeholder="Type to add a tag" data-role="tagsinput">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
@section('script')

<script type="text/javascript">
	window.URL = window.URL || window.webkitURL;
	$("form").submit( function( e ) {
		var form = this;
		e.preventDefault(); //Stop the submit for now
		var fileInput = $(this).find("input[type=file]")[0],
			file = fileInput.files && fileInput.files[0];

		if( file ) {
			var img = new Image();

			img.src = window.URL.createObjectURL( file );

			img.onload = function() {
				var width = img.naturalWidth,
					height = img.naturalHeight;

				window.URL.revokeObjectURL( img.src );

				if( width <= 240 && height <= 120 ) {
					form.submit();
				}
				else {
					alert("Please Upload Given Size of Logo.");
				}
			};
		}
		else { //No file was input or browser doesn't support client side reading
			form.submit();
		}

	});

</script>


@endsection