@extends('layouts.app')
		@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
@section('content')
<div class="row">
	<form class="form form-horizontal mar-top" action="{{route('products.stock_qty')}}" method="POST" enctype="multipart/form-data" id="choice_form">
		<input name="_method" type="hidden" value="POST">
		
		@csrf
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">{{__('Product Information')}}</h3>
			</div>
				<div class="panel-body">

						<div id="demo-stk-lft-tab-1" class="tab-pane fade active in">
							<div class="form-group" id="category">
	                            <label class="col-lg-2 control-label">{{__('User Type')}}</label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="user_type" id="user_type" required>
	                                	
										<option value="all_user" @if(old('user_type')=="all_user") selected @endif>All User</option>
										<option value="in_house" @if(old('user_type')=="in_house") selected @endif>In House User</option>
	                                	@foreach($user as $users)
	                                	    <option value="{{$users->id}}" @if(old('user_type')==$users->id) selected @endif>{{__($users->name)}}</option>
	                                	@endforeach
	                                </select>
	                            </div>
	                        </div>
							<div class="form-group" id="category">
	                            <label class="col-lg-2 control-label">{{__('Category')}}</label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
	                                    <option value=""> Select Category </option>
	                                	@foreach($categories as $category)
	                                	    <option value="{{$category->id}}" @if(old('category_id')==$category->id) selected @endif>{{__($category->name)}}</option>
	                                	@endforeach
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group" id="subcategory">
	                            <label class="col-lg-2 control-label">{{__('Subcategory')}}</label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id" required>

	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group" id="subsubcategory">
	                            <label class="col-lg-2 control-label">{{__('Sub Subcategory')}}</label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id">

	                                </select>
	                            </div>
	                        </div>
	                        
	                        <div class="form-group">
	                            <label class="col-lg-2 control-label">{{__('Quantity')}}</label>
	                            <div class="col-lg-7">
	                                <input type="number" class="form-control" name="qty" value="{{old('qty')}}" placeholder="Quantity"  required>
	                            </div>
	                        </div>
	                        
							
				        </div>
				
				
				</div>
				<div class="panel-footer text-right">
					<button type="submit" name="button" class="btn btn-purple">{{ __('Save') }}</button>
				</div>
		</div>
	</form>
</div>
@endsection

@section('script')

<script type="text/javascript">

	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
		    $('#subcategory_id').append($('<option>', {
				value: 'all_sub_cat',
				text: 'All Sub Categories'
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
			$('#subsubcategory_id').append($('<option>', {
				value: 'all_sub_sub_cat',
				text: 'All Sub Sub Categories'
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		});
	}


	$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

</script>

@endsection
