@extends('layouts.app')

@section('content')
<div class="row">
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
	<form class="form form-horizontal mar-top" action="{{route('products.cat_export')}}" method="POST"  id="choice_form">
		<input name="_method" type="hidden" value="POST">
		
		@csrf
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">{{__('Product Information')}}</h3>
			</div>
				<div class="panel-body">

						<div id="demo-stk-lft-tab-1" class="tab-pane fade active in">
							<div class="form-group">
	                            <label class="col-lg-2 control-label">{{__('Marg Code')}}</label>
	                            <div class="col-lg-7">
	                                 <select class="form-control demo-select2-placeholder" name="marg" id="marg">
										
	                                	    <option value="0" >{{__('All Product')}}</option>
	                                	    <option value="1" >{{__('In Marg')}}</option>
	                                	    <option value="2" >{{__('Not In Marg ')}}</option>
	                                	
	                                </select>
	                            </div>
	                        </div>
							<div class="form-group">
	                            <label class="col-lg-2 control-label">{{__('Brand Name')}}</label>
	                            <div class="col-lg-7">
	                                 <select class="form-control demo-select2-placeholder" name="brand_id" id="brand_id">
									 <option value="0" >{{__('All Brands')}}</option>
										@foreach($brands as $brand)
	                                	    <option value="{{$brand->id}}" >{{__($brand->name)}}</option>
	                                	@endforeach
	                                </select>
	                            </div>
	                        </div>
							<div class="form-group" id="category">
	                            <label class="col-lg-2 control-label">{{__('Category')}}</label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
	                                	<option value="0" >{{__('All Category')}}</option>
	                                	@foreach($categories as $category)
	                                	    <option value="{{$category->id}}" >{{__($category->name)}}</option>
	                                	@endforeach
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group" id="subcategory">
	                            <label class="col-lg-2 control-label">{{__('Subcategory')}}</label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id" required>
										<option value="0" >{{__('All Subcategory')}}</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group" id="subsubcategory">
	                            <label class="col-lg-2 control-label">{{__('Sub Subcategory')}}</label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id" required>
										<option value="0" >{{__('All Sub Subcategory')}}</option>
	                                </select>
	                            </div>
	                        </div>
	                    </div>
				
				
				</div>
				<div class="panel-footer text-right">
					<button type="submit" name="button" class="btn btn-purple">{{ __('Export') }}</button>
				</div>
		</div>
	</form>
</div>
@endsection

@section('script')

<script type="text/javascript">

	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
			
			$('#subcategory_id').append($('<option>', {
				value: '0',
				text: 'All Subcategory'
			}));
			
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		   /* $("#subcategory_id > option").each(function() {
		        
		            $("#subcategory_id").val(this.value).change();
		      
		    });
*/
		 //   $('.demo-select2').select2();

		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
			$('#subsubcategory_id').append($('<option>', {
				value: '0',
				text: 'All Sub Subcategory'
			}));
			
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
			/*
		    $("#subsubcategory_id > option").each(function() {
		      
		            $("#subsubcategory_id").val(this.value).change();
		       
		    });
*/
		 //   $('.demo-select2').select2();

		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}


	$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

</script>

@endsection
