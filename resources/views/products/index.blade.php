@extends('layouts.app')

@section('content')

@if($type != 'Seller')
    <div class="row">
        <div class="col-lg-12 pull-right">
            <a href="{{ route('products.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Product')}}</a>
        </div>
    </div>
@endif

<br>

<div class="panel">
    <!--Panel heading-->
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{ __($type.' Products') }}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_products" action="" method="GET">
                @if($type == 'Seller')
                    <div class="box-inline pad-rgt pull-left">
                        <div class="select" style="min-width: 200px;">
                            <select class="form-control demo-select2" id="user_id" name="user_id" onchange="sort_products()">
                                <option value="">All Sellers</option>
                                @foreach (App\Seller::all() as $key => $seller)
                                    @if ($seller->user != null && $seller->user->shop != null)
                                        <option value="{{ $seller->user->id }}" @if ($seller->user->id == $seller_id) selected @endif>{{ $seller->user->shop->name }} ({{ $seller->user->name }})</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                 <div class="box-inline pad-rgt pull-left">
                        <div class="select" style="min-width: 200px;">
                            <select class="form-control demo-select2" id="trashed" name="trashed" onchange="sort_products()">
                                <option value="0" @if ($trashed == 0) selected @endif>Without Trashed</option>
                                <option value="1" @if ($trashed == 1) selected @endif>Trashed Items</option>
                                
                            </select>
                        </div>
                    </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 200px;">
                        <select class="form-control demo-select2" name="type" id="type" onchange="sort_products()">
                            <option value="">Sort by</option>
                            <option value="name,asc" @isset($col_name , $query) @if($col_name == 'name' && $query == 'asc') selected @endif @endisset>{{__('Name Assending (123)')}}</option>
                            <option value="name,desc" @isset($col_name , $query) @if($col_name == 'name' && $query == 'desc') selected @endif @endisset>{{__('Name Descending (321)')}}</option>
                            <option value="product_id,asc" @isset($col_name , $query) @if($col_name == 'product_id' && $query == 'asc') selected @endif @endisset>{{__('SKU Assending (123)')}}</option>
                            <option value="product_id,desc" @isset($col_name , $query) @if($col_name == 'product_id' && $query == 'desc') selected @endif @endisset>{{__('SKU Descending (321)')}}</option>
                            <option value="current_stock,asc" @isset($col_name , $query) @if($col_name == 'current_stock' && $query == 'asc') selected @endif @endisset>{{__('Stock Assending (123)')}}</option>
                            <option value="current_stock,desc" @isset($col_name , $query) @if($col_name == 'current_stock' && $query == 'desc') selected @endif @endisset>{{__('Stock Descending (321)')}}</option>
                            <option value="rating,desc" @isset($col_name , $query) @if($col_name == 'rating' && $query == 'desc') selected @endif @endisset>{{__('Rating (High > Low)')}}</option>
                            <option value="rating,asc" @isset($col_name , $query) @if($col_name == 'rating' && $query == 'asc') selected @endif @endisset>{{__('Rating (Low > High)')}}</option>
                            <option value="num_of_sale,desc"@isset($col_name , $query) @if($col_name == 'num_of_sale' && $query == 'desc') selected @endif @endisset>{{__('Num of Sale (High > Low)')}}</option>
                            <option value="num_of_sale,asc"@isset($col_name , $query) @if($col_name == 'num_of_sale' && $query == 'asc') selected @endif @endisset>{{__('Num of Sale (Low > High)')}}</option>
                            <option value="unit_price,desc"@isset($col_name , $query) @if($col_name == 'unit_price' && $query == 'desc') selected @endif @endisset>{{__('Base Price (High > Low)')}}</option>
                            <option value="unit_price,asc"@isset($col_name , $query) @if($col_name == 'unit_price' && $query == 'asc') selected @endif @endisset>{{__('Base Price (Low > High)')}}</option>
                        </select>
                    </div>
                </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Align Book Id')}}</th>
                    <th width="20%">
                    @if(isset($col_name , $query))
                         @if($col_name == 'name' && $query == 'asc')
                        <a href="javascript:$('#type').val('name,desc');sort_products()">{{__('Name')}} <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        @elseif($col_name == 'name' && $query == 'desc')
                        <a href="javascript:$('#type').val('name,asc');sort_products()">{{__('Name')}} <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        @else
                        <a href="javascript:$('#type').val('name,asc');sort_products()">{{__('Name')}}</a>
                        @endif
                    @else
                    <a href="javascript:$('#type').val('name,asc');sort_products()">{{__('Name')}}</a>
                    @endif
                    
                    </th>
                    @if($type == 'Seller')
                        <th>{{__('Seller Name')}}</th>
                    @endif
                    <th>
                    @if(isset($col_name , $query))
                         @if($col_name == 'product_id' && $query == 'asc')
                        <a href="javascript:$('#type').val('product_id,desc');sort_products()">{{__('SKU')}} <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        @elseif($col_name == 'product_id' && $query == 'desc')
                        <a href="javascript:$('#type').val('product_id,asc');sort_products()">{{__('SKU')}} <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        @else
                        <a href="javascript:$('#type').val('product_id,asc');sort_products()">{{__('SKU')}}</a>
                        @endif
                    @else
                    <a href="javascript:$('#type').val('product_id,asc');sort_products()">{{__('SKU')}}</a>
                    @endif
                    
                    </th>
                    <th>
                    @if(isset($col_name , $query))
                        @if($col_name == 'num_of_sale' && $query == 'asc')
                        <a href="javascript:$('#type').val('num_of_sale,desc');sort_products()">{{__('Num of Sale')}} <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        @elseif($col_name == 'num_of_sale' && $query == 'desc')
                        <a href="javascript:$('#type').val('num_of_sale,asc');sort_products()">{{__('Num of Sale')}} <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        @else
                        <a href="javascript:$('#type').val('num_of_sale,asc');sort_products()">{{__('Num of Sale')}}</a>
                        @endif
                    @else
                    <a href="javascript:$('#type').val('num_of_sale,asc');sort_products()">{{__('Num of Sale')}}</a>
                    @endif
                    </th>
                    
                    <th>
                    @if(isset($col_name , $query))
                        @if($col_name == 'current_stock' && $query == 'asc')
                        <a href="javascript:$('#type').val('current_stock,desc');sort_products()">{{__('Total Stock')}} <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        @elseif($col_name == 'current_stock' && $query == 'desc')
                        <a href="javascript:$('#type').val('current_stock,asc');sort_products()">{{__('Total Stock')}} <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        @else
                        <a href="javascript:$('#type').val('current_stock,asc');sort_products()">{{__('Total Stock')}}</a>
                        @endif
                    @else
                    <a href="javascript:$('#type').val('current_stock,asc');sort_products()">{{__('Total Stock')}}</a>
                    @endif
                    </th>
                    <th>
                    @if(isset($col_name , $query))
                        @if($col_name == 'unit_price' && $query == 'asc')
                        <a href="javascript:$('#type').val('unit_price,desc');sort_products()">{{__('Base Price')}} <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        @elseif($col_name == 'unit_price' && $query == 'desc')
                        <a href="javascript:$('#type').val('unit_price,asc');sort_products()">{{__('Base Price')}} <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        @else
                        <a href="javascript:$('#type').val('unit_price,asc');sort_products()">{{__('Base Price')}}</a>
                        @endif
                    @else
                    <a href="javascript:$('#type').val('unit_price,asc');sort_products()">{{__('Base Price')}}</a>
                    @endif
                    </th>
                    <!--<th>{{__('Todays Deal')}}</th>-->
                    <th>
                    @if(isset($col_name , $query))
                        @if($col_name == 'rating' && $query == 'asc')
                        <a href="javascript:$('#type').val('rating,desc');sort_products()">{{__('Rating')}} <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        @elseif($col_name == 'rating' && $query == 'desc')
                        <a href="javascript:$('#type').val('rating,asc');sort_products()">{{__('Rating')}} <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        @else
                        <a href="javascript:$('#type').val('rating,asc');sort_products()">{{__('Rating')}}</a>
                        @endif
                    @else
                    <a href="javascript:$('#type').val('rating,asc');sort_products()">{{__('Rating')}}</a>
                    @endif
                    </th>
                    <th>{{__('Published')}}</th>
                    <th>{{__('Featured')}}</th>
                    <th>{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $key => $product)
                    <tr>
                        <td>{{ ($key+1) + ($products->currentPage() - 1)*$products->perPage() }}</td>
                        <td>{{$product->alignbook_pro_id}}</td>
                        
                        <td>
                             @if($product->trashed())
                             <a class="media-block">
                             @else
                            <a href="{{ route('product', $product->slug) }}" target="_blank" class="media-block">
                                @endif
                                <div class="media-left">
                                    @if(empty($product->thumbnail_img))
                                        <img loading="lazy"  class="img-md" src="{{ asset('img/thumb.jpg')}}" alt="Image">
                                    @else
                                        <img loading="lazy"  class="img-md" src="{{ asset($product->thumbnail_img)}}" alt="Image">
                                    @endif
                                </div>
                                <div class="media-body">{{ __($product->name) }}</div>
                            </a>
                        </td>
                         @if($type == 'Seller')
                            @php
                              $userData =  \App\User::where('id', $product->user_id)->get();
                            @endphp
                           <td> {{$userData[0]->name}}</td>
                        @endif
                        
                        <td>{{ $product->product_id ?? ' ' }}</td>
                       
                        <td>{{ $product->num_of_sale }} {{__('times')}}</td>
                        <td>
                            @php
                                $qty = 0;
                                if($product->variant_product){
                                    foreach ($product->stocks as $key => $stock) {
                                        $qty += $stock->qty;
                                    }
                                }
                                else{
                                    $qty = $product->current_stock;
                                }
                                echo $qty;
                            @endphp
                        </td>
                        <td>{{ number_format($product->unit_price,2) }}</td>
                        <!--<td><label class="switch">
                                <input onchange="update_todays_deal(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->todays_deal == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>-->
                        <td>{{ $product->rating }}</td>
                        @if($product->trashed())
                             <td>N/A</td><td>N/A</td>
                        @else
                        <td><label class="switch">
                                <input onchange="update_published(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->published == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>
                        <td><label class="switch">
                                <input onchange="update_featured(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->featured == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>
                        @endif
                        <td>
                            @if($product->trashed())
                                <form action="{{route('products.restore')}}" method="post" >
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                    <button class="btn btn-primary">Restore</button>
                                </form>
                                <form action="{{route('products.forceDelete')}}" method="post" >
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                    <button class="btn btn-danger">Force Delete</button>
                                </form>
                            @else
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    @if ($type == 'Seller')
                                        <li><a href="{{route('products.seller.edit', encrypt($product->id))}}">{{__('Edit')}}</a></li>
                                    @else
                                        <li><a href="{{route('products.admin.edit', encrypt($product->id))}}">{{__('Edit')}}</a></li>
                                    @endif
                                    <li><a href="{{route('products.trsnfertoseller', $product->id)}}">{{__('Transfer To Seller')}}</a></li>
                                    <li><a onclick="confirm_modal('{{route('products.destroy', $product->id)}}');">{{__('Delete')}}</a></li>
                                    <li><a href="{{route('products.duplicate', $product->id)}}">{{__('Duplicate')}}</a></li>
                                </ul>
                            </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $products->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
    <script type="text/javascript">

        $(document).ready(function(){
            //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
        });

        function update_todays_deal(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.todays_deal') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Todays Deal updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function update_published(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.published') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Published products updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function update_featured(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.featured') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Featured products updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function sort_products(el){
            $('#sort_products').submit();
        }

    </script>
@endsection
