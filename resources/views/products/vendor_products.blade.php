@extends('layouts.app')

@section('content')

@if($vendor_user_id != null)
    <div class="row">
        <div class="col-lg-12 pull-right">
            <button onclick="assignProduct()"  class="btn btn-rounded btn-info pull-right">{{__('Assign New Product')}}</button>
        </div>
    </div>
@endif

<br>

<div class="panel">
    <!--Panel heading-->
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{ __($type.' Products') }}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_products" action="" method="GET">
                @php /*
                <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 200px;">
                        <select class="form-control demo-select2" id="user_id" name="user_id" onchange="sort_products()">
                            <option value="">All Vendors</option>
                            @foreach (App\Vendor::all() as $key => $vendor)
                                @if ($vendor->user != null)
                                    <option value="{{ $vendor->user->id }}" @if ($vendor->user->id == $vendor_user_id) selected @endif>{{ $vendor->user->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div> */
                @endphp
                <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 200px;">
                        <select class="form-control demo-select2" name="type" id="type" onchange="sort_products()">
                            <option value="">Sort by</option>
                            <option value="rating,desc" @isset($col_name , $query) @if($col_name == 'rating' && $query == 'desc') selected @endif @endisset>{{__('Rating (High > Low)')}}</option>
                            <option value="rating,asc" @isset($col_name , $query) @if($col_name == 'rating' && $query == 'asc') selected @endif @endisset>{{__('Rating (Low > High)')}}</option>
                            <option value="num_of_sale,desc"@isset($col_name , $query) @if($col_name == 'num_of_sale' && $query == 'desc') selected @endif @endisset>{{__('Num of Sale (High > Low)')}}</option>
                            <option value="num_of_sale,asc"@isset($col_name , $query) @if($col_name == 'num_of_sale' && $query == 'asc') selected @endif @endisset>{{__('Num of Sale (Low > High)')}}</option>
                            <option value="unit_price,desc"@isset($col_name , $query) @if($col_name == 'unit_price' && $query == 'desc') selected @endif @endisset>{{__('Base Price (High > Low)')}}</option>
                            <option value="unit_price,asc"@isset($col_name , $query) @if($col_name == 'unit_price' && $query == 'asc') selected @endif @endisset>{{__('Base Price (Low > High)')}}</option>
                        </select>
                    </div>
                </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="20%">{{__('Name')}}</th>
                    @if($vendor_user_id != null)
                        <th>{{__('Vendor Name')}}</th>
                    @endif
                    <th>{{__('Num of Sale')}}</th>
                    <th>{{__('Total Stock')}}</th>
                    <th>{{__('Base Price')}}</th>
                    <th>{{__('Todays Deal')}}</th>
                    <th>{{__('Rating')}}</th>
                    <th>{{__('Published')}}</th>
                    <th>{{__('Featured')}}</th>
                    <th>{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $key => $product)
                    <tr>
                        <td>{{ ($key+1) + ($products->currentPage() - 1)*$products->perPage() }}</td>
                        <td>
                            <a href="{{ route('product', $product->slug) }}" target="_blank" class="media-block">
                                <div class="media-left">
                                    <img loading="lazy"  class="img-md" src="{{ asset($product->thumbnail_img)}}" alt="Image">
                                </div>
                                <div class="media-body">{{ __($product->name) }}</div>
                            </a>
                        </td>
                        @if($vendor_user_id != null)
                            <td>{{ $product->user->name }}</td>
                        @endif
                        <td>{{ $product->num_of_sale }} {{__('times')}}</td>
                        <td>
                            @php
                                $qty = 0;
                                if($product->variant_product){
                                    foreach ($product->stocks as $key => $stock) {
                                        $qty += $stock->qty;
                                    }
                                }
                                else{
                                    $qty = $product->current_stock;
                                }
                                echo $qty;
                            @endphp
                        </td>
                        <td>{{ number_format($product->unit_price,2) }}</td>
                        <td><label class="switch">
                                <input onchange="update_todays_deal(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->todays_deal == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>
                        <td>{{ $product->rating }}</td>
                        <td><label class="switch">
                                <input onchange="update_published(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->published == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>
                        <td><label class="switch">
                                <input onchange="update_featured(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->featured == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a onclick="confirm_modal('{{route('products.vendor_remove', $product->id)}}');">{{__('Remove Assigned Product')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $products->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

 <div class="modal fade" id="assign_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-content">
                <div class="panel">
                <div class="panel-title">
                    <h4>Assign Product</h4>
                </div>
                    <form class="form-horizontal" id="assign_form" action="{{ route('products.vendor_assign') }}" method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-2 control-label">{{__('Vendor')}}</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="user_id" name="user_id" required>
                                    <option value="">Select Vendor</option>
                                    @foreach (App\Vendor::all() as $key => $vendor)
                                        @if ($vendor->user != null)
                                            <option value="{{ $vendor->user->id }}" @if ($vendor->user->id == $vendor_user_id) selected @endif>{{ $vendor->user->name }}</option>
                                        @endif
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                           <div class="form-group" id="category">
                               <label class="col-md-2 control-label">{{__('Category')}}</label>
                               <div class="col-md-10">
                                   <select class="form-control" name="category_id" id="category_id" required>
                                        <option value="">Select Category</option>
                                       @foreach(\App\Category::all() as $category)
                                           @if (\App\HomeCategory::where('category_id', $category->id)->first() == null)
                                               <option value="{{$category->id}}">{{__($category->name)}}</option>
                                           @endif
                                       @endforeach
                                   </select>
                               </div>
                           </div>
                           <div class="form-group mb-3">
                               <label class="col-md-2 control-label" for="products">{{__('Products')}}</label>
                               <div class="col-md-10">
                                   <select name="products[]" id="products" class="form-control select2" data-placeholder="Choose Products" multiple required>

                                   </select>
                               </div>
                           </div>

                       </div>
                       <div class="panel-footer">
                            <button class="btn btn-primary">Add Product</button>
                       </div> 
                   </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')
    <script type="text/javascript">
        
        function assignProduct() {
            $('assign_form').trigger('reset');
            $('#assign_modal').modal('show', {backdrop: 'static'});
        }

        $(document).ready(function(){
            //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
            
        $('#products').select2();
        $('#category_id').on('change', function() {
            get_products_by_category();
        });

        function get_products_by_category(){
            var category_id = $('#category_id').val();
            $.post('{{ route('products.get_products_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                $('#products').html(null);
                 $('#products').append($('<option>', {
                        value: "",
                        text: 'Select Products'
                    }));
                for (var i = 0; i < data.length; i++) {
                    $('#products').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                }
                $('#products').trigger("change");
            });
        }
            
        });

        function update_todays_deal(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.todays_deal') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Todays Deal updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function update_published(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.published') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Published products updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function update_featured(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.featured') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Featured products updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function sort_products(el){
            $('#sort_products').submit();
        }

    </script>
@endsection
