@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Best Selling')}}</h3>
    </div>

    <!--Horizontal Form-->
    <!--===================================================-->
    <form class="form-horizontal" action="{{ route('best_selling.update', $bestSelling->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
		
        <input type="hidden" name="_method" value="PATCH">
        <div class="panel-body">
            <div class="form-group" id="category">
                <label class="col-lg-2 control-label">{{__('Background Color')}}</label>
                <div class="col-lg-7">
                    <input type="text" name="bgcolor" value="{{$bestSelling->background_color}}" class="form-control" placeholder="#282563"/>
                </div>
            </div>
            <div class="form-group" id="category">
                <label class="col-lg-2 control-label">{{__('Text Color')}}</label>
                <div class="col-lg-7">
                    <input type="text" name="textcolor" value="{{$bestSelling->text_color}}" class="form-control" placeholder="#fff"/>
                </div>
            </div>
            <div class="form-group" id="category">
                <label class="col-lg-2 control-label">{{__('Category')}}</label>
                <div class="col-lg-7">
                    <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
                        @foreach(\App\Category::orderby('position')->get() as $category)
                            <option value="{{$category->id}}" @php if($bestSelling->category_id == $category->id) echo "selected"; @endphp>{{__($category->name)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			<div class="form-group mb-3">
				<label class="col-sm-2 control-label" for="products">{{__('Products')}}</label>
				<div class="col-sm-7">
					<select name="products[]" id="products" class="form-control select2" data-placeholder="Choose Products" multiple required>
						
					</select>
				</div>
			</div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
        </div>
    </form>
    <!--===================================================-->
    <!--End Horizontal Form-->

</div>

<script type="text/javascript">

    $(document).ready(function(){
        $('#products').select2();
        get_products_by_category();

        $('#category_id').on('change', function() {
            get_products_by_category();
        });
        
        function get_products_by_category(){
            var category_id = $('#category_id').val();
            $.post('{{ route('products.get_products_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                $('#products').html(null);
                 $('#products').append($('<option>', {
                        value: "",
                        text: 'Select Products'
                    }));
                for (var i = 0; i < data.length; i++) {
                    $('#products').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name+" ( #"+data[i].product_id+" )"
                    }));
                }
                $("#products > option").each(function() {
                    @foreach (json_decode($bestSelling->products) as $key => $id)
                        if(this.value == '{{$id}}') {
                            $(this).prop('selected', true);
                        }
                    @endforeach
                    });
                $('#products').trigger("change");
            });
        }


    });
</script>
