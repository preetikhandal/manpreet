@extends('layouts.app')

@section('content')

    <div class="row">
       
    </div>

    <br>

    <!-- Basic Data Tables -->
    <!--===================================================-->
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all h-100">
            <h3 class="panel-title pull-left pad-no">{{__('Pharmacy Order List')}}</h3>
            <div class="pull-right clearfix">
            <form class="" id="sort_orders" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <!--<div class="" style="min-width: 200px;display:inline-block;">-->
                    <!--    <input type="text" class="form-control" id="search" name="cust_search"@isset($cust_search) value="{{ $cust_search }}" @endisset placeholder="Cust Name">-->
                    <!--</div>-->
                   
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type & Enter">
                    </div>
                    
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                </div>
            </form>
                
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Patient name')}}</th>
                    <th>{{__('Email')}}</th>
                    <th>{{__('Mobile No')}}</th>
                    <th>{{__('Order medicines')}}</th>
                    <th >{{__('Upload prescription')}}</th>
                    <th >{{__('Status')}}</th> 
                    <th >{{__('Doctor name')}}</th>
                    <th >{{__('Adress')}}</th>
                     
                </tr>
                </thead>
                <tbody>
                @foreach($feedbacks as $key => $feedback)
                        <tr>
                            <td>{{ ($key+1) + ($feedbacks->currentPage() - 1)*$feedbacks->perPage() }}</td>
                            <td><a href="{{route('feedback-details',encrypt($feedback->id))}}">{{$feedback->name}}</a></td>
                            <td >{{$feedback->email}}</td>
                            <td >{{$feedback->phone}}</td>
                            <td width="40%">{{$feedback->comment}}</td>
                            @if($feedback->documents !=null)
                            <td >
                            <img  width="100%" class="image" src="{{ asset('prescription')}}/{{$feedback->documents}}">
                           
                            |<a style="text-decoration:underline;color: blue;" href="{{ asset('prescription')}}/{{$feedback->documents}}" download="{{ asset('prescription')}}/{{$feedback->documents}}">Download</a>
                            </td>
                            @else
                            <td >Na</td>
                            @endif
                            <td>
                            <select class="form-control">
                                
                               <option value="{{$feedback->status}}/{{$feedback->id}}">{{$feedback->status}}</option>
                               @if($feedback->status_historyco =='' )
                                 <option value="Confirm/{{$feedback->id}}"> Confirm</option>
                               @endif
                               @if($feedback->status_historyco=='')
                                 <option value="Cancel/{{$feedback->id}}" >Cancel</option>
                               @endif
                               
                            </select>
                            </td>
                            <td >{{$feedback->docter_name}}</td>
                            <td >{{$feedback->adress}}</td>             
                        </tr>
                @endforeach
                </tbody>
            </table>
            <div class="clearfix">
                <div class="pull-right">
                    {{ $feedbacks->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
    
    
@endsection

@section('script')
<script type="text/javascript"> 

$('select').on('change', function() {
    $ar = this.value.split("/");
    $confirm = confirm("Do you want to "+$ar[0]+" this order");
    if($confirm){
            $.ajax({
            url:"{{url('ajax/pharmacyStatus')}}",
            type: 'post',
            //dataType: "json",
            data: {
            "_token": "{{ csrf_token() }}",
            "status": this.value
            },
            success: function( data ) {
               window.location.reload()            
            }
        });
    }else{
        window.location.reload();
    }
});
</script>

@endsection
