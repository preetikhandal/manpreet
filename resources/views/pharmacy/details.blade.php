@extends('layouts.app')

@section('content')

<div class="col-lg-10 col-lg-offset-1 pad-btm mar-btm">
    <div class="panel">
        <div class="pad-all bg-gray-light">
            <h3 class="mar-no">{{ $Pharmacy->subject }} #{{ $Pharmacy->code }}</h3>
             <ul class="mar-top list-inline">
                <li>{{ $Pharmacy->name }}</li>
               
                <li>{{ $Pharmacy->created_at }}</li>
                <li><span class="badge badge-pill badge-secondary">{{ ucfirst($Pharmacy->status) }}</span></li>
            </ul>
        </div>

        <div class="panel-body">
            
            <div class="pad-top">
                
                <div class="media bord-top pad-top">
                <a class="media-left" href="#">
                
               
                
                
                </a>
                    <h5><strong>Email:-</strong>{{$Pharmacy->email}}</h5>
                    <h5><strong>Phone:-</strong>{{$Pharmacy->phone}}</h5>
                    <h5><strong>Docter Name:-</strong>{{$Pharmacy->docter_name}}</h5>
                    <div class="media-body">
                        <div class="comment-header">
                            <a href="#" class="media-heading box-inline text-main text-bold">{{ $Pharmacy->name }}</a>
                            <p class="text-muted text-sm">{{$Pharmacy->created_at}}</p>
                        </div>
                
                        <p>
                        {{$Pharmacy->adress}}
                        <p> {{$Pharmacy->comment}}</p>
                            @if($Pharmacy->documents != null)
                                <div>
                                <div>
                                              <img height="200px" src="{{ asset('prescription')}}/{{$Pharmacy->documents}}" width="300px">
                                            </div>
                                </div>
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    
@endsection
