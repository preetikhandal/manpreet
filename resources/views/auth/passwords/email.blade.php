@extends('layouts.blank')

@section('content')
<div class="container forget-password">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<img src="{{asset('img/fogtpwKey.png')}}" alt="key" border="0">
						<h2 class="text-center">{{ __('Forgot Password?') }}</h2>
						<p>{{__('Enter your email address or Phone no to recover your password.')}}</p>
						<form id="register-form" role="form" autocomplete="off" class="form" method="POST" action="{{ route('password.email') }}">
						@csrf
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
									@if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
										<input id="text" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email or Phone">
									@else
										<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ __('Email or Phone') }}" name="email" required>
									@endif

								
								</div>
									@if ($errors->has('email'))
										<span class="invalid-feedback text-danger" role="alert">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
							</div>
							<div class="form-group">
								<button class="btn btn-lg btn-primary btn-block btnForget" type="submit">
									{{ __('Send Request') }}
								</button>
							</div>
						</form>
						 <div class="pad-top">
							<a href="{{route('user.login')}}" class="btn-link text-bold text-main">{{__('Back to Login')}}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
