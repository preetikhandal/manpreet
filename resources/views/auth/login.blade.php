@extends('layouts.login')

@section('content')

@php
    $generalsetting = \App\GeneralSetting::first();
@endphp
<style>
.magic-radio+label:before, .magic-checkbox+label:before {
    border: 1px solid #fff;
}
</style>
<div class="flex-row">
	<div class="container login-container" style="width:100%">
		<div class="row">
		    @if ($errors->any())
                <div class="alert alert-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			<div class="col-md-5 login-form-1">
				<br/><br/><br/>
				@if ($generalsetting->admin_login_sidebar != null)
					<img src="{{ asset($generalsetting->admin_login_sidebar) }}" class="img-fit" >
				 @else
					 <img src="{{ asset('img/bg-img/login-box.jpg') }}" class="img-fit" >
				@endif
			</div>
			<div class="col-md-7 col-12 login-form-2">
			
				<!--<div class="text-center">
					<br>
					@if($generalsetting->logo != null)
						<img loading="lazy"  src="{{ asset($generalsetting->logo) }}" class="" height="44">
					@else
						<img loading="lazy"  src="{{ asset('frontend/images/logo/logo.png') }}" class="" height="44">
					@endif

					<br>
					<br>
					<br>
				</div>-->
				<form method="POST" role="form" action="{{ route('login') }}">
					@csrf
					<div class="form-group">
						 <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
					
					</div>
					<div class="form-group">
						<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password">
						
					</div>
					<div class="row">
						<div class="col-md-6 col-12">
							<div class="form-group">
								<div class="checkbox pad-btm text-left text-xs-center">
									<input id="demo-form-checkbox" class="magic-checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
									<label for="demo-form-checkbox" class="btn-link">
										{{ __('Remember Me') }}
									</label>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-12">
							<div class="form-group">
								@if(env('MAIL_USERNAME') != null && env('MAIL_PASSWORD') != null)
									<div class="checkbox pad-btm text-right text-xs-center">
										<a href="{{ route('password.request') }}" class="btn-link">{{__('Forgot password')}} ?</a>
									</div>
								@endif
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-lg btn-block" style="background:#007bff">
							{{ __('Login') }}
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

