@extends('layouts.app')

@section('content')

    <div class="pad-all text-center">
        <form class="" action="{{ route('seller_sale_report.index') }}" method="GET">
            <div class="box-inline mar-btm pad-rgt">
                 Sort by verificarion status:
                 <div class="select">
                     <select class="demo-select2" name="verification_status" required>
                             <option>--Select Verification Status--</option>
                        <option value="1">Approved</option>
                        <option value="0">Non Approved</option>
                     </select>
                 </div>
            </div>
            <button class="btn btn-default" type="submit">Filter</button>
        </form>
    </div>


    <div class="col-md-offset-2 col-md-8">
        <div class="panel">
            <!--Panel heading-->
            <div class="panel-heading">
                <h3 class="panel-title">{{ __('Seller Based Selling Report') }}</h3>
            </div>

            <!--Panel body-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped mar-no">
                        <thead>
                            <tr><th>#</th>
                                <th>Seller Name</th>
                                <th>Shop Name</th>
                                <th>Number of Product Sale</th>
                                <th>Order Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sellers as $key => $seller)
                                @if($seller->user_id != null)
                                @php
                                //dd($seller->user_id);
                                @endphp
                                    <tr>
                                       <td>{{$sellers->firstItem() + $key }}</td>
                                        <td>{{\App\User::find($seller->user_id)->name ?? ' ' }}</td>
                                        <td>{{ (\App\Shop::where('user_id',$seller->user_id)->pluck('name')) }}</td>
                                        <td>
                                            @php
                                                $num_of_sale = 0;
                                                $orderdata=\App\OrderDetail::where('seller_id',$seller->user_id)->get();
                                                foreach ($orderdata as $key => $product) {
                                                    $num_of_sale += $product->quantity;
                                                }
                                            @endphp
                                            {{ $num_of_sale }}
                                        </td>
                                        <td>
                                            {{ single_price(\App\OrderDetail::where('seller_id', $seller->user_id)->sum('price')) }}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                 <div class="card-footer clearfix">
					{{$sellers->appends(request()->all())->links()}}
                    </div>
            </div>
        </div>
    </div>

@endsection
