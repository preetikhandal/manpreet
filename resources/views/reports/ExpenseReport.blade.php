@extends('layouts.app')

@section('content')
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-title p-3 text-white">
							<h3>Filters
							</h3>
						</div>
						<form class="form-horizontal"  action="{{url('/admin/reports/expenses')}}" method="get" >
							<div class="panel-body">
								<div class="row">
									<div class="col-md-3 pr-0">
										<label for="category" class="col-sm-12 col-form-label">Category</label>
										<div class="col-sm-12">
											<select class="form-control select2" id="category" name="category" required style="width: 100%;">
												<option value="0">All</option>
												@foreach($ExpensesCategory as $EC)
													<option value="{{$EC->ECategoryID}}" @if($ExpensesCategorySelected == $EC->ECategoryID) selected @endif>{{$EC->ECategoryName}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-md-3 pr-0">
										<label for="category" class="col-sm-12 col-form-label">Status</label>
										<div class="col-sm-12">
											<select class="form-control select2" id="paymentStatus" name="paymentStatus" required style="width: 100%;">
												<option value="Paid"  @if($ExpensesPaymentStatusSelected == "Paid") selected @endif>Paid</option>
												<option value="Partially Paid" @if($ExpensesPaymentStatusSelected == "Partially Paid") selected @endif>Partially Paid</option>
												<option value="Unpaid" @if($ExpensesPaymentStatusSelected == "Unpaid") selected @endif>Unpaid</option>
												<option value="All" @if($ExpensesPaymentStatusSelected == "All") selected @endif>All</option>
											</select>
										</div>
									</div>
									<div class="col-md-3 pr-0">
										<label for="inputEmail3" class="col-sm-12 col-form-label">Date Range</label>
										<div class="col-sm-12">
											<input type="text" class="form-control" id="DateRange" name="date" placeholder="Date Range">
										</div>
									</div>
                                     <div class="col-md-1 pr-0">
                                         	<label for="category" class="col-form-label">&nbsp;</label><br />
											<button class="btn btn-success">Filter</button>
                                    <br />&nbsp;
										</div>
								  </div>
							</div>
							<!-- /.card-body -->
						</form>
					</div>
					<div class="panel">
						<div class="panel-title p-0 text-white bg-pattern-danger">
							
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-hover table-sm table-info table-bordered">
									<thead class="bg-gray text-white">
										<tr class="text-center">
											<th class="w-5">#</th>
											<th class="w-20">Date</th>
											<th class="w-20">Description</th>
											<th class="w-20">Expense Category</th>
											<th class="w-10">Invoice Amount</th>
											<th class="w-10">Paid Amount</th>
											<th class="w-10">Status</th>
											<th class="w-5">Action</th>
										</tr>
									</thead>
									<tbody>
									@php $arrycount=count($Expenses); @endphp
									@if($arrycount==0)
										<tr><td colspan="7" class="text-center"><h3>No Records found</h3></td></tr>
									@else
										@foreach($Expenses as $Key => $E)
											<tr class="text-center">
												<td>{{$Expenses->firstItem() + $Key }}</td>
												<td>{{Carbon\Carbon::parse($E->ExpenseInvoiceDate)->format('d/m/Y') }}</td>
												<td>{{$E->ExpenseName}}</td>
												<td>{{\App\ExpensesCategory::find($E->ECategoryID)->ECategoryName}}</td>
												<td>₹{{$E->ExpensesInvoiceAmount}}</td>
												<td>₹{{$E->PaidAmount}}</td>
                                                <td class="align-middle">@if($E->PaymentStatus == "Paid") <span class="right badge badge-success"> @elseif($E->PaymentStatus == "Partially Paid") 
                                            <span class="right badge badge-warning">
                                                @else <span class="right badge badge-danger"> @endif
                                            {{$E->PaymentStatus}}</span></td>
												<td>
													<div class="btn-group">
														<a href="{{url('admin/expenses/view/')}}/{{$E->ExpenseID}}" class="btn btn-sm btn-primary btn-flat">View</a>
														<a href="{{url('admin/expenses/edit/')}}/{{$E->ExpenseID}}" class="btn btn-sm btn-warning btn-flat">Edit</a>
													 </div>
												</td>
											</tr>
										@endforeach
                                            <tr class="text-center">
												<td colspan="3"></td>
                                                <td><strong>Total</strong></td>
                                                <td><strong>₹{{$ExpensesInvoiceAmount}}</strong></td>
                                                <td><strong>₹{{$PaidAmount}}</strong></td>
                                                <td class="align-middle"></td>
												<td></td>
											</tr>
									@endif
									</tbody>
								</table>
								{{$Expenses->appends(request()->except('page'))->links()}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('script')
	<script>
	$(function () {
		//Initialize Select2 Elements
		$('.select2').select2()
	});
	</script>
	<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
	<!-- daterange picker -->
	<link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
	<!-- date-range-picker -->
	<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>

	<script type="text/javascript">
				{
			var start = moment("{{$dateStart}}", "YYYY-MM-DD");

		}
			
			{
			var end = moment("{{$dateEnd}}", "YYYY-MM-DD");
		}
		
		
		function cb(start, end) {
			$('#DateRange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}

		$('#DateRange').daterangepicker({
			"locale": {
			"format": "DD/MM/YYYY",
			},
			startDate: start,
			endDate: end,
			ranges: {
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);

		cb(start, end);
	</script>
@endsection