@extends('layouts.app')

@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
                
					<div class="panel">
						<div class="panel-title">
							<h3>
                                Filters
							</h3>
						</div>
						<form class="form-horizontal" action="{{url('/admin/reports/sales')}}" method="get" >
							<div class="panel-body">
									<div class="row">
										<label for="inputEmail3" class="col-sm-2 col-form-label">Date Range</label>
										<div class="col-sm-5">
											<input type="text" class="form-control" id="DateRange" name="date" placeholder="Date Range">
										</div>
                                        <div class="col-sm-1">
											<button class="btn btn-success">Filter</button>
										</div>
                                        
									</div>
								  
							</div>
							<!-- /.card-body -->
						</form>
					</div>
					<div class="panel">
						<div class="panel-title p-0 text-white bg-pattern-danger"> <div class="col-sm-1">
										<!--button class="btn btn-success ml-auto">Export</button-->
										</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-hover table-sm table-info table-bordered" id="printTable">
								<thead class="text-white">
									<tr class="bg-gray">
										<th class="w-20 text-center">#</th>
										<th class="w-20 text-center">Order ID</th>
										<th class="w-20 text-center">Order Code</th>
										<th class="w-20 text-center">Date</th>
										<!--th class="w-20 text-center">Customer Name</th-->
										<th class="w-7 text-center">Payment Status</th>
										<th class="w-7 text-center">Product(s) Price</th>
										<th class="w-7 text-center">Tax</th>
										<th class="w-7 text-center">Shipping</th>
										<th class="w-7 text-center">Total Amount</th>
										<th class="w-5 text-center">Action</th>
									</tr>
								</thead>
								<tbody>
                                    @php 
                                    $total_price = 0;
                                    $total_tax = 0;
                                    $total_shipping_cost = 0;
                                    $gt = 0;
                                    @endphp
								@foreach($Order as $key=>$O)
                                    <tr class="text-center">
                                        <td class="align-middle">{{($key+1)+($Order->currentPage()-1)*$Order->perPage() }}</td>
                                        <td class="align-middle">{{$O->id}}</td>
                                        <td class="align-middle">{{$O->code}}</td>
                                        
                                        <td class="align-middle">{{Carbon\Carbon::parse($O->created_at)->format('d/m/Y')}}</td>
                                        <!--td class="align-middle">{{App\User::find(1)}}</td-->
                                        <td class="align-middle">@if($O->payment_status == "paid") <label class="label label-success">Paid</label>
                                                @else <label class="label label-danger">Unpaid</label> @endif
                                        </td>
                                        <td class="align-middle">{{$Price = $O->orderDetails->sum('price')}}</td>
                                        <td class="align-middle">{{$Tax = $O->orderDetails->sum('tax')}}</td>
                                        <td class="align-middle">{{$SC = $O->orderDetails->sum('shipping_cost')}}</td>
                                        <td class="align-middle">{{$T = $Price + $Tax + $SC}}</td>
                                        <td class="align-middle">
                                            <div class="btn-group">
                                                <a href="{{ route('orders.show', encrypt($O->id)) }}" class="btn btn-sm btn-block btn-warning">View</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @php
                                    $total_price = $total_price + $Price;
                                    $total_tax = $total_tax + $Tax;
                                    $total_shipping_cost = $total_shipping_cost + $SC;
                                    $gt = $gt + $T;
                                    @endphp
									@endforeach
                                    
                                    <tr class="text-center">
                                        <td class="align-middle text-right" colspan="5"> Total &nbsp; &nbsp; &nbsp;</td>
                                        <td class="align-middle"><strong>{{$total_price}}</strong></td>
                                        <td class="align-middle"><strong>{{$total_tax}}</strong></td>
                                        <td class="align-middle"><strong>{{$total_shipping_cost}}</strong></td>
                                        <td class="align-middle"><strong>{{$gt}}</strong></td>
                                        <td class="align-middle"></td>
                                    </tr>
								</tbody>
							</table>
						</div>
					</div>
                    <div class="card-footer clearfix">
					{{$Order->appends(request()->all())->links()}}
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('script')
	<script>
	$(function () {
		//Initialize Select2 Elements
		$('.select2').select2()
	});
	</script>
	<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
	<!-- daterange picker -->
	<link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
	<!-- date-range-picker -->
	<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>

	<script type="text/javascript">
	
			{
			var start = moment("{{$dateStart}}", "YYYY-MM-DD");

		}
			
			{
			var end = moment("{{$dateEnd}}", "YYYY-MM-DD");
		}
		
		function cb(start, end) {
			$('#DateRange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}

		$('#DateRange').daterangepicker({
			"locale": {
			"format": "DD/MM/YYYY",
			},
			startDate: start,
			endDate: end,
			ranges: {
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);

		cb(start, end);
	</script>
@endsection