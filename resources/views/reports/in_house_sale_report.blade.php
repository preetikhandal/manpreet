@extends('layouts.app')

@section('content')

<div class="col-md-8 col-md-offset-2">
     <div class="panel">
            <!--Panel heading-->
            <div class="panel-heading">
                <h3 class="panel-title">Download Product Sales Report</h3>
            </div>
            <div class="panel-body">
                 <form action="{{route('ProductsSalesReportDownload.Downlaod')}}" id="download_form" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                           <div class="input-daterange input-group" id="datepicker_range">
                                <input type="text" class="input-sm form-control" name="startDate" id="startDate" value="" />
                                <span class="input-group-addon">to</span>
                                <input type="text" class="input-sm form-control" name="endDate" id="endDate" value="" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <button  class="btn btn-primary">Download</button>
                        </div>
                    </div>
                    </form>
                    <br />
            </div>
        </div>
    </div>
    
    


    <div class="col-md-offset-2 col-md-8">
        <div class="panel">
            <!--Panel heading-->
            <div class="panel-heading">
                <h3 class="panel-title">Product wise sale report [Total]</h3>
            </div>

            <!--Panel body-->
            <div class="panel-body">
                <div class="pad-all text-center">
                    <form class="" action="{{ route('in_house_sale_report.index') }}" method="GET">
                        <div class="box-inline mar-btm pad-rgt">
                             Sort by Category:
                             <div class="select">
                                 <select id="demo-ease" class="demo-select2" name="category_id" required>
                                     @foreach (\App\Category::all() as $key => $category)
                                         <option value="{{ $category->id }}">{{ __($category->name) }}</option>
                                     @endforeach
                                 </select>
                             </div>
                        </div>
                        <button class="btn btn-default" type="submit">Filter</button>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped mar-no"><!--  demo-dt-basic class is used for creating js data table -->
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Num of Sale</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $key => $product)
                                <tr>
                                     <td>{{($key+1)+($products->currentPage()-1)*$products->perPage() }}</td>
                                    <td>{{ __($product->name) }}</td>
                                    <td>{{ $product->num_of_sale }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                 <div class="card-footer clearfix">
					{{$products->appends(request()->all())->links()}}
                    </div>
            </div>
        </div>
    </div>

@endsection


@section('script')
    <script type="text/javascript">
$('.input-daterange').datepicker({
    format: "dd/mm/yyyy",
    endDate: "date()",
    autoclose: true
});
    </script>
@endsection
