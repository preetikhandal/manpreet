@extends('layouts.app')

@section('content')

    <!--div class="pad-all text-center">
        <form class="" action="{{ route('wish_report.index') }}" method="GET">
            <div class="box-inline mar-btm pad-rgt">
                 Sort by Category:
                 <div class="select">
                     <select id="demo-ease" class="demo-select2" name="category_id" required>
                         @foreach (\App\Category::all() as $key => $category)
                             <option value="{{ $category->id }}">{{ __($category->name) }}</option>
                         @endforeach
                     </select>
                 </div>
            </div>
            <button class="btn btn-default" type="submit">Filter</button>
        </form>
    </div-->


    <div class="col-md-offset-2 col-md-8">
        <div class="panel">
            <!--Panel heading-->
            <div class="panel-heading">
                <h3 class="panel-title">Product Wish Report</h3>
            </div>

            <!--Panel body-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped mar-no">
                        <thead>
                            <tr>
                                 <th>S. No.</th>
                                <th>Product Name</th>
                                <th>Wish Count</th>
                            </tr>
                        </thead>
                        <tbody>@php $i=1; @endphp
                            @foreach ($wishlists as $key => $wish)
                               @php
                               $Product = \App\Product::find($wish->product_id);
                               @endphp
                               @if($Product != null)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$Product->name }}</td>
                                        <td>{{$wish->wish_count ??' ' }}</td>
                                    </tr>
                               @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>

@endsection
