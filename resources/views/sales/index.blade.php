@extends('layouts.app')

@section('content')
<style>
    .ordcode{
        color:#282563;
    }
    .ordcode:hover{
        color:#EB3038;
    }
</style>
<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Orders')}}</h3>

        <div class="pull-right clearfix">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type code & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <form  id="sort_orders" method="GET">
        <div class="row">
            <div class="col-md-5">
               <div class="input-daterange input-group" id="datepicker_range">
                    <input type="text" class="input-sm form-control" name="startDate" id="startDate" value="{{$startDate}}" />
                    <span class="input-group-addon">to</span>
                    <input type="text" class="input-sm form-control" name="endDate" id="endDate" value="{{$endDate}}" />
                </div>
            </div>
            <div class="col-md-2">
                <select class="form-control demo-select2" name="payment_type" id="payment_type">
                    <option value="all" @isset($payment_status) @if($payment_status == 'all') selected @endif @endisset >All</option>
                    <option value="paid"  @isset($payment_status) @if($payment_status == 'paid') selected @endif @endisset>{{__('Paid')}}</option>
                    <option value="unpaid"  @isset($payment_status) @if($payment_status == 'unpaid') selected @endif @endisset>{{__('Un-Paid')}}</option>
                </select>
            </div>
            <div class="col-md-3">
                <select name="delivery_status" id="delivery_status" class="form-control">
                    <option value="all" @isset($delivery_status) @if($delivery_status == 'all') selected @endif @endisset>All</option>
                    <option value="confirmation_pending" @isset($delivery_status) @if($delivery_status == 'confirmation_pending') selected @endif @endisset>{{__('Confirmation Pending')}}</option>
                    <option value="hold"   @isset($delivery_status) @if($delivery_status == 'hold') selected @endif @endisset>{{__('Hold')}}</option>
                    <option value="pending"   @isset($delivery_status) @if($delivery_status == 'pending') selected @endif @endisset>{{__('Pending')}}</option>
                    <option value="on_review"   @isset($delivery_status) @if($delivery_status == 'on_review') selected @endif @endisset>{{__('On review')}}</option>
                    <option value="ready_to_ship"   @isset($delivery_status) @if($delivery_status == 'ready_to_ship') selected @endif @endisset>{{__('Ready to Ship')}}</option>
                    <option value="ready_to_delivery"   @isset($delivery_status) @if($delivery_status == 'ready_to_delivery') selected @endif @endisset>{{__('Ready to Delivery')}}</option>
                    <option value="shipped"   @isset($delivery_status) @if($delivery_status == 'shipped') selected @endif @endisset>{{__('Shipped')}}</option>
                    <option value="on_delivery"   @isset($delivery_status) @if($delivery_status == 'on_delivery') selected @endif @endisset>{{__('On delivery')}}</option>
                    <option value="delivered"   @isset($delivery_status) @if($delivery_status == 'delivered') selected @endif @endisset>{{__('Delivered')}}</option>
                    <option value="processing_refund"   @isset($delivery_status) @if($delivery_status == 'processing_refund') selected @endif @endisset>{{__('Processing Refund')}}</option>
                    <option value="refunded"   @isset($delivery_status) @if($delivery_status == 'refunded') selected @endif @endisset>{{__('Refunded')}}</option>
                    <option value="cancel"   @isset($delivery_status) @if($delivery_status == 'cancel') selected @endif @endisset>{{__('Cancel')}}</option>
                </select>
            </div>
            <div class="col-md-1">
                <button class="btn btn-primary">Filter</button>
            </div>
            <div class="col-md-1">
                <button type="button" onclick="download()" class="btn btn-primary">Download</button>
            </div>
        </div>
        </form>
        
        <form action="{{route('sales.Downlaod')}}" id="download_form" method="post">
            @csrf
            <input type="hidden" name="startDate" id="startDateDownload">
            <input type="hidden" name="endDate" id="endDateDownload">
            <input type="hidden" name="payment_type" id="payment_typeDownload">
            <input type="hidden" name="delivery_status" id="delivery_statusDownload">
        </form>
        <script>
        function download() {
            startDate = $('#startDate').val();
            endDate = $('#endDate').val();
            payment_type = $('#payment_type').val();
            delivery_status = $('#delivery_status').val();
            $('#startDateDownload').val(startDate);
            $('#endDateDownload').val(endDate);
            $('#payment_typeDownload').val(payment_type);
            $('#delivery_statusDownload').val(delivery_status);
            $('#download_form').submit();
        }
        </script>
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Order Code</th>
                    <th>Num. of<br />Products</th>
                    <th>Customer</th>
                    <th>Total Order Value</th>
                    <th>Discount</th>
                    <th>Wallet<br />Credit Used</th>
                    <th>Amount</th>
                    <th>Delivery Status</th>
                    <th>Payment Status</th>
                    <th width="10%">{{__('options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $key => $order)
                    <tr>
                        <td>
                            {{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }}
                        </td>
                        <td>
                            <a class="ordcode" href="{{route('sales.show', encrypt($order->id))}}">{{ $order->code }}</a>
                        </td>
                        <td>
                            {{ count($order->orderDetails) }}
                        </td>
                        <td>
                            @if ($order->user_id != null)
                                {{ $order->user->name }}
                            @else
                                Guest ({{ $order->guest_id }})
                            @endif
                        </td>
                        <td>
                            {{ single_price($order->grand_total + $order->coupon_discount + $order->wallet_credit) }}
                        </td>
                        <td>
                            {{ single_price($order->coupon_discount) }}
                        </td>
                        <td>
                            {{ single_price($order->wallet_credit) }}
                        </td>
                        <td>
                            {{ single_price($order->grand_total) }}
                        </td>
                        <td>
                            @php
                                $status = 'Delivered';
                                foreach ($order->orderDetails as $key => $orderDetail) {
                                    if($orderDetail->delivery_status != 'delivered'){
                                        $status = 'Pending';
                                    }
                                }
                            @endphp
                            {{ $status }}
                        </td>
                        <td>
                            <span class="badge badge--2 mr-4">
                                @if ($order->payment_status == 'paid')
                                    <i class="bg-green"></i> Paid
                                @else
                                    <i class="bg-red"></i> Unpaid
                                @endif
                            </span>
                        </td>
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('sales.show', encrypt($order->id))}}">{{__('View')}}</a></li>
                                    <li><a href="{{ route('customer.invoice.download', $order->id) }}">{{__('Download Invoice')}}</a></li>
                                    <li><a onclick="confirm_modal('{{route('orders.destroy', $order->id)}}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $orders->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
    <script type="text/javascript">
$('.input-daterange').datepicker({
    format: "dd/mm/yyyy",
    endDate: "date()",
    autoclose: true
});
    </script>
@endsection
