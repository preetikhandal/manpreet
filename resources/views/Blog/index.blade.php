@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <a href="{{ route('blog.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Blog')}}</a>
        </div>
    </div>

    <br>

    <!-- Basic Data Tables -->
    <!--===================================================-->
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all h-100">
            <h3 class="panel-title pull-left pad-no">{{__('Blogs')}}</h3>
            <div class="pull-right clearfix">
                <form class="" id="sort_sellers" action="" method="GET">
                    <div class="box-inline pad-rgt pull-left">
                        <div class="" style="min-width: 200px;">
                            <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type Blog Title & Enter">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Title')}}</th>
                    <th>{{__('Blog Summary')}}</th>
                    <th>{{__('Blog Slug')}}</th>
                    <th>{{__('Blog Image')}}</th>
                    <th width="10%">{{__('Options')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($Blogs as $key => $blog)
                        <tr>
                            <td>{{ ($key+1) + ($Blogs->currentPage() - 1)*$Blogs->perPage() }}</td>
                            <td>{{$blog->BlogTitle}}</td>
                            <td width="40%">{{$blog->BlogSummary}}</td>
                            <td>{{$blog->URLSlug}}</td>
                            <td>
                                 @if($blog->FeaturedImage!=null)
									<img src="{{asset($blog->FeaturedImage)}}" style="width:50px" />
								@else
									<img src="{{asset('img/thumb.jpg')}}" style="width:50px" />
								@endif
                            </td>
                            <td>
                                <div class="btn-group dropdown">
                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                        {{__('Actions')}} <i class="dropdown-caret"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{route('blog.edit', $blog->id)}}">{{__('Edit Blog')}}</a></li>
                                        <li><a onclick="confirm_modal('{{route('blog.destroy', $blog->id)}}');">{{__('Delete Blog')}}</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </table>
            <div class="clearfix">
                <div class="pull-right">
                    {{ $Blogs->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
  
@endsection
