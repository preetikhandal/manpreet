@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <!-- <a href="{{ route('sellers.create')}}" class="btn btn-info pull-right">{{__('add_new')}}</a> -->
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Customers wallet history')}}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_customers" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder=" Type email or name & Enter">
                    </div>
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                    
                    
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Payment method')}}</th>
                    <th>{{__('Date')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Payment Details')}}</th>
                    <th>{{__('Payment Status')}}</th>
                </tr>
            </thead>
            <tbody>
                   
                @foreach($wallet_history as $key=>  $wallet_historys)
                             
                           @php
                           
                           $username =  \App\User::where('id', $wallet_historys->user_id)->get();
                           
                           @endphp
                    <tr>
                        
                        <td>{{$key+1}}</td>
                        <td>{{$username[0]->name}}</td>
                        <td>{{$wallet_historys->payment_method}}</td>
                        <td>{{ date('d-M-y', strtotime($wallet_historys->created_at)) }}</td>
                        <td>{{$wallet_historys->amount}}</td>
                        <td>{{$wallet_historys->payment_details}} </td>
                        
                        <td>{{$wallet_historys->payment_status}}</td>
                        
                      
                        
                        
                    </tr>
                @endforeach
            </tbody>
        </table>
        
    </div>
</div>

@endsection
@section('script')
    <script type="text/javascript">
        function sort_customers(el){
            $('#sort_customers').submit();
        }
    </script>
@endsection
