@if(count($combinations[0]) > 0)
	<table class="table table-bordered">
		<thead>
			<tr>
				<td class="text-center">
					<label for="" class="control-label">{{__('Variant')}}</label>
				</td>
				<td class="text-center">
					<label for="" class="control-label">{{__('Products')}}</label>
				</td>
				<td class="text-center">
					<label for="" class="control-label">{{__('Show/Hide')}}</label>
				</td>
			</tr>
		</thead>
		<tbody>
@foreach ($combinations as $key => $combination)
	@php
		$str = implode("-",$combination);
        $variation_show_hide = 1;
	@endphp
	@if(strlen($str) > 0)
		<tr>
			<td>
				<label for="" class="control-label">{{ $str }}</label>
                <input type="hidden" name="variation[]" value="{{$str}}">
			</td>
			<td>
                <select name="product_variation_select[{{$str}}]" class="form-control variationselect2">
                    <option value="0" @if(isset($product_variation[$str])) @if($product_variation[$str] == 0) selecte
                @endif @endif>No Product Selected</option>
                @foreach($products as $p) 
                <option value="{{$p->id}}" @if(isset($product_variation[$str])) @if($product_variation[$str] == $p->id) selected @php
                        $variation_show_hide = $p->variation_show_hide; 
                @endphp 
                @endif @endif>{{$p->name}} - (SKU: {{$p->product_id}})</option>
                @endforeach
                </select>
			</td>
			<td>
                <select name="product_variation_show[{{$str}}]" class="form-control"><option value="1" @if($variation_show_hide == 1) selected @endif>Show</option><option value="0"  @if($variation_show_hide == 0) selected @endif>Hide</option></select>
			</td>
		</tr>
	@endif
@endforeach
            <script>
                $('.variationselect2').select2();
                </script>

	</tbody>
</table>
@endif
