<div class="panel-heading">
    <h3 class="panel-title">{{__('Add Your User Base Coupon')}}</h3>
</div>
<div class="form-group">
    <label class="col-lg-3 control-label" for="coupon_code">{{__('Coupon code')}}</label>
    <div class="col-lg-9">
        <input type="text" placeholder="{{__('Coupon code')}}" id="coupon_code" value="{{old('coupon_code')}}" name="coupon_code" class="form-control" required>
    </div>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">{{__('Slab Rate')}}</label>
    <div class="col-lg-9">
        <table class="table" id="slabRate">
            <tr><th>Cart Value From</th><th>Cart Value to</th><th>Discount %</th><th>Action</th></tr>
            <tr id="slabrate1"><td><input type="text" name="CartValueFrom[]" class="form-control"></td><td><input type="text" name="CartValueTo[]" class="form-control"></td><td><input type="text" name="CartValueDiscount[]" class="form-control"></td><td><button type="button" class="btn btn-danger btn-sm" onclick="$('#slabrate1').remove();">Remove</button></td></tr>
        </table>
        <button type="button" class="btn btn-sm btn-primary" onclick="addSlab()">Add Slab</button>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-3 control-label" for="name">{{__('Customer')}}</label>
    <div class="col-lg-9">
        <select name="user_id" class="form-control user_id" id="user_id" required>

        </select>
    </div>
</div>

<script type="text/javascript">
var n = 2;
function addSlab() {
    var html = '<tr id="slabrate'+n+'"><td><input type="text" name="CartValueFrom[]" class="form-control"></td><td><input type="text" name="CartValueTo[]" class="form-control"></td><td><input type="text" name="CartValueDiscount[]" class="form-control"></td><td><button class="btn btn-danger btn-sm" onclick="$(\'#slabrate'+n+'\').remove();">Remove</button></td></tr>';
    $('#slabRate').append(html);
    n++;
}
    $(document).ready(function(){
        $('#user_id').select2({
        minimumInputLength: 3,
        ajax: {
            url: '{{ route("coupon.get_user") }}',
            dataType: 'json',
            processResults: function (data) {
              return {
                results:  $.map(data, function (item) {
                    text = item.name
                    if(item.phone != null) {
                        text = text+" ("+item.phone+")";
                    }
                    if(item.email != null) {
                        text = text+" ["+item.email+"]";
                    }
                      return {
                          text: text,
                          id: item.id
                      }
                  })
              };
            },
        },
    });
    
    });

</script>
