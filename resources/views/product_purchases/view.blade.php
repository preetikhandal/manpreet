@extends('layouts.app')

@section('content')
<section class="content">
	<div class="panel">
		<div class="panel-body">
			<div class="row ">
				<div class="col-md-5">
					<div class="panel">
						<div class="panel-title">
							<h3>Product Purchased Invoice Information</h3>
                            <div class="pull-right">
							<a href="javascript:window.history.back()" class="btn btn-flat float-right btn-danger"><i class="fa fa-arrow-left"></i>&nbsp; Back </a>
                            </div>
						</div>
						<div class="panel-body pt-4">
							<div class="row">
								<div class="col-12">
									<table class="table table-striped table-sm">
										<tr><th><b>Company Name: </b></th><td>{{$ProductPurchase->vendor->company_name}}</td></tr>
										<tr><th><b>Invoice No: </b></th><td>{{$ProductPurchase->invoice_no}}</td></tr>
										<tr><th><b>	Invoice Date: </b></th><td>{{Carbon\Carbon::parse($ProductPurchase->invoice_date)->format('d-M-Y')}}</td></tr>
										<tr><th><b>Invoice Amount: </b></th><td class="font-size-20 text-danger font-weight-bold">&#8377;{{$ProductPurchase->invoie_amount}}</td></tr>
										<tr><th><b>Paid Amount: </b></th><td class="font-size-20 text-danger font-weight-bold">(-) &#8377;{{$ProductPurchase->paid_amount}}</td></tr>
										<tr><th><b>Outstanding Amount: </b></th><td class="font-size-20 text-success font-weight-bold">&#8377;{{$ProductPurchase->outstanding_amount}}</td></tr>
										<tr><th><b>Payment Status: </b></th><td>{{$ProductPurchase->status}}</td></tr>
									</table>
								</div>
							</div>
						</div>
						<div class="panel-footer">
							<div class="text-right">
								<a href="{{url('admin/product/purchase/edit/')}}/{{$ProductPurchase->product_purchase_id}}" class="btn btn-sm btn-primary">
									 Edit
								</a>
								<a href="javascript:window.history.back()" class="btn btn-sm btn-danger">
									<i class="fas fa-arrow-left"></i> Back
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="panel">
						<div class="paenl-title">
							<h3 >Payment History</h3>
							<div class="pull-right">
                                <a href="{{url('admin/product/purchase/addpayment/'.$ProductPurchase->product_purchase_id)}}" class="btn btn-primary float-right"><i class="fa fa-plus"></i>&nbsp; Add Payment </a>
                            </div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-hover table-sm table-info table-bordered">
											<thead class="bg-gray text-white">
												<tr class="text-center">
													<th>#</th>
													<th>Payment Date</th>
													<th>Amount</th>
													<th>Payment Method</th>
													<th>Payment RefID</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											@php $arrycount=count($ProductPurchasePayment); $sn=1; @endphp
											@if($arrycount==0)
												<tr><td colspan="6" class="text-center"><h3>No Records found</h3></td></tr>
											@else
												@foreach($ProductPurchasePayment as $Key => $P)
													<tr class="text-center">
														<td>{{ $sn++ }}</td>
														<td>{{Carbon\Carbon::parse($P->paid_date)->format('d-M-Y')}}</td>
														<td>{{ $P->paid_amount }}</td>
														<td>{{ $P->payment_method }}</td>
														<td>{{ $P->refID }}</td>
														<td>
															<form method="post" action="{{url('admin/product/purchase/deletepayment')}}" onclick="return confirm('Do you want to delete this?');" style="display:inline-block">
																{!!csrf_field()!!}
																<input type="hidden" name="id" value="{{$P->product_purchase_payment_id}}">
																
																<button>Delete</button> 
															</form>
														</td>
													</tr>
												@endforeach
											@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.card-body -->
	</div>
</section>
@endsection