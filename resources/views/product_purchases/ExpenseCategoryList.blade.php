@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-title">
                <div class="pull-right">
                    <a href="{{url('admin/expenses/createcategory')}}" class="btn float-right btn-primary"> Add New Category &nbsp;<i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('Category Name')}}</th>
                            <th width="10%">{{__('Options')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $Key => $C)
                            <tr>
                                <td>{{$categories->firstItem() + $Key }}</td>
                                <td>{{$C->ECategoryName}}</td>
                                <td>
                                    <div class="btn-group">
                                    <a href="{{url('admin/expenses/categoryedit/')}}/{{$C->ECategoryID}}" class="btn btn-sm btn-warning btn-flat">Edit
                                    </a>&nbsp;
                                    <form method="post" action="{{url('admin/expenses/categorydelete')}}" onclick="return confirm('Do you want to delete this Category?');" style="display:inline-block">
                                        {!!csrf_field()!!}
                                        <input type="hidden" name="id" value="{{$C->ECategoryID}}">
                                        <button type="submit" class="btn btn-danger btn-sm  btn-flat">Delete</button>
                                    </form>
                                 </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="clearfix">
                    <div class="pull-right">
                        {{ $categories->appends(request()->except('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection