@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="row">
    <div class="col-sm-12">
        <a href="javascript:window.history.back();" class="btn btn-danger pull-right">{{__('Back')}}</a> 
    </div>
</div>


    <form class="form-horizontal" action="{{url('admin/product/purchase/save')}}" method="post">
        @csrf
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Product Purchase Information')}}</h3>
    </div>
        <div class="panel-body">
            <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="form-group">
                <div class="col-md-12">
                    Company Name<br />
                    <select class="form-control select2" id="campany_name" name="campany_name" required style="width: 100%;">
                        <option value="">Select Company Name</option>
                        @foreach($Vendor as $V)
                            <option value="{{$V->id}}" data-gst="{{$V->gstin}}">{{$V->company_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    Invoice Date <br />
                    <input type="text" autocomplete="off" class="form-control datepicker" name='invoice_date' id="invoice_date" placeholder="dd/mm/yyyy" >
                </div>
                <div class="col-sm-6">
                    Invoice Number <br />
                    <input type="text" name="invoice_no" class="form-control" id="invoice_no"  placeholder="Invoice Number" value="{{ old('invoice_no') }}" >
                </div>
            </div>
        </div>
        </div>
</div>
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Product/Items')}}</h3>
    </div>
        <div class="panel-body">
            <table class="table table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>{{__('Category')}}</th>
                        <th>{{__('Product')}}</th>
                        <th>{{__('MRP Price')}}</th>
                        <th>{{__('Unit Price')}}</th>
                        <th>{{__('Discount Price')}}</th>
                        <th>{{__('Quantity')}}</th>
                        <th>{{__('GST %')}}</th>
                        <th>{{__('SGST')}}</th>
                        <th>{{__('CGST')}}</th>
                        <th>{{__('IGST')}}</th>
                        <th>{{__('Sub Total')}}</th>
                        <th>{{__('To Stock')}}</th>
                        <th>{{__('Options')}}</th>
                    </tr>
                </thead>
            <tbody id="Item">
                <tr id="row_1">';
                <td><select class="form-control" name="category_id[]" id="category_id" required onchange="get_product_by_category(this.value, 1)">
                    <option value="">Select Category</option>
                    @foreach(\App\Category::all() as $category)
                    <option value="{{$category->id}}">{{__($category->name)}}</option>
                                        @endforeach</select></td>
                <td><select class="form-control" id="product_id_1" name="product_id[]"></select></td>
                <td><input type="text" class="form-control" id="mrp_price_1" onkeyup="calculatePrice()" name="mrp_price[]" placeholder="MRP Price"></td>
                <td><input type="text" class="form-control" id="unit_price_1" onkeyup="calculatePrice()" name="unit_price[]" placeholder="Unit Price"></td>
                <td><input type="text" class="form-control" id="discount_1" onkeyup="calculatePrice()" name="discount[]" placeholder="Discount"></td>
                <td><input type="text" class="form-control" id="quantity_1" onkeyup="calculatePrice()" name="quantity[]" placeholder="Quantity"></td>
                <td><input type="text" class="form-control" id="gst_per_1" onkeyup="calculatePrice()" name="gst_per[]" placeholder="GST %"></td>
                <td><input type="text" class="form-control" id="sgst_price_1" name="sgst_price[]" placeholder="0"><input type="hidden" name="sgst_per[]" value="0" id="sgst_per_1"></td>
                <td><input type="text" class="form-control" id="cgst_price_1" name="cgst_price[]" placeholder="0"><input type="hidden" name="cgst_per[]" value="0" id="cgst_per_1"></td>
                <td><input type="text" class="form-control" id="igst_price_1" name="igst_price[]" placeholder="0"><input type="hidden" name="igst_per[]" value="0" id="igst_per_1"></td>
                <td><input type="text" class="form-control" id="subtotal_1" name="subtotal[]" placeholder="Sub Total" value="0"></td>
                <td><select class="form-control" name="stock_update[]"><option value="1">Yes</option><option value="0">No</option></select></td>
                <td><button class="btn btn-danger" onclick="removeItem(1)">Delete</button></td>
                </tr>
            </tbody>
                
            <tfooter>
                <tr>
                    <td colspan="7" style="text-align:right;padding-top:15px;"><strong>Total</strong>&nbsp; &nbsp;</td>
                    <td><input type="text" name="sgst" class="form-control" id="gt_sgst" required placeholder="sgst" value="{{ old('sgst',0) }}"></td>
                    <td><input type="text" name="cgst" class="form-control" id="gt_cgst" required placeholder="cgst" value="{{ old('cgst',0) }}"></td>
                    <td><input type="text" name="igst" class="form-control" id="gt_igst" required placeholder="igst" value="{{ old('igst',0) }}"></td>
                    <td><input type="text" name="invoice_amount" class="form-control" id="gt" placeholder="Invoice Amount" value="{{ old('invoice_amount',0) }}"/></td>
                    <td colspan="2"></td>
                </tr>
            </tfooter>
            <tfooter>
                <tr>
                    <td colspan="8">
                        <button type="button" class="btn btn-primary" onclick="addProduct()">Add Product</button>
                    </td>
                </tr>
            </tfooter>
            </table>
        </div>
</div>
        <script>
            var company_gst = "";
            var gst_state_code = "00";
            var alde_state_code = "09";
            var integrated_tax = true;
            
            $("#campany_name").change(function() {
                 company_gst = $("#campany_name option:selected").data('gst');
                 gst_state_code = company_gst.substring(0,2);
                console.log(gst_state_code);
                if(alde_state_code == gst_state_code) {
                   integrated_tax = false
                } else {
                   integrated_tax = true;
                }
            });
            
            
            var category = "";
            category += '                <option value="">Select Category</option>';
                                        @foreach(\App\Category::all() as $category)
            category += '                       <option value="{{$category->id}}">{{__($category->name)}}</option>';
                                        @endforeach
            var n = 2;
            var productArray = [];
            productArray.push(1);
            function addProduct() {
                    html = '<tr id="row_'+n+'">';
                    html += '<td><select class="form-control" name="category_id[]" id="category_id" required onchange="get_product_by_category(this.value, '+n+')">'+category+'</select></td>';
                    html += '<td><select class="form-control" id="product_id_'+n+'" name="product_id[]"></select></td>';
                    html += '<td><input type="text" class="form-control" id="mrp_price_'+n+'" onkeyup="calculatePrice('+n+')"  name="mrp_price[]" placeholder="MRP Price"></td>';
                    html += '<td><input type="text" class="form-control" id="unit_price_'+n+'" onkeyup="calculatePrice('+n+')"  name="unit_price[]" placeholder="Unit Price"></td>';
                    html += '<td><input type="text" class="form-control" id="discount_'+n+'" onkeyup="calculatePrice('+n+')" name="discount[]" placeholder="Discount"></td>';
                    html += '<td><input type="text" class="form-control" id="quantity_'+n+'" onkeyup="calculatePrice('+n+')" name="quantity[]" placeholder="Quantity"></td>';
                    html += '<td><input type="text" class="form-control" id="gst_per_'+n+'" onkeyup="calculatePrice('+n+')" name="gst_per[]" placeholder="GST %"></td>';
                    html += '<td><input type="text" class="form-control" id="sgst_price_'+n+'" name="sgst_price[]" placeholder="SGST" value="0"><input type="hidden" name="sgst_per[]" value="0" id="sgst_per_'+n+'"></td>';
                    html += '<td><input type="text" class="form-control" id="cgst_price_'+n+'" name="cgst_price[]" placeholder="CGST" value="0"><input type="hidden" name="cgst_per[]" value="0" id="cgst_per_'+n+'"></td>';
                    html += '<td><input type="text" class="form-control" id="igst_price_'+n+'" name="igst_price[]" placeholder="IGST" value="0"><input type="hidden" name="igst_per[]" value="0" id="igst_per_'+n+'"></td>';
                    html += '<td><input type="text" class="form-control" id="subtotal_'+n+'" name="subtotal[]" placeholder="Sub Total" value="0"></td>';
                    html += '<td><select class="form-control" name="stock_update[]"><option value="1">Yes</option><option value="0">No</option></select></td>';
                    html += '<td><button class="btn btn-danger" onclick="removeItem('+n+')">Delete</button></td>';
                    html += '</tr>';
                    if(n == 1) {
                        $('#Item').html("");
                    }
                    $('#Item').append(html);
                    $('#product_id_'+n).select2();
                    productArray.push(n);
                 n++;
                }
            
            function get_product_by_category(category_id, ele_id) {
                $.post('{{ route('products.get_products_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                    $('#product_id_'+ele_id).html(null);
                        $('#product_id_'+ele_id).append($('<option>', {
                            value: "",
                            text: 'Select Products'
                        }));
                    for (var i = 0; i < data.length; i++) {
                        $('#product_id_'+ele_id).append($('<option>', {
                            value: data[i].id,
                            text: data[i].name
                        }));
                    }
                    $('#product_id_'+ele_id).trigger("change");
                });
            }
            function removeItem(rowid) {
                var index = productArray.indexOf(rowid);
                productArray.splice(index, 1);
                $('#row_'+rowid).remove();
            }
            function calculatePrice() {
                gt_igst = 0;
                gt_sgst = 0;
                gt_cgst = 0;
                gt = 0;
                $.each(productArray, function(i,v) {
                    mrp_price = parseFloat($('#mrp_price_'+v).val());
                    unit_price = parseFloat($('#unit_price_'+v).val());
                    quantity = parseFloat($('#quantity_'+v).val());
                    gst_per = parseFloat($('#gst_per_'+v).val());
                    if(isNaN(mrp_price) || isNaN(unit_price) || isNaN(quantity) || isNaN(gst_per)) {
                        return;
                    } else {
                        if(integrated_tax) {
                            gst1 = gst_per;
                            gst_per = parseInt(gst_per)/100;
                            gst = unit_price * gst_per;
                            subtotal = unit_price + gst;
                            discount = subtotal/mrp_price;
                            discount = discount * 100;
                            discount = 100 - discount;
                            subtotal = quantity * subtotal;
                            gt_igst = gt_igst + gst;
                            gt = gt + subtotal;
                            $('#discount_'+v).val(discount.toFixed(2));
                            $('#sgst_price_'+v).val(0);
                            $('#cgst_price_'+v).val(0);
                            $('#igst_price_'+v).val(gst.toFixed(2));
                            $('#sgst_per_'+v).val(0);
                            $('#cgst_per_'+v).val(0);
                            $('#igst_per_'+v).val(gst1);
                            $('#subtotal_'+v).val(subtotal.toFixed(2));
                        } else {
                            gst1 = parseInt(gst_per)/2;
                            gst_per = parseInt(gst_per)/100;
                            cgst_gst = unit_price * (gst_per/2);
                            sgst_gst = unit_price * (gst_per/2);
                            subtotal = unit_price + cgst_gst + sgst_gst;
                            discount = subtotal/mrp_price;
                            discount = discount * 100;
                            discount = 100 - discount;
                            subtotal = quantity * subtotal;
                            gt_sgst = gt_sgst + sgst_gst;
                            gt_cgst = gt_cgst + cgst_gst;
                            gt = gt + subtotal;
                            $('#discount_'+v).val(discount.toFixed(2));
                            $('#sgst_price_'+v).val(sgst_gst.toFixed(2));
                            $('#cgst_price_'+v).val(cgst_gst.toFixed(2));
                            $('#igst_price_'+v).val(0);
                            $('#sgst_per_'+v).val(gst1);
                            $('#cgst_per_'+v).val(gst1);
                            $('#igst_per_'+v).val(0);
                            $('#subtotal_'+v).val(subtotal.toFixed(2));
                        }
                    }
                });
                $('#gt').val(gt.toFixed(2));
                $('#gt_igst').val(gt_igst.toFixed(2));
                $('#gt_cgst').val(gt_cgst.toFixed(2));
                $('#gt_sgst').val(gt_sgst.toFixed(2));
            }
        </script>
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Payment Information (Optional)')}}</h3>
    </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="paidamount" class="col-md-3 text-right control-label">Paid Amount</label>
                <div class="col-md-9">
                    <input type="number" name="paid_amount" class="form-control" id="paid_amount" placeholder="0" value="{{ old('paid_amount') }}">
                    <span id="amountErr" style="color:red;font-weight:600"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="outstandingamount" class="col-sm-3 text-right control-label">Outstanding Amount</label>
                <div class="col-sm-9">
                    <input type="number" name="outstanding_amount" class="form-control" id="outstanding_amount"  placeholder="0" value="{{ old('outstanding_amount') }}" min="0">
                </div>
            </div>
            <div class="form-group">
                <label for="paymentdate" class="col-sm-3 text-right control-label col-form-label">Payment Date</label>
                <div class="col-sm-9">
                     <input type="text" autocomplete="off" class="form-control datepicker" name='payment_date' id="payment_date" placeholder="dd/mm/yyyy" >
                </div>
            </div>
            <div class="form-group">
                <label for="paymentmethod" class="col-md-3 text-right control-label col-form-label">Payment Method</label>
                <div class="col-md-9">
                    <select class="form-control select2" id="payment_method" name="payment_method" style="width: 100%;">
                        <option value="">Select Payment Method</option>
                            <option value="Cash">Cash</option>
                            <option value="Card">Card</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Other">Other</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="refID" class="col-md-3 text-right control-label col-form-label">Payment Ref ID</label>
                <div class="col-md-9">
                    <input type="text" name="refID" class="form-control" id="refID" placeholder="Payment Ref ID" value="{{ old('refID') }}"/>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
        </div>
</div>
</form>

	@endsection
	@section('script')
	
	<script>
	$(function () {
		//Initialize Select2 Elements
		$('.select2').select2()
	});
	</script>
    <script>
        $(function () {
            // INITIALIZE DATEPICKER PLUGIN
            $('.datepicker').datepicker({
                clearBtn: true,
                format: "dd/mm/yyyy",
                autoclose: true,
            });
        });
    </script>
	<script>
		$( function() {
			$("#invoice_amount").blur(function(){
				TotalAmnt=$(this).val();
				$("#outstanding_amount").val(TotalAmnt);
			});
			
			$("#paidamount").blur(function(){
				PaidAmnt=$(this).val();
				invoiceamount=$("#invoice_amount").val();
				
				if(parseInt(PaidAmnt)>parseInt(invoiceamount))
				{
					$("#amountErr").html("Paid Amount can not be greater than Invoice Amount.");
					$("#paidamount").val("");
					$("#paidamount").focus();
				}
				else{
					$("#amountErr").html("");
					Outstanding_Amnt=invoiceamount-PaidAmnt;
					$("#outstandingamount").val(Outstanding_Amnt);				
				}
			});
		});
	</script>
	@endsection
