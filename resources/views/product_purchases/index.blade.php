@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{url('admin/product/purchase/create')}}" class="btn btn-info pull-right">{{__('Add New Purchase')}}</a> 
    </div>
</div>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-title bord-btm clearfix pad-all h-100">
        <h3>{{__('Purchases')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Date')}}</th>
                    <th>{{__('Invoice No.')}}</th>
                    <th>{{__('Company Name')}}</th>
                    <th><center>{{__('CGST')}}</center></th>
                    <th><center>{{__('SGST')}}</center></th>
                    <th><center>{{__('IGST')}}</center></th>
                    <th><center>{{__('Total Amount')}}</center></th>
                    <th><center>{{__('Outstanding')}}</center></th>
                    <th><center>{{__('Status')}}</center></th>
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ProductPurchase as $key => $P)
                    <tr>
                        <td>{{ $ProductPurchase->firstItem() + $key }}</td>
                        <td>{{Carbon\Carbon::parse($P->invocie_date)->format('d-M-Y')}}</td>
                        <td>{{ $P->invoice_no }}</td>
                        <td>{{ $P->vendor->company_name }}</td>
                        <td><center>₹{{$P->cgst}}</center></td>
                        <td><center>₹{{$P->sgst}}</center></td>
                        <td><center>₹{{$P->igst}}</center></td>
                        <td><center>₹{{$P->invoice_amount}}</center></td>
                        <td><center>₹{{$P->outstanding_amount}}</center></td>
                        <td><center>@if($P->status == "Paid") <label class="label label-success">Paid</label> @else <label class="label label-danger">{{$P->status}}</label> @endif</center></td>
                        <td>
                            <div class="btn-group">
                                <a href="{{url('admin/product/purchase/view/')}}/{{$P->product_purchase_id}}" class="btn btn-sm btn-primary btn-flat">View</a>&nbsp;
                                <a href="{{url('admin/product/purchase/edit/')}}/{{$P->product_purchase_id}}" class="btn btn-sm btn-warning btn-flat">Edit</a>&nbsp;
                                <form method="post" action="{{url('admin/product/purchase/delete')}}" onclick="return confirm('Do you want to delete this Expense? Also Expense payment details will be deleted.');" style="display:inline-block">
                                    {!!csrf_field()!!}
                                    <input type="hidden" name="id" value="{{$P->product_purchase_id}}">
                                    <button type="submit" class="btn btn-danger btn-sm  btn-flat">Delete</button>
                                </form>
                             </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $ProductPurchase->appends(request()->except('page'))->links() }}
            </div>
        </div>
    </div>
</div>
@endsection