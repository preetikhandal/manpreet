@extends('layouts.app')

@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Request Products')}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th>{{__('Product Name')}}</th>
                        <th>{{__('Customer Name')}}</th>
                        <th>{{__('Phone')}}</th>
                        <th>{{__('Email')}}</th>
                        <th>{{__('Message')}}</th>
                        <th>{{__('Status')}}</th>
                        <th width="10%">{{__('Action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ProductRequest as $key => $PR)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td><a href="{{url('product/'.$PR->product)}}" target="_blank">
									{{\App\Product::where('slug',$PR->product)->first()->name ?? ' '}}
								</a>
							</td>
                            <td>{{$PR->name}}</td>
                            <td>{{$PR->phone}}</td>
                            <td>{{$PR->email}}</td>
                            <td>{{$PR->message}}</td>
							<td>{{$PR->status}}</td>
                            <td>
								<div class="btn-group dropdown">
									<button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
										{{__('Actions')}} <i class="dropdown-caret"></i>
									</button>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="{{route('productrequest.process', $PR->id.'~In Process')}}">{{__('In Process')}}</a></li>
										<li><a href="{{route('productrequest.process',$PR->id.'~Email')}}">{{__('Precure')}}</a></li>
										<li><a onclick="confirm_modal('{{route('productrequest.destroy', $PR->id)}}');">{{__('Delete')}}</a></li>
									</ul>
								</div>
							</td>
                            
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection
