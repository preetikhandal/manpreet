@extends('frontend.layouts.app')

@section('content')
 <style>
    .countdown .countdown-digit{
        background:#282563!important;
        color:#fff!important;
        font-weight:600;
    }
    .aldeproductslider .list-product{ margin: 0px 0px 5px 0px; }
</style>
@php
	if (Cache::has('best_selling')){
	   $best_selling =  Cache::get('best_selling');
	} else {
		$best_selling = \App\BestSelling::orderBy('updated_at', 'desc')->limit(20)->get();
		Cache::add('best_selling', $best_selling,120);
	}
@endphp
@if(count($best_selling)!=0)
<div @if($mobileapp == 1) style="margin-top:75px" @endif>
    
        
           
            <section class="pb-4">
                <div class="container ">
                    <div class="text-center text-dark">
                        <h1 class="h3 py-3">Best Selling Product List </h1>
                        <!--01/31/2021 -->
                        
                    </div>
                    <div class="row sm-no-gutters gutters-5 aldeproductslider deepscustomrow">
                        
                        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                            <div class="row">
                   @foreach($best_selling as $key => $bestSelling)
                   	@foreach(\App\Product::where('published', 1)->whereIn('id', json_decode($bestSelling->products))->get() as $key => $product)
                        @php
                        if(\App\Product::find($product->id) == null) {
                            continue;
                        }
                        $home_base_price = home_base_price($product->id);
                        $home_discounted_base_price = home_discounted_base_price($product->id);
                        if($home_base_price == "₹ 0.00") {
                            $product->current_stock = 0;
                            $qty = 0;
                        }
                        @endphp
                        <div class="col-md-2">
                            	<div class="img-block text-center">
                            		<a href="{{ route('product', $product->slug) }}" class="thumbnail">
                            		    @if($product->thumbnail_img != null)
                            			<img class="first-img  img-fluid swiper-lazy" src="{{ asset('frontend/images/product-thumb.jpg') }}" data-src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" />
                            			@else
                            		    <img class="first-img  img-fluid swiper-lazy" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
                            			@endif
                            		</a>
                            	</div>
                            	@if($product->current_stock != 0)
                                	@if($home_base_price != $home_discounted_base_price)
                                	<ul class="product-flag">
                                		<li class="new discount-price bg-orange">{{discount_calulate($home_base_price, $home_discounted_base_price )}}% off</li>
                                	</ul>
                                	@endif
                            	@endif
                            	<div class="product-decs text-center">
                            		<h2><a href="{{ route('product', $product->slug) }}" class="product-link"> {!! Str::limit($product->name, 58, ' ...') !!}</a></h2>
                            		<div class="pricing-meta">
                            			<ul>
                            			    @if($product->current_stock == 0)
                            			    <li class="current-price">&nbsp; &nbsp; &nbsp; &nbsp; </li>
                            			    @else
                                			    @if($home_base_price != $home_discounted_base_price)
                                				<li class="old-price">{{ $home_base_price }}</li>
                                				@endif
                                				<li class="current-price">{{ $home_discounted_base_price }}</li>
                            				@endif
                            			</ul>
                            		</div>
                            	</div>
                            	<div class="add-to-cart-buttons">
                            	    @if($product->current_stock == 0)
                            	    <button class="btn btn95" type="button" disabled> Out of Stock</button>
                            	    @else
                            	    <button class="btn btn25" onclick="addToCart(this, {{$product->id}}, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
                            	    <button class="btn btn70" onclick="buyNow(this, {{ $product->id }}, 1, 'full')" type="button"> Buy Now</button>
                            	    @endif
                            	</div>
                            </div>
                        
                      
    						
    				@endforeach
                    @endforeach
				</div></div>
            </section>
     
        
</div>
@endif
@endsection
