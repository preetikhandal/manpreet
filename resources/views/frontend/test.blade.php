@extends('frontend.layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.19/mmenu.min.css" integrity="sha512-gIn3+aW4xrkqoIvXsJ7F3woKfU+KTvEzXm0IXKmWqXZ8oyD9TfzFEdGDMF0egqto86E91yZVPzoa82a/dgSMig==" crossorigin="anonymous" />
<style>
nav {
    display:none;
}
.header {
    display:none;
}
            .mm-menu {
				height:  100% !important;
				--mm-color-background: #3ba5df;
				--mm-listitem-size: 50px;
			}
			.mm-navbars_top {
				--mm-color-background: #4bb5ef;
				border-bottom: none !important;
			}
			.mm-navbar {
				color: #fff !important;
				font-size: 14px;
				font-weight: bold;
			}
			.mm-navbar input {
				text-align: center;
			}
			.mm-navbar:first-child {
				align-content: center;
				justify-content: center;
				align-items: center;
			}
			.mm-navbar img {
				flex-grow: 0;
				opacity: 0.6;
				border: 1px solid #fff;
				border-radius: 50px;
				width: 100px;
				height: 100px;
				padding: 10px;
				margin: 0 10px;
			}
			.mm-navbar .fa {
				flex-grow: 0;
				border: 1px solid rgba(255, 255, 255, 0.6);
				border-radius: 30px;
				color: rgba(255, 255, 255, 0.8) !important;
				font-size: 16px !important;
				line-height: 50px;
				width: 50px;
				height: 50px;
				padding: 0;
			}
			.mm-navbar .fa:hover {
				border-color: #fff;
				color: #fff !important;
			}

			.mm-panels > .mm-panel:after {
				content: none;
				display: none;
			}
			.mm-panels > .mm-panel > .mm-listview {
				margin-top: 10px !important;
			}

			.mm-listview {
				font-size: 16px;
			}
			.mm-listitem:last-child:after {
				content: none;
				display: none;
			}
			.mm-listitem a,
			.mm-listitem span {
				color: rgba(255, 255, 255, 0.7);
				text-align: center;
				padding-right: 20px !important;
			}
			.mm-listitem a:hover,
			.mm-listitem a:hover + span {
				color: #fff;
			}
</style>

<nav id="menu">
    <ul id="panel-menu">
        @foreach(\App\Category::orderBy('position','asc')->get() as $category)
            <li>
                @if($category->banner!=null)
					<img loading="lazy"  class="cat-image ml-2" src="{{ asset($category->banner) }}" style="width:50px;height:50px">
				@else
					<img loading="lazy"  class="cat-image ml-2" src="{{ asset('frontend/images/chatLogo.png') }}" style="width:50px;height:50px">
				@endif
                <span>{{ __($category->name) }}</span>
                @php
                    $Category = \App\Category::orderBy('position','asc')->find($category->id);
                @endphp
                <ul>
                    @foreach ($Category->subcategories as $key2 => $subcategory)
                        <li>
                            @if($subcategory->icon == null)
                                <img loading="lazy" class="cat-image ml-2" src="{{ asset('frontend/images/chatLogo.png') }}" style="width:50px;height:50px">
                            @else
                                <img loading="lazy" class="cat-image ml-2" src="{{ asset($subcategory->icon) }}" style="width:50px;height:50px">
                            @endif
                            <span>{{ __($subcategory->name) }}</span>
                            @php 
                                $category_slug = \App\SubCategory::find($subcategory->id)->category->slug;
                                $subcategory_slug = \App\SubCategory::find($subcategory->id)->slug;
                            @endphp
                            <ul>
                            @foreach (\App\SubCategory::find($subcategory->id)->subsubcategories as $key3 => $subsubcategory)
                                <li>
                                    @if($subsubcategory->icon == null)
                                     <img class="cat-image ml-2" src="{{ asset('frontend/images/chatLogo.png') }}" style="width:50px;height:50px">
                                    @else
                                     <img loading="lazy"  class="cat-image ml-2" src="{{ asset($subsubcategory->icon) }}" style="width:50px;height:50px">
                                    @endif
                                    <a href="{{ route('products.subsubcategory', [$category_slug, $subcategory_slug, $subsubcategory->slug]) }}">
                                     {{ __($subsubcategory->name) }}</a>
                                </li>
                            @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>
</nav>		

@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/jquery.mhead@1.0.1/dist/jquery.mhead.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.19/mmenu.js" integrity="sha512-wvqkumeSxABjtGcAy7Ubx0+qsUEadompYD8/Z3nHfI4xzEqnbGTMw4JCg3dcXwyvGXuFEreG4We+1D/1HkY7Uw==" crossorigin="anonymous"></script>
<script>
	$(function() {
				
			const menu = new Mmenu("#menu", {
		extensions 	: [ "position-bottom", "fullscreen", "theme-black",  "border-full" ],
		navbars		: [{
			height 	: 2,
			content : [
				'Categories'
			]
		}, {
			content: [ "searchfield" ]
		}, {
			content : ["prev","title"]
		}, {
             "position": "bottom",
             "content": [
                "<button class='btn btn-primary' id='closebtn'>Close</button>"
             ]
          }
                          ]}, {});
		

            // Get the API
            const api = menu.API;

            // Invoke a method
            const panel = document.querySelector( "#menu" );
            api.open();
            $('#closebtn').on('click', function() {
               window.history.go(-1);
            });

});
</script>
@endsection
