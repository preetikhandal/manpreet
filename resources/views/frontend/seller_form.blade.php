@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-9 mx-auto">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{__('Shop Informations')}}
                                    </h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                            <li class="active"><a href="{{ route('shops.create') }}">{{__('Create Shop')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($errors->any())
                			<div class="alert alert-danger">
                				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
                				<ul>
                					@foreach($errors->all() as $error)
                						<li>{{ $error }}</li>
                					@endforeach
                				</ul>
                			</div>
	                	@endif
                        <form class="" action="{{ route('shops.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @if (!Auth::check())
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2">
                                        {{__('User Info')}}
                                    </div>
                                    <div class="form-box-content p-3">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <!-- <label>{{ __('Name') }}</label> -->
                                                    <div class="input-group input-group--style-1">
                                                        <input type="text" class="form-control{{ $errors->has('user_name') ? ' is-invalid' : '' }}" value="{{ old('user_name') }}" placeholder="{{ __('Name') }}" name="user_name">
                                                        <span class="input-group-addon">
                                                            <i class="text-md la la-user"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <!-- <label>{{ __('Email') }}</label> -->
                                                    <div class="input-group input-group--style-1">
                                                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ __('Email') }}" onkeyup="checkEmail()" name="email" id="email" required>
                                                        <span class="input-group-addon">
                                                            <i class="text-md la la-envelope"></i>
                                                            <span style="color:red;" id="error"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <script>
                
                                        function checkEmail(){
                                            var email = $('#email').val();
                                            if(IsEmail(email)==false){
                                              
                                                $('#error').html('please enter valid email');
                                              return false;
                                            }else{
                                                
                                                $.ajax({
                                                     url:'{{ route('checkemail') }}',
                                                     method:'POST',
                                                     dataType: "json",
                                                     data:{_token:'{{ @csrf_token() }}','email':email},
                                                     async:false,
                                                     success:function(data){
                                                         console.log(data.msg)
                                                        // location.reload();
                                                        if(data.status_code == 500){
                                                            $('#error').html(data.msg);
                                                            return false;
                                                            
                                                        }else{
                                                            $('#error').html(''); 
                                                        }
                                                        
                                                     }
                                                }); 
                                            }
                                        }
                                        
                                        
                                        function IsEmail(email) {
                                          var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                          if(!regex.test(email)) {
                                            return false;
                                          }else{
                                            return true;
                                          }
                                        }
                                </script>
										
										<div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <!-- <label>{{ __('Password') }}</label> -->
                                                    <div class="input-group input-group--style-1">
                                                        <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}" placeholder="{{ __('Mobile Number') }}" name="phone">
                                                        <span class="input-group-addon">
                                                            <i class="text-md la la-envelope"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <!-- <label>{{ __('Password') }}</label> -->
                                                    <div class="input-group input-group--style-1">
                                                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Password') }}" name="password">
                                                        <span class="input-group-addon">
                                                            <i class="text-md la la-lock"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <!-- <label>{{ __('Confirm Password') }}</label> -->
                                                    <div class="input-group input-group--style-1">
                                                        <input type="password" class="form-control" placeholder="{{ __('Confirm Password') }}" name="password_confirmation">
                                                        <span class="input-group-addon">
                                                            <i class="text-md la la-lock"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{__('Basic Info')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {{__('Shop Name')}} <span class="required-star">*</span> <br />
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Shop Name')}}" value="{{old('name')}}" name="name" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            {{__('Logo')}}<br />
                                            <input type="file" name="logo" id="file-2" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-2" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{__('Choose image')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{__('Address')}} <br />
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Address')}}" value="{{old('address')}}" name="address" required>
                                        </div>
                                        <div class="col-md-6">
                                            {{__('Landmark')}} <br />
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Landmark')}}" value="{{old('landmark')}}" name="landmark">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            {{__('City')}} <br />
                                            <input type="text" class="form-control mb-3" placeholder="{{__('City')}}" value="{{old('city')}}" name="city" required>
                                        </div>
                                        <div class="col-md-4">
                                            {{__('State')}} <br />
                                            <input type="text" class="form-control mb-3" placeholder="{{__('State')}}" value="{{old('state')}}" name="state" required>
                                        </div>
                                        <div class="col-md-4">
                                            {{__('Pincode')}} <br />
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Pincode')}}" value="{{old('pincode')}}" name="pincode" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{__('Contact Person Name')}} <br />
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Contact Person Name')}}" value="{{old('contact_person_name')}}" name="contact_person_name"  required>
                                        </div>
                                        <div class="col-md-6">
                                            {{__('Contact Person Phone')}} <br />
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Contact Person Phone')}}" value="{{old('contact_person_phone')}}" name="contact_person_phone" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{__('Firm Type')}} <br />
                                            <select class="form-control mb-3" name="firm_type" required>
                                                <option value="">Please Select</option>
                                                <option value="Public Limited">Public Limited</option>
                                                <option value="Private Limited">Private Limited</option>
                                                <option value="Sole Proprietorship">Sole Proprietorship</option>
                                                <option value="LLP">LLP</option >
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            {{__('GSTIN')}} <br />
                                            <input type="text" class="form-control mb-3" placeholder="{{__('GSTIN')}}" value="{{old('gstin')}}" name="gstin">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{__('Trade License No')}} <br />
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Trade License No')}}" value="{{old('trade_license_no')}}" name="trade_license_no">
                                        </div>
                                        <div class="col-md-6">
                                            {{__('Business Registered In')}} <br />
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Business Registered In')}}" value="{{old('business_registration_in')}}" name="business_registration_in">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="text-right mt-4" >
                                <button type="submit" class="btn btn-styled btn-base-1">{{__('Save')}}</button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
