@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('frontend.inc.vendor_side_nav')
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{__('Orders')}}
                                    </h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                            <li class="active"><a href="{{ route('orders.index') }}">{{__('Orders')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if (count($orders) > 0)
                            <!-- Order history table -->
                            <div class="card no-border mt-4">
                                <div>
                                    <table class="table table-sm table-hover table-responsive-md">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{__('Order Code')}}</th>
                                                <th>{{__('Num. of Products')}}</th>
                                                <!--th>{{__('Customer')}}</th-->
                                                <!--th>{{__('Amount')}}</th-->
                                                <th>{{__('Order Status')}}</th>
                                                <th>{{__('Action Required')}}</th>
                                                <th>{{__('Options')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($orders as $key => $order_id)
                                                @php
                                                    $order = \App\Order::find($order_id->id);
                                                    $order_details = $order->orderDetails->where('seller_id', Auth::user()->id)->first();
                                                @endphp
                                                @if($order != null)
                                                    <tr>
                                                        <td>
                                                            {{ $key+1 }}
                                                        </td>
                                                        <td>
                                                            <a href="#{{ $order->code }}" onclick="show_order_details({{ $order->id }})">{{ $order->code }}</a>
                                                        </td>
                                                        <td>
                                                            {{ count($order->orderDetails->where('seller_id', Auth::user()->id)) }}
                                                        </td>
                                                        <td>
                                                            <span class="badge badge--2 mr-4">
                                                                @if ($order_details->vendor_status == null)
                                                                    <i class="bg-red"></i> {{__('Invoice Not Generated')}}
                                                                @else
                                                                    <i class="bg-green"></i> {{__($order_details->vendor_status)}}
                                                                @endif
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="badge badge--2 mr-4">
                                                                @if ($order_details->vendor_status == null)
                                                                    <i class="bg-red"></i> {{__('Click Generate Invoice to Generate Order Invoice.')}}
                                                                @elseif ($order_details->vendor_status == "Invoice Generated")
                                                                    <i class="bg-red"></i> {{__('You need to wait for Invoice Payment.')}}
                                                                @elseif ($order_details->vendor_status == "Invoice Paid")
                                                                    <i class="bg-red"></i> {{__('Process for shipping.')}}
                                                                @endif
                                                            </span>
                                                        </td>
                                                        <td>
                                                            @if ($order_details->vendor_status == null)
                                                            <button onclick="vendor_generate_invoice({{ $order->id }})" class="btn btn-primary">{{__('Generate Invoice')}}</button>&nbsp;
                                                            @endif
                                                            @if ($order_details->vendor_status == "Invoice Paid")
                                                            <a href="{{route('orders.print_label',$order->id)}}" target="_blank" class="btn btn-primary">{{__('Print Shipping Label')}}</a>&nbsp;
                                                            <button onclick="order_shipping_info({{ $order->id }})" class="btn btn-primary">{{__('Order Shipped')}}</button>&nbsp;
                                                            @endif
                                                            @if($order->prescription_file != null)<a href="{{ asset('prescription/'.$order->prescription_file) }}" target="_blank" class="btn btn-primary">{{__('Download Prescription')}}</a> @endif
                                                            <button onclick="show_order_details({{ $order->id }})" class="btn btn-primary">{{__('Order Details')}}</button>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif

                        <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                {{ $orders->links() }}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="invoice-generate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="invoice-generate-modal-body">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="shipping_info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <form action="{{route('orders.shipping')}}" method="post">
                @csrf
                <div id="modal-body">
                    <div class="card mt-3">
                        <div class="card-header py-2 px-3 ">
                        <div class="heading-6 strong-600">{{__('Shipping Info')}}</div>
                        </div>
                        <div class="card-body pb-0">
                            <input type="hidden" name="order_id" id="modal_order_id">
                            <div class="form-group row">
                                <div class="col-sm-3 text-right pt-2">
                                    Courier &nbsp;
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name='courier' id="shippingModalCourier" placeholder="Courier Company Name" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3 text-right pt-2">
                                    AWB/Ref/Tracking No. &nbsp;
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="awb" class="form-control" id="shippingModalawb" placeholder="AWB Number" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('script')
<style>
@media (min-width: 992px) {
.datepicker-dropdown.dropdown-menu {
display: block;
opacity: 1;
visibility: visible;
z-index: 1060; 
}
}

</style>
    <!--Bootstrap Tags Input [ OPTIONAL ]-->
    <link href="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">


    <!--Bootstrap Datepicker [ OPTIONAL ]-->
    <script src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script>
    function vendor_generate_invoice(order_id)
    {
        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }
        $.post('{{ route('orders.vendor_invoice_generate') }}', { _token : '{{ csrf_token() }}', order_id : order_id}, function(data){
            $('#invoice-generate-modal-body').html(data);
            $('#invoice-generate').modal();
            $('.c-preloader').hide();
        });
    }
    
    function order_shipping_info(order_id)
    {
        $('#shippingModalCourier').val("");
        $('#shippingModalawb').val("");
        $('#modal_order_id').val(order_id);
        $('#shipping_info').modal();
        $('.c-preloader').hide();
    }
    
</script>
@endsection
