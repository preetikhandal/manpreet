@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-12 ">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Category Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
        	<span class="err text-danger"></span>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required value="{{$category->name}}">
                    </div>
                     <input type="hidden" name="digital" value="0" />
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="banner">{{__('Menu Banner')}}</label>
                    <div class="col-sm-4">
                        <input type="file" id="menu_banner" name="menu_banner" class="form-control" accept="image/png,image/jpg,image/jpeg">
						<small class="text-danger"> (490px * 550px)</small>
                    </div>
                
                    <label class="col-sm-2 control-label" for="banner">{{__('Desktop Icon')}}</label>
                    <div class="col-sm-4">
                        <input type="file" id="banner" name="banner" class="form-control" accept="image/png,image/jpg,image/jpeg">
						<small class="text-danger">(197px * 186px)</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="icon">{{__('Mobile Icon')}} </label>
                    <div class="col-sm-10">
                        <input type="file" id="icon" name="icon" class="form-control" accept="image/png,image/jpg,image/jpeg">
						<small class="text-danger">(197px * 186px)</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="bg_img">{{__('Background Image')}} </label>
                    <div class="col-sm-10">
                        <input type="file" id="bg_img" name="bg_img" class="form-control" accept="image/png,image/jpg,image/jpeg">
						<small class="text-danger">(222px * 250px)</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="bg_color">{{__('Background Color')}}</label>
                    <div class="col-sm-4">
                        <input type="text" id="bg_color" name="bg_color" value="{{$category->background_color}}" class="form-control" placeholder="{{__('Background Color')}}">
					
                    </div>
                
                    <label class="col-sm-2 control-label" for="text_color">{{__('Text Color')}}</label>
                    <div class="col-sm-4">
                        <input type="text" id="banner" name="text_color" value="{{$category->text_color}}" class="form-control" placeholder="{{__('Text Color')}}">
                    </div>
                </div>
                
				<div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Top Sku')}}<em> (Seperated by Comma)</em></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="top_sku">@if($category->top_sku != null){{ trim(implode(",", json_decode($category->top_sku,true))) }} @endif</textarea>
                    </div>
                </div>
               <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Title')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="meta_title" value="{{ $category->meta_title }}" placeholder="{{__('Meta Title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Description')}}</label>
                    <div class="col-sm-10">
                        <textarea name="meta_description" rows="8" class="form-control">{{ $category->meta_description }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Keywords')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="meta_keywords[]" value="{{ $category->meta_keywords }}" placeholder="Type to add a keyword" data-role="tagsinput">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Slug')}}</label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="{{__('Slug')}}" id="slug" name="slug" value="{{ $category->slug }}" class="form-control">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="position">{{__('Position')}}</label>
                    <div class="col-sm-4">
                    	@php
        					$dbposition=\App\Category::get();
        					$allposiotns=array();
        					foreach($dbposition as $value){
        							array_push($allposiotns,$value->position);
        					}						
        				@endphp
                        <select name="position" class="form-control demo-select2-placeholder">
    						<option value="{{$category->position}}" selected>{{$category->position}}</option>
    						@for($n=1;$n<=40;$n++)
    							@if(!in_array($n,$allposiotns))
    								<option value="{{$n}}">{{$n}}</option>
    							@endif
    						@endfor
    					</select>
                    </div>
                
                    <label class="col-sm-2 control-label" for="position">{{__('Discount (%)')}}</label>
                    <div class="col-sm-4">
                        <input type="text" min=0 value="{{ $category->discount }}" class="form-control" name="category_discount" placeholder="{{__('Category Discount')}}">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-1 control-label">{{__('Content')}}</label>
                    <div class="col-sm-11">
                        <textarea class="editor" name="category_content">{{ $category->content }}</textarea>
                    </div>
                </div>
                <div class="form-group">
					<div class="col-12" style="background:#e1e1e1;padding:5px">
						<h3>Return Policy</h3>
					</div>
				</div>
				<div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Return Days')}}</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="return_days" value="{{ $category->return_days }}" placeholder="{{__('eg. 7')}}" min="1" max="20">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Heading')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="policy_heading" value="{{ $category->policy_heading }}" placeholder="{{__('eg. 7 Days Returns')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Content')}}</label>
                    <div class="col-sm-10">
                        <textarea name="policy_content" rows="8" class="form-control" placeholder="eg. 7 Day FREE Returns. This item is eligible for return within 7 days of delivery.">{{ $category->policy_content }}</textarea>
                    </div>
                </div>
                @if (\App\BusinessSetting::where('type', 'category_wise_commission')->first()->value == 1)
                   <!-- <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">{{__('Commission Rate')}}</label>
                        <div class="col-sm-8">
                            <input type="number" min="0" step="0.01" id="commision_rate" name="commision_rate" value="{{ $category->commision_rate }}" class="form-control">
                        </div>
                        <div class="col-lg-2">
                            <option class="form-control">%</option>
                        </div>
                    </div>-->
                @endif
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Menu Display')}}</label>
                    <div class="col-sm-10">
                        <select name="display" class="form-control">
                            <option value="1" @if($category->display == 1) selected @endif>Show</option>
                            <option value="0" @if($category->display == 0) selected @endif>Hide</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
			<br/>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>
<script type="text/javascript">
	$('INPUT[type="file"]').change(function () {
		var ext = this.value.match(/\.(.+)$/)[1];
	    switch (ext) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			    $(".err").text('');
				$('#submitbtn').attr('disabled', false);
				break;
			default:
				$(".err").text('This is not an allowed file type.');
				this.value = '';
		}
	});
</script>
@endsection