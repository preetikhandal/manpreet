@extends('frontend.layouts.app')
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" >
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                        @include('frontend.inc.seller_side_nav')
                </div>
                <style>
                    .form-horizontal, .panel {     width: 100%; padding: 15px;background: #fff;}
                </style>
                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                       <div class="row">
	
</div>
@section('content')
@if($errors->any())
	<div class="alert alert-danger">
		<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
		<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
<div class="col-12 ">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Category Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('seller.savecategories') }}" method="POST" enctype="multipart/form-data">
        	@csrf
        	<span class="err text-danger"></span>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Name')}} <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                    <input type="hidden" name="digital" value="0" />
                    
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="banner">{{__('Menu Banner')}} </label>
                    <div class="col-sm-4">
                        <input type="file" id="menu_banner" name="menu_banner" class="form-control" accept="image/png,image/jpg,image/jpeg">
						<small class="text-danger"> (490px * 550px)</small>
                    </div>
                
                    <label class="col-sm-2 control-label" for="banner">{{__('Desktop Icon')}} </label>
                    <div class="col-sm-4">
                        <input type="file" id="banner" name="banner" class="form-control" accept="image/png,image/jpg,image/jpeg">
						<small class="text-danger">(197px * 186px)</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="icon">{{__('Mobile Icon')}} </label>
                    <div class="col-sm-10">
                        <input type="file" id="icon" name="icon" class="form-control" accept="image/png,image/jpg,image/jpeg">
						<small class="text-danger">(197px * 186px)</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="bg_img">{{__('Background Image')}} </label>
                    <div class="col-sm-10">
                        <input type="file" id="bg_img" name="bg_img" class="form-control" accept="image/png,image/jpg,image/jpeg">
						<small class="text-danger">(222px * 250px)</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="bg_color">{{__('Background Color')}}</label>
                    <div class="col-sm-4">
                        <input type="text" id="bg_color" name="bg_color" value="{{ old('bg_color') }}" class="form-control" placeholder="{{__('Background Color')}}">
                    </div>
                
                    <label class="col-sm-2 control-label" for="text_color">{{__('Text Color')}}</label>
                    <div class="col-sm-4">
                        <input type="text" id="banner" name="text_color" value="{{ old('text_color') }}" class="form-control" placeholder="{{__('Text Color')}}">
					
                    </div>
                </div>
                
				<div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Top SKU')}}<em> (Seperated By Comma)</em></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="top_sku"></textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Title')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="meta_title" placeholder="{{__('Meta Title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Description')}}</label>
                    <div class="col-sm-10">
                        <textarea name="meta_description" rows="8" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Keywords')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="meta_keywords[]" value="{{ old('meta_keywords[]') }}" placeholder="Type to add a keyword" data-role="tagsinput">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="position">{{__('Position')}}</label>
                    <div class="col-sm-4">
                        @php
        					$dbposition=\App\Category::get();
        					$allposiotns=array();
        					foreach($dbposition as $value){
        							array_push($allposiotns,$value->position);
        					}						
        				@endphp
                        <select name="position" class="form-control demo-select2-placeholder">
							@for($n=1;$n<=40;$n++)
    							@if(!in_array($n,$allposiotns))
    								<option value="{{$n}}">{{$n}}</option>
    							@endif
    						@endfor
                        </select>
                    </div>
                
                    <label class="col-sm-2 control-label" for="position">{{__('Discount (%)')}}</label>
                    <div class="col-sm-4">
                        <input type="text" min=0 value="0" class="form-control" name="category_discount" placeholder="{{__('Category Discount')}}">
                    </div>
                </div>
                
                
				<div class="form-group">
                    <label class="col-sm-1 control-label">{{__('Content')}}</label>
                    <div class="col-sm-11">
                        <textarea name="category_content" class="editor"></textarea>
                    </div>
                </div>
                <div class="form-group">
					<div class="col-12" style="background:#e1e1e1;padding:5px">
						<h3>Return Policy</h3>
					</div>
				</div>
				<div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Return Days')}}</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="return_days" value="{{ old('return_days') }}" placeholder="{{__('eg. 7')}}" min="1" max="20">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Heading')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="policy_heading" value="{{ old('policy_heading') }}" placeholder="{{__('eg. 7 Days Returns')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Content')}}</label>
                    <div class="col-sm-10">
                        <textarea name="policy_content" rows="8" class="form-control" placeholder="eg. 7 Day FREE Returns. This item is eligible for return within 7 days of delivery.">{{ old('policy_content') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Menu Display')}}</label>
                    <div class="col-sm-10">
                        <select name="display" class="form-control">
                            <option value="1">Show</option>
                            <option value="0">Hide</option>
                        </select>
                    </div>
                </div>
                @if (\App\BusinessSetting::where('type', 'category_wise_commission')->first()->value == 1)
                    <!--<div class="form-group">
                        <label class="col-sm-2 control-label" for="name">{{__('Commission Rate')}}</label>
                        <div class="col-sm-8">
                            <input type="number" min="0" step="0.01" placeholder="{{__('Commission Rate')}}" id="commision_rate" name="commision_rate" class="form-control">
                        </div>
                        <div class="col-lg-2">
                            <option class="form-control">%</option>
                        </div>
					</div>-->
                @endif
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit" id="submitbtn">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>
<script type="text/javascript">
	$('INPUT[type="file"]').change(function () {
		var ext = this.value.match(/\.(.+)$/)[1];
	    switch (ext) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			    $(".err").text('');
				$('#submitbtn').attr('disabled', false);
				break;
			default:
				$(".err").text('This is not an allowed file type.');
				this.value = '';
		}
	});
</script>
@endsection