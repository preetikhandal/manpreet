@extends('frontend.layouts.app')

@section('content')
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @if(Auth::user()->user_type == 'seller')
                        @include('frontend.inc.seller_side_nav')
                    @elseif(Auth::user()->user_type == 'customer')
                        @include('frontend.inc.customer_side_nav')
                    @endif
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        @if($errors->any())
							<div class="alert alert-danger">
								<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
								<ul>
									@foreach($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
                        <!-- Page title -->
                        <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-12">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 text-white">
                                        {{__('Manage Profile')}}
                                    </h2>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}" class="text-white">{{__('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}" class="text-white">{{__('Dashboard')}}</a></li>
                                            <li class="active"><a href="{{ route('profile') }}" class="text-white">{{__('Manage Profile')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form class="" action="{{ route('seller.profile.update') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{__('Basic info')}}
                                </div>
                                <div class="form-box-content p-3">
                                    @if(isset(Auth::user()->name))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Your Name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Your Name')}}" name="name" value="{{ Auth::user()->name }}">
                                        </div>
                                    </div>
                                    @else
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Your Name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Your Name')}}" name="name" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    
                                    @endif
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Your Email')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="email" class="form-control mb-3" placeholder="{{__('Your Email')}}" name="email" value="{{ Auth::user()->email }}" @if(isset(Auth::user()->email)) readonly @endif>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Photo')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="photo" id="file-3" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-3" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{__('Choose image')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                    
                                    @if(!isset(Auth::user()->password))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Your Password')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control mb-3" placeholder="{{__('New Password')}}" name="new_password">
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Confirm Password')}} </label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control mb-3" placeholder="{{__('Confirm Password')}}" name="confirm_password">
                                        </div>
                                    </div>
                                    
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{__('Shipping info')}}
                                </div>
                                <div class="form-box-content p-3">
                                   @if(isset(Auth::user()->address))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Address')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea class="form-control textarea-autogrow mb-3" placeholder="{{__('Your Address')}}" rows="1" value="{{ (Auth::user()->address) ? Auth::user()->address : old('address')}}" name="address" >{{ Auth::user()->address }}</textarea>
                                        </div>
                                    </div>
                                    @else
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Address')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea class="form-control textarea-autogrow mb-3" placeholder="{{__('Your Address')}}" rows="1" value="{{ old('address') }}" name="address" ></textarea>
                                        </div>
                                    </div>
                                    @endif
                                   
                                    
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Country')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <select class="form-control mb-3 selectpicker" data-placeholder="{{__('Select your country')}}" name="country">
                                                    @foreach (\App\Country::all() as $key => $country)
                                                        <option value="{{ $country->name }}" <?php if(Auth::user()->country == $country->name) echo "selected";?> >{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    @if(isset(Auth::user()->city))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('City')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Your City')}}" name="city"  value="{{ Auth::user()->city }}">
                                        </div>
                                    </div>
                                    @else
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('City')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Your City')}}" name="city"  value="{{ old('city') }}">
                                        </div>
                                    </div>
                                    @endif
                                    
                                    @if(isset(Auth::user()->postal_code))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Postal code')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Your Postal Code')}}" name="postal_code"  value="{{ Auth::user()->postal_code }}" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6">
                                        </div>
                                    </div>
                                    @else
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Postal code')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Your Postal Code')}}" name="postal_code"  value="{{ old('postal_code') }}" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6">
                                        </div>
                                    </div>
                                    @endif
                                    
                                    @if(isset(Auth::user()->phone))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Phone')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Your Phone Number')}}" name="phone" value="{{ (Auth::user()->phone) ? Auth::user()->phone : old('phone')}}" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10" @if(isset(Auth::user()->phone)) readonly @endif>
                                        </div>
                                    </div>
                                    @else
                                      <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Phone')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Your Phone Number')}}" name="phone" value="{{ old('phone') }}" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10">
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{__('Payment Setting')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Cash Payment')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <label class="switch mb-3">
                                                <input value="1" name="cash_on_delivery_status" type="checkbox" @if (Auth::user()->seller->cash_on_delivery_status == 1) checked @endif>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Bank Payment')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <label class="switch mb-3">
                                                <input value="1" name="bank_payment_status" type="checkbox" @if (Auth::user()->seller->bank_payment_status == 1) checked @endif>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                  
                                @if(isset(Auth::user()->seller->bank_name))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Bank Name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Bank Name')}}" value="{{Auth::user()->seller->bank_name }}" name="bank_name">
                                        </div>
                                    </div>
                                @else
                                <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Bank Name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Bank Name')}}" value="{{ old('bank_name') }}" name="bank_name">
                                        </div>
                                    </div>
                                @endif
                                
                                @if(isset(Auth::user()->seller->bank_acc_name))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Bank Account Name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Bank Account Name')}}" value="{{ Auth::user()->seller->bank_acc_name }}" name="bank_acc_name">
                                        </div>
                                    </div>
                                @else
                                <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Bank Account Name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Bank Account Name')}}" value="{{ old('bank_acc_name') }}" name="bank_acc_name">
                                        </div>
                                    </div>
                                @endif
                                @if(isset(Auth::user()->seller->bank_acc_no))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Bank Account Number')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Bank Account Number')}}" value="{{ Auth::user()->seller->bank_acc_no }}" name="bank_acc_no">
                                        </div>
                                    </div>
                                @else
                                <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Bank Account Number')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Bank Account Number')}}" value="{{ old('bank_acc_no') }}" name="bank_acc_no">
                                        </div>
                                    </div>
                                @endif
                                 @if(isset(Auth::user()->seller->bank_routing_no))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Bank Routing Number')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control mb-3" placeholder="{{__('Bank Routing Number')}}" value="{{ Auth::user()->seller->bank_routing_no }}" name="bank_routing_no" >
                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Bank Routing Number')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control mb-3" placeholder="{{__('Bank Routing Number')}}" value="{{ old('bank_routing_no') }}" name="bank_routing_no" >
                                        </div>
                                    </div>
                                @endif
                                @if(isset(Auth::user()->order_cancel_credit))
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Order cancel credit limit')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3"  value="{{ Auth::user()->seller->order_cancel_credit }}" readonly>
                                        </div>
                                        
                                    </div>
                                </div>
                              
                                
                                @endif
                            </div>
                            <div class="text-right mt-4">
                                <input type="submit" value="Update Profile" class="btn btn-styled btn-base-1">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
