@extends('frontend.layouts.app')

@section('content')
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
   background:#20b34e!important;
}
.process-steps li {
    width: 20%;
    float: left;
    text-align: center;
    position: relative;
}
.select2-container--default .select2-selection--single {
    border-radius: 2px;
    border: 1px solid #e6e6e6;
    background-color: #fff!important;
    font-size: 0.875rem;
    font-weight: 400;
    color: rgba(0, 0, 0, 0.4);
    height: calc(2.25rem + 2px);
    padding: 0 .75rem;
    outline: none;
}
</style>
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('frontend.inc.seller_side_nav')
                </div>
                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="row mb-2">
                            <div class="col-12 p-0">
                                <div class="page-title bg-blue p-3" style="border-radius: 0.25rem; ">
                                    <div class="row align-items-center">
                                        <div class="col-md-6">
                                            <h2 class="heading heading-6 text-capitalize strong-600 mb-0 text-white p-0">
                                                {{__('Orders Recived')}}
                                            </h2>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="float-md-right">
                                                <ul class="breadcrumb">
                                                    <li><a href="{{ route('home') }}" class="text-white">{{__('Home')}}</a></li>
                                                    <li><a href="{{ route('dashboard') }}" class="text-white">{{__('Dashboard')}}</a></li>
                                                    <li class="active"><a href="{{ route('orders.index') }}" class="text-white">{{__('Orders')}}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="page-title bg-white p-3" style="border-radius: 0.25rem;">
                            <form method="get">
                            <div class="row">
                                <div class="col-md-6 text-right pt-2">Filter Product : &nbsp;</div>
                                <div class="col-md-5">
                                       <select name="order_filter" class="form-control">
                                            <option value="1" @if($filter == 1) selected @endif>Pending & Ready to Delivery</option>
                                            <option value="2" @if($filter == 2) selected @endif>Pending Only</option>
                                            <option value="3" @if($filter == 3) selected @endif>Ready to Delivery Only</option>
                                            <option value="4" @if($filter == 4) selected @endif>On Shipping</option>
                                            <option value="5" @if($filter == 5) selected @endif>Delivered</option>
                                        </select>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-primary bg-blue" style="border-color:#232F3E">Go</button>
                                </div>
                            </div>
                            </form>
                        </div>

                      
                        <div class="row mt-2">
                        @if (count($orders) > 0)
                            @foreach ($orders as $key => $order)
                                @php
                                    $order = \App\Order::find($order->id);
                                    $orderDetails = $order->orderDetails->where('seller_id', Auth::user()->id);
									
								@endphp
                                @foreach ($orderDetails as $key => $orderDetail)
                                     @php
                                        $status = $orderDetail->delivery_status;
                                    @endphp
                                    <article class="card card-product-list">
                                    	<div class="row no-gutters align-items-center">
                                    		<div class="col-md-2 col-3">
                                    			<a href="{{ route('product', $orderDetail->product->slug) }}" >
                                			    	@if($orderDetail->product->thumbnail_img!="")
                                    			    	<img src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($orderDetail->product->thumbnail_img) }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                                    			    @else
                                    			        <img src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                                    			    @endif
                                    			</a>
                                    		</div> 
                                    		<div class="col-md-7 col-9 ">
                                    			<div class="info-main ">
                                    			    @if ($orderDetail->product != null)
                                    			    	<a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank" class="h5 title">{{ $orderDetail->product->name }}</a>
                                    		        @else
                                                        <strong>{{ __('Product Unavailable') }}</strong>
                                                    @endif
                                    		        <p class="info">Order 
                                    		            <a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="text-blue font-weight-bold"># {{ $order->code }}</a> 
                                    		            <span class="block-sm">Delivery Status
                                        		           
                                                            @if(str_replace('_', ' ', $status)=="pending")
                                                                <span class="badge badge-info"> {{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                                                            @else
                                                                <span class="badge badge-success"> {{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                                                            @endif
                                    		            </span>
                                    		        </p>
                                    		        <p class="info">Order Date <b class="text-blue">{{ date('d-m-Y', $order->date) }} </b>
                                    		           <span class="block-sm">Payment Status 
                                    		             @if ($order->payment_status == 'paid')
                                    		                <span class="badge badge-success"> {{__('Paid')}}</span>
                                                         @else
                                                            <span class="badge badge-warning">{{__('Unpaid')}}</span>
                                                         @endif
                                    		           </span>
                                    		        </p>
                                    		        <p class="info">
                                    		            <span class="block-sm">Customer
                                    		                <strong>@if ($order->user_id != null)
                                    		                   @if(isset($order->user->name))
                                                                {{ $order->user->name }}
                                                                @endif
                                                            @else
                                                                Guest ({{ $order->guest_id }})
                                                            @endif</strong>
                                    		            </span>
                                    		            <span class="block-sm">Qty
                                    		                <strong>{{ count($orderDetails) }}</strong>
                                    		            </span>
                                    		        </p>
                                    		        <span class="price h5 d-block d-md-none">Product Price {{ single_price($orderDetail->price) }} </span>
                                    			</div>
                                    		</div> 
                                    		<div class="col-12 d-block d-md-none mb-2 px-2 mobilebtn">
                                    		    @if($status == "pending")
                                                 <button onclick="confirm_order({{$order->id}}, 'ready_to_ship')" class="btn btn-light bg-blue btn-block">Ready to Ship</button>&nbsp;
                                                 <button onclick="show_order_details({{ $order->id }})" class="btn btn-light btn-block text-white" style="background:#131921;">{{__('Order Details kk')}}</button>&nbsp;
                                                 <button onclick="#" class="btn btn-light btn-block text-white" style="background:#131921;">{{__('Order Cancel')}}</button>&nbsp;
                                                @elseif($status == "ready_to_ship")
                                                 <button onclick="order_shipping_info({{ $order->id }})" class="btn btn-light bg-blue btn-block">{{__('Add Shipping Info')}}</button>&nbsp;
                                                 <a href="{{ route('orders.printlabel.seller', [Auth::User()->id, $order->id]) }}" target="_blank" class="btn btn-light btn-block text-white" style="background:#131921;">{{__('Print Shipping Label')}}</a>&nbsp;
                                                @endif
                                    		</div>
                                    		<div class="col-md-3 d-none d-md-block">
                                    			<div class="info-aside">
                                    				<div class="price-wrap">
                                    					<span class="price h5"><span class="info">Product Price</span> {{ single_price($orderDetail->price) }} </span>	
                                    				</div> <br/>
                                    				<p class="d-none d-md-block">
                                    				    @if($status == "pending")
                                                        <button onclick="confirm_order({{$order->id}}, 'ready_to_ship')" class="btn btn-light bg-blue btn-block">Ready to Ship</button>
                                                        <button onclick="show_order_details({{ $order->id }})" class="btn btn-light btn-block text-white" style="background:#131921;">{{__('Order Details')}}</button>
                                                         <button type="button" class="btn btn-light btn-block float-right text-white mb-3" data-toggle="modal" data-target="#exampleModal2" onclick="orderCancel('{{$order->id}}')" style="background-color: #eb3037">{{__('Cancel Order')}}</button>
                                                        @elseif($status == "ready_to_ship")
                                                        <button onclick="order_shipping_info({{ $order->id }})" class="btn btn-light bg-blue btn-block">{{__('Add Shipping Info')}}</button>&nbsp;
                                                        <a href="{{ route('orders.printlabel.seller', [Auth::User()->id, $order->id]) }}" target="_blank" class="btn btn-light btn-block text-white" style="background:#131921;">{{__('Print Shipping Label')}}</a>&nbsp;
                                                        @endif
                                    				</p>
                                    			</div> 
                                    		</div> 
                                    	</div> 
                                    </article>
                                @endforeach
                            @endforeach
                        @else
							<div class="card no-border mt-4">
                                <div>
									<table class="table table-sm table-hover table-responsive-md">
                                        <thead>
                                            <tr class="text-center">
                                                <th><h3>{{__('No Order History Found')}}</h3></th>
                                            </tr>
                                        </thead>
									</table>
								</div>
							</div>
                        @endif
                    </div>

                        <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                {{ $orders->links() }}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="shipping_info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <form action="{{route('orders.shipping')}}" method="post">
                @csrf
                <div id="modal-body">
                    <div class="card mt-3">
                        <div class="card-header py-2 px-3 ">
                        <div class="heading-6 strong-600">{{__('Shipping Info')}}</div>
                        </div>
                        <div class="card-body pb-0">
                            <input type="hidden" name="order_id" id="modal_order_id">
                            <input type="hidden" name="seller_id" value="{{Auth::User()->id}}">
                            <div class="form-group row">
                                <div class="col-sm-3 text-right pt-2">
                                    Courier &nbsp;
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name='courier' id="shippingModalCourier" placeholder="Courier Company Name" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3 text-right pt-2">
                                    AWB/Ref/Tracking No. &nbsp;
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="awb" class="form-control" id="shippingModalawb" placeholder="AWB Number" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>

@endsection

<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order cancel</h5>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          Warrning:- You can cancel only 2 credit order in this month if you order credit limit will be zero then 5 % amount will be debit from your wallet
        @php
           $seller =  \App\Seller::where('user_id',Auth::user()->id)->get();
                                         
        @endphp
          
          Your Order Credit Limit Is <strong>{{$seller[0]->order_cancel_credit}}</strong>
          <br>
           
          <strong>Rs. <span id="deductedAmont"></span></strong> Amount has been deduct from your wallet if your order credit limit will zero
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" style="background-color: #eb3037;" data-dismiss="modal">Cancel</button>
        
        <!--<button type="button" class="btn btn-primary">Confirm</button>-->
         <form action="{{ route('purchase_history.order_cancel') }}" method="post" class="m-0">
                @csrf <input type="hidden" id="cancelOrderId" name="order_id"/>
            <button type="submit" class="btn btn-light bg-blue float-right text-white">{{__('Confirm')}}</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--END MODEL-->

<script>
    function orderCancel($orderId){
        
        $('#cancelOrderId').val($orderId);
         $.post('{{ route('orders.get_deducted_cancel_amount') }}', {_token:'{{ @csrf_token() }}', order_id:$orderId}, function(data){
                 //location.reload();
                 //alert('amount'+ data);
                 $('#deductedAmont').html(data);
            });
        
    }
    
</script>

@section('script')
<script>
    function order_shipping_info(order_id)
    {
        $('#shippingModalCourier').val("");
        $('#shippingModalawb').val("");
        $('#modal_order_id').val(order_id);
        $('#shipping_info').modal();
        $('.c-preloader').hide();
    }
    
        function confirm_order(order_id, status) {
            var setFlash = 'Order status has been updated';
            seller_id = {{Auth::User()->id}};
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, seller_id : seller_id}, function(data){
                 location.reload();
            });
        }
</script>
@endsection

