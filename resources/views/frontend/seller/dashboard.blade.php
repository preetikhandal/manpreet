@extends('frontend.layouts.app')

@section('content')
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
    background:#20b34e!important;
}
    .panel-order .row {
    	border-bottom: 1px solid #ccc;
    }
    .panel-order .row:last-child {
    	border: 0px;
    }
    .panel-order .row .col-md-1  {
    	text-align: center;
    	padding-top: 15px;
    }
    .panel-order .row .col-md-1 img {
    	width: 50px;
    	max-height: 50px;
    }
    .panel-order .row .row {
    	border-bottom: 0;
    }
    .panel-order .row .col-md-11 {
    	border-left: 1px solid #ccc;
    }
    .panel-order .row .row .col-md-12 {
    	padding-top: 7px;
    	padding-bottom: 7px; 
    }
    .panel-order .row .row .col-md-12:last-child {
    	font-size: 11px; 
    	color: #555;  
    	background: #efefef;
    }
    .panel-order .btn-group {
    	margin: 0px;
    	padding: 0px;
    }
    .panel-order .panel-body {
    	padding-top: 0px;
    	padding-bottom: 0px;
    }
    .panel-order .panel-deading {
    	margin-bottom: 0;
    } 
    .prodname{
        font-weight:bold;
        font-size:14px;
    }
    .prodname::after{
        content:",";
    }
    .prodname:last-child::after{
        content:" ";
    }
    .total,.totalpricevalue{
        font-size:13px;
    }
    .totalpricevalue{font-weight:800;}
    .label {
      color: white;
      padding: 5px;
    }
</style>
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid d-none d-lg-block">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('frontend.inc.seller_side_nav')
                </div>

                <div class="col-lg-9">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    {{__('Dashboard')}}
                                </h2>
                            </div>
                            <div class="col-md-6">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                        <li class="active"><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dashboard content -->
                    <div class="">
                        <div class="row">
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center green-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-upload"></i>
                                        <span class="d-block title heading-3 strong-400">{{ count(\App\Product::where('user_id', Auth::user()->id)->get()) }}</span>
                                        <span class="d-block sub-title">{{__('Products')}}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center red-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-cart-plus"></i>
                                        <span class="d-block title heading-3 strong-400">{{ count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'delivered')->get()) }}</span>
                                        <span class="d-block sub-title">{{__('Total sale')}}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center red-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-cart-plus"></i>
                                        <span class="d-block title heading-3 strong-400">
                                            @php
                                           $seller =  \App\Seller::where('user_id',Auth::user()->id)->get();
                                         
                                           @endphp
                                             {{$seller[0]->order_cancel_credit}}
                                        </span>
                                        <span class="d-block sub-title">{{__('order Credit Limit')}}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center red-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-cart-plus"></i>
                                        <span class="d-block title heading-3 strong-400">
                                            @php
                                            $wallets = \App\Wallet::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->paginate(9);
        
                                                $cashWallet = \App\Wallet::where('user_id', Auth::user()->id)->where('payment_method','cashback')->sum('amount');
                                                $paywallet = \App\Wallet::where('user_id', Auth::user()->id)->where('payment_method','Wallet Payment')->sum('amount');
                                                $totalWallet  =  $cashWallet-$paywallet;
                                            @endphp
                                            {{ $totalWallet }}</span>
                                        <span class="d-block sub-title">{{__('Wallet Amount')}}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center blue-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-rupee"></i>
                                        @php
                                            $orderDetails = \App\OrderDetail::where('seller_id', Auth::user()->id)->get();
                                            $total = 0;
                                            foreach ($orderDetails as $key => $orderDetail) {
                                                if($orderDetail->order->payment_status == 'paid'){
                                                    $total += $orderDetail->price;
                                                }
                                            }
                                        @endphp
                                        <span class="d-block title heading-3 strong-400">{{ single_price($total) }}</span>
                                        <span class="d-block sub-title">{{__('Total earnings')}}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center yellow-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-check-square-o"></i>
                                        <span class="d-block title heading-3 strong-400">{{ count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'delivered')->get()) }}</span>
                                        <span class="d-block sub-title">{{__('Successful orders')}}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2 text-center">
                                        {{__('Orders')}}
                                    </div>
                                    <div class="form-box-content p-3">
                                        <table class="table mb-0 table-bordered" style="font-size:14px;">
                                            <tr>
                                                <td>{{__('Total orders')}}:</td>
                                                <td><strong class="heading-6">{{ count(\App\OrderDetail::where('seller_id', Auth::user()->id)->get()) }}</strong></td>
                                            </tr>
                                            <tr >
                                                <td>{{__('Pending orders')}}:</td>
                                                <td><strong class="heading-6">{{ count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'pending')->get()) }}</strong></td>
                                            </tr>
                                            <tr >
                                                <td>{{__('Cancelled orders')}}:</td>
                                                <td><strong class="heading-6">{{ count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'cancelled')->get()) }}</strong></td>
                                            </tr>
                                            <tr >
                                                <td>{{__('Successful orders')}}:</td>
                                                <td><strong class="heading-6">{{ count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'delivered')->get()) }}</strong></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
								<div class="bg-white mt-4 p-4 text-center">
                                    <div class="heading-4 strong-700">{{__('Shop')}}</div>
                                    <p>{{__('Manage & organize your shop')}}</p>
                                    <a href="{{ route('shops.index') }}" class="btn btn-styled bg-blue btn-sm">{{__('Go to setting')}}</a>
                                </div>
                                <div class="bg-white mt-4 p-4 text-center">
                                    <div class="heading-4 strong-700">{{__('Payment')}}</div>
                                    <p>{{__('Configure your payment method')}}</p>
                                    <a href="{{ route('profile') }}" class="btn btn-styled bg-blue btn-sm">{{__('Configure Now')}}</a>
                                </div>
                                <!--<div class="bg-white mt-4 p-5 text-center">
                                    <div class="mb-3">
                                        @if(Auth::user()->seller->verification_status == 0)
                                            <img loading="lazy"  src="{{ asset('frontend/images/icons/non_verified.png') }}" alt="" width="130">
                                        @else
                                            <img loading="lazy"  src="{{ asset('frontend/images/icons/verified.png') }}" alt="" width="130">
                                        @endif
                                    </div>
                                    @if(Auth::user()->seller->verification_status == 0)
                                        <a href="{{ route('shop.verify') }}" class="btn btn-styled btn-base-1">{{__('Verify Now')}}</a>
                                    @endif
                                </div>-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2 text-center">
                                        {{__('Products')}}
                                    </div>
                                    <div class="form-box-content p-3 category-widget">
                                        <ul class="clearfix">
                                            @foreach (\App\Category::all() as $key => $category)
                                                @if(count($category->products->where('user_id', Auth::user()->id))>0)
                                                    <li><a>{{ __($category->name) }}<span>({{ count($category->products->where('user_id', Auth::user()->id)) }})</span></a></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                        <div class="text-center mt-2">
                                            <a href="{{ route('seller.products.upload')}}" class="btn bg-blue">{{__('Add New Product')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid d-block d-lg-none" style="max-width:100%!important">
            <div class="row">
                <div class="col-12">
                    <div class="page-title">
                        <div class="row align-items-center bg-blue py-2">
                            <div class="col-12">
                                <h2 class="heading heading-3 text-capitalize strong-600 mb-0 p-0 text-white">
                                    {{__('Seller account')}}
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mobileaccount">
                    <div class="widget-profile-menu py-3">
                        <ul class="categories categories--style-3">
                            <li>
                                <a href="{{ route('seller.products') }}">
                                    <span class="category-name">
                                        {{__('Products')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('product_bulk_upload.index')}}">
                                    <span class="category-name">
                                        {{__('Product Bulk Upload')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('ordersTabular') }}">
                                    <span class="category-name">
                                        {{__('Orders')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('reviews.seller') }}">
                                    <span class="category-name">
                                        {{__('Product Reviews')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('shops.index') }}">
                                    <span class="category-name">
                                        {{__('Shop Setting')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('withdraw_requests.index') }}">
                                    <span class="category-name">
                                        {{__('Money Withdraw')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12">
                    <div class="page-title">
                        <div class="row align-items-center bg-blue py-2">
                            <div class="col-12">
                                <h2 class="heading heading-3 text-capitalize strong-600 mb-0 p-0 text-white">
                                    {{__('User account')}}
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mobileaccount">
                    <div class="widget-profile-menu py-3">
                        <ul class="categories categories--style-3">
                            <li>
                                <a href="{{ route('purchase_history.index') }}">
                                    <span class="category-name">
                                        {{__('Your Orders')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('wishlists.index') }}" >
                                    <span class="category-name">
                                        {{__('Wishlist')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('orders.track') }}" >
                                    <span class="category-name">
                                        {{__('Track Order')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('customer_refund_track') }}" >
                                    <span class="category-name">
                                        {{__('Track Refund Request')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('profile') }}" >
                                    <span class="category-name">
                                        {{__('Manage Profile')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            @if (\App\BusinessSetting::where('type', 'wallet_system')->first()->value == 1)
                                <li>
                                    <a href="{{ route('wallet.index') }}" >
                                        <span class="category-name">
                                            {{__('My Wallet')}}
                                        </span>
                                        <i class="fa fa-chevron-right float-right"></i>
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ route('support_ticket.index') }}" >
                                    <span class="category-name">
                                        {{__('Support Ticket')}}
                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>  
    </section>

@endsection
