@extends('frontend.layouts.app')

@section('content')
<!-- Seller Style -->

<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" >
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                       @include('frontend.inc.seller_side_nav') 
                </div>
                <style>
                    .form-horizontal, .panel {     width: 100%; padding: 15px;background: #fff;}
                </style>
                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                       <div class="row">
	
</div>

<div class="row mt-4">
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all">
            <h3 class="panel-title pull-left pad-no" style="font-size: 22px;">Seller Order List</h3>
           
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Order Code')}}</th>
                    
                    <th>{{__('Customer')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Discount Amount')}}</th>
                    <th>{{__('Order Status')}}</th>
                    <th>{{__('Pay Method')}}</th>
                    <th>{{__('Pay Status')}}</th>
                    <th>{{__('Seller Amount')}}</th>
                    <th>{{__('Service Provider Comission')}}</th>
                    <th>{{__('Product Discount')}}</th>
                    <th>{{__('Inclusive Comission')}}</th>
                    <th>{{__('Exclusive Comission')}}</th>
                    <th>{{__('Credit to seller wallet')}}</th>
                   
                </tr>
            </thead>
            <tbody>
               
                @foreach ($orders as $key => $order_id)
                    @php
                        $seller_id = $order_id->seller_id;
                        $order = \App\Order::find($order_id->id);
                       
                        $orderDetails = \App\OrderDetail::find($order_id->order_id);
                        
                    @endphp
                    @if($order != null)
                        @php
                        if ($order->orderDetails->where('seller_id',  $seller_id)->first()->payment_status == 'paid') {
                            $payment_status = "paid";
                        } else {
                            $payment_status = "unpaid";
                        }
                        @endphp
                        <tr>
                            <td>
                                {{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }} 
                            </td>
                            <td>
                               <a href="#" class="ordcode"> {{ $order->code }}</a>@if($order->viewed == 0) <span class="pull-right badge badge-info">{{ __('New') }}</span> @endif
                            </td>
                           
                            <td>
                                @if ($order->user_id != null)
                                    {{ $order->user->name??' ' }}
                                @else
                                    Guest ({{ $order->guest_id }})
                                @endif
                            </td>
                            <td>
								{{ single_price($order->orderDetails->where('seller_id', $seller_id)->sum('price') + $order->orderDetails->where('seller_id', $seller_id)->sum('tax')) }}
		
                            </td>
                            <td>Type discount</td>
                            <td>
                                @php
                                    $status = $order->orderDetails->where('seller_id', $seller_id)->first()->delivery_status;
                                @endphp
                                @if($status == "Confirmation Pending")
                                <label class="label label-danger">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @elseif($status == "pending")
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @else
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @endif
                            </td>
                            <td>
                                {{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}
                            </td>
                            <td>
                                <span class="badge badge--2 mr-4">
                                    @if ($payment_status == 'paid')
                                        <i class="bg-green"></i> Paid
                                    @else
                                        <i class="bg-red"></i> Unpaid
                                    @endif
                                </span>
                            </td>
                            <td>{{single_price($order->service_provider)}}</td>
                            <td>{{$order->seller_commission}}</td>
                            @php
                               $getPrice = \App\OrderDetail::where('order_id', $order_id->id)->get(); 
                               
                              
                               if(isset($getPrice[0]->inclusive_price)){
                                   $comissionselleralde = single_price($order->seller_commission);
                                }else{
                                    $comissionselleralde = 0;
                                }
                               
                                if(isset($getPrice[0]->exclusive_price)){
                                   $comissionselleraldeex = single_price($order->seller_commission);
                                }else{
                                    $comissionselleraldeex = 0;
                                }
                            @endphp
                            <td>
                                @php
                                $discountAmount = $order->service_provider - $order->seller_commission ;
                                @endphp
                                {{single_price($getPrice[0]->discount)}}
                                
                            </td>
                            <td>{{$comissionselleralde}}</td>
                            <td>{{$comissionselleraldeex}}</td>
                            <td>{{$order->paid_to_seller_account}}</td>
                            <td> <button onclick="show_order_details({{ $order->id }})" class="btn btn-light btn-block text-white" style="background:#131921;">{{__('Order Details')}}</button>&nbsp;</td>
                            
                        </tr>
                     @endif
                @endforeach
            </tbody>
            </table>
            
            
        </div>
        <div class="clearfix">
        <!--<div class="pull-right">-->
        <!--    {{ $orders->appends(request()->input())->links() }}-->
        <!--</div>-->
    </div>
    </div>
    
</div>

<div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>

<script>
    $(function(){
    var $select = $(".percent");
    for (i=1;i<=100;i++){
        $select.append($('<option></option>').val(i).html(i))
    }
});
</script>

                   
                    </div>
                </div>
            </div>
        </div>
    </section>

    
@endsection

