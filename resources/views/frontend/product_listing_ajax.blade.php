@foreach ($products as $key => $product)
<input type="hidden" id="totalpages" value="{{$products->lastPage()}}">
    @php
        $home_base_price = home_base_price($product->id);
        $home_discounted_base_price = home_discounted_base_price($product->id);
        if($home_base_price == "₹ 0.00") {
            $product->current_stock = 0;
            $qty = 0;
        }
    @endphp
<div class="col-md-3 col-6" id="results">
    <article class="list-product">
		<div class="img-block">
			 @if($mobileapp == 0)
				<a href="{{ route('product', $product->slug) }}" class="thumbnail">
				@else
				<a href="{{ route('product', ['slug' => $product->slug,  'android' => 1]) }}" class="thumbnail">
				@endif
			    @if($product->thumbnail_img != null)
				<img class="mx-auto d-block lazyload  img-fluid" src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" />
				@else
			    <img class="mx-auto d-block lazyload  img-fluid" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
				@endif
			</a>
		</div>
		@if($product->current_stock != 0)
			@if($home_base_price != $home_discounted_base_price)
			<ul class="product-flag">
				<li class="new discount-price bg-orange">{{discount_calulate($home_base_price, $home_discounted_base_price )}}% off</li>
			</ul>
			@endif
		@endif
		<div class="product-decs text-center">
			@if($mobileapp == 0)
			<h2><a href="{{ route('product', $product->slug) }}" class="product-link">{{ __(ucwords(strtolower($product->name))) }}</a></h2>
			@else
			<h2><a href="{{ route('product', ['slug' => $product->slug,  'android' => 1]) }}" class="product-link">{{ __(ucwords(strtolower($product->name))) }}</a></h2>
			@endif
			<div class="pricing-meta">
				<ul>
				    @if($product->current_stock == 0)
    			    <li class="current-price">&nbsp; &nbsp; &nbsp; &nbsp; </li>
    			    @else
					    @if($home_base_price != $home_discounted_base_price)
						<li class="old-price">{{ $home_base_price }}</li>
						@endif
						<li class="current-price">{{ $home_discounted_base_price }}</li>
					@endif
				</ul>
			</div>
		</div>
		<div class="add-to-cart-buttons">
		    @if($product->current_stock == 0)
		    <button class="btn btn95" type="button" disabled> Out of Stock</button>
		    @else
		    <button class="btn btn25" onclick="addToCart(this, {{$product->id}}, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
		    <button class="btn btn70" onclick="buyNow(this, {{ $product->id }}, 1, 'full')" type="button"> Buy Now</button>
		    @endif
		    @if(env('BARGAIN_BTN',false))
			@php $bargain_cat = explode(",",env('BARGAIN_CATEGORY','')); @endphp
			    @if(in_array($product->category_id, $bargain_cat))
			    <a href="{{env('BARGAIN_LINK','')}}" target="_blank" class="btn btn95" style="margin-top:5px;width:calc(90% + 4px);">
                {{__('For best lowest price Click')}}
				</a>
		        @endif
		    @endif
		</div>
	</article>
</div>
@endforeach

<!--
@foreach ($products as $key => $product)
	<div class="col-xxl-2 col-xl-3 col-lg-3 col-md-4 col-6" id="results">
		<div class="product-box-2 bg-white alt-box my-md-2">
			<div class="position-relative overflow-hidden">
				<a href="{{ route('product', $product->slug) }}" class="d-block product-image h-100 text-center" tabindex="0">
					@if($product->thumbnail_img!=NULL)
						<img class="img-fit lazyload mx-auto" src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}">
					@else
						<img class="img-fit lazyload mx-auto" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
					@endif
				</a>
				<div class="product-btns clearfix">
                    <button class="btn add-wishlist" title="Add to Wishlist" onclick="addToWishList({{ $product->id }})" type="button">
                        <i class="la la-heart-o"></i>
                    </button>
                    <button class="btn add-cart" title="Add to Wishlist" onclick="addToCart(this, {{ $product->id }},1)" type="button">
                        <i class="la la-cart-plus"></i>
                    </button>
                    
                   
                    <button class="btn quick-view" title="Quick view" onclick="showAddToCartModal({{ $product->id }})" type="button">
                        <i class="la la-eye"></i>
                    </button>
                </div>
			</div>
			<div class="p-md-3 p-2">
				<input type="hidden" id="totalpages" value="{{$products->lastPage()}}">
				<div class="price-box">
				 @php
			    $home_base_price = home_base_price($product->id);
			    $home_discounted_base_price = home_discounted_base_price($product->id);
			    @endphp
				@if($home_base_price != $home_discounted_base_price)
					<del class="old-product-price strong-400">{{ $home_base_price }} </del>
				  <span class="product-price strong-600">{{ $home_discounted_base_price }} <span style="color:red;font-size:14px;">({{discount_calulate($home_base_price, $home_discounted_base_price )}}% off)</span></span>
                @else
				<span class="product-price strong-600">{{ $home_discounted_base_price }} </span>
				@endif
				</div>
				<div class="star-rating star-rating-sm mt-1">
					{{ renderStarRating($product->rating) }}
				</div>
				<h2 class="product-title p-0">
					<a href="{{ route('product', $product->slug) }}" class=" text-truncate">{{ __($product->name) }}</a>
				</h2>
				@if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated)
					<div class="club-point mt-2 bg-soft-base-1 border-light-base-1 border">
						{{ __('Club Point') }}:
						<span class="strong-700 float-right">{{ $product->earn_point }}</span>
					</div>
				@endif
			</div>
		</div>
	</div>
@endforeach-->
