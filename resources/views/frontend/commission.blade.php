@extends('frontend.layouts.app')

@section('content')
<!-- Seller Style -->

<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" >
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                       @include('frontend.inc.seller_side_nav') 
                </div>
                <style>
                    .form-horizontal, .panel {     width: 100%; padding: 15px;background: #fff;}
                </style>
                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                       <div class="row">
	
</div>

<div class="row mt-4">
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all">
            <h3 class="panel-title pull-left pad-no" style="font-size: 22px;">Commission List</h3>
           
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Sub Category 2</th>
                    <th>Commission %</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($sellerCommission as $sellerCommissionList)
                          @php
                             if($sellerCommissionList->commission_type ==0 ){
                               $sellerTYpe = 'Inclusive';
                             }else{
                                 $sellerTYpe = 'Exclusive';
                             }
                             $catName =  \App\Category::where('id', $sellerCommissionList->category_id)->get();
                             
                             $subcatName =  \App\SubCategory::where('id', $sellerCommissionList->subcategory_id)->get();
                             
                             $subsubcategoryName =  \App\SubSubCategory::where('id', $sellerCommissionList->subsubcategory_id)->get();
                            
                          @endphp
                        <tr>
                            <td>{{$sellerCommissionList->id}}</td>
                            <td>{{$sellerTYpe}}</td>
                            <td>{{$catName[0]->name}}</td>
                            <td>{{$subcatName[0]->name}}</td>
                            <td>{{$subsubcategoryName[0]->name}}</td>
                            <td>{{$sellerCommissionList->commission}}</td>
                       
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
</div>

<script>
    $(function(){
    var $select = $(".percent");
    for (i=1;i<=100;i++){
        $select.append($('<option></option>').val(i).html(i))
    }
});

$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

	$('#subsubcategory_id').on('change', function() {
	    // get_brands_by_subsubcategory();
		//get_attributes_by_subsubcategory();
	});
	
	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
			$('#subsubcategory_id').append($('<option>', {
				value: null,
				text: null
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}


</script>

                   
                    </div>
                </div>
            </div>
        </div>
    </section>

    
@endsection

