@extends('frontend.layouts.app')

@section('content')
    <section class="gry-bg py-4 profile">
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-md-7">
                    <div class="main-content p-4 bg-white">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{__('Get in touch with us by filling the details below')}}
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <form class="" action="{{ url('contact_send') }}" method="POST" >
                            @csrf
							<input type="text" name="Name" value="" class="d-none" />
							<input type="text" name="Email" value="" class="d-none" />
							<div class="form-box bg-white mt-4">
								<div class="form-box-content p-3">
									<div class="row">
										<div class="col-12">
											<div class="form-group">
												<!-- <label>{{ __('Name') }}</label> -->
												<div class="input-group input-group--style-1">
													<input type="text" class="form-control" value="{{ old('name') }}" placeholder="{{__('Name*')}}" name="name" required />
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<!-- <label>{{ __('Email') }}</label> -->
												<div class="input-group input-group--style-1">
													<input type="email" class="form-control" value="{{ old('email') }}" placeholder="{{ __('Email') }}" name="email" />
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<!-- <label>{{ __('Phone') }}</label> -->
												<div class="input-group input-group--style-1">
													<input type="text" class="form-control" placeholder="{{__('Phone*')}}" value="{{ old('phone') }}" name="phone" required />
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col">
											<div class="form-group">
												<!-- <label>{{ __('Subject') }}</label> -->
												<div class="input-group input-group--style-1">
													<input type="text" class="form-control" placeholder="{{ __('Subject') }}" name="subject" value="{{ old('subject') }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12">
											<div class="form-group">
												<!-- <label>{{ __('Message') }}</label> -->
												<div class="input-group input-group--style-1">
													<textarea class="form-control" placeholder="{{ __('Message') }}" name="message">{{ old('message') }}</textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
                           
                            <div class="text-right mt-4">
                                <button type="submit" class="btn btn-styled btn-base-1">{{__('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
				<div class="col-md-5">
                    <div class="main-content p-4 bg-white">
					</div>
				</div>
            </div>
        </div>
    </section>

@endsection
