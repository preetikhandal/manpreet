@extends('frontend.layouts.app')

@section('content')
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
   background:#20b34e!important;
}
.process-steps li {
    width: 50%;
   
}
</style>
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @if(Auth::user()->user_type == 'seller')
                        @include('frontend.inc.seller_side_nav')
                    @elseif(Auth::user()->user_type == 'customer')
                        @include('frontend.inc.customer_side_nav')
                    @endif
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="row">
                            <div class="col-12 px-0">
                                <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-12">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 p-0 text-white">
                                        {{__('Return History')}}
                                    </h2>
                                </div>
                            </div>
                        </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            @if (count($ReturnProduct) > 0)
                                @foreach ($ReturnProduct as $key => $RP)
                                    @php
                                        $order=\App\Order::find($RP->order_id);
                                        $Product=\App\Product::find($RP->product_id);
                                        $OrderDetail=\App\OrderDetail::where('order_id',$RP->order_id)->where('product_id',$RP->product_id)->first();
                                    @endphp
                                    @foreach ($order->orderDetails as $key => $orderDetail)
                                        <article class="card card-product-list">
                                        	<div class="row no-gutters align-items-center">
                                        		<div class="col-md-2 col-3">
                                        			<a href="{{ route('product', $orderDetail->product->slug) }}" >
                                    			    	@if($orderDetail->product->thumbnail_img!="")
                                        			    	<img src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($orderDetail->product->thumbnail_img) }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                                        			    @else
                                        			        <img src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                                        			    @endif
                                        			</a>
                                        		</div> 
                                        		<div class="col-md-7 col-9 ">
                                        			<div class="info-main ">
                                        			    @if ($orderDetail->product != null)
                                        			    	<a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank" class="h5 title">{{ $orderDetail->product->name }}</a>
                                        		        @else
                                                            <strong>{{ __('Product Unavailable') }}</strong>
                                                        @endif
                                        		        <p class="info">Order 
                                        		            <a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="text-blue font-weight-bold"># {{ $order->code }}</a> 
                                        		            <span class="block-sm">Status
                                                                @if(str_replace('_', ' ', $RP->status)=="processing refund")
                                                                    <span class="badge badge-info"> {{ucfirst(str_replace('_', ' ',$RP->status))}}</span>
                                                                @else
                                                                    <span class="badge badge-success"> {{ucfirst(str_replace('_', ' ',$RP->status))}}</span>
                                                                @endif
                                        		            </span>
                                        		        </p>
                                        		        <p class="info">Request Date <b class="text-blue">{{$RP->created_at->format('d/m/Y')}} </b>
                                        		           <span class="block-sm">Payment Status 
                                        		             @if ($order->payment_status == 'paid')
                                        		                <span class="badge badge-success"> {{__('Paid')}}</span>
                                                             @else
                                                                <span class="badge badge-warning">{{__('Unpaid')}}</span>
                                                             @endif
                                        		           </span>
                                        		        </p>
                                        		        <span class="price h5 d-block d-md-none">Product Price {{ single_price($orderDetail->price) }} </span>
                                        			</div>
                                        		</div> 
                                        		<div class="col-12 d-block d-md-none mb-2 px-2 mobilebtn">
                                        		    <a href="#" onclick="show_return_history_details({{ $RP->order_id}},{{$OrderDetail->product_id}})" class="btn  btn-light btn50 float-left bg-blue"> {{__('Track Refund Status')}} </a>
                                        		</div>
                                        		<div class="col-md-3 d-none d-md-block">
                                        			<div class="info-aside">
                                        				<div class="price-wrap">
                                        					<span class="price h5"><span class="info">Product Price</span> {{ single_price($orderDetail->price) }} </span>	
                                        				</div> <br/>
                                        				<p class="d-none d-md-block">
                                        					<a href="#" onclick="show_return_history_details({{ $RP->order_id}},{{$OrderDetail->product_id}})" class="btn btn-light bg-blue btn-block">{{__('Track Refund Status')}}</a>
                                        				</p>
                                        			</div> 
                                        		</div> 
                                        	</div> 
                                        </article>
                                    @endforeach
                                @endforeach
                            @else
    							<div class="card no-border mt-4">
                                    <div>
    									<table class="table table-sm table-hover table-responsive-md">
                                            <thead>
                                                <tr class="text-center">
                                                    <th><h3>{{__('No Return History Found')}}</h3></th>
                                                </tr>
                                            </thead>
    									</table>
    								</div>
    							</div>
                            @endif
                        </div>
                        <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                {{ $ReturnProduct->links() }}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="return_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative" style="border-color:#282563;">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="return-details-modal-body">

                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5">{{__('Make Payment')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="payment_modal_body"></div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript">
        $('#order_details').on('hidden.bs.modal', function () {
            location.reload();
        })
    </script>

@endsection
