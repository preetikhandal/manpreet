@extends('frontend.layouts.app')

@section('content')
<style>

</style>
    <section class="bg-white pt-1 pb-3 py-md-4" id="userlogin_section" @if($mobileapp == 1) style="margin-top:90px" @endif>
		<div class="container">
			<div class="d-flex justify-content-center">
				<div class="user_card ">
				    <div class="row">
        		        <div class="col-12">
        		            @if($errors->any())
                    			<div class="alert alert-danger">
                    				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
                    				<ul>
                    					@foreach($errors->all() as $error)
                    						<li>{{ $error }}</li>
                    					@endforeach
                    				</ul>
                    			</div>
                    		@endif
        		        </div>
		            </div>
					<div class="text-center px-3 my-3">
						<h1 class="heading heading-4 strong-500">
							{{__('Login to your account.')}}
						</h1>
					</div>
					<div class="px-5"  id="socialmedialogin">
						<div class="">
							@if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
								<div class="mb-0">
								    <a href="{{ route('social.login', ['provider' => 'apple']) }}" class="btn btn-styled btn-block btn-apple btn-icon--2 btn-icon-left px-4 mb-3">
											<span class="icon fa fa-apple" ></span> {{__('Sign in with Apple')}}
									</a>
									
									@if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
										<a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="btn btn-styled btn-block btn-facebook btn-icon--2 btn-icon-left px-4 mb-3 " style="color:#fff!important">
											<i class="icon fa fa-facebook text-white"></i> {{__('Login with Facebook')}}
										</a>
									@endif
									@if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)
										<a href="{{ route('social.login', ['provider' => 'google']) }}" class="btn btn-styled btn-block btn-google btn-icon--2 btn-icon-left px-4">
											<i class="icon fa fa-google"></i> {{__('Login with Google')}}
										</a>
									@endif
									@if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
										<a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="btn btn-styled btn-block btn-twitter btn-icon--2 btn-icon-left px-4">
											<i class="icon fa fa-twitter"></i> {{__('Login with Twitter')}}
										</a>
									@endif
								</div>
								<div class="or or--1 mt-0 text-center">
									<span>or</span>
								</div>
							@endif
							
							<button class="btn btn-styled btn-block bg-blue btn-icon--2 btn-icon-left px-4 mb-3" id="signInbtn">
								{{__('Sign in with Phone or Email')}}
							</button>
						</div>
					</div>
					<div class="d-flex px-5 justify-content-center form_container">
						<form role="form" id="loginform" method="POST" style="display:none">
							@csrf
							<div class="input-group mb-3">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-user"></i></span>
								</div>
									<input type="text" class="form-control input_user {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{__('Email Or Phone')}}" name="email" id="email" onkeyup="checknumber(this.value)" required >
							</div>
							<div class="input-group mb-3" id="OTPDiv" style="display:none;">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
									<input type="text" class="form-control input_user" value="{{ old('otp') }}" placeholder="{{__('OTP')}}" name="otp" id="otp">
							</div>
							<div class="input-group mb-2" id="PasswordDiv">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
								<input type="password" class="form-control input_pass {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{__('Password')}}" name="password" id="password" >
							</div>
							
						    @if($mobileapp == 1)
						    <input type="hidden" name="remember" value="1">
						    @else
							<div class="form-group">
								<div class="custom-control custom-checkbox">
									 <input id="demo-form-checkbox" class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
									<label class="custom-control-label" for="demo-form-checkbox">{{ __('Remember Me') }}</label>
								</div>
							</div>
							@endif
							
                            <div class="row" id="mobileOtpDiv" style="display:none;">
							<div class="col-12 text-right pl-1">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md btn-block bg-blue">{{ __('Login') }}</button>
							</div>
							<div class="col-12 pt-2 text-center pl-1">
								<a href="javascript:loginwithOTP()" class="btn btn-styled btn-base-1 btn-md btn-block" style="background:#131921">{{ __('Login with OTP') }}</a>
							</div>
							</div>
                            
							<div class="col-12 justify-content-center mt-3 login_container" id="loginDiv">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md w-100 bg-blue">{{ __('Login') }}</button>
							</div>
							<div class="col-12 justify-content-center mt-3 login_container" id="SignupDiv" style="display:none;">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md w-100 bg-blue">{{ __('Sign Up') }}</button>
							</div>
                            
                            <div class="col-12" id="mobileOtpretryDiv" style="display:none;">
                                <center id="mobileotpretrycontentdiv"></center>
                            </div>
						</form>
					</div>
					<div class="my-2">
						<div class="d-flex justify-content-center links">
							Don't have an account? 
							<a href="{{ route('user.registration') }}" class="strong-600 ml-2 text-blue">{{__('Sign Up')}}</a>
						</div>
						<div class="d-flex justify-content-center links">
							<a href="{{ route('password.request') }}" class="link link-xs link--style-3 strong-600 text-blue text13">{{__('Forgot your password?')}}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<script>
    var otptime = 30;
    var timee;
    function loginwithOTP() {
        phone = $('#email').val();
        $.post('{{ route('generate.otp.login') }}', { _token:'{{ csrf_token() }}', phone:phone,name}, function(data){
            if(data.result == true) {
                $('#email').attr('readonly','readonly');
                $('#PasswordDiv').hide();
                $('#mobileOtpDiv').hide();
                $('#OTPDiv').show();
                $('#mobileOtpretryDiv').show();
               // $('#loginDiv').show();
                optcount();
                timee = setInterval(function(){ optcount(); }, 1000);
                msg = 'OTP has been generated successfully and send to your mobile number';
                if(data.newuser == true) {
                    msg += ' You will be registered as new user and agreed to our T & C.';
                    $('#SignupDiv').show();
                    $('#loginDiv').hide();
                } else {
                    $('#SignupDiv').hide();
                    $('#loginDiv').show();
                }
                showFrontendAlert('success', msg, 3000);
                
            } else {
                showFrontendAlert('warning', 'Enter a valid mobile number.', 3000);
            }
        });
    }
    
    $('#loginform').submit(function(e) {
        e.preventDefault();
        var data = $('#loginform').serialize();
        $.post('{{ route('user.login.validate') }}', data, function(data){
            if(data.result == true) {
                showFrontendAlert('success', 'Login has been successful.');
                $.cookie("name", data.name, { expires: 1 , path: '/' });
                window.location = data.redirectTo;
            } else {
                
                showFrontendAlert('warning', data.message, 3000);
                
            }
        });
    });
    
    function optcount() {
        if(otptime > 0) {
            $('#mobileotpretrycontentdiv').html('Resend OTP in '+otptime+' seconds.');
            otptime = otptime - 1;
        } else {
            $('#mobileotpretrycontentdiv').html('<a href="javascript:loginwithOTP()">Resend OTP</a>');
            clearInterval(timee);
            otptime = 30;
        }
    }
    
    function checknumber(v) {
        if(v.length == 0) {
            $('#mobileOtpDiv').hide();
            $('#loginDiv').show();
        }
        if(v.length > 0) {
            if(isNaN(v)) {
                $('#mobileOtpDiv').hide();
                $('#loginDiv').show();
            } else {
                $('#loginDiv').hide();
                $('#mobileOtpDiv').show();
            }
        }
    }
</script>

<script>
$(document).ready(function () {
	signInbtn=$("#signInbtn");
	signInbtn.click(function () {
	   $("#loginform").show();
	   $("#socialmedialogin").hide();
	});
});
</script>
@endsection
