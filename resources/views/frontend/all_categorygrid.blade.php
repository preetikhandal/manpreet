@extends('frontend.layouts.app')

@section('content')
<style>
    .allcats {
        background: #fff;
        padding: 0;
        margin: 0;
        min-height: 150px;
        box-shadow: 0 1px 5px #c1c1c1;
        border-radius: 3px;
    }
    .defaultProImg {
        padding: 0;
        padding-bottom: 0;
        padding-top: 0;
        position: relative;
    }
    .allcats h2 {
        background: #131921;
        color: #fff;
        font-size: 17px;
        padding: 10px;
        margin: 0;
        position: relative;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }
    .allcats .contanerSec {
        padding: 10px 0;
    }
    .allcats .contanerSec ul {
        padding: 0 2%;
        margin: 0;
        list-style: list-style-type;
        width: 100%;
    }

    .allcats .contanerSec ul li {
        padding: 0 2%;
        text-align: center;
        margin: 0;
        list-style: none;
        position: relative;
        display: inline-block;
        width: 32.2%;
        border-right: 1px solid #e5e5e5;
        vertical-align: top;
    }
    .allcats .contanerSec ul .noborder{
        border-right: none!important;
    }
    .allcats .contanerSec ul li a {
        display: block;
        color: #000;
    }
    @media only screen and (max-width: 1366px) and (min-width: 788px){
        .allcats .contanerSec ul li picture {
            min-height: 133px;
            display: block;
        }
    }
    .center-block {
        display: block;
        margin-right: auto;
        margin-left: auto;
    }
    .height10 {
        height: 10px;
    }
    .allcats .contanerSec ul li p {
        display: block;
        min-height: 40px;
        padding: 0;
        margin: 0;
        font-weight: 700;
        font-size: 13px;
        color: #000;
    }
    .allcats .contanerSec ul li.blockLINE {
        padding: 0;
        margin: 0;
        display: block;
        width: 100%;
        border-right: none;
        border-bottom: 1px solid #e5e5e5;
    }
</style>
<div class="py-2 gry-bg" @if($mobileapp == 1) style="margin-top:75px" @endif>
    <div id="js_ShopByCategory" class="col-lg-12 defaultProImg allcats" loaded="1">
       <h2>Shop By Category</h2>
       <div class="clr"></div>
       <div class="contanerSec">
          <ul>
              @php $i=0 @endphp
              @foreach($categories as $category)
              @php $i++ @endphp
              @if($category->display == 1)
              @if($i%3==0)
              <li class="noborder">
              @else
              <li>
              @endif
             
                @if($mobileapp == 0)
					<a href="{{route('products.category', $category->slug)}}" class="text-black">
				    @else
					<a href="{{ route('products.category', ['category_slug'=>$category->slug, 'android' => 1]) }}" class="text-black">
				    @endif
              
                   @if($category->banner != null)
						<picture><img src="{{ asset($category->banner) }}" class="lazyload img-fit center-block"  alt="{{ __($category->name) }}"></picture>
					@else
						<picture><img loading="lazy"  class="lazyload img-fit center-block" src="{{ asset('frontend/images/caticon.png') }}"></picture>
					@endif 
                    <div class="clr height10"></div>
                   <p>{{ __($category->name) }}</p>
                </a>
             </li>
             @endif
             
                @if($i%3==0)
                 <li class="blockLINE"></li>
                @endif
            @endforeach
             
            
          </ul>
       </div>
    </div>
</div>
@endsection