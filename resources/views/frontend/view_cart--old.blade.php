@extends('frontend.layouts.app')

@section('content')
	<style>
		.cart_table .table-cart > thead > tr > th{
			font-size:12px!important;
		}
		.cart_table .table-cart .product-quanity{width:150px;}
	</style>
    <section class="slice-xs sct-color-2 border-bottom">
        <div class="container-fluid container-sm">
            <div class="row cols-delimited justify-content-center">
                <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center active">
                        <div class="block-icon mb-0">
                            <i class="la la-shopping-cart"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">1. {{__('My Cart')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-map-o"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">2. {{__('Shipping info')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon mb-0 c-gray-light">
                            <i class="la la-truck"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">3. {{__('Delivery info')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-credit-card"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">4. {{__('Payment')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-check-circle"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">5. {{__('Confirmation')}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="py-4 gry-bg" id="cart-summary">
        <div class="container-fluid">
            @if(Session::has('cart'))
                <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-xl-8">
                    <!-- <form class="form-default bg-white p-4" data-toggle="validator" role="form"> -->
                    <div class="form-default bg-white p-3">
                        <div class="">
                            <div class="cart_table">
                                <table class="table-cart border-bottom">
                                    <thead>
                                        <tr>
                                            <th class="product-image"></th>
                                            <th class="product-name">{{__('Product')}}</th>
                                            <th class="product-price d-none d-lg-table-cell">{{__('Price')}}</th>
                                            <th class="product-quanity d-none d-md-table-cell">{{__('Quantity')}}</th>
                                            <th class="product-total">{{__('Total')}}</th>
                                            <th class="product-remove"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $total = 0;
                                        @endphp
                                        @foreach (Session::get('cart') as $key => $cartItem)
                                            @php
                                            $product = \App\Product::find($cartItem['id']);
                                            $total = $total + $cartItem['price']*$cartItem['quantity'];
                                            $product_name_with_choice = $product->name;
                                            if ($cartItem['variant'] != null) {
                                                $product_name_with_choice = $product->name.' - '.$cartItem['variant'];
                                            }
                                            // if(isset($cartItem['color'])){
                                            //     $product_name_with_choice .= ' - '.\App\Color::where('code', $cartItem['color'])->first()->name;
                                            // }
                                            // foreach (json_decode($product->choice_options) as $choice){
                                            //     $str = $choice->name; // example $str =  choice_0
                                            //     $product_name_with_choice .= ' - '.$cartItem[$str];
                                            // }
                                            @endphp
                                            <tr class="cart-item">
                                                <td class="product-image">
                                                    <a href="#" class="mr-3">
                                                        <img loading="lazy"  src="{{ asset($product->thumbnail_img) }}">
                                                    </a>
                                                </td>

                                                <td class="product-name">
                                                    <span class="pr-4 d-block">{{ $product_name_with_choice }}</span>
                                                </td>

                                                <td class="product-price d-none d-lg-table-cell">
                                                    <span class="pr-3 d-block">{{ single_price($cartItem['price']) }}</span>
                                                </td>

                                                <td class="product-quantity d-none d-md-table-cell">
                                                    @if($cartItem['digital'] != 1)
                                                        <div class="input-group input-group--style-2 pr-4" style="width: 100%;">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-number" type="button" data-type="minus" data-field="quantity[{{ $key }}]">
                                                                    <i class="la la-minus"></i>
                                                                </button>
                                                            </span>
                                                                <input type="text" name="quantity[{{ $key }}]" class="form-control input-number" placeholder="1" value="{{ $cartItem['quantity'] }}" min="1" max="10" onchange="updateQuantity({{ $key }}, this)">
                                                                <span class="input-group-btn">
                                                                <button class="btn btn-number" type="button" data-type="plus" data-field="quantity[{{ $key }}]">
                                                                    <i class="la la-plus"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td class="product-total">
                                                    <span>{{ single_price(($cartItem['price']+$cartItem['tax'])*$cartItem['quantity']) }}</span>
                                                </td>
                                                <td class="product-remove">
                                                    <a href="#" onclick="removeFromCartView(event, {{ $key }})" class="text-right pl-4">
                                                        <i class="la la-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row align-items-center pt-4">
                            <div class="col-md-6">
                                <a href="{{ route('home') }}" class="link link--style-3">
                                    <i class="la la-mail-reply"></i>
                                    {{__('Return to shop')}}
                                </a>
                            </div>
                            <div class="col-md-6 text-right">
                                @if(Auth::check())
                                    <a href="{{ route('checkout.shipping_info') }}" class="btn btn-styled btn-base-1">{{__('Continue to Shipping')}}</a>
                                @else
                                    <button class="btn btn-styled btn-base-1" onclick="showCheckoutModal()">{{__('Continue to Shipping')}}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- </form> -->
                </div>

                <div class="col-xl-4 ml-lg-auto">
                    @include('frontend.partials.cart_summary')
                </div>
				</div>
            @else
                <div class="dc-header">
                    <h3 class="heading heading-6 strong-700">{{__('Your Cart is empty')}}</h3>
                </div>
            @endif
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="GuestCheckout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-zoom" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">{{__('Login')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="p-3">
                        <form role="form" id="loginform" method="POST">
							@csrf
							<div class="input-group mb-3">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-user"></i></span>
								</div>
									<input type="text" class="form-control input_user {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{__('Email Or Phone')}}" name="email" id="email" onkeyup="checknumber(this.value)">
							</div>
							<div class="input-group mb-3" id="OTPDiv" style="display:none;">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
									<input type="text" class="form-control input_user" value="{{ old('otp') }}" placeholder="{{__('OTP')}}" name="otp" id="otp">
							</div>
							<div class="input-group mb-2" id="PasswordDiv">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
								<input type="password" class="form-control input_pass {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{__('Password')}}" name="password" id="password">
							</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
									 <input id="demo-form-checkbox" class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
									<label class="custom-control-label" for="demo-form-checkbox">{{ __('Remember Me') }}</label>
								</div>
							</div>
							
                            <div class="row" id="mobileOtpDiv" style="display:none;">
							<div class="col-6 pr-1">
								<button type="button" class="btn btn-styled btn-base-1 btn-md btn-block" onclick="loginwithOTP()">{{ __('Login with OTP') }}</button>
							</div>
							<div class="col-6 text-right pl-1">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md btn-block">{{ __('Login') }}</button>
							</div>
							</div>
                            
							<div class="col-12 justify-content-center mt-3 login_container pl-0 pr-0" id="loginDiv">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md w-100">{{ __('Login') }}</button>
							</div>
                            
							<div class="col-12 justify-content-center mt-3 login_container" id="SignupDiv" style="display:none;">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md w-100">{{ __('Sign Up') }}</button>
							</div>
                            
                            <div class="col-12" id="mobileOtpretryDiv" style="display:none;">
                                <center id="mobileotpretrycontentdiv"></center>
                            </div>
                            <div class="col-12">
                                
                                    <a href="{{ route('password.request') }}" class="link link-xs link--style-3">{{__('Forgot password?')}}</a>
                            </div>
						</form>
                    </div>
                     @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                        <div class="or or--1 mt-3 text-center">
                            <span>or</span>
                        </div>
                        <div class="p-3 pb-0">
                            @if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
                                <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="btn btn-styled btn-block btn-facebook btn-icon--2 btn-icon-left px-4 mb-3">
                                    <i class="icon fa fa-facebook"></i> {{__('Login with Facebook')}}
                                </a>
                            @endif
                            @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)
                                <a href="{{ route('social.login', ['provider' => 'google']) }}" class="btn btn-styled btn-block btn-google btn-icon--2 btn-icon-left px-4 mb-3">
                                    <i class="icon fa fa-google"></i> {{__('Login with Google')}}
                                </a>
                            @endif
                            @if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                            <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="btn btn-styled btn-block btn-twitter btn-icon--2 btn-icon-left px-4 mb-3">
                                <i class="icon fa fa-twitter"></i> {{__('Login with Twitter')}}
                            </a>
                            @endif
                        </div>
                    @endif
                    @if (\App\BusinessSetting::where('type', 'guest_checkout_active')->first()->value == 1)
                        <div class="or or--1 mt-0 text-center">
                            <span>or</span>
                        </div>
                        <div class="text-center">
                            <a href="{{ route('checkout.shipping_info') }}" class="btn btn-styled btn-base-1">{{__('Guest Checkout')}}</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
    function removeFromCartView(e, key){
        e.preventDefault();
        removeFromCart(key);
    }

    function updateQuantity(key, element){
        $.post('{{ route('cart.updateQuantity') }}', { _token:'{{ csrf_token() }}', key:key, quantity: element.value}, function(data){
            updateNavCart();
            $('#cart-summary').html(data);
        });
    }

    function showCheckoutModal(){
        $('#GuestCheckout').modal();
    }
        var otptime = 10;
    var timee;
    function loginwithOTP() {
        phone = $('#email').val();
        $.post('{{ route('generate.otp.login') }}', { _token:'{{ csrf_token() }}', phone:phone}, function(data){
            if(data.result == true) {
                $('#email').attr('readonly','readonly');
                $('#PasswordDiv').hide();
                $('#mobileOtpDiv').hide();
                $('#OTPDiv').show();
                $('#mobileOtpretryDiv').show();
                //$('#loginDiv').show();
                optcount();
                timee = setInterval(function(){ optcount(); }, 1000);
                msg = 'OTP has been generated successfully and send to your mobile number';
                if(data.newuser == true) {
                    msg += ' You will be registered as new user and agreed to our T & C.';
                    $('#SignupDiv').show();
                    $('#loginDiv').hide();
                } else {
                    $('#SignupDiv').hide();
                    $('#loginDiv').show();
                }
                showFrontendAlert('success', msg);
            } else {
                showFrontendAlert('warning', 'Enter a valid mobile number.');
            }
        });
    }
    
    $('#loginform').submit(function(e) {
        e.preventDefault();
        var data = $('#loginform').serialize();
        $.post('{{ route('user.login.validate') }}', data, function(data){
            if(data.result == true) {
                showFrontendAlert('success', 'Login has been successful.');
                $.cookie("name", data.name, { expires: 1 , path: '/' });
                window.location = "{{ route('checkout.shipping_info') }}";
            } else {
                showFrontendAlert('warning', data.message);
            }
        });
    });
    
    function optcount() {
        if(otptime > 0) {
            $('#mobileotpretrycontentdiv').html('Resend OTP in '+otptime+' seconds.');
            otptime = otptime - 1;
        } else {
            $('#mobileotpretrycontentdiv').html('<a href="javascript:loginwithOTP()">Resend OTP</a>');
            clearInterval(timee);
            otptime = 10;
        }
        
    }
    
    function checknumber(v) {
        if(v.length == 0) {
            $('#mobileOtpDiv').hide();
            $('#loginDiv').show();
        }
        if(v.length > 0) {
            if(isNaN(v)) {
                $('#mobileOtpDiv').hide();
                $('#loginDiv').show();
            } else {
                $('#loginDiv').hide();
                $('#mobileOtpDiv').show();
            }
        }
    }
    </script>
@endsection
