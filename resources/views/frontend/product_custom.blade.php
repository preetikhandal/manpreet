@extends('frontend.layouts.app')
@if(isset($subsubcategory_id))
    @php
        $meta_title = \App\SubSubCategory::find($subsubcategory_id)->meta_title;
        $meta_description = \App\SubSubCategory::find($subsubcategory_id)->meta_description;
        $meta_keywords = \App\SubSubCategory::find($subsubcategory_id)->tags;
    @endphp
@elseif (isset($subcategory_id))
    @php
        $meta_title = \App\SubCategory::find($subcategory_id)->meta_title;
        $meta_description = \App\SubCategory::find($subcategory_id)->meta_description;
        $meta_keywords = \App\SubCategory::find($subcategory_id)->meta_keywords;
    @endphp
@elseif (isset($category_id))
    @php
        $meta_title = \App\Category::find($category_id)->meta_title;
        $meta_description = \App\Category::find($category_id)->meta_description;
        $meta_keywords = \App\Category::find($category_id)->meta_keywords;
    @endphp
@elseif (isset($brand_id))
    @php
        $meta_title = \App\Brand::find($brand_id)->meta_title;
        $meta_description = \App\Brand::find($brand_id)->meta_description;
        $meta_keywords = \App\Brand::find($brand_id)->tags;
    @endphp
@else
    @php
        $meta_title = \App\SeoSetting::first()->meta_title;
        $meta_description = \App\SeoSetting::first()->description;
        $meta_keywords = \App\SeoSetting::first()->keyword;
    @endphp
@endif


@section('meta_title'){{ $meta_title }}@stop
@section('meta_description'){{ $meta_description }}@stop
@section('meta_keywords'){{ $meta_keywords }}@stop

@section('meta')
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $meta_title }}">
    <meta itemprop="description" content="{{ $meta_description }}">

    <!-- Twitter Card data -->
    <meta name="twitter:title" content="{{ $meta_title }}">
    <meta name="twitter:description" content="{{ $meta_description }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $meta_title }}" />
    <meta property="og:description" content="{{ $meta_description }}" />
@endsection

@section('content')
<style> .aldeproductslider .list-product{ margin: 0px 0px 5px 0px; }
.active_sub a:before {
    content: "\f105";
    font: normal normal normal 16px/1 FontAwesome;
    margin-right:5;
    text-rendering: optimizeLegibility;
    text-transform: none;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    font-smoothing: antialiased;
    opacity: 0.7;
    font-size: 90%;
    margin-right: 5px;
}

</style>
    <div class="breadcrumb-area" @if($mobileapp == 1) style="margin-top:80px" @endif>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    @if($mobileapp == 0)
                    <ul class="breadcrumb">
                        <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                        <li><a href="{{route('categories.all')}}">{{__('All Categories')}}</a></li>
                        @if(isset($subsubcategory_id))
                            @php
                            $SubSubCategory = \App\SubSubCategory::find($subsubcategory_id);
                            $SubCategory = \App\SubCategory::find($SubSubCategory->sub_category_id);
                            $Category = \App\Category::find($SubCategory->category_id);
                            @endphp
                            <li ><a href="{{ route('products.category', $Category->slug) }}">{{ $Category->name }}</a></li>
                            <li ><a href="{{ route('products.subcategory', [$Category->slug,$SubCategory->slug]) }}">{{ $SubCategory->name }}</a></li>
                            <li class="active"><a href="{{ route('products.subsubcategory', [$Category->slug, $SubCategory->slug,$SubSubCategory->slug]) }}">{{ $SubSubCategory->name }}</a></li>
                        @elseif(isset($subcategory_id))
                            <li ><a href="{{ route('products.category', \App\SubCategory::find($subcategory_id)->category->slug) }}">{{ \App\SubCategory::find($subcategory_id)->category->name }}</a></li>
                            <li class="active"><a href="{{ route('products.subcategory', [\App\SubCategory::find($subcategory_id)->category->slug, \App\SubCategory::find($subcategory_id)->slug]) }}">{{ \App\SubCategory::find($subcategory_id)->name }}</a></li>
                        @elseif(isset($category_id))
                            <li class="active"><a href="{{ route('products.category', \App\Category::find($category_id)->slug) }}">{{ \App\Category::find($category_id)->name }}</a></li>
                        @endif
                    </ul>
                    @else
                    <ul class="breadcrumb">
                        <li><a href="{{ route('home', ['android' => 1]) }}">{{__('Home')}}</a></li>
                        <li><a href="{{ route('categories.all', ['android' => 1]) }}">{{__('All Categories')}}</a></li>
                        @if(isset($subsubcategory_id))
                            @php
                            $SubSubCategory = \App\SubSubCategory::find($subsubcategory_id);
                            $SubCategory = \App\SubCategory::find($SubSubCategory->sub_category_id);
                            $Category = \App\Category::find($SubCategory->category_id);
                            @endphp
                            <li ><a href="{{ route('products.category', ['category_slug'=>$Category->slug, 'android' => 1]) }}">{{ $Category->name }}</a></li>
                            <li ><a href="{{ route('products.subcategory', ['category_slug'=>$Category->slug, 'subcategory_slug' => $SubCategory->slug, 'android' => 1]) }}">{{ $SubCategory->name }}</a></li>
                            <li class="active"><a href="{{ route('products.subsubcategory', ['category_slug'=>$Category->slug, 'subcategory_slug' => $SubCategory->slug, 'subsubcategory_slug' => $SubSubCategory->slug, 'android' => 1]) }}">{{ $SubSubCategory->name }}</a></li>
                        @elseif(isset($subcategory_id))
                            <li ><a href="{{ route('products.category', ['category_slug' => \App\SubCategory::find($subcategory_id)->category->slug, 'android' => 1]) }}">{{ \App\SubCategory::find($subcategory_id)->category->name }}</a></li>
                            <li class="active"><a href="{{ route('products.subcategory', ['category_slug' => \App\SubCategory::find($subcategory_id)->category->slug, 'subcategory_slug' => \App\SubCategory::find($subcategory_id)->slug, 'android' => 1]) }}">{{ \App\SubCategory::find($subcategory_id)->name }}</a></li>
                        @elseif(isset($category_id))
                            <li class="active"><a href="{{ route('products.category', ['category_slug'=>\App\Category::find($category_id)->slug, 'android' => 1]) }}">{{ \App\Category::find($category_id)->name }}</a></li>
                        @endif
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <section class="gry-bg py-md-2">
        <div class="container-fluid sm-px-0">
            <form class="" id="search-form" action="{{ url()->current() }}" method="GET">
                @if($brand_slug!=null)
                    <input type="hidden" name="brand" id="brand_slug" value="{{$brand_slug}}" />
                @endif
                
            @if($mobileapp == 1)
            <input type="hidden" name="android" value="1" />
            @endif
                <input type="hidden" name="sort_by" id="sort_by" value="{{$sort_by}}" />
                <div class="row productcustomrow">
                <div class="col-xl-3 side-filter d-xl-block">
                    <div class="filter-overlay filter-close"></div>
                    <div class="filter-wrapper c-scrollbar">
                        <div class="filter-title d-flex d-xl-none justify-content-between pb-3 align-items-center">
                            <h3 class="h6">Filters</h3>
                            <button type="button" class="close filter-close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="bg-white sidebar-box mb-3">
                            <div class="box-title text-center">
                                {{__('Categories')}}
                            </div>
                            <div class="box-content">
                                <div class="category-filter">
                                    <ul>
                                        @if(!isset($category_id) && !isset($subcategory_id) && !isset($subsubcategory_id))
                                            @foreach(\App\Category::orderBy('position','asc')->get() as $category)
                                                <li class="font-weight-bold active"><a href="{{ route('products.category', $category->slug) }}">{{ __($category->name) }}</a></li>
                                            @endforeach
                                        @else
                                            <li class="active d-none d-md-block"><a href="{{route('categories.all')}}">{{__('All Categories')}}</a></li>
                                            @if($mobileapp == 0)
                                                <li class="active d-block d-md-none"><a href="{{ route('categories.all.mobilegrid') }}">{{__('All Categories')}}</a></li>
                                            @else
                                                <li class="active d-block d-md-none"><a href="{{route('categories.all.mobilegrid', ['android' => 1])}}">{{__('All Categories')}}</a></li>
                                            @endif
                                            @foreach(\App\Category::orderBy('position','asc')->get() as $category)
                                            @if($category->display == 1)
                                            <li class="font-weight-bold active"><a href="{{ route('products.category', $category->slug) }}">{{ __($category->name) }}</a></li>
                                                @if(isset($category_id) && $category_id==$category->id)
                                                    @php
                                                        $Category = \App\Category::orderBy('position','asc')->find($category_id);
                                                    @endphp
                                                    <ul class="ml-2">
                                                    @foreach ($Category->subcategories as $key2 => $subcategory)
                                                        @if($subcategory->display == 1)
                                                         <li class="child active_sub"><a href="{{ route('products.subcategory', [$Category->slug, $subcategory->slug]) }}">{{ __($subcategory->name) }}</a></li>
                                                            @if(isset($subcategory_id) && $subcategory_id==$subcategory->id)
                                                                @php 
                                                                    $category_slug = \App\SubCategory::find($subcategory_id)->category->slug;
                                                                    $subcategory_slug = \App\SubCategory::find($subcategory_id)->slug;
                                                                @endphp
                                                                <ul class="ml-5">
                                                                @foreach (\App\SubCategory::find($subcategory_id)->subsubcategories as $key3 => $subsubcategory)
                                                                    @if($subsubcategory->display == 1)
                                                                    <li><a href="{{ route('products.subsubcategory', [$category_slug, $subcategory_slug, $subsubcategory->slug]) }}">{{ __($subsubcategory->name) }}</a></li>
                                                                    @endif
                                                                @endforeach
                                                                </ul>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                    </ul>
                                                @endif
                                            @endif
                                            @endforeach
                                        @endif
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white sidebar-box mb-3">
                            <div class="box-title text-center">
                                {{__('Price range')}}
                            </div>
                            <div class="box-content">
                                <div class="range-slider-wrapper mt-3">
                                    <!-- Range slider container -->
                                    <div id="input-slider-range" data-range-value-min="{{$min}}" data-range-value-max="{{$max}}"></div>
                                    <!-- Range slider values -->
                                    <div class="row">
                                       <div class="col-6">
                                            <span class="range-slider-value value-low"
                                                @if (isset($min_price))
                                                    data-range-value-low="{{ $min_price }}"
                                                @elseif($min > 0)
                                                    data-range-value-low="{{ $min }}"
                                                @else
                                                    data-range-value-low="0"
                                                @endif
                                                id="input-slider-range-value-low"></span>
                                        </div>

                                        <div class="col-6 text-right">
                                            <span class="range-slider-value value-high"
                                                @if (isset($max_price))
                                                    data-range-value-high="{{ $max_price }}"
                                                @elseif($max > 0)
                                                    data-range-value-high="{{ $max }}"
                                                @else
                                                    data-range-value-high="0"
                                                @endif
                                                id="input-slider-range-value-high"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white sidebar-box mb-3 ">
                            <div class="box-title text-center">
                                {{__('Sort by')}}
                            </div>
                            <div class="box-content">
                                <div class="category-filter">
                                    <div class="sort-by-box px-1">
										<div class="form-group">
											<select class="form-control sortSelect" data-minimum-results-for-search="Infinity" onchange="filter(this,this.value)">
												<option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{__('Newest')}}</option>
												<option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{__('Oldest')}}</option>
												<option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{__('Price low to high')}}</option>
												<option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{__('Price high to low')}}</option>
											</select>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                        
						@if(!empty($all_colors))
                        <div class="bg-white sidebar-box mb-3">
                            <div class="box-title text-center">
                                {{__('Filter by color')}}
                            </div>
                            <div class="box-content">
                                <!-- Filter by color -->
                                <ul class="list-inline checkbox-color checkbox-color-circle mb-0">
                                    @foreach ($all_colors as $key => $color)
                                        <li>
                                            <input type="radio" id="color-{{ $key }}" name="color" value="{{ $color }}" @if(isset($selected_color) && $selected_color == $color) checked @endif onchange="filter()">
                                            <label style="background: {{ \App\Color::where('name', $color)->first()->code }};" for="color-{{ $key }}" data-toggle="tooltip" data-original-title="{{ $color }}"></label>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endif
                        @foreach ($attributes as $key => $attribute)
                            @if (\App\Attribute::find($attribute['id']) != null)
                                <div class="bg-white sidebar-box mb-3">
                                    <div class="box-title text-center">
                                        Filter by {{ \App\Attribute::find($attribute['id'])->name }}
                                    </div>
                                    <div class="box-content">
                                        <!-- Filter by others -->
                                        <div class="filter-checkbox">
                                            @if(array_key_exists('values', $attribute))
                                                @foreach ($attribute['values'] as $key => $value)
                                                    @php
                                                        $flag = false;
                                                        if(isset($selected_attributes)){
                                                            foreach ($selected_attributes as $key => $selected_attribute) {
                                                                if($selected_attribute['id'] == $attribute['id']){
                                                                    if(in_array($value, $selected_attribute['values'])){
                                                                        $flag = true;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    @endphp
                                                    <div class="checkbox">
                                                        <input type="checkbox" id="attribute_{{ $attribute['id'] }}_value_{{ $value }}" name="attribute_{{ $attribute['id'] }}[]" value="{{ $value }}" @if ($flag) checked @endif onchange="filter()">
                                                        <label for="attribute_{{ $attribute['id'] }}_value_{{ $value }}">{{ $value }}</label>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        
                        <div class="bg-white sidebar-box mb-3 ">
                            <div class="box-title text-center">
                                {{__('Out of Stock')}}
                            </div>
                            <div class="box-content">
                                <div class="category-filter">
                                    <div class="sort-by-box px-1">
										<div class="form-group">
											<select class="form-control sortSelect" name="out_of_stock"  onchange="filter()">
												<option value="0" @if(isset($out_of_stock)) @if ($out_of_stock == '0') selected @endif @else selected @endif>{{__('Not Include')}}</option>
												<option value="1" @isset($out_of_stock) @if ($out_of_stock == '1') selected @endif @endisset>{{__('Include')}}</option>
											</select>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url()->current()}}" class="btn btn-styled btn-block btn-base-4 bg-blue">Reset filter</a> 
                        {{-- <button type="submit" class="btn btn-styled btn-block btn-base-4">Apply filter</button> --}}
                    </div>
                </div>
                <div class="col-xl-9">
                    <!-- <div class="bg-white"> -->
                        @isset($category_id)
                            <input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
                        @endisset
                        @isset($subcategory_id)
                            <input type="hidden" name="subcategory" value="{{ \App\SubCategory::find($subcategory_id)->slug }}">
                        @endisset
                        @isset($subsubcategory_id)
                            <input type="hidden" name="subsubcategory" value="{{ \App\SubSubCategory::find($subsubcategory_id)->slug }}">
                        @endisset
						<div class="d-block d-md-none">
							<div class="row bg-white mb-2 py-2">
								<div class="col-6" style="display: flex;align-items: center;justify-content: center;border-right:1px solid #f1f1f1">
									<button type="button" class="btn p-1 btn-sm" data-toggle="modal" href="#sortbymodal" style="background: none;">
                                        <i class="la la-sort" style="font-size:1.5em"></i> <span style="font-size:1.5em">Sort</span> 
                                    </button>
								</div>
								<div class="col-6" style="display: flex;align-items: center;justify-content: center;">
									<button type="button" class="btn p-1 btn-sm" id="side-filter" style="background: none;">
                                        <i class="la la-filter" style="font-size:1.5em"></i> <span style="font-size:1.5em">Categories</span>
                                    </button>
								</div>
							</div>
						</div>
                       <input type="hidden" name="min_price" value="">
                        <input type="hidden" name="max_price" value="">
                        <!-- <hr class=""> -->
                        <div class="products-box-bar p-3 bg-white aldeproductslider">
                            <div class="row sm-no-gutters gutters-5 results1">
                                @foreach ($catPro as $key => $product)
                                    @php
                        			    $home_base_price = home_base_price($product->id);
                        			    $home_discounted_base_price = home_discounted_base_price($product->id);
                        			    if($home_base_price == "₹ 0.00") {
                                            $product->current_stock = 0;
                                            $qty = 0;
                                        }
                    			    @endphp
                                    <div class="col-md-3 col-6">
                                        <article class="list-product">
                							<div class="img-block">
                							    @if($mobileapp == 0)
                								<a href="{{ route('product', $product->slug) }}" class="thumbnail">
                								@else
                								<a href="{{ route('product', ['slug' => $product->slug,  'android' => 1]) }}" class="thumbnail">
                								@endif
                								    @if($product->thumbnail_img != null)
                									<img class="mx-auto d-block lazyload img-fluid" src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" />
                									@else
                								    <img class="mx-auto d-block lazyload  img-fluid" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
                									@endif
                								</a>
                							</div>
                							@if($product->current_stock != 0)
                    							@if($home_base_price != $home_discounted_base_price)
                    							<ul class="product-flag">
                    								<li class="new discount-price bg-orange">{{discount_calulate($home_base_price, $home_discounted_base_price )}}% off</li>
                    							</ul>
                    							@endif
                    							
                							@endif
                							<div class="product-decs text-center">
                							    @if($mobileapp == 0)
                								<h2><a href="{{ route('product', $product->slug) }}" class="product-link">{{ __(ucwords(strtolower($product->name))) }}</a></h2>
                								@else
                								<h2><a href="{{ route('product', ['slug' => $product->slug,  'android' => 1]) }}" class="product-link">{{ __(ucwords(strtolower($product->name))) }}</a></h2>
                								@endif
                								<div class="pricing-meta">
                									<ul>
                    								    @if($product->current_stock == 0)
                                        			    <li class="current-price">&nbsp; &nbsp; &nbsp; &nbsp; </li>
                                        			    @else
                    									    @if($home_base_price != $home_discounted_base_price)
                    										<li class="old-price">{{ $home_base_price }}</li>
                    										@endif
                    										<li class="current-price">{{ $home_discounted_base_price }}</li>
                    									@endif
                									</ul>
                								</div>
                							</div>
                							<div class="add-to-cart-buttons">
                							    @if($product->current_stock == 0)
                							    <button class="btn btn95" type="button" disabled> Out of Stock</button>
                							    @else
                							    <button class="btn btn25" onclick="addToCart(this, {{$product->id}}, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
                							    <button class="btn btn70" onclick="buyNow(this, {{ $product->id }}, 1, 'full')" type="button"> Buy Now</button>
                							    @endif
                							    @if(env('BARGAIN_BTN',false))
                								@php $bargain_cat = explode(",",env('BARGAIN_CATEGORY','')); @endphp
                								    @if(in_array($product->category_id, $bargain_cat))
                    							    <a href="{{env('BARGAIN_LINK','')}}" target="_blank" class="btn btn95" style="margin-top:5px;width:calc(90% + 4px);">
                                                    {{__('For best lowest price Click')}}
                    								</a>
                							        @endif
                							    @endif
                							</div>
                						</article>
            						</div>
        						 @endforeach
        					</div>
        					<div class="row sm-no-gutters gutters-5">
								<div class="col-12 text-center pb-5 mb-5 mb-md-0 pb-md-0">
									<div class="ajax-loading text-center"><img src="{{ asset('frontend/images/loading.gif') }}" /></div>
								</div>
							</div>
    					</div>
                       

                    <!-- </div> -->
                </div>
            </div>
			<!--Sort Modal--->
			<div class="modal fade" id="sortbymodal" data-keyboard="false" data-backdrop="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Sort By</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<div class="sort-by-box px-1">
								<div class="form-group">
									<label>{{__('Sort by')}}</label>
									<select class="form-control sortSelect" data-minimum-results-for-search="Infinity" onchange="filter(this,this.value)">
										<option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{__('Newest')}}</option>
										<option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{__('Oldest')}}</option>
										<option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{__('Price low to high')}}</option>
										<option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{__('Price high to low')}}</option>
									</select>
								</div>
							</div>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dalog -->
			</div><!-- /.modal -->
            </form>
        </div>
    </section>
	
	
@endsection

@section('script')
    <script type="text/javascript">
       function filter(ele = null, val = null){
			if(val != null)  {
				$('#sort_by').val(val);
			}
            $('#search-form').submit();
        }
        function rangefilter(arg){
            $('input[name=min_price]').val(arg[0]);
            $('input[name=max_price]').val(arg[1]);
            filter();
        }
    </script>
    
	<style>
		.product-box-2.alt-box .product-btns .btn {
        width: 33.33%;
        }
	</style>
@endsection
