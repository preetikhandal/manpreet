@extends('frontend.layouts.app')

@section('content')
 <style>
    .countdown .countdown-digit{
        background:#282563!important;
        color:#fff!important;
        font-weight:600;
    }
    .aldeproductslider .list-product{ margin: 0px 0px 5px 0px; }
</style>
<div @if($mobileapp == 1) style="margin-top:75px" @endif>
    
        <div>
           
            <section class="pb-4">
                <div class="container ">
                   
                    <div class="row sm-no-gutters gutters-5 aldeproductslider deepscustomrow">
                                                                                                                <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/genteal-eye-drop-tosqi" class="thumbnail">
            								                									<img class="mx-auto d-block lazyloaded" src="https://staging.aldebazaar.com/uploads/products/flash_deal/genteal-eye-dropthumb1598927434.png" data-src="https://staging.aldebazaar.com/uploads/products/flash_deal/genteal-eye-dropthumb1598927434.png" alt="Genteal Eye Drop">
            									            								</a>
            							</div>
            							            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;">6.5% off</li>
            							</ul>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/genteal-eye-drop-tosqi" class="product-link">Genteal Eye Drop</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                									    	<li class="old-price">₹ 182.00</li>
            										            										    <li class="current-price">₹ 170.00</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 1, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 1, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					                                                                                         <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/genteal-gel-qkqv5" class="thumbnail">
            								                									<img class="mx-auto d-block lazyloaded" src="https://staging.aldebazaar.com/uploads/products/flash_deal/genteal-gelthumb1598927439.jpg" data-src="https://staging.aldebazaar.com/uploads/products/flash_deal/genteal-gelthumb1598927439.jpg" alt="Genteal Gel">
            									            								</a>
            							</div>
            							            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;">3.5% off</li>
            							</ul>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/genteal-gel-qkqv5" class="product-link">Genteal Gel</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                									    	<li class="old-price">₹ 350.00</li>
            										            										    <li class="current-price">₹ 338.00</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 2, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 2, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					                                                                                         <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/systane-ultra-ophthalmic-solution-rbxo4" class="thumbnail">
            								                									<img class="mx-auto d-block lazyloaded" src="https://staging.aldebazaar.com/uploads/products/flash_deal/systane-ultra-ophthalmic-solutionthumb1598927451.png" data-src="https://staging.aldebazaar.com/uploads/products/flash_deal/systane-ultra-ophthalmic-solutionthumb1598927451.png" alt="Systane Ultra Ophthalmic Solution">
            									            								</a>
            							</div>
            							            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;">3% off</li>
            							</ul>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/systane-ultra-ophthalmic-solution-rbxo4" class="product-link">Systane Ultra Ophthalmic Solution</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                									    	<li class="old-price">₹ 412.00</li>
            										            										    <li class="current-price">₹ 400.00</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 3, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 3, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					                                                                                         <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/tears-naturale-ii-eye-drops-imh9a" class="thumbnail">
            								                									<img class="mx-auto d-block lazyloaded" src="https://staging.aldebazaar.com/uploads/products/flash_deal/tears-naturale-ii-eye-dropsthumb1598927458.jpg" data-src="https://staging.aldebazaar.com/uploads/products/flash_deal/tears-naturale-ii-eye-dropsthumb1598927458.jpg" alt="Tears Naturale Ii Eye Drops">
            									            								</a>
            							</div>
            							            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;">6% off</li>
            							</ul>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/tears-naturale-ii-eye-drops-imh9a" class="product-link">Tears Naturale Ii Eye Drops</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                									    	<li class="old-price">₹ 196.00</li>
            										            										    <li class="current-price">₹ 184.00</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 4, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 4, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					                                                                                         <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/tears-naturale-ii-eye-drops-5inun" class="thumbnail">
            								                								    <img class="mx-auto d-block ls-is-cached lazyloaded" src="https://staging.aldebazaar.com/frontend/images/product-thumb.png" alt="Tears Naturale Ii Eye Drops">
            									            								</a>
            							</div>
            							            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;">13.5% off</li>
            							</ul>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/tears-naturale-ii-eye-drops-5inun" class="product-link">Tears Naturale Ii Eye Drops</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                									    	<li class="old-price">₹ 88.70</li>
            										            										    <li class="current-price">₹ 76.70</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 5, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 5, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					                                                                                         <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/led-lamp-shine-pro-prp2v" class="thumbnail">
            								                								    <img class="mx-auto d-block ls-is-cached lazyloaded" src="https://staging.aldebazaar.com/frontend/images/product-thumb.png" alt="Led Lamp Shine Pro">
            									            								</a>
            							</div>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/led-lamp-shine-pro-prp2v" class="product-link">Led Lamp Shine Pro</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                										    <li class="current-price">₹ 345.00</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 4526, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 4526, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					                                                                                         <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/diwan-7836-top-storage--gmwmb" class="thumbnail">
            								                									<img class="mx-auto d-block lazyloaded" src="https://staging.aldebazaar.com/uploads/products/flash_deal/diwan-7836-top-storagethumb1599285786.jpg" data-src="https://staging.aldebazaar.com/uploads/products/flash_deal/diwan-7836-top-storagethumb1599285786.jpg" alt="Diwan 7836 Top Storage ">
            									            								</a>
            							</div>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/diwan-7836-top-storage--gmwmb" class="product-link">Diwan 7836 Top Storage </a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                										    <li class="current-price">₹ 20,690.00</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 18999, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 18999, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					                                                                                         <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/flourish-concepts-cycle-book-end-mkajh" class="thumbnail">
            								                									<img class="mx-auto d-block lazyloaded" src="https://staging.aldebazaar.com/uploads/products/flash_deal/flourish-concepts-cycle-book-endthumb1599302546.jpg" data-src="https://staging.aldebazaar.com/uploads/products/flash_deal/flourish-concepts-cycle-book-endthumb1599302546.jpg" alt="Flourish Concepts Cycle Book End">
            									            								</a>
            							</div>
            							            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;">25% off</li>
            							</ul>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/flourish-concepts-cycle-book-end-mkajh" class="product-link">Flourish Concepts Cycle Book End</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                									    	<li class="old-price">₹ 800.00</li>
            										            										    <li class="current-price">₹ 600.00</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 19166, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 19166, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					                                                                                         <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/flourish-concepts-chain-book-end-yqab5" class="thumbnail">
            								                									<img class="mx-auto d-block lazyloaded" src="https://staging.aldebazaar.com/uploads/products/flash_deal/flourish-concepts-chain-book-endthumb1599302548.jpg" data-src="https://staging.aldebazaar.com/uploads/products/flash_deal/flourish-concepts-chain-book-endthumb1599302548.jpg" alt="Flourish Concepts Chain Book End">
            									            								</a>
            							</div>
            							            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;">27% off</li>
            							</ul>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/flourish-concepts-chain-book-end-yqab5" class="product-link">Flourish Concepts Chain Book End</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                									    	<li class="old-price">₹ 1,100.00</li>
            										            										    <li class="current-price">₹ 803.00</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 19168, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 19168, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					                                                                                         <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="https://staging.aldebazaar.com/product/flourish-concepts-cycle-book-end-locsa" class="thumbnail">
            								                									<img class="mx-auto d-block lazyloaded" src="https://staging.aldebazaar.com/uploads/products/flash_deal/flourish-concepts-cycle-book-endflash_deal1601881494.jpeg" data-src="https://staging.aldebazaar.com/uploads/products/flash_deal/flourish-concepts-cycle-book-endflash_deal1601881494.jpeg" alt="Flourish Concepts Cycle Book End">
            									            								</a>
            							</div>
            							            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;">27% off</li>
            							</ul>
            							            							<div class="product-decs text-center">
            								<h2><a href="https://staging.aldebazaar.com/product/flourish-concepts-cycle-book-end-locsa" class="product-link">Flourish Concepts Cycle Book End</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									                									    	<li class="old-price">₹ 1,100.00</li>
            										            										    <li class="current-price">₹ 803.00</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							                							    <button class="btn btn25" onclick="addToCart(this, 19169, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, 19169, 1, 'full')" type="button"> Buy Now</button>
            							                							</div>
            						</article>
        						</div>
    						     					     				</div>
                    
                    
                    
                                                                        </div>-->
                </div>
            </section>
        </div>
    </div>
    
        
</div>
@endsection
