@extends('frontend.layouts.app')

@section('content')
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
    background:#20b34e!important;
}
</style>
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @if(Auth::user()->user_type == 'seller')
                        @include('frontend.inc.seller_side_nav')
                    @elseif(Auth::user()->user_type == 'customer')
                        @include('frontend.inc.customer_side_nav')
                    @endif
                </div>

                <div class="col-lg-9">
                    <!-- Page title -->
                    <div class="row mb-2">
                        <div class="col-12 p-0">
                    <div class="page-title bg-blue p-3" style="border-radius: 0.25rem; ">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0 text-white p-0">
                                    {{__('Purchase History')}}
                                </h2>
                            </div>
                        </div>
                    </div>
                    </div></div>
               
                    <div class="row">
                        @if (count($orders) > 0)
                            @foreach ($orders as $key => $order)
                                @foreach ($order->orderDetails as $key => $orderDetail)
                                    <article class="card card-product-list">
                                    	<div class="row no-gutters align-items-center">
                                    		<div class="col-md-2 col-3">
                                    			<a href="{{ route('product', $orderDetail->product->slug) }}" >
                                			    	@if($orderDetail->product->thumbnail_img!="")
                                    			    	<img src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($orderDetail->product->thumbnail_img) }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                                    			    @else
                                    			        <img src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                                    			    @endif
                                    			</a>
                                    		</div> 
                                    		<div class="col-md-7 col-9 ">
                                    			<div class="info-main ">
                                    			    @if ($orderDetail->product != null)
                                    			    	<a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank" class="h5 title">{{ $orderDetail->product->name }}</a>
                                    		        @else
                                                        <strong>{{ __('Product Unavailable') }}</strong>
                                                    @endif
                                    		        <p class="info">Order 
                                    		            <a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="text-blue font-weight-bold"># {{ $order->code }}</a> 
                                    		            <span class="block-sm">Delivery Status
                                        		            @php
                                                                $status = $order->orderDetails->first()->delivery_status;
                                                            @endphp
                                                            @if(str_replace('_', ' ', $status)=="pending")
                                                                <span class="badge badge-info"> {{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                                                            @else
                                                                <span class="badge badge-success"> {{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                                                            @endif
                                    		            </span>
                                    		        </p>
                                    		        <p class="info">Order Date <b class="text-blue">{{ date('d-m-Y', $order->date) }} </b>
                                    		           <span class="block-sm">Payment Status 
                                    		             @if ($order->payment_status == 'paid')
                                    		                <span class="badge badge-success"> {{__('Paid')}}</span>
                                                         @else
                                                            <span class="badge badge-warning">{{__('Unpaid')}}</span>
                                                         @endif
                                    		           </span>
                                    		        </p>
                                    		        <span class="price h5 d-block d-md-none">Product Price {{ single_price($orderDetail->price) }} </span>
                                    			</div>
                                    		</div> 
                                    		<div class="col-12 d-block d-md-none mb-2 px-2 mobilebtn">
                                    		    @if ($status == "pending_confirmation" || $status == "pending" || $status == "ready_to_ship")
                                    		    <a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="btn btn-light btn50 float-left bg-blue"> Details </a>
                                    		    <a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="btn btn-light btn50 float-left bg-red">Cancel Order </a>
                                    		    @else
                                    		    <a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="btn btn-light btn-block bg-blue"> Details </a>
                                    		    @endif
                            		            <!--a href="{{ route('customer.invoice.download', $order->id) }}" class="btn btn-light bg-red btn50">Download Invoice</a-->
                                    		</div>
                                    		<div class="col-md-3 d-none d-md-block">
                                    			<div class="info-aside">
                                    				<div class="price-wrap">
                                    					<span class="price h5"><span class="info">Product Price</span> {{ single_price($orderDetail->price) }} </span>	
                                    				</div> <br/>
                                    				<p class="d-none d-md-block">
                                    					<a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="btn btn-light bg-blue btn-block">View Details </a>
                                    					@if ($status == "pending_confirmation" || $status == "pending" || $status == "ready_to_ship")
                                    					<a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="btn btn-light bg-red btn-block">Cancel Order </a>
                                    					@endif
                                    				</p>
                                    			</div> 
                                    		</div> 
                                    	</div> 
                                    </article>
                                @endforeach
                            @endforeach
                        @else
							<div class="card no-border mt-4">
                                <div>
									<table class="table table-sm table-hover table-responsive-md">
                                        <thead>
                                            <tr class="text-center">
                                                <th><h3>{{__('There are no recent orders to show.')}}</h3></th>
                                            </tr>
                                        </thead>
									</table>
								</div>
							</div>
                        @endif
                    </div>
                    <div class="pagination-wrapper py-4">
                        <ul class="pagination justify-content-end">
                            {{ $orders->links() }}
                        </ul>
                    </div>
                </div>
            </div>
            
            
        </div>
    </section>

    <div class="modal fade p-0" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5">{{__('Make Payment')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="payment_modal_body"></div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript">
        $('#order_details').on('hidden.bs.modal', function () {
            location.reload();
        })
    </script>

@endsection
