@extends('frontend.layouts.app')

@section('content')

<div class="py-4 gry-bg" @if($mobileapp == 1) style="margin-top:75px" @endif>
    <div class="mt-4">
        <div class="container-fluid">
            <div class="bg-white px-3 pt-3">
                <div class="row gutters-10">
                    @foreach (\App\Brand::all() as $brand)
                        <div class="col-xxl-2 col-lg-2 col-sm-4 text-center">
							<a href="{{ route('categories.all', ['brand'=>$brand->slug]) }}" class="d-block p-3 mb-3 border rounded">
								@if($brand->logo!=null)
									<img src="{{ asset($brand->logo) }}" class="lazyload img-fit" height="50" alt="{{ __($brand->name) }}">
								@else
									<h5 class="heading-5 strong-400">{{ucwords(strtolower($brand->name))}}</h5>
								@endif
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
