@extends('frontend.layouts.app')

@section('content')
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
   background:#20b34e!important;
}
.process-steps li {
    width: 20%;
    float: left;
    text-align: center;
    position: relative;
}
.select2-container--default .select2-selection--single {
    border-radius: 2px;
    border: 1px solid #e6e6e6;
    background-color: #fff!important;
    font-size: 0.875rem;
    font-weight: 400;
    color: rgba(0, 0, 0, 0.4);
    height: calc(2.25rem + 2px);
    padding: 0 .75rem;
    outline: none;
}
</style>
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('frontend.inc.seller_side_nav')
                </div>
                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="row mb-2">
                            <div class="col-12 p-0">
                                <div class="page-title bg-blue p-3" style="border-radius: 0.25rem; ">
                                    <div class="row align-items-center">
                                        <div class="col-md-6">
                                            <h2 class="heading heading-6 text-capitalize strong-600 mb-0 text-white p-0">
                                                {{__('Orders Recived')}}
                                            </h2>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="float-md-right">
                                                <ul class="breadcrumb">
                                                    <li><a href="{{ route('home') }}" class="text-white">{{__('Home')}}</a></li>
                                                    <li><a href="{{ route('dashboard') }}" class="text-white">{{__('Dashboard')}}</a></li>
                                                    <li class="active"><a href="{{ route('orders.index') }}" class="text-white">{{__('Orders')}}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="page-title bg-white p-3" style="border-radius: 0.25rem;">
                            <form method="get">
                            <div class="row">
                                <div class="col-md-6 text-right pt-2">Filter Product : &nbsp;</div>
                                <div class="col-md-5">
                                       <select name="order_filter" class="form-control">
                                            <option value="pending" >Pending & Ready to Delivery</option>
                                            <option value="shipping" >On Shipping</option>
                                            <option value="delivered" >Delivered</option>
                                        </select>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-primary bg-blue" style="border-color:#232F3E">Go</button>
                                </div>
                            </div>
                            </form>
                        </div>
                        <!--START TABLE ORDER-->
                          <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Order Code')}}</th>
                    <th>{{__('Order Date')}}</th>
                    <th>{{__('Num. of Products')}}</th>
                    <th>{{__('Customer')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Coupen Discount')}}</th>
                    <th>{{__('Order Status')}}</th>
                    <th>{{__('Pay Method')}}</th>
                    <th>{{__('Pay Status')}}</th>
                    <th>{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $key => $order_id)
                    @php
                        $order = \App\Order::find($order_id->id);
                    @endphp
                    @if($order != null)
                    @php
                    if ($order->orderDetails->where('seller_id',  $admin_user_id)->first()->payment_status == 'paid') {
                        $payment_status = "paid";
                    } else {
                        $payment_status = "unpaid";
                    }
                    @endphp
                        <tr>
                            <td>
                                {{ ($key+1) + ($orders->currentPage() - 1)*$orders->perPage() }} <input type="checkbox" data-ordercode="{{$order->code}}" data-payment="{{$payment_status}}" name="order_id[]" value="{{$order->id}}" id="check_order_id" class="check_order_id" />
                            </td>
                            <td>
                                <a href="{{ url('orders', encrypt($order->id)) }}" class="ordcode"> {{ $order->code }}</a> @if($order->viewed == 0) <span class="pull-right badge badge-info">{{ __('New') }}</span> @endif
                            </td>
                            <td>
                                {{ date('d-m-Y h:i A', $order->date) }}
                            </td>
                             @php
								$return_product_id=\App\ReturnProduct::where('order_id',$order_id->id)->pluck('product_id');
								//dd(\Route::currentRouteName());
							@endphp
                            <td class="text-center">
                                @if(count($return_product_id)>0 && (\Route::currentRouteName() == 'orders.index.refunded.admin' || \Route::currentRouteName() == 'orders.index.processing_refund.admin'))
								 Return Request of {{count($return_product_id)}} out of {{ count($order->orderDetails->where('seller_id', $admin_user_id)) }} 
							@else
                                {{ count($order->orderDetails->where('seller_id', $admin_user_id)) }}
							@endif
                            </td>
                            <td>
                                
                                @if(\Route::currentRouteName() == 'orders.index.ready_to_delivery.admin')
                                
                                  @php $shipping_address = json_decode($order->shipping_address,true); @endphp
                                    {{$shipping_address['name']}}
                                    
                                @else
                                @if ($order->user_id != null)
                                      
                                    {{ $order->user->name??' ' }}@if(customerBookingsCount($order->user_id) == 1) <span class="pull-right badge badge-info">{{ __('New Customer') }}</span> @endif
                                @else
                                    Guest ({{ $order->guest_id }})
                                @endif
                                @endif
                            </td>
                            <td>
                               
						
							{{ single_price($order->grand_total ) }}
						
                            </td>
                            <td>
                               
								{{ single_price($order->coupon_discount ) }}
						
                            </td>
                            <td>
                                @php
                                    $status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->delivery_status;
                                @endphp
                                @if($status == "Confirmation Pending")
                                <label class="label label-danger">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @elseif($status == "pending")
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @else
                                <label class="label label-warning">{{ ucfirst(str_replace('_', ' ', $status)) }}</label>
                                @endif
                            </td>
                            <td>
                                {{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}
                            </td>
                            <td>
                                <span class="badge badge--2 mr-4">
                                    @if ($payment_status == 'paid')
                                        <i class="bg-green"></i> Paid
                                    @else
                                        <i class="bg-red"></i> Unpaid
                                    @endif
                                </span>
                            </td>
                            <td>
                                @if(count($order->orderDetails->where('seller_id', $admin_user_id)->where('delivery_status', 'confirmation_pending')) > 0)
                                        <a  href="javascript:confirm_order({{$order->id}}, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a>
                                        @if($order->prescription_file != null) 
                                            <a href="{{ asset('prescription/'.$order->prescription_file) }}" class="btn btn-primary">{{__('Download Prescription')}}</a>
                                        @endif
                                        @if($payment_status == "paid")
                                            <a href="javascript:confirm_order({{$order->id}}, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                      
                                           
                                        @endif
                                       
                                @else
                                    @if($status == "delivered")
                                     @if(count($return_product_id)>0 && (\Route::currentRouteName() == 'orders.index.refunded.admin' || \Route::currentRouteName() == 'orders.index.processing_refund.admin'))
                                         <a href="{{ route('returnorders.show', encrypt($order->id)) }}" class="btn btn-primary">{{__('View Return')}}</a>
                                     @endif
                                    @elseif($status == "pending")
                                        <a  href="javascript:confirm_order({{$order->id}}, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a>
                                        @if($order->prescription_file != null) 
                                            <a href="{{ asset('prescription/'.$order->prescription_file) }}" class="btn btn-primary">{{__('Download Prescription')}}</a>
                                        @endif
                                        <!--a onclick="confirm_modal('{{route('orders.destroy', $order->id)}}');" class="btn btn-primary">{{__('Delete')}}</a-->
                                        @if($payment_status == "paid")
                                            <a href="javascript:confirm_order({{$order->id}}, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                        
                                        @endif
                                    @elseif($status == "ready_to_ship") 
                                        <a href="javascript:order_shipping_info({{ $order->id }})"  class="btn btn-primary">{{__('Add Shipping info')}}</a>
                                        <a href="javascript:confirm_order({{$order->id}}, 'ready_to_delivery')" class="btn btn-primary">Ready to Delivery</a>
                                        <a href="{{ route('orders.print_label', $order->id) }}" target="_blank" class="btn btn-primary">{{__('Print Label')}}</a>
                                    @elseif($status == "shipped" or $status == "on_delivery") 
                                        @if($payment_status == 'unpaid')
                                            <a href="javascript:confirm_payment({{$order->id}}, 'paid')" class="btn btn-primary">Mark Paid</a>
                                            <span id="orderbtn_{{$order->id}}"><a href="javascript:void(0)" class="btn btn-primary" disabled>{{__('Mark Delivered')}}</a></span>
                                        @else
                                        <span id="orderbtn_{{$order->id}}"><a href="javascript:confirm_order({{$order->id}}, 'delivered')" class="btn btn-primary">{{__('Mark Delivered')}}</a></span>
                                        @endif
                                        
                                        @if($payment_status == "paid")
                                            <a href="javascript:confirm_order({{$order->id}}, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                       
                                        @endif
                                        
                                    @elseif($status == "processing_refund") 
                                        <a href="javascript:confirm_order({{$order->id}}, 'refunded')" class="btn btn-primary">Refunded to Wallet</a>
                                    @elseif($status == "ready_to_delivery")
                                        <a href="{{ route('orders.print_label', $order->id) }}" target="_blank" class="btn btn-primary">{{__('Print Label')}}</a>
                                        <button class="btn btn-primary" disabled>{{__('User below button to assign Agent')}}</button>
                                    @elseif($status == "on_review" or $status == "hold")
                                        <a href="javascript:confirm_order({{$order->id}}, 'pending')" class="btn btn-primary">Move to Pending</a>   
                                    @else
                                    <button class="btn btn-primary" disabled>{{__('No Action')}}</button>
                                    @endif
                                @endif
                                
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        
        <div class="clearfix">
            <div class="col-md-12">
                <br />&nbsp;
                @if(\Route::currentRouteName() == 'orders.index.on_delivery.admin')
                <button class="btn btn-danger" onclick="mark_undelivered()">Unable to Delivered</button>
                @endif
                @if(\Route::currentRouteName() == 'orders.index.ready_to_delivery.admin')
                <button class="btn btn-primary" onclick="showmodal('assign_status_modal');">Assign Delivery Agent</button>
                @endif
                
                @if(\Route::currentRouteName() == 'orders.index.on_delivery.admin' || \Route::currentRouteName() == 'orders.index.shipped.admin' )
                <button class="btn btn-success" id="markdeliverybtn" onclick="showmodal('mark_delivery_modal')" disabled>Mark Selected to Delivered</button>
                <button class="btn btn-primary" id="markpaidbtn" onclick="showmodal('payment_status_modal');" disabled>Mark Selected to Paid</button>
                @endif
                
                <br />&nbsp;
                <br />&nbsp;
            </div>
        </div>
        <div class="clearfix">
            <div class="pull-right">
                {{ $orders->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
                        <!--END START TABLE ORDER-->

                      
                       

                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="shipping_info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <form action="{{route('orders.shipping')}}" method="post">
                @csrf
                <div id="modal-body">
                    <div class="card mt-3">
                        <div class="card-header py-2 px-3 ">
                        <div class="heading-6 strong-600">{{__('Shipping Info')}}</div>
                        </div>
                        <div class="card-body pb-0">
                            <input type="hidden" name="order_id" id="modal_order_id">
                            <input type="hidden" name="seller_id" value="{{Auth::User()->id}}">
                            <div class="form-group row">
                                <div class="col-sm-3 text-right pt-2">
                                    Courier &nbsp;
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name='courier' id="shippingModalCourier" placeholder="Courier Company Name" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3 text-right pt-2">
                                    AWB/Ref/Tracking No. &nbsp;
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="awb" class="form-control" id="shippingModalawb" placeholder="AWB Number" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>

@endsection

<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order cancel</h5>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          Warrning:- You can cancel only 2 credit order in this month if you order credit limit will be zero then 5 % amount will be debit from your wallet
        @php
           $seller =  \App\Seller::where('user_id',Auth::user()->id)->get();
                                         
        @endphp
          
          Your Order Credit Limit Is <strong>{{$seller[0]->order_cancel_credit}}</strong>
          <br>
           
          <strong>Rs. <span id="deductedAmont"></span></strong> Amount has been deduct from your wallet if your order credit limit will zero
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" style="background-color: #eb3037;" data-dismiss="modal">Cancel</button>
        
        <!--<button type="button" class="btn btn-primary">Confirm</button>-->
         <form action="{{ route('purchase_history.order_cancel') }}" method="post" class="m-0">
                @csrf <input type="hidden" id="cancelOrderId" name="order_id"/>
            <button type="submit" class="btn btn-light bg-blue float-right text-white">{{__('Confirm')}}</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--END MODEL-->

<script>
    function orderCancel($orderId){
        
        $('#cancelOrderId').val($orderId);
         $.post('{{ route('orders.get_deducted_cancel_amount') }}', {_token:'{{ @csrf_token() }}', order_id:$orderId}, function(data){
                 //location.reload();
                 //alert('amount'+ data);
                 $('#deductedAmont').html(data);
            });
        
    }
    
</script>

@section('script')
<script>
    function order_shipping_info(order_id)
    {
        $('#shippingModalCourier').val("");
        $('#shippingModalawb').val("");
        $('#modal_order_id').val(order_id);
        $('#shipping_info').modal();
        $('.c-preloader').hide();
    }
    
    function confirm_order(order_id, status) {
        var setFlash = 'Order status has been updated';
        seller_id = {{Auth::User()->id}};
        $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, seller_id : seller_id}, function(data){
             location.reload();
        });
    }
    
    function mark_delivered() {
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "delivered";
        var setFlash = 'Order set for delivered status.';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
             location.reload();
        });
    }
        
    function assign_delivery() {
        agent_id = $('#modal_delivery_agent_id').val();
        $('#modal_delivery_agent_id').val("");
        $('#modal_delivery_agent_id').trigger('change');
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "assign";
        var setFlash = 'Order set for out for delivery status.';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, agent_id:agent_id, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
             location.reload();
        });
    }
        
    function change_status(action) {
        if(action == 'delivery') {
            status = $('#modal_delivery_status').val();
            $('#modal_delivery_status').val("");
            $('#modal_delivery_status').trigger('change');
        }
        if(action == 'payment') {
            status = $('#modal_payment_status').val();
            $('#modal_payment_status').val("");
            $('#modal_payment_status').trigger('change');
        }
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        }); 
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        
        var setFlash = 'Order '+action+' status has been updated';
        $.post('{{ route('orders.update_status') }}', {_token:'{{ @csrf_token() }}', action:action, setFlash:setFlash, order_ids:productArray, status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
             location.reload();
        });
    }
        
    function order_shipping_info(order_id)
    {
        $('#modal_order_id').val(order_id);
        $('#shipping_info').modal();
    }
        function sort_orders(el){
            $('#sort_orders').submit();
        }
    
        function confirm_payment(order_id, status) {
            var setFlash = 'Payment status has been updated';
            $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                location.reload();
            });
        }

        function confirm_order(order_id, status) {
            var setFlash = 'Order status has been updated';
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                 location.reload();
            });
        }

        function trash_order(order_id, status) {
            var setFlash = 'Order status has been updated';
            $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}', setFlash:setFlash, order_id:order_id,status:status, @if(isset($seller_id)) seller_id : {{$seller_id}} @endif}, function(data){
                 location.reload();
            });
        }
</script>
@endsection

