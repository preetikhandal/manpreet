<!-- FOOTER -->

<footer id="footer" class="footer ">
    <div class="footer-top pb-0 pt-0">
        <div class="container-fluid">
            <div class="row mb-4 " style="border-bottom:1px solid #eee;padding:1rem 0">
				<div class="col">
					<h4 class="heading heading-xs strong-600 text-uppercase">
						{{__('Shop by Category')}}</h4>
					<ul class="list-inline">
						@foreach (\App\Category::orderby('position')->get() as $key => $category)	
                            @php $brands = array(); @endphp
							<li class="list-inline-item pb-2">
								<a href="{{ route('products.category', $category->slug) }}" class="footercategories" >
                                    <span class="cat-name">{{ __($category->name) }}</span>
                                </a>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
            
            <div class="row cols-xs-space cols-sm-space cols-md-space pb-2">
                @php
                    $generalsetting = \App\GeneralSetting::first();
                @endphp
                <div class="col-md-2 col-6 mb-0">
					<div class="col text-left ">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
                            {{__('Contact Info')}}
                        </h4>
                        <ul class="footer-links contact-widget">
                            <li>
                               <span class="d-block heading strong-600">{{__('Address')}}:</span>
                               <span class="d-block">{{ $generalsetting->address }}</span>
                            </li>
                            <li>
								<span class="d-block heading strong-600">{{__('Phone')}}:</span>
                               <span class="d-block"><a href="tel:{{ $generalsetting->phone }}">{{ $generalsetting->phone }}</a></span>
                            </li>
                            <li>
								<span class="d-block heading strong-600">{{__('Email')}}:</span>
                               <span class="d-block">
                                   <a href="mailto:{{ $generalsetting->email }}">{{ $generalsetting->email  }}</a>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
				<div class="col-6 col-md-2 text-left mb-0">
                    <div class="col">
						<h4 class="heading heading-xs strong-600 text-uppercase mb-2">
                          {{__('Important Links')}}
                       </h4>
						<ul class="footer-links">
						    <li><a href="{{ url('cashback-condition') }}" title="">{{__('Cashback Condition')}}</a></li>
							<li><a href="{{ route('terms') }}" title="">{{__('Terms')}}</a></li>
							<li><a href="{{ route('privacypolicy') }}" title="">{{__('Privacy Policy')}}</a></li>
							<li><a href="{{ route('sellerpolicy') }}" title="">{{__('Seller Policy')}}</a></li>
							<li><a href="{{ route('returnpolicy') }}" title="">{{__('Return Policy')}}</a></li>
							<li><a href="{{ route('supportpolicy') }}" title="">{{__('Support Policy')}}</a></li>
						</ul>
					</div>
					<div class="col text-left p-0 d-block d-md-none mt-4">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
						   {{__('Company Overview')}}
                        </h4>
                        <ul class="footer-links rightSide">
							<li><a href="{{ route('blog') }}" title="Blog">{{__('Blog')}}</a></li>
							<li><a href="{{ route('feedback-form') }}" title="Blog" class="btn btn-base-1 feedbackBtn btn-icon-left bg-blue">{{__('Feedback')}}</a></li>
							<li><a href="{{ route('pharmacy-enquiry') }}" title="Blog" class="btn btn-base-1 feedbackBtn btn-icon-left bg-blue">{{__('Pharmacy')}}</a></li>
                        </ul>
                    </div>
                </div>
				<div class="col-6 col-md-2 d-none d-md-block">
                    <div class="col text-center text-md-left">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
						   {{__('Company Overview')}}
                        </h4>
                        <ul class="footer-links">
							<!--<li><a href="{{ url('about-us') }}" title="About Us">{{__('About Us')}}</a></li>
							<li><a href="{{ url('career-opportunities') }}" title="We are Hiring">{{__('We are Hiring')}}</a></li>
							<li><a href="{{ url('contact-us') }}" title="Contact Us">{{__('Contact Us')}}</a></li> -->
							<li><a href="{{ route('blog') }}" title="Blog">{{__('Blog')}}</a></li>
							<li><a style="padding: 5px 10px;
    color: white;
    background: black;
    border: 1px solid black;" href="{{ route('feedback-form') }}" class="btn btn-base-1 btn-icon-left bg-blue" title="feedback">{{__('Feedback')}}</a></li>
	<li><a style="padding: 5px 10px;
    color: white;
    background: black;
    border: 1px solid black;margin-top: 9px;" href="{{ route('pharmacy-enquiry') }}" class="btn btn-base-1 btn-icon-left bg-blue" title="feedback">{{__('Pharmacy')}}</a></li>
                        </ul>
                    </div>
                </div>
				<div class="col-md-2 col-lg-2 col-4">
                    <div class="col text-left p-0">
                       <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
                          {{__('My Account')}}
                       </h4>
                       <ul class="footer-links">
                            @if (Auth::check())
                                <li>
                                    <a href="{{ route('logout') }}" title="Logout">
                                        {{__('Logout')}}
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{ route('user.login') }}" title="Login">
                                        {{__('Login')}}
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ route('purchase_history.index') }}" title="Order History">
                                    {{__('Order History')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('wishlists.index') }}" title="My Wishlist">
                                    {{__('My Wishlist')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('orders.track') }}" title="Track Order">
                                    {{__('Track Order')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    @if (\App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
                        <div class="col d-none d-md-block">
                            <div class="mt-4">
                                <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
                                    {{__('Be a Seller')}}
                                </h4>
                                <a href="{{ route('shops.create') }}" class="btn btn-base-1 btn-icon-left bg-blue">
                                    {{__('Apply Now')}}
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
				<div class="col-md-4 col-lg-4 col-8">
					<div class="row">
						<div class="col-md-6">
							<div class="col text-center text-md-left p-0">
								<h4 class="heading heading-xs strong-600 text-uppercase text-left mb-2">{{__('Connect')}}</h4>
								<ul class="my-3 my-md-0 social-nav deepssocial model-2">
									@if ($generalsetting->facebook != null)
										<li>
											<a href="{{ $generalsetting->facebook }}" target="_blank" class="fa fa-facebook"></a>
										</li>
									@endif
									@if ($generalsetting->instagram != null)
										<li>
											<a href="{{ $generalsetting->instagram }}" target="_blank"><img  src="{{ asset('frontend/img/Insta icons.png')}}" alt="instragram"></a>
										</li>
									@endif
									@if ($generalsetting->twitter != null)
										<li>
											<a href="{{ $generalsetting->twitter }}" target="_blank" class="fa fa-twitter"></a>
										</li>
									@endif
									@if ($generalsetting->youtube != null)
										<li>
											<a href="{{ $generalsetting->youtube }}" class="fa fa-youtube" style="background: red" target="_blank"></a>
										</li>
									@endif
								</ul>
								<ul class="inline-links">
									@if (\App\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1)
										<li>
											<img loading="lazy" alt="paypal" src="{{ asset('frontend/images/icons/cards/paypal.png')}}" height="20">
										</li>
									@endif
									@if (\App\BusinessSetting::where('type', 'stripe_payment')->first()->value == 1)
										<li>
											<img loading="lazy" alt="stripe" src="{{ asset('frontend/images/icons/cards/stripe.png')}}" height="20">
										</li>
									@endif
									@if (\App\BusinessSetting::where('type', 'sslcommerz_payment')->first()->value == 1)
										<li>
											<img loading="lazy" alt="sslcommerz" src="{{ asset('frontend/images/icons/cards/sslcommerz.png')}}" height="20">
										</li>
									@endif
									@if (\App\BusinessSetting::where('type', 'instamojo_payment')->first()->value == 1)
										<li>
											<img loading="lazy" alt="instamojo" src="{{ asset('frontend/images/icons/cards/instamojo.png')}}" height="20">
										</li>
									@endif
									@if (\App\BusinessSetting::where('type', 'razorpay')->first()->value == 1)
										<li>
											<img loading="lazy" alt="razorpay" src="{{ asset('frontend/images/icons/cards/rozarpay.png')}}" style="height: 35px">
										</li>
									@endif
									@if (\App\BusinessSetting::where('type', 'paystack')->first()->value == 1)
										<li>
											<img loading="lazy" alt="paystack" src="{{ asset('frontend/images/icons/cards/paystack.png')}}" height="20">
										</li>
									@endif
									@if (\App\BusinessSetting::where('type', 'cash_payment')->first()->value == 1)
										<li>
											<img loading="lazy" alt="cash on delivery" src="{{ asset('frontend/images/icons/cards/cod.png')}}" style="height: 35px">
										</li>
									@endif
									@if (\App\Addon::where('unique_identifier', 'offline_payment')->first() != null && \App\Addon::where('unique_identifier', 'offline_payment')->first()->activated)
										@foreach(\App\ManualPaymentMethod::all() as $method)
										  <li>
											<img loading="lazy" alt="{{ $method->heading }}" src="{{ asset($method->photo)}}" height="20">
										</li>
										@endforeach
									@endif
								</ul>
							</div>
						</div>
						<div class="col-md-6" @if($mobileapp == 1) style="display:none" @endif>
							<div class="col text-center">
								<h4 class="heading heading-xs strong-600 text-uppercase mb-2">{{__('Download App')}}</h4>
								 <ul class="footer-links">
									 <li>
										<a href="https://play.google.com/store/apps/details?id=com.aldeb.aldebazaar" target="_blank">
											<img src="{{asset('frontend/images/icons/playstore.png')}}" class="img-fluid mx-auto" style="width:136px; height: 41px;">
										</a>
									</li>
									<li>
										<a href="https://apps.apple.com/app/id1533227336" target="_blank">
											<img src="{{asset('frontend/images/icons/applestore.png')}}" class="img-fluid mx-auto" style="width:136px; height: 41px;">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
            </div>
			<!--<div class="line mt-4 mb-4" style="border-top: 1px solid #d4d3cd;"></div>-->
			<!--<div class="row cols-xs-space cols-sm-space cols-md-space text-center text-md-left deeps_auth-bar mt-4">
				<div class="col-md-4">
					<div class="row align-items-center no-gutters">
						<div class="col-md-4 text-center">
							<img src="{{asset('frontend/images/icons/secure-rebrand_x6f8yq.svg') }}" class="img-fluid img lazyload">
						</div>
						<div class="info col-md-8">
							<h5 class="font18">Reliable</h5><p class="font12 text-gray">All products displayed on ALDE BAZAAR are procured from verified and licensed pharmacies. All labs listed on the platform are accredited</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row align-items-center no-gutters">
						<div class="col-md-4 text-center">
							<img src="{{asset('frontend/images/icons/reliable-rebrand_rcpof3.svg') }}" class="img-fluid img lazyload">
						</div>
						<div class="info col-md-8">
							<h5 class="font18">Secure</h5><p class="font12 text-gray">ALDE BAZAAR uses Secure Sockets Layer (SSL) 128-bit encryption and is Payment Card Industry Data Security Standard (PCI DSS) compliant</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row align-items-center no-gutters">
						<div class="col-md-4 text-center">
							<img src="{{asset('frontend/images/icons/affordable-rebrand_ivgidq.svg') }}" class="img-fluid img lazyload">
						</div>
						<div class="info col-md-8">
							<h5 class="font18">Affordable</h5><p class="font12 text-gray">Find affordable medicine substitutes, save up to 50% on health products, up to 80% off on lab tests and free doctor consultations.</p>
						</div>
					</div>
				</div>
			</div>-->
        </div>
    </div>

    <div class="footer-bottom py-2 sct-color-3">
        <div class="container-fluid">
            <div class="row row-cols-xs-spaced flex flex-items-xs-middle">
				<div class="col">
					<div class="copyright text-center">
                        <ul class="copy-links no-margin">
                            <li>
                                © {{ date('Y') }} {{ $generalsetting->site_name }}
                            </li>
						</ul>
					</div>
				</div>
            </div>
        </div>
    </div>
</footer>
