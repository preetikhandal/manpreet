<div class="header bg-white d-none d-lg-block">
	@php
		if (Cache::has('generalsetting')){
		   $generalsetting =  Cache::get('generalsetting');
		} else {
			 $generalsetting = \App\GeneralSetting::first();
			Cache::forever('generalsetting', $generalsetting);
		}
	@endphp
    <!-- Top Bar -->
    <div class="top-navbar">
        <div class="container-fluid" style="width:97%">
            <div class="row">
                <div class="col-12 text-right">
                    <ul class="inline-links">
                        @if (\App\BusinessSetting::where('type', 'classified_product')->first()->value)
                            <li>
                                <a href="{{ route('customer_packages_list_show') }}" class="top-bar-item">{{__('Classified Packages')}}</a>
                            </li>
                        @endif
                        <li>
                            <a href="{{ route('orders.track') }}" class="top-bar-item">{{__('Track Order')}}</a>
                        </li>
                        @if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated)
                            <li>
                                <a href="{{ route('affiliate.apply') }}" class="top-bar-item">{{__('Be an affiliate partner')}}</a>
                            </li>
                        @endif
                        @auth
                        <li>
                            <a href="{{ route('dashboard') }}" class="top-bar-item">{{__('My Account')}}</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" class="top-bar-item">{{__('Logout')}}</a>
                        </li>
                        @else
                        <li>
                            <a href="{{ route('user.login') }}" class="top-bar-item">{{__('Login')}}</a>
                        </li>
                        <li>
                            <a href="{{ route('user.registration') }}" class="top-bar-item">{{__('Registration')}}</a>
                        </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </div>
    </div>
	<!-- END Top Bar -->
    <div class="position-relative logo-bar-area">
        <div class="">
            <div class="container-fluid" style="max-width: 100%!important;padding-left:10px;padding-right:10px;">
                <div class="row no-gutters align-items-center">
                    <div class="col-lg-2 col-8">
                        <div class="d-flex">
                            <!-- Brand/Logo -->
                            <a class="navbar-brand w-100 " href="{{ route('home') }}">
                                @if($generalsetting->logo != null)
                                    <img src="{{ asset($generalsetting->logo) }}" alt="{{ env('APP_NAME') }}">
                                @else
                                    <img src="{{ asset('frontend/images/logo/logo.png') }}" alt="{{ env('APP_NAME') }}">
                                @endif
                            </a>
                            @if(Route::currentRouteName() != 'home' && Route::currentRouteName() != 'categories.all')
                                <!--<div class="d-none d-xl-block category-menu-icon-box">
                                    <div class="dropdown-toggle navbar-light category-menu-icon" id="category-menu-icon">
                                        <span class="navbar-toggler-icon"></span>
                                    </div>
                                </div>-->
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-10 col-4 position-static">
                        <div class="row d-flex w-100">
                            <div class="search-box flex-grow-1 px-3">
                                <form action="{{ route('search') }}" method="GET">
                                    <div class="d-flex position-relative">
                                        <div class="d-lg-none search-box-back">
                                            <button class="" type="button"><i class="la la-long-arrow-left"></i></button>
                                        </div>
                                        <div class="w-100">
                                            <input type="text" aria-label="Search" id="search" name="q" class="w-100" placeholder="{{__('I am shopping for...')}}" autocomplete="off">
                                        </div>
                                        <div class="form-group category-select d-none d-xl-block">
                                            <select class="form-control selectpicker" name="category">
                                                <option value="">{{__('All Categories')}}</option>
												@php
													if (Cache::has('Categories')){
													   $Categories =  Cache::get('Categories');
													} else {
														$Categories = \App\Category::all();
														Cache::forever('Categories', $Categories);
													}
												@endphp
                                                @foreach ($Categories as $key => $category)
                                                <option value="{{ $category->slug }}"
                                                    @isset($category_id)
                                                        @if ($category_id == $category->id)
                                                            selected
                                                        @endif
                                                    @endisset
                                                    >{{ __($category->name) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button class="d-none d-lg-block" type="submit" style="background:#fff;border:1px solid #EB3038">
                                            <i class="la la-search la-flip-horizontal"  style="color:#EB3038"></i>
                                        </button>
                                        <div class="typed-search-box d-none">
                                            <div class="search-preloader">
                                                <div class="loader"><div></div><div></div><div></div></div>
                                            </div>
                                            <div class="search-nothing d-none">

                                            </div>
                                            <div id="search-content" style="max-height:400px;overflow-y: scroll;">

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="logo-bar-icons d-inline-block ml-auto">
                                <div class="d-inline-block d-lg-none">
                                    <div class="nav-search-box">
                                        <a href="#" class="nav-box-link">
                                            <i class="la la-search la-flip-horizontal d-inline-block nav-box-icon"></i>
                                        </a>
                                    </div>
                                </div>
								<!--<div class="d-none d-lg-inline-block">
                                    <div class="nav-wishlist-box" id="wishlist">
                                        <a href="#" data-toggle="modal" data-target="#pincodemodal" class="nav-box-link">
                                            <i class="fa fa-map-marker d-inline-block nav-box-icon" ></i>
                                            <span class="nav-box-text d-none d-xl-inline-block" >{{__('Others')}}</span>
											<i class="fa fa-caret-down" style="font-size: 16px;position: relative;top: 2px; color: #000; padding-right: 2px;"></i>
                                        </a>
                                    </div>
                                </div>-->
								<div class="d-none d-lg-inline-block">
                                    <div class="nav-wishlist-box" id="wishlist">
                                        <a href="tel:{{ $generalsetting->phone }}" class="nav-box-link">
                                            <i class="fa fa-phone-square d-inline-block nav-box-icon" ></i>
                                            <span class="nav-box-text d-none d-xl-inline-block" > {{ $generalsetting->phone }}</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="d-none d-lg-inline-block">
                                    <div class="nav-wishlist-box" id="wishlist">
                                        <a href="{{ route('wishlists.index') }}" class="nav-box-link">
                                            <i class="la la-heart-o d-inline-block nav-box-icon" style="font-size:20px"></i>
                                            <span class="nav-box-text d-none d-xl-inline-block">{{__('Wishlist')}}</span>
                                            @if(Auth::check())
                                                <span class="nav-box-number" style="background-color:#282563">{{ count(Auth::user()->wishlists)}}</span>
                                            @else
                                                <span class="nav-box-number" style="background-color:#282563">0</span>
                                            @endif
                                        </a>
                                    </div>
                                </div>
                                <div class="d-inline-block" data-hover="dropdown">
                                    <div class="nav-cart-box dropdown" id="cart_items">
                                        <a href="" class="nav-box-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="la la-shopping-cart d-inline-block nav-box-icon"  style="top:6px;"></i>
                                            <span class="nav-box-text d-none d-xl-inline-block">{{__('Cart')}}</span>
                                            @if(Session::has('cart'))
                                                <span class="nav-box-number" style="background-color:#282563;top: -4px;">{{ count(Session::get('cart'))}}</span>
                                            @else
                                                <span class="nav-box-number" style="background-color:#282563;top: -4px;">0</span>
                                            @endif
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right px-0">
                                            <li>
                                                <div class="dropdown-cart px-0">
                                                    @if(Session::has('cart'))
                                                        @if(count($cart = Session::get('cart')) > 0)
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{__('Cart Items')}}</h3>
                                                            </div>
                                                            <div class="dropdown-cart-items c-scrollbar">
                                                                @php
                                                                    $total = 0;
                                                                @endphp
                                                                @foreach($cart as $key => $cartItem)
                                                                    @php
                                                                        $product = \App\Product::find($cartItem['id']);
                                                                        $total = $total + $cartItem['price']*$cartItem['quantity'];
                                                                    @endphp
                                                                    <div class="dc-item">
                                                                        <div class="d-flex align-items-center">
                                                                            <div class="dc-image">
                                                                                <a href="{{ route('product', $product->slug) }}">
                                                                                    <img src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($product->thumbnail_img) }}" class="img-fluid lazyload" alt="{{ __($product->name) }}">
                                                                                </a>
                                                                            </div>
                                                                            <div class="dc-content">
                                                                                <span class="d-block dc-product-name text-capitalize strong-600 mb-1">
                                                                                    <a href="{{ route('product', $product->slug) }}">
                                                                                        {{ __(ucwords(strtolower($product->name))) }}
                                                                                    </a>
                                                                                </span>

                                                                                <span class="dc-quantity">x{{ $cartItem['quantity'] }}</span>
                                                                                <span class="dc-price">{{ single_price($cartItem['price']*$cartItem['quantity']) }}</span>
                                                                            </div>
                                                                            <div class="dc-actions">
                                                                                <button onclick="removeFromCart({{ $key }})">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                            <div class="dc-item py-3">
                                                                <span class="subtotal-text">{{__('Subtotal')}}</span>
                                                                <span class="subtotal-amount">{{ single_price($total) }}</span>
                                                            </div>
                                                            <div class="py-2 text-center dc-btn">
                                                                <ul class="inline-links inline-links--style-3">
                                                                    <li class="px-1">
                                                                        <a href="{{ route('cart') }}" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1">
                                                                            <i class="la la-shopping-cart"></i> {{__('View cart')}}
                                                                        </a>
                                                                    </li>
                                                                    @if (Auth::check())
                                                                    <li class="px-1">
                                                                        <a href="{{ route('checkout.shipping_info') }}" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1 light-text">
                                                                            <i class="la la-mail-forward"></i> {{__('Checkout')}}
                                                                        </a>
                                                                    </li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        @else
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{__('Your Cart is empty')}}</h3>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <div class="dc-header">
                                                            <h3 class="heading heading-6 strong-700">{{__('Your Cart is empty')}}</h3>
                                                        </div>
                                                    @endif
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hover-category-menu" id="hover-category-menu">
            <div class="container">
                <div class="row no-gutters position-relative">
                    <div class="col-lg-3 position-static">
                        <div class="category-sidebar" id="category-sidebar">
                            <div class="all-category">
                                <span>{{__('CATEGORIES')}}</span>
                                <a href="{{ route('categories.all') }}" class="d-inline-block">See All ></a>
                            </div>
                            <ul class="categories">
                                @foreach (\App\Category::all()->take(11) as $key => $category)
                                    @php
                                        $brands = array();
                                    @endphp
                                    <li class="category-nav-element" data-id="{{ $category->id }}">
                                        <a href="{{ route('products.category', $category->slug) }}">
											@if(!empty($category->icon))
												<img class="cat-image lazyload" src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($category->icon) }}" width="30" alt="{{ __($category->name) }}">
											@endif
                                            <span class="cat-name">{{ __($category->name) }}</span>
                                        </a>
                                        @if(count($category->subcategories)>0)
                                            <div class="sub-cat-menu c-scrollbar">
                                                <div class="c-preloader">
                                                    <i class="fa fa-spin fa-spinner"></i>
                                                </div>
                                            </div>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Navbar -->

    <!--<div class="main-nav-area d-none d-lg-block">
        <nav class="navbar navbar-expand-lg navbar--bold navbar--style-2 navbar-light bg-default">
            <div class="container">
                <div class="collapse navbar-collapse align-items-center justify-content-center" id="navbar_main">
                    <ul class="navbar-nav">
                        @foreach (\App\Search::orderBy('count', 'desc')->get()->take(5) as $key => $search)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('suggestion.search', $search->query) }}">{{ $search->query }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </nav>
    </div> -->
	<div class="main-nav-area d-none d-lg-block">
        <nav class="navbar navbar-expand-lg navbar--bold navbar--style-2 navbar-light bg-default">
            <div class="container">
                <div class="collapse navbar-collapse align-items-center justify-content-center" id="navbar_main">
                    <ul class="navbar-nav mx-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="{{url('categories')}}" id="navbarDropdown" > {{__('All Categories')}}</a>
							<ul class="dropdown-menu " aria-labelledby="navbarDropdown">
								@foreach (\App\Category::all() as $key => $category)
									@php $brands = array(); @endphp 
									<li @if(count($category->subcategories)>0) class='has-submenu'  @endif>
										<a class="dropdown-item @if(count($category->subcategories)>0) dropdown-toggle @endif" href="{{ route('products.category', $category->slug) }}">
										{{ __(strtolower($category->name)) }}</a>
										@if(count($category->subcategories)>0)
											@if(!empty($category->menu_banner))
												<div class="megasubmenu dropdown-menu" style="background:url('{{asset($category->menu_banner)}}') right bottom no-repeat #fff; width:600px">
											@else
												<div class="megasubmenu dropdown-menu" style="background:#fff; width:600px">
											@endif
										
												<h5 class="">{{ __($category->name) }}</h5>
												<ul class="list-unstyled">
													@foreach (\App\SubCategory::where('category_id',$category->id)->get() as $key => $subcategory)
														<li><a class="nav-link"href="{{ route('products.subcategory', $subcategory->slug) }}">{{ __(strtolower($subcategory->name)) }}</a></li>
													@endforeach
												</ul>	
											</div>
										@endif
									</li>
								@endforeach
							</ul>
						</li>
						
						@php
							if (Cache::has('menu')){
							   $menu =  Cache::get('menu');
							} else {
								 $menu = \App\Menu::orderBy('position')->get();
								Cache::forever('menu', $menu);
							}
						@endphp

						@foreach($menu as $key => $menu)	
							<li class="nav-item">
								<a class="nav-link" href="{{$menu->link}}">{{$menu->name}}</a>
							</li>
						@endforeach
					</ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<!---Only For Small Devices--->
<div class="header bg-white d-block d-lg-none">
    <!-- mobile menu -->
    <div class="mobile-side-menu d-lg-none">
        <div class="side-menu-overlay opacity-0" onclick="sideMenuClose()"></div>
        <div class="side-menu-wrap opacity-0">
            <div class="side-menu closed">
                <div class="side-menu-header " style="background:#EB3038">
                    <div class="side-menu-close py-1" onclick="sideMenuClose()">
                        <i class="la la-close"></i>
                    </div>
                    @auth
                        <div class="widget-profile-box px-2 py-4 d-flex align-items-center">
                            @if (Auth::user()->avatar_original != null)
                                <!--<div class="image " style="background-image:url('{{ asset(Auth::user()->avatar_original) }}')"></div>-->
                            @else
                                <!--<div class="image " style="background-image:url('{{ asset('frontend/images/user.png') }}')"></div>-->
                            @endif

                            <div class="name"><i class="fa fa-user" aria-hidden="true" style="font-size:17px;"></i> {{ Auth::user()->name }}</div>
                        </div>
                        <div class="side-login px-3 pb-3">
                            <a href="{{ route('logout') }}">{{__('Sign Out')}}</a>
                        </div>
                    @else
                        <!--<div class="widget-profile-box px-3 py-4 d-flex align-items-center">
                                <div class="image " style="background-image:url('{{ asset('frontend/images/icons/user-placeholder.jpg') }}')"></div>
                        </div>-->   
                        <div class="side-login px-3 py-3">
							<i class="fa fa-user" aria-hidden="true" style="font-size:17px;"></i> Hello,
                            <a href="{{ route('user.login') }}">{{__('Login & Signup')}}</a>
                           <!-- <a href="{{ route('user.registration') }}">{{__('Register')}}</a>-->
                        </div>
                    @endauth
                </div>
                <div class="side-menu-list">
                    <ul class="side-user-menu">
                        <li>
                            <a href="{{ route('home') }}">
                                <!--<i class="la la-home"></i>-->
                                <span>{{__('Home')}}</span>
                            </a>
                        </li>
						<div class="border-top my-1"></div>
						<li>
                            <a href="{{url('categories')}}">
                                <span>{{__('Shop by Category')}}</span>
                            </a>
                        </li>
						@foreach(\App\Menu::orderBy('position')->get() as $key => $menu)	
							<li>
								<a href="{{$menu->link}}">
									<span>{{$menu->name}}</span>
								</a>
							</li>
						@endforeach
						<div class="border-top my-1"></div>
					   <li>
                            <a href="{{ route('dashboard') }}">
                                <!--<i class="la la-dashboard"></i>-->
                                <span>{{__('My Account')}}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('purchase_history.index') }}">
                                <!--<i class="la la-file-text"></i>-->
                                <span>{{__('My Orders')}}</span>
                            </a>
                        </li>
                        @auth
                            @php
                                $conversation = \App\Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();
                            @endphp
                            @if (\App\BusinessSetting::where('type', 'conversation_system')->first()->value == 1)
                                <li>
                                    <a href="{{ route('conversations.index') }}" class="{{ areActiveRoutesHome(['conversations.index', 'conversations.show'])}}">
                                        <!--<i class="la la-comment"></i>-->
                                        <span class="category-name">
                                            {{__('Conversations')}}
                                            @if (count($conversation) > 0)
                                                <span class="ml-2" style="color:green"><strong>({{ count($conversation) }})</strong></span>
                                            @endif
                                        </span>
                                    </a>
                                </li>
                            @endif
                        @endauth
                        <li>
                            <a href="{{ route('cart') }}">
                                <!--<i class="la la-shopping-cart"></i>-->
                                <span>{{__('Cart')}}</span>
                                @if(Session::has('cart'))
                                    <span class="badge" id="cart_items_sidenav">{{ count(Session::get('cart'))}}</span>
                                @else
                                    <span class="badge" id="cart_items_sidenav">0</span>
                                @endif
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('wishlists.index') }}">
                                <!--<i class="la la-heart-o"></i>-->
                                <span>{{__('Wishlist')}}</span>
                            </a>
                        </li>

                        @if (\App\BusinessSetting::where('type', 'wallet_system')->first()->value == 1)
                            <li>
                                <a href="{{ route('wallet.index') }}">
                                    <!--<i class="la la-inr"></i>-->
                                    <span>{{__('My Wallet')}}</span>
                                </a>
                            </li>
                        @endif
						
                        @php
                        $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
                        $club_point_addon = \App\Addon::where('unique_identifier', 'club_point')->first();
                        @endphp
                        @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
                            <li>
                                <a href="{{ route('customer_refund_request') }}" class="{{ areActiveRoutesHome(['customer_refund_request'])}}">
                                    <!--<i class="la la-file-text"></i>-->
                                    <span class="category-name">
                                        {{__('Sent Refund Request')}}
                                    </span>
                                </a>
                            </li>
                        @endif

                        @if ($club_point_addon != null && $club_point_addon->activated == 1)
                            <li>
                                <a href="{{ route('earnng_point_for_user') }}" class="{{ areActiveRoutesHome(['earnng_point_for_user'])}}">
                                    <!--<i class="la la-dollar"></i>-->
                                    <span class="category-name">
                                        {{__('Earning Points')}}
                                    </span>
                                </a>
                            </li>
                        @endif

                        <li>
                            <a href="{{ route('support_ticket.index') }}" class="{{ areActiveRoutesHome(['support_ticket.index', 'support_ticket.show'])}}">
                                <!--<i class="la la-support"></i>-->
                                <span class="category-name">
                                    {{__('Support Ticket')}}
                                </span>
                            </a>
                        </li>
						<div class="border-top my-1"></div>
                    </ul>
                   
					@if (Auth::check() && Auth::user()->user_type == 'seller')
                        <div class="sidebar-widget-title py-0">
                            <span>{{__('Shop Options')}}</span>
                        </div>
                        <ul class="side-seller-menu">
                            <li>
                                <a href="{{ route('seller.products') }}">
                                    <!--<i class="la la-diamond"></i>-->
                                    <span>{{__('Products')}}</span>
                                </a>
                            </li>
                            @if (\App\Addon::where('unique_identifier', 'pos_system')->first() != null && \App\Addon::where('unique_identifier', 'pos_system')->first()->activated)
                                <li>
                                    <a href="{{route('poin-of-sales.seller_index')}}">
                                        <!--<i class="la la-fax"></i>-->
                                        <span>
                                            {{__('POS Manager')}}
                                        </span>
                                    </a>
                                </li>
                            @endif

                            <li>
                                <a href="{{ route('orders.index') }}">
                                    <!--<i class="la la-file-text"></i>-->
                                    <span>{{__('Orders')}}</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('shops.index') }}">
                                    <!--<i class="la la-cog"></i>-->
                                    <span>{{__('Shop Setting')}}</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('withdraw_requests.index') }}">
                                    <!--<i class="la la-money"></i>-->
                                    <span>
                                        {{__('Money Withdraw')}}
                                    </span>
                                </a>
                            </li>

                            @php
                                $conversation = \App\Conversation::where('receiver_id', Auth::user()->id)->where('receiver_viewed', '1')->get();
                            @endphp
                            @if (\App\BusinessSetting::where('type', 'conversation_system')->first()->value == 1)
                                <li>
                                    <a href="{{ route('conversations.index') }}" class="{{ areActiveRoutesHome(['conversations.index', 'conversations.show'])}}">
                                        <!--<i class="la la-comment"></i>-->
                                        <span class="category-name">
                                            {{__('Conversations')}}
                                            @if (count($conversation) > 0)
                                                <span class="ml-2" style="color:green"><strong>({{ count($conversation) }})</strong></span>
                                            @endif
                                        </span>
                                    </a>
                                </li>
                            @endif

                            <li>
                                <a href="{{ route('payments.index') }}">
                                    <!--<i class="la la-cc-mastercard"></i>-->
                                    <span>{{__('Payment History')}}</span>
                                </a>
                            </li>
                        </ul>
                        <div class="sidebar-widget-title py-0">
                            <span>{{__('Earnings')}}</span>
                        </div>
                        <div class="widget-balance py-3">
                            <div class="text-center">
                                <div class="heading-4 strong-700 mb-4">
                                    @php
                                        $orderDetails = \App\OrderDetail::where('seller_id', Auth::user()->id)->where('created_at', '>=', date('-30d'))->get();
                                        $total = 0;
                                        foreach ($orderDetails as $key => $orderDetail) {
                                            if($orderDetail->order != null && $orderDetail->order != null && $orderDetail->order->payment_status == 'paid'){
                                                $total += $orderDetail->price;
                                            }
                                        }
                                    @endphp
                                    <small class="d-block text-sm alpha-5 mb-2">{{__('Your earnings (current month)')}}</small>
                                    <span class="p-2 bg-base-1 rounded">{{ single_price($total) }}</span>
                                </div>
                                <table class="text-left mb-0 table w-75 m-auto">
                                    <tbody>
                                        <tr>
                                            @php
                                                $orderDetails = \App\OrderDetail::where('seller_id', Auth::user()->id)->get();
                                                $total = 0;
                                                foreach ($orderDetails as $key => $orderDetail) {
                                                    if($orderDetail->order != null && $orderDetail->order->payment_status == 'paid'){
                                                        $total += $orderDetail->price;
                                                    }
                                                }
                                            @endphp
                                            <td class="p-1 text-sm">
                                                {{__('Total earnings')}}:
                                            </td>
                                            <td class="p-1">
                                                {{ single_price($total) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            @php
                                                $orderDetails = \App\OrderDetail::where('seller_id', Auth::user()->id)->where('created_at', '>=', date('-60d'))->where('created_at', '<=', date('-30d'))->get();
                                                $total = 0;
                                                foreach ($orderDetails as $key => $orderDetail) {
                                                    if($orderDetail->order != null && $orderDetail->order->payment_status == 'paid'){
                                                        $total += $orderDetail->price;
                                                    }
                                                }
                                            @endphp
                                            <td class="p-1 text-sm">
                                                {{__('Last Month earnings')}}:
                                            </td>
                                            <td class="p-1">
                                                {{ single_price($total) }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
					 <div class="sidebar-widget-title py-0">
						<span>{{__('Contact Us')}}</span>
					</div>
					<div class="py-3 px-3">
						<span class="mail_txt"><span class="mail_txt">Customer Care : <a href="tel:{{ $generalsetting->phone }}" target="_blank" rel="noopener">{{ $generalsetting->phone }}</a><br></span></span>
						<span class="mail_txt"><span class="mail_txt">Email : <a href="mailto:{{ $generalsetting->email }}" target="_blank" rel="noopener">{{ $generalsetting->email }}</a><br></span></span>
						
						
						<!--<p style="margin: 0px; padding: 0px 0px 8px; font-size: 13px;">If you encounter any bugs, glitches, lack of functionality, delayed deliveries, billing errors or other problems on the beta website, please email us on <a href="mailto:{{ $generalsetting->email  }}" style="color: #008ecc;">{{ $generalsetting->email  }}</a></p>-->
					</div>	
					<div class="sidebar-widget-title py-0">
						<span>{{__('Download App')}}</span>
					</div>
					<div class="py-3 px-3">	
						<div class="row">
							<div class="col-6 p-2">
								<a href="https://play.google.com/store/apps/details?id=com.aldeb.aldebazaar" target="_blank" rel="noopener"><img src="{{asset('frontend/images/icons/playstore.png')}}" alt="Download Alde App for Android from Play Store" class="img-fluid mx-auto" style="height: 41px;"></a>
							</div>
							<div class="col-6 p-2">
								<a href="" target="_blank" rel="noopener"><img src="{{asset('frontend/images/icons/applestore.png')}}" alt="Download Alde App for iOs from App Store" class="img-fluid mx-auto" style="height: 41px;"></a>
							</div>
						</div>
						
					</div>
		   </div>
        </div>
    </div>
    <!-- end mobile menu -->

    <div class="position-relative logo-bar-area  @if($mobileapp == 1)  sm-fixed-top @endif">
        <div class="">
            <div class="container-fluid" style="max-width:100%!important;">
                @if($mobileapp != 1)
                <div class="row no-gutters align-items-center">
                    <div class="col-lg-2 col-8">
                        <div class="d-flex">
                            <div class="d-block d-lg-none mobile-menu-icon-box">
                                <!-- Navbar toggler  -->
                                <a href="" onclick="sideMenuOpen(this)">
                                    <div class="hamburger-icon">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                            </div>

							<a class="navbar-brand w-100 " href="{{ route('home') }}">
                                @php
                                    $generalsetting = \App\GeneralSetting::first();
                                @endphp
                                @if($generalsetting->logo != null)
                                    <img src="{{ asset('frontend/images/logo/white-mobile-logo.png') }}" alt="{{ env('APP_NAME') }}">
                                @else
                                    <img src="{{ asset('frontend/images/logo/logo.png') }}" alt="{{ env('APP_NAME') }}">
                                @endif
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-10 col-4 position-static">
                        <div class="row d-flex w-100">
                            

                            <div class="logo-bar-icons d-inline-block ml-auto">
								<div class="d-inline-block">
                                    <div class="nav-wishlist-box" id="wishlist">
										@auth
											<a href="{{ route('logout') }}" class="nav-box-link">
												<span class="nav-box-text" style="font-size:13px;color:#282563">{{__('Logout')}}</span>
											</a>
										@else
											<a href="{{ route('user.login') }}" class="nav-box-link">
												<span class="nav-box-text" style="font-size:13px;color:#282563">{{__('Sign In')}}</span>
											</a>
										@endauth
                                    </div>
                                </div>
                                <div class="d-inline-block" data-hover="dropdown">
                                    <div class="nav-cart-box dropdown" id="cart_items">
                                        <a href="" class="nav-box-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="la la-shopping-cart d-inline-block nav-box-icon"></i>
                                            <span class="nav-box-text d-none d-xl-inline-block">{{__('Cart')}}</span>
                                            @if(Session::has('cart'))
                                                <span class="nav-box-number" style="background-color:#282563">{{ count(Session::get('cart'))}}</span>
                                            @else
                                                <span class="nav-box-number" style="background-color:#282563">0</span>
                                            @endif
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right px-0">
                                            <li>
                                                <div class="dropdown-cart px-0">
                                                    @if(Session::has('cart'))
                                                        @if(count($cart = Session::get('cart')) > 0)
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{__('Cart Items')}}</h3>
                                                            </div>
                                                            <div class="dropdown-cart-items c-scrollbar">
                                                                @php
                                                                    $total = 0;
                                                                @endphp
                                                                @foreach($cart as $key => $cartItem)
                                                                    @php
                                                                        $product = \App\Product::find($cartItem['id']);
                                                                        $total = $total + $cartItem['price']*$cartItem['quantity'];
                                                                    @endphp
                                                                    <div class="dc-item">
                                                                        <div class="d-flex align-items-center">
                                                                            <div class="dc-image">
                                                                                <a href="{{ route('product', $product->slug) }}">
                                                                                    <img src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($product->thumbnail_img) }}" class="img-fluid lazyload" alt="{{ __($product->name) }}">
                                                                                </a>
                                                                            </div>
                                                                            <div class="dc-content">
                                                                                <span class="d-block dc-product-name text-capitalize strong-600 mb-1">
                                                                                    <a href="{{ route('product', $product->slug) }}">
                                                                                        {{ __($product->name) }}
                                                                                    </a>
                                                                                </span>

                                                                                <span class="dc-quantity">x{{ $cartItem['quantity'] }}</span>
                                                                                <span class="dc-price">{{ single_price($cartItem['price']*$cartItem['quantity']) }}</span>
                                                                            </div>
                                                                            <div class="dc-actions">
                                                                                <button onclick="removeFromCart({{ $key }})">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                            <div class="dc-item py-3">
                                                                <span class="subtotal-text">{{__('Subtotal')}}</span>
                                                                <span class="subtotal-amount">{{ single_price($total) }}</span>
                                                            </div>
                                                            <div class="py-2 text-center dc-btn">
                                                                <ul class="inline-links inline-links--style-3">
                                                                    <li class="px-1">
                                                                        <a href="{{ route('cart') }}" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1">
                                                                            <i class="la la-shopping-cart"></i> {{__('View cart')}}
                                                                        </a>
                                                                    </li>
                                                                    @if (Auth::check())
                                                                    <li class="px-1">
                                                                        <a href="{{ route('checkout.shipping_info') }}" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1 light-text">
                                                                            <i class="la la-mail-forward"></i> {{__('Checkout')}}
                                                                        </a>
                                                                    </li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        @else
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{__('Your Cart is empty')}}</h3>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <div class="dc-header">
                                                            <h3 class="heading heading-6 strong-700">{{__('Your Cart is empty')}}</h3>
                                                        </div>
                                                    @endif
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			    @else
					<div class="row no-gutters align-items-center">
						<div class="col-12">
							<br/>
						</div>
					</div>
				@endif
				<div class="row no-gutters align-items-center">
					 <div class="col-12">
						<form action="{{ route('search') }}" method="GET">
							<div class="input-group mt-2 mb-1">
								<input type="text" id="search1" name="q"  class="form-control" placeholder="{{__('I am shopping for...')}}" autocomplete="off" style="border:1px solid #EB3038!important">
								<div class="input-group-append">
									<button class="btn" style="background:#EB3038" type="submit"><i class="la la-search la-flip-horizontal d-inline-block nav-box-icon text-white" ></i></button>
								</div>
								<div class="typed-search-box d-none">
									<div class="search-preloader">
										<div class="loader"><div></div><div></div><div></div></div>
									</div>
									<div class="search-nothing d-none">

									</div>
									<div id="search-content1" style="max-height:400px;overflow-y: scroll;">

									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
        <div class="hover-category-menu" id="hover-category-menu">
            <div class="container">
                <div class="row no-gutters position-relative">
                    <div class="col-lg-3 position-static">
                        <div class="category-sidebar" id="category-sidebar">
                            <div class="all-category">
                                <span>{{__('CATEGORIES')}}</span>
                                <a href="{{ route('categories.all') }}" class="d-inline-block">See All ></a>
                            </div>
                            <ul class="categories">
                                @foreach (\App\Category::all()->take(11) as $key => $category)
                                    @php
                                        $brands = array();
                                    @endphp
                                    <li class="category-nav-element" data-id="{{ $category->id }}">
                                        <a href="{{ route('products.category', $category->slug) }}">
                                            <img class="cat-image lazyload" src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($category->icon) }}" width="30" alt="{{ __($category->name) }}">
                                            <span class="cat-name">{{ __($category->name) }}</span>
                                        </a>
                                        @if(count($category->subcategories)>0)
                                            <div class="sub-cat-menu c-scrollbar">
                                                <div class="c-preloader">
                                                    <i class="fa fa-spin fa-spinner"></i>
                                                </div>
                                            </div>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Navbar -->
</div>
	
	
<!-- The Modal -->
<div class="modal fade text-center py-5" id="pincodemodal">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header p-2" style="border-bottom: 1px solid transparent;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
			<div class="top-strip"></div>
			<h3 class="mb-0 font23">Where do you want the delivery?</h3>
			<p class="py-1 text-muted font15" id="delivery_location"><i class="fa fa-map-marker modal-icon"></i> We can't detect your location</p>
			<div class="popularRegionsBgBrd"><span class="js_pin_location_msg2">enter a pincode</span></div>
			<form class="mt-3" method="post">
				{!! csrf_field() !!}
				<div class="input-group w-75 mx-auto">
				  <input type="text" class="form-control" name="pincode" id="pincode" placeholder="Enter Pincode" aria-label="Enter Pincode" aria-describedby="button-addon2" autocomplete="off" maxlength=6 pattern="[0-9]{6}" onkeypress="return isNumberKey(event);" required>
				  <div class="input-group-append">
					<button class="btn btn-primary" type="button" id="pincodeBtn">Apply</button>
				  </div>
				</div>
				<p class="text-danger pinerror text-center w-75 font-weight-bold"></p>
			</form>
			<p class="pb-1 text-muted"><small class="text-danger" id="pincodeErr"></small></p>
      </div>
	  
    </div>
  </div>
</div>	
	
	
	