<!DOCTYPE html>
@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
<html dir="rtl" lang="en">
@else
<html lang="en">
@endif
<head>

@php
    $seosetting = \App\SeoSetting::first();
@endphp

<meta charset="utf-8">
@if($mobileapp == 1)
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
@else
<meta name="viewport" content="width=device-width, initial-scale=1.0">
@endif
<meta name="robots" content="index, follow">
<meta name="description" content="@yield('meta_description', $seosetting->description)" />
<meta name="keywords" content="@yield('meta_keywords', $seosetting->keyword)">
<meta name="author" content="{{ $seosetting->author }}">
<meta name="sitemap_link" content="{{ $seosetting->sitemap_link }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
@yield('meta')

@if(!isset($detailedProduct))
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ config('app.name', 'Laravel') }}">
    <meta itemprop="description" content="{{ $seosetting->description }}">
    <meta itemprop="image" content="{{ asset(\App\GeneralSetting::first()->logo) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ config('app.name', 'Laravel') }}">
    <meta name="twitter:description" content="{{ $seosetting->description }}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="{{ asset(\App\GeneralSetting::first()->logo) }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ config('app.name', 'Laravel') }}" />
    <meta property="og:type" content="Ecommerce Site" />
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:image" content="{{ asset(\App\GeneralSetting::first()->logo) }}" />
    <meta property="og:description" content="{{ $seosetting->description }}" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
@endif

<!-- Favicon -->
<link type="image/x-icon" href="{{ asset(\App\GeneralSetting::first()->favicon) }}" rel="shortcut icon" />

<title>@yield('meta_title', $seosetting->meta_title)</title>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet" media="none" onload="if(media!='all')media='all'">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}" type="text/css" media="all">

<!-- Icons -->
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}" type="text/css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" href="{{ asset('frontend/css/line-awesome.min.css') }}" type="text/css" media="none" onload="if(media!='all')media='all'">

<link type="text/css" href="{{ asset('frontend/css/bootstrap-tagsinput.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jodit.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/sweetalert2.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/slick.css') }}" rel="stylesheet" media="all">
<link type="text/css" href="{{ asset('frontend/css/xzoom.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jquery.share.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/intlTelInput.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('css/spectrum.css')}}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">

<!-- Global style (main) -->
<link type="text/css" href="{{ asset('frontend/css/alde-shop.css?v=1.2') }}" rel="stylesheet" media="all">


<link type="text/css" href="{{ asset('frontend/css/main.css?v=1.3') }}" rel="stylesheet" media="all">

@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
     <!-- RTL -->
    <link type="text/css" href="{{ asset('frontend/css/alde.rtl.css?v=1.2') }}" rel="stylesheet" media="all">
@endif

<!-- Facebook Chat style -->
<link href="{{ asset('frontend/css/fb-style.css?v=1.2')}}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">

<!-- color theme -->
<link href="{{ asset('frontend/css/colors/'.\App\GeneralSetting::first()->frontend_color.'.css?v=1.2')}}" rel="stylesheet" media="all">

<!-- Custom style -->
<link type="text/css" href="{{ asset('frontend/css/custom-style.css?v=1.4') }}" rel="stylesheet" media="all">
<link type="text/css" href="{{ asset('frontend/css/owl.carousel.min.css?v=1.3') }}" rel="stylesheet" media="all">
<link type="text/css" href="{{ asset('frontend/css/alde-carousel.min.css?v=1.5') }}" rel="stylesheet" media="all">

<!-- jQuery -->
<script src="{{ asset('frontend/js/vendor/jquery.min.js') }}"></script>


@if (\App\BusinessSetting::where('type', 'google_analytics')->first()->value == 1)
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('TRACKING_ID') }}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '{{ env('TRACKING_ID') }}');
    </script>
@endif

@if(env('APP_ENV') == 'prod')

@if(\Route::currentRouteName() == 'order_confirmed')

<!-- Facebook Pixel Code >
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '310372787152856'); 
fbq('track', 'PageView');
fbq('track', 'Purchase', {value: 0.00, currency: 'INR'});
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=310372787152856&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

@else
<!-- Facebook Pixel Code >
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '310372787152856'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=310372787152856&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

@endif

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '803618233673342'); 
fbq('track', 'PageView');
@if(\Route::currentRouteName() == 'cart')
fbq('track', 'AddToCart');
@elseif(\Route::currentRouteName() == 'checkout.shipping_info')
fbq('track', 'InitiateCheckout');
@elseif(\Route::currentRouteName() == 'checkout.store_shipping_infostore')
fbq('track', 'AddPaymentInfo');
@elseif(\Route::currentRouteName() == 'order_confirmed')
fbq('track', 'Purchase');
@endif
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=803618233673342&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
@endif

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-179498873-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-179498873-1');
</script>

@if(env('APP_ENV') == 'prod')
<!-- Global site tag (gtag.js) - Google Ads: 467641837 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-467641837"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-467641837');
</script>

<script>
  window.addEventListener('load',function(){
    if(jQuery('.category-filter').length>0){
  gtag('event', 'conversion', {'send_to': 'AW-467641837/vcAVCNOzvu0BEO3L_t4B'});
    } 
    if(window.location.pathname.indexOf('/order-confirmed')!=-1){
        gtag('event', 'conversion', {'send_to': 'AW-467641837/uFKfCMfV0O0BEO3L_t4B'});
    } 
    jQuery('.btn--add-to-cart').click(function(){
        gtag('event', 'conversion', {'send_to': 'AW-467641837/Hi2nCP3M0O0BEO3L_t4B'});
    })
  })
</script>
@endif
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<style>
.best-sell-slider-2 {
    margin-right:15px;
    display:none;
}
.swiper-button-next {
right: 0px;
}

.swiper-button-next, .swiper-button-prev {
    position: absolute;
    top: calc(50% - 50px) !important;
    display: inline-block;
    font-size: 0;
    cursor: pointer;
    text-align: center;
    color: #000;
    border: 1px solid #ebebeb;
    background: #fff;
    z-index: 9;
    opacity: 1;
    border-radius: 3px 0 0 3px;
    -webkit-transition: all 300ms linear;
    -moz-transition: all 300ms linear;
    -ms-transition: all 300ms linear;
    -o-transition: all 300ms linear;
    transition: all 300ms linear;
    -webkit-transition: all 300ms linear;
    -moz-transition: all 300ms linear;
    -ms-transition: all 300ms linear;
    -o-transition: all 300ms linear;
    transition: all 300ms linear;
    height: 80px!important;
    line-height: 100px;
    width: 35px;
    box-shadow: 0 1px 3px #888;
}
.swiper-button-next:before {
content: "\f105";
}
.swiper-button-prev:before {
content: "\f104";
}
.swiper-button-next:before, .swiper-button-prev:before {
font-size: 18px;
line-height: 80px;
display: block;
font-family: "Fontawesome";
font-weight: 600;
}
.swiper-button-next:after, .swiper-button-prev:after {
content: '';
}
.swiper-button-next:hover, .swiper-button-prev:hover {
background: rgba(0, 0, 0, 0.1);
}
</style>

</head>
<body>


<!-- MAIN WRAPPER -->
<div class="body-wrap shop-default shop-cards shop-tech gry-bg">

    <!-- Header -->
    @include('frontend.inc.nav')

    @yield('content')

    @include('frontend.inc.footer')

    @include('frontend.partials.modal')

    @if (\App\BusinessSetting::where('type', 'facebook_chat')->first()->value == 1)
        <div id="fb-root"></div>
        <!-- Your customer chat code -->
        <div class="fb-customerchat"
          attribution=setup_tool
          page_id="{{ env('FACEBOOK_PAGE_ID') }}">
        </div>
    @endif

    <div class="modal fade" id="addToCart">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <button type="button" class="close absolute-close-btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div id="addToCart-modal-body">

                </div>
            </div>
        </div>
    </div>

</div><!-- END: body-wrap -->

<!-- SCRIPTS -->
<a href="#" class="back-to-top btn-back-to-top"></a>

<!-- Core -->
<script src="{{ asset('frontend/js/vendor/popper.min.js') }}"></script>
<script src="{{ asset('frontend/js/vendor/bootstrap.min.js') }}"></script>

<!-- Plugins: Sorted A-Z -->
<script src="{{ asset('frontend/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('frontend/js/select2.min.js') }}"></script>
<script src="{{ asset('frontend/js/nouislider.min.js') }}"></script>


<script src="{{ asset('frontend/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('frontend/js/slick.min.js') }}"></script>

<script src="{{ asset('frontend/js/jquery.share.js') }}"></script>

<script>
    function showFrontendAlert(type, message, time=2000){
        if(type == 'danger'){
            type = 'error';
        }
        swal({
            position: 'top-end',
            type: type,
            title: message,
            showConfirmButton: false,
            timer: time
        });
    }
</script>

@foreach (session('flash_notification', collect())->toArray() as $message)
    <script>
        showFrontendAlert('{{ $message['level'] }}', '{{ $message['message'] }}', 3000);
    </script>
@endforeach

<script src="{{ asset('frontend/js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('frontend/js/jodit.min.js') }}"></script>
<script src="{{ asset('frontend/js/xzoom.min.js') }}"></script>
<script src="{{ asset('frontend/js/fb-script.js') }}"></script>
<script src="{{ asset('frontend/js/lazysizes.min.js') }}"></script>
<script src="{{ asset('frontend/js/intlTelInput.min.js') }}"></script>

<!-- App JS -->
<script src="{{ asset('frontend/js/alde-shop.js') }}"></script>

@if($mobileapp == 1)
<script src="{{ asset('frontend/js/main.app.js?v=1.2') }}"></script>
@else
<script src="{{ asset('frontend/js/main.js') }}"></script>
@endif

<script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
@php $currentURL= (substr(strrchr(url()->current(),"/"),1)); $user=""; $seller="";
    if(isset(Auth::user()->user_type)=="user")
        $user ="user";
    if(isset(Auth::user()->user_type)=="seller")
        $seller="seller";
@endphp
<!--if($mobileapp == 0 && $currentURL!="login" && $currentURL!="registration" && $currentURL!="cart" && $currentURL!="checkout" && $currentURL!="delivery_info" && $currentURL!="order-confirmed" && $user != 'customer' && $seller != 'seller')-->
@if($currentURL!="login" && $currentURL!="registration" && $currentURL!="delivery_info")
<script async type='text/javascript' src="{{asset('frontend/js/wa.js')}}"></script>
<div class="wa__btn_popup d-sm-none">
    <div class="wa__btn_popup_txt"> <strong>Whatsapp for order</strong></div>
    <div class="wa__btn_popup_icon"></div>
</div>
<div class="wa__popup_chat_box">
    <div class="wa__popup_heading">
        <div class="wa__popup_title">Start a Conversation</div>
        <div class="wa__popup_intro">Hi! Click one of our member below to chat on <strong>Whatsapp</strong></a></div>
    </div>
    <!-- /.wa__popup_heading -->
    <div class="wa__popup_content wa__popup_content_left">
        <div class="wa__popup_notice">The team typically replies in a few minutes.</div>
        <div class="wa__popup_content_list">
			<div class="wa__popup_content_item">
				<a target="_blank" href="https://api.whatsapp.com/send?phone=918006880055" class="wa__stt wa__stt_online">
					<div class="wa__popup_avatar">
						<div class="wa__cs_img_wrap" style="background: url({{asset('frontend/images/chatLogo.png')}}) center center no-repeat; background-size: cover;"></div>
					</div>
					<div class="wa__popup_txt">
						<div class="wa__member_name">Alde Bazaar</div>
						<!-- /.wa__member_name -->
						<div class="wa__member_duty">Online</div>
						<!-- /.wa__member_duty -->
					 </div>
					<!-- /.wa__popup_txt -->
				</a>
			</div>
        </div>
        <!-- /.wa__popup_content_list -->
    </div>
    <!-- /.wa__popup_content -->
</div>
<!-- /.wa__popup_chat_box -->
@endif
<script>

    $(document).ready(function() {
        $('.category-nav-element').each(function(i, el) {
            $(el).on('mouseover', function(){
                if(!$(el).find('.sub-cat-menu').hasClass('loaded')){
                    $.post('{{ route('category.elements') }}', {_token: '{{ csrf_token()}}', id:$(el).data('id')}, function(data){
                        $(el).find('.sub-cat-menu').addClass('loaded').html(data);
                    });
                }
            });
        });
        if ($('#lang-change').length > 0) {
            $('#lang-change .dropdown-item a').each(function() {
                $(this).on('click', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    var locale = $this.data('flag');
                    $.post('{{ route('language.change') }}',{_token:'{{ csrf_token() }}', locale:locale}, function(data){
                        location.reload();
                    });

                });
            });
        }

        if ($('#currency-change').length > 0) {
            $('#currency-change .dropdown-item a').each(function() {
                $(this).on('click', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    var currency_code = $this.data('currency');
                    $.post('{{ route('currency.change') }}',{_token:'{{ csrf_token() }}', currency_code:currency_code}, function(data){
                        location.reload();
                    });

                });
            });
        }
    });

	var searchtimer;
    $('#search').on('keyup', function(){
        clearTimeout(searchtimer);
        searchtimer = setTimeout(function() {search("#search");}, 500);
    });

    $('#search').on('focus', function(){
	    clearTimeout(searchtimer);
        searchtimer = setTimeout(function() {search("#search");}, 500);
    });
	var search1timer;
	$('#search1').on('keyup', function(){
	    clearTimeout(search1timer);
        search1timer = setTimeout(function() {search("#search1");}, 500);
    });

    $('#search1').on('focus', function(){
        clearTimeout(search1timer);
        search1timer = setTimeout(function() {search("#search1");}, 500);
    });

    var q = 0;
    function search(search_value){
        var search = $(search_value).val();
        q = Math.round(Math.random() * 10000);
        if(search.length > 0){
            $('body').addClass("typed-search-box-shown");

            $('.typed-search-box').removeClass('d-none');
            $('.search-preloader').removeClass('d-none');
            $.post('{{ route('search.ajax') }}', { _token: '{{ @csrf_token() }}', search:search, q:q}, function(data){
                    if(data.q == q) {
                       if(data.data == '0'){
                        // $('.typed-search-box').addClass('d-none');
                        if(search_value=="#search")
                            $('#search-content').html(null);
                        if(search_value=="#search1")
                            $('#search-content1').html(null);
                        $('.typed-search-box .search-nothing').removeClass('d-none').html('Sorry, nothing found for <strong>"'+search+'"</strong>');
                        $('.search-preloader').addClass('d-none');

                    }
                    else{
                        $('.typed-search-box .search-nothing').addClass('d-none').html(null);

                      if(search_value=="#search") $('#search-content').html(data.data);

                       if(search_value=="#search1") $('#search-content1').html(data.data);
                        $('.search-preloader').addClass('d-none');
                    } 
                }
            });
        }
        else {
            $('.typed-search-box').addClass('d-none');
            $('body').removeClass("typed-search-box-shown");
        }
    }

    function countCookie() {
        no = 0;
        $('.nav-cart-box .dc-quantity').each(function(i,v) { 
            no = no + parseInt($(v).html().slice(1));
        });
        no = no / 2;
        
        $.cookie("cart_items_count", no, { expires: 1 , path: '/' });
    }
    
    function updateNavCart(){
        $.post('{{ route('cart.nav_cart') }}', {_token:'{{ csrf_token() }}'}, function(data){
            $('.cart_items').html(data.data);
            $.cookie("cart_items_count", data.cart_items_count, { expires: 1 , path: '/' });
        });
    }
updateNavCart();


setTimeout(function() { 
    @if(Auth::User())
    $.cookie("name", "{{Auth::User()->name}}", { expires: 1 , path: '/' });
    @else
    $.cookie("name", "", { expires: 1 , path: '/' });
    @endif
} , 500);

    function removeFromCart(key){
        $.post('{{ route('cart.removeFromCart') }}', {_token:'{{ csrf_token() }}', key:key}, function(data){
            updateNavCart();
            $('#cart-summary').html(data.data);
            showFrontendAlert('success', 'Item has been removed from cart');
            $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())-1);
        });
    }

    function addToCompare(id){
        $.post('{{ route('compare.addToCompare') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
            $('#compare').html(data);
            showFrontendAlert('success', 'Item has been added to compare list');
            $('#compare_items_sidenav').html(parseInt($('#compare_items_sidenav').html())+1);
        });
    }

    function addToWishList(id){
        @if (Auth::check() && (Auth::user()->user_type == 'customer' || Auth::user()->user_type == 'seller'))
            $.post('{{ route('wishlists.store') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
                if(data != 0){
                    $('#wishlist').html(data);
                    showFrontendAlert('success', 'Item has been added to wishlist');
                }
                else{
                    showFrontendAlert('warning', 'Please login first');
                }
            });
        @else
            showFrontendAlert('warning', 'Please login first');
        @endif
    }

    function showAddToCartModal(id){
        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }
        $('#addToCart-modal-body').html(null);
        $('#addToCart').modal();
        $('.c-preloader').show();
        $.post('{{ route('cart.showCartModal') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
            $('.c-preloader').hide();
            $('#addToCart-modal-body').html(data);
            $('.xzoom, .xzoom-gallery').xzoom({
                Xoffset: 20,
                bg: true,
                tint: '#000',
                defaultScale: -1
            });
            getVariantPrice();
        });
    }

    $('#option-choice-form input').on('change', function(){
       getVariantPrice();
    });

    function getVariantPrice(){
        if($('#option-choice-form input[name=quantity]').val() > 0 && checkAddToCartValidity()){
            $.ajax({
               type:"POST",
               url: '{{ route('products.variant_price') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   //$('#option-choice-form #chosen_price_div').removeClass('d-none');
                   //$('#option-choice-form #chosen_price_div #chosen_price').html(data.price+""+discount_calulate);
                   //$('#available-quantity').html(data.quantity);
                   $('.input-number').prop('max', data.quantity);
                   //console.log(data.quantity);
                   if(parseInt(data.quantity) < 1 && data.digital  != 1){
                       $('.buy-now').hide();
                       $('.add-to-cart').hide();
                   }
                   else{
                       $('.buy-now').show();
                       $('.add-to-cart').show();
                   }
               }
           });
        }
    }

    function checkAddToCartValidity(){
        var names = {};
        $('#option-choice-form input:radio').each(function() { // find unique names
              names[$(this).attr('name')] = true;
        });
        var count = 0;
        $.each(names, function() { // then count them
              count++;
        });

        if($('#option-choice-form input:radio:checked').length == count){
            return true;
        }

        return false;
    }
    
    function addToCart(ele,product_id, quantity = 1, btnsize = 'full') {
        var html = $(ele).html();
        $(ele).prop('disabled', true);
        if(btnsize == 'full') {
            $(ele).html('<i class="fa fa-spin fa-spinner"></i> Loading');
        } else {
            $(ele).html('<i class="fa fa-spin fa-spinner"></i>');
        }
        
        $.ajax({
               type:"POST",
               url: '{{ route('cart.addToCart') }}',
               data: {id : product_id, quantity : quantity},
               success: function(data){
                   $(ele).html(html);
                   $(ele).prop('disabled', false);
                   updateNavCart();
                   $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())+1);
                    showFrontendAlert('success', 'Product added into cart.');
               }
           });
    }

    function buyNow(ele, product_id, quantity = 1, btnsize = 'full'){
        var html = $(ele).html();
        $(ele).prop('disabled', true);
        if(btnsize == 'full') {
            $(ele).html('<i class="fa fa-spin fa-spinner"></i> Loading');
        } else {
            $(ele).html('<i class="fa fa-spin fa-spinner"></i>');
        }
        //$('#addToCart').modal();
        //$('.c-preloader').show();
        $.ajax({
           type:"POST",
           url: '{{ route('cart.addToCart') }}',
           data: {id : product_id, quantity : quantity},
           success: function(data){
               $(ele).html(html);
               $(ele).prop('disabled', false);
               //$('#addToCart-modal-body').html(null);
               //$('.c-preloader').hide();
               //$('#modal-size').removeClass('modal-lg');
               //$('#addToCart-modal-body').html(data);
               updateNavCart();
               $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())+1);
               window.location.replace("{{ route('cart') }}");
           }
       });
    }

    function show_purchase_history_details(order_id)
    {
        $('#order-details-modal-body').html(null);

        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }

        $.post('{{ route('purchase_history.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id}, function(data){
            $('#order-details-modal-body').html(data);
            $('#order_details').modal();
            $('.c-preloader').hide();
        });
    }
function show_return_history_details(order_id,product_id)
    {
        $('#return-details-modal-body').html(null);

        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }

        $.post('{{ route('return_history.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id , product_id : product_id }, function(data){
            $('#return-details-modal-body').html(data);
            $('#return_details').modal();
            $('.c-preloader').hide();
        });
    }
    function show_order_details(order_id)
    {
        $('#order-details-modal-body').html(null);

        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }

        $.post('{{ route('orders.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id}, function(data){
            $('#order-details-modal-body').html(data);
            $('#order_details').modal();
            $('.c-preloader').hide();
        });
    }

    function cartQuantityInitialize(){
        $('.btn-number').click(function(e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());

            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });

        $('.input-number').focusin(function() {
            $(this).data('oldValue', $(this).val());
        });

        $('.input-number').change(function() {

            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }

     function imageInputInitialize(){
         $('.custom-input-file').each(function() {
             var $input = $(this),
                 $label = $input.next('label'),
                 labelVal = $label.html();

             $input.on('change', function(e) {
                 var fileName = '';

                 if (this.files && this.files.length > 1)
                     fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                 else if (e.target.value)
                     fileName = e.target.value.split('\\').pop();

                 if (fileName)
                     $label.find('span').html(fileName);
                 else
                     $label.html(labelVal);
             });

             // Firefox bug fix
             $input
                 .on('focus', function() {
                     $input.addClass('has-focus');
                 })
                 .on('blur', function() {
                     $input.removeClass('has-focus');
                 });
         });
     }

</script>
<script>
/*Auto Show/Hide Menu Dropdown*/
	const $dropdown = $(".dropdown");
	const $dropdownToggle = $(".dropdown-toggle");
	const $dropdownMenu = $(".dropdown-menu");
	//mega menu
	const $hassubmenu = $(".has-submenu");
	const showClass = "show";

	$(window).on("load resize", function() {
	  if (this.matchMedia("(min-width: 768px)").matches) {
		$dropdown.hover(
		  function() {
			const $this = $(this);
			$this.addClass(showClass);
			$this.find($dropdownToggle).attr("aria-expanded", "true");
			$this.find($dropdownMenu).addClass(showClass);
		  },
		  function() {
			const $this = $(this);
			$this.removeClass(showClass);
			$this.find($dropdownToggle).attr("aria-expanded", "false");
			$this.find($dropdownMenu).removeClass(showClass);
		  }
		);
		$hassubmenu.hover(
		  function() {
			const $this = $(this);
			$this.addClass(showClass);
			$this.find($dropdownToggle).attr("aria-expanded", "true");
			$this.find($dropdownMenu).addClass(showClass);
		  },
		  function() {
			const $this = $(this);
			$this.removeClass(showClass);
			$this.find($dropdownToggle).attr("aria-expanded", "false");
			$this.find($dropdownMenu).removeClass(showClass);
		  }
		);
		
	  } else {
		$dropdown.off("mouseenter mouseleave");
	  }
	});
	

</script>

<script type="text/javascript">
	$(function(){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	});
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyB1CpDOSkWVilSvtKf4Mq7Bp3dOri0Rh74"></script>
<script>
function getLocation(){
    if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(showPosition,showError);
        $.cookie("GeoRun", true);
    }
    else{
        console.log("Geolocation is not supported by this browser.");
        $.cookie("GeoRun", true);
    }
}

function showPosition(position){
    lat=position.coords.latitude;
    lon=position.coords.longitude;
    displayLocation(lat,lon);
}

function showError(error){
    switch(error.code){
        case error.PERMISSION_DENIED:
            console.log("User denied the request for Geolocation.");
        break;
        case error.POSITION_UNAVAILABLE:
            console.log("Location information is unavailable.");
        break;
        case error.TIMEOUT:
            console.log("The request to get user location timed out.");
        break;
        case error.UNKNOWN_ERROR:
            console.log("An unknown error occurred.");
        break;
    }
}

function displayLocation(latitude,longitude){
    var geocoder;
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(latitude, longitude);

    geocoder.geocode(
        {'latLng': latlng}, 
        function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var add= results[0].formatted_address ;
                    var  value=add.split(",");

                    count=value.length;
                    // country=value[count-1];
                    // state=value[count-2];
                    city=value[count-3];
					city=$.trim(city);
                  
					if(typeof $.cookie('CityCookie') !== 'undefined'){
						$("#Current_location").html($.cookie('CityCookie') +" ("+$.cookie('PinCookie')+")");
						$("#delivery_location").html('<i class="fa fa-map-marker modal-icon"></i> Deliver in '+"<b>"+$.cookie('CityCookie')+", "+$.cookie('PinCookie') +"</b>");
					}
					else{
						$.ajax({
							url:"{{url('ajax/getpincode')}}",
							type: 'post',
							dataType: "json",
							data:"cityname="+city,
							success: function( data ) {
								console.log(data.success);
								if(data.success==false)
								{
									$(".pinerror").html("<i class='fa fa-exclamation' aria-hidden='true'></i> We can't detect your location");
									$("#pincode").val("");
								}
								else{
									$(".pinerror").html("");
									$("#delivery_location").html('<i class="fa fa-map-marker modal-icon"></i> Deliver in '+"<b>"+city+", "+data.value +"</b>");
									$(".js_pin_location_msg2").html("or enter a pincode");
									$("#Current_location").html(city +" ("+data.value+")");
									$('#pincodemodal').modal('hide');
									
									$.cookie("CityCookie", city);
									$.cookie("PinCookie", data.value);
								}
							}
						});
					}
					
                }
                else  {
                    console.log("address not found");
                }
            }
            else {
                console.log("Geocoder failed due to: " + status);
            }
        }
    );
}
$(function(){  
   if(typeof $.cookie('GeoRun') === 'undefined') {
       getLocation(); 
   }
})
</script>
	<script>
	/*
		function setCarousel() {
		$('.best-sell-slider-2').owlCarousel({
				autoplay :   false,
				loop: true,
				smartSpeed : 300,
				slideSpeed: 300,
                paginationSpeed: 500,
                slideBy: 4,
				nav :  true ,
				dots :  false ,
				margin:0,
				responsive:{
					0:{
						items:1,
				        nav:false,
				        slideBy: 4,
					},
					350:{
						items:2,
				        nav:false,
					},
					500:{
						items:3,
					},
					768:{
						items:4,
					},
					992:{
						items:5,
					},
                    1200:{
                        items:6,
                    },
                    1600:{
                        items:8,
                    }
				}
			});
			lazySizes.init();
		} */
			function setCarousel(ele_id) {
		    var swiper = new Swiper("#"+ele_id, {
      slidesPerView: 2,
      spaceBetween: 5,
      freeMode: true,
        observer: true,
        observeParents: true,
        preloadImages: true,
        observeSlideChildren:true,
        lazy: true,
       breakpoints: {
        349: {
          slidesPerView: 1,
        },
        350: {
          slidesPerView: 2,
        },
        500: {
          slidesPerView: 3,
        },
        768: {
          slidesPerView: 4,
        },
        992: {
          slidesPerView: 5,
        },
        1200: {
          slidesPerView: 6,
        },
        1600: {
          slidesPerView: 8,
        }
       },
        // Navigation arrows
        navigation: {
          nextEl: '.'+ele_id+'_swiper-button-next',
          prevEl: '.'+ele_id+'_swiper-button-prev',
        },
       on: {
        init: function () {
          $("#"+ele_id).show();
          
        },
    }
    });
    //lazySizes.init();
	}
		
		function setCarousel2() {
		$('.best-sell-slider-3').owlCarousel({
				autoplay :   false,
				loop: false,
				smartSpeed : 300,
				slideSpeed: 300,
                paginationSpeed: 500,
                singleItem: true,
				nav :  true ,
				dots :  false ,
				margin:0,
				responsive:{
					0:{
						items:1,
				        nav:false,
					},
					350:{
						items:2,
				        nav:false,
					},
					500:{
						items:3,
					},
                    1200:{
                        items:3,
                    }
				}
			});
			lazySizes.init();
		}
		</script>
		<script>
            $(function(){
                $("#mobilecouponbtn").click(function(){
                    var mobilecouponval= $("#mobilecoupon").val();
                    if(mobilecouponval==""){
                        $(".couponerr").text("Please Enter Coupon code.");
                        return false;
                    }
                    else{
                        $(".couponerr").text("");
                        $("#orignalcoupon").val(mobilecouponval);
                        $("#applycpnform").submit();
                    }
                });
                $("#changecouponbtn").click(function(){
                    $("#removecouponform").submit();
                });
            })
        </script>
@yield('script')

</body>
</html>
@if($debug == 1)
{{ d(Request::all()) }}
{{ d(Session::all()) }}
{{ d(Auth::User()) }}
@endif