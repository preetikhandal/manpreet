@extends('frontend.layouts.app')

@section('content')
 <style>
    .countdown .countdown-digit{
        background:#282563!important;
        color:#fff!important;
        font-weight:600;
    }
    .aldeproductslider .list-product{ margin: 0px 0px 5px 0px; }
</style>
<div @if($mobileapp == 1) style="margin-top:75px" @endif>
    
        <div>
           
            <section class="pb-4">
                <div class="container ">
                   
                    <div class="row sm-no-gutters gutters-5 aldeproductslider deepscustomrow">
                        @foreach ($flash_deal->flash_deal_products as $key => $flash_deal_product)
                            @php 
                                $product = \App\Product::find($flash_deal_product->product_id);
                                $home_base_price = home_base_price($product->id);
                			    $home_discounted_base_price = home_discounted_base_price($product->id);
                            @endphp
                            @if ($product->published != 0)
                                <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="{{ route('product', $product->slug) }}" class="thumbnail">
            								    @if($product->flash_deal_img != null)
            									<img class="mx-auto d-block lazyload" src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($product->flash_deal_img) }}" alt="{{ __($product->name) }}" />
            									@else
            								    <img class="mx-auto d-block lazyload" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
            									@endif
            								</a>
            							</div>
            							@if($home_base_price != $home_discounted_base_price)
            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;">{{discount_calulate($home_base_price, $home_discounted_base_price )}}% off</li>
            							</ul>
            							@endif
            							<div class="product-decs text-center">
            								<h2><a href="{{ route('product', $product->slug) }}" class="product-link">{!! Str::limit($product->name, 58, ' ...') !!}</a></h2>
            								<div class="pricing-meta">
            									<ul>
            									    @if($home_base_price != $home_discounted_base_price)
            									    	<li class="old-price">{{ $home_base_price }}</li>
            										@endif
            										    <li class="current-price">{{ $home_discounted_base_price }}</li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							    @if($product->current_stock == 0)
            							    <button class="btn btn95" type="button" disabled> Out of Stock</button>
            							    @else
            							    <button class="btn btn25" onclick="addToCart(this, {{$product->id}}, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, {{ $product->id }}, 1, 'full')" type="button"> Buy Now</button>
            							    @endif
            							</div>
            						</article>
        						</div>
    						 @endif
    					 @endforeach
    				</div>
                </div>
            </section>
        </div>
    </div>
    
        
</div>
@endsection
