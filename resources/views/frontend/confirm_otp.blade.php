@extends('frontend.layouts.app')

@section('content')
<style>
	.checkbox input[type="checkbox"]:checked + label::after{
		color:#273895;
		border:1px solid #273895;
	}
</style>
	<section class="bg-white pt-1 pb-3 py-md-4" id="userlogin_section" @if($mobileapp == 1) style="margin-top:90px" @endif>
		<div class="container">
			<div class="d-flex justify-content-center">
				<div class="user_card py-1">
					<div class="text-center px-3 my-2">
						<h1 class="heading heading-4 strong-500">
							{{__('Verify OTP.')}}
						</h1>
					</div>
					<!--div class="px-5">
						<div class="">
							@if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
								<div>
								     <a href="{{ route('social.login', ['provider' => 'apple']) }}" class="btn btn-styled btn-block btn-apple btn-icon--2 btn-icon-left px-4 mb-3">
											<span class="icon fa fa-apple" ></span> {{__('Sign in with Apple')}}
									</a>
									@if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
										<a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="btn btn-styled btn-block btn-facebook btn-icon--2 btn-icon-left px-4 mb-3">
											<i class="icon fa fa-facebook"></i> {{__('Login with Facebook')}}
										</a>
									@endif
									@if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)
										<a href="{{ route('social.login', ['provider' => 'google']) }}" class="btn btn-styled btn-block btn-google btn-icon--2 btn-icon-left px-4">
											<i class="icon fa fa-google"></i> {{__('Login with Google')}}
										</a>
									@endif
									@if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
										<a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="btn btn-styled btn-block btn-twitter btn-icon--2 btn-icon-left px-4">
											<i class="icon fa fa-twitter"></i> {{__('Login with Twitter')}}
										</a>
									@endif
								</div>
								<div class="or or--1 mt-0 text-center">
									<span>or</span>
								</div>
							@endif
						</div>
					</div-->
					<div class="d-flex px-5 justify-content-center form_container">
						<form role="form" action="{{ route('confirm.ajax_confirming.otp') }}" method="POST">
							@csrf

							<div class="input-group mb-3">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
								 <input type="tel" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" placeholder="{{ __('OTP') }}" name="otp" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6" required>
								 @if ($errors->has('otp'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('otp') }}</strong>
									</span>
								@endif
							</div>
					
							<div class="checkbox pad-btm text-left">
								<input class="magic-checkbox" type="checkbox" name="checkbox_example_1" id="checkboxExample_1a" required>
								<label for="checkboxExample_1a" class="text-sm">By signing up you agree to our <a href="{{route('terms')}}" class="text-blue" target="_blank">terms and conditions</a>.</label>
							</div>
							<div class="text-right">
								<button type="submit" class="btn btn-styled btn-base-1 w-100 btn-md">{{ __('Verify OTP') }}</button>
							</div>
						</form>
					</div>
					<div class="my-2">
						<div class="d-flex justify-content-center links">
							Already have an account?
							<a href="{{ route('user.login') }}" class="strong-600 ml-2 text-blue">{{__('Log In')}}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('script')
    <script type="text/javascript">

        var isPhoneShown = true;

        var input = document.querySelector("#phone-code");
        var iti = intlTelInput(input, {
            separateDialCode: true,
            preferredCountries: []
        });

        var countryCode = iti.getSelectedCountryData();


        input.addEventListener("countrychange", function() {
            var country = iti.getSelectedCountryData();
            $('input[name=country_code]').val(country.dialCode);
        });

        $(document).ready(function(){
            $('.email-form-group').hide();
        });

        function autoFillSeller(){
            $('#email').val('seller@example.com');
            $('#password').val('123456');
        }
        function autoFillCustomer(){
            $('#email').val('customer@example.com');
            $('#password').val('123456');
        }

        function toggleEmailPhone(el){
            if(isPhoneShown){
                $('.phone-form-group').hide();
                $('.email-form-group').show();
                isPhoneShown = false;
                $(el).html('Use Phone Instead');
            }
            else{
                $('.phone-form-group').show();
                $('.email-form-group').hide();
                isPhoneShown = true;
                $(el).html('Use Email Instead');
            }
        }
    </script>
@endsection
