@extends('frontend.layouts.app')

@section('content')
<link type="text/css" href="{{ asset('frontend/css/mmenu.min.css') }}" rel="stylesheet" media="all">
<style>
nav {
    display:none;
}
.header {
    display:none;
}
.mm-menu {
	height:  100% !important;
	--mm-color-background: #232F3E;
	--mm-listitem-size: 50px;
}
.mm-navbars_top {
	--mm-color-background: #131921;
	border-bottom: none !important;
}
.mm-navbar {
	color: #fff !important;
	font-size: 14px;
	font-weight: bold;
}
.mm-navbar input {
	text-align: center;
}
.mm-navbar:first-child {
	align-content: center;
	justify-content: center;
	align-items: center;
}
.mm-navbar img {
	flex-grow: 0;
	opacity: 0.6;
	border: 1px solid #fff;
	border-radius: 50px;
	padding: 10px;
	margin: 0 10px;
}
.mm-navbar .fa {
	flex-grow: 0;
	border: 1px solid rgba(255, 255, 255, 0.6);
	border-radius: 30px;
	color: rgba(255, 255, 255, 0.8) !important;
	font-size: 16px !important;
	line-height: 50px;
	width: 50px;
	height: 50px;
	padding: 0;
}
.mm-navbar .fa:hover {
	border-color: #fff;
	color: #fff !important;
}

.mm-panels > .mm-panel:after {
	content: none;
	display: none;
}
.mm-panels > .mm-panel > .mm-listview {
	margin-top: 10px !important;
}

.mm-listview {
	font-size: 16px;
}
.mm-listitem:last-child:after {
	content: none;
	display: none;
}
.mm-listitem a,
.mm-listitem span {
	color: rgba(255, 255, 255, 0.7);
	text-align: center;
	padding-right: 20px !important;
}
.mm-listitem a:hover,
.mm-listitem a:hover + span {
	color: #fff;
}
.mm-navbar .btn-primary{
    background:#131921!important;
    border-color:#131921!important;
}
</style>

<nav id="menu" @if($mobileapp == 1) style="padding-top:15px" @endif>
    <ul id="panel-menu">
        @foreach ($subcategories as $key2 => $subcategory)
            @if($subcategory->display == 1)
            <li>
                @if($subcategory->icon == null)
                    <img loading="lazy" class="cat-image ml-2" src="{{ asset('frontend/images/chatLogo.png') }}" style="width:50px;height:50px">
                @else
                    <img loading="lazy" class="cat-image ml-2" src="{{ asset($subcategory->icon) }}" style="width:50px;height:50px">
                @endif
                
                @php 
                    $category_slug = \App\SubCategory::find($subcategory->id)->category->slug;
                    $subcategory_slug = \App\SubCategory::find($subcategory->id)->slug;
                @endphp
                
                @if(count($subcategory->subsubcategories->where('display',1)) > 0)
                <span>{{ __($subcategory->name) }}</span>
                <ul>
                @foreach (\App\SubCategory::find($subcategory->id)->subsubcategories as $key3 => $subsubcategory)
                    @if($subsubcategory->display == 1)
                    <li>
                        @if($subsubcategory->icon == null)
                         <img class="cat-image ml-2" src="{{ asset('frontend/images/chatLogo.png') }}" style="width:50px;height:50px">
                        @else
                         <img loading="lazy"  class="cat-image ml-2" src="{{ asset($subsubcategory->icon) }}" style="width:50px;height:50px">
                        @endif
                        <a href="{{ route('products.subsubcategory', [$category_slug, $subcategory_slug, $subsubcategory->slug]) }}">
                         {{ __($subsubcategory->name) }}</a>
                    </li>
                    @endif
                @endforeach
                </ul>
                @else
                <a href="{{ route('products.subcategory', [$category_slug, $subcategory_slug]) }}">
                                     {{ __($subcategory->name) }}</a>
                @endif
            </li>
            @endif
        @endforeach
    </ul>
</nav>		

@endsection
@section('script')
<script src="{{ asset('frontend/js/mhead.min.js') }}"></script>
<script src="{{ asset('frontend/js/mmenu.min.js') }}"></script>
<script>
	$(function() {
				
			const menu = new Mmenu("#menu", {
		extensions 	: [ "position-bottom", "fullscreen", "theme-black",  "border-full" ],
		navbars		: [{
			height 	: 2,
			content : [
				'Categories'
			]
		}, {
			content: [ "searchfield" ]
		}, {
			content : ["prev","title"]
		}, {
             "position": "bottom",
             "content": [
                "<button class='btn btn-primary' id='closebtn'>Close</button>"
             ]
          }
                          ]}, {});


    // Get the API
    const api = menu.API;

    // Invoke a method
    const panel = document.querySelector( "#menu" );
    api.open();
    $('#closebtn').on('click', function() {
       window.history.go(-1);
    });

});
</script>
@endsection
