@extends('frontend.layouts.app')

@section('content')

    <div id="page-content">
        <section class="processing slice-xs sct-color-2 border-bottom" @if($mobileapp == 1) style="margin-top:80px" @endif>
            <div class="container-fluid">
                <div class="row cols-delimited justify-content-center">
                    <div class="process">
                        <div class="process-row">
                            <div class="process-step">
                                <a href="{{ route('cart') }}" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-2x text-white"></i></a>
                                <p>My Cart</p>
                            </div>
                            <div class="process-step">
                                <a href="javascript:;" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-map-o fa-2x text-white"></i></a>
                                <p>Shipping Info</p>
                            </div>
                            <div class="process-step">
                                <a href="javascript:;" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-credit-card fa-2x"></i></a>
                                <p>Payment</p>
                            </div> 
                             <div class="process-step">
                                <a href="javascript:;" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-check-circle-o fa-2x"></i></a>
                                <p>Confirmation</p>
                            </div> 
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>

        <section class="py-5 gry-bg">
            <div class="container-fluid">
                <div class="row cols-xs-space cols-sm-space cols-md-space">
                    <div class="col-lg-8">
                        <form class="form-default" data-toggle="validator" action="{{ route('checkout.store_shipping_infostore') }}" method="POST" onsubmit="return validateUser()">
                            @csrf
                            <div class="card">
                                @if(Auth::check())
                                    @php
                                        $user = Auth::user();
                                       
                                    @endphp
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{__('Full Name')}}</label>
                                                    <input type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}" id="name" required maxlength="190">
                                                    @error('name')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
										<div class="row">	
											<div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{__('Phone')}}</label>
                                                    <input type="text" class="form-control" value="{{ old('phone', $user->phone) }}" name="phone" id="phone" required digits maxlength="10" minlength="10" onkeypress="return isNumberKey(event);">
                                                    @error('phone')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
											<div class="col-md-6">
                                                <div class="form-group">
                                                <label class="control-label">{{__('Email')}}(Optional)</label>
                                                    <input type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}" id="email" >
                                                    @error('email')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
										</div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{__('Address')}}</label>
                                                    <input type="text" class="form-control" name="address" value="{{ old('address', $user->address) }}" id="address" required maxlength="250">
                                                    @error('address')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <!--<div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{__('Select your country')}}</label>
                                                    <select class="form-control selectpicker" data-live-search="true" name="country">
                                                        @foreach (\App\Country::all() as $key => $country)
                                                            <option value="{{ $country->name }}" @if ($country->code == $user->country) selected @endif>{{ $country->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>-->
                                             <input type="hidden" class="form-control" name="country" value="India">
                                            <div class="col-md-4">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{__('City')}}</label>
                                                    <input type="text" class="form-control" value="{{ old('city', $user->city) }}" name="city" id="city" required maxlength="30">
                                                    @error('city')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{__('State')}}</label>
                                                    <input type="text" class="form-control" value="{{ old('state', $user->state) }}" name="state" id="state" required maxlength="30">
                                                    @error('state')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
											<div class="col-md-4">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{__('Postal code')}}</label>
                                                    <input type="number" min="0" class="form-control" value="{{  old('postal_code', $user->postal_code) }}" name="postal_code" id="postal_code" required digits minlength="6" maxlength="6"  onkeypress="return isNumberKey(event);">
                                                    @error('postal_code')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="checkout_type" value="logged">
                                        <input type="hidden" name="country" value="India">
                                    </div>
                                @else
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{__('Full Name')}}</label>
                                                    <input type="text" class="form-control" name="name" id="name" placeholder="{{__('Name')}}" value="{{old('name')}}" required maxlength="190">
                                                    @error('name')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
										<div class="row">		
											<div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{__('Phone')}}</label>
                                                    <input type="number" min="0" class="form-control" placeholder="{{__('Phone')}}" name="phone" id="phone" value="{{old('phone')}}" required digits minlength="10" maxlength="10"  onkeypress="return isNumberKey(event);">
                                                    @error('phone')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{__('Email')}}</label>
                                                    <input type="text" class="form-control" name="email" id="email" placeholder="{{__('Email')}}" value="{{old('email')}}" required maxlength="190">
                                                    @error('email')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{__('Address')}}</label>
                                                    <input type="text" class="form-control" name="address" id="address" placeholder="{{__('Address')}}" value="{{old('address')}}" required maxlength="250">
                                                    @error('address')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <!--<div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{__('Select your country')}}</label>
                                                    <select class="form-control custome-control" data-live-search="true" name="country">
                                                        @foreach (\App\Country::all() as $key => $country)
                                                            <option value="{{ $country->name }}">{{ $country->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>-->
                                            <div class="col-md-4">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{__('City')}}</label>
                                                    <input type="text" class="form-control" placeholder="{{__('City')}}" name="city" id="city" value="{{old('city')}}" required maxlength="30">
                                                    @error('city')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{__('State')}}</label>
                                                    <input type="text" class="form-control" placeholder="{{__('State')}}" name="state" id="state" value="{{old('state')}}" required maxlength="30">
                                                    @error('state')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
											<div class="col-md-4">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{__('Postal code')}}</label>
                                                    <input type="number" min="0" class="form-control" placeholder="{{__('Postal code')}}" name="postal_code" id="postal_code" value="{{old('postal_code')}}"  required digits minlength="6" maxlength="6"  onkeypress="return isNumberKey(event);">
                                                    @error('postal_code')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="checkout_type" value="guest">
										<input type="hidden" name="country" value="India">
                                    </div>
                                @endif
                            </div>
                            
                            <div class="row align-items-center pt-4 d-none d-lg-block d-lg-flex">
                                <div class="col-md-6">
                                    <a href="{{ route('home') }}" class="btn btn-styled btn-base-1" style="background:#131921">
                                        <i class="fa fa-reply" aria-hidden="true"></i>
                                        {{__('Return to shop')}}
                                    </a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button type="submit" id="btndeliveryinfo"  class="btn btn-styled btn-base-1 bg-blue">{{__('Continue to Payment')}}</a>
                                </div>
                            </div>
                            {{-- <div class="row align-items-center pt-4">
                                <div class="col-6">
                                    <a href="{{ route('home') }}" class="btn btn-styled btn-base-1" style="background:#131921" >
                                        <i class="fa fa-reply" aria-hidden="true"></i>
                                        {{__('Return to shop')}}
                                    </a>
                                </div>
                                <div class="col-6 text-right">
                                    <button type="submit" class="btn btn-styled btn-base-1 bg-blue">{{__('Continue to Payment')}}</a>
                                </div>
                            </div> --}}
                        </form>
                    </div>

                    <div class="col-lg-4 ml-lg-auto">
                        @include('frontend.partials.cart_summary')
                    </div>
                    
                            
                </div>
                
                    <div class="row align-items-center pt-4 d-block d-lg-none">
                        <div class="col-md-12 text-center">
                            <button onclick="$('#btndeliveryinfo').click();"  class="btn-block btn btn-styled btn-base-1 bg-blue">{{__('Continue to Payment')}}</a>
                        </div>
                        <div class="col-md-12 text-center pt-2">
                            <a href="{{ route('home') }}">
                                <i class="fa fa-reply" aria-hidden="true"></i>
                                {{__('Return to shop')}}
                            </a>
                        </div>
                    </div>
            </div>
        </section>
    </div>

<!-- The Modal -->
<div class="modal fade text-center py-5" id="otpmodal">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header p-2" style="border-bottom: 1px solid transparent;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
			<div class="top-strip"></div>
			<h3 class="mb-0 font23">Quick Verify</h3>
			<div class="popularRegionsBgBrd"><span id="otpmsg"></span></div>
			<form class="mt-3" id="otpverifyform" method="post" onsubmit="return varifyOTPUser();">
				<div class="input-group w-75 mx-auto">
				  <input type="text" class="form-control" name="otp" id="otp" placeholder="Enter OTP" aria-label="Enter OTP" aria-describedby="button-addon2" autocomplete="off" maxlength=6 pattern="[0-9]{6}" onkeypress="return isNumberKey(event);" required>
				  <div class="input-group-append">
					<button class="btn btn-primary" id="otpBtn">Verify</button>
				  </div>
				</div>
				<p class="text-danger pinerror text-center w-75 font-weight-bold"></p>
			</form>
			<p class="pb-1"><span class="text-danger" id="OTPErr"></span></p>
      </div>
    </div>
  </div>
</div>	
	
<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

var verify_form = false;
function validateUser() {
    name = $('#name').val();
    email = $('#email').val();
    phone = $('#phone').val();
    postal_code = $('#postal_code').val();
    city = $('#city').val();
    address = $('#address').val();
    state = $('#state').val();
    if(verify_form == true) {
        return true;
    }
    $.post('{{route('cart_user_verify')}}', { _token:'{{ csrf_token() }}', name:name, state:state, email:email, phone:phone, city:city, address:address, postal_code:postal_code}, function(data){
            if(data.result == true) {
                if(data.redirect == true) {
                    verify_form = true;
                    $('#btndeliveryinfo').click();
                } else if(data.otp_type == 'email') {
                    msg = 'OTP has been generated successfully and send to your email address';
                    showFrontendAlert('success', msg);
                    $('#otpmsg').text("We sent OTP on your email "+email);
                    $('#otpmodal').modal('show');
                }
                else if(data.otp_type == 'phone') {
                    msg = 'OTP has been generated successfully and send to your mobile number';
                    if(data.new_user == true) {
                        msg += ' You will be registered as new user and agreed to our T & C.';
                    }
                    showFrontendAlert('success', msg);
                    $('#otpmsg').text("We sent OTP on your phone "+phone);
                    $('#otpmodal').modal('show');
                }
            } else {
                showFrontendAlert('warning', data.message);
            }
        }).fail(function(xhr, status, error) {
            if(xhr.status == 422) {
                var m = '';
                data = JSON.parse(xhr.responseText);
                $(data.errors).each(function(i,v) {
                    $.each(v, function(x,y) {
                        m += y[0]+"<br />"; 
                    });
                });
                showFrontendAlert('warning', m);
            }
        });
    return false;
}

function varifyOTPUser() {
    name = $('#name').val();
    email = $('#email').val();
    phone = $('#phone').val();
    otp = $('#otp').val();
    postal_code = $('#postal_code').val();
    city = $('#city').val();
    address = $('#address').val();
    $.post('{{route('cart_user_verify.post')}}', { _token:'{{ csrf_token() }}', otp:otp, name:name, email:email,phone:phone, city:city, address:address, postal_code:postal_code }, function(data){
            if(data.result == true) {
                //$('#formShippingInfo').submit();
                $('#btndeliveryinfo').click();
            } else {
               $('#OTPErr').text(data.message);
            }
        }).fail(function(xhr, status, error) {
            if(xhr.status == 422) {
                var m = '';
                data = JSON.parse(xhr.responseText);
                $(data.errors).each(function(i,v) {
                    $.each(v, function(x,y) {
                        m += y[0]+"<br />"; 
                    });
                });
                showFrontendAlert('warning', m);
            }
        });
    return false;
}
</script>

@endsection
