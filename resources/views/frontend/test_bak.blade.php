@extends('frontend.layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.19/mmenu.min.css" integrity="sha512-gIn3+aW4xrkqoIvXsJ7F3woKfU+KTvEzXm0IXKmWqXZ8oyD9TfzFEdGDMF0egqto86E91yZVPzoa82a/dgSMig==" crossorigin="anonymous" />
<style>
nav {
    display:none;
}
.header {
    display:none;
}
            .mm-menu {
				height:  100% !important;
				--mm-color-background: #3ba5df;
				--mm-listitem-size: 50px;
			}
			.mm-navbars_top {
				--mm-color-background: #4bb5ef;
				border-bottom: none !important;
			}
			.mm-navbar {
				color: #fff !important;
				font-size: 14px;
				font-weight: bold;
			}
			.mm-navbar input {
				text-align: center;
			}
			.mm-navbar:first-child {
				align-content: center;
				justify-content: center;
				align-items: center;
			}
			.mm-navbar img {
				flex-grow: 0;
				opacity: 0.6;
				border: 1px solid #fff;
				border-radius: 50px;
				width: 100px;
				height: 100px;
				padding: 10px;
				margin: 0 10px;
			}
			.mm-navbar .fa {
				flex-grow: 0;
				border: 1px solid rgba(255, 255, 255, 0.6);
				border-radius: 30px;
				color: rgba(255, 255, 255, 0.8) !important;
				font-size: 16px !important;
				line-height: 50px;
				width: 50px;
				height: 50px;
				padding: 0;
			}
			.mm-navbar .fa:hover {
				border-color: #fff;
				color: #fff !important;
			}

			.mm-panels > .mm-panel:after {
				content: none;
				display: none;
			}
			.mm-panels > .mm-panel > .mm-listview {
				margin-top: 10px !important;
			}

			.mm-listview {
				font-size: 16px;
			}
			.mm-listitem:last-child:after {
				content: none;
				display: none;
			}
			.mm-listitem a,
			.mm-listitem span {
				color: rgba(255, 255, 255, 0.7);
				text-align: center;
				padding-right: 20px !important;
			}
			.mm-listitem a:hover,
			.mm-listitem a:hover + span {
				color: #fff;
			}
</style>
<nav id="menu">
    <ul id="panel-menu">
    	<li><span>Category 1</span>
    		<ul>
    			<li><a>Sub Categories 11</a></li>
    			<li><a>Sub Categories 12</a></li>
    			<li><a>Sub Categories 13</a></li>
    			<li><a>Sub Categories 14</a></li>
    		</ul>
    	</li>
    	<li><span>Category 2</span>
    		<ul>
    			<li><a>Sub Categories 21</a></li>
    			<li><a>Sub Categories 22</a></li>
    			<li><a>Sub Categories 23</a></li>
    			<li><a>Sub Categories 24</a></li>
    		</ul>
    	</li>
    	<li><span>Category 3</span>
    		<ul>
    			<li><a>Sub Categories 31</a></li>
    			<li><a>Sub Categories 32</a></li>
    			<li><a>Sub Categories 33</a></li>
    			<li><a>Sub Categories 34</a></li>
    		</ul>
    	</li>
    	<li><span>Category 4</span>
    		<ul>
    			<li><a>Sub Categories 41</a></li>
    			<li><a>Sub Categories 42</a></li>
    			<li><a>Sub Categories 43</a></li>
    			<li><a>Sub Categories 44</a></li>
    		</ul>
    	</li>
    </ul>
</nav>		

@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/jquery.mhead@1.0.1/dist/jquery.mhead.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.19/mmenu.js" integrity="sha512-wvqkumeSxABjtGcAy7Ubx0+qsUEadompYD8/Z3nHfI4xzEqnbGTMw4JCg3dcXwyvGXuFEreG4We+1D/1HkY7Uw==" crossorigin="anonymous"></script>
<script>
	$(function() {
				
			const menu = new Mmenu("#menu", {
		extensions 	: [ "position-bottom", "fullscreen", "theme-black",  "border-full" ],
		navbars		: [{
			height 	: 2,
			content : [
				'Categories'
			]
		}, {
			content: [ "searchfield" ]
		}, {
			content : ["prev","title"]
		}, {
             "position": "bottom",
             "content": [
                "<button class='btn btn-primary' id='closebtn'>Close</button>"
             ]
          }
                          ]}, {});
		

            // Get the API
            const api = menu.API;

            // Invoke a method
            const panel = document.querySelector( "#menu" );
            api.open();
            $('#closebtn').on('click', function() {
               window.history.go(-1);
            });

});
</script>
@endsection
