@extends('frontend.layouts.app')
@section('meta_keywords') {{$Blog->SEO_metatags}} @endsection
@section('meta_description') {{$Blog->SEO_metaDescription}} @endsection
@section('content')
<style>
.site-section { padding: 2em 0; }
@media (min-width: 768px) {
    .site-section { padding: 2em 0; } 
}
.site-section.site-section-sm { padding: 4em 0; }
.single-content { font-size: .9rem; }
.single-content h1 {
  font-size: 2rem;
  color: #000; 
}	
.post-meta {font-size: .8rem; }
.post-meta a { color: #000; }
.trend-entry { margin-bottom: 30px; }
.trend-entry .number {
	-webkit-box-flex: 0;
	-ms-flex: 0 0 50px;
	flex: 0 0 50px;
	font-size: 30px;
	line-height: 1;
	color: #ccc; 
}
.trend-entry .trend-contents h2 { font-size: 18px; }
.trend-entry .trend-contents h2 a { color: #000; }
.date-read { color: #b4b4b4; }
.bg-white{border-radius:10px}
img{ width:100%!important;}
</style>
<div class="site-section pb-4" @if($mobileapp == 1) style="margin-top:75px" @endif>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-9 single-content bg-white pt-3">
				<p class="mb-2">
					<img src="{{asset($Blog->FeaturedImage)}}" alt="{{$Blog->BlogTitle}}" class="img-fluid img-thumbnail">
				</p>  
				<div class="post-meta d-flex mb-3">
					<div class="vcard">
						<span class="date-read">{{Carbon\Carbon::parse($Blog->created_at)->format('M d Y')}} </span>
					</div>
				</div>
				<h1 class="mb-2">{{$Blog->BlogTitle}}</h1>
				<p>{!! $Blog->BlogContent !!}</p>
				<div class="pt-5 float-right">
					<p>Share:  <a href="http://www.facebook.com/share.php?u={{url('blog/'.$Blog->URLSlug)}}" title="Share this page on facebook" target="_blank" class="pop share-square share-square-facebook" style="display: inline-block;"></a>
					<a href="https://twitter.com/share?url={{url('blogs/'.$Blog->URLSlug)}}&text={{$Blog->BlogTitle}}" title="Share this page on Twitter" target="_blank" class="pop share-square share-square-twitter" style="display: inline-block;"></a>
					<a href="https://www.linkedin.com/shareArticle?url={{url('blogs/'.$Blog->URLSlug)}}&title={{$Blog->BlogTitle}}" title="Share this page on Linkedin" target="_blank" class="pop share-square share-square-linkedin" style="display: inline-block;"></a>
					<a href="https://pinterest.com/pin/create/bookmarklet/?media={{asset('images/upload/blog/'.$Blog->FeaturedImage)}}&url={{url('blogs/'.$Blog->URLSlug)}}&description={{$Blog->BlogTitle}}" title="Share this page on Pinterest" target="_blank" class="pop share-square share-square-pinterest" style="display: inline-block;"></a>
					</p>
				</div>
			</div>
			<div class="col-lg-3 ml-auto">
				<div class="card" style="border-radius:10px;">
					<h5 class="card-header p-2" >Popular Blogs</h5>
					<div class="card-body widget-post">
						@php $sn=1; @endphp
						@foreach($PopularBlog as $P)
							<div class="trend-entry d-flex">
								<div class="number align-self-start">{{$sn++}}</div>
								<div class="trend-contents">
									<h2><a href="{{url('blog/'.$P->URLSlug)}}">{{$P->BlogTitle}}</a></h2>
									<div class="post-meta">
										<span class="date-read">{{Carbon\Carbon::parse($Blog->created_at)->format('M d Y')}}</span>
									</div>
								</div>
							</div>
						@endforeach
					</div>
					<p class="text-right">
    				  <a href="{{route('blog')}}" class="btn btn-primary">View All Blogs <span class="icon-keyboard_arrow_right"></span></a>
    				</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
