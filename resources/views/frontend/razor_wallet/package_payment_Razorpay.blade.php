@php
    $customer_package = CustomerPackage::findOrFail(Session::get('payment_data')['customer_package_id']);
@endphp

@php */
@extends('frontend.layouts.app')

@section('content')
@php
    $customer_package = CustomerPackage::findOrFail(Session::get('payment_data')['customer_package_id']);
@endphp
    <form action="{!!route('payment.rozer')!!}" method="POST" id='rozer-pay' style="display: none;">
        <!-- Note that the amount is in paise = 50 INR -->
        <!--amount need to be in paisa-->
        <script src="https://checkout.razorpay.com/v1/checkout.js"
                data-key="{{ env('RAZOR_KEY') }}"
                data-amount={{$customer_package->amount}}
                data-buttontext=""
                data-name="{{ env('APP_NAME') }}"
                data-description="Classified Package Payment"
                data-image="{{ asset(\App\GeneralSetting::first()->logo) }}"
                data-prefill.name= {{ Auth::user()->name}}
                data-prefill.email= {{ Auth::user()->email}}
                data-theme.color="#ff7529">
        </script>
        <input type="hidden" name="_token" value="{!!csrf_token()!!}">
    </form>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#rozer-pay').submit()
        });
    </script>
@endsection
*/
@endphp
 
<script src="{{ asset('frontend/js/vendor/jquery.min.js') }}"></script>   
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<center id="cancel" style="display:none;"><br /><br />
Payment cancelled, you will be redirect to back soon.
</center>
<script>
var options = {
    "key": "{{ env('RAZOR_KEY') }}", // Enter the Key ID generated from the Dashboard
    "amount": "{{$customer_package->amount}}", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "name": "{{ env('APP_NAME') }}",
    "description": "Classified Package Payment",
    "image": "{{ asset(\App\GeneralSetting::first()->logo) }}",
    "callback_url": "{!!route('payment.rozer')!!}",
    "prefill": {
        "name": "{{ Auth::user()->name}}",
        "email": "{{ Auth::user()->email}}"
    },
    "theme": {
        "color": "#ff7529"
    },
    "modal": {
        "ondismiss": function(){
            $('#cancel').show();
            setTimeout(function() { 
                window.location = '{{url()->previous()}}'; }, 2000);
        }
    }
};

var rzp1 = new Razorpay(options);
        $(document).ready(function(){
           rzp1.open();
});
    
</script>
