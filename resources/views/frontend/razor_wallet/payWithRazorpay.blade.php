@php
$previous = url()->previous();
if(stristr($previous, 'checkout/delivery_info')) {
$previous = route('checkout.store_delivery_info_with_shipping');
}
/*
    <form action="{!!route('payment.rozer')!!}" method="POST" id='rozer-pay' style="display: none;">
        <!-- Note that the amount is in paise = 50 INR -->
        <!--amount need to be in paisa-->
        <script src="https://checkout.razorpay.com/v1/checkout.js"
                data-key="{{ env('RAZOR_KEY') }}"
                data-amount={{Session::get('payment_data')['amount']*100}}
                data-buttontext=""
                data-name="{{ env('APP_NAME') }}"
                data-description="Wallet Payment"
                data-image="{{ asset(\App\GeneralSetting::first()->logo) }}"
                data-prefill.name="{{ Auth::user()->name}}"
                data-prefill.email="{{ Auth::user()->email}}"
                data-theme.color="#ff7529"
                data-modal ='{"ondismiss": function(){console.log(‘Checkout form closed’);}}'
                >
        </script>
        <input type="hidden" name="_token" value="{!!csrf_token()!!}">
    </form>
    
<script src="{{ asset('frontend/js/vendor/jquery.min.js') }}"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('#rozer-pay').submit()
        });
    </script>
    */
    
    @endphp
 
<script src="{{ asset('frontend/js/vendor/jquery.min.js') }}"></script>   
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<center id="cancel" style="display:none;"><br /><br />
Payment cancelled, you will be redirect to back soon.
</center>
<script>
var options = {
    "key": "{{ env('RAZOR_KEY') }}", // Enter the Key ID generated from the Dashboard
    "amount": "{{Session::get('payment_data')['amount']*100}}", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "name": "{{ env('APP_NAME') }}",
    "description": "Wallet Payment",
    "image": "{{ asset(\App\GeneralSetting::first()->logo) }}",
    "callback_url": "{!!route('payment.rozer')!!}",
    "prefill": {
        "name": "{{ Auth::user()->name}}",
        "email": "{{ Auth::user()->email}}"
    },
    "theme": {
        "color": "#ff7529"
    },
    "modal": {
        "ondismiss": function(){
            $('#cancel').show();
            setTimeout(function() { 
                window.location = '{{$previous}}'; }, 2000);
        }
    }
};

var rzp1 = new Razorpay(options);
        $(document).ready(function(){
           rzp1.open();
});
    
</script>