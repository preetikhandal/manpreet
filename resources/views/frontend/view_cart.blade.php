@extends('frontend.layouts.app')

@section('content')
<style>
	.product_row{
		border-bottom:1px solid #f1f1f1;
		padding: 1.25rem 0;
	}
	.product-name { width: 70% !important; }
	.product-name a,.product-price{
		font-size: 1.1em;
		font-weight: bold;
		line-height: 1.5;
		letter-spacing: -0.5px;
		text-transform: none;
		color: #2b2b2c;
		vertical-align: middle;
		border: none;
	}
	.product-name a:hover{
		color:#b22222; text-decoration:underline;
	}
	.input-group--style-2 .input-group-btn > .btn {
		border-radius: 10px;
		background: transparent;
		border-color: #e6e6e6;
		color: #818a91;
		font-size: 0.625rem;
		padding-top: 0.6875rem;
		padding-bottom: 0.6875rem;
		cursor: pointer;
	}
	.product-remove a {
		font-size: 1rem;
		color: #818a91;
	}
	.product-remove a:hover { color: #b22222; }
	.number-of-items{
		font-family: Arial,sans-serif;
		font-size:18px; line-height: 24px!important;
	}
	.shipping{color: #555!important;padding-top:20px;}
	.font15{ font-size:15px;}
	.cart-area [class*="col-"]{
	    padding-left:5px;
	    padding-right:5px;
	}
	.shipmsg{
	    color:red;
	    font-size:14px;
	}
</style>
	<section class="processing slice-xs sct-color-2 border-bottom pb-0" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid ">
            <div class="row cols-delimited justify-content-center">
                <div class="process">
                    <div class="process-row">
                        <div class="process-step">
                            <a href="javascript:;" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-2x text-white"></i></a>
                            <p>My Cart</p>
                        </div>
                        <div class="process-step">
                            <a href="javascript:;" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-map-o fa-2x"></i></a>
                            <p>Shipping Info</p>
                        </div>
                        <div class="process-step">
                            <a href="javascript:;" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-credit-card fa-2x"></i></a>
                            <p>Payment</p>
                        </div> 
                         <div class="process-step">
                            <a href="javascript:;" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-check-circle-o fa-2x"></i></a>
                            <p>Confirmation</p>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section>
	
	<section class="py-2 gry-bg mb-5" id="cart-summary">
	   <div class="container-fluid cart-area cart-page-one">
	      <div class="row outofstock" style="display:none;">
	          <div class="col-xl-12">
	              <div class="alert alert-danger">
	                  Some of items in your cart is not in stock, Remove that item(s) to proceed further.
	              </div>
	          </div>
	     </div>
	      <div class="row lowstock"  style="display:none;">
	          <div class="col-xl-12">
	              <div class="alert alert-danger">
	                  Required stock not available, Descrese the quantity to proceed further.
	              </div>
	          </div>
	     </div>
		@if(Session::has('cart'))
		    @if(count(Session::get('cart')) > 0)
			<div class="row">
				<div class="col-xl-8">
					<div class="form-default bg-white px-3">
						<div class="row bg-blue py-2">
        					<div class="col-6 col-md-5 px-0 text-center font15">Product</div>
        					<div class="col-6 col-md-2 px-0 text-center font15">Price</div>
        					<div class="col-6 col-md-2 px-0 text-center d-none d-md-block font15">Qty</div>
        					<div class="col-6 col-md-3 px-0 text-center d-none d-md-block font15">Total</div>
        				</div>
						@php
						   $cartCashBack = Session::get('cart_cashback_Product_subtotal');
						  
						 $stopProcessing = false;
						 $lowStock = false;
						 $outofstock = false;
						   $applyCoupe = Session::get('applycoupen'); 
						   $subtotal = 0; $discount = 0; $coupon_category_discount = 0; $total = 0; $tax = 0; $shipping = 0;$getPercent=0;$getPercent=0;
						   $cart = Session::get('cart');
						@endphp
						
						  
						@foreach (Session::get('cart') as $key => $cartItem)
							@php
							    if(!isset($cartItem['coupon_category_discount'])) {
							        $cartItem['coupon_category_discount'] = 0;
							    }
								$product = \App\Product::find($cartItem['id']);
							    $categoryId = $product->category_id;
                        	    $subcategoryId = $product->subcategory_id;
                                $subsubcategory = $product->subsubcategory_id;
                                
            		           if(isset($product->user_id)){
                            	    $sellerId = $product->user_id;
                            	}else{
                            	    $sellerId = 0;
                            	}
                                if(!empty($sellerId)){
                                        if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                                        $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
                                    }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                                            $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
                                    }else if(!empty($categoryId) && !empty($sellerId)){
                                         $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
                                    }    
                                }


        
                                
                                if(isset($get_subcategory->commission) && $get_subcategory->commission_type==1){
                                    $exculsivePercent = $get_subcategory->commission;
                                }else{
                                    $exculsivePercent = 0; 
                                }
                               
                                if(!empty($exculsivePercent)){
                                      $getPercent = ($product->unit_price*$exculsivePercent)/100;
                                      $cartIteamPrice = $cartItem['price'];
                                }else{
                                   $cartIteamPrice = $cartItem['price'];
                                }
                               
                              
								
								$total = $total + $cartIteamPrice*$cartItem['quantity'];
								$subtotal += $cartIteamPrice*$cartItem['quantity'];
								$shipping += $cartItem['shipping']*$cartItem['quantity'];
								$discount += $cartItem['discount'] * $cartItem['quantity'];
								$product_discount = ($cartItem['coupon_category_discount'] + $cartItem['discount']) * $cartItem['quantity'];
								$coupon_category_discount += $cartItem['coupon_category_discount'] * $cartItem['quantity'];
								$product_name_with_choice = $product->name;
								if ($cartItem['variant'] != null) {
									$product_name_with_choice = $product->name.' - '.$cartItem['variant'];
								}
                                $min_purchased_quantity = $product->min_purchased_quantity;
                                if($min_purchased_quantity <= 1) {
                                    $min_purchased_quantity = 1;
                                }
                                $qty = 0;
                                if($product->variant_product){
                                    foreach ($product->stocks as $key => $stock) {
                                        $qty += $stock->qty;
                                    }
                                }
                                else{
                                    if($product->current_stock > -1) {
                                    $qty = $product->current_stock;
                                    } else {
                                    $qty = 100;
                                    }
                                }
							@endphp
							
							
							<div class="row product_row">
        						<div class="col-3 col-md-2 product-image px-2">
        							@if($product->thumbnail_img != null)
        								<a href="{{ route('product', $product->slug) }}" target="_blank">
        									<img loading="lazy" alt="{{$product->name}}" src="{{ asset($product->thumbnail_img) }}" class="img-fluid">
        								</a>
        							@else
        								<a href="{{ route('product', $product->slug) }}" target="_blank"><img loading="lazy" alt="{{$product->name}}" src="{{asset('frontend/images/product-thumb.jpg')}}" class="img-fluid"/></a>
        							@endif
        						</div>
        						<div class="col-6 col-md-3 px-0">
        							<div class="product-name">
        								<span class="pr-2 d-block"><a href="{{ route('product', $product->slug) }}" target="_blank">{{ $product_name_with_choice }}</a></span>
        								<span class="shipping">
        									@if($cartItem['shipping']==0)
        									<span class="shippingdiv"></span>
        									@else
        										Shipping {{ single_price($shipping) }}
        									@endif
        								</span>
        							</div>
        						</div>
        						<div class="col-3 col-md-2 px-0 text-left text-md-center">
        							<div class="product-price"> 
        							@if($cartItem['discount'] != 0)
    							    <del style="color:red">{{ single_price($cartIteamPrice + $cartItem['tax'] + $cartItem['discount']+$getPercent) }}</del>  <br />
    							    @endif
        								{{ single_price($cartIteamPrice) }}
        							</div>
        						</div>
        						<div class="col-7 col-md-2 text-center">
        							<div class="product-qty float-right">
        								@if($cartItem['digital'] != 1)
        									<div class="input-group input-group--style-2" style="width: 120px;">
        										<span class="input-group-btn">
        											<button class="btn btn-number" type="button" data-type="minus" data-field="quantity[{{ $key }}]">
        												<i class="la la-minus"></i>
        											</button>
        										</span>
        										<input readonly type="text" name="quantity[{{ $key }}]" class="form-control input-number" placeholder="1" value="{{ $cartItem['quantity'] }}"  min="{{$min_purchased_quantity}}"  max="{{$qty}}" onchange="updateQuantity({{ $key }}, this)">
        										<span class="input-group-btn">
        											<button class="btn btn-number" type="button" data-type="plus" data-field="quantity[{{ $key }}]">
        												<i class="la la-plus"></i>
        											</button>
        										</span>
        									</div>
        									@if($qty == 0)
        									<span style="color:red;">Not in-Stock, Remove item to proceed further.</span>
        									@php 
        									    $stopProcessing = true;
        									    $outofstock = true;
        									    @endphp
        									@elseif($cartItem['quantity'] > $qty)
        									    <span style="color:red;">Required stock not avalilable, Current avalilable Stock {{$qty}}</span>
        									    @php 
        									    $stopProcessing = true;
        									    $lowStock = true;
        									    @endphp
        									@endif
        								@endif
        							</div>
        						</div>
        						<div class="col-4 col-md-2 px-0 text-right product-tota">
        						    <div class="product-price">
        					        @if($cartItem['discount'] != 0)
        					        <del class="d-none d-md-block" style="color:red">{{ single_price(($cartIteamPrice + $cartItem['tax'] + $cartItem['discount']) *$cartItem['quantity']+$getPercent) }}</del>  
        					        @endif
        						    	{{ single_price(($cartIteamPrice+$cartItem['tax'])*$cartItem['quantity']) }}
        						    </div>
        						</div>
        						<div class="col-1 col-md-1 text-right">
        							<div class="product-remove">
        								<a href="#" onclick="removeFromCartView(event, {{ $key }})" class="text-left "><i class="la la-trash"></i></a>
        							</div>
        						</div>
        					</div>
						@endforeach
						<div class="row subtotal_row py-2 d-none d-md-block">
							<div class="col-md-6 float-left">
                                <a href="javascript:history.back(-2)" class="btn btn-styled btn-base-1 bg-blue">
                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                    {{__('Continue Shopping')}}
                                </a>
                            </div>
							<div class="col-md-6 float-right text-right">
								<span class="number-of-items">Subtotal ({{ count(Session::get('cart')) }} {{__('items')}}):</span>
								<span class="product-price">
									<span class="size-medium"><span class="currencyINR">&nbsp;&nbsp;</span>&nbsp;{{ single_price($subtotal) }}</span>
								</span>
							</div>
						</div>
					</div>
				</div>
				
						
				<div class="col-xl-4 ml-lg-auto">
					<div class="card sticky-top">
						<div class="card-title py-1 bg-blue">
							<div class="row subtotal_row pt-2 text-right d-block d-md-none">
								<div class="col-12">
									<div data-name="Subtotals" class="a-row a-spacing-mini sc-subtotal sc-subtotal-activecart sc-java-remote-feature">
										<span class="number-of-items">Subtotal  ({{ count(Session::get('cart')) }} {{__('items')}}):</span>
										<span class="product-price">
											<span class="size-medium text-white"><span class="currencyINR">&nbsp;&nbsp;</span>&nbsp;{{ single_price($subtotal) }}</span>
										</span>
									</div>
								</div>
							</div>
							<div class="d-none d-md-block">
								<div class="row align-items-center">
									<div class="col-12">
										<h3 class="heading heading-3 strong-400 mb-0">
											<span class="text-white">{{__('Summary')}}</span>
										</h3>
									</div>
								</div>
							</div>
						</div>	
						<div class="card-title py-3">	
							
							<div class="row subtotal_row pt-2 text-right d-none d-md-block">
								<div class="col-12">
									<div data-name="Subtotals" class="a-row a-spacing-mini sc-subtotal sc-subtotal-activecart sc-java-remote-feature">
										<span class="number-of-items">Subtotal ({{ count(Session::get('cart')) }} {{__('items')}}):</span>
										<span class="product-price">
											<span class="size-medium"><span class="currencyINR">&nbsp;&nbsp;</span>&nbsp;{{ single_price($subtotal) }}</span>
										</span>
									</div>
								</div>
							</div>
							<table class="table-cart table-cart-review">
								<tfoot>
									<!--<tr class="cart-subtotal">
										<th>{{__('Subtotal')}}</th>
										<td class="text-right">
											<span class="strong-600 product-price">{{ single_price($subtotal) }}</span>
										</td>
									</tr>-->

									<tr class="cart-shipping">
										<th>{{__('Tax')}}</th>
										<td class="text-right">
											<span class="text-italic">{{ single_price($tax) }}</span>
										</td>
									</tr>
									<tr class="cart-shipping">
										<th>{{__('Product Shipping')}}</th>
										<td class="text-right">
											<span class="text-italic">{{ single_price($shipping) }}</span>
										</td>
									</tr>
									@php
									    if($coupon_category_discount > 0) {
									        session(['coupon_discount' => $coupon_category_discount]);
									    }
									@endphp
									
        							@if (Session::has('coupon_discount'))
        								<tr class="cart-shipping">
        									<th>{{__('Coupon Discount')}}</th>
        									<td class="text-right">
        										<span class="text-italic">(-) {{ single_price(Session::get('coupon_discount')) }}</span>
        									</td>
        								</tr>
        							@endif
        							
									
									@php
        								$total = $subtotal+$tax+$shipping;
        								if(Session::has('coupon_discount')){
        									$total -= Session::get('coupon_discount');
        								}
        							@endphp
        							
            						@php
                                		if($total < env('FREE_CART_VALUE',0)) {
                                            session(['cart_shipping' => env('BELOW_FREE_CART_CHARGE',0)]);
                                        } else {
                                            session(['cart_shipping' => 0]);
                                        }
                                        $total += 0;//session('cart_shipping');
            						@endphp
            						
            						@if(session('cart_shipping') > 0)
            						<tr class="cart-total">
										<th><span class="strong-600">{{__('Delivery Charges')}}</span></th>
										<td class="text-right">
											<strong><span class="product-price">(+) {{ single_price(0) }}</span></strong>
										</td>
									</tr>
									<script> $('.shippingdiv').html("Eligible for free shipping above ₹250 order value"); </script>
									@else
									<script> $('.shippingdiv').html("Eligible for free shipping"); </script>
            						@endif
									<tr class="cart-total">
										<th><span class="strong-600">{{__('Total')}}</span></th>
										<td class="text-right">
											<strong><span class="product-price">{{ single_price($total) }}</span></strong>
										</td>
									</tr>
								</tfoot>
							</table>
							@if (\App\BusinessSetting::where('type', 'coupon_system')->first()->value == 1)
								<div class=" clearfix">
								<div class="col-12">
								@if (Session::has('coupon_id'))
									<div class="mt-3">
										<form class="form-inline" action="{{ route('checkout.remove_coupon_code') }}" method="POST" enctype="multipart/form-data">
											@csrf

											
											@if($applyCoupe)
											<div class="form-group flex-grow-1  mb-0">
												<div class="form-control bg-gray w-100">{{ \App\Coupon::find(Session::get('coupon_id'))->code }}</div>
											</div>
											<button type="submit" class="btn btn-base-1" style="background:#131921">{{__('Change Coupon')}}</button>
											@endif
										</form>
									</div>
								@else
									<!-- <div class="mt-3">
										<form class="form-inline" action="{{ route('checkout.apply_coupon_code') }}" method="POST" enctype="multipart/form-data">
											@csrf
											<div class="form-group flex-grow-1  mb-0">
												<input type="text" class="form-control w-100" name="code" placeholder="{{__('Have coupon code? Enter here')}}" required>
											</div>
											<button type="submit" class="btn btn-base-1" style="background:#131921">{{__('Apply')}}</button>
										</form>
									</div> -->
								@endif
								

								</div>
							@endif
							
							<div class="clearfix"></div>
							<span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="
        ----------COD------------</br>
                            1-499 Shipping Charge Rs.50 </br>
        					500-999 Shipping charge Rs.30</br></br>
        					------Online Payment------</br>
        					1-499 Shipping Charge Rs.50</br>
        					500 above shipping free!!</br>-">
               <button class="btn btn-primary" style="pointer-events: none;" type="button" disabled>To save you shipping charges click here</button>
       </span>

							

						
							

							<div class="clearfix">&nbsp;&nbsp;&nbsp;</div>

							
								<div class="col-12 text-right my-3">
								    @if($stopProcessing == true)
								    	<button class="btn btn-styled btn-xs-block btn-base-1 bg-blue" disabled>{{__('Proceed to Buy')}}</button>
								    	<script>
								    	@if($lowStock)
								    	   $('.lowStock').slideDown();
								    	@else
								    	  $('.outofstock').slideDown();
								    	@endif
								    	</script>
								    @else
    									@if(Auth::check())
    										<a href="{{ route('checkout.shipping_info') }}" class="btn btn-xs-block btn-styled btn-base-1 bg-blue">{{__('Proceed to Buy')}}</a>
    									@else
    										<button class="btn btn-styled btn-xs-block btn-base-1 bg-blue" onclick="showCheckoutModal()">{{__('Proceed to Buy')}}</button>
    									@endif
									@endif
								
                                    
                                     <a href="javascript:history.back(-2)" class="btn btn-styled btn-base-1 d-md-none btn-sm-block btn-xs-block mt-2 bg-blue">
                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                    {{__('Continue Shopping')}}
                                </a>
								</div>
                                    
							</div>
    						
						</div>
					</div>
				</div>
			</div>
		    @else
	        <div class="row">
               <div class="col-md-12 py-3">
		        	<h3 class="heading heading-6 strong-700 text-center">{{__('Your Cart is empty')}}</h3>
		        </div>
		        <div class="col-md-12 text-center">
                    <a href="{{ route('home') }}" class="btn btn-styled btn-base-1 bg-blue">
                        <i class="fa fa-reply" aria-hidden="true"></i>
                        {{__('Return to shop')}}
                    </a>
                </div>
			</div>
		    @endif
		@else
			<div class="row">
               <div class="col-md-12 py-3">
		        	<h3 class="heading heading-6 strong-700 text-center">{{__('Your Cart is empty')}}</h3>
		        </div>
		        <div class="col-md-12 text-center">
                    <a href="{{ route('home') }}" class="btn btn-styled btn-base-1 bg-blue">
                        <i class="fa fa-reply" aria-hidden="true"></i>
                        {{__('Return to shop')}}
                    </a>
                </div>
			</div>
		@endif
	   </div>
	</section>
    <div class="py-5 py-md-0"></div>
    <!-- Modal -->
    <style>
        .or--1 > span{height:0;}
        
    </style>
    <div class="modal fade" id="GuestCheckout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-zoom modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">{{__('Login')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="">
                        <form role="form" id="loginform"  style="display:none" method="POST">
                            <a href="javascript:resetLogin()"><i class="fa fa-reply" aria-hidden="true"></i> Back </a> <br /><br />
                            <input type="hidden" name="cart_login" value="1">
							@csrf
							<div class="input-group mb-3">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-user"></i></span>
								</div>
									<input type="text" class="form-control input_user {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{__('Email Or Phone')}}" name="email" id="email" onkeypress="return isNumberKeyGuest(event);"  onkeyup="checknumber(this.value)" autocomplete="off">
							</div>
							<div class="input-group mb-3" id="OTPDiv" style="display:none;">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
									<input type="text" class="form-control input_user" value="{{ old('otp') }}" placeholder="{{__('OTP')}}" name="otp" id="otp" onkeypress="return isNumberKey(event);" autocomplete="off">
							</div>
							<div class="input-group mb-2" id="PasswordDiv">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
								<input type="password" class="form-control input_pass {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{__('Password')}}" name="password" id="password">
							</div>
							<div class="form-group" id="remembermeDiv">
								<div class="custom-control custom-checkbox">
									 <input id="demo-form-checkbox" class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
									<label class="custom-control-label" for="demo-form-checkbox">{{ __('Remember Me') }}</label>
								</div>
							</div>
							
                            <div class="row" id="mobileOtpDiv" style="display:none;">
							<div class="col-6 pr-1">
								<button type="button" class="btn btn-styled btn-base-1 btn-md btn-block" style="background:#131921" onclick="loginwithOTP(this, 'mobileOtpDiv')">{{ __('Login with OTP') }}</button>
							</div>
							<div class="col-6 text-right pl-1">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md btn-block bg-blue">{{ __('Login') }}</button>
							</div>
							</div>
							
                            <div class="row" id="guestmobileOtpDiv" style="display:none;">
							<div class="col-12 pr-1">
								<button type="button" class="btn btn-styled btn-base-1 btn-md btn-block bg-blue" onclick="loginwithOTP(this, 'guestmobileOtpDiv')">{{ __('Verify') }}</button>
							</div>
							</div>
                            
							<div class="col-12 justify-content-center mt-3 login_container pl-0 pr-0" id="loginDiv">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md w-100 bg-blue">{{ __('Login') }}</button>
							</div>
							
							<div class="col-12 justify-content-center mt-3 login_container pl-0 pr-0" id="GuestloginDiv" style="display:none;">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md w-100">{{ __('Continue') }}</button>
							</div>
                            
							<div class="col-12 justify-content-center mt-3 login_container" id="SignupDiv" style="display:none;">
								<button type="submit" class="btn btn-styled btn-base-1 btn-md w-100">{{ __('Sign Up') }}</button>
							</div>
                            
                            <div class="col-12 text-center" id="mobileOtpretryDiv" style="display:none;margin-top:10px;">
                                <center id="mobileotpretrycontentdiv"></center>
                            </div>
                            <div class="col-12 text-center" style="margin-top:10px;" id="forgetpasswordLink">
                                 <a href="{{ route('password.request') }}" class="link link-xs link--style-3">{{__('Forgot password?')}}</a>
                            </div>
						</form>
                    </div>
                    <div id="socialmedialogin">
                     @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                        <div class="px-3 pb-0">	
						<button class="btn btn-styled btn-block btn-apple btn-icon--2 btn-icon-left px-4 mb-3" id="signInbtn">
							{{__('Sign in with Phone or Email')}}
						</button>
						</div>
						
                        <div class="or or--1 text-center">
                            <span>or</span>
                        </div>
                        
                        <div class="px-3 pb-0">
                            
						     <a href="{{ route('social.login', ['provider' => 'apple']) }}" class="btn btn-styled btn-block btn-apple btn-icon--2 btn-icon-left px-4 mb-3">
											<span class="icon fa fa-apple" ></span> {{__('Sign in with Apple')}}
							</a>
                            @if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
                                <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="btn btn-styled btn-block btn-facebook btn-icon--2 btn-icon-left px-4 mb-3">
                                    <i class="icon fa fa-facebook"></i> {{__('Login with Facebook')}}
                                </a>
                            @endif
                            @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)
                                <a href="{{ route('social.login', ['provider' => 'google']) }}" class="btn btn-styled btn-block btn-google btn-icon--2 btn-icon-left px-4 mb-3">
                                    <i class="icon fa fa-google"></i> {{__('Login with Google')}}
                                </a>
                            @endif
                            @if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                            <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="btn btn-styled btn-block btn-twitter btn-icon--2 btn-icon-left px-4 mb-3">
                                <i class="icon fa fa-twitter"></i> {{__('Login with Twitter')}}
                            </a>
                            @endif
                        </div>
                    @endif
                    </div>
                    <div id="guestCheckout">
                    @if (\App\BusinessSetting::where('type', 'guest_checkout_active')->first()->value == 1)
                        <div class="or or--1 mt-0 text-center">
                            <span>or</span>
                        </div>
                        <div class="text-center">
                            <button type="button" onclick="guestCheckout()" class="btn btn-styled btn-base-1 bg-blue">{{__('Guest Checkout')}}</button>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
    var guestcheckout = false;
    function guestCheckout() {
        guestcheckout = true;
        $('#PasswordDiv').hide();
        $('#remembermeDiv').hide();
        $('#loginDiv').hide();
        $('#guestmobileOtpDiv').show();
        $('#email').prop('placeholder','Enter Phone Number');
	   $("#loginform").show();
	   $("#socialmedialogin").hide();
	   $("#guestCheckout").hide();
	   $("#forgetpasswordLink").hide();
    }
    
    function resetLogin() {
        guestcheckout = false;
        $('#PasswordDiv').show();
        $('#email').attr('readonly',false);
        $('#email').val('');
        $('#password').val('');
        $('#remembermeDiv').show();
        $('#loginDiv').show();
        $('#GuestloginDiv').hide();
        $('#SignupDiv').hide();
        $('#guestmobileOtpDiv').hide();
        $('#email').prop('placeholder','Enter Email or Phone Number');
    	$("#socialmedialogin").show();
    	$("#guestCheckout").show();
    	$("#forgetpasswordLink").show();
    	$("#loginform").hide();
    	$("#mobileOtpretryDiv").hide();
    	$("#mobileOtpDiv").hide();
    	$("#OTPDiv").hide();
    }
    
    function removeFromCartView(e, key){
        e.preventDefault();
        removeFromCart(key);
        //checkcoupon();
    }
    
    function updateQuantity(key, element){
        $.post('{{ route('cart.updateQuantity') }}', { _token:'{{ csrf_token() }}', key:key, quantity: element.value}, function(data){
            updateNavCart();
            $('#cart-summary').html(data);
        });
    }

    function showCheckoutModal(){
        resetLogin();
        $('#GuestCheckout').modal();
    }
        var otptime = 30;
    var timee;
    function loginwithOTP(e, ele) {
        var html = $(e).html();
        $(e).prop('disabled', true);
        $(e).html('<i class="fa fa-spin fa-spinner"></i> '+ html);
        phone = $('#email').val();
        $.post('{{ route('generate.otp.login') }}', { _token:'{{ csrf_token() }}', phone:phone}, function(data){
            if(data.result == true) {
                $('#email').attr('readonly','readonly');
                $('#PasswordDiv').hide();
                $('#mobileOtpDiv').hide();
                $('#OTPDiv').show();
                $('#mobileOtpretryDiv').show();
                //$('#loginDiv').show();
                optcount();
                timee = setInterval(function(){ optcount(); }, 1000);
                msg = 'OTP has been generated successfully and send to your mobile number';
                if(data.newuser == true) {
                    msg += ' You will be registered as new user and agreed to our T & C.';
                    if(ele == "guestmobileOtpDiv") {
                        $('#guestmobileOtpDiv').hide();
                        $('#GuestloginDiv').show();
                    } else {
                        $('#SignupDiv').show();
                        $('#loginDiv').hide();
                    }
                } else {
                    if(ele == "guestmobileOtpDiv") {
                        $('#guestmobileOtpDiv').hide();
                        $('#GuestloginDiv').show();
                    } else {
                        $('#SignupDiv').hide();
                        $('#loginDiv').show();
                    }
                }
                $(e).prop('disabled', false);
                $(e).html(html);
                showFrontendAlert('success', msg);
            } else {
                $(e).prop('disabled', false);
                $(e).html(html);
                showFrontendAlert('warning', 'Enter a valid mobile number.');
            }
        });
    }
    
    $('#loginform').submit(function(e) {
        e.preventDefault();
        var data = $('#loginform').serialize();
        $.post('{{ route('user.login.validate') }}', data, function(data){
            if(data.result == true) {
                showFrontendAlert('success', 'Login has been successful.');
                $.cookie("name", data.name, { expires: 1 , path: '/' });
                window.location = "{{ route('checkout.shipping_info') }}";
            } else {
                showFrontendAlert('warning', data.message);
            }
        });
    });
    
    function optcount() {
        if(otptime > 0) {
            $('#mobileotpretrycontentdiv').html('Resend OTP in '+otptime+' seconds.');
            otptime = otptime - 1;
        } else {
            if(guestcheckout) {
                $('#mobileotpretrycontentdiv').html('<a href="javascript:loginwithOTP(\'\', \'guestmobileOtpDiv\')">Resend OTP</a>');
            } else {
                $('#mobileotpretrycontentdiv').html('<a href="javascript:loginwithOTP(\'\',\'mobileOtpDiv\')">Resend OTP</a>');
            }
            clearInterval(timee);
            otptime = 30;
        }
    }
    
    function checknumber(v) {
        if(guestcheckout == false) {
           if(v.length == 0) {
            $('#mobileOtpDiv').hide();
            $('#loginDiv').show();
            }
            if(v.length > 0) {
                if(isNaN(v)) {
                    $('#mobileOtpDiv').hide();
                    $('#loginDiv').show();
                } else {
                    $('#loginDiv').hide();
                    $('#mobileOtpDiv').show();
                }
            } 
        }
    }
    
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}


function isNumberKeyGuest(evt){
    if(guestcheckout) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57));
    } else {
        return true;
    }
}

    </script>
    
<script>
$(document).ready(function () {
	signInbtn=$("#signInbtn");
	signInbtn.click(function () {
	   $("#loginform").show();
	   $("#socialmedialogin").hide();
	   $("#guestCheckout").hide();
	});
});
</script>
@endsection
