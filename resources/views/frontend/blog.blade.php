@extends('frontend.layouts.app')

@section('content')
<style>
	.bg-white{border-radius:10px}
	.details-card { background: #ecf0f1; }
	.card-content {
		background: #ffffff;
		border: 4px;
		border-radius:10px;
		box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
	}
	.card-img {
		position: relative;
		overflow: hidden;
		border-radius: 0;
		z-index: 1;
	}
 .number {
	-webkit-box-flex: 0;
	-ms-flex: 0 0 50px;
	flex: 0 0 50px;
	font-size: 30px;
	line-height: 1;
	color: #ccc; 
}
	

	.card-img span {
		position: absolute;
		top: 15%;
		left: 12%;
		background: #1ABC9C;
		padding: 6px;
		color: #fff;
		font-size: 12px;
		border-radius: 4px;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		-ms-border-radius: 4px;
		-o-border-radius: 4px;
		transform: translate(-50%,-50%);
	}
	.card-img span h4{
		font-size: 12px;
		margin:0;
		padding:10px 5px;
		line-height: 0;
	}
	.card-desc {
		padding: 1.25rem;
	}

	.card-desc h3 {
		color: #000000;
		font-weight: 600;
		font-size: 1.2em;
		line-height: 1.4em;
		margin-top: 0;
		margin-bottom: 5px;
		padding: 0;
	}
	.card-desc h3 a{color: #000000;}
	.card-desc p {
		color: #747373;
		font-size: 14px;
		font-weight: 400;
		font-size: 1em;
		line-height: 1.5;
		margin: 0px;
		margin-bottom: 20px;
		padding: 0;
		font-family: 'Raleway', sans-serif;
		min-height:100px;
	}
	.btn-card{
		background-color: #94A2ED;
		color: #fff;
		box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
		padding: .84rem 2.14rem;
		font-size: .81rem;
		-webkit-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
		transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
		-o-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
		transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
		transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
		margin: 0;
		border: 0;
		-webkit-border-radius: .125rem;
		border-radius: .125rem;
		cursor: pointer;
		text-transform: uppercase;
		white-space: normal;
		word-wrap: break-word;
		color: #fff;
	}
	.btn-card:hover {
		background: #94A2ED;
	}
	a.btn-card {
		text-decoration: none;
		color: #fff;
	}
	ul {
		list-style: outside none none;
		margin: 0;
		padding: 0;
	}
	.entry-meta {
		border-bottom: 1px solid #e5e5e5;
		display: flex;
		justify-content: space-between;
		padding-bottom: 16px;
		margin-bottom: 18px;
	}
	.entry-meta li {
		font-size: 15px;
	}
	.entry-meta li a{
		color: #082c4b;
		font-weight: 500;
		-webkit-transition: all 0.3s ease-in-out;
		-moz-transition: all 0.3s ease-in-out;
		-ms-transition: all 0.3s ease-in-out;
		-o-transition: all 0.3s ease-in-out;
		transition: all 0.3s ease-in-out;
	}
	
	.widget-post .media {
		margin-bottom: 30px;
	}
	.widget-post .media .item-img {
		overflow: hidden;
		margin-top: 4px;
		border-radius: 4px;
		width: 100px;
	}
	.widget-post .media .item-img a {
		display: inline-block;
	}
	.widget-post .media .item-img a img {
		border-radius: 4px;
		transform: scale(1);
		-webkit-transition: all 0.3s ease-out;
		-moz-transition: all 0.3s ease-out;
		-ms-transition: all 0.3s ease-out;
		-o-transition: all 0.3s ease-out;
		transition: all 0.3s ease-out;
	}
	.media-body.space-sm {
		margin-left: 15px;
	}
	.widget-post .media .media-body .post-title {
		margin-bottom: 8px;
		line-height: 1.4;
	}
	.widget-post .media .media-body .post-title a {
		color: #444444;
		font-weight: 500;
		-webkit-transition: all 0.3s ease-out;
		-moz-transition: all 0.3s ease-out;
		-ms-transition: all 0.3s ease-out;
		-o-transition: all 0.3s ease-out;
		transition: all 0.3s ease-out;
	}
	.widget-post .media .media-body .post-date {
		font-size: 14px;
		color: #ee212b;
	}
	.details-card img {
		max-width: 100%;
		height: auto;
	}
	.post-title{font-size:1rem;}
</style>
<section class="gry-bg  details-card" @if($mobileapp == 1) style="margin-top:75px" @endif>
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<h1 class="text-center p-4 bg-white">Our Blogs</h1>
			</div>
		</div>
	
		
    		<div class="row mt-2">
    			<div class="col-md-8">
    				<div class="row">
    				    @php $blogscount=count($Blog);
    				    @endphp
    					@foreach($Blog as $B)
    						<div class="@if($blogscount>=3) col-md-4 @else col-{{ 12/$blogscount }} @endif mb-4">
    							<div class="card-content">
    								<div class="card-img">
    									<a href="{{url('blog/'.$B->URLSlug)}}">
    										@if($B->FeaturedImage!=null)
    											<img src="{{asset($B->FeaturedImage)}}" alt="{{$B->BlogTitle}}">
    										@else
    											<img src="{{asset('frontend/images/placeholder.jpg')}}" alt="{{$B->BlogTitle}}">
    										@endif
										</a>
    								</div>
    								<div class="card-desc">
    									<h3><a href="{{url('blog/'.$B->URLSlug)}}">{{$B->BlogTitle}}</a></h3>
    									<ul class="entry-meta">
    										<li></li>
    										<li >{{Carbon\Carbon::parse($B->created_at)->diffForHumans()}}</li>
    									</ul>
    									<p>{!! Str::limit($B->BlogSummary, 150, ' ...') !!}</p>
    									<a href="{{url('blog/'.$B->URLSlug)}}" class="btn-card">Read More</a>   
    								</div>
    							</div>
    						</div>
    					@endforeach
    				
    					 <div class="pull-right">
                            {{ $Blog->appends(request()->input())->links() }}
                        </div>
    				</div>
    			</div>
    			<div class="col-md-4">
    			<!-- Side Widget -->
    				<div class="card" style="border-radius:10px;">
    					<h5 class="card-header p-2" >Popular Blogs</h5>
    					<div class="card-body widget-post">
						@php $sn=1; @endphp
    					@foreach($PopularBlog as $P)
    						<div class="media">
    							<div class="">
    								<div class="number align-self-start">{{$sn++}}</div>
    							</div>
    							<div class="media-body space-sm">
    								<h5 class="post-title">
    									<a href="{{url('blog/'.$P->URLSlug)}}">{{$P->BlogTitle}}</a>
    								</h5>
    								<div class="post-date">{{Carbon\Carbon::parse($P->created_at)->diffForHumans()}}</div>
    							</div>
    						</div>
    					@endforeach
    					</div>
    				</div>
    			</div>
    		</div>
    
	</div>
</section>

@endsection
