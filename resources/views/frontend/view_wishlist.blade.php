@extends('frontend.layouts.app')

@section('content')
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @if(Auth::user()->user_type == 'seller')
                        @include('frontend.inc.seller_side_nav')
                    @elseif(Auth::user()->user_type == 'customer')
                        @include('frontend.inc.customer_side_nav')
                    @endif
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 p-0 text-white">
                                        {{__('Wishlist')}}
                                    </h2>
                                </div>
                            </div>
                        </div>
                    @if(count($wishlists)!=0)
                        <!-- Wishlist items -->
						@php $product_id=""; @endphp
					<form id="option-choice-form">
						@csrf
						<input type="hidden" name="id" id="id">
                        <div class="row shop-default-wrapper shop-cards-wrapper shop-tech-wrapper mt-4">
                            @foreach ($wishlists as $key => $wishlist)
                                @if ($wishlist->product != null)
                                    <div class="col-xl-6 col-12" id="wishlist_{{ $wishlist->id }}">
                                        <div class="card card-product mb-3 product-card-2">
                                            <div class="card-body p-3">
                                            <div class="row">
                                                <div class="col-4">
                                                    <a href="{{ route('product', $wishlist->product->slug) }}" class="d-block" target="_blank">
														@if($wishlist->product->thumbnail_img!=null)
															<img src="{{asset($wishlist->product->thumbnail_img)}}" alt="{{$wishlist->product->name}}" class="img-fluid"/>
														@else
															<img src="{{asset('frontend/images/product-thumb.jpg')}}" alt="{{$wishlist->product->name}}" class="img-fluid"/>
														@endif
                                                    </a>
                                                </div>
												<div class="col-8">
													<h2 class="heading heading-6 strong-600 mt-2 text-truncate-2">
														<a href="{{ route('product', $wishlist->product->slug) }}"target="_blank">{{ $wishlist->product->name }}</a>
													</h2>
													<div class="mt-2">
														<div class="price-box">
														@php
															$home_base_price = home_base_price($wishlist->product->id);
															$home_discounted_base_price = home_discounted_base_price($wishlist->product->id);
														@endphp
															@if(home_base_price($wishlist->product->id) != home_discounted_base_price($wishlist->product->id))
																<del class="old-product-price strong-400">{{ home_base_price($wishlist->product->id) }}</del>
																<span class="product-price strong-600">{{ home_discounted_base_price($wishlist->product->id) }}</span>
																<strong>
																<span style="color:red;font-size:14px;">({{discount_calulate($home_base_price, $home_discounted_base_price )}}% off)</span></strong>
															@else
																<span class="product-price strong-600">{{ home_discounted_base_price($wishlist->product->id) }}</span>
															@endif
														</div>
													</div>
												</div>
                                            </div>
                                            </div>
                                            <div class="card-footer p-3">
                                                <div class="product-buttons">
                                                    <div class="row align-items-center">
                                                        <div class="col-2">
                                                            <a href="#" class="link link--style-3 bg-red btn" data-toggle="tooltip" data-placement="top" title="Remove from wishlist" onclick="removeFromWishlist({{ $wishlist->id }})">
                                                                <i class="fa fa-times mr-0"></i>
                                                            </a>
                                                        </div>
                                                        <div class="col-10">
														@php
															$qty = 0;
															if($wishlist->product->variant_product){
																foreach ($wishlist->product->stocks as $key => $stock) {
																	$qty += $stock->qty;
																}
															}
															else{
																if($wishlist->product->current_stock > -1) {
																$qty = $wishlist->product->current_stock;
																} else {
																$qty = 100;
																}
																
															}
														@endphp
															 @if ($qty > 0)
																<button type="button" class="btn btn-block btn-base-1 btn-circle btn-icon-left" onclick="addWishtToCart(this,this.id,{{$wishlist->id}})" id="{{$wishlist->product_id}}">
                                                                <i class="la la-shopping-cart mr-2"></i>{{__('Move to cart')}}
																</button>
															@else
																<button type="button" class="btn btn-block btn-base-3 btn-icon-left strong-700" disabled>
																<i class="la la-cart-arrow-down"></i> {{__('Out of Stock')}}
																</button>
															@endif	
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
					</form>
                    @else
						<div class="card no-border mt-4">
							<div>
								<table class="table table-sm table-hover table-responsive-md">
									<thead>
										<tr class="text-center">
											<th><h3>{{__('No Wishlist Found')}}</h3></th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					@endif 
                     
                     
                        <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                {{ $wishlists->links() }}
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        function addWishtToCart(el,id,wishid){
			//id= $("#id").val(id);
			var html = $(el).html();
            $(el).prop('disabled', true);
            $(el).html('<i class="fa fa-spin fa-spinner"></i> Loading');
            addToCart(el,id);
			removeFromWishlist(wishid);
		}
        function removeFromWishlist(id){
            $.post('{{ route('wishlists.remove') }}',{_token:'{{ csrf_token() }}', id:id}, function(data){
                $('#wishlist').html(data);
                $('#wishlist_'+id).hide();
                showFrontendAlert('success', 'Item has been renoved from wishlist');
            })
        }
    </script>
@endsection
