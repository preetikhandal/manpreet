@extends('frontend.layouts.app')
@php
	$data=\App\Policy::where('name', 'privacy_policy')->first();
@endphp

@section('meta_title'){{ $data->meta_title }}@stop

@section('meta_description'){{ $data->description }}@stop

@section('content')

    <section class="gry-bg py-4">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="p-4 bg-white">
                        @php
                            echo $data->content;
                        @endphp
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

