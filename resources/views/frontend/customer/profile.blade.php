@extends('frontend.layouts.app')

@section('content')
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @if(Auth::user()->user_type == 'seller')
                        @include('frontend.inc.seller_side_nav')
                    @elseif(Auth::user()->user_type == 'customer')
                        @include('frontend.inc.customer_side_nav')
                    @endif
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        @if($errors->any())
							<div class="alert alert-danger">
								<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
								<ul>
									@foreach($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
                        <!-- Page title -->
                        <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-12">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 p-0 text-white">
                                        {{__('Manage Profile')}}
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <form class="" action="{{ route('customer.profile.update') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-box bg-white mt-4" style="border:1px solid #282563">
                                <div class="form-box-title px-3 py-2 bg-blue text-white">
                                    {{__('Basic info')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Your Name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Your Name')}}" name="name" value="{{ Auth::user()->name }}">
                                        </div>
                                       
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Your Email')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="email" class="form-control mb-3" placeholder="{{__('Your Email')}}" name="email" value="{{ Auth::user()->email }}" @if(isset(Auth::user()->email)) readonly @endif>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Photo')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <span class="err text-danger"></span>
                                            <input type="file" name="photo" id="file-3" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/jpg,image/jpeg,image/x-png" />
                                            <label for="file-3" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{__('Choose image')}}
                                                </strong>
                                            </label>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Your Password')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control mb-3" placeholder="{{__('New Password')}}" name="new_password">
                                        </div>
                                    	
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Confirm Password')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control mb-3" placeholder="{{__('Confirm Password')}}" name="confirm_password">
                                        </div>
                                    	
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4" style="border:1px solid #282563">
                                <div class="form-box-title px-3 py-2 bg-blue text-white">
                                    {{__('Shipping info')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Address')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea class="form-control textarea-autogrow mb-3" placeholder="{{__('Your Address')}}" rows="1" name="address">{{ Auth::user()->address }}</textarea>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Country')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <select class="form-control mb-3 selectpicker" data-placeholder="{{__('Select your country')}}" name="country">
                                                    @foreach (\App\Country::all() as $key => $country)
                                                        <option value="{{ $country->name }}" <?php if(Auth::user()->country == $country->name) echo "selected";?> >{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('City')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Your City')}}" name="city" value="{{ Auth::user()->city }}">
                                        </div>
                                    	
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Postal Code')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Your Postal Code')}}" name="postal_code"  value="{{ Auth::user()->postal_code }}" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6">
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Phone')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Your Phone Number')}}" name="phone" value="{{ Auth::user()->phone }}" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10"  @if(isset(Auth::user()->phone)) readonly @endif>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="text-right mt-4">
                                <button type="submit" class="btn btn-styled btn-base-1">{{__('Update Profile')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection
@section('script')
    <script type="text/javascript">
    	$('INPUT[type="file"]').change(function () {
    		var ext = this.value.match(/\.(.+)$/)[1];
    		var FileSize = this.files[0].size / 1024 / 1024; // in MB
    		if (FileSize > 4) {
    			$(".err").text('Max file size: 4MB');
    			this.value = '';
    		}
    		switch (ext) {
    			case 'jpg':
    			case 'jpeg':
    			case 'png':
    				$('#uploadButton').attr('disabled', false);
    				break;
    			default:
    				$(".err").text('This is not an allowed file type.Only jpg/png files are allowed to upload');
    				this.value = '';
    		}
    	});
    </script>
@endsection
