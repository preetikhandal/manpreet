@extends('frontend.layouts.app')

@section('content')
    <style>
    .slick-arrow .la-angle-left {    vertical-align: middle;
    position: absolute;
    left: 10%;
    top: 40%;}
    .slick-arrow .la-angle-right {    vertical-align: middle;
    position: absolute;
    right: 10%;
    top: 40%;}
    .slick-slide img { margin: 0 auto; text-align: center; }
        .flash-deal-box .countdown .countdown-digit{
            background:#fff!important;
        }
        .pcflash .flash-deal-box .countdown .countdown-digit{
            background:#EB3038!important;
            color:#fff!important;
        }
        .best-sell-slider-22 .slick-prev {     left: 0;
    background: url({{ asset('frontend/images/left-arrow.png') }}) #fff;
    background-repeat: no-repeat;
    background-position-y: 50%;
        background-position-x: 40%;}
        .slick-next { right: 0;   
    background: url({{ asset('frontend/images/right-arrow.png') }}) #fff;
    background-repeat: no-repeat;
        background-position-y: 50%;
    background-position-x: 40%; }
        .best-sell-slider-22 .slick-prev, .slick-next {
            background-size: 16px;
            position: absolute; 
    top: calc(50% - 50px) !important;
    display: inline-block;
    font-size: 0;
    cursor: pointer;
    text-align: center;
    color: #000;
    border: 1px solid #ebebeb;
    z-index: 9;
    opacity: 1;
    border-radius: 3px 0 0 3px;
    -webkit-transition: all 300ms linear;
    -moz-transition: all 300ms linear;
    -ms-transition: all 300ms linear;
    -o-transition: all 300ms linear;
    transition: all 300ms linear;
    -webkit-transition: all 300ms linear;
    -moz-transition: all 300ms linear;
    -ms-transition: all 300ms linear;
    -o-transition: all 300ms linear;
    transition: all 300ms linear;
    height: 80px!important;
    line-height: 100px;
    width: 35px;
    box-shadow: 0 1px 3px #888;}
    .brandBanner .arrow-round .slick-arrow{height: 40px !important; border-radius: 100% !important; }
    .brandBanner .slick-arrow .la-angle-left {left: 25%; top: 30%;}
    .brandBanner .slick-arrow .la-angle-right {right: 25%; top: 30%;}
    </style>
	<div id="section_all_category" class="mobileCategory d-lg-none d-md-none" @if($mobileapp == 1) style="margin-top:75px" @endif>
		<div class="all-category-wrap gry-bg d-block d-md-none d-lg-none">
			<div class="sticky-top">
				<div class="container-fluid" style="padding-right: 0;padding-left: 0;">
					<div class="row">
						<div class="col">
							<div class="all-category-menu" style="background:#FFFFFF">
								<ul class="clearfix no-scrollbar">
									@foreach ($MobileCategories as $key => $category)
									    @if($category->icon!=null)
										<li class="@php if($key == 0) echo 'active' @endphp" style="width: 66px; height: 62px;background:#FFFFFF">
										    @if($mobileapp == 0)
										        @if(count($category->subcategories->where('display',1)) > 0)
											    <a href="{{ route('subcategories.all.mobile', ['category_slug'=>$category->slug]) }}" class="row">
											    @else
											    <a href="{{ route('products.category', [$category->slug]) }}" class="row">
											    @endif
										    @else
										        @if(count($category->subcategories->where('display',1)) > 0)
											    <a href="{{ route('subcategories.all.mobile', ['category_slug'=>$category->slug, 'android' => 1]) }}" class="row">
											    @else
											    <a href="{{ route('products.category', ['category_slug' => $category->slug, 'android' => 1]) }}" class="row">
											    @endif
										    @endif
												<div class="col">
													@if(!empty($category->icon))
														<img loading="lazy"  class="img-fluid" src="{{ asset($category->icon) }}" alt="{{$category->name}}">
													@else
													    <img loading="lazy"  class="img-fluid" src="{{ asset('frontend/images/caticon.png') }}" alt="{{$category->name}}">
													@endif
												</div>
											</a>
										</li>
										@endif
									@endforeach
									<li class="" style="width: 66px; height: 62px;">
									    @if($mobileapp == 0)
										<a href="{{ route('categories.all.mobilegrid') }}" class="row">
									     @else
										<a href="{{route('categories.all.mobilegrid', ['android' => 1])}}" class="row">
									     @endif
											<div class="col" style="margin-top: 32px;">
												View All >
											</div>
										</a>  
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <section class="home-banner-area mb-2 pt-1 pb-0">
        <div class="container-fluid" style="max-width: 100%!important;padding-left:0px;padding-right:0px">
            <div class="row no-gutters position-relative">
				<div class="col-12 d-none d-sm-block order-1 order-lg-0">	
                    <div class="home-slide">
						<div class="slick-carousel" data-slick-arrows="false" data-slick-dots="true" data-slick-autoplay="true">
							@php
							    if(env('CACHE_DRIVER') == 'redis') {
                        		$Sliders = Cache::tags(['Sliders'])->rememberForever('Sliders', function () {
                                        return  \App\Slider::where('published', 1)->get();
                                });
                                } else {
                                $Sliders = Cache::rememberForever('Sliders', function () {
                                        return  \App\Slider::where('published', 1)->get();
                                });
                                }
							@endphp
							@foreach ($Sliders as $key => $slider)
								@if($slider->photo!="")
								<div class="">
								    @if($mobileapp == 0)
									<a href="{{ $slider->link }}">
									@else
									<a href="{{ $slider->link }}?android=1">
									@endif
									<img class="d-block w-100 h-100 lazyload" src="{{ asset('frontend/images/placeholder-rect.jpg') }}" data-src="{{ asset($slider->photo) }}" alt="{{ env('APP_NAME')}} promo">
									</a>
								</div>
								@endif
							@endforeach
						</div>
                    </div>
                </div>
				<div class="col-12 d-block d-md-none order-1">	
                    <div class="home-slide">
						<div class="slick-carousel" data-slick-arrows="false" data-slick-dots="true" data-slick-autoplay="true">
							@foreach ($Sliders as $key => $slider)
								<!--<div class="" style="height:275px;">-->
								@if($slider->mobile_photo!="")
								<div class="">
								    @if($mobileapp == 0)
									<a href="{{ $slider->link }}">
									@else
									<a href="{{ $slider->link }}?android=1">
									@endif
									<img class="d-block w-100 h-100 lazyload" src="{{ asset('frontend/images/placeholder-mob.jpg') }}" data-src="{{ asset($slider->mobile_photo) }}" alt="{{ env('APP_NAME')}} promo">
									</a>
								</div>
								@endif
							@endforeach
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
     @php
    if(env('CACHE_DRIVER') == 'redis') {
		$flash_deal = Cache::tags(['FlashDeal'])->rememberForever('FlashDealFeatured', function () {
                return  \App\FlashDeal::where('status', 1)->first();
        });
        } else {
        
        $flash_deal = Cache::rememberForever('FlashDealFeatured', function () {
			
                return  \App\FlashDeal::where('status', 1)->first();
        });
		
        }
    @endphp
  
    
    @if($flash_deal != null && strtotime(date('d-m-Y')) >= $flash_deal->start_date )
    <section class="mb-2 aldeproductslider">
        <div class="container-fluid bg-white py-0" style="overflow:hidden;">
			<div class="row d-block d-lg-none bg-blue">
				<div class="col-12">
	            	<div class="section-title-1 clearfix pb-2 px-0 d-flex h-100">
    					<div class="row">
    						<div class="col-9">
    							<h3 class="heading-6 strong-600 mb-0 float-left pb-2" style="color:#000;">
    							<span class="pl-3 text-white">{{$flash_deal->title}}</span>
    							</h3>
    							<div class="flash-deal-box float-left">
						        	<div class="countdown countdown--style-1 countdown--style-1-v1 text-white" data-countdown-date="{{ date('m/d/Y', $flash_deal->end_date) }}" data-countdown-label="show"></div>
    						    </div>
    						</div>
    						<div class="col-3 float-left justify-content-end align-self-end">
    						    @if($mobileapp == 0)
    						    <a href="{{ route('flash-deal-details', ['slug' => $flash_deal->slug]) }}" class="btn bg-white text-black px-1 py-1">View All</a>
    						    @else
    						    <a href="{{ route('flash-deal-details', ['slug' => $flash_deal->slug, 'android' => 1]) }}" class="btn bg-white text-black px-1 py-1">View All</a>
    						    @endif
    						</div>
    					</div>
    	        	</div>
				</div>
			</div>
			<div class="row">
			    
			    @if($flash_deal->background_img!=null)
    		        <div class="col-lg-2 col-md-3 p-0 d-none d-lg-block pcflash" style="z-index:10;">
				    	@if($mobileapp == 0)
    					<a href="{{ route('flash-deal-details', ['slug' => $flash_deal->slug]) }}">
    					@else
    					<a href="{{ route('flash-deal-details', ['slug' => $flash_deal->slug, 'android' => 1]) }}">
    					@endif
        				    <img src="{{ $flash_deal->background_img }}" style="width: 100%;height: 86%">
        				</a>
        				<div class="flash-deal-box py-1" style="">
					    	<div class="countdown countdown--style-1 countdown--style-1-v1 " data-countdown-date="{{ date('m/d/Y', $flash_deal->end_date) }}" data-countdown-label="show"></div>
				    	</div>
                    </div>
    		    @else
                    <div class="col-lg-2 col-md-3 stickblock d-none d-lg-block" style="background: #282563;z-index:10;">
                        <div class="flash-deal-box py-1" style="">
					    	<div class="countdown countdown--style-1 countdown--style-1-v1 " data-countdown-date="{{ date('m/d/Y', $flash_deal->end_date) }}" data-countdown-label="show"></div>
				    	</div>
        				<h2 style="color: #fff">{{$flash_deal->title}}</h2>
        				@if($mobileapp == 0)
    					<a href="{{ route('flash-deal-details', ['slug' => $flash_deal->slug]) }}">View All &raquo;</a>
    					@else
    					<a href="{{ route('flash-deal-details', ['slug' => $flash_deal->slug, 'android' => 1]) }}" >View All &raquo;</a>
    					@endif
    					
                    </div>
                @endif
			    
                <div class="col-lg-10 col-md-9 pt-1 px-0 pl-2">
                    
                    
                    
                    
                    
                    
                    
                <div class="best-sell-slider-22">
                <div class="swiper-wrapper" id="flash">
                    @foreach ($flash_deal->flash_deal_products as $key => $flash_deal_product)
                        @php
                            $product = \App\Product::find($flash_deal_product->product_id);
                        @endphp
                        @if ($product != null && $product->published != 0)
                                @php
                                if(\App\Product::find($product->id) == null) {
                                    continue;
                                }
                			    $home_base_price = home_base_price($product->id);
                			    $home_discounted_base_price = home_discounted_base_price($product->id);
                			    @endphp
        						<!-- Single Item -->
        						<article class="list-product slide">
        							<div class="img-block text-center">
        							    @if($mobileapp == 0)
        								<a href="{{ route('product', ['slug' => $product->slug]) }}" class="thumbnail">
        								@else
        								<a href="{{ route('product', ['slug' => $product->slug, 'android' => 1]) }}" class="thumbnail">
        								@endif
        								    @if($product->flash_deal_img != null)
        									<img class="first-img lazyload img-fluid" src="{{ asset('frontend/images/product-thumb.jpg') }}" data-src="{{ asset($product->flash_deal_img) }}" alt="{{ __($product->name) }}" />
        									@else
        								    <img class="first-img lazyload img-fluid" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
        									@endif
        								</a>
        							</div>
        							@if($home_base_price != $home_discounted_base_price)
        							<ul class="product-flag">
        								<li class="new discount-price bg-orange">{{discount_calulate($product->id,$home_base_price, $home_discounted_base_price )}}% off</li>
        							</ul>
        							@endif
        							<div class="product-decs text-center">
        								<h2><a href="{{ route('product', $product->slug) }}" class="product-link">{!! Str::limit($product->name, 58, ' ...') !!}</a></h2>
        								<div class="pricing-meta">
        									<ul>
        									    @if($home_base_price != $home_discounted_base_price)
        										<li class="old-price">{{ $home_base_price }}</li>
        										@endif
        										<li class="current-price">{{ $home_discounted_base_price }}</li>
        									</ul>
        									
        								</div>
        							</div>
        							<div class="add-to-cart-buttons">
        							    @if($product->current_stock == 0)
        							    <button class="btn btn95" type="button" disabled> Out of Stock</button>
        							    @else
        							    <button class="btn btn25" onclick="addToCart(this, {{$product->id}}, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
        							    <button class="btn btn70" onclick="buyNow(this, {{ $product->id }}, 1, 'full')" type="button"> Buy Now</button>
        							    @endif
        							</div>
        						</article>
                        @endif
                    @endforeach
        				</div>
        				 <!-- If we need navigation buttons -->
        				
                        <!--<div class="flash_deal_slider_swiper-button-prev swiper-button-prev d-none d-md-block"></div>-->
                        <!--<div class="flash_deal_slider_swiper-button-next swiper-button-next d-none d-md-block"></div>-->
                    </div>
                    <script>
                    



                    </script>
                </div>
                </div>
            </div>
    </section>
    @endif
    <div class="mb-2">
        <div class="container-fluid" style="box-shadow: 0 1px 5px #c1c1c1!important;">
            <div class="row gutters-0 deepscustomrow">
				@php
					if (Cache::has('Banners')){
					   $Banners =  Cache::get('Banners');
					} else {
						$Banners = \App\Banner::where('position', 1)->where('published', 1)->where('displayposition',"Desktop")->get();
						Cache::forever('Banners', $Banners);
					}
					$BannerCount = count($Banners);
				
				@endphp
                @foreach ($Banners as $key => $banner1)
                    <div class="d-none d-md-block col-{{ 12/count($Banners) }}">
                        <div class="media-banner mb-2 mb-lg-0">
                            @if($mobileapp == 0)
                            <a href="{{ $banner1->url }}"  class="banner-container">
                            @else
                            <a href="{{ $banner1->url }}?android=1"  class="banner-container">
                            @endif
                                <img src="{{ asset('frontend/images/banner.jpg') }}" data-src="{{ asset($banner1->photo) }}" alt="{{ env('APP_NAME') }} promo" class="img-fluid lazyload">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
			
            <div class="row gutters-0 deepscustomrow1">
                @php
					if (Cache::has('Mobile_Banners')){
					   $Mobile_Banners =  Cache::get('Mobile_Banners');
					} else {
						$Mobile_Banners = \App\Banner::where('position', 1)->where('published', 1)->where('displayposition',"Mobile")->get();
						Cache::forever('Mobile_Banners', $Mobile_Banners);
					}
				@endphp
                @foreach ($Mobile_Banners as $key => $mob_banner)
					<div class="d-block d-md-none col-12 @if(count($Mobile_Banners)>1) my-2 @endif">
						<div class="media-banner">
						    @if($mobileapp == 0)
							<a href="{{ $mob_banner->url }}" class="banner-container">
							@else
							<a href="{{ $mob_banner->url }}?android=1" class="banner-container">
							@endif
								<img src="{{ asset('frontend/images/banner.jpg') }}" data-src="{{ asset($mob_banner->photo) }}" alt="{{ env('APP_NAME') }} promo" class="img-fluid lazyload">
							</a>
						</div>
					</div>
				@endforeach
            </div>
        </div>
    </div>

    <div id="section_featured">

    </div>
	

	
	<div class="row gutters-0 deepscustomrow1">
                @php
					if (Cache::has('Mobile_Banners')){
					   $Mobile_Banners =  Cache::get('Mobile_Banners');
					} else {
						$Mobile_Banners = \App\Banner::where('position', 1)->where('published', 1)->where('displayposition',"Mobile")->get();
						Cache::forever('Mobile_Banners', $Mobile_Banners);
					}
				@endphp
                @foreach ($Mobile_Banners as $key => $mob_banner)
					<div class="d-block d-md-none col-12 @if(count($Mobile_Banners)>1) my-2 @endif">
						<div class="media-banner">
						    @if($mobileapp == 0)
							<a href="{{ $mob_banner->url }}" class="banner-container">
							@else
							<a href="{{ $mob_banner->url }}?android=1" class="banner-container">
							@endif
								<img src="{{ asset('frontend/images/banner.jpg') }}" data-src="{{ asset($mob_banner->photo) }}" alt="{{ env('APP_NAME') }} promo" class="img-fluid lazyload">
							</a>
						</div>
					</div>
				@endforeach
            </div>
        </div>
    </div>


    <!-- <div id="section_best_selling">

    </div> -->
	
	

	<div class="mb-2">
        <div class="container-fluid" style="box-shadow: 0 1px 5px #c1c1c1!important;">
            <div class="row gutters-0 deepscustomrow">
                
				@php
					if (Cache::has('Banners2')){
					   $Banners2 =  Cache::get('Banners2');
					} else {
						$Banners2 = \App\Banner::where('position', 2)->where('published', 1)->where('displayposition',"Desktop")->get();
						Cache::forever('Banners2', $Banners2);
					}
					
					$BannerCount2 = count($Banners2);
					$Ba2 = round(($BannerCount2/2), 0, PHP_ROUND_HALF_DOWN);
				@endphp
                @foreach ($Banners2 as $key => $banner)
                    <div class="d-none d-md-block col-{{ 12/count($Banners2) }}">
                        <div class="media-banner mb-2 mb-lg-0">
                            @if($mobileapp == 0)
                            <a href="{{ $banner->url }}" class="banner-container">
                            @else
                            <a href="{{ $banner->url }}?android=1" class="banner-container">
                            @endif
                                <img src="{{ asset('frontend/images/banner.jpg') }}" data-src="{{ asset($banner->photo) }}" alt="{{ env('APP_NAME') }} promo" class="img-fluid lazyload">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row gutters-0 deepscustomrow">
                @php
					if (Cache::has('Mobile_Banners2')){
					   $Mobile_Banners2 =  Cache::get('Mobile_Banners2');
					} else {
						$Mobile_Banners2 = \App\Banner::where('position', 2)->where('published', 1)->where('displayposition',"Mobile")->get();
						Cache::forever('Mobile_Banners2', $Mobile_Banners2);
					}
				@endphp
                @foreach ($Mobile_Banners2 as $key => $mob_banner2)
					<div class="d-block d-md-none col-12 @if(count($Mobile_Banners2)>1) my-2 @endif">
						<div class="media-banner">
						     @if($mobileapp == 0)
							<a href="{{ $mob_banner2->url }}" class="banner-container">
							 @else
							<a href="{{ $mob_banner2->url }}?android=1" class="banner-container">
							 @endif
								<img src="{{ asset('frontend/images/banner.jpg') }}" data-src="{{ asset($mob_banner2->photo) }}" alt="{{ env('APP_NAME') }} promo" class="img-fluid lazyload w-100">
							</a>
						</div>
					</div>
				@endforeach
            </div>
        </div>
    </div>

    <div id="section_home_categories">

    </div>

	<div id="section_Brand_Banners" class="brandBanner">
		
    </div>

   

	<div id="section_home_categories2">

    </div>
	
	<!--<div id="section_home_categories3">

    </div>-->
	
    <!--<div id="section_best_sellers">

    </div>-->

    <!--<div id="section_top_categories">
		<section class="mb-2">
			<div class="container-fluid bg-white d-none d-sm-block">
				<div class="row myrow d-block d-lg-none">
					<div class="col-12">
						<div class="section-title-1 clearfix">
							<h3 class="heading-5 strong-700 mb-0 float-left">
								<span class="mr-4 text-white">{{__('Top Catogories')}}</span>
							</h3>
							<ul class="float-right inline-links text-right">
								<li>
								     @if($mobileapp == 0)
									<a href="{{ route('categories.all') }}" class="active">{{__('View All Catogories')}}</a>
									@else
									<a href="{{ route('categories.all', ['android' => 1]) }}" class="active">{{__('View All Catogories')}}</a>
									@endif
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
				    <div class="col-lg-2 stickblock d-none d-lg-block" style=" background: #282563">
        				<h2 style="color: #fff">{{__('Top Catogories')}}</h2>
        				<a href="{{ route('categories.all', ['android' => 1]) }}">View All &raquo;</a>
                    </div>
                    <div class="col-lg-10 pt-2 px-1">
					<div class="col-12">
						<div class="row gutters-5">
							@php
								if (Cache::has('topcategories')){
								   $topcategories =  Cache::get('topcategories');
								} else {
									 $topcategories = \App\Category::where('top', 1)->get();
									Cache::forever('topcategories', $topcategories);
								}
							
							@endphp
							@foreach ($topcategories as $category)
								<div class="mb-2 col-md-2">
								    @if($mobileapp == 0)
									  @if( $category->name== 'Pharmacy')
									    <a href="{{route('pharmacy-enquiry')}}" class="bg-white border d-block c-base-2 box-2 icon-anim">
										@else
										<a href="{{ route('products.category', $category->slug) }}" class="bg-white border d-block c-base-2 box-2 icon-anim">
										@endif
									@else
									<a href="{{ route('products.category', ['category_slug'=>$category->slug,'android' => 1] ) }}" class="bg-white border d-block c-base-2 box-2 icon-anim">
									@endif
										<div class="row align-items-center no-gutters">
											<div class="col-5 text-center">
												<img src="{{ asset('frontend/images/product-thumb.jpg') }}" data-src="{{ asset($category->banner) }}" alt="{{ __($category->name) }}" class="img-fluid img lazyload">
											</div>

											<div class="info col-7 pl-1">
												<div class="name text-truncate py-2">{{ __($category->name) }}</div>
											</div>
										</div>
									</a>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>-->
	<div class="mb-2">
        <div class="container-fluid" style="box-shadow: 0 1px 5px #c1c1c1!important;">
            <div class="row gutters-0 deepscustomrow">
				@php
					if (Cache::has('Banners3')){
					   $Banners3 =  Cache::get('Banners3');
					} else {
						$Banners3 = \App\Banner::where('position', 3)->where('published', 1)->where('displayposition',"Desktop")->get();
						Cache::forever('Banners3', $Banners3);
					}
					
					$BannerCount3 = count($Banners3);
					$Ba2 = round(($BannerCount3/2), 0, PHP_ROUND_HALF_DOWN);
				@endphp
                @foreach ($Banners3 as $key => $banner3)
                    <div class="d-none d-md-block col-12 @if(count($Banners3)>1) my-2 @endif">
                        <div class="media-banner mb-2 mb-lg-0">
                            @if($mobileapp == 0)
                            <a href="{{ $banner3->url }}" class="banner-container">
                            @else
                            <a href="{{ $banner3->url }}?android=1" class="banner-container">
                            @endif
                                <img src="{{ asset('frontend/images/full-banner.jpg') }}" data-src="{{ asset($banner3->photo) }}" alt="{{ env('APP_NAME') }} promo" class="img-fluid lazyload">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row gutters-0 deepscustomrow">
                @php
					if (Cache::has('Mobile_Banners3')){
					   $Mobile_Banners3 =  Cache::get('Mobile_Banners3');
					} else {
						$Mobile_Banners3 = \App\Banner::where('position', 3)->where('published', 1)->where('displayposition',"Mobile")->get();
						Cache::forever('Mobile_Banners3', $Mobile_Banners3);
					}
				@endphp
                @foreach ($Mobile_Banners3 as $key => $mob_banner3)
					<div class="d-block d-md-none col-12 @if(count($Mobile_Banners3)>1) my-2 @endif">
						<div class="media-banner">
						     @if($mobileapp == 0)
							<a href="{{ $mob_banner3->url }}" class="banner-container">
							 @else
							<a href="{{ $mob_banner3->url }}?android=1" class="banner-container">
							 @endif
								<img src="{{ asset('frontend/images/banner.jpg') }}" data-src="{{ asset($mob_banner3->photo) }}" alt="{{ env('APP_NAME') }} promo" class="img-fluid lazyload w-100">
							</a>
						</div>
					</div>
				@endforeach
            </div>
        </div>
    </div>
	@if($mobileapp == 0)
	<!--<div id="section_shop_brands">
		<section class="mb-2">
			<div class="container-fluid bg-white">
				<div class="row myrow d-block d-lg-none">
					<div class="col-12">
						<div class="section-title-1 clearfix">
							<h3 class="heading-5 strong-700 mb-0 float-left text-white">
								<span class="mr-4">{{__('Shop by Top Brands')}}</span>
							</h3>
							<ul class="float-right inline-links text-right">
								<li>
									<a href="{{ route('brands.all') }}" class="active">{{__('View All Brands')}}</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
				    <div class="col-lg-2 stickblock d-none d-lg-block" style=" background: #282563">
        				<h2 style="color: #fff">{{__('Shop by Top Brands')}}</h2>
        				<a href="{{ route('brands.all') }}">View All &raquo;</a>
                    </div>
					<div class="col-lg-10 pt-2 px-1">
						<div class="row gutters-0 align-items-center" style="margin-left:0px">
							@php
								if (Cache::has('brands')){
								   $brands =  Cache::get('brands');
								} else {
									 $brands = \App\Brand::get()->take(32);
									Cache::forever('brands', $brands);
								}
							@endphp
						
							@foreach ($brands as $brand)
								 <div class="column bg-white">
									<a href="{{ route('categories.all', ['brand'=>$brand->slug]) }}"><img style="padding:3px" src="{{ asset($brand->logo) }}" alt="{{ __(ucwords(strtolower($brand->name))) }}" class="img-fluid img lazyload"></a>
								  </div>
							@endforeach
						</div>
					</div>
				</div>
				
			</div>
		</section>
	</div>-->
	@endif
	<style>
	.footer-top-bar h4 {
	    font-size:18px !important;
	}
	.footer-top-bar h5 {
	    font-size:16px !important;
	}
	.desc h4{
	    line-height: 1.3;
	    font-weight: 600 !important;
	}
	</style>
	<section class="slice-sm footer-top-bar" style="background:#f3f3f3" id="alde_footer">
		<div class="container-fluid sct-inner">
			<div class="row no-gutters ">
			    @php
                    $generalsetting = \App\GeneralSetting::first();
                @endphp
				<div class="col-12 desc my-3"  style="padding: 0.2rem 0;">
				    
				    @if (strlen($generalsetting->home_content) > 2500)
				        <div class="halfcontent">
				         {!! Str::limit($generalsetting->home_content, 438, ' ...') !!}
				        </div>
                        <div class="detailedDiv" style="display:none">
                          {!! $generalsetting->home_content !!}
                        </div>
                      <a href="javascript:void(0)" class="mt-2 btn btn-sm bg-blue text-white more">Read More >></a>
                    @endif
				</div>
			</div>
		</div>
	</section>

	<!---------Offer Modal---------------->
	@php
		$popup_offerData=App\popup_offer::where('published','1')->first();
	@endphp
	@if($popup_offerData!=null)
		<div class="fade text-center mt-5 p-0"  id="offerModal" role="dialog" style="display:none">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content" style="border: none">
					<div class="modal-body mb-0 p-0">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closebtn">
							<span aria-hidden="true"><i class="fa fa-times"></i></span>
						</button>
						<a href="{{$popup_offerData->url}}">
							<img src="{{asset($popup_offerData->photo)}}" class="img-fluid" alt="{{ env('APP_NAME')}} Offer" style="width:100%"/>
						</a>
					</div>
				</div>
			</div>
		</div>
		<script>
			$(window).on('load', function() {
				//if(typeof $.cookie('offerModalCookie') !== 'undefined')
				
				if(typeof $.cookie('offerModalCookie') == 'undefined') {
					if(window.matchMedia("(min-width: 300px)").matches){
						var popupURL= "{{$popup_offerData->url}}" ;
						setTimeout(function(){
							$("#offerModal").addClass("modal");
							$("#offerModal").css({"display" : "block"});
						   $('#offerModal').modal('show');
						}, 2000);
						$.removeCookie("offerModalCookie");
						var expDate = new Date();
						expDate.setTime(expDate.getTime() + (120 * 60 * 1000)); // add 120 minutes(2 hours)
						$.cookie("offerModalCookie", popupURL, { path: '/', expires: expDate });
					}
					else{ $("#offerModal").css({"display" : "none"}); }
				}
				else{
					$("#offerModal").css({"display" : "none"});
				}
				$("#closebtn").click(function () {
					$("#offerModal").modal("hide");
				});
			});
		</script>
	@endif
@endsection

@section('script')
	
    <script>
        $(document).ready(function(){
            $.post('{{ route('home.section.featured') }}', {_token:'{{ csrf_token() }}'}, function(data){
                $('#section_featured').html(data);
                slickInit();
            });

            $.post('{{ route('home.section.best_selling') }}', {_token:'{{ csrf_token() }}'}, function(data){
                $('#section_best_selling').html(data);
                slickInit();
            });
            
			$.post('{{ route('home.section.brand_banner') }}', {_token:'{{ csrf_token() }}'}, function(data){
                $('#section_Brand_Banners').html(data);
                slickInit();
            });

            $.post('{{ route('home.section.home_categories') }}', {_token:'{{ csrf_token() }}'}, function(data){
                $('#section_home_categories').html(data);
                slickInit();
            });
            
			$.post('{{ route('home.section.home_categories2') }}', {_token:'{{ csrf_token() }}'}, function(data){
				$('#section_home_categories2').html(data);
				slickInit();
			});
			
			$.post('{{ route('home.section.home_categories3') }}', {_token:'{{ csrf_token() }}'}, function(data){
                $('#section_home_categories3').html(data);
                slickInit();
            });

            $.post('{{ route('home.section.best_sellers') }}', {_token:'{{ csrf_token() }}'}, function(data){
                $('#section_best_sellers').html(data);
                slickInit();
            });
			
			
			$("#section_all_category a").click(function () {
			//	console.log("dd");
			   // var target = $(this).attr("href"); 
				// return true;
			});
        });
    </script>
	<script>
		function isNumberKey(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;

			return true;
		}
	</script>
	
	<script>
		$(function(){
			$("#pincodeBtn").click(function(){
				var pincode=$("#pincode").val();
				if(pincode=="" || pincode.length<6 || pincode.length>6){
					$(".pinerror").html("Please enter valid 6 digit pincode.");
				}
				else{
					$.ajax({
						url:"{{url('ajax/checkpicode')}}",
						type: 'post',
						dataType: "json",
						data:"pincode="+pincode,
						success: function( data ) {
							//console.log(data.success);
							if(data.success==false)
							{
								$(".pinerror").html("<i class='fa fa-exclamation' aria-hidden='true'></i> Please enter a valid pincode");
								$("#pincode").val("");
							}
							else{
								$(".pinerror").html("");
								$("#delivery_location").html('<i class="fa fa-map-marker modal-icon"></i> Deliver in '+"<b>"+data.value+", "+pincode +"</b>");
								$(".js_pin_location_msg2").html("or enter a pincode");
								$("#Current_location").html(data.value +" ("+pincode+")");
								$('#pincodemodal').modal('hide');
								
								$.cookie("CityCookie", data.value);
								$.cookie("PinCookie", pincode);
							}
						}
					});
				}
			});
		})
	</script>
	<script>
	    $(function(){
	        $(".more").click(function(){
	            if($(this).hasClass("less")) {
					$(this).removeClass("less");
					$(this).html("Read More >>");
				} else {
					$(this).addClass("less");
					$(this).html("<< Read Leass");
				}
				$('.halfcontent').slideToggle();
				$('.detailedDiv').slideToggle();
	        });
	    })
	    
	</script>
	
@endsection
