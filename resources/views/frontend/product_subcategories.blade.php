@extends('frontend.layouts.app')

@section('meta_title'){{ $Category->meta_title }}@stop

@section('meta_description'){{ $Category->meta_description }}@stop

@section('meta')
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $Category->meta_title }}">
    <meta itemprop="description" content="{{ $Category->meta_description }}">
    <meta itemprop="image" content="{{ asset($Category->menu_banner) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ $Category->meta_title }}">
    <meta name="twitter:description" content="{{ $Category->meta_description }}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="{{ asset($Category->menu_banner) }}">
    <meta name="twitter:label1" content="Price">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $Category->meta_title }}" />
    <meta property="og:type" content="product" />
    <meta property="og:url" content="{{ route('product', $Category->slug) }}" />
    <meta property="og:image" content="{{ asset($Category->menu_banner) }}" />
    <meta property="og:description" content="{{ $Category->meta_description }}" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
@endsection

@section('content')
	<div class="breadcrumb-area" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                        <li><a href="{{ url('categories') }}">{{__('All Categories')}}</a></li>
                        @if(isset($CategoryID))
                            <li class="active"><a href="{{ route('products.category', \App\Category::find($CategoryID)->slug) }}">{{ ucwords(strtolower(\App\Category::find($CategoryID)->name)) }}</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
	@foreach($SubCategory as $subCat_Products)
		
		<section class="mb-2" id="sbcatsprods">
			<div class="container-fluid bg-white">
				<div class="row">
					<div class="col-12">
						<div class="section-title-1 clearfix">
							<h3 class="heading-5 strong-700 mb-0 float-left">
								<span class="mr-4">{{strtolower($subCat_Products->name)}}</span>
							</h3>
							<ul class="inline-links float-right">
								<li><a href="{{route('products.subcategory', [\App\Category::find($CategoryID)->slug, $subCat_Products->slug])}}" class="active">{{__('View All')}}</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="px-2 py-4 p-md-3">
					<div class="caorusel-box arrow-round gutters-5">
						<div class="slick-carousel" data-slick-items="4" data-slick-xl-items="6" data-slick-lg-items="6"  data-slick-md-items="4" data-slick-sm-items="2" data-slick-xs-items="2">
							@foreach (filter_products(\App\Product::where('published', 1)->where('subcategory_id', $subCat_Products->id)->inRandomOrder())->take(10)->get() as $key => $product)
							<div class="caorusel-card">
								<div class="product-card-2 card card-product shop-cards shop-tech">
									<div class="card-body p-0">
										<div class="card-image">
											<a href="{{ route('product', $product->slug) }}" class="d-block">
												@if($product->featured_img!=NULL)
													<img class="img-fit lazyload mx-auto" src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($product->featured_img) }}" alt="{{ __($product->name) }}">
												@else
													<img class="img-fit lazyload mx-auto" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
												@endif
											</a>
										</div>

										<div class="p-md-3 p-2">
											<div class="price-box text-center">
											 @php
                            			    $home_base_price = home_base_price($product->id);
                            			    $home_discounted_base_price = home_discounted_base_price($product->id);
                            			    @endphp
                            				@if($home_base_price != $home_discounted_base_price)
                            					<del class="old-product-price strong-400">{{ $home_base_price }} </del>
                            				  <span class="product-price strong-600">{{ $home_discounted_base_price }} <span style="color:red;font-size:14px;">({{discount_calulate($home_base_price, $home_discounted_base_price )}}% off)</span></span>
                                            @else
                            				<span class="product-price strong-600">{{ $home_discounted_base_price }} </span>
                            				@endif
											</div>
											<!-- <div class="star-rating star-rating-sm mt-1">
												{{ renderStarRating($product->rating) }}
											</div>-->
											<h2 class="product-title p-0 text-truncate-2 text-center">
												<a href="{{ route('product', $product->slug) }}">{{ __($product->name) }}</a>
											</h2>

											@if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated)
												<div class="club-point mt-2 bg-soft-base-1 border-light-base-1 border">
													{{ __('Club Point') }}:
													<span class="strong-700 float-right">{{ $product->earn_point }}</span>
												</div>
											@endif
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>
	@endforeach
	<section class="slice-sm footer-top-bar bg-white d-none d-lg-block" id="alde_footer">
		<div class="container-fluid sct-inner">
			<div class="row no-gutters ">
				<div class="col-12" style="padding: 1rem 0;">
					{!! \App\Category::find($CategoryID)->content !!}
				</div>
			</div>
		</div>
	</section>
@endsection

@section('script')
	
@endsection
