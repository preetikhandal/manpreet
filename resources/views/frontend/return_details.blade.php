@extends('frontend.layouts.app')

@section('content')
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
    
    background:#20b34e!important;
   
}
input[type="checkbox"][readonly] {
  pointer-events: none;
}
</style>
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @if(Auth::user()->user_type == 'seller')
                        @include('frontend.inc.seller_side_nav')
                    @elseif(Auth::user()->user_type == 'customer')
                        @include('frontend.inc.customer_side_nav')
                    @endif
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="row mb-2">
                            <div class="col-12 p-0">
                                <div class="page-title bg-blue p-3" style="border-radius: 0.25rem; ">
                                    <div class="row align-items-center">
                                        <div class="col-md-6 col-12">
                                            <h2 class="heading heading-6 text-capitalize strong-600 mb-0 text-white p-0">
                                                {{__('Order Details')}}
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @isset($order)
                            <div class="row">
                                <div class="card my-3">
                                    <div class="card-header py-2 px-3 heading-6 strong-600 clearfix">
                                        <div class="float-left">{{__('Order Summary')}}</div>
                                    </div>
                                    <div class="card-body pb-0">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <table class="details-table table">
                                                    <tr>
                                                        <td class="w-50 strong-600">{{__('Order Code')}}:</td>
                                                        <td>{{ $order->code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="w-50 strong-600">{{__('Customer')}}:</td>
                                                        <td>{{ json_decode($order->shipping_address)->name }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-lg-6">
                                                <table class="details-table table">
                                                    <tr>
                                                        <td class="w-50 strong-600">{{__('Order date')}}:</td>
                                                        <td>{{ date('d-m-Y H:m A', $order->date) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="w-50 strong-600">{{__('Total order amount')}}:</td>
                                                        <td>{{ single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax')) }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @php $returnItem = 0; @endphp
                                @foreach ($order->orderDetails as $key => $orderDetail) 
                                    @php
                                        $status = $orderDetail->delivery_status;
                                        $rem_return_days=\Carbon\Carbon::createFromTimestamp( $order->date)->diffInDays(Carbon\Carbon::now()->addDays(1));
                                        $ReturnProduct=\App\ReturnProduct::where('order_id',$orderDetail->order_id)->where('product_id',$orderDetail->product_id)->first();
                                    @endphp
                                    <input type="hidden" id="order_id" value="{{$order->id}}"/>
                                    <article class="card card-product-list">
                                    	<div class="row no-gutters align-items-center">
                                    		<div class="col-md-2 col-3">
                                    			<a href="{{ route('product', $orderDetail->product->slug) }}" >
                                			    	@if($orderDetail->product->thumbnail_img!="")
                                    			    	<img src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($orderDetail->product->thumbnail_img) }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                                    			    @else
                                    			        <img src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                                    			    @endif
                                    			</a>
                                    		</div> 
                                    		<div class="col-md-7 col-9 ">
                                    			<div class="info-main">
                                    			    @if ($orderDetail->product != null)
                                    			    	<a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank" class="h5 title">{{ $orderDetail->product->name }}</a>
                                    		        @else
                                                        <strong>{{ __('Product Unavailable') }}</strong>
                                                    @endif
                                    		        <p class="info">Order 
                                    		            <a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="text-blue font-weight-bold"># {{ $order->code }}</a> 
                                    		            <span class="block-sm">Delivery Status
                                        		            @php
                                                                $status = $order->orderDetails->first()->delivery_status;
                                                            @endphp
                                                            @if(str_replace('_', ' ', $status)=="pending")
                                                                <span class="badge badge-info"> {{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                                                            @else
                                                                <span class="badge badge-success"> {{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                                                            @endif
                                    		            </span>
                                    		        </p>
                                    		        <p class="info">Order Date <b class="text-blue">{{ date('d-m-Y', $order->date) }} </b>
                                    		           <span class="block-sm">Payment Status 
                                    		             @if ($order->payment_status == 'paid')
                                    		                <span class="badge badge-success"> {{__('Paid')}}</span>
                                                         @else
                                                            <span class="badge badge-warning">{{__('Unpaid')}}</span>
                                                         @endif
                                    		           </span>
                                    		        </p>
                                    		        <span class="price h5 d-block d-md-none">Product Price {{ single_price($orderDetail->price) }} </span>
                                		            @if($orderDetail->product->category->return_days==null || $rem_return_days > $orderDetail->product->category->return_days)
                                    		            <span class="badge badge-primary">{{__('Return Not Applicable')}}</span>
                                    		        @elseif($ReturnProduct!=null)
                                    		             <span class="badge badge-primary"> {{ ucfirst(str_replace('_', ' ', $ReturnProduct->status)) ??'NA' }}</span>
                                                    @else
                                                       @php $returnItem++; @endphp
                                                        {{__('Within '.$orderDetail->product->category->return_days.' days of Delivery ')}}
                                                        <div class="custom-control custom-checkbox">
                                                          <input type="checkbox" class="custom-control-input" id="check{{$orderDetail->product_id}}" value="{{$orderDetail->product_id}}">
                                                          <label class="custom-control-label" for="check{{$orderDetail->product_id}}">Apply for Return</label>
                                                        </div>
                                    		        @endif
                                    			</div>
                                    		</div> 
                                    		<!--<div class="col-12 d-block d-md-none mb-2 px-2 mobilebtn">
                                    		    <a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="btn  btn-light btn50 float-left bg-blue"> Details </a>
                            		            <a href="{{ route('customer.invoice.download', $order->id) }}" class="btn btn-light bg-red btn50">Download Invoice</a>
                                    		</div>-->
                                    		<div class="col-md-3 d-none d-md-block">
                                    			<div class="info-aside">
                                    				<div class="price-wrap">
                                    					<span class="price h5"><span class="info">Product Price</span> {{ single_price($orderDetail->price) }} </span>	
                                    				</div> <br/>
                                    				<!--<p class="d-none d-md-block">
                                    					<a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})" class="btn btn-light bg-blue btn-block">View Details </a>
                                    					<a href="{{ route('customer.invoice.download', $order->id) }}" class="btn btn-light btn-block bg-red">Download Invoice</a>
                                    				</p>-->
                                    			</div> 
                                    		</div> 
                                    	</div> 
                                    </article>
                                @endforeach    
                            </div>
                            <div class="row">
                                <div class="col-12 ml-md-auto col-md-6">
                                    @if($returnItem > 0)
                                    <div class="input-group mb-3">
                                      <input type="text" class="form-control nodata" name="reason" id="reason" required placeholder="Reason of Return" aria-label="Reason of Return" aria-describedby="send_all">
                                      <div class="input-group-append">
                                        <span class="btn btn-styled btn-base-1 input-group-text nodata" id="send_all">{{__('Apply')}}</span>
                                      </div>
                                    </div>
                                    @else
                                    <center><p>No item(s) avaliable for return.</p></center>
                                    @endif
                                </div>
                            </div>
                        @endisset 
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script type="text/javascript">
$(document).ready(function () { 
    var total=$('input[type=checkbox][readonly!=readonly]').length;
    /*if(total==1){
           $(".nodata").hide();
       }*/
       
    $("#selectAll").click(function() {
        $("input[type=checkbox]").prop("checked", $(this).prop("checked"));
         $("input[readonly=readonly]").prop("checked", false);
    });

    $("input[type=checkbox]").click(function() {
        var total_c=$('input[type=checkbox]:checked').length;
        
       // alert(total+" ~ "+total_c);
       
        if(total_c==total-1){
             $("#selectAll").prop("checked", true);
        }
        if (!$(this).prop("checked")) {
            $("#selectAll").prop("checked", false);
        }
    });
    
     $("#send_all").click(function() {
         var data=[];
      
        $('input[type=checkbox]:checked').each(function () {
            var status = (this.checked ? $(this).val() : "");
            var id = $(this).attr("id");
           // $('#output').append("<h3>" + id + " : " + status + "</h3>");
           if(id!='selectAll'){
                data.push(status);
           }
           
            });
           
        var order_id=$("#order_id").val();
        var return_reason=$("#reason").val();
            //alert(data+" ~ "+order_id);
            $.post('{{ route('customer_all_refund_request') }}', {_token: '{{ csrf_token()}}', product_id:data,order_id:order_id,return_reason:return_reason}, function(data){
                     // $(".nodata").hide();
                       location.reload();
           });
     });
});
    </script>

@endsection