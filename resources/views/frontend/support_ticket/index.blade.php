@extends('frontend.layouts.app')

@section('content')
<!-- Seller Style -->
<link type="text/css" href="{{ asset('frontend/css/seller.css') }}" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" @if($mobileapp == 1) style="margin-top:75px" @endif>
        <div class="container-fluid">
        <div class="row cols-xs-space cols-sm-space cols-md-space">
            <div class="col-lg-3 d-none d-lg-block">
                @if(Auth::user()->user_type == 'seller')
                    @include('frontend.inc.seller_side_nav')
                @elseif(Auth::user()->user_type == 'customer')
                    @include('frontend.inc.customer_side_nav')
                @endif
            </div>

            <div class="col-lg-9">
                <div class="main-content">
                    <!-- Page title -->
                    <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <h2 class="heading text-white p-0 heading-6 text-capitalize strong-600 mb-0">
                                    {{__('Support Ticket')}}
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 offset-md-4">
                            <div class="dashboard-widget text-center plus-widget mt-4 c-pointer" data-toggle="modal" data-target="#ticket_modal">
                                <i class="la la-plus"></i>
                                <span class="d-block title heading-6 strong-400 c-base-1">{{ __('Create a Ticket') }}</span>
                            </div>
                        </div>
                    </div>
                     @if(count($tickets) > 0)
                        @foreach ($tickets as $key => $ticket)
                            <article class="card card-product-list mt-3">
                            	<div class="row no-gutters align-items-center">
                            		<div class="col-12">
                            			<div class="info-main ">
                            		        <p class="info">Ticket Code: 
                            		            <a href="{{route('support_ticket.show', encrypt($ticket->id))}}" class="text-blue h5 font-weight-bold"># {{ $ticket->code }}</a> 
                            		            <p class="block-sm"><span class="h6">Subject:</span> {{ $ticket->subject }}</p>
                            		            <span class="block-sm"><span class="h6">Status:</span>
                                		           @if ($ticket->status == 'pending')
                                                        <span class="badge badge-pill badge-danger">Pending</span>
                                                    @elseif ($ticket->status == 'open')
                                                        <span class="badge badge-pill badge-secondary">Open</span>
                                                    @else
                                                        <span class="badge badge-pill badge-success">Solved</span>
                                                    @endif
                            		            </span>
                            		        </p>
                            		        <p class="info">Created at <b class="text-blue">{{ $ticket->created_at }} </b></p>
                            		        <p class="mt-2">
                            		         <a href="{{route('support_ticket.show', encrypt($ticket->id))}}" class="btn btn-light bg-blue">View Details <i class="la la-angle-right text-sm"></i> </a>
                            		        </p>
                            			</div>
                            		</div> 
                            	</div> 
                            </article>
                         @endforeach
                    @else
                        <div class="card no-border mt-4">
                            <div>
								<table class="table table-sm table-hover table-responsive-md">
                                    <thead>
                                        <tr class="text-center">
                                            <td class="text-center pt-5 h4" colspan="100%">
                                                <i class="la la-meh-o d-block heading-1 alpha-5"></i>
                                                <span class="d-block">{{ __('No history found.') }}</span>
                                            </td>
                                        </tr>
                                    </thead>
								</table>
							</div>
						</div>
                    @endif
 
                    <div class="pagination-wrapper py-4">
                        <ul class="pagination justify-content-end">
                            {{ $tickets->links() }}
                        </ul>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>

<div class="modal fade" id="ticket_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
        <div class="modal-content position-relative">
            <div class="modal-header">
                <h5 class="modal-title strong-600 heading-5">{{__('Create a Ticket')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-3 pt-3">
                <form class="" action="{{ route('support_ticket.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Subject <span class="text-danger">*</span></label>
                        <input type="text" class="form-control mb-3" name="subject" placeholder="Subject" value="{{old('subject')}}" required>
                    </div>
                    <div class="form-group">
                        <label>Provide a detailed description <span class="text-danger">*</span></label>
                        <textarea class="form-control editor" name="details" placeholder="Type your reply" data-buttons="bold,underline,italic,|,ul,ol,|,paragraph,|,undo,redo">{{old('details')}}</textarea>
                    </div>
                    <div class="form-group">
                        <input type="file" name="attachments[]" id="file-2" class="custom-input-file custom-input-file--2" data-multiple-caption="{count} files selected" multiple accept="image/x-png,image/jpg,image/jpeg,application/pdf" />
                        <label for="file-2" class=" mw-100 mb-0">
                            <i class="fa fa-upload"></i>
                            <span>Attach files.</span>
                        </label>
                        <span class="err text-danger"></span>
                    </div>
                    <div class="text-right mt-4">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('cancel')}}</button>
                        <button type="submit" class="btn btn-base-1 uploadButton">{{__('Send Ticket')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
@if(old('details') != '')
$('#ticket_modal').modal('show');
@endif
</script>
<script type="text/javascript">
	$(function(){
		$(".uploadButton").click(function(){
			var msg=$(".editor").val().trim();
			if(msg==""){
				$(".err").text("Please type your reply.");
				return false;
			}
			else{
				$(".err").text("");
			}
		});
	})
	$('INPUT[type="file"]').change(function () {
		var ext = this.value.match(/\.(.+)$/)[1];
		ext= ext.toLowerCase();
		var FileSize = this.files[0].size / 1024 / 1024; // in MB
		if (FileSize > 4) {
			$(".err").text('Max file size: 4MB');
			this.value = '';
		}
		switch (ext) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			case 'pdf':
				$('.uploadButton').attr('disabled', false);
				break;
			default:
				$(".err").text('This is not an allowed file type. Only jpg/png/pdf files are allowed to upload');
				this.value = '';
		}
	});
</script>
@endsection
