@php

$home_base_price = home_base_price($product->id);
$home_discounted_base_price = home_discounted_base_price($product->id);

if($home_base_price == "₹ 0.00") {
    $product->current_stock = 0;
    $qty = 0;
}

@endphp

@if($product->current_stock)
 
<article class="list-product slide"  >
	<div class="img-block text-center">
	     
		<a href="{{ route('product', $product->slug) }}" class="thumbnail">
		    @if($product->thumbnail_img != null)
		    
			<img class="first-img  img-fluid swiper-lazy" src="{{ asset($product->thumbnail_img) }}" data-src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" />
			@else
			
		    <img class="first-img  img-fluid swiper-lazy" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
			@endif
		</a>
	</div>
	@if($product->current_stock != 0)
    	@if($home_base_price != $home_discounted_base_price)
    	<ul class="product-flag">
    		<li class="new discount-price bg-orange">{{discount_calulate($product->id,$home_base_price, $home_discounted_base_price )}}% off</li>
    	</ul>
    	@endif
	@endif
	
	<div class="product-decs text-center">
		<h2><a href="{{ route('product', $product->slug) }}" class="product-link"> {!! Str::limit($product->name, 58, ' ...') !!}</a></h2>
		<div class="pricing-meta">
			<ul>
			    @if($product->current_stock == 0)
			    <li class="current-price">&nbsp; &nbsp; &nbsp; &nbsp; </li>
			    @else
    			    @if($home_base_price != $home_discounted_base_price)
    				<li class="old-price">{{ $home_base_price }}</li>
    				@endif
    				<li class="current-price">{{ $home_discounted_base_price }}</li>
				@endif
			</ul>
		</div>
	</div>

	<div class="add-to-cart-buttons">
	    @if($product->current_stock == 0)
	    <button class="btn btn95" type="button" disabled> Out of Stock</button>
	    @else
	    <button class="btn btn25" onclick="addToCart(this, {{$product->id}}, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
	    <button class="btn btn70" onclick="buyNow(this, {{ $product->id }}, 1, 'full')" type="button"> Buy Now</button>
	    @endif
	</div>
</article>

@endif