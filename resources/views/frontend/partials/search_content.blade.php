<div class="keyword">
    @if (sizeof($keywords) > 0)
        <div class="title">{{__('Popular Suggestions')}}</div>
        <ul>
            @foreach ($keywords as $key => $keyword)
                <li><a href="{{ route('suggestion.search', $keyword) }}">{{ $keyword }}</a></li>
            @endforeach
        </ul>
    @endif
</div>
<div class="category">
    @if (count($subsubcategories) > 0)
        <div class="title">{{__('Category Suggestions')}}</div>
        <ul>
            @foreach ($subsubcategories as $key => $subsubcategory)
                @php
                if(env('CACHE_DRIVER') == 'redis') {
					$SubCategory = Cache::tags(['SubCategory'])->rememberForever('SubCategory:'.$subsubcategory->sub_category_id, function () use ($subsubcategory) {
                            return  \App\SubCategory::find($subsubcategory->sub_category_id);
                    });
					$Category = Cache::tags(['Category'])->rememberForever('Category:'.$SubCategory->category_id, function () use ($SubCategory) {
                            return  \App\Category::where('id', $SubCategory->category_id)->first();
                    });
                } else {
                    $SubCategory = Cache::rememberForever('SubCategory:'.$subsubcategory->sub_category_id, function () use ($subsubcategory) {
                            return  \App\SubCategory::find($subsubcategory->sub_category_id);
                    });
					$Category = Cache::rememberForever('Category:'.$SubCategory->category_id, function () use ($SubCategory) {
                            return  \App\Category::where('id', $SubCategory->category_id)->first();
                    });
                }
				@endphp
                <li><a href="{{ route('products.subsubcategory', [$Category->slug, $SubCategory->slug, $subsubcategory->slug]) }}">{{ __(ucwords(strtolower($subsubcategory->name))) }} in {{ __(ucwords(strtolower($Category->name))) }}</a></li>
            @endforeach
        </ul>
    @endif
</div>
<div class="product">
    @if (count($products) > 0)
        <div class="title">{{__('Products')}}</div>
        <ul>
            @foreach ($products as $key => $product)
                <li>
                    <a href="{{ route('product', $product->slug) }}">
                        <div class="d-flex search-product align-items-center">
                            @if($product->thumbnail_img != null)
                            <div class="image" style="background-image:url('{{ asset($product->thumbnail_img) }}');">
                            @else
							<div class="image" style="background-image:url('{{ asset('frontend/images/product-thumb.png') }}');">
							@endif
                            </div>
                            <div class="w-100 overflow--hidden">
                                <div class="product-name text-truncate">
                                    {{ __($product->name) }} 
                                    @if($product->salt != "")
                                     <br /><span style="font-size:12px"> ({{$product->salt}}) </span>
                                    @endif
                                </div>
                                @php 
                                        $home_base_price = home_base_price($product->id);
                                        $home_discounted_base_price = home_discounted_base_price($product->id);
                                        if($home_base_price == "₹ 0.00") {
                                            $product->current_stock = 0;
                                            $qty = 0;
                                        } else {
                                            $qty = 100;
                                        }
                                        $discounted_per = discount_calulate($product->id,$home_base_price, $home_discounted_base_price);
                                        @endphp
                                        
                                <div class="clearfix">
                                    <div class="price-box float-left">
                                        @if($qty == 0)
                                        <span class="product-price strong-600"></span>
                                        @else
                                        @if($home_base_price != $home_discounted_base_price)
                                            <del class="old-product-price strong-400">{{ $home_base_price }}</del>
                                        @endif
                                        <span class="product-price strong-600">{{ $home_discounted_base_price }}</span>
                                        
                                        @if($home_base_price != $home_discounted_base_price)
                                            <span class="product-price strong-200" style="font-size:14px; color:red">&nbsp; {{$discounted_per}}% off</span>
                                        @endif
                                        @endif
                                    </div>
                                    {{-- <div class="stock-box float-right">
                                        @php
                                            $qty = 0;
                                            foreach (json_decode($product->variations) as $key => $variation) {
                                                $qty += $variation->qty;
                                            }
                                        @endphp
                                        @if(count(json_decode($product->variations, true)) >= 1)
                                            @if ($qty > 0)
                                                <span class="badge badge-pill bg-green">{{__('In stock')}}</span>
                                            @else
                                                <span class="badge badge badge-pill bg-red">{{__('Out of stock')}}</span>
                                            @endif
                                        @endif
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            @endforeach
        </ul>
    @endif
</div>
<div class="category">
    @if (count($brands) > 0)
        <div class="title ">{{__('Brand Suggestions')}}</div>
        <ul>
            @foreach ($brands as $key => $brand)
                <li><a href="{{ route('categories.all', ['brand'=>$brand->slug]) }}" >{{ __($brand->name) }}</a></li>
            @endforeach
        </ul>
    @endif
</div>
@if(\App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
    <div class="product">
        @if (count($shops) > 0)
            <div class="title">{{__('Shops')}}</div>
            <ul>
                @foreach ($shops as $key => $shop)
                    <li>
                        <a href="{{ route('shop.visit', $shop->slug) }}">
                            <div class="d-flex search-product align-items-center">
                                <div class="image" style="background-image:url('{{ asset($shop->logo) }}');">
                                </div>
                                <div class="w-100 overflow--hidden ">
                                    <div class="product-name text-truncate heading-6 strong-600">
                                        {{ $shop->name }}

                                        <div class="stock-box d-inline-block">
                                            @if($shop->user->seller->verification_status == 1)
                                                <span class="ml-2"><i class="fa fa-check-circle" style="color:green"></i></span>
                                            @else
                                                <span class="ml-2"><i class="fa fa-times-circle" style="color:red"></i></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="price-box alpha-6">
                                        {{ $shop->address }}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
@endif
