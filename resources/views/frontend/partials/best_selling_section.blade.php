@php
	if (Cache::has('best_selling')){
	   $best_selling =  Cache::get('best_selling');
	} else {
		$best_selling = \App\BestSelling::orderBy('updated_at', 'desc')->limit(20)->get();
		Cache::forever('best_selling', $best_selling,120);
	}
@endphp

@if(count($best_selling)!=0)
<section class="mb-2 aldeproductslider">
    <div class="container-fluid bg-white py-2 py-md-0"  style="overflow:hidden">
        <div class="row d-block d-lg-none">
			<div class="col-8 float-left">
			
					<h3 class="heading-5 strong-700 mb-0 float-left">
						<span class="mr-4">{{__('Best Selling')}}</span>
					</h3>
			
			</div>
			<div class="col-4 float-left">
				<a href="{{route('best-selling')}}" class="btn text-white" style="background:#232F3E;">View All</a>
			</div>
		</div>

		<div class="row">
            <div class="col-lg-2 stickblock d-none d-lg-block" style="z-index:10; @if($best_selling[0]->background_color) background: {{ $best_selling[0]->background_color }} @else background: #FF0909 @endif">
				
				<h2 style="@if($best_selling[0]->text_color) color: {{ $best_selling[0]->text_color }} @else color: #fff @endif">{{__('Best Selling')}}</h2>
            	<a href="{{route('best-selling')}}">View All &raquo;</a>
            </div>
            <div class="col-lg-10 pt-1 px-0">
                <div class="best-sell-slider-22" >
            <div class="swiper-wrapper" id="bestSellings">
                   @foreach($best_selling as $key => $bestSelling)
                   	@foreach(\App\Product::where('published', 1)->whereIn('id', json_decode($bestSelling->products))->get() as $key => $product)
                        @php
                        if(\App\Product::select('id')->where('id', $product->id)->first() == null) {
                            continue;
                        }
                        @endphp
                        @include('frontend.partials.product_slider_block', ['product' => $product])
    						
    				@endforeach
                    @endforeach
				</div>
				 <!-- If we need navigation buttons -->
				
                <!--<div class="best_selling_slider_swiper-button-prev swiper-button-prev d-none d-md-block"></div>-->
                <!--<div class="best_selling_slider_swiper-button-next swiper-button-next d-none d-md-block"></div>-->
    		    </div>
    		</div>   
		</div>
	</div>
</section>
	<script>
	$(document).ready(function() {
	 setCarousel('best_selling_slider');
	});
		$('#bestSellings').slick({
  autoplay:true,
  autoplaySpeed:2000,
  arrows:true,
  prevArrow:'<button type="button" class="slick-prev"></button>',
  nextArrow:'<button type="button" class="slick-next"></button>',
  centerMode:true,
  slidesToShow:5,
  slidesToScroll:5,
  focusOnSelect: false,
   responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
  });
	</script>
@endif
