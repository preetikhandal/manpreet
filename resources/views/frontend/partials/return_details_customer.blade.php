<div class="modal-header">
    <h5 class="modal-title strong-600 heading-5">{{__('Refund Tracking')}}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

@php
    $status = $OrderDetail->delivery_status;
  
@endphp

<div class="modal-body gry-bg px-3 pt-0">
    <div class="pt-4">
        <ul class="ml-auto process-steps clearfix mr-auto">
            <li @if($status == 'processing_refund') class="active" @else class="done" @endif>
                <div class="icon">1</div>
                <div class="title">{{__('Refund Under Process')}}</div>
            </li>
         
            <li @if($status == 'refunded') class="done" @endif>
                <div class="icon">2</div>
                <div class="title">{{__('Refund Successfull')}}</div>
            </li>
        </ul>
    </div>
</div>


