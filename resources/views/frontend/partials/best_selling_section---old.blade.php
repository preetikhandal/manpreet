@if (\App\BusinessSetting::where('type', 'best_selling')->first()->value == 1)
    <section class="mb-2">
        <div class="container-fluid bg-white">
			<div class="row myrow">
				<div class="col-12">
					<div class="section-title-1 clearfix">
						<h3 class="heading-5 strong-700 mb-0 float-left">
							<span class="mr-4">{{__('Best Selling')}}</span>
						</h3>
						<ul class="inline-links float-right">
							<li><a  class="active">{{__('Top 20')}}</a></li>
						</ul>
					</div>
				</div>
			</div>
            <div class="px-2 py-3 p-md-3">
                <div class="caorusel-box arrow-round gutters-5">
                    <div class="slick-carousel" data-slick-items="3" data-slick-lg-items="2"  data-slick-md-items="2" data-slick-sm-items="1" data-slick-xs-items="1" data-slick-rows="2">
						@php
							if (Cache::has('best_selling')){
							   $best_selling =  Cache::get('best_selling');
							} else {
								$best_selling = filter_products(\App\Product::where('published', 1)->orderBy('num_of_sale', 'desc'))->limit(20)->get();
								Cache::add('best_selling', $best_selling,120);
							}
						@endphp
                        @foreach ($best_selling as $key => $product)
                            <div class="caorusel-card my-1">
                                <div class="row no-gutters product-box-2 align-items-center">
                                    <div class="col-md-5 col-6">
                                        <div class="position-relative overflow-hidden h-100">
                                            <a href="{{ route('product', $product->slug) }}" class="d-block product-image h-100">
												@if($product->thumbnail_img!=NULL)
													<img class="img-fit lazyload mx-auto" src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}">
												@else
													<img class="img-fit lazyload mx-auto" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
												@endif
                                            </a>
                                            <div class="product-btns">
                                                <button class="btn add-wishlist" title="Add to Wishlist" onclick="addToWishList({{ $product->id }})">
                                                    <i class="la la-heart-o"></i>
                                                </button>
                                                <!--<button class="btn add-compare" title="Add to Compare" onclick="addToCompare({{ $product->id }})">
                                                    <i class="la la-refresh"></i>
                                                </button>-->
                                                <button class="btn quick-view" title="Quick view" onclick="showAddToCartModal({{ $product->id }})">
                                                    <i class="la la-eye"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-7 border-left">
                                        <div class="p-3">
                                            <h2 class="product-title mb-0 p-0  text-truncate-2">
                                                <a href="{{ route('product', $product->slug) }}">{{ __(ucwords(strtolower($product->name))) }}</a>
                                            </h2>
                                            <!--<div class="star-rating star-rating-sm mb-2">
                                                {{ renderStarRating($product->rating) }}
                                            </div>-->
                                            <div class="clearfix">
                                                <div class="price-box float-left">
                                                    @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                                        <del class="old-product-price strong-400">{{ home_base_price($product->id) }}</del>
                                                    @endif
                                                    <span class="product-price strong-600">
                                                        {{ home_discounted_base_price($product->id) }}
                                                    </span>
                                                </div>
                                                <div class="float-right">
                                                    <button class="add-to-cart btn" title="Add to Cart" onclick="showAddToCartModal({{ $product->id }})">
                                                        <i class="la la-shopping-cart"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
