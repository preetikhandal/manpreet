<div class="modal-header bg-blue">
    <h5 class="modal-title strong-600 heading-5">{{__('Order id')}}: {{ $order->code }}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

@php
    $status = $order->orderDetails->first()->delivery_status;
    $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
@endphp

<div class="modal-body gry-bg px-1 px-md-3 pt-0">
    <div class="pt-4">
        <ul class="process-steps clearfix">
            <li @if($status == 'pending') class="active" @else class="done" @endif>
                <div class="icon">1</div>
                <div class="title">{{__('Order placed')}}</div>
            </li>
            <li @if($status == 'on_review') class="active" @elseif($status == 'on_delivery' || $status == 'delivered') class="done" @endif>
                <div class="icon">2</div>
                <div class="title">{{__('On review')}}</div>
            </li>
            <li @if($status == 'on_delivery') class="active" @elseif($status == 'delivered') class="done" @endif>
                <div class="icon">3</div>
                <div class="title">{{__('On delivery')}}</div>
            </li>
            <li @if($status == 'delivered') class="done" @endif>
                <div class="icon">4</div>
                <div class="title">{{__('Delivered')}}</div>
            </li>
        </ul>
    </div>
    <div class="card mt-4">
        <div class="card-header py-2 px-3 heading-6 strong-600 clearfix">
            <div class="float-left">{{__('Order Summary')}}</div>
        </div>
        <div class="card-body pb-0">
            <div class="row">
                <div class="col-lg-6 px-0">
                    <table class="details-table table">
                        <tr>
                            <td class="w-50 strong-600">{{__('Order Code')}}:</td>
                            <td>{{ $order->code }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{__('Customer')}}:</td>
                            <td>{{ json_decode($order->shipping_address)->name }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{__('Email')}}:</td>
                            @if ($order->user_id != null)
                                <td>{{ $order->user->email }}</td>
                            @endif
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{__('Shipping address')}}:</td>
                            <td>{{ json_decode($order->shipping_address)->address }}, {{ json_decode($order->shipping_address)->city }}, {{ json_decode($order->shipping_address)->postal_code }}, {{ json_decode($order->shipping_address)->country }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-6 px-0">
                    <table class="details-table table">
                        <tr>
                            <td class="w-50 strong-600">{{__('Order date')}}:</td>
                            <td>{{ date('d-m-Y H:m A', $order->date) }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{__('Order status')}}:</td>
                            <td>{{ ucfirst(str_replace('_', ' ', $status)) }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{__('Total order amount')}}:</td>
                            <td>{{ single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax')) }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{__('Shipping method')}}:</td>
                            <td>{{__('Flat shipping rate')}}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{__('Payment method')}}:</td>
                            <td>{{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <style>
    .detailsdiv .info-aside {
        width: 55%;
    }
    </style>
    <div class="row">
        <div class="col-lg-8">
            <div class="card my-3 detailsdiv">
                <div class="card-header py-2 px-3 heading-6 strong-600">{{__('Order Details')}}
                    <div class="info-aside float-right">
                         @if($status=='delivered')
                            <form action="{{ route('return.details') }}" method="post">
                                @csrf
                                <input type="hidden" value="{{$order->id}}" name="order_id"/>
                                <button type="submit" class="btn btn-light bg-blue float-right text-white">{{__('Apply for Return')}}</button>
                            </form>
                         @elseif ($status == "pending_confirmation" || $status == "pending" || $status == "ready_to_ship")
                            
                                 
                                <form action="{{ route('purchase_history.order_cancel') }}" method="post">
                                    @csrf <input type="hidden" value="{{$order->id}}" name="order_id"/>
                                    <button type="submit" class="btn btn-light bg-blue float-right text-white">{{__('Cancel Order')}}</button>
                                </form>
                                 
                               
                        @endif
    			    	<a href="{{ route('customer.invoice.download', $order->id) }}" class="btn btn-light bg-red float-right">Download Invoice</a>
    				</div>
				</div>
            </div>    
                @foreach ($order->orderDetails as $key => $orderDetail)
                	<article class="card card-product-list">
                    	<div class="row no-gutters align-items-center">
                    		<div class="col-md-2 col-3 ">
                    			<a href="{{ route('product', $orderDetail->product->slug) }}">
                			    	@if($orderDetail->product->thumbnail_img!="")
                    			    	<img src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($orderDetail->product->thumbnail_img) }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                    			    @else
                    			        <img src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ $orderDetail->product->name }}" class="img-fluid lazyload">
                    			    @endif
                    			</a>
                    		</div> 
                    		<div class="col-md-7 col-9 ">
                    			<div class="info-main ">
                    			    @if ($orderDetail->product != null)
                    			    	<a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank" class="h5 title">{{ $orderDetail->product->name }}</a>
                    		        @else
                                        <strong>{{ __('Product Unavailable') }}</strong>
                                    @endif
                                    <span class="qty">Qty {{ $orderDetail->quantity }}</span>
                    		        <span class="price h5 d-block d-md-none">{{ single_price($orderDetail->price) }} </span>
                    			</div>
                    		</div> 
                    		<div class="col-md-3 d-none d-md-block">
                    			<div class="info-aside" style="border-left: none">
                    				<div class="price-wrap">
                    					<span class="price h5"> {{ single_price($orderDetail->price) }} </span>	
                    				</div>
                    			</div> 
                    		</div> 
                    	</div> 
                    </article>
                @endforeach
        </div>
        <div class="col-lg-4">
            <div class="card mt-4">
                <div class="card-header py-2 px-3 heading-6 strong-600">{{__('Order Ammount')}}</div>
                <div class="card-body pb-0">
                    <table class="table details-table">
                        <tbody>
                            <tr>
                                <th>{{__('Subtotal')}}</th>
                                <td class="text-right">
                                    <span class="strong-600">{{ single_price($order->orderDetails->sum('price')) }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>{{__('Shipping')}}</th>
                                <td class="text-right">
                                    <span class="text-italic">{{ single_price($order->orderDetails->sum('shipping_cost')) }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>{{__('Tax')}}</th>
                                <td class="text-right">
                                    <span class="text-italic">{{ single_price($order->orderDetails->sum('tax')) }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>{{__('Coupon Discount')}}</th>
                                <td class="text-right">
                                    <span class="text-italic">{{ single_price($order->coupon_discount) }}</span>
                                </td>
                            </tr>
                            @if($order->cart_shipping > 0)
                            <tr>
                                <th>{{__('Delivery Charges')}}</th>
                                <td class="text-right">
                                    <span class="text-italic">{{ single_price($order->cart_shipping) }}</span>
                                </td>
                            </tr>
                            @endif
                            @if($order->wallet_credit > 0)
                            <tr>
                                <th>{{__('Wallet Credit Used')}}</th>
                                <td class="text-right">
                                    <span class="text-italic">{{ single_price($order->wallet_credit) }}</span>
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <th><span class="strong-600">{{__('Total')}}</span></th>
                                <td class="text-right">
                                    <strong><span>{{ single_price($order->grand_total) }}</span></strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @if ($order->manual_payment && $order->manual_payment_data == null)
                <button onclick="show_make_payment_modal({{ $order->id }})" class="btn btn-block btn-base-1">{{__('Make Payment')}}</button>
            @endif
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order cancel</h5>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          Warrning:- Do you want to cancel this order
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        
        <!--<button type="button" class="btn btn-primary">Confirm</button>-->
         <form action="{{ route('purchase_history.order_cancel') }}" method="post">
                @csrf <input type="hidden" id="cancelOrderId" name="order_id"/>
            <button type="submit" class="btn btn-light bg-blue float-right text-white">{{__('Confirm')}}</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--END MODEL-->




<script type="text/javascript">
    function show_make_payment_modal(order_id){
        $.post('{{ route('checkout.make_payment') }}', {_token:'{{ csrf_token() }}', order_id : order_id}, function(data){
            $('#payment_modal_body').html(data);
            $('#payment_modal').modal('show');
            $('input[name=order_id]').val(order_id);
        });
    }
</script>
