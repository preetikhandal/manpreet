<style>
	.font15{ font-size:15px;}
	.cart-area [class*="col-"]{
	    padding-left:5px;
	    padding-right:5px;
	}
	.shipmsg{
	    color:red;
	    font-size:20px;
	}
</style>
<div class="container-fluid cart-area cart-page-one">
   <div class="row outofstock" style="display:none;">
	          <div class="col-xl-12">
	              <div class="alert alert-danger">
	                  Some of items in your cart is not in stock, Remove that item(s) to proceed further.
	              </div>
	          </div>
	     </div>
	     
	      <div class="row lowstock"  style="display:none;">
	          <div class="col-xl-12">
	              <div class="alert alert-danger">
	                  Required stock not available, Descrese the quantity to proceed further.
	              </div>
	          </div>
	     </div>
	 @if(count(Session::get('cart')) > 0)
	<div class="row">
				<div class="col-xl-8">
					<div class="form-default bg-white px-3">
						<div class="row bg-blue py-2">
        					<div class="col-6 col-md-5 px-0 text-center font15">Product</div>
        					<div class="col-6 col-md-2 px-0 text-center font15">Price</div>
        					<div class="col-6 col-md-2 px-0 text-center d-none d-md-block font15">Qty</div>
        					<div class="col-6 col-md-3 px-0 text-center d-none d-md-block font15">Total</div>
        				</div>
						@php 
						  $cartCashBack = Session::get('cart_cashback_Product_subtotal');
						 
						 $stopProcessing = false;
						 $lowStock = false;
						 $outofstock = false;
						 
						$applyCoupe = Session::get('applycoupen');
						$subtotal = 0; $discount = 0; $coupon_category_discount = 0; $total = 0; $tax = 0; $shipping = 0;$getPercent=0; @endphp
						@foreach (Session::get('cart') as $key => $cartItem)
						     
							@php
							    
							    if(!isset($cartItem['coupon_category_discount'])) {
							        $cartItem['coupon_category_discount'] = 0;
							    }
							    
								$product = \App\Product::find($cartItem['id']);
								
								$categoryId = $product->category_id;
                        	    $subcategoryId = $product->subcategory_id;
                                $subsubcategory = $product->subsubcategory_id;
            		           if(isset($product->user_id)){
                        	    $sellerId = $product->user_id;
                        	}else{
                        	    $sellerId = 0;
                        	}
                            if(!empty($sellerId)){
                                    if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                                    $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
                                }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                                        $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
                                }else if(!empty($categoryId) && !empty($sellerId)){
                                     $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
                                }    
                            }


                                
                                if(isset($get_subcategory->commission) && $get_subcategory->commission_type==1){
                                    $exculsivePercent = $get_subcategory->commission;
                                }else{
                                    $exculsivePercent = 0; 
                                }
                               
                                if(!empty($exculsivePercent)){
                                      $getPercent = ($product->unit_price*$exculsivePercent)/100;
                                      $cartIteamPrice = $cartItem['price'];
                                }else{
                                   $cartIteamPrice = $cartItem['price'];
                                }
								
								$total = $total + $cartIteamPrice*$cartItem['quantity'];
								$subtotal += $cartIteamPrice*$cartItem['quantity'];
								$shipping += $cartItem['shipping']*$cartItem['quantity'];
								$discount += $cartItem['discount'] * $cartItem['quantity'];
								$product_discount = ($cartItem['coupon_category_discount'] + $cartItem['discount']) * $cartItem['quantity'];
								$coupon_category_discount += $cartItem['coupon_category_discount'] * $cartItem['quantity'];
								$product_name_with_choice = $product->name;
								
								
								if ($cartItem['variant'] != null) {
									$product_name_with_choice = $product->name.' - '.$cartItem['variant'];
								}
                                $min_purchased_quantity = $product->min_purchased_quantity;
                                if($min_purchased_quantity <= 1) {
                                    $min_purchased_quantity = 1;
                                }
                                $qty = 0;
                                if($product->variant_product){
                                    foreach ($product->stocks as $key => $stock) {
                                        $qty += $stock->qty;
                                    }
                                }
                                else{
                                    if($product->current_stock > -1) {
                                    $qty = $product->current_stock;
                                    } else {
                                    $qty = 100;
                                    }
                                }
							@endphp
							
							
							<div class="row product_row">
        						<div class="col-3 col-md-2 product-image px-2">
        							@if($product->thumbnail_img!=null)
        								<a href="{{ route('product', $product->slug) }}" target="_blank">
        									<img loading="lazy" alt="{{$product->name}}" src="{{ asset($product->thumbnail_img) }}" class="img-fluid">
        								</a>
        							@else
        								<a href="{{ route('product', $product->slug) }}" target="_blank"><img loading="lazy" alt="{{$product->name}}" src="{{asset('frontend/images/product-thumb.jpg')}}" class="img-fluid"/></a>
        							@endif
        						</div>
        						<div class="col-6 col-md-3 px-0">
        							<div class="product-name">
        								<span class="pr-2 d-block"><a href="{{ route('product', $product->slug) }}" target="_blank">{{ $product_name_with_choice }}</a></span>
        								<span class="shipping">
        									@if($cartItem['shipping']==0)
        									<span class="shippingdiv"></span>
        									@else
        										Shipping {{ single_price($shipping) }}
        									@endif
        								</span>
        							</div>
        						</div>
        						<div class="col-3 col-md-2 px-0 text-left text-md-center">
        							<div class="product-price"> 
        							@if($cartItem['discount'] != 0)
    							    <del style="color:red">{{ single_price($cartIteamPrice + $cartItem['tax'] + $cartItem['discount']) }}</del>  <br />
    							    @endif
        								{{ single_price($cartIteamPrice) }}
        							</div>
        						</div>
        						<div class="col-7 col-md-2 text-center">
        							<div class="product-qty float-right">
        								@if($cartItem['digital'] != 1)
        									<div class="input-group input-group--style-2" style="width: 120px;">
        										<span class="input-group-btn">
        											<button class="btn btn-number" type="button" data-type="minus" data-field="quantity[{{ $key }}]">
        												<i class="la la-minus"></i>
        											</button>
        										</span>
        										<input type="text" name="quantity[{{ $key }}]" class="form-control input-number" placeholder="1" value="{{ $cartItem['quantity'] }}"  min="{{$min_purchased_quantity}}"  max="{{$qty}}" onchange="updateQuantity({{ $key }}, this)">
        										<span class="input-group-btn">
        											<button class="btn btn-number" type="button" data-type="plus" data-field="quantity[{{ $key }}]">
        												<i class="la la-plus"></i>
        											</button>
        										</span>
        									</div>
        									@if($qty == 0)
        									<span style="color:red;">Not in-Stock, Remove item to proceed further.</span>
        									@php 
        									    $stopProcessing = true;
        									    $outofstock = true;
        									    @endphp
        									@elseif($cartItem['quantity'] > $qty)
        									    <span style="color:red;">Required stock not avalilable, Current avalilable Stock {{$qty}}</span>
        									    @php 
        									    $stopProcessing = true;
        									    $lowStock = true;
        									    @endphp
        									@endif
        								@endif
        							</div>
        						</div>
        						<div class="col-4 col-md-2 px-0 text-right product-tota">
        						    <div class="product-price">
        					        @if($cartItem['discount'] != 0)
        					        <del class="d-none d-md-block" style="color:red">{{ single_price(($cartIteamPrice + $cartItem['tax'] + $cartItem['discount']) *$cartItem['quantity']) }}</del>  
        					        @endif
        						    	{{ single_price(($cartIteamPrice+$cartItem['tax'])*$cartItem['quantity']) }}
        						    </div>
        						</div>
        						<div class="col-1 col-md-1 text-right">
        							<div class="product-remove">
        								<a href="#" onclick="removeFromCartView(event, {{ $key }})" class="text-left "><i class="la la-trash"></i></a>
        							</div>
        						</div>
        					</div>
						@endforeach
						<div class="row subtotal_row py-2 d-none d-md-block">
							<div class="col-md-6 float-left">
                                <a href="javascript:history.back(-2)" class="btn btn-styled btn-base-1 bg-blue">
                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                    {{__('Continue Shopping')}}
                                </a>
                            </div>
							<div class="col-md-6 float-right text-right">
								<span class="number-of-items">Subtotal ({{ count(Session::get('cart')) }} {{__('items')}}):</span>
								<span class="product-price">
									<span class="size-medium"><span class="currencyINR">&nbsp;&nbsp;</span>&nbsp;{{ single_price($subtotal) }}</span>
								</span>
							</div>
						</div>
					</div>
				</div>
				
						
				<div class="col-xl-4 ml-lg-auto">
					<div class="card sticky-top">
						<div class="card-title py-1 bg-blue">
							<div class="row subtotal_row pt-2 text-right d-block d-md-none">
								<div class="col-12">
									<div data-name="Subtotals" class="a-row a-spacing-mini sc-subtotal sc-subtotal-activecart sc-java-remote-feature">
										<span class="number-of-items">Subtotal ({{ count(Session::get('cart')) }} {{__('items')}}):</span>
										<span class="product-price">
											<span class="size-medium text-white"><span class="currencyINR">&nbsp;&nbsp;</span>&nbsp;{{ single_price($subtotal) }}</span>
										</span>
									</div>
								</div>
							</div>
							<div class="d-none d-md-block">
								<div class="row align-items-center">
									<div class="col-12">
										<h3 class="heading heading-3 strong-400 mb-0">
											<span class="text-white">{{__('Summary')}}</span>
										</h3>
									</div>
								</div>
							</div>
						</div>	
						<div class="card-title py-3">	
							
							<div class="row subtotal_row pt-2 text-right d-none d-md-block">
								<div class="col-12">
									<div data-name="Subtotals" class="a-row a-spacing-mini sc-subtotal sc-subtotal-activecart sc-java-remote-feature">
										<span class="number-of-items">Subtotal ({{ count(Session::get('cart')) }} {{__('items')}}):</span>
										<span class="product-price">
											<span class="size-medium"><span class="currencyINR">&nbsp;&nbsp;</span>&nbsp;{{ single_price($subtotal) }}</span>
										</span>
									</div>
								</div>
							</div>
							<table class="table-cart table-cart-review">
								<tfoot>
									<!--<tr class="cart-subtotal">
										<th>{{__('Subtotal')}}</th>
										<td class="text-right">
											<span class="strong-600 product-price">{{ single_price($subtotal) }}</span>
										</td>
									</tr>-->

									<tr class="cart-shipping">
										<th>{{__('Tax')}}</th>
										<td class="text-right">
											<span class="text-italic">{{ single_price($tax) }}</span>
										</td>
									</tr>
									<tr class="cart-shipping">
										<th>{{__('Product Shipping')}}</th>
										<td class="text-right">
											<span class="text-italic">{{ single_price($shipping) }}</span>
										</td>
									</tr>
									@php
									    if($coupon_category_discount > 0) {
									        session(['coupon_discount' => $coupon_category_discount]);
									    }
									@endphp
									
        							@if (Session::has('coupon_discount'))
        								<tr class="cart-shipping">
        									<th>{{__('Coupon Discount')}}</th>
        									<td class="text-right">
        										<span class="text-italic">(-) {{ single_price(Session::get('coupon_discount')) }}</span>
        									</td>
        								</tr>
        							@endif
									
									@php
        								$total = $subtotal+$tax+$shipping;
        								if(Session::has('coupon_discount')){
        									$total -= Session::get('coupon_discount');
        								}
        							@endphp
        							
            						@php
                                		if($total < env('FREE_CART_VALUE',0)) {
                                            session(['cart_shipping' => env('BELOW_FREE_CART_CHARGE',0)]);
                                        } else {
                                            session(['cart_shipping' => 0]);
                                        }
                                        $total += session('cart_shipping');

										 
            						@endphp
            						
            						@if(session('cart_shipping') > 0)
            						<tr class="cart-total">
										<th><span class="strong-600">{{__('Delivery Charges')}}</span></th>
										<td class="text-right">
											<strong><span class="product-price">(+) {{ single_price(session('cart_shipping')) }}</span></strong>
										</td>
									</tr>
									<script> $('.shippingdiv').html("Eligible for free shipping above ₹250 order value"); </script>
									@else
									<script> $('.shippingdiv').html("Eligible for free shipping"); </script>
            						@endif
									<tr class="cart-total">
										<th><span class="strong-600">{{__('Total')}}</span></th>
										<td class="text-right">
											<strong><span class="product-price">{{ single_price($total) }}</span></strong>
										</td>
									</tr>
								</tfoot>
							</table>
							@if (\App\BusinessSetting::where('type', 'coupon_system')->first()->value == 1)
								<div class="row">
								<div class="col-12">
								@if (Session::has('coupon_id'))
									<div class="mt-3">
										<form class="form-inline" action="{{ route('checkout.remove_coupon_code') }}" method="POST" enctype="multipart/form-data">
											@csrf
											
											@if($applyCoupe)
											<div class="form-group flex-grow-1  mb-0">
												<div class="form-control bg-gray w-100">{{ \App\Coupon::find(Session::get('coupon_id'))->code }}</div>
											</div>
											<button type="submit" class="btn btn-base-1" style="background:#131921">{{__('Change Coupon')}}</button>
											@endif
											
										</form>
									</div>
								@else
								    <!-- <div class="mt-3">
										<form class="form-inline" action="{{ route('checkout.apply_coupon_code') }}" method="POST" enctype="multipart/form-data">
											@csrf
											<div class="form-group flex-grow-1  mb-0">
												<input type="text" class="form-control w-100" name="code" placeholder="{{__('Have coupon code? Enter here')}}" required>
											</div>
											<button type="submit" class="btn btn-base-1" style="background:#131921">{{__('Apply')}}</button>
										</form>
									</div> -->
								@endif

								

								</div>
								</div>
							@endif
							<div class="row">
							<div class="clearfix">&nbsp;&nbsp;&nbsp;</div>
							<span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="
        ----------COD-----------</br>
                            1-499 Shipping Charge Rs.50 </br>
        					500-999 Shipping charge Rs.30</br></br>
        					------Online Payment------</br>
        					1-499 Shipping Charge Rs.50</br>
        					500 above shipping free!!</br>-">
               <button class="btn btn-primary" style="pointer-events: none;" type="button" disabled>To save you shipping charges click here</button>
       </span>
								</div>
							
								
								<div class="col-12 text-right my-3">
								    @if($stopProcessing == true)
								    	<button class="btn btn-styled btn-xs-block btn-base-1 bg-blue" disabled>{{__('Proceed to Buy')}}</button>
								    	<script>
								    	@if($lowStock)
								    	   $('.lowStock').slideDown();
								    	@else
								    	  $('.outofstock').slideDown();
								    	@endif
								    	</script>
								    @else
    									@if(Auth::check())
    										<a href="{{ route('checkout.shipping_info') }}" class="btn btn-xs-block btn-styled btn-base-1 bg-blue">{{__('Proceed to Buy')}}</a>
    									@else
    										<button class="btn btn-styled btn-xs-block btn-base-1 bg-blue" onclick="showCheckoutModal()">{{__('Proceed to Buy')}}</button>
    									@endif
									@endif
									<a href="javascript:history.back(-2)" class="btn btn-styled btn-base-1 d-md-none btn-sm-block btn-xs-block mt-2 bg-blue">
                                        <i class="fa fa-reply" aria-hidden="true"></i>
                                        {{__('Continue Shopping')}}
                                    </a>
								</div>
                                    
							</div>
    						
						</div>
					</div>
				</div>
	</div>
    @else
    <div class="row">
       <div class="col-md-12 py-3">
        	<h3 class="heading heading-6 strong-700 text-center">{{__('Your Cart is empty')}}</h3>
        </div>
        <div class="col-md-12 text-center">
            <a href="{{ route('home') }}" class="btn btn-styled btn-base-1">
                <i class="fa fa-reply" aria-hidden="true"></i>
                {{__('Return to shop')}}
            </a>
        </div>
	</div>
    @endif

</div>

<script type="text/javascript">
    cartQuantityInitialize();
</script>
