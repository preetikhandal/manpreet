@php
	if (Cache::has('featured_product')){
	   $featured_product =  Cache::get('featured_product');
	} else {
		 $featured_product = filter_products(\App\Product::where('published', 1)->where('featured', '1'))->get();
		Cache::forever('featured_product', $featured_product);
	}
@endphp
@if(count($featured_product)!=0)
 <style>
    .slick-slide img { margin: 0 auto; text-align: center; }
        .flash-deal-box .countdown .countdown-digit{
            background:#fff!important;
        }
        .pcflash .flash-deal-box .countdown .countdown-digit{
            background:#EB3038!important;
            color:#fff!important;
        }
        .slick-prev {     left: 0;
    background: url({{ asset('frontend/images/left-arrow.png') }}) #fff;
    background-repeat: no-repeat;
    background-position-y: 50%;
        background-position-x: 40%;}
        .slick-next { right: 0;   
    background: url({{ asset('frontend/images/right-arrow.png') }}) #fff;
    background-repeat: no-repeat;
        background-position-y: 50%;
    background-position-x: 40%; }
        .slick-prev, .slick-next {
            background-size: 16px;
            position: absolute; 
    top: calc(50% - 50px) !important;
    display: inline-block;
    font-size: 0;
    cursor: pointer;
    text-align: center;
    color: #000;
    border: 1px solid #ebebeb;
    z-index: 9;
    opacity: 1;
    border-radius: 3px 0 0 3px;
    -webkit-transition: all 300ms linear;
    -moz-transition: all 300ms linear;
    -ms-transition: all 300ms linear;
    -o-transition: all 300ms linear;
    transition: all 300ms linear;
    -webkit-transition: all 300ms linear;
    -moz-transition: all 300ms linear;
    -ms-transition: all 300ms linear;
    -o-transition: all 300ms linear;
    transition: all 300ms linear;
    height: 80px!important;
    line-height: 100px;
    width: 35px;
    box-shadow: 0 1px 3px #888;}
    </style>
<section class="mb-2 aldeproductslider">
    <div class="container-fluid bg-white py-2 py-md-0" style="overflow:hidden">
           <div class="row d-block d-lg-none">
			<div class="col-8 float-left">
				
					<h3 class="heading-5 strong-700 mb-0 float-left">
						<span class="mr-4">{{__('Featured Products')}}</span>
					</h3>
			
			</div>
			<div class="col-4 float-left">
				<a href="{{route('feature-product-list')}}" class="btn text-white" style="background:#232F3E;">View All</a>
			</div>
		</div>
        <div class="row">
            <div class="col-lg-2 stickblock d-none d-lg-block" style="background: #282563;z-index:10;">
                	
				<h2 style=" color: #fff">{{__('Featured Products')}}</h2>
				<a href="{{route('feature-product-list')}}">View All &raquo;</a>
			
            </div>
            <div class="col-lg-10 pt-1 px-0 pl-2">
            <div class=" best-sell-slider-22" >
            <div class="swiper-wrapper" id="featureSlider">
                @foreach ($featured_product as $key => $product)
					@php
                    if(\App\Product::select('id')->where('id', $product->id)->first() == null) {
                            continue;
                        }
                    @endphp
                    @include('frontend.partials.product_slider_block', ['product' => $product])
				@endforeach
				</div>
				 <!-- If we need navigation buttons -->
				
                <!--<div class="feature_slider_swiper-button-prev swiper-button-prev d-none d-md-block"></div>-->
                <!--<div class="feature_slider_swiper-button-next swiper-button-next d-none d-md-block"></div>-->
			</div>
			</div>
        </div>
    </div>
</section>
	<script>

	$('#featureSlider').slick({
  autoplay:true,
  autoplaySpeed:2000,
  arrows:true,
  prevArrow:'<button type="button" class="slick-prev"></button>',
  nextArrow:'<button type="button" class="slick-next"></button>',
  centerMode:true,
  margin:30,
  slidesToShow:5,
  slidesToScroll:5,
  focusOnSelect: false,
   responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
  });
	</script>

@endif