@php
	if (Cache::has('homeCategories')){
	   $homeCategories =  Cache::get('homeCategories');
	} else {
		 $homeCategories = \App\HomeCategory::where('status', 1)->orderBy('position')->get();
		Cache::forever('homeCategories', $homeCategories);
	}
@endphp

@foreach ($homeCategories as $homekey => $homeCategory)
	<?php if($homekey<=2) continue; ?>
    <?php // if($homekey==7) break; ?>
    <section class="mb-2 aldeproductslider">
        <div class="container-fluid bg-white"   style="overflow:hidden">
    		<div class="row d-block d-lg-none">
    			<div class="col-12">
    				<!--<div class="section-title-1 clearfix pb-1">
    					<h3 class="heading-5 strong-700 mb-0 float-left" style="color:#000;">
    						<span class="mr-4">{{ __($homeCategory->category->name) }}</span>
    					</h3>
    					<ul class="inline-links float-lg-right nav mt-3 mb-2 m-lg-0">	
    						<li class="active">
    							<a href="{{ route('products.category', $homeCategory->category->slug) }}" class="d-block active">{{ __('View All') }}</a>
    						</li>
    					</ul>
    				</div>-->
    				<div class="section-title-1 clearfix pb-1">
    					<div class="row">
    						<div class="col-8">
    							<h3 class="heading-6 strong-600 mb-0 float-left" style="color:#000;">
    							<span class="mr-4">{{ __($homeCategory->category->name) }}</span>
    							</h3>
    						</div>
    						<div class="col-4 float-left">
    							<a href="{{ route('products.category', $homeCategory->category->slug) }}" class="btn text-white" style="background:#232F3E;">{{ __('View All') }}</a>
    						</div>
    					</div>
    	        	</div>
    			</div>
    		</div>
    		<div class="row">
                <div class="col-lg-2 stickblock d-none d-lg-block" style="z-index:10;@if($homeCategory->category->background_color) background: {{ $homeCategory->category->background_color }} @else background: #282563 @endif">
    				<h2 style="@if($homeCategory->category->text_color) color: {{ $homeCategory->category->text_color }} @else color: #fff @endif">{{ __($homeCategory->category->name) }}</h2>
    				<a href="{{ route('products.category', $homeCategory->category->slug) }}">View All &raquo;</a>
                </div>
                <div class="col-lg-10 pt-1 px-0 pl-2">
                <div class="best-sell-slider-22" id="">
                <div class="swiper-wrapper" id="{{$homeCategory->category->slug}}_slider">
                    
                    @php
                    $home_category_products = Cache::rememberForever('HomeCategory:'.$homeCategory->category->slug, function () use ($homeCategory) {
                            return  filter_products(\App\Product::where('published', 1)->whereIn('id', json_decode($homeCategory->products)))->get();
                    });
                    @endphp
                       @foreach ($home_category_products as $key => $product)
                            @php
                            if(\App\Product::select('id')->where('id', $product->id)->first() == null) {
                            continue;
                            }
                            @endphp
                               @include('frontend.partials.product_slider_block', ['product' => $product])
        				@endforeach
				</div>
				 <!-- If we need navigation buttons -->
				
                <!--<div class="{{$homeCategory->category->slug}}_slider_swiper-button-prev swiper-button-prev d-none d-md-block"></div>-->
                <!--<div class="{{$homeCategory->category->slug}}_slider_swiper-button-next swiper-button-next d-none d-md-block"></div>-->
        			</div>
                </div>
            </div>
    	</div>
    </section>
	<script>
$('#{{$homeCategory->category->slug}}_slider').slick({
  autoplay:true,
  autoplaySpeed:2000,
  arrows:true,
  prevArrow:'<button type="button" class="slick-prev"></button>',
  nextArrow:'<button type="button" class="slick-next"></button>',
  centerMode:true,
  slidesToShow:5,
  slidesToScroll:5,
  focusOnSelect: false,
   responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
  });
	</script>
@endforeach

	
