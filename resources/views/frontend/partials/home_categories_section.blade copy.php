@php
	if (Cache::has('homeCategories')){
	   $homeCategories =  Cache::get('homeCategories');
	} else {
		 $homeCategories = \App\HomeCategory::where('status', 1)->orderBy('position')->get();
		Cache::forever('homeCategories', $homeCategories);
	}
@endphp

@foreach ($homeCategories as $homekey => $homeCategory)
	<?php if($homekey>=3) break; ?>
	<section class="mb-2">
		<div class="container-fluid bg-white">
			<div class="row myrow">
				<div class="col-12">
					<div class="section-title-1 clearfix d-none d-lg-block">
						<h3 class="heading-5 strong-700 mb-0 float-lg-left" style="color:#000;">
							<span class="mr-4">{{ __(strtolower($homeCategory->category->name)) }}</span>
						</h3>
						<ul class="inline-links float-lg-right nav mt-3 mb-2 m-lg-0">	
							<li class="active">
								<a href="{{ route('products.category', $homeCategory->category->slug) }}" class="d-block active">{{ __('View All') }}</a>
							</li>
						</ul>
					</div>
					<div class="section-title-1 clearfix d-block d-sm-none">
						<div class="row">
							<div class="col-8">
								<h3 class="heading-6 strong-600 mb-0 float-left" style="color:#000;">
								<span class="mr-4">{{ __(strtolower($homeCategory->category->name)) }}</span>
								</h3>
							</div>
							<div class="col-4 float-left">
								<a href="{{ route('products.category', $homeCategory->category->slug) }}" class="btn text-white" style="background:#273895;">{{ __('View All') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="px-2 py-4 p-md-3">
				<div class="caorusel-box arrow-round gutters-5">
					<div class="slick-carousel" data-slick-items="4" data-slick-xl-items="6" data-slick-lg-items="6"  data-slick-md-items="4" data-slick-sm-items="2" data-slick-xs-items="2">
						@foreach (json_decode($homeCategory->subsubcategories) as $key => $subsubcategory)
							@if (\App\SubSubCategory::find($subsubcategory) != null)
								@foreach (filter_products(\App\Product::where('published', 1)->where('subsubcategory_id', $subsubcategory))->get() as $key => $product)
									<div class="caorusel-card">
										<div class="product-card-2 card card-product shop-cards shop-tech">
											<div class="card-body p-0">
												<div class="position-relative overflow-hidden">
													<div class="card-image">
														<a href="{{ route('product', $product->slug) }}" class="d-block">
															@if($product->featured_img!=NULL)
																<img class="img-fit lazyload mx-auto" src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($product->featured_img) }}" alt="{{ __($product->name) }}">
															@else
																<img class="img-fit lazyload mx-auto" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($product->name) }}">
															@endif
														</a>
													</div>
												</div>

												<div class="p-md-3 p-2">
													<div class="price-box text-center">
														@if(home_base_price($product->id) != home_discounted_base_price($product->id))
															<del class="old-product-price strong-400">{{ home_base_price($product->id) }}</del>
														@endif
														<span class="product-price strong-600">{{ home_discounted_base_price($product->id) }}</span>
													</div>
													<h2 class="product-title p-0 text-truncate-2 text-center">
														<a href="{{ route('product', $product->slug) }}">{{ __($product->name) }}</a>
													</h2>
													@if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated)
														<div class="club-point mt-2 bg-soft-base-1 border-light-base-1 border">
															{{ __('Club Point') }}:
															<span class="strong-700 float-right">{{ $product->earn_point }}</span>
														</div>
													@endif
												</div>
											</div>
										</div>
									</div>
								@endforeach
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
@endforeach
