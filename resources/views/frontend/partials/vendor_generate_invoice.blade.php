<div class="modal-header">
    <h5 class="modal-title strong-600 heading-5">{{__('Order id')}}: {{ $order->code }}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form action="{{route('orders.vendor_invoice_generating')}}" method="post">
    @csrf
<div class="modal-body gry-bg px-3 pt-0">
    <div class="card mt-3">
        <div class="card-header py-2 px-3 ">
        <div class="heading-6 strong-600">{{__('Order Summary')}}</div>
        </div>
        <div class="card-body pb-0">
            <input type="hidden" name="category" value="{{env('VENDOR_EXPESES_CATEGORY',0)}}">
            <div class="form-group row">
                <div class="col-md-3">
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input type="hidden" name="expensename" value="Product Purchased for Order">
                </div>
                <div class="col-sm-3">
                    Your Invoice Date <br />
                    <input type="text" autocomplete="off" class="form-control datepicker" name='invoicedate' id="invoicedate" placeholder="dd/mm/yyyy" >
                </div>
                <div class="col-sm-3">
                    Your Invoice Number <br />
                    <input type="text" name="invoiceno" class="form-control" id="invoiceno"  placeholder="Invoice Number" value="{{ old('invoiceno') }}" >
                </div>
            </div>
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-header py-2 px-3 ">
        <div class="heading-6 strong-600">{{__('Order Summary')}}</div>
        </div>
        <div class="card-body pb-0">
            <div class="row">
                <div class="col-lg-6">
                    <table class="details-table table">
                        <tr>
                            <td class="w-50 strong-600">{{__('Order Code')}}:</td>
                            <td>{{ $order->code }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-6">
                    <table class="details-table table">
                        <tr>
                            <td class="w-50 strong-600">{{__('Order date')}}:</td>
                            <td>{{ date('d-m-Y H:m A', $order->date) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card mt-4">
                <div class="card-header py-2 px-3 heading-6 strong-600">{{__('Order Details')}}</div>
                <div class="card-body pb-0">
                    <table class="details-table table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th width="40%">{{__('Product')}}</th>
                                <th>{{__('Quantity')}}</th>
                                <th>{{__('Unit Price')}}</th>
                                <th>{{__('Sub Total')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($order->orderDetails->where('seller_id', Auth::user()->id) as $key => $orderDetail)
                            <input type="hidden" class="form-control" name="expenses_type[]" value="Product Expenses">
                            <input type="hidden" class="form-control" name="item_description[]" value="{{$orderDetail->product->id}}">
                            <input type="hidden" class="form-control" name="category_id[]" value="{{$orderDetail->product->category_id}}">
                            <input type="hidden" class="form-control" name="quantity[]" value="{{$orderDetail->quantity}}">
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>
                                        @if ($orderDetail->product != null)
                                            <a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank">{{ $orderDetail->product->name }}</a>
                                        @else
                                            <strong>{{ __('Product Unavailable') }}</strong>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $orderDetail->quantity }}
                                    </td>
                                    <td><input type="text" class="form-control" name="unit_price[]" placeholder="Unit Price" required></td>
                                    <td><input type="text" class="form-control" name="sub_total[]" placeholder="Sub Total" required></td>
                                </tr>
                            @endforeach
                            <tr><td colspan="4" class="text-right">IGST &nbsp; &nbsp;</td><td><input type="text" name="igst" class="form-control" id="igst" required placeholder="IGST" value="{{ old('igst',0) }}"></td></tr>
                            <tr><td colspan="4" class="text-right">CGST &nbsp; &nbsp;</td><td><input type="text" name="cgst" class="form-control" id="cgst" required placeholder="CGST" value="{{ old('cgst',0) }}"></td></tr>
                            <tr><td colspan="4" class="text-right">SGST &nbsp; &nbsp;</td><td><input type="text" name="sgst" class="form-control" id="sgst" required placeholder="SGST" value="{{ old('sgst',0) }}"></td></tr>
                            <tr><td colspan="4" class="text-right">Grand Total &nbsp; &nbsp;</td><td><input type="number" name="invoiceamount" class="form-control" id="invoiceamount" placeholder="Invoice Amount" value="{{ old('Invoice Amount') }}"/></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
        <button class="btn btn-primary">Generate</button>
        </div>
    </div>
</div>
     <script>
        $(function () {
            // INITIALIZE DATEPICKER PLUGIN
            $('.datepicker').datepicker({
                clearBtn: true,
                format: "dd/mm/yyyy",
                autoclose: true,
            });
        });
    </script>