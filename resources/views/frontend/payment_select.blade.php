@extends('frontend.layouts.app')

@section('content')
@php

 $stopProcessing = false;
 $lowStock = false;
 $outofstock = false;
    $all_product_ids = array();
    foreach (Session::get('cart') as $key => $cartItem){
        array_push($all_product_ids, $cartItem['id']);
        $product = \App\Product::find($cartItem['id']);
        $qty = 0;
        if($product->variant_product){
            foreach ($product->stocks as $key => $stock) {
                $qty += $stock->qty;
            }
        } else {
            if($product->current_stock > -1) {
            $qty = $product->current_stock;
            } else {
            $qty = 100;
            }
        }
        if($qty == 0) {
            $stopProcessing = true;
            $outofstock = true;
        } elseif($cartItem['quantity'] > $qty) {
		    $stopProcessing = true;
		    $lowStock = true;
        }
    }
    
$prescriptioncategory = env('PRESCRIPTION_CATEGORY');
$prescriptioncategory = explode(",",$prescriptioncategory);
$PrescriptionRequired = \App\Product::whereIn('id', $all_product_ids)->whereIn('category_id', $prescriptioncategory)->select('category_id')->count();
@endphp
    <div id="page-content">
        <section class="processing slice-xs sct-color-2 border-bottom" @if($mobileapp == 1) style="margin-top:80px" @endif>
            <div class="container-fluid">
                <div class="row cols-delimited justify-content-center">
                    <div class="process">
                        <div class="process-row">
                            <div class="process-step">
                                <a href="{{ route('cart') }}" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-2x text-white"></i></a>
                                <p>My Cart</p>
                            </div>
                            <div class="process-step">
                                <a href="{{ route('checkout.shipping_info') }}" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-map-o fa-2x text-white"></i></a>
                                <p>Shipping Info</p>
                            </div>
                            <div class="process-step">
                                <a href="javascript:;" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-credit-card fa-2x text-white"></i></a>
                                <p>Payment</p>
                            </div> 
                             <div class="process-step">
                                <a href="javascript:;" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-check-circle-o fa-2x"></i></a>
                                <p>Confirmation</p>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-3 gry-bg">
            <div class="container-fluid">
                    @if($stopProcessing)
                        @if($outofstock)
                        <div class="row outofstock">
            	          <div class="col-xl-12">
            	              <div class="alert alert-danger">
            	                  Some of items in your cart is not in stock, Remove that item(s) to proceed further. <a href="{{route('cart')}}">Click Here</a> to go to cart.
            	              </div>
            	          </div>
                	     </div>
                	     @else
                	      <div class="row lowstock">
                	          <div class="col-xl-12">
                	              <div class="alert alert-danger">
                	                  Required stock not available, Descrese the quantity to proceed further.  <a href="{{route('cart')}}">Click Here</a> to go to cart.
                	              </div>
                	          </div>
                	     </div>
                	     @endif
            	     @endif
                <div class="row cols-xs-space cols-sm-space cols-md-space">
                    <div class="col-lg-8">
                        <form action="{{ route('payment.checkout') }}" class="form-default" data-toggle="validator" role="form" method="POST" id="checkout-form" enctype="multipart/form-data">
                            @csrf
                            @if($PrescriptionRequired > 0) 
                            <div class="card">
                                <div class="card-title px-4 py-3">
                                    <h3 class="heading heading-5 strong-500">
                                        {{__('Prescription Required for this order')}}
                                    </h3>
                                </div>
                                <div class="card-body text-center">
                                    <div class="row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="file" id="prescriptionFile" class="form-control" name="prescriptionFile" accept="image/x-png,image/jpeg,image/jpg,.pdf" required>
                                                    <span class="text-danger py-2 allowedtype">* Jpg/png/pdf files only</span>
                                                    <span id="errorMsgnofile" class="text-danger" style="display:none;">Kindly select the prescription</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="card">
                                <div class="card-title px-4 py-2 bg-blue">
                                    <h3 class="heading heading-5 strong-500 text-white">
                                        {{__('Select a payment option')}}
                                    </h3>
                                </div>
                                <div class="card-body text-center">
                                    <div class="row justify-content-center">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-12 text-left">
                                                <span id="errorMsgnopaymentop" class="text-danger font-weight-bold" style="display:none;">Please select the Payment Option</span>
                                               </div>
                                                @if(\App\BusinessSetting::where('type', 'razorpay')->first()->value == 1)
                                                @php
                                                $shipData = Session::get('shippingData');

                                                @endphp
                                                
                                                <div class="col-12 text-left">
                                                    <label class="mb-2" style="border:1px solid #dcdcdc; padding:10px;border-radius:5px">
                                                        
                                                @if(isset($shipData['payoption']))   
                                                    @if($shipData['payoption'] == 'DBC')
                                                    
                                                        <input type="radio" id="" checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','DBC')">
                                                        @else
                                                         <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','DBC')">
                                                    @endif
                                               
                                                @else
                                                    <input type="radio" id="" checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','DBC')">
                                                @endif
                                               
                                                <span>
                                                           &nbsp; Debit  Card/Credit Card
                                                        </span>
                                                        <img src="{{asset('frontend/images/icons/cards/visa.png')}}" class="img-fluid ml-2 mr-2">
                                                        <img src="{{asset('frontend/images/icons/cards/mastercard.png')}}" class="img-fluid mr-2">
                                                        <img src="{{asset('frontend/images/icons/cards/maestro.png')}}" class="img-fluid mr-2">
                                                        <img src="{{asset('frontend/images/icons/cards/rupay.png')}}" class="img-fluid mr-2">
                                                    </label>
                                                </div>
                                                
                                                <div class="col-12 text-left">
                                                    <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px">
                                                @if(isset($shipData['payoption']))
                                                    @if($shipData['payoption'] == 'NB')
                                                        <input type="radio" id="" checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','NB')">
                                                    @else
                                                     <input type="radio" id="" name="payment_option" value="razorpay" onclick="paymentMode('razorpay','NB')">
                                                    @endif
                                                @else
                                                 <input type="radio" id="" name="payment_option" value="razorpay" onclick="paymentMode('razorpay','NB')">
                                                @endif
                                                   
                                                    <span>
                                                          &nbsp; Net Banking
                                                        </span>
                                                    </label>
                                                </div>
                                                <div class="col-12 text-left">
                                                    <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px">
                                                @if(isset($shipData['payoption'])) 
                                                    @if($shipData['payoption'] == 'UPI')
                                                        <input type="radio" id=""  checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','UPI')">
                                                    @else
                                                     <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','UPI')">
                                                    @endif
                                               @else
                                                    <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','UPI')">
                                                @endif
                                                <span>
                                                          &nbsp; UPI
                                                        </span>
                                                    </label>
                                                </div>
                                                <div class="col-12 text-left">
                                                    <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px">
                                                @if(isset($shipData['payoption']))
                                                    @if($shipData['payoption'] == 'WA')
                                                        <input type="radio" id="" checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','WA')">
                                                    @else
                                                     <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','WA')">
                                                   
                                                    @endif
                                                @else
                                                    <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','WA')">
                                                @endif
                                                    <span>
                                                           &nbsp; Wallets : Airtel Money, FreeCharge, PayZapp, MobiKwik, JioMoney
                                                        </span>
                                                    </label>
                                                </div>
                                                @endif
                                                
                                                @if(\App\BusinessSetting::where('type', 'paytm')->first()->value == 1)
                                                <div class="col-12 text-left">
                                                    <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px">
                                                @if(isset($shipData['payoption']))
                                                    @if($shipData['payoption'] == 'PayTM')
                                                        <input type="radio" id="" checked name="payment_option" value="paytm" onclick="paymentMode('razorpay','PayTM')">
                                                    @else
                                                     <input type="radio" id="" name="payment_option" value="paytm" onclick="paymentMode('razorpay','PayTM')">
                                                    @endif
                                                @else
                                                 <input type="radio" id="" name="payment_option" value="paytm" onclick="paymentMode('razorpay','PayTM')">
                                                @endif
                                                   
                                                    <span>
                                                          &nbsp; PayTM :: Wallet, UPI, NetBanking, Cards
                                                        </span>
                                                    </label>
                                                </div>
                                                @endif
                                                
                                                
                                                @if(\App\BusinessSetting::where('type', 'cash_payment')->first()->value == 1)
                                                    @php
                                                        $digital = 0;
                                                        foreach(Session::get('cart') as $cartItem){
                                                            if($cartItem['digital'] == 1){
                                                                $digital = 1;
                                                            }
                                                        }
                                                    @endphp
                                                    @if($digital != 1)
                                                    <div class="col-12 text-left">
                                                        <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px;">
                                                     @if(isset($shipData['payoption']))
                                                        @if($shipData['payoption'] == 'COD')
                                                            <input type="radio" id="" checked name="payment_option" value="cash_on_delivery" onclick="paymentMode('COD','COD')">
                                                        @else
                                                         <input type="radio" id="" name="payment_option" value="cash_on_delivery" onclick="paymentMode('COD','COD')">
                                                        @endif
                                                    @else
                                                    <input type="radio" id="" name="payment_option" value="cash_on_delivery" onclick="paymentMode('COD','COD')">
                                                    @endif
                                                        <span>
                                                               &nbsp; Cash On Delivery
                                                            </span>
                                                        </label>
                                                    </div>
                                                    @endif
                                                @endif
                                                
                                                @if (Auth::check())
                                                    @if (\App\Addon::where('unique_identifier', 'offline_payment')->first() != null && \App\Addon::where('unique_identifier', 'offline_payment')->first()->activated)
                                                        @foreach(\App\ManualPaymentMethod::all() as $method)
                                                          <div class="col-6">
                                                              <label class="payment_option mb-4" data-toggle="tooltip" data-title="{{ $method->heading }}">
                                                                  <input type="radio" id="" name="payment_option" value="{{ $method->heading }}">
                                                                  <span>
                                                                      <img loading="lazy"  src="{{ asset($method->photo)}}" class="img-fluid">
                                                                  </span>
                                                              </label>
                                                          </div>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @if (Auth::check() && \App\BusinessSetting::where('type', 'wallet_system')->first()->value == 1)
                                       
                                        
                                    @endif
                                </div>
                            </div>
                            
                            
                            <div class="row align-items-center pt-4 d-none d-lg-block d-lg-flex">
                                <div class="col-6">
                                    <a href="{{ route('home') }}" class="btn btn-styled btn-base-1" style="background:#131921">
                                        <i class="fa fa-reply" aria-hidden="true"></i>
                                        {{__('Return to shop')}}
                                    </a>
                                </div>
                                <div class="col-6 text-right">
                                    @if($stopProcessing)
                                    <button type="button" class="btn btn-styled btn-base-1 bg-blue" disabled>{{__('Complete Order')}}</button>
                                    @else
                                    <button type="button" onclick="submitOrder(this)" class="btn btn-styled btn-base-1 bg-blue paymnybtn">{{__('Complete Order')}}</button>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-4 ml-lg-auto">
                        @include('frontend.partials.cart_summary')
                    </div>
                </div>
                 <div class="row align-items-center pt-4 d-block d-lg-none">
                        <div class="col-md-12 text-center">
                            @if($stopProcessing)
                            <button type="button" class="btn btn-styled btn-block btn-base-1 bg-blue" disabled>{{__('Complete Order')}}</button>
                            @else        
                            <button type="button"  onclick="submitOrder(this)" class="btn btn-styled btn-block btn-base-1 bg-blue paymnybtn">{{__('Complete Order')}}</button>
                            @endif
                        </div>
                        <div class="col-md-12 text-center pt-2">
                            <a href="{{ route('home') }}">
                                <i class="fa fa-reply" aria-hidden="true"></i>
                                {{__('Return to shop')}}
                            </a>
                        </div>
                  </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="wallet_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <!-- <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5">{{__('Recharge Wallet')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> -->
                <form class="" action="{{ route('wallet.recharge') }}" method="post">
                    @csrf
                    <div class="modal-body gry-bg px-3 pt-3">
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{__('Amount')}} <span class="required-star">*</span></label>
                            </div>
                            <div class="col-md-10">
                                <input type="number" class="form-control mb-3" name="amount" placeholder="{{__('Amount')}}" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{__('Payment Method')}}</label>
                            </div>
                            <div class="col-md-10">
                                <div class="mb-3" style="border:1px solid #e6e6e6">
                                    <select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="payment_option">
                                        @if (\App\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1)
                                            <option value="paypal">{{__('Paypal')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'stripe_payment')->first()->value == 1)
                                            <option value="stripe">{{__('Stripe')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'sslcommerz_payment')->first()->value == 1)
                                            <option value="sslcommerz">{{__('SSLCommerz')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'instamojo_payment')->first()->value == 1)
                                            <option value="instamojo">{{__('Instamojo')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'paystack')->first()->value == 1)
                                            <option value="paystack">{{__('Paystack')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'voguepay')->first()->value == 1)
                                            <option value="voguepay">{{__('VoguePay')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'razorpay')->first()->value == 1)
                                            <option value="razorpay">{{__('Razorpay')}}</option>
                                        @endif
                                        @if (\App\Addon::where('unique_identifier', 'paytm')->first() != null && \App\Addon::where('unique_identifier', 'paytm')->first()->activated)
                                            <option value="paytm">{{__('Paytm')}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-base-1">{{__('Confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if (Session::has('wallet_credit'))
    <form id="remove_wallet_credit" action="{{ route('checkout.remove_wallet_credit') }}" method="post">
    @else
    <form id="apply_wallet_credit" action="{{ route('checkout.apply_wallet_credit') }}" method="post">
    @endif
    @csrf
    </form>

@endsection

@section('script')
    <script type="text/javascript">
        function use_wallet(ele){
            var html = $(ele).html();
            $(ele).prop('disabled', true);
            $(ele).html('<i class="fa fa-spin fa-spinner"></i> Loading...');
            
            $('input[name=payment_option]').val('wallet');
            $("input[name=payment_option][value='wallet']").prop("checked",true);
            @if($PrescriptionRequired > 0) 
                if($('#prescriptionFile').val() == "") {
                    $('#errorMsgnofile').slideDown(100);
                    $("#prescriptionFile").focus(); 
                    $(ele).html(html);
                    $('html, body').animate({
                         scrollTop: ($('#prescriptionFile').offset().top - 300)
                    }, 2000);
                    $(ele).prop('disabled', false);
                }
                else {
                    $('#errorMsgnofile').slideUp(100);
                    $(ele).prop('disabled', true);
                    $(ele).html('<i class="fa fa-spin fa-spinner"></i>');
                    $('#checkout-form').submit();
                }
			@else
			    $(ele).prop('disabled', true);
                $(ele).html('<i class="fa fa-spin fa-spinner"></i> Loading...');
                $('#checkout-form').submit();
            @endif
        }
        function submitOrder(el){
            if($('input[name=payment_option]:checked').val()==undefined ){
                $('#errorMsgnopaymentop').slideDown(100);
                
                $('html, body').animate({
                     scrollTop: ($('#errorMsgnopaymentop').offset().top - 300)
                }, 2000);
                return false;
            }
            @if($PrescriptionRequired > 0) 
                if($('#prescriptionFile').val() == "") {
                    $('#errorMsgnofile').slideDown(100);
                    $("#prescriptionFile").focus(); 
                    $(ele).html(html);
                    $('html, body').animate({
                         scrollTop: ($('#prescriptionFile').offset().top - 300)
                    }, 2000);
                    $(ele).prop('disabled', false);
                } else {
                    $('#errorMsgnofile').slideUp(100);
    			    $('.paymnybtn').prop('disabled', true);
                    $('.paymnybtn').html('<i class="fa fa-spin fa-spinner"></i> Loading...');
                    $('#checkout-form').submit();
                }
			@else
			    $('.paymnybtn').prop('disabled', true);
                $('.paymnybtn').html('<i class="fa fa-spin fa-spinner"></i> Loading...');
                $('#checkout-form').submit();
            @endif
        }
    </script>
    <script type="text/javascript">
          
      
        function show_wallet_modal(){
            $('#wallet_modal').modal('show');
        }

        function show_make_wallet_recharge_modal(){
            $.post('{{ route('offline_wallet_recharge_modal') }}', {_token:'{{ csrf_token() }}'}, function(data){
                $('#offline_wallet_recharge_modal_body').html(data);
                $('#offline_wallet_recharge_modal').modal('show');
            });
        }

        //Change shipping price according to payment mode
       

        function paymentMode($apymnetMode,$payoption){
         
            $totalAmount = $('#totalPrice').val();
            
                    $.ajax({
    				url:"{{route('ajax.updatePaymentOption')}}",
    				type: 'POST',
    				dataType:'json',
    				data:{'paymentOption':$apymnetMode,'totalAmount':$totalAmount,'payoption':$payoption},
    				success: function(data) {
                        if(data){
                            location.reload();
                        }
    					
    				}
    			});
            
            
           
        }
    </script>
<script>
    $('INPUT[type="file"]').change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        ext= ext.toLowerCase();
		var FileSize = this.files[0].size / 1024 / 1024; // in MB
		if (FileSize > 4) {
		    $(".allowedtype").hide();
			$("#errorMsgnofile").text('Max file size: 4MB');
			this.value = '';
		}
		else{
		    $(".allowedtype").hide();
		    $("#errorMsgnofile").text('');
		}
        switch (ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'pdf':
                $('.paymnybtn').attr('disabled', false);
                break;
            default:
                $("#errorMsgnofile").html("This is not an allowed file type.");
                $("#errorMsgnofile").slideDown();
                $(".allowedtype").show();
                $('html, body').animate({
                     scrollTop: ($('#prescriptionFile').offset().top - 300)
                }, 2000);
                this.value = '';
        }
    });
</script>
@endsection
