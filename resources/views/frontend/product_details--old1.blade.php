@extends('frontend.layouts.app')

@section('meta_title'){{ $detailedProduct->meta_title }}@stop

@section('meta_description'){{ $detailedProduct->meta_description }}@stop

@section('meta_keywords'){{ $detailedProduct->tags }}@stop

@section('meta')
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $detailedProduct->meta_title }}">
    <meta itemprop="description" content="{{ $detailedProduct->meta_description }}">
    <meta itemprop="image" content="{{ asset($detailedProduct->meta_img) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ $detailedProduct->meta_title }}">
    <meta name="twitter:description" content="{{ $detailedProduct->meta_description }}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="{{ asset($detailedProduct->meta_img) }}">
    <meta name="twitter:data1" content="{{ single_price($detailedProduct->unit_price) }}">
    <meta name="twitter:label1" content="Price">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $detailedProduct->meta_title }}" />
    <meta property="og:type" content="product" />
    <meta property="og:url" content="{{ route('product', $detailedProduct->slug) }}" />
    <meta property="og:image" content="{{ asset($detailedProduct->meta_img) }}" />
    <meta property="og:description" content="{{ $detailedProduct->meta_description }}" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
    <meta property="og:price:amount" content="{{ single_price($detailedProduct->unit_price) }}" />
@endsection

@section('content')
<link href="{{asset('frontend/css/product-detail-style-light.css')}}" rel="stylesheet">
    <style>
         .fancybox-button--thumbs{display:none!important;}
		.tabs--style-2 .nav-tabs .nav-link {
			border: 0;
			border-bottom: 1px solid #232F3E;
			margin-right: 0;
			color: #2b2b2c;
			padding: 1rem 0;
			font-size: 0.875rem;
			font-family: "Open Sans", sans-serif;
		}
		@media (max-width: 576px){
            .fancybox-show-thumbs .fancybox-inner {
                right: 0!important;
            }
        }
		.tabs--style-2 .nav-tabs .nav-item.show .nav-link, .tabs--style-2 .nav-tabs .nav-link.active, .tabs--style-2 .nav-tabs .nav-link:hover {
			border-bottom: 1px solid #fff;
			background: #e6e6e6 !important;
			color: #000;
		}
		.tabs--style-2 .nav-tabs .nav-item {
			margin-right: 1rem;
		}
		.tabs--style-2 .nav-tabs .nav-link {
			border: 0;
			border-bottom: 1px solid #94A2ED;
			margin-right: 0;
			color: #2b2b2c;
			padding: 1rem 1rem;
			font-size: 0.875rem;
			font-family: "Open Sans", sans-serif;
		}
		#tab_default_5 td a:hover{
			color:#94A2ED!important;
			text-decoration:underline;
		}
    	.card-notify-year {
            position: absolute;
            right: -5px;
            top: -12px;
            text-align: center;
            color: #fff;      
            font-size: 14px;      
            width: 50px;
            height: 50px;    
            padding: 15px 0 0 0;
    		z-index:99;
    	}
    	.btn-social{position:relative;padding-left:44px;text-align:left;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}
    	.btn-social :first-child{ position:absolute;left:0;top:0;bottom:0;width:32px;line-height:34px;font-size:1.6em;text-align:center;border-right:1px solid rgba(0,0,0,0.2)}
    	
    	.fa-facebook{background:#3B5998!important;}
    	.fa-twitter{background:#2BA9E1!important;}
    	.fa-linkedin{background:#0077B5!important;}
    	.fa-envelope{background:#3B5998!important;}
    	.fa-whatsapp{background:#2DB742!important;}
    	.btn-linkedin{ border-color:#0077B5!important; }
    	.btn-email{ border-color: #517FA4!important; }
    	.btn-whatsapp{ border-color: #2DB742!important; }
    	.btn-facebook,.btn-twitter,.btn-linkedin,.btn-email,.btn-whatsapp{ color:#000!important;}
    </style>
    
    <style>
		.bluebg{background:#282563;}
		
		@media (max-width: 800px){
			.fancybox-thumbs {
				width: 0px!important;
			}
			.fancybox-show-thumbs .fancybox-inner { right: 0; }
		}
		.morecontent span {
			display: none;
		}
		.morelink {
			display: block;
		}
		.piece {
			font-size: 13px;
			color: #777;
		}
		.prd-block .size-list li.active span.value, .prd-block .size-list li:hover:not(.absent-option) span.value {
			border-color: #292664;
			color: #292664;
		}
		.slick-next:before {
			content: '\f105';
		}
		.slick-prev:before, .slick-next:before {
			font-family: 'FontAwesome';
			font-size: 27px;
			line-height: 1;
			color: #2D2C2C;
			opacity: 0.7;
			transition: opacity 0.2s;
		}
		.slick-prev:before {
			content: '\f104';
		}
		.product-previews-carousel [data-video]:before {
			color: #ffffff;
			content: '\f01d';
			font-family: 'FontAwesome';
			font-size: 40px;
			left: 50%;
			line-height: 1em;
			position: absolute;
			top: 50%;
			transform: translate(-50%, -50%);
			z-index: 1;
		}
		.panel > .panel-heading:after {
			position: absolute;
			top: 50%;
			right: 0;
			margin-top: -14px;
			padding-left: 2px;
			width: 30px;
			height: 30px;
			content: "\f105";
			text-align: center;
			font-size: 14px;
			font-family: 'FontAwesome';
			line-height: 28px;
			pointer-events: none;
			transition: 0.2s;
		}
		.buynow{
			background:#7386ED;
		}
		
		.btn-social{position:relative;padding-left:44px;text-align:left;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}
    	.btn-social :first-child{ position:absolute;left:0;top:0;bottom:0;width:32px;line-height:34px;font-size:1.6em;text-align:center;border-right:1px solid rgba(0,0,0,0.2)}
    	
    	.fa-facebook{background:#3B5998!important;}
    	.fa-twitter{background:#2BA9E1!important;}
    	.fa-linkedin{background:#0077B5!important;}
    	.fa-envelope{background:#3B5998!important;}
    	.fa-whatsapp{background:#2DB742!important;}
    	.btn-linkedin{ border-color:#0077B5!important; }
    	.btn-email{ border-color: #517FA4!important; }
    	.btn-whatsapp{ border-color: #2DB742!important; }
    	.btn-facebook,.btn-twitter,.btn-linkedin,.btn-email,.btn-whatsapp{ color:#000!important;}
	</style>
    <div class="page-content productDetails bg-white">
        <div class="holder mt-0 d-none d-md-block">
            <div class="container-fluid">
                <ul class="breadcrumbs">
					<li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
					<li><a href="{{ url('categories') }}">{{__('All Categories')}}</a></li>
					@php 
					$Category = \App\Category::find($detailedProduct->category_id);
					$SubCategory = \App\SubCategory::find($detailedProduct->subcategory_id);
					$SubSubCategory = \App\SubSubCategory::find($detailedProduct->subsubcategory_id);
					@endphp
					<li><a href="{{ route('products.category', $Category->slug) }}">{{ ucwords(strtolower($Category->name)) }}</a></li>
					<li><a href="{{ route('products.subcategory', [$Category->slug,$SubCategory->slug]) }}">{{ ucwords(strtolower($SubCategory->name)) }}</a></li>
					<li><a href="{{ route('products.subsubcategory', [$Category->slug,$SubCategory->slug, $SubSubCategory->slug]) }}">{{ ucwords(strtolower($SubSubCategory->name)) }}</a></li>
					<li><span>{{ __(ucwords(strtolower($detailedProduct->name))) }}</span></li>
                </ul>
            </div>
        </div>
        <div class="holder mt-0">
            <div class="container-fluid">
                <div class="row prd-block prd-block--mobile-image-first js-prd-gallery" id="prdGallery100">
                   <div class="col-xl-5">
                        <div class="prd-block_info js-prd-m-holder mb-2 mb-md-0"></div><!-- Product Gallery -->
                        <div class="prd-block_main-image main-image--slide js-main-image--slide">
                            <div class="prd-block_main-image-holder js-main-image-zoom" data-zoomtype="inner">
                                <div class="prd-has-loader">
                                    <div class="gdw-loader"></div>
                                    @if(is_array(json_decode($detailedProduct->photos)) && count(json_decode($detailedProduct->photos)) > 0)
                                    <img src="{{ asset(json_decode($detailedProduct->photos)[0]) }}" class="zoom" alt="{{$detailedProduct->name}}" data-zoom-image="{{ asset(json_decode($detailedProduct->photos)[0]) }}">
                                    @else
                                    <img src="{{ asset('frontend/images/product-thumb.jpg') }}" style="padding-top:100%;" alt="{{$detailedProduct->name}}">
                                    @endif
                                </div>
                                <div class="prd-block_main-image-next slick-next js-main-image-next">NEXT</div>
                                <div class="prd-block_main-image-prev slick-prev js-main-image-prev">PREV</div>
                            </div>
                            @if(is_array(json_decode($detailedProduct->photos)) && count(json_decode($detailedProduct->photos)) > 0)
                            <div class="prd-block_main-image-links">
								<a href="{{ asset(json_decode($detailedProduct->photos)[0]) }}" class="prd-block_zoom-link" ><i class="fa fa-search-plus"></i></a>
							</div>
							@endif
                        </div>
                        @if(is_array(json_decode($detailedProduct->photos)) && count(json_decode($detailedProduct->photos)) > 0)
                        <div class="product-previews-wrapper">
                            <div class="product-previews-carousel" id="previewsGallery100">
                                @foreach (json_decode($detailedProduct->photos) as $key => $photo)
                                <a href="#" data-value="Silver" data-image="{{ asset($photo) }}" data-zoom-image="{{ asset($photo) }}"><img src="{{ asset($photo) }}" alt="{{$detailedProduct->name}}"></a>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <!-- /Product Gallery -->
                    </div>
                            
                    <div class="col-md">
                        <div class="prd-block_info">
                            <div class="js-prd-d-holder prd-holder">
                                <div class="prd-block_title-wrap">
                                    <h1 class="prd-block_title">{{ __(ucwords(strtolower($detailedProduct->name))) }}</h1>
                                </div>
								<div class="row">
									<div class="col-12">
									     @php
									        $product_company_show = env('PRODUCT_COMPANY_SHOW_CATEGORY','');
                                            $product_company_show = explode(",",$product_company_show);
                                        @endphp
									    @if(in_array($detailedProduct->category_id, $product_company_show))
									    	<span>{{\App\Brand::find($detailedProduct->brand_id)->name}}</span><br/>
										@endif
										<!--
										@if($detailedProduct->salt != null)
									    	<span class="more">{{$detailedProduct->salt}}</span>
									    @endif
									    -->
									   
                                        @if($detailedProduct->salt != null)
                                        <small style="font-size:13px;">
                                            @if(strlen($detailedProduct->salt) > 80) 
                                                @php
                                                    $c = Str::limit($detailedProduct->salt, 80, '');
                                                    $converted = Str::substr($detailedProduct->salt, 80);
                                                @endphp
                                                {!! $c !!}
                                                    <span id="shortview"><a  style="display:inline;" href="javascript:$('#fullview').show(100);$('#shortview').hide(100);">View More >></a></span>
                                                    <span id="fullview" style="display:none;">{{$converted}} <a  style="display:inline;" href="javascript:$('#fullview').hide(100);$('#shortview').show(100);"><< View Less</a></span>
                                            @else 
                                                {{$detailedProduct->salt}}
                                            @endif 
                                         </small>
                                        @endif
									</div>
								</div>
                                <div class="prd-block_info-top">
                                    <div class="product-sku">SKU: <span>#{{$detailedProduct->product_id}}</span></div>
                                    @php
                                        $total = 0;
                                        $total += $detailedProduct->reviews->count();
                                    @endphp
                                    <div class="prd-rating"><a href="#" class="prd-review-link text-blue" >{{ renderStarRating($detailedProduct->rating) }} <span>{{ $total }} reviews</span></a></div>
                                    <div class="prd-availability">{{__('Sold by')}}: <span>
                                    @if ($detailedProduct->added_by == 'seller' && \App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
                                        <a href="{{ route('shop.visit', $detailedProduct->user->shop->slug) }}">{{ $detailedProduct->user->shop->name }}</a>
                                    @else
                                        {{ __('Alde Bazaar') }}
                                    @endif
                                    </span></div>
                                </div>
                            </div>
                            @php
								$category_discount=\App\Category::where('id',$detailedProduct->category_id)->first();
							@endphp
							@php
                                $home_base_price = home_base_price($detailedProduct->id);
                                $home_discounted_base_price = home_discounted_base_price($detailedProduct->id);
							@endphp
							
                            <div class="prd-block_options topline">
								<div class="prd-color1 swatches"><span class="option-label">Price:</span>
                                    <div class="">
                                        @if($home_base_price != $home_discounted_base_price)
										<span class="prd-block_price--old">{{ $home_base_price }}</span> &nbsp;
										<span class="prd-block_price--actual">{{$home_discounted_base_price}} <small class="piece">/{{ $detailedProduct->unit }}</small><span class="pl-1" style="color:#05991e;font-size:14px;">({{discount_calulate($home_base_price, $home_discounted_base_price )}}% off)</span></span>
										@else
										<span class="prd-block_price--actual">{{$home_discounted_base_price}} <small class="piece">/{{ $detailedProduct->unit }}</small>
										@endif
									</div>
                                </div>
                                
                                @if (count(json_decode($detailedProduct->colors)) > 0)
                                <div class="prd-color1 swatches"><span class="option-label">{{__('Color')}}:</span>
                                    <ul class="size-list js-size-list">
                                        @foreach (json_decode($detailedProduct->colors) as $color)
                                        <li data-value="{{$color}}" onclick="checkProduct('{{$color}}', 'Colors')" style="cursor:pointer;">
                                            <input type="hidden" id="Colors-{{ $color }}" class="attr-Colors" value="{{ $color }}"><span class="value" style="background: {{\App\Color::where('name', $color)->first()->code }};"></span></a>
										</li>
										@endforeach
                                    </ul>
                                </div>
                                @endif
                                @php
                                $variation_order = json_decode($detailedProduct->variation_order,true);
                                if($variation_order == null) {
                                $variation_order = array();
                                }
                                    if(in_array("Colors", $variation_order)) {
                                        unset($variation_order[array_search("Colors",$variation_order)]);
                                    }
                                    $variation = json_decode($detailedProduct->variation,true);
                                    if(isset($variation["Colors"])) {
                                        unset($variation["Colors"]);
                                    }
                                @endphp
                                
                                @if (count($variation_order) > 0)
                                    @foreach($variation as $key=>$vo)
                                    <div class="prd-size swatches"><span class="option-label">{{ $key }}:</span>
                                        <ul class="size-list js-size-list">
                                            @php $key = str_ireplace(" ", "_", $key); @endphp
                                            @foreach ($vo as $value)
                                                @php $valueid = str_ireplace(" ", "_", $value); @endphp
                                            <li data-value="{{$value}}"  onclick="checkProduct('{{$value}}', '{{$key}}')">
                                                <input id="{{ $key }}-{{ $valueid }}" class="attr-{{$key}}" value="{{ $value }}" type="hidden">
                                                <span class="value">{{ $value }}</span>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endforeach
                                @endif
                                
                                <div class="mssage" style="color:red;display:none;">No Product availiable with your selection</div>
                                 <div class="row no-gutters pb-3 d-none" style="display:none" id="chosen_price_div">
                                        <div class="product-price">
                                            <strong id="chosen_price" style="color:#000">
                                            </strong>
                                        </div>
                                </div>
                                <form id="option-choice-form">
                                @csrf
                                <input type="hidden" name="id" value="{{ $detailedProduct->id }}">
                                <div class="prd-block_qty"><span class="option-label">Qty:</span>
                                    <div class="qty qty-changer">
                                        <fieldset><input type="button" value="&#8210;" class="decrease"> 
                                        <input type="text" name="quantity" class="qty-input" id="quantityval" value="1" data-min="1" data-max="10"> 
                                        <input type="button" value="+" class="increase"></fieldset>
                                    </div>
                                </div>
                                </form>
                            </div>
                            @php
                                $qty = 0;
                                if($detailedProduct->variant_product){
                                    foreach ($detailedProduct->stocks as $key => $stock) {
                                        $qty += $stock->qty;
                                    }
                                }
                                else{
                                    if($detailedProduct->current_stock > -1) {
                                    $qty = $detailedProduct->current_stock;
                                    } else {
                                    $qty = 100;
                                    }
                                    
                                }
                            @endphp
                            @if ($qty != 0)
                            <div class="prd-block_actions topline">
                                <!--<div class="prd-block_price">Total: <span class="prd-block_price--actual">₹ 180.00</span> </div>-->
                                <div class="btn-wrap mr-2"><button class="btn btn--add-to-cart buynow" style="background:#EB3038" onclick="buyNow(this, {{$detailedProduct->id}},  $('#quantityval').val(),  'full')"><span>Buy Now</span></button></div>
                                <div class="btn-wrap"><button class="btn btn--add-to-cart" style="background:#282563" onclick="addToCart(this, {{$detailedProduct->id}}, $('#quantityval').val(), 'full')"><span>Add to Cart</span></button></div>
                                <div class="prd-block_link">
                                    @if(env('BARGAIN_BTN',false))
									@php $bargain_cat = explode(",",env('BARGAIN_CATEGORY','')); @endphp
									    @if(in_array($detailedProduct->category_id, $bargain_cat))
        							    <a href="{{env('BARGAIN_LINK','')}}" target="_blank" class="btn btn-styled btn-alt-base-1 strong-400 hov-bounce hov-shaddow ml-2 text-white">
                                        {{__('For best lowest price Click')}}
        								</a>
								        @endif
								    @endif
									<a href="javascript:addToWishList({{ $detailedProduct->id }})" class="fa fa-heart-o"></a>
									<a href="javascript:void(0)" class="fa fa-share-alt" data-toggle="modal" data-target="#shareitemmodal"></a>
								</div>
                            </div>
                            @endif
                            <div class="prd-safecheckout topline">
                                @if ($qty == 0)
                                 @else
                                <button type="button" class="btn btn-styled btn-alt-base-1 strong-400 hov-bounce hov-shaddow mb-2 text-white" data-toggle="modal" data-target="#productRequest">
                                    Request Product
								</button>
								@endif
								<button type="button" class="btn btn-styled btn-alt-base-1 strong-400 hov-bounce hov-shaddow mb-2 text-white" data-toggle="modal" data-target="#productRequest">
                                    For best lowest price Click
								</button>
								@if($detailedProduct->pdf != null)
								<a href="{{ asset($detailedProduct->pdf) }}" class="btn btn-styled btn-alt-base-1 strong-400 hov-bounce hov-shaddow mb-2 text-white" >
                                    Downloads
								</a>
								@endif
								<br/>
    								@if((\App\Category::find($detailedProduct->category_id)->policy_heading)!="")
    									<a href="#policymodal" data-toggle="modal" class="mb-2 strong-700" style="color:#007185">
    										<img src="{{ asset('frontend/images/return.png') }}" height="26">
    										{{(\App\Category::find($detailedProduct->category_id)->policy_heading)}}
    									</a>
                                    @endif
								</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="holder mt-4">
                <div class="container-fluid">
					<div class="row rowspadding">
						<div class="col-md-4 mb-2">
							<div class="card">
								<div class="card-header text-white text-center bluebg">Description</div>
								<div class="card-body text-center px-4">
									<div class="list-group list-group-flush">
										<p class="text-justify moredescription text14"><?php echo $detailedProduct->description; ?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 mb-2">
							<div class="card">
								<div class="card-header text-white text-center bluebg">Details</div>
								<div class="card-body text-center">
									<div class="list-group list-group-flush">
										<div class="table-responsive">
											<table class="table table-striped table-borderless h-font text-uppercase mytable">
												<tbody class="text-left">
													<tr>
														<td>SKU</td>
														<td><b>#{{$detailedProduct->product_id}}</b></td>
													</tr>
													<tr>
														<td>Brand</td>
														<td><b><a href="{{ route('products.brand', \App\Brand::find($detailedProduct->brand_id)->slug) }}" target="_blank">{{\App\Brand::find($detailedProduct->brand_id)->name}}</a></b></td>
													</tr>
													<tr>
														<td>Category</td>
														<td><b><a href="{{url('category/'.$Category->slug)}}" target="_blank">{{$Category->name}}</a></b></td>
													</tr>
													<tr>
														<td>SubCategory</td>
														<td><b><a href="{{url('category/'.$Category->slug.'/'.$SubCategory->slug)}}" target="_blank">{{$SubCategory->name}}</a></b></td>
													</tr>
													<tr>
														<td>Sub SubCategory</td>
														<td><b><a href="{{route('products.subsubcategory', [$Category->slug, $SubCategory->slug, $SubSubCategory->slug])}}" target="_blank">{{$SubSubCategory->name}}</a></b></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 mb-2">
							<div class="card">
								<div class="card-header text-white text-center bluebg">Reviews</div>
								<div class="card-body text-center px-4">
									<div class="list-group list-group-flush">
										<div id="productReviews">
											<div class="prd-rating">{{ renderStarRating($detailedProduct->rating) }}<span> Based on {{$total}} review</span></div>
										
                                        @if(count($detailedProduct->reviews) <= 0)
                                            <div class="text-center text14">
                                                {{ __('There have been no reviews for this product yet.') }}
                                            </div>
                                        @endif
                                        @foreach ($detailedProduct->reviews as $key => $review)
											<div class="review-item text-left">
												<h4 class="review-item_author">{{ $review->user->name }}</h4>
												@for ($i=0; $i < $review->rating; $i++)
												<div class="fa fa-star">
												@endfor
                                                @for ($i=0; $i < 5-$review->rating; $i++)
                                                    <i class="fa fa-star-o"></i>
                                                @endfor
												<p class="more">{{ $review->comment }}</p>
											</div>
										@endforeach
											<div class="text-center"><a href="#" class="btn-decor">Write Review</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                
            </div>
        </div>
    </div>
	<br/>
	<!-------Return Policy Modal-------->
	<div class="modal fade" id="policymodal">
	  <div class="modal-dialog modal-dialog-centered">
		<!--Content-->
		<div class="modal-content">
		  <!--Header-->
		  <div class="modal-header">
			<p class="heading">Return Policy</p>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true" class="white-text">&times;</span>
			</button>
		  </div>

		  <!--Body-->
		  <div class="modal-body">
			<div class="row">
			  <div class="col-12">
				<p class="text-justify">7 Day FREE Returns This item is eligible for return within 7 days of delivery. You can avail replacement, in an unlikely event of the damaged, defective, or different item delivered to you. You can also return the product for a full refund. Please keep the item in its original condition, with brand outer box, MRP tags attached, warranty cards, and original accessories in manufacturer packaging for a successful return pick-up. We may contact you to ascertain the damage or defect in the product prior to issuing a refund/replacement.</p>
			  </div>
			</div>
		  </div>
		  <!--Footer-->
		  <div class="modal-footer justify-content-center">
			<a href="{{ route('returnpolicy') }}" class="btn ml-2 btn-link strong-700 text-white" style="color:#007185" target="_blank">Read Full Return Policy</a>
		  </div>
		</div>
		<!--/.Content-->
	  </div>
	</div>

    <section class="gry-bg  my-2">
        <div class="container-fluid" style="max-width:100%!important;padding-left:5px;padding-right:5px">
            <div class="row">
                	@php 
			/*
				
					$related_prods=filter_products(\App\Product::where('subcategory_id', $detailedProduct->subcategory_id)->where('id', '!=', $detailedProduct->id))->limit(10)->get();
			
				@if(count($related_prods)>0)
				<div class="col-12 d-none" id="section_best_selling">
    				<section class="mb-2 aldeproductslider">
                        <div class="container-fluid bg-white py-2 py-md-0">
                    		<div class="row d-block d-lg-none">
                    			<div class="col-12">
                    				<div class="section-title-1 clearfix pb-1">
                    					<h3 class="heading-5 strong-700 mb-0 float-left">
                    						<span class="mr-4">{{__('Related products')}}</span>
                    					</h3>
                    				</div>
                    			</div>
                    		</div>
                            <div class="row">
                                <div class="col-lg-2 stickblock d-none d-lg-block" style="background: #282563">
                    				<h2 style=" color: #fff">{{__('Related products')}}</h2>
                                </div>
                                <div class="col-lg-10 pt-1 px-0">
                                <div class="best-sell-slider-2 owl-carousel owl-nav-style owl-nav-style-5">
                                    @foreach ($related_prods as $key => $related_product)
                                        @php
                        			    $home_base_price = home_base_price($related_product->id);
    									$home_discounted_base_price = home_discounted_base_price($related_product->id);
                        			    @endphp
                    						<!-- Single Item -->
                    						<article class="list-product">
                    							<div class="img-block">
                    								<a href="{{ route('product', $related_product->slug) }}" class="thumbnail">
                    								    @if($related_product->thumbnail_img!=NULL)
                    									<img class="first-img lazyload" src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($related_product->thumbnail_img) }}" alt="{{ __($related_product->name) }}" />
                    									@else
                    								    <img class="first-img lazyload" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($related_product->name) }}">
                    									@endif
                    								</a>
                    							</div>
                    							@if($home_base_price != $home_discounted_base_price)
                    							<ul class="product-flag">
                    								<li class="new discount-price bg-success">{{discount_calulate($home_base_price, $home_discounted_base_price )}}% off</li>
                    							</ul>
                    							@endif
                    							<div class="product-decs text-center">
                    								<h2><a href="{{ route('product', $related_product->slug) }}" class="product-link">{{ __($related_product->name) }}</a></h2>
                    								<div class="pricing-meta">
                    									<ul>
                    									    @if($home_base_price != $home_discounted_base_price)
                    										<li class="old-price">{{ $home_base_price }}</li>
                    										@endif
                    										<li class="current-price">{{ $home_discounted_base_price }}</li>
                    									</ul>
                    									
                    								</div>
                    							</div>
                    							<div class="add-to-cart-buttons">
                    							    @if($related_product->current_stock == 0)
                    							    <button class="btn btn95" type="button" disabled> Out of Stock</button>
                    							    @else
                    							    <button class="btn btn25" onclick="addToCart(this, {{$related_product->id}}, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
                    							    <button class="btn btn70" onclick="buyNow(this, {{ $related_product->id }}, 1, 'full')" type="button"> Buy Now</button>
                    							    @endif
                    							</div>
                    						</article>
                					@endforeach
                    			</div>
                    			</div>
                            </div>
                        </div>
                    </section>
                </div>	
			
				@endif
				*/
					@endphp
				<div class="col-12" id="section_featured">
				@include('frontend.partials.featured_products_section')
				</div>
			
			<div class="col-12 d-none" id="">
    				<section class="mb-2 aldeproductslider">
                        <div class="container-fluid bg-white py-2 py-md-0">
                    		<div class="row d-block d-lg-none">
                    			<div class="col-12">
                    				<div class="section-title-1 clearfix pb-1">
                    					<h3 class="heading-5 strong-700 mb-0 float-left">
                    						<span class="mr-4">{{__('Top Selling Products From This Seller')}}</span>
                    					</h3>
                    				</div>
                    			</div>
                    		</div>
                            <div class="row">
                                <div class="col-lg-2 stickblock d-none d-lg-block" style="background: #282563">
                    				<h2 style=" color: #fff">{{__('Top Selling Products From This Seller')}}</h2>
                                </div>
                                <div class="col-lg-10 pt-1 px-0">
                                <div class="best-sell-slider-2 owl-carousel owl-nav-style owl-nav-style-5">
                                    @foreach (filter_products(\App\Product::where('user_id', $detailedProduct->user_id)->orderBy('num_of_sale', 'desc'))->limit(6)->get() as $key => $top_product)
                                        @php
                        			    $home_base_price = home_base_price($related_product->id);
    									$home_discounted_base_price = home_discounted_base_price($related_product->id);
                        			    @endphp
                    						<!-- Single Item -->
                    						<article class="list-product">
                    							<div class="img-block">
                    								<a href="{{ route('product', $top_product->slug) }}" class="thumbnail">
                    								    @if($top_product->thumbnail_img!=NULL)
                    									<img class="first-img lazyload" src="{{ asset('frontend/images/product-thumb.png') }}" data-src="{{ asset($top_product->thumbnail_img) }}" alt="{{ __($top_product->name) }}" />
                    									@else
                    								    <img class="first-img lazyload" src="{{ asset('frontend/images/product-thumb.png') }}" alt="{{ __($top_product->name) }}">
                    									@endif
                    								</a>
                    							</div>
                    							@if($home_base_price != $home_discounted_base_price)
                    							<ul class="product-flag">
                    								<li class="new discount-price bg-success">{{discount_calulate($home_base_price, $home_discounted_base_price )}}% off</li>
                    							</ul>
                    							@endif
                    							<div class="product-decs text-center">
                    								<h2><a href="{{ route('product', $top_product->slug) }}" class="product-link">{{ $top_product->name }}</a></h2>
                    								<div class="pricing-meta">
                    									<ul>
                    									    @if($home_base_price != $home_discounted_base_price)
                    										<li class="old-price">{{ $home_base_price }}</li>
                    										@endif
                    										<li class="current-price">{{ $home_discounted_base_price }}</li>
                    									</ul>
                    								</div>
                    							</div>
                    							<div class="add-to-cart-buttons">
                    							    @if($related_product->current_stock == 0)
                    							    <button class="btn btn95" type="button" disabled> Out of Stock</button>
                    							    @else
                    							    <button class="btn btn25" onclick="addToCart(this, {{$related_product->id}}, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
                    							    <button class="btn btn70" onclick="buyNow(this, {{ $related_product->id }}, 1, 'full')" type="button"> Buy Now</button>
                    							    @endif
                    							</div>
                    						</article>
                					@endforeach
                    			</div>
                    			</div>
                            </div>
                        </div>
                    </section>
                </div>
           
            </div>
        </div>
    </section>
    <div class="modal fade" id="chat_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5">{{__('Any question about this product?')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="" action="{{ route('conversations.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $detailedProduct->id }}">
                    <div class="modal-body gry-bg px-3 pt-3">
                        <div class="form-group">
                            <input type="text" class="form-control mb-3" name="title" value="{{ $detailedProduct->name }}" placeholder="Product Name" required>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="8" name="message" required placeholder="Your Question">{{ route('product', $detailedProduct->slug) }}</textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Cancel')}}</button>
                        <button type="submit" class="btn btn-base-1 btn-styled">{{__('Send')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-zoom" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">{{__('Login')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <div class="card">
                                <div class="card-body px-4">
                                    <form class="form-default" role="form" action="{{ route('cart.login.submit') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <div class="input-group input-group--style-1">
                                                <input type="email" name="email" class="form-control" placeholder="{{__('Email')}}">
                                                <span class="input-group-addon">
                                                    <i class="text-md ion-person"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group input-group--style-1">
                                                <input type="password" name="password" class="form-control" placeholder="{{__('Password')}}">
                                                <span class="input-group-addon">
                                                    <i class="text-md ion-locked"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-md-6">
                                                <a href="#" class="link link-xs link--style-3">{{__('Forgot password?')}}</a>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <button type="submit" class="btn btn-styled btn-base-1 px-4">{{__('Sign in')}}</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card">
                                <div class="card-body px-4">
                                    @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)
                                        <a href="{{ route('social.login', ['provider' => 'google']) }}" class="btn btn-styled btn-block btn-google btn-icon--2 btn-icon-left px-4 my-4">
                                            <i class="icon fa fa-google"></i> {{__('Login with Google')}}
                                        </a>
                                    @endif
                                    @if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
                                        <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="btn btn-styled btn-block btn-facebook btn-icon--2 btn-icon-left px-4 my-4">
                                            <i class="icon fa fa-facebook"></i> {{__('Login with Facebook')}}
                                        </a>
                                    @endif
                                    @if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                                    <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="btn btn-styled btn-block btn-twitter btn-icon--2 btn-icon-left px-4 my-4">
                                        <i class="icon fa fa-twitter"></i> {{__('Login with Twitter')}}
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!------ Share Modal ------>
	<div class="modal fade" id="shareitemmodal">
		<div class="modal-dialog modal-sm modal-dialog-centered">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header p-2" style="background:#282563;">
					<h6 class="modal-title text-white">Share this product with friends</h6>
					<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
				</div>
				<!-- Modal body -->
				<div class="modal-body">
					<a href="https://www.facebook.com/sharer.php?u={{url('product/'.$detailedProduct->slug)}}&text={{$detailedProduct->meta_title}}" target="_blank" class="btn btn-block btn-social btn-facebook bg-white">
						<span class="fa fa-facebook text-white"></span> Facebook
					</a>
					<a href="https://twitter.com/share?url={{url('product/'.$detailedProduct->slug)}}&text={{$detailedProduct->meta_title}}" target="_blank" class="btn btn-block btn-social btn-twitter bg-white">
						<span class="fa fa-twitter text-white"></span> Twitter
					</a>
					<a href="https://www.linkedin.com/shareArticle?url={{url('product/'.$detailedProduct->slug)}}&title={{$detailedProduct->meta_title}}" target="_blank" class="btn btn-block btn-social btn-linkedin bg-white">
						<span class="fa fa-linkedin text-white"></span> linkedin
					</a>
					<a href="mailto:?subject=I want to recommend this product at Aldebazaar.com&body=I want to recommend this product at Aldebazaar.com%0A%0A{{$detailedProduct->name}}%0A%0ALearn more: {{url('product/'.$detailedProduct->slug)}} " target="_blank" class="btn btn-block btn-social btn-email bg-white">
						<span class="fa fa-envelope text-white"></span> Email
					</a>
					@php
						$msg="I want to recommend this product at Aldebazaar.com\n".$detailedProduct->name." \n Learn more: ".url('product/'.$detailedProduct->slug) ;
						$msg=urlencode($msg);
					@endphp
					<a href="whatsapp://send?text={{$msg}}" data-action="share/whatsapp/share" target="_blank" class="btn btn-block btn-social btn-whatsapp bg-white">
						<span class="fa fa-whatsapp text-white"></span> Share via Whatsapp
					</a>
				</div>
			</div>
		</div>
	</div>
    <!-------Return Policy Modal-------->
	<div class="modal fade" id="policymodal">
	  <div class="modal-dialog modal-dialog-centered">
		<!--Content-->
		<div class="modal-content">
		  <!--Header-->
		  <div class="modal-header">
			<p class="heading">Return Policy</p>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true" class="white-text">&times;</span>
			</button>
		  </div>

		  <!--Body-->
		  <div class="modal-body">
			<div class="row">
			  <div class="col-12">
				<p class="text-justify">{!! (\App\Category::find($detailedProduct->category_id)->policy_content) !!}</p>
			  </div>
			</div>
		  </div>
		  <!--Footer-->
		  <div class="modal-footer justify-content-center">
			<a href="{{ route('returnpolicy') }}" class="btn ml-2 btn-link strong-700" style="color:#007185" target="_blank">Read Full Return Policy</a>
		  </div>
		</div>
		<!--/.Content-->
	  </div>
	</div>
	<!-------Product Request Modal-------->
	<div class="modal fade" id="productRequest">
	  <div class="modal-dialog modal-lg modal-dialog-centered">
		<!--Content-->
		<div class="modal-content">
		  <!--Header-->
		  <div class="modal-header">
			<p class="heading">Product Request Form</p>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true" class="white-text">&times;</span>
			</button>
		  </div>

		  <!--Body-->
		  <div class="modal-body p-0">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3 d-none d-md-block">
						<div class="contact-info">
							<img src="{{asset('frontend/images/contact-image.png')}}" alt="image"/>
							<h2>Product Request</h2>
							<p>Fill up the form for product request.</p>
						</div>
					</div>
					<div class="col-12 col-md-9">
					<form  method="POST" id="product_request_form">
						@csrf
						<div class="contact-form">
							<div class="form-group">
								<h4 class="product-title mb-2 col-12">Product : {{$detailedProduct->name}}</h4>
								<input type="hidden" class="form-control" readonly value="{{$detailedProduct->slug}}" name="productname">
								<input type="text" name="phone" value="" class="d-none"/>
								<input type="text" name="subject" value="" class="d-none"/>
							</div>
							<div class="form-group">
							  <label class="control-label col-sm-2" for="name">Name*:</label>
							  <div class="col-sm-10">          
								<input type="text" class="form-control" id="name" placeholder="Enter your Name" name="name" required value="@if(Auth::user()) {{ Auth::user()->name }} @endif">
							  </div>
							</div>
							<div class="form-group">
							  <label class="control-label col-sm-2" for="mobile">Mobile:</label>
							  <div class="col-sm-10">          
								<input type="text" class="form-control" id="mobile" placeholder="Enter your Mobile Number" name="mobile" value="@if(Auth::user()) {{ Auth::user()->phone }} @endif">
							  </div>
							</div>
							<div class="form-group">
							  <label class="control-label col-sm-2" for="email">Email:</label>
							  <div class="col-sm-10">
								<input type="email" class="form-control" id="email" placeholder="Enter your email" name="email" value="@if(Auth::user()) {{ Auth::user()->email }} @endif">
							  </div>
							</div>
							<div class="form-group">
							  <label class="control-label col-sm-2" id="message">Message:</label>
							  <div class="col-sm-10">
								<textarea class="form-control" rows="5" id="message" name="message"></textarea>
							  </div>
							</div>
							<div class="form-group">        
							  <div class="col-sm-offset-2 col-sm-10">
								<button type="button" id="send" class="btn btn-default">Submit</button>
							  </div>
							</div>
						</div>
					</form>
					<div id="data" style="position: absolute;top: 50%;-ms-transform: translateY(-50%);transform: translateY(-50%);left:10%;font-size:20px"></div>
					</div>
				</div>
			</div>

		  </div>
		  <!--Footer-->
		</div>
		<!--/.Content-->
	  </div>
	</div>
	<style>
		.col-md-3{
			background: #ff9b00;
			padding: 3%!important;
			border-top-left-radius: 0.5rem!important;
			border-bottom-left-radius: 0.5rem!important;
		}
		.contact-info{
			margin-top:10%!important;
		}
		.contact-info p{font-size: 1.2rem;
			line-height: 1.4;}
		.contact-info img{
			margin-bottom: 15%!important;
		}
		.contact-info h2{
			margin-bottom: 10%!important;
			font-size: 1.6rem;
		}
		.col-md-9{
			background: #f1f1f1!important;
			padding: 3%!important;
			border-top-right-radius: 0.5rem!important;
			border-bottom-right-radius: 0.5rem!important;
		}
		.contact-form label{
			font-weight:600!important;
		}
		.contact-form button{
			background: #25274d!important;
			color: #fff!important;
			font-weight: 600!important;
			width: 25%!important;
		}
		.contact-form button:focus{
			box-shadow:none!important;
		}
	</style>

@endsection

@section('script')
    <script src="{{asset('frontend/js/jquery-scrollLock.min.js')}}"></script>
    <script src="{{asset('frontend/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.ez-plus.min.js')}}"></script>
    <script src="{{asset('frontend/js/bootstrap-tabcollapse.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
    		$('#share').share({
    			//networks: ['facebook','twitter','linkedin','tumblr','in1','stumbleupon','digg'],
    			networks: ['facebook','twitter','linkedin'],
    			theme: 'square'
    		});
            getVariantPrice();
    	});

        function CopyToClipboard(containerid) {
            if (document.selection) {
                var range = document.body.createTextRange();
                range.moveToElementText(document.getElementById(containerid));
                range.select().createTextRange();
                document.execCommand("Copy");

            } else if (window.getSelection) {
                var range = document.createRange();
                document.getElementById(containerid).style.display = "block";
                range.selectNode(document.getElementById(containerid));
                window.getSelection().addRange(range);
                document.execCommand("Copy");
                document.getElementById(containerid).style.display = "none";

            }
            showFrontendAlert('success', 'Copied');
        }

        function show_chat_modal(){
            @if (Auth::check())
                $('#chat_modal').modal('show');
            @else
                $('#login_modal').modal('show');
            @endif
        }
    var variation_order = JSON.parse('{!! $detailedProduct->variation_order !!}');
    var variation = JSON.parse('{!! $detailedProduct->variation !!}');
    var variation_product = JSON.parse('{!! $detailedProduct->variation_product !!}');
    var product_id = {{$detailedProduct->id}};
    $.each(variation_product,function(i,v) {
        if(v == product_id) {
            i = i.split("-");
            $.each(variation_order, function(ii,vv) {
                vv = vv.replaceAll(" ", "_");
                i[ii] = i[ii].replaceAll(" ", "_");
                //$('#'+vv+'-'+i[ii]).attr('checked','checked');
                $('#'+vv+'-'+i[ii]).parent('li').addClass('active');
            });
        }
    });
    var str = "";
    function checkProduct(value, attributeName) {
        str = "";
        $.each(variation_order, function(i,v) {
            if(i > 0) {
                str +="-";
            }
            if(v == attributeName) {
                str +=value;
            } else {
                v = v.replaceAll(" ", "_");
                str +=$('.attr-'+v).parent('li.active').data('value');
            }
        });
        if(variation_product[str] == 0) {
            $('.mssage').show();
        } else {
            $.post('{{ route('products.url') }}', { _token : '{{ @csrf_token() }}', product_id : variation_product[str]}, function(data){
            window.location = data;
        });
        }
        
    }
    
    </script>
    <style>
        .checkbox-alphanumeric input:checked ~ label {
            background:#273895;
            color:white;
        }
    </style>
    <link rel="stylesheet" href="{{asset('frontend/fancybox/jquery.fancybox.min.css')}}" />
	<script src="{{asset('frontend/fancybox/jquery.fancybox.min.js')}}"></script>

	
    <script src="{{asset('frontend/js/product-detail-js.js')}}"></script>
	<style>
		.fancybox-infobar{display:none!important;}
	</style>
	<script>
		$(document).ready(function() {
			var offer_owl = $('.offer-carousel');
			offer_owl.owlCarousel({
				nav: false,
				//loop: false,
				responsiveClass: true,
				responsive: {
					0: {
						items: 1,
						loop:true
					},
				}
			})
			$(".fancybox")
            .attr('rel', 'gallery')
            .fancybox({
                padding: 2,
                'loop': true
            });
		})
	</script>
	<script>
		$(function(){
			$("#send").click(function(){
				$.ajax({
					url:"{{ route('product.product_request') }}",
					type: 'post',
					dataType: 'json',
					data: $('form#product_request_form').serialize(),
					success: function( data ) {
						if(data.success==true)
						{
							$("#product_request_form")[0].reset();
							html="Your request has been sent to our administrator & will notify you once back in stock.";
							$("#data").html(html);
							$("#product_request_form").hide();
							setTimeout(function(){ $('#productRequest').modal('hide'); $("#data").hide();
							$("#product_request_form").show(); }, 10000);
						}
						else{
							console.log("error");
							return false;
						}
					}
				});
			});
		});
		$(document).on('click', '.decrease, .increase', function (e) {
			var $this = $(e.target);
				input = $($this).parent().find('.qty-input');
				v = $this.hasClass('decrease') ? input.val() - 1 : input.val() * 1 + 1;
				min = input.attr('data-min') ? input.attr('data-min') : 1;
				max = input.attr('data-max') ? input.attr('data-max') : false;
			if (v >= min) {
				if (!max == false && v > max) {
					return false
				} else input.val(v);
			}
			e.preventDefault();
		});
		$(document).on('change', '.qty-input', function (e) {
			var input = $(e.target);
				min = input.attr('data-min') ? input.attr('data-min') : 1;
				max = input.attr('data-max');
				v = input.val();
			if (v > max) input.val(max);
			else if (v < min) input.val(min);
		});
	</script>
	<script>
		$(document).ready(function()
		{
			// Configure/customize these variables.
			var showChar = 92;  // How many characters are shown by default
			var ellipsestext = "...";
			var moretext = "Show more >";
			var lesstext = "Show less";
		
			$('.more').each(function() {
				var content = $(this).html();
				if(content.length > showChar) {
					var c = content.substr(0, showChar);
					var h = content.substr(showChar, content.length - showChar);
					var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
					$(this).html(html);
				}
			});
			
			$('.moredescription').each(function() {
				var content = $(this).html();
				if(content.length > 320) {
					var c = content.substr(0, 320);
					var h = content.substr(320, content.length - 320);
					var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink text-right">' + moretext + '</a></span>';
					$(this).html(html);
				}
			});
			
			$(".morelink").click(function(){
				if($(this).hasClass("less")) {
					$(this).removeClass("less");
					$(this).html(moretext);
				} else {
					$(this).addClass("less");
					$(this).html(lesstext);
				}
				$(this).parent().prev().toggle();
				$(this).prev().toggle();
				return false;
			});
		});
	</script>
@endsection
