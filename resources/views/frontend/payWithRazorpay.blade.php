@php 
$previous = url()->previous();
if(stristr($previous, 'checkout/delivery_info')) {
$previous = route('checkout.store_delivery_info_with_shipping');
}
@endphp
 
<script src="{{ asset('frontend/js/vendor/jquery.min.js') }}"></script>   
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<center id="cancel" style="display:none;"><br /><br />
Payment cancelled, you will be redirect to back soon.
</center>
<script>
var options = {
    "key": "{{ env('RAZOR_KEY') }}", // Enter the Key ID generated from the Dashboard
    "amount": "{{round($order->grand_total) * 100}}", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "name": "{{ env('APP_NAME') }}",
    "description": "Cart Payment for Order #{{$order->code}}",
    "image": "{{ asset(\App\GeneralSetting::first()->logo) }}",
    "callback_url": "{!!route('payment.rozer')!!}",
    "prefill": {
        "name": "{{ Session::get('shipping_info')['name'] }}",
        "email": "{{ Session::get('shipping_info')['email'] }}"
    },
    "notes": {
        "order_id": "{{$order->code}}",
    },
    "theme": {
        "color": "#ff7529"
    },
    "modal": {
        "ondismiss": function(){
            $('#cancel').show();
            setTimeout(function() { 
                window.location = '{{ $previous }}'; }, 2000);
        }
    }
};

var rzp1 = new Razorpay(options);
        $(document).ready(function(){
           rzp1.open();
});
    
</script>