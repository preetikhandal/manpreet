@extends('layouts.blank')

@section('content')
<div class="text-center">
    <p class="h4 text-uppercase text-bold"><h2>{{ $exception->getMessage() }}</h2></p>
    <hr class="new-section-sm bord-no">
    <div class="pad-top"><a class="btn btn-primary" href="{{env('APP_URL')}}">{{__('Return Home')}}</a></div>
</div>
@endsection
