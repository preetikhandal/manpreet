@extends('layouts.app')

@section('content')

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Product Bulk Upload')}}</h3>
        </div>
        <div class="panel-body">
            <div class="alert" style="color: #004085;background-color: #cce5ff;border-color: #b8daff;margin-bottom:0;margin-top:10px;">
                <strong>Step 1:</strong>
                <p>1. Download the skeleton file and fill it with proper data.</p>
                <p>2. You can download the example file to understand how the data must be filled.</p>
                <p>3. Once you have downloaded and filled the skeleton file, upload it in the form below and submit.</p>
                <p>4. After uploading products you need to edit them and set product's images and choices.</p>
            </div>
            <br>
            <div class="">
                <a href="{{ asset('download/product_bulk_upload_with_img.xlsx') }}" download><button class="btn btn-primary">Download Bulk Upload CSV</button></a>
                <a href="{{ asset('download/product_bulk_update.xlsx') }}" download><button class="btn btn-primary">Download Bulk Update CSV</button></a>
            </div>
            <div class="alert" style="color: #004085;background-color: #cce5ff;border-color: #b8daff;margin-bottom:0;margin-top:10px;">
                <strong>Step 2:</strong>
                <p>1. Category,Sub category,Sub Sub category and Brand should be in numerical ids.</p>
                <p>2. You can download the pdf to get Category,Sub category,Sub Sub category and Brand id.</p>
            </div>
            <br>
            <div class="">
                <a href="{{ route('pdf.download_category') }}"><button class="btn btn-primary">Download Category</button></a>
                <a href="{{ route('pdf.download_sub_category') }}"><button class="btn btn-primary">Download Sub category</button></a>
                <a href="{{ route('pdf.download_sub_sub_category') }}"><button class="btn btn-primary">Download Sub Sub category</button></a>
                <a href="{{ route('pdf.download_brand') }}"><button class="btn btn-primary">Download Brand</button></a>
            </div>
            <br>
        </div>
    </div>
@php
/*
    <!--div class="panel">
        <div class="panel-heading">
            <h1 class="panel-title"><strong>{{__('Upload Product File')}}</strong></h1>
        </div>
        <div class="panel-body">
           <form class="form-horizontal" id="MyForm" action="{{ route('bulk_product_upload') }}" method="POST" enctype="multipart/form-data" onsubmit="return showmodal()">
                @csrf
				<input type="hidden" name="uploaderNumber" id="uploaderNumber" value="1">
                <div class="form-group">
                    <input type="file" class="form-control" name="bulk_file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <button class="btn btn-primary" type="submit">{{__('Upload CSV')}}</button>
                    </div>
                </div>
            </form>
			<div class="progressDiv">
			</div>
			<div class="ajax_success"></div>
			
			<div class="progress" style="display:none">
			  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div id="success"></div>
		</div>
    </div-->
    */
    @endphp
    
    <div class="panel">
        <div class="panel-heading">
            <h1 class="panel-title"><strong>Upload Product File with Img</strong></h1>
        </div>
        <div class="panel-body">
           <form class="form-horizontal" id="MyForm" action="{{ route('bulk_product_upload_img') }}" method="POST" enctype="multipart/form-data" onsubmit="return showmodal()">
                @csrf
				<input type="hidden" name="uploaderNumber" id="uploaderNumber" value="1">
                <div class="form-group">
                    <input type="file" class="form-control" name="bulk_file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <button class="btn btn-primary" type="submit">{{__('Upload CSV')}}</button> &nbsp; &nbsp;
                            <a href="{{ route('product_bulk_upload.process') }}" class="btn btn-primary" target="_blank">Process Product</a>
                            <a href="{{ route('product_bulk_upload.list') }}" class="btn btn-primary" target="_blank">Process Product List</a>
                    </div>
                </div>
            </form>
			<div class="progressDiv">
			</div>
			<div class="ajax_success"></div>
			
			<div class="progress" style="display:none">
			  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div id="success"></div>
		</div>
    </div>
    <div class="panel" style="background:#CCE5FF">
        <div class="panel-heading">
            <h1 class="panel-title" style="color:#EB3038"><strong>{{__('Update Product File')}}</strong></h1>
        </div>
        <div class="panel-body">
           <form class="form-horizontal" id="MyForm" action="{{ route('bulk_product_update') }}" method="POST" enctype="multipart/form-data" onsubmit="return showmodal()">
                @csrf
				<input type="hidden" name="uploaderNumber" id="uploaderNumber" value="1">
                <div class="form-group">
                    <input type="file" class="form-control" style="background:#CCE5FF" name="bulk_file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <button class="btn btn-primary" style="background:#282563;border-color:#282563!important" type="submit">{{__('Upload CSV')}}</button>
                    </div>
                </div>
            </form>
			<div class="progressDiv">
			</div>
			<div class="ajax_success"></div>
			
			<div class="progress" style="display:none">
			  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div id="success"></div>
		</div>
    </div>
    
	<!-- The Modal -->
	<div class="modal" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<!-- Modal body -->
				<div class="modal-body">
				    <center>
					 <i class="fa fa-spin fa-spinner fa-4x"></i>
					 <br />
					 Processing...
					 </center>
				</div>
			</div>
		</div>
	</div>
	
	<script>
	function showmodal() {
	    $('#myModal').modal('show');
	    return true;
	}
	</script>
@endsection

