
@extends('layouts.app')
@section('content')
   
<div class="container">
    <div class="card bg-light mt-3">
        <div class="card-header">
            Import User  Excel to database
        </div>
        <div class="card-body">
        
            <form action="{{ route('bulk-user-upload') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="user_bulk_file" class="form-control">
                <br>
                <button class="btn btn-success">Import User Data</button>
               
            </form>
        </div>
    </div>
</div>
@endsection
   
