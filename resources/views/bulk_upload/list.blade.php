@extends('layouts.app')

@section('content')
<br>

<div class="panel">
    <!--Panel heading-->
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">Product Upload Queue Manager :: Total Product {{$ProductUploadQueue->total()}}</h3>
        
        <div class="pull-right clearfix">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 100px;display:inline-block;">
                        <form action="{{route('product_bulk_upload.clear')}}" method="post" >
                            @csrf
                            <button class="btn btn-primary">Clear Queue</button>
                        </form>
                    </div>
                </div>
        </div>
    </div>


    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="20%">Name</th>
                    <th>SKU</th>
                    <th>thumb</th>
                    <th>Photos</th>
                    <th>Upload Try</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ProductUploadQueue as $key => $product)
                    <tr>
                        <td>{{ ($key+1) + ($ProductUploadQueue->currentPage() - 1)*$ProductUploadQueue->perPage() }}</td>
                        
                        <td>{{$product->name}}</td>
                        <td>{{$product->product_id}}</td>
                        <td>@if(empty($product->thumb))
                                        <img loading="lazy"  class="img-md" src="{{ asset('img/thumb.jpg')}}" alt="Image">
                                    @else
                                        <img loading="lazy"  class="img-md" src="{!! $product->thumb !!}" alt="Image">
                                    @endif
                        </td>
                        <td>@if(empty($product->photos))
                                <img loading="lazy"  class="img-md" src="{{ asset('img/thumb.jpg')}}" alt="Image">
                            @else
                                @php
                                    $photos = explode(";", $product->photos);
                                @endphp
                                @foreach($photos as $p)
                                    <img loading="lazy"  class="img-md" src="{!! $p !!}" alt="Image">
                                @endforeach
                            @endif
                        </td>
                        <td>{{$product->trycount}}</td>
                        <td>
                            <form action="{{route('product_bulk_upload.remove')}}" method="post" >
                                @csrf
                                <input type="hidden" name="id" value="{{ $product->id }}">
                                <button class="btn btn-primary">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $ProductUploadQueue->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
