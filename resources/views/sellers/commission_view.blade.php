
@extends('layouts.app')

@section('content')

<div class="row">
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all h-100">
            <h3 class="panel-title pull-left pad-no">Commission List</h3>
           
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Sub Category 2</th>
                    <th>Commission %</th>
                </tr>
                </thead>
                <tbody>
                        <tr>
                            <td>1</td>
                            <td>Inclusive</td>
                            <td>Shirts</td>
                            <td>Formal</td>
                            <td>Large</td>
                            <td>10</td>
                       
                        </tr>
                </tbody>
            </table>
            
        </div>
    </div>
</div>



@endsection
