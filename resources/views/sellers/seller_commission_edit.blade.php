@extends('layouts.app')

@section('content')

   <style>
    .form-group {
    margin-left: 0 !important;
    margin-right: 0 !important;
}
</style>
<div class="row">
	<form class="form form-horizontal mar-top" action="{{ route('editSellerCommission') }}" method="POST" enctype="multipart/form-data" id="">
		@csrf
		<input name="_method" type="hidden" >
		<input type="hidden" name="commissionId" value="{{$sellerComissionDetails->id}}">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Commission Panel</h3>
			</div>
				<div class="panel-body">
				    <div class="col-lg-8">
				        <div class="row">
				         <div class="col-lg-6 form-group">
				            <label>All Seller</label>
			                <select class="form-control demo-select2-placeholder" name="sellerId" >
                                  <option value="">All Sellers</option>
                                 @foreach (App\Seller::all() as $key => $seller)
                                    @if ($seller->user != null && $seller->user->shop != null)
                                        <option value="{{ $seller->user->id }}" @if($seller->user->id == $sellerComissionDetails->user_id) selected @endif >{{ $seller->user->shop->name }} ({{ $seller->user->name }})</option>
                                    @endif
                                @endforeach
                            </select>
    				    </div>
                        
    				  
    				    
    				    <div class="col-lg-6 form-group">
				            <label>Category</label>
			                <select class="form-control demo-select2-placeholder" name="category_id" id="category_id">
                                
                                @foreach($categories as $category)
											<option value="{{$category->id}}" @if($category->id == $sellerComissionDetails->category_id) selected @endif>{{__($category->name)}}</option>
										@endforeach
                            </select>
    				    </div>
    				        @php
    				          $subcatName = \App\SubCategory::findOrFail($sellerComissionDetails->subcategory_id);
    				          $SubsubcatName = \App\SubSubCategory::findOrFail($sellerComissionDetails->subsubcategory_id);
    				          
    				        @endphp
    				    <div class="col-lg-6 form-group">
				            <label>Sub Category</label>
			                <select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id">
                               @if($sellerComissionDetails->subcategory_id)
                                  
			                    <option value="{{$sellerComissionDetails->subcategory_id}}">{{$subcatName->name}}</option>
			                    @endif
                            </select>
    				    </div>
    				    <div class="col-lg-6 form-group">
				            <label>Sub Category 2</label>
			                <select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id">
                                @if($sellerComissionDetails->subsubcategory_id)
			                    <option value="{{$sellerComissionDetails->subsubcategory_id}}">{{$SubsubcatName->name}}</option>
			                    @endif
                            </select>
    				    </div>
    				    
    				    <div class="col-lg-6 form-group">
				            <label>Commission Type</label>
			                <select class="form-control demo-select2-placeholder" name="commissionType" id="marg">
                                <option value="0" @if($sellerComissionDetails->commission_type ==0 ) echo 'selected';@endif</option>Inclusive</option>
                                <option value="1" @if($sellerComissionDetails->commission_type ==1 ) echo 'selected';@endif>Exclusive</option>
                            </select>
    				    </div>
    				    
    				   
    				    
    				    @php
    				  
    				    if($sellerComissionDetails->commision_choose == 'r'){
    				       
    				       $commiionRupease = $sellerComissionDetails->commission;
    				    }else{
    				       $commisionPercentage =  $sellerComissionDetails->commission;
    				    }
    				    
    				    @endphp
    				     <div class="col-lg-6 form-group">
				            <label>Choose Commission</label>
			                <select class="form-control demo-select2-placeholder" name="commision_choose" id="commision_ch">
                                <option value="p" @if($sellerComissionDetails->commision_choose =='p' ) echo 'selected';@endif>Percentage</option>
                                <option value="r" @if($sellerComissionDetails->commision_choose =='r' ) echo 'selected';@endif>Rupease</option>
                            </select>
    				    </div>
    				    
    				    <div id="pe">
    				    <div class="col-lg-12 form-group">
				            <label>Commission in percentage</label>
			                <select class="form-control demo-select2-placeholder percent" name="commission" id="marg">
			                     @if($sellerComissionDetails->commission)
			                    <option value="{{$sellerComissionDetails->commission}}">{{$sellerComissionDetails->commission}}%</option>
			                    @endif
                               
                            </select>
    				    </div>
    				    </div>
    				    <div id="re" style="display: none;">
    				    <div class="col-lg-12 form-group">
				            <label>Commission in Rupease %</label>
			                 <input type="text" class="form-control" name="commision_rupease" value="{{$sellerComissionDetails->commission}}">
    				    </div>
                        </div>
                        
                        
    				</div>
				    </div>
    				
				</div>
				<div class="panel-footer text-center">
					<button type="submit" name="button" class="btn btn-purple">Submit</button>
				</div>
		</div>
	</form>
</div>

<div class="row">
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all h-100">
            <h3 class="panel-title pull-left pad-no">Commission List</h3>
           
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Sub Category 2</th>
                    <th>Commission %</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($sellerCommission as $sellerCommissionList)
                          @php
                             if($sellerCommissionList->commission_type ==0 ){
                               $sellerTYpe = 'Inclusive';
                             }else{
                                 $sellerTYpe = 'Exclusive';
                             }
                             $catName = '';
                             $subcatName = '';
                             $subsubcategoryName = '';
                             
                            
                             $catName =  \App\Category::where('id', $sellerCommissionList->category_id)->get();
                            
                             
                            
                             $subcatName =  \App\SubCategory::where('id', $sellerCommissionList->subcategory_id)->get();
                            
                             
                            
                             $subsubcategoryName =  \App\SubSubCategory::where('id', $sellerCommissionList->subsubcategory_id)->get();
                           
                             
                          @endphp
                        <tr>
                            <td>{{$sellerCommissionList->id}}</td>
                            <td>{{$sellerTYpe}}</td>
                             
                            <td> {{ $catName[0]->name ?? '' }}</td>
                            
                            <td>{{$subcatName[0]->name ?? ''}}</td>
                            
                            <td>{{$subsubcategoryName[0]->name ?? ''}}</td>
                            
                            <td>{{$sellerCommissionList->commission}}</td>
                             <td><a href="{{ route('sellercomission.show', encrypt($sellerCommissionList->id)) }}"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style="color:red;" href="#" onclick="deleteCommission({{$sellerCommissionList->id}})"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                       
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
</div>

<script>
    $(function(){
    var $select = $(".percent");
    for (i=1;i<=100;i++){
        $select.append($('<option></option>').val(i).html(i))
    }
});

$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

	$('#subsubcategory_id').on('change', function() {
	    // get_brands_by_subsubcategory();
		//get_attributes_by_subsubcategory();
	});
	
	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
			$('#subsubcategory_id').append($('<option>', {
				value: null,
				text: null
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}
	
	//Delete commission
	function deleteCommission(id){
	    
	    var r = confirm("Do you want to delete Seller Configuration!");
        if (r == true) {
              $.post('{{ route('deleteSellerCommisionConfig') }}',{_token:'{{ csrf_token() }}', sellerId:id}, function(data){
    		   
    		 location.reload();  
    		});
        } else {
          return false;
        }
	    
	    
	    
	}
	
	 //Choose commision
     
     
     $( "#commision_ch" ).change(function() {
       var type = $(this).val();
       //alert(tyep);
       if(type == 'r'){
           $('#re').show();
           $('#pe').hide();
       }
       
       if(type == 'p'){
           $('#pe').show();
           $('#re').hide();
       }
     });


</script>



@endsection
