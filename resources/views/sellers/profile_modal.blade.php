<div class="panel">
    <div class="panel-body">
        <div class="">
            <!-- Simple profile -->
            <div class="text-center">
                <div class="pad-ver">
                    <img src="{{ asset($seller->user->avatar_original) }}" class="img-lg img-circle" alt="Profile Picture">
                </div>
                <h4 class="text-lg text-overflow mar-no">{{ $seller->user->name }}</h4>
                <p class="text-sm text-muted">{{ $seller->user->shop->name }}</p>

                <div class="pad-ver btn-groups">
                    <a href="{{ $seller->user->shop->facebook }}" class="btn btn-icon demo-pli-facebook icon-lg add-tooltip" data-original-title="Facebook" data-container="body"></a>
                    <a href="{{ $seller->user->shop->twitter }}" class="btn btn-icon demo-pli-twitter icon-lg add-tooltip" data-original-title="Twitter" data-container="body"></a>
                    <a href="{{ $seller->user->shop->google }}" class="btn btn-icon demo-pli-google-plus icon-lg add-tooltip" data-original-title="Google+" data-container="body"></a>
                </div>
            </div>
            <hr>

            <!-- Profile Details -->
            <p><strong>Campany Name : </strong>{{ $seller->user->name }}</p>
            <p><strong>Campany Id : </strong>{{ $seller->user->id }}</p>
            <p><strong>Contact : </strong>{{$seller->user->shop->contact_person_phone}} ({{$seller->user->shop->contact_person_name}})</p>
            <p><strong>Address : </strong>{{ $seller->user->shop->address }}, {{ $seller->user->shop->city }}, {{ $seller->user->shop->state }}, {{ $seller->user->shop->pincode }}</p>
            <p><strong>Firm type</strong> {{ $seller->user->shop->firm_type }}</p>
            <p><strong>GSTIN</strong> {{ $seller->user->shop->gstin }}</p>
            <p><strong>Trade License No</strong> {{ $seller->user->shop->trade_license_no }}</p>
            <p><strong>Business Registration In</strong> {{ $seller->user->shop->business_registration_in}}</p>
            <p class="pad-ver text-main text-sm text-uppercase text-bold">{{__('Payout Info')}}</p>
            <p>{{__('Bank Name')}} : {{ $seller->bank_name }}</p>
            <p>{{__('Bank Acc Name')}} : {{ $seller->bank_acc_name }}</p>
            <p>{{__('Bank Acc Number')}} : {{ $seller->bank_acc_no }}</p>
            <p>{{__('Bank Routing Number')}} : {{ $seller->bank_routing_no }}</p>

            <br>

            <div class="table-responsive">
                <table class="table table-striped mar-no">
                    <tbody>
                    <tr>
                        <td>Total Products</td>
                        <td>{{ App\Product::where('user_id', $seller->user->id)->get()->count() }}</td>
                    </tr>
                    <tr>
                        <td>Total Orders</td>
                        <td>{{ App\OrderDetail::where('seller_id', $seller->user->id)->get()->count() }}</td>
                    </tr>
                    <tr>
                        <td>Total Sold Amount</td>
                        @php
                            $orderDetails = \App\OrderDetail::where('seller_id', $seller->user->id)->get();
                            $total = 0;
                            foreach ($orderDetails as $key => $orderDetail) {
                                if($orderDetail->order->payment_status == 'paid'){
                                    $total += $orderDetail->price;
                                }
                            }
                        @endphp
                        <td>{{ single_price($total) }}</td>
                    </tr>
                    <tr>
                        <td>Wallet Balance</td>
                        <td>{{ single_price($seller->user->balance) }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
