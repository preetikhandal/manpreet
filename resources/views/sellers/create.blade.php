@extends('layouts.app')

@section('content')
        @if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-lg-12 col-lg-offset-3" style="margin-left: 2px;">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Seller Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('sellers.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Name')}}" id="name" value="{{old('name')}}" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="email">{{__('Email Address')}}</label>
                    <div class="col-sm-9">
                        <input type="email" placeholder="{{__('Email Address')}}" id="email" value="{{old('email')}}" onkeyup="checkEmail()" name="email" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="password">{{__('Password')}}</label>
                    <div class="col-sm-9">
                        <input type="password" placeholder="{{__('Password')}}" id="password" name="password" class="form-control" required>
                    </div>
                </div>
                
            </div>
            <script>
                
        function checkEmail(){
            var email = $('#email').val();
           alert(email);
            if(email.length > 3){
                 $.ajax({
                 url:'{{ route('checkemail') }}',
                 method:'POST',
                 data:{_token:'{{ @csrf_token() }}','email':email},
                 async:false,
                 success:function(data){
                    // location.reload();
                 }
             });
            }else{
                alert('Please enter reason for denied order');
            }
            
            
            
        }
            </script>
            
            <!--Seller shop information -->
                  <div class="panel-body">
                 <center><strong>Shop Info</strong></center><br />
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="password">{{__('Shop Name ')}}</label>
                    <div class="col-sm-9">
                       <input type="text" class="form-control mb-3" value="{{old('Shop Name')}}" placeholder="{{__('Shop Name')}}" name="name" >
                    </div>
                </div>
                <div class="form-group">
                <div class="col-md-2 text-right">
                    <label>{{__('Contact Perosn Name')}} <span class="required-star">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control mb-3" placeholder="{{__('Contact Perosn Name')}}" name="contact_person_name" value="{{old('contact_person_name')}}" >
                </div>
                <div class="col-md-2 text-right">
                    <label class="text-right">{{__('Contact Perosn Phone')}} <span class="required-star">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="tel" class="form-control mb-3" placeholder="{{__('Contact Perosn Phone')}}" name="contact_person_phone" value="{{old('contact_person_phone')}}"  onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10">
                </div>
                </div>
                
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Address')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Address')}}" name="address" value="{{old('address')}}">
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Landmark')}} <span class="required-star"></span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Landmark')}}" name="landmark"  value="{{old('landmark')}}">
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label>{{__('City')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('City')}}" name="city" value="{{old('city')}}" >
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label>{{__('State')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('State')}}" name="state" value="{{old('state')}}">
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Pincode')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Pincode')}}" name="pincode" value="{{old('pincode')}}" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6">
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Firm Type')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Firm Type')}}" name="firm_type" value="{{old('firm_type')}}">
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label>{{__('GSTIN')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('GSTIN')}}" name="gstin" value="{{old('gstin')}}">
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label>{{__('License')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Business License')}}" name="trade_license_no" value="{{old('trade_license_no')}}">
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Business Registered In')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Business Registered In')}}" name="business_registration_in" value="{{old('business_registration_in')}}">
                                        </div>
                                    </div>
                                    
                                    
                                   
            </div>
            
                <!--Een Seller shop information-->
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
