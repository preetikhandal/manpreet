@extends('layouts.app')

@section('content')
<style>
 .mb-3 {
     margin-bottom:10px;
 }
</style>
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Seller Information')}}</h3>
        </div>
        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('sellers.update', $seller->id) }}" method="POST" enctype="multipart/form-data">
            <center><strong>Login Info</strong></center>
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="email">{{__('Email Address')}}</label>
                    <div class="col-sm-9">
                        <input type="email" placeholder="{{__('Email Address')}}" id="email" name="email" class="form-control" value="{{$seller->user->email}}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="password">{{__('Password')}}</label>
                    <div class="col-sm-9">
                        <input type="password" placeholder="{{__('Password')}}" id="password" name="password" class="form-control">
                    </div>
                </div>
            </div>
            @php $shop = \App\Shop::where('user_id',$seller->user_id)->first(); @endphp
            <div class="panel-body">
                 <center><strong>Shop Info</strong></center><br />
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="password">{{__('Shop Name')}}</label>
                    <div class="col-sm-9">
                        @if(isset($shop->name))
                       <input type="text" class="form-control mb-3" placeholder="{{__('Shop Name')}}" name="name" value="{{ $shop->name }}" required>
                       @else
                        <input type="text" class="form-control mb-3" placeholder="{{__('Shop Name')}}" name="name"  required>
                       @endif
                    </div>
                </div>
                <div class="form-group">
                    @if(isset($shop->contact_person_name))
                <div class="col-md-2 text-right">
                    <label>{{__('Contact Perosn Name')}} <span class="required-star">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control mb-3" placeholder="{{__('Contact Perosn Name')}}" name="contact_person_name" value="{{ old('contact_person_name', $shop->contact_person_name) }}" required>
                </div>
                @else
                
                <div class="col-md-2 text-right">
                    <label class="text-right">{{__('Contact Perosn Phone')}} <span class="required-star">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="tel" class="form-control mb-3" placeholder="{{__('Contact Perosn Phone')}}" name="contact_person_phone"   onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10" required>
                </div>
                @endif
                </div>
                
                                    <div class="row">
                                        
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Address')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            @if(isset($shop->address))
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Address')}}" name="address" value="{{ old('address', $shop->address) }}"  required>
                                            @else
                                             <input type="text" class="form-control mb-3" placeholder="{{__('Address')}}" name="address"   required>
                                            @endif
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Landmark')}} <span class="required-star"></span></label>
                                        </div>
                                        <div class="col-md-4">
                                            @if(isset($shop->landmark))
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Landmark')}}" name="landmark"  value="{{ old('landmark', $shop->landmark) }}">
                                            @else
                                             <input type="text" class="form-control mb-3" placeholder="{{__('Landmark')}}" name="landmark" >
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label>{{__('City')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            @if(isset($shop->city))
                                            <input type="text" class="form-control mb-3" placeholder="{{__('City')}}" name="city" value="{{ old('city', $shop->city) }}" required>
                                            @else
                                            <input type="text" class="form-control mb-3" placeholder="{{__('City')}}" name="city"  required>
                                            @endif
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label>{{__('State')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            @if(isset( $shop->state))
                                            <input type="text" class="form-control mb-3" placeholder="{{__('State')}}" name="state" value="{{ old('state', $shop->state) }}" required>
                                            @else
                                            <input type="text" class="form-control mb-3" placeholder="{{__('State')}}" name="state"  required>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Pincode')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            @if(isset($shop->pincode))
                                            <input type="tel" class="form-control mb-3" placeholder="{{__('Pincode')}}" name="pincode" value="{{ old('pincode', $shop->pincode) }}" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6" required>
                                           @else
                                           <input type="tel" class="form-control mb-3" placeholder="{{__('Pincode')}}" name="pincode"  onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6" required>
                                           @endif
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Firm Type')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            @if(isset($shop->firm_type))
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Firm Type')}}" name="firm_type" value="{{ old('firm_type', $shop->firm_type) }}">
                                            @else
                                             <input type="text" class="form-control mb-3" placeholder="{{__('Firm Type')}}" name="firm_type" >
                                            @endif
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label>{{__('GSTIN')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            @if(isset($shop->gstin))
                                            <input type="text" class="form-control mb-3" placeholder="{{__('GSTIN')}}" name="gstin" value="{{ old('gstin', $shop->gstin) }}">
                                            @else
                                             <input type="text" class="form-control mb-3" placeholder="{{__('GSTIN')}}" name="gstin" >
                                            @endif
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label>{{__('License')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            @if(isset($shop->trade_license_no))
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Business License')}}" name="trade_license_no" value="{{ old('trade_license_no', $shop->trade_license_no) }}">
                                            @else
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Business License')}}" name="trade_license_no">
                                            @endif
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Business Registered In')}} <span class="required-star">*</span></label>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            @if(isset($shop->business_registration_in))
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Business Registered In')}}" name="business_registration_in" value="{{ old('business_registration_in', $shop->business_registration_in) }}">
                                            @else
                                             <input type="text" class="form-control mb-3" placeholder="{{__('Business Registered In')}}" name="business_registration_in">
                                             @endif
                                        </div>
                                       
                                        <div class="col-md-2 text-right">
                                            <label>{{__('Order cancel credit limit')}} <span class="required-star">*</span></label>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            @if(isset($seller->order_cancel_credit))
                                            <input type="text" class="form-control mb-3" placeholder="{{__('order cancel credit')}}" name="order_cancel_credit" value="{{ old('order_cancel_credit', $seller->order_cancel_credit) }}">
                                            @else
                                             <input type="text" class="form-control mb-3" placeholder="{{__('order cancel credit')}}" name="order_cancel_credit" >
                                             @endif
                                        </div>
                                        
                                        
                                    </div>
                                    
                                    
                                   
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
                <!--<a href="{{route('sellers.sellercommission')}}" class="btn btn-purple">Next</a>-->
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
