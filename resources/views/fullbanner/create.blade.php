@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Banner Information')}}</h3>
    </div>

    <!--Horizontal Form-->
    <!--===================================================-->
    <form class="form-horizontal" action="{{ route('home_banners.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3" for="url">{{__('URL')}}</label>
                <div class="col-sm-9">
                    <input type="text" placeholder="{{__('URL')}}" id="url" name="url" class="form-control" required>
                </div>
            </div>
            <input type="hidden" name="position" value="{{ $position }}">
            <div class="form-group">
                <label class="col-sm-3" for="url">{{__('Display Position')}}</label>
                <div class="col-sm-9">
                    <select class="form-control demo-select2" name="displayposition" required>
                        <option value="Desktop">{{__('Desktop')}}</option>
                        <option value="Mobile">{{__('Mobile')}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label">{{__('Banner Images')}}</label>
                    <strong class="text-danger">(450px * 200px)</strong>
                </div>
                <div class="col-sm-9">
                    <div id="photo">

                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
        </div>
    </form>
    <!--===================================================-->
    <!--End Horizontal Form-->

</div>

<script type="text/javascript">

    $(document).ready(function(){

        $('.demo-select2').select2();

        $("#photo").spartanMultiImagePicker({
            fieldName:        'photo',
            maxCount:         1,
            rowHeight:        '200px',
            groupClassName:   'col-md-4 col-sm-9 col-xs-6',
            maxFileSize:      '',
            dropFileLabel : "Drop Here",
            onExtensionErr : function(index, file){
                console.log(index, file,  'extension err');
                alert('Please only input png or jpg type file')
            },
            onSizeErr : function(index, file){
                console.log(index, file,  'file size too big');
                alert('File size too big');
            }
        });
    });
	
	window.URL = window.URL || window.webkitURL;
	$("form").submit( function( e ) {
		var form = this;
		e.preventDefault(); //Stop the submit for now
									//Replace with your selector to find the file input in your form
		var fileInput = $(this).find("input[type=file]")[0],
			file = fileInput.files && fileInput.files[0];

		if( file ) {
			var img = new Image();

			img.src = window.URL.createObjectURL( file );

			img.onload = function() {
				var width = img.naturalWidth,
					height = img.naturalHeight;

				window.URL.revokeObjectURL( img.src );

				if( width == 450 && height == 200 ) {
					form.submit();
				}
				else {
					alert("Please Upload Given Size of Image.");
				}
			};
		}
		else { //No file was input or browser doesn't support client side reading
			form.submit();
		}

	});

</script>
