@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Subcategory Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('subcategories.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="icon">{{__('Icon')}} </label>
                    <div class="col-sm-9">
                        <input type="file" id="icon" name="icon" class="form-control">
                        <small class="text-danger">(50px * 50px)</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Category')}}</label>
                    <div class="col-sm-9">
                        <select name="category_id" required class="form-control demo-select2-placeholder">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{__($category->name)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
				<div class="form-group">
                    <label class="col-sm-3 control-label">{{__('Top Sku')}}<em> (Seperated by Comma)</em></label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="top_sku"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{__('Meta Title')}}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="meta_title" placeholder="{{__('Meta Title')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{__('Meta Description')}}</label>
                    <div class="col-sm-9">
                        <textarea name="meta_description" rows="8" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{__('Meta Keywords')}}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="meta_keywords[]" value="{{ old('meta_keywords[]') }}" placeholder="Type to add a keyword" data-role="tagsinput">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{__('Menu Display')}}</label>
                    <div class="col-sm-9">
                        <select name="display" class="form-control demo-select2-placeholder">
                            <option value="1">Show</option>
                            <option value="0">Hide</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
