@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('subcategories.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Subcategory')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Sub-Categories')}}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_subcategories" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder=" Type name & Enter">
                    </div>
                </div>
                <span style="float: left;margin-top: 6px;">OR</span>
				<div style="margin-left:20px" class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                       <select class="form-control" name="categorySearchID">
							<option value="">SELECT CATEGORY</option>
							@foreach(\App\Category::get() as $categories)
								<option value="{{$categories->id}}" >{{strtoupper($categories->name)}}</option>
							@endforeach
					   </select>
                    </div>
                </div>
				<input type="submit" name="searchdata" class="btn btn-primary" value="Search">
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Icon')}}</th>
                    <th>{{__('Subcategory')}}</th>
                    <th>{{__('Subcategory ID')}}</th>
                    <th>{{__('Category')}}</th>
                    <th>{{__('Category ID')}}</th>
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($subcategories as $key => $subcategory)
                    <tr>
                        <td>{{ ($key+1) + ($subcategories->currentPage() - 1)*$subcategories->perPage() }}</td>
                        <td>
                        	@if(!empty($subcategory->icon))
                        		<img loading="lazy"  class="img-md" src="{{ asset($subcategory->icon) }}" alt="{{__('Icon')}}">
                        	@else
                        		<img loading="lazy"  class="img-md" src="{{ asset('img/thumb.jpg') }}" alt="{{__('Icon')}}">
                        	@endif
                        </td>
                        <td>{{__($subcategory->name)}} </td>
                        <td>{{__($subcategory->id)}}</td>
                        <td>{{$subcategory->category->name}}</td>
                        <td>{{$subcategory->category->id}}</td>
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('subcategories.edit', encrypt($subcategory->id))}}">{{__('Edit')}}</a></li>
                                    <li><a onclick="confirm_modal('{{route('subcategories.destroy', $subcategory->id)}}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $subcategories->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
