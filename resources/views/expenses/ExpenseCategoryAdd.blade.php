@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	<!-- Main content -->
<div class="row">
    <div class="col-md-offset-2 col-md-8">
        <div class="panel">
            <div class="panel-title">
                <h3 class="panel-title">Add New Expense Category</h3>
                <div class="pull-right">
                <a href="{{url('admin/expenses/category')}}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> &nbsp;Back</a>
                </div>
            </div>
            <form class="form-horizontal" method="post" action="{{url('admin/expenses/categorysave')}}">
                {!! csrf_field() !!}
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 text-right control-label col-form-label">Category Name</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="name" required  placeholder="Category Name" value="{{ old('name') }}" >
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="panel-footer text-right">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
</div>
<!-- /.row -->
@endsection
@section('extra')


@endsection
