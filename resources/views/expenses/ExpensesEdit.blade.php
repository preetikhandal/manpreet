@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="row">
    <div class="col-sm-12">
        <a href="{{url('admin/expenses')}}" class="btn btn-danger pull-right">{{__('Back')}}</a> 
    </div>
</div>

			<div class="row">
				<div class="col-md-12">
                    <form class="form-horizontal r-separator" action="{{url('admin/expenses/update/'.$Expenses->ExpenseID)}}" method="post">
					<div class="panel">
						<div class="panel-title">
							<h3 >Expense Information</h3>
                            <div class="pull-right">
							<a href="javascript:window.history.back()" class="btn btn-flat float-right btn-danger"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
                            </div>
						</div>
							{!! csrf_field() !!}
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="category" class="col-md-3 text-right control-label col-form-label">Expense Category</label>
                                    <div class="col-md-9">
                                        <select class="form-control select2" id="category" name="category" required style="width: 100%;">
											<option value="">Select Expense Category</option>
											@foreach($categories as $C)
												<option value="{{$C->ECategoryID}}" @if($C->ECategoryID==$Expenses->ECategoryID) selected @endif>{{$C->ECategoryName}}</option>
											@endforeach
										</select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                    </div>
                                    <div class="col-md-3">
                                        Expense Name <br />
                                        <input type="text" name="expensename" class="form-control" id="expensename" required placeholder="Expense Name" value="{{ $Expenses->ExpenseName }}">
                                    </div>
                                    <div class="col-sm-3">
                                        Invoice Date <br />
                                        <input type="text" autocomplete="off" class="form-control datepicker" name='invoicedate' id="invoicedate" placeholder="dd/mm/yyyy" value="{{Carbon\Carbon::parse($Expenses->ExpenseInvoiceDate)->format('d/m/Y')}}">
                                    </div>
                                    <div class="col-sm-3">
                                        Invoice Number <br />
                                        <input type="text" name="invoiceno" class="form-control" id="invoiceno"  placeholder="Invoice Number" value="{{ $Expenses->ExpenseInvoiceNo }}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                    </div>
                                    <div class="col-md-2">
                                        IGST <br />
                                        <input type="text" name="igst" class="form-control" id="igst" required placeholder="igst" value="{{ $Expenses->igst }}">
                                    </div>
                                    <div class="col-sm-2">
                                        CGST <br />
                                        <input type="text" name="cgst" class="form-control" id="cgst" required placeholder="cgst" value="{{ $Expenses->cgst }}">
                                    </div>
                                    <div class="col-sm-2">
                                        SGST <br />
                                        <input type="text" name="sgst" class="form-control" id="sgst" required placeholder="sgst" value="{{ $Expenses->sgst }}">
                                    </div>
                                    <div class="col-sm-3">
                                        Invoice Amount <br />
                                        <input type="number" name="invoiceamount" class="form-control" id="invoiceamount" placeholder="Invoice Amount" value="{{ $Expenses->ExpensesInvoiceAmount }}"/>
                                    </div>
                                </div>
                            </div>
							<!-- /.card-body -->
						</div>
						<!-- /.panel-->
                    
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{__('Expense Items')}}</h3>
                            </div>
                                <div class="panel-body">
                                    <table class="table table-striped" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>{{__('Expenses Type')}}</th>
                                                <th>{{__('Category')}}</th>
                                                <th>{{__('Item Description')}}</th>
                                                <th>{{__('Unit Price')}}</th>
                                                <th>{{__('Quantity')}}</th>
                                                <th>{{__('Sub Total')}}</th>
                                                <th>{{__('Options')}}</th>
                                            </tr>
                                        </thead>
                                    <tbody id="expensesItem">
                                        @php
                                        $category = '<option value="">Select Category</option>';
                                        foreach(\App\Category::all() as $c) {
                                         $category = $category.'<option value="'.$c->id.'">'.$c->name.'</option>';
                                        }
                                        @endphp
                                        @foreach($ExpensesDetails as $key => $ED)
                                        <tr id="row_{{$key}}">
                                            <input type="hidden" name="expenses_details_id[]" id="expenses_details_id_{{$key}}" value="{{$ED->expenses_details_id}}">
                                            <input type="hidden" name="current_stock[]" id="current_stock_{{$key}}" value="{{$ED->quantity}}">
                                            @if($ED->expenses_type == "Item Expenses")
                                                <td><input type="text" class="form-control" name="expenses_type[]" value="Item Expenses" readonly></td>
                                                <td><input type="hidden" name="category_id[]" value="0">-</td>
                                                <td><input type="text" class="form-control" name="item_description[]" value="{{$ED->item_description}}" placeholder="Item Description"></td>
                                           @else
                                                <td><input type="text" class="form-control" name="expenses_type[]" value="Product Expenses" readonly></td>
                                                <td><select class="form-control" name="category_id[]" id="category_id_{{$key}}" required onchange="get_product_by_category(this.value, {{$key}})">{!! $category !!}</select></td>
                                                <td><select class="form-control" id="item_description_{{$key}}" name="item_description[]" placeholder="Item Description"></select></td>
                                         
                                           @endif
                                                <td><input type="text" class="form-control" name="unit_price[]" value="{{$ED->unit_price}}"  placeholder="Unit Price"></td>
                                                <td><input type="text" class="form-control" name="quantity[]" value="{{$ED->quantity}}" placeholder="Quantity"></td>
                                                <td><input type="text" class="form-control" name="subtotal[]" value="{{$ED->sub_total}}" placeholder="Sub Total"></td>
                                                <td><button type="button" class="btn btn-danger" onclick="removeItem({{$key}})">Delete</button></td>
                                        </tr>
                                        @endforeach
                                        @if(count($ExpensesDetails) == 0)
                                        <tr>
                                            <td colspan="7">
                                                <strong>Select below dropdown to add row.</strong>
                                            </td>
                                        </tr>
                                        @endif
                                    </tbody>
                                    <tfooter>
                                        <tr>
                                            <td colspan="7">
                                                <select id="expensesType" class="form-control">
                                                    <option value="">Select to add row</option>
                                                    <option value="ProductExpenses">Product Expenses</option>
                                                    <option value="ItemExpenses">Item Expenses</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </tfooter>
                                    </table>
                                    <div id="expensesdetailremovediv">
                                    </div>
                                </div>
                            
							 <div class="panel-footer text-right">
                                <button class="btn btn-purple" type="submit">{{__('Update')}}</button>
                            </div>
                        </div>
                        
						</form>
					</div>
				</div>
			<!-- /.row -->
	@endsection
	@section('script')
	
	<script>
        var category = "";
            category += '                <option value="">Select Category</option>';
                                        @foreach(\App\Category::all() as $category)
            category += '                       <option value="{{$category->id}}">{{__($category->name)}}</option>';
                                        @endforeach
             
            var n = {{count($ExpensesDetails) + 1}};
            $('#expensesType').change(function() {
                if($('#expensesType').val() == "ItemExpenses") {
                    html = '<tr id="row_'+n+'">';
                    html += '<input type="hidden" name="expenses_details_id[]" id="expenses_details_id_'+n+'" value="0">';
                    html += '<td><input type="text" class="form-control" name="expenses_type[]" value="Item Expenses" readonly></td>';
                    html += '<td><input type="hidden" name="category_id[]" value="0">-</td>';
                    html += '<td><input type="text" class="form-control" name="item_description[]" placeholder="Item Description"></td>';
                    html += '<td><input type="text" class="form-control" name="unit_price[]" placeholder="Unit Price"></td>';
                    html += '<td><input type="text" class="form-control" name="quantity[]" placeholder="Quantity"></td>';
                    html += '<td><input type="text" class="form-control" name="subtotal[]" placeholder="Sub Total" value="0"></td>';
                    html += '<td><button class="btn btn-danger" onclick="removeItem('+n+')">Delete</button></td>';
                    if(n == 1) {
                        $('#expensesItem').html("");
                    }
                    $('#expensesItem').append(html);
                }
                
                if($('#expensesType').val() == "ProductExpenses") {
                    html = '<tr id="row_'+n+'">';
                    html += '<input type="hidden" name="expenses_details_id[]" id="expenses_details_id_'+n+'" value="0">';
                    html += '<td><input type="text" class="form-control" name="expenses_type[]" value="Product Expenses" readonly></td>';
                    html += '<td><select class="form-control" name="category_id[]" id="category_id" required onchange="get_product_by_category(this.value, '+n+')">'+category+'</select></td>';
                    html += '<td><select class="form-control" id="item_description_'+n+'" name="item_description[]" placeholder="Item Description"></select></td>';
                    html += '<td><input type="text" class="form-control" name="unit_price[]" placeholder="Unit Price"></td>';
                    html += '<td><input type="text" class="form-control" name="quantity[]" placeholder="Quantity"></td>';
                    html += '<td><input type="text" class="form-control" name="subtotal[]" placeholder="Sub Total" value="0"></td>';
                    html += '<td><button class="btn btn-danger" onclick="removeItem('+n+')">Delete</button></td>';
                    if(n == 1) {
                        $('#expensesItem').html("");
                    }
                    $('#expensesItem').append(html);
                    $('#item_description_'+n).select2();
                }
                n++;
                $('#expensesType').val("");
            });
        
            function get_product_by_category(category_id, ele_id, selectid=0) {
                $.post('{{ route('products.get_products_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                    $('#item_description_'+ele_id).html(null);
                        $('#item_description_'+ele_id).append($('<option>', {
                            value: "",
                            text: 'Select Products'
                        }));
                    for (var i = 0; i < data.length; i++) {
                        $('#item_description_'+ele_id).append($('<option>', {
                            value: data[i].id,
                            text: data[i].name
                        }));
                    }
                    if(selectid > 0) {
                        $('#item_description_'+ele_id).val(selectid);
                    }
                    $('#item_description_'+ele_id).trigger("change");
                });
            }
            function removeItem(rowid) {
                edid = $('#expenses_details_id_'+rowid).val();
                if(edid > 0) {
                    html = '<input type="hidden" name="deleted_expenses_details_ids[]" value="'+edid+'">';
                    $('#expensesdetailremovediv').append(html);
                }
                $('#row_'+rowid).remove();
            }
        
         @foreach($ExpensesDetails as $key => $ED)
        //    $('#category_id_{{$key}}').select2();
            $('#category_id_{{$key}}').val({{$ED->category_id}});
          //  $('#category_id_{{$key}}').trigger("change");
            get_product_by_category({{$ED->category_id}}, {{$key}}, {{$ED->product_id}});
        @endforeach
	$(function () {
		//Initialize Select2 Elements
		$('.select2').select2()
	});
	</script>
    <script>
        $(function () {
            // INITIALIZE DATEPICKER PLUGIN
            $('.datepicker').datepicker({
                clearBtn: true,
                format: "dd/mm/yyyy",
                autoclose : true
            });
        });
    </script>
	@endsection
