@extends('layouts.app')

@section('content')
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
<div class="row">
    <div class="col-sm-12">
        <a href="{{url('admin/expenses')}}" class="btn btn-danger pull-right">{{__('Back')}}</a> 
    </div>
</div>


    <form class="form-horizontal" action="{{url('admin/expenses/save')}}" method="post">
        @csrf
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Expense Information')}}</h3>
    </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="category" class="col-md-3 text-right control-label col-form-label">Expense Category</label>
                <div class="col-md-9">
                    <select class="form-control select2" id="category" name="category" required style="width: 100%;">
                        <option value="">Select Expense Category</option>
                        @foreach($categories as $C)
                            <option value="{{$C->ECategoryID}}">{{$C->ECategoryName}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                </div>
                <div class="col-md-3">
                    Expense Name <br />
                    <input type="text" name="expensename" class="form-control" id="expensename" required placeholder="Expense Name" value="{{ old('expensename') }}">
                </div>
                <div class="col-sm-3">
                    Invoice Date <br />
                    <input type="text" autocomplete="off" class="form-control datepicker" name='invoicedate' id="invoicedate" placeholder="dd/mm/yyyy" >
                </div>
                <div class="col-sm-3">
                    Invoice Number <br />
                    <input type="text" name="invoiceno" class="form-control" id="invoiceno"  placeholder="Invoice Number" value="{{ old('invoiceno') }}" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                </div>
                <div class="col-md-2">
                    IGST <br />
                    <input type="text" name="igst" class="form-control" id="igst" required placeholder="igst" value="{{ old('igst',0) }}">
                </div>
                <div class="col-sm-2">
                    CGST <br />
                    <input type="text" name="cgst" class="form-control" id="cgst" required placeholder="cgst" value="{{ old('cgst',0) }}">
                </div>
                <div class="col-sm-2">
                    SGST <br />
                    <input type="text" name="sgst" class="form-control" id="sgst" required placeholder="sgst" value="{{ old('sgst',0) }}">
                </div>
                <div class="col-sm-3">
                    Invoice Amount <br />
                    <input type="number" name="invoiceamount" class="form-control" id="invoiceamount" placeholder="Invoice Amount" value="{{ old('Invoice Amount') }}"/>
                </div>
            </div>
        </div>
</div>
        
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Expense Items')}}</h3>
    </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>{{__('Expenses Type')}}</th>
                        <th>{{__('Category')}}</th>
                        <th>{{__('Item Description')}}</th>
                        <th>{{__('Unit Price')}}</th>
                        <th>{{__('Quantity')}}</th>
                        <th>{{__('Sub Total')}}</th>
                        <th>{{__('Options')}}</th>
                    </tr>
                </thead>
            <tbody id="expensesItem">
                <tr>
                    <td colspan="7">
                        <strong>Select below dropdown to add row.</strong>
                    </td>
                </tr>
            </tbody>
            <tfooter>
                <tr>
                    <td colspan="7">
                        <select id="expensesType" class="form-control">
                            <option value="">Select to add row</option>
                            <option value="ProductExpenses">Product Expenses</option>
                            <option value="ItemExpenses">Item Expenses</option>
                        </select>
                    </td>
                </tr>
            </tfooter>
            </table>
        </div>
</div>
        <script>
            var category = "";
            category += '                <option value="">Select Category</option>';
                                        @foreach(\App\Category::all() as $category)
            category += '                       <option value="{{$category->id}}">{{__($category->name)}}</option>';
                                        @endforeach
             
            var n = 1;
            $('#expensesType').change(function() {
                if($('#expensesType').val() == "ItemExpenses") {
                    html = '<tr id="row_'+n+'">';
                    html += '<td><input type="text" class="form-control" name="expenses_type[]" value="Item Expenses" readonly></td>';
                    html += '<td><input type="hidden" name="category_id[]" value="0">-</td>';
                    html += '<td><input type="text" class="form-control" name="item_description[]" placeholder="Item Description"></td>';
                    html += '<td><input type="text" class="form-control" name="unit_price[]" placeholder="Unit Price"></td>';
                    html += '<td><input type="text" class="form-control" name="quantity[]" placeholder="Quantity"></td>';
                    html += '<td><input type="text" class="form-control" name="subtotal[]" placeholder="Sub Total" value="0"></td>';
                    html += '<td><button class="btn btn-danger" onclick="removeItem('+n+')">Delete</button></td>';
                    if(n == 1) {
                        $('#expensesItem').html("");
                    }
                    $('#expensesItem').append(html);
                }
                
                if($('#expensesType').val() == "ProductExpenses") {
                    html = '<tr id="row_'+n+'">';
                    html += '<td><input type="text" class="form-control" name="expenses_type[]" value="Product Expenses" readonly></td>';
                    html += '<td><select class="form-control" name="category_id[]" id="category_id" required onchange="get_product_by_category(this.value, '+n+')">'+category+'</select></td>';
                    html += '<td><select class="form-control" id="item_description_'+n+'" name="item_description[]" placeholder="Item Description"></select></td>';
                    html += '<td><input type="text" class="form-control" name="unit_price[]" placeholder="Unit Price"></td>';
                    html += '<td><input type="text" class="form-control" name="quantity[]" placeholder="Quantity"></td>';
                    html += '<td><input type="text" class="form-control" name="subtotal[]" placeholder="Sub Total" value="0"></td>';
                    html += '<td><button class="btn btn-danger" onclick="removeItem('+n+')">Delete</button></td>';
                    if(n == 1) {
                        $('#expensesItem').html("");
                    }
                    $('#expensesItem').append(html);
                    $('#item_description_'+n).select2();
                }
                n++;
                $('#expensesType').val("");
            });
            function get_product_by_category(category_id, ele_id) {
                var category_id = $('#category_id').val();
                $.post('{{ route('products.get_products_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                    $('#item_description_'+ele_id).html(null);
                        $('#item_description_'+ele_id).append($('<option>', {
                            value: "",
                            text: 'Select Products'
                        }));
                    for (var i = 0; i < data.length; i++) {
                        $('#item_description_'+ele_id).append($('<option>', {
                            value: data[i].id,
                            text: data[i].name
                        }));
                    }
                    $('#item_description_'+ele_id).trigger("change");
                });
            }
            function removeItem(rowid) {
                $('#row_'+rowid).remove();
            }
        </script>
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Payment Information (Optional)')}}</h3>
    </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="paidamount" class="col-md-3 text-right control-label">Paid Amount</label>
                <div class="col-md-9">
                    <input type="number" name="paidamount" class="form-control" id="paidamount" placeholder="0" value="{{ old('paidamount') }}">
                    <span id="amountErr" style="color:red;font-weight:600"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="outstandingamount" class="col-sm-3 text-right control-label">Outstanding Amount</label>
                <div class="col-sm-9">
                    <input type="number" name="outstandingamount" class="form-control" id="outstandingamount"  placeholder="0" value="{{ old('outstandingamount') }}" min="0">
                </div>
            </div>
            <div class="form-group">
                <label for="paymentdate" class="col-sm-3 text-right control-label col-form-label">Payment Date</label>
                <div class="col-sm-9">
                     <input type="text" autocomplete="off" class="form-control datepicker" name='paymentdate' id="paymentdate" placeholder="dd/mm/yyyy" >
                </div>
            </div>
            <div class="form-group">
                <label for="paymentmethod" class="col-md-3 text-right control-label col-form-label">Payment Method</label>
                <div class="col-md-9">
                    <select class="form-control select2" id="paymentmethod" name="paymentmethod" style="width: 100%;">
                        <option value="">Select Payment Method</option>
                            <option value="Cash">Cash</option>
                            <option value="Card">Card</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Other">Other</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="refID" class="col-md-3 text-right control-label col-form-label">Payment Ref ID</label>
                <div class="col-md-9">
                    <input type="text" name="refID" class="form-control" id="refID" placeholder="Payment Ref ID" value="{{ old('refID') }}"/>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
        </div>
</div>
</form>

	@endsection
	@section('script')
	
	<script>
	$(function () {
		//Initialize Select2 Elements
		$('.select2').select2()
	});
	</script>
    <script>
        $(function () {
            // INITIALIZE DATEPICKER PLUGIN
            $('.datepicker').datepicker({
                clearBtn: true,
                format: "dd/mm/yyyy",
                autoclose: true,
            });
        });
    </script>
	<script>
		$( function() {
			$("#invoiceamount").blur(function(){
				TotalAmnt=$(this).val();
				$("#outstandingamount").val(TotalAmnt);
			});
			
			$("#paidamount").blur(function(){
				PaidAmnt=$(this).val();
				invoiceamount=$("#invoiceamount").val();
				
				if(parseInt(PaidAmnt)>parseInt(invoiceamount))
				{
					$("#amountErr").html("Paid Amount can not be greater than Invoice Amount.");
					$("#paidamount").val("");
					$("#paidamount").focus();
				}
				else{
					$("#amountErr").html("");
					Outstanding_Amnt=invoiceamount-PaidAmnt;
					$("#outstandingamount").val(Outstanding_Amnt);				
				}
			});
		});
	</script>
	@endsection
