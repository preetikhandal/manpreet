@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{url('admin/expenses/create')}}" class="btn btn-info pull-right">{{__('Add New Expense')}}</a> 
    </div>
</div>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Expeses')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Expense Category')}}</th>
                    <th>{{__('Expense Name')}}</th>
                    <th>{{__('Expense Invoice No.')}}</th>
                    <th>{{__('Expense Invoice Date')}}</th>
                    <th>{{__('Invoice Amount')}}</th>
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($Expenses as $key => $E)
                    <tr>
                        <td>{{ $Expenses->firstItem() + $key }}</td>
                        <td>{{\App\ExpensesCategory::find($E->ECategoryID)->ECategoryName}}</td>
                        <td>{{$E->ExpenseName}}</td>
                        <td>{{$E->ExpenseInvoiceNo}}</td>
                        <td>{{Carbon\Carbon::parse($E->ExpenseInvoiceDate)->format('d-M-Y')}}</td>
                        <td>{{$E->ExpensesInvoiceAmount}}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{url('admin/expenses/view/')}}/{{$E->ExpenseID}}" class="btn btn-sm btn-primary btn-flat">View</a>&nbsp;
                                <a href="{{url('admin/expenses/edit/')}}/{{$E->ExpenseID}}" class="btn btn-sm btn-warning btn-flat">Edit</a>&nbsp;
                                <form method="post" action="{{url('admin/expenses/delete')}}" onclick="return confirm('Do you want to delete this Expense? Also Expense payment details will be deleted.');" style="display:inline-block">
                                    {!!csrf_field()!!}
                                    <input type="hidden" name="id" value="{{$E->ExpenseID}}">
                                    <button type="submit" class="btn btn-danger btn-sm  btn-flat">Delete</button>
                                </form>
                             </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $Expenses->appends(request()->except('page'))->links() }}
            </div>
        </div>
    </div>
</div>
@endsection