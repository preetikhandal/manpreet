@extends('layouts.app')

@section('content')
	<!-- Main content -->
	@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="panel">
						
						<div class="panel-title">
							<h3>Expense Information</h3>
                            <div class="pull-right">
							<a href="javascript:window.history.back()" class="btn btn-flat float-right btn-danger"> <i class="fa fa-arrow-left"></i> &nbsp;Back</a>
                            </div>
						</div>
						<form class="form-horizontal" action="{{url('admin/expenses/savepayment')}}" method="post">
							{!! csrf_field() !!}
							<div class="panel-body">
								<div class="form-group">
									<label for="ExpenseID" class="col-md-3 text-right control-label col-form-label">Expense</label>
									<div class="col-md-9">
										@if($SelectedExpense==NULL)
											<select class="form-control select2" id="ExpenseID" name="ExpenseID" required style="width: 100%;" onchange="getAmount(this.value)">
												<option value="">Select Expense</option>
												@foreach($Expenses as $E)
													<option value="{{$E->ExpenseID}}">{{$E->ExpenseName}} - Invoice Amount &#8377;{{$E->ExpensesInvoiceAmount}} - Balance Amount &#8377; {{$E->OutstandingAmount}} </option>
												@endforeach
											</select>
										@else
											<select class="form-control select2" id="ExpenseID" name="ExpenseID" required style="width: 100%;" onchange="getAmount(this.value)">
												<option value="">Select Expense</option>
												@foreach($Expenses as $E)
													<option value="{{$E->ExpenseID}}" @if($E->ExpenseID==$SelectedExpense->ExpenseID) selected @endif>{{$E->ExpenseName}} - Invoice Amount &#8377;{{$E->ExpensesInvoiceAmount}} - Balance Amount &#8377; {{$E->OutstandingAmount}} </option>
												@endforeach
											</select>
										@endif
									</div>
								</div>
								<div class="form-group">
									<label for="outstandingamount" class="col-sm-3 text-right control-label col-form-label">Outstanding Amount</label>
									<div class="col-sm-9">
										@if($SelectedExpense==NULL)
											<input type="text" readonly name="outstandingamount" class="form-control" id="outstandingamount"  placeholder="0" value="{{ old('outstandingamount') }}" min="0">
										@else
											<input type="text" readonly name="outstandingamount" class="form-control" id="outstandingamount"  placeholder="0" value="{{ $SelectedExpense->OutstandingAmount }}" min="0">
										@endif
									</div>
								</div>
								<div class="form-group">
									<label for="payingamount" class="col-md-3 text-right control-label col-form-label">Paying Amount</label>
									<div class="col-md-9">
										<input type="number" name="payingamount" class="form-control" id="payingamount" placeholder="0" value="{{ old('payingamount') }}">
										<span id="amountErr" style="color:red;font-weight:600"></span>
									</div>
								</div>
								<div class="form-group">
									<label for="paymentdate" class="col-sm-3 text-right control-label col-form-label">Payment Date</label>
									<div class="col-sm-9">
											<input type="text" autocomplete="off" class="form-control datepicker" name='paymentdate' id="paymentdate" placeholder="dd/mm/yyyy" >
									</div>
								</div>
								<div class="form-group">
									<label for="paymentmethod" class="col-md-3 text-right control-label col-form-label">Payment Method</label>
									<div class="col-md-9">
										<select class="form-control select2" id="paymentmethod" name="paymentmethod" style="width: 100%;">
											<option value="">Select Payment Method</option>
												<option value="Cash">Cash</option>
												<option value="Card">Card</option>
												<option value="Cheque">Cheque</option>
												<option value="Other">Other</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="refID" class="col-md-3 text-right control-label col-form-label">Payment Ref ID</label>
									<div class="col-md-9">
										<input type="text" name="refID" class="form-control" id="refID" placeholder="Payment Ref ID" value="{{ old('refID') }}"/>
									</div>
								</div>
							</div>
							<!-- /.card-body -->
							<div class="card-body">
								<div class="row">
									<div class="col-md-12 text-center">
										<button type="submit" class="btn btn-purple">Save</button>
                                        <br />
                                        &nbsp;
									</div>
								</div>
							</div>
							</form>
						</div>
						<!-- /.card -->
					</div>
				</div>
			<!-- /.row -->
	@endsection
	@section('script')
	
	<script>
	$(function () {
		//Initialize Select2 Elements
		$('.select2').select2()
	});
	</script>
    <script>
        $(function () {
            // INITIALIZE DATEPICKER PLUGIN
            $('.datepicker').datepicker({
                clearBtn: true,
                format: "dd/mm/yyyy"
            });
        });
    </script>
	<script>
		var InvoiceAmount = []
		@foreach($Expenses as $E)
			InvoiceAmount[{{$E->ExpenseID}}] = {{$E->OutstandingAmount}};
		@endforeach
		function getAmount(v)
		{
			$('#outstandingamount').val(InvoiceAmount[v]);
		}
	</script>
	<script>
		$(function(){
			$("#payingamount").blur(function(){
				payingamount=$(this).val();
				outstandingamount=$("#outstandingamount").val();
				if(parseInt(payingamount)>parseInt(outstandingamount))
				{
					$("#amountErr").html("Paying Amount can not be greater than Outstanding Amount.");
					$("#payingamount").val("");
					$("#payingamount").focus();
				}
				else{
					$("#amountErr").html("");				
				}
			});
		});
	</script>
	@endsection
