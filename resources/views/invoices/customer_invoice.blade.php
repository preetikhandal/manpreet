<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <meta http-equiv="Content-Type" content="text/html;"/>
    <meta charset="UTF-8">
	<style media="all">
		*{
			margin: 0;
			padding: 0;
			line-height: 1.3;
			font-family: sans-serif;
			color: #333542;
		}
		body{
			font-size: .875rem;
		}
		.gry-color *,
		.gry-color{
			color:#878f9c;
		}
		table{
			width: 100%;
		}
		table th{
			font-weight: normal;
		}
		table.padding th{
			padding: .5rem .7rem;
		}
		table.padding td{
			padding: .7rem;
		}
		table.sm-padding td{
			padding: .2rem .7rem;
		}
		.border-bottom td,
		.border-bottom th{
			border-bottom:1px solid #eceff4;
		}
		.text-left{
			text-align:left;
		}
		.text-right{
			text-align:right;
		}
		.small{
			font-size: .85rem;
		}
		
		.currency{
            font-family: DejaVu Sans;
		}
	</style>
</head>
<body>
	<div>

		@php
			$generalsetting = \App\GeneralSetting::first();
		@endphp

		<div style="background: #eceff4;padding: 1.5rem;">
			<table>
				<tr>
					<td>
					    @php
					    /*
						@if($generalsetting->logo != null)
							<img loading="lazy" src="{{ asset($generalsetting->logo) }}" height="40" style="display:inline-block;">
						@else
							<img loading="lazy"  src="{{ asset('email/logo_2.png') }}" height="40" style="display:inline-block;">
						@endif
						*/
						@endphp
						<img loading="lazy"  src="{{ asset('email/logo_2.png') }}" height="40" style="display:inline-block;">
					</td>
					<td style="font-size: 2.5rem;" class="text-right strong">Order Confirmation</td>
				</tr>
			</table>
			<table>
				<tr>
					<td style="font-size: 1.2rem;" class="strong">{{ $generalsetting->site_name }}</td>
					<td class="text-right"></td>
				</tr>
				<tr>
					<td class="gry-color small">{{ $generalsetting->address }}</td>
					<td class="text-right"></td>
				</tr>
				<tr>
					<td class="gry-color small">Email: {{ $generalsetting->email }}</td>
					<td class="text-right small"><span class="gry-color small">Order ID:</span> <span class="strong">{{ $order->code }}</span></td>
				</tr>
				<tr>
					<td class="gry-color small">Phone: {{ $generalsetting->phone }}</td>
					<td class="text-right small"><span class="gry-color small">Order Date:</span> <span class=" strong">{{ date('d-m-Y', $order->date) }}</span></td>
				</tr>
			</table>

		</div>

		<div style="padding: 1.5rem;padding-bottom: 0">
			<table>
				@php
					$shipping_address = json_decode($order->shipping_address);
				@endphp
				<tr><td class="strong small gry-color">Bill to:</td></tr>
				<tr><td class="strong">{{ $shipping_address->name }}</td></tr>
				<tr><td class="gry-color small">{{ $shipping_address->address }}, {{ $shipping_address->city }}, {{ $shipping_address->state }},  {{ $shipping_address->postal_code }}</td></tr>
				@if(isset($shipping_address->email))
				<tr><td class="gry-color small">Email: {{ $shipping_address->email }}</td></tr>
				@endif
				<tr><td class="gry-color small">Phone: {{ $shipping_address->phone }}</td></tr>
			</table>
		</div>

	    <div style="padding: 1.5rem;">
			<table class="padding text-left small border-bottom">
				<thead>
	                <tr class="gry-color" style="background: #eceff4;">
	                    <th width="45%">Product Name</th>
	                    <th width="10%">Qty</th>
	                    <th width="15%">MRP Price</th>
	                    <th width="15%">Discounted Price</th>
	                    <th width="20%" class="text-right">Total</th>
	                </tr>
				</thead>
				<tbody class="strong">
				    @php
				    $save = 0;
				    @endphp
	                @foreach ($order->orderDetails as $key => $orderDetail)
		                @if ($orderDetail->product != null)
							<tr class="">
								<td>{{ $orderDetail->product->name }}</td>
								<td class="gry-color">{{ $orderDetail->quantity }}</td>
								<td class="gry-color currency">{{ single_price(($orderDetail->price + $orderDetail->discount + $orderDetail->tax)/$orderDetail->quantity) }}</td>
								<td class="gry-color currency">{{ single_price(($orderDetail->price + $orderDetail->tax)/$orderDetail->quantity) }}</td>
			                    <td class="text-right currency">{{ single_price($orderDetail->price+$orderDetail->tax) }}</td>
							</tr>
							@php
							    $save += $orderDetail->discount;
							@endphp
		                @endif
					@endforeach
	            </tbody>
			</table>
		</div>

	    <div style="padding:0 1.5rem;">
	        <table style="width: 40%;margin-left:auto;" class="text-right sm-padding small strong">
		        <tbody>
			        <tr>
			            <th class="gry-color text-left">Sub Total</th>
			            <td class="currency">{{ single_price($order->orderDetails->sum('price')) }}</td>
			        </tr>
			        <tr>
			            <th class="gry-color text-left">Shipping Cost</th>
			            <td class="currency">{{ single_price($order->orderDetails->sum('shipping_cost')) }}</td>
			        </tr>
			        <tr>
			            <th class="gry-color text-left">Total Tax</th>
			            <td class="currency">{{ single_price($order->orderDetails->sum('tax')) }}</td>
			        </tr>
			        
			        <tr class="border-bottom">
			            <th class="gry-color text-left">Coupon Discount</th>
			            <td class="currency">{{ single_price($order->coupon_discount) }}</td>
			        </tr>
			        @if($order->cart_shipping > 0)
			        <tr class="border-bottom">
			            <th class="gry-color text-left">Delivery Charges</th>
			            <td class="currency">{{ single_price($order->cart_shipping) }}</td>
			        </tr>
			        @endif
			        <tr>
			            <th class="text-left strong">Grand Total</th>
			            <td class="currency">{{ single_price($order->grand_total) }}</td>
			        </tr>
			        @if($save > 0)
			        <tr>
			            <th class="text-left strong">You saved</th>
			            <td class="currency">{{ single_price($save) }}</td>
			        </tr>
			        @endif

					
		        </tbody>
		    </table>
	    </div>

	</div>
</body>
</html>