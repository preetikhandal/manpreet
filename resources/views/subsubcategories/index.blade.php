@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('subsubcategories.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Sub Subcategory')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Sub-Sub-categories')}}</h3>
        <div  class="pull-right clearfix">
			<form class="" id="sort_categories" action="" method="GET">
                <div style="margin-left:20px" class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                       <select class="form-control" name="categorySearchID" id="categorySearchID">
							<option value="">SELECT CATEGORY</option>
							@foreach(\App\Category::get() as $categories)
								<option value="{{$categories->id}}" >{{strtoupper($categories->name)}}</option>
							@endforeach
					   </select>
                    </div>
                </div>
				<div style="margin-left:5px" class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                       <select class="form-control" name="subcategorySearchID" id="subcategorySearchID">
							<option value="">SELECT SUB CATEGORY</option>
					   </select>
                    </div>
                </div>
				
				<input type="submit" name="searchdata" class="btn btn-primary" value="Search">
		   </form>
		</div>
        
		<div class="pull-left clearfix"><br/>
            <form class="" id="sort_subsubcategories" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder=" Type name & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Icon kk')}}</th>
                    <th>{{__('Sub Subcategory')}}</th>
                    <th>{{__('Sub Subcategory ID')}}</th>
                    <th>{{__('Subcategory')}}</th>
                    <th>{{__('Subcategory ID')}}</th>
                    <th>{{__('Category')}}</th>
                    <th>{{__('Category ID')}}</th>
                    {{-- <th>{{__('Attributes')}}</th> --}}
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($subsubcategories as $key => $subsubcategory)
                    <tr>
                        <td>{{ ($key+1) + ($subsubcategories->currentPage() - 1)*$subsubcategories->perPage() }}</td>
                        <td>
                        	@if(!empty($subsubcategory->icon))
                        		<img loading="lazy"  class="img-md" src="{{ asset($subsubcategory->icon) }}" alt="{{__('Icon')}}">
                        	@else
                        		<img loading="lazy"  class="img-md" src="{{ asset('img/thumb.jpg') }}" alt="{{__('Icon')}}">
                        	@endif
                        </td>
                        <td>{{__($subsubcategory->name)}}</td>
                        <td>{{__($subsubcategory->id)}}</td>
                        <td>{{$subsubcategory->subcategory->name}}</td>
                        <td>{{$subsubcategory->subcategory->id}}</td>
                        <td>{{$subsubcategory->subcategory->category->name}}</td>
                        <td>{{$subsubcategory->subcategory->category->id}}</td>
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('subsubcategories.edit', encrypt($subsubcategory->id))}}">{{__('Edit')}}</a></li>
                                    <li><a onclick="confirm_modal('{{route('subsubcategories.destroy', $subsubcategory->id)}}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $subsubcategories->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
	<script type="text/javascript">
		$(function(){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
		});
	</script>
	<script>
		$(function(){
			$("#categorySearchID").change(function(){
				var categorySearchID=$(this).val();
				$.ajax({
					url:"{{route('ajax.getsubcategory')}}",
					type: 'POST',
					data:"categorySearchID="+categorySearchID,
					success: function(data) {
						$("#subcategorySearchID").html(data);
					}
				});
				
			});
		})
	</script>

@endsection
