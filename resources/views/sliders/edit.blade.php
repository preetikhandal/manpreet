<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('SLider Information')}}</h3>
    </div>
@if($errors->any())
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
    <!--Horizontal Form-->
    <!--===================================================-->
    <form class="form-horizontal" action="{{ route('sliders.update', $Slider->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="PATCH">
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3" for="url">{{__('URL')}}</label>
                <div class="col-sm-9">
                    <input type="text" id="url" name="url" placeholder="http://example.com/" class="form-control" value="{{$Slider->link}}" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label">{{__('Desktop Slider Image')}}</label>
                    <strong class="text-danger">(1341px*330px) Exiting image will be overwritten</strong>
                </div>
                <div class="col-sm-9">
                    <div id="photos">

                    </div>
                </div>
            </div>
			<div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label">{{__('Mobile Slider Image')}}</label>
                    <strong class="text-danger">(1080px*450px) Exiting image will be overwritten</strong>
                </div>
                <div class="col-sm-9">
                    <div id="mobile_photos">

                    </div>
                </div>
            </div>
			<div class="form-group">
                <div class="col-sm-6">
                    <label class="control-label">{{__('Desktop Slider')}}</label>
                    <img src="{{asset($Slider->photo)}}" style="width:100%"/>
                </div>
                <div class="col-sm-9">
                    <label class="control-label">{{__('Mobile Slider')}}</label>
					<img src="{{asset($Slider->mobile_photo)}}" style="width:100%" />
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-purple" type="submit">{{__('Update')}}</button>
        </div>
    </form>
    <!--===================================================-->
    <!--End Horizontal Form-->

</div>


<script type="text/javascript">
    $(document).ready(function(){
        $("#photos").spartanMultiImagePicker({
            fieldName:        'photo',
            maxCount:         1,
            rowHeight:        '200px',
            groupClassName:   'col-md-4 col-sm-9 col-xs-6',
            maxFileSize:      '',
            dropFileLabel : "Drop Here",
            onExtensionErr : function(index, file){
                console.log(index, file,  'extension err');
                alert('Please only input png or jpg type file')
            },
            onSizeErr : function(index, file){
                console.log(index, file,  'file size too big');
                alert('File size too big');
            }
        });
		//Mobile slider
		$("#mobile_photos").spartanMultiImagePicker({
            fieldName:        'mobile_photo',
            maxCount:         1,
            rowHeight:        '200px',
            groupClassName:   'col-md-4 col-sm-9 col-xs-6',
            maxFileSize:      '',
            dropFileLabel : "Drop Here",
            onExtensionErr : function(index, file){
                console.log(index, file,  'extension err');
                alert('Please only input png or jpg type file')
            },
            onSizeErr : function(index, file){
                console.log(index, file,  'file size too big');
                alert('File size too big');
            }
        });
		
    });
	
</script>

