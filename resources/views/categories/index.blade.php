@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('categories.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Category')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no">{{__('Categories')}}</h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_categories" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder=" Type name & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('ID')}}</th>
                    <th>{{__('Menu Banner')}}</th>
                    <th>{{__('Desktop Icon')}}</th>
                    <th>{{__('Mobile Icon')}}</th>
                    <th>{{__('Featured')}}</th>
                    <th>{{__('Position')}}</th>
                    <th>{{__('Discount(%)')}}</th>
                   <!-- <th>{{__('Commission')}}</th>-->
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $key => $category)
                    <tr>
                        <td>{{ ($key+1) + ($categories->currentPage() - 1)*$categories->perPage() }}</td>
                        <td>{{__($category->name)}}</td>
                        <td>{{$category->id}}</td>
                        <td>
							@if(!empty($category->menu_banner))
								<img loading="lazy"  class="img-md" src="{{ asset($category->menu_banner) }}" alt="{{__('Menu banner')}}">
							@else
								<img loading="lazy"  class="img-md" src="{{ asset('img/thumb.jpg') }}" alt="{{__('Menu banner')}}">
							@endif
						</td>
                        <td>
							@if(!empty($category->banner))
								<img loading="lazy"  class="img-md" src="{{ asset($category->banner) }}" alt="{{__('banner')}}">
							@else
								<img loading="lazy"  class="img-md" src="{{ asset('img/thumb.jpg') }}" alt="{{__('banner')}}">
							@endif
						</td>
                        <td>
							@if(!empty($category->icon))
								<img loading="lazy"  class="img-xs" src="{{ asset($category->icon) }}" alt="{{__('icon')}}">
							@else
								<img loading="lazy"  class="img-md" src="{{ asset('img/thumb.jpg') }}" alt="{{__('icon')}}">
							@endif
						</td>
                        <td><label class="switch">
                            <input onchange="update_featured(this)" value="{{ $category->id }}" type="checkbox" <?php if($category->featured == 1) echo "checked";?> >
                            <span class="slider round"></span></label></td>
                        <td>{{ $category->position }}</td>
                        <td>{{ $category->discount }}</td>
                       <!--<td>{{ $category->commision_rate }} %</td>-->
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('categories.edit', encrypt($category->id))}}">{{__('Edit')}}</a></li>
                                    <li><a onclick="confirm_modal('{{route('categories.destroy', $category->id)}}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                {{ $categories->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script type="text/javascript">
        function update_featured(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('categories.featured') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Featured categories updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }
    </script>
@endsection
