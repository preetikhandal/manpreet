@extends('layouts.app')
@section('content')
    @if($errors->any())
		<div class="alert alert-danger">
			<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
    <div class="col-lg-8 col-lg-offset-2">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{__('Coupon Information Adding')}}</h3>
            </div>

            <form class="form-horizontal" action="{{ route('coupon.store') }}" method="POST" enctype="multipart/form-data">
            	@csrf
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="name">{{__('Coupon Type')}}</label>
                        <div class="col-lg-9">
                            <select name="coupon_type" id="coupon_type" class="form-control demo-select2" onchange="coupon_form()" required>
                                <option value="">Select One</option>
                                <option value="user_code">{{__('For User Referral (Wallet)')}}</option>
                                <option value="product_base">{{__('For Products (Instant)')}}</option>
                                <option value="cart_base">{{__('For Total Orders (Instant)')}}</option>
                                <option value="instant">{{__('For Category (Instant)')}}</option>
                                <option value="category_base">{{__('For Category (Wallet)')}}</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="name">{{__('Coupon Uses')}}</label>
                        <div class="col-lg-9">
                            <select name="coupon_use" id="coupon_use" class="form-control demo-select2" required>
                                <option value="">Select One</option>
                                <option value="unlimited">Unlimited Use Per User</option>
                                <option value="single">Single Use Per User</option>
                            </select>
                        </div>
                    </div>

                    <div id="coupon_form">

                    </div>

                <div class="panel-footer text-right">
                    <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
                </div>
            </form>

        </div>
    </div>

@endsection
@section('script')

<script type="text/javascript">

    function coupon_form(){
        var coupon_type = $('#coupon_type').val();
		$.post('{{ route('coupon.get_coupon_form') }}',{_token:'{{ csrf_token() }}', coupon_type:coupon_type}, function(data){
            $('#coupon_form').html(data);

            $('#demo-dp-range .input-daterange').datepicker({
                startDate: '-0d',
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true
        	});
		});
    }

</script>

@endsection
