<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Redis;

Route::get("debug112", function() { 
    $start_time = microtime(true);
    $redis = Redis::connection();
    $product = $redis->get('product:id:424');
    if($product == null) {
        $product = \App\Product::where('id',424)->first();
        $redis->set('product:id:424', json_encode($product));
    } else {
        $product = json_decode($product);
    }
    $end_time = microtime(true); 
    $execution_time = ($end_time - $start_time); 
    echo "Using Redis : $execution_time <br />"; 
       $redis->del('product:id:424');
    //dd($product->description);
$start_time = microtime(true);
                            $product = \App\Product::find(424);
                            $end_time = microtime(true); 
                              
                            // Calculate script execution time 
                            $execution_time = ($end_time - $start_time); 
                              echo "MySQL Select ALL Data Using Find (Laravel): $execution_time <br />"; 
                            
                                                    $start_time = microtime(true);
                                                        $product = \App\Product::where('id',424)->first();
                                                        $end_time = microtime(true); 
                              
                            // Calculate script execution time 
                            $execution_time = ($end_time - $start_time); 
                             
                              echo "MySQL Select ALL Data Using First (Laravel): $execution_time <br />"; 
                                                    $start_time = microtime(true);
                                                        $product = \App\Product::select('id', 'published')->where('id',424)->first();
                                                        $end_time = microtime(true); 
                              
                            // Calculate script execution time 
                            $execution_time = ($end_time - $start_time); 
                             
                             echo "MySQL Select Selected coloumn Data Using First (Laravel): $execution_time <br />"; 
                                                    $start_time = microtime(true);
                                                    $id = 424;
                                                        $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                                                                return \App\Product::find($id);
                                                        });
                                                        $end_time = microtime(true); 
                              
                            // Calculate script execution time 
                            $execution_time = ($end_time - $start_time); 
                             
                              echo "Getting Redis cached Data : $execution_time <br />"; 
                                                    $start_time = microtime(true);
                                                    $id = 424;
                                                        $product = Cache::rememberForever('Product:Published:'.$id, function () use ($id) {
                                                                return \App\Product::select('id', 'published')->where('id',$id)->first();
                                                        });
                                                        $end_time = microtime(true); 
                              
                            // Calculate script execution time 
                            $execution_time = ($end_time - $start_time); 
                             
                              echo "Getting Redis cached Data : $execution_time <br />"; 
d(Request::all());
d(Session::all());
d(Auth::User()); 
}); 

Route::get("sitemap.xml", 'SitemapController@sitemap');
Route::get("sitemaps/{fileName}", 'SitemapController@sitemaps');
//Feedback Form layout

Route::get("feedback-form", 'CustomerController@feedbackForm')->name('feedback-form');
Route::get("review-list/{slug}", 'ProductController@reviewList')->name('review-list');
Route::post("savefeedback/form", 'CustomerController@savefeedback')->name('savefeedback.form');
//Add pharmacy order
Route::get("pharmacy-enquiry", 'CustomerController@pharmacykForm')->name('pharmacy-enquiry');
Route::post("add-pharmacy-form", 'CustomerController@pharmacykFormAdd')->name('add-pharmacy-form');



Route::get('/testmail', function() {
    $dataEmail = array('email' => 'deeps17may@gmail.com', 'name' => 'Arjun');
    
    if(isset($dataEmail['lname'])){
       $dataEmail['lname'] = $dataEmail['name']." ".$dataEmail['lname']; 
    }
    $order = \App\Order::find(28);
echo  $render = view('emails.order_confirmation_client', ['array' => array('order' => $order)])->render();
die();
    Mail::send('emails.order_confirmation_client', ['array' => array('order' => $order)], function ($m) use ($dataEmail) {
            $m->to($dataEmail['email'], $dataEmail['name'])->subject('Alde Bazaar :: Order Confirmation');
        });
});

Route::get('/cron', function() {
     $exitCode = Artisan::call('calculateReferralCommision');
     return 'Done';
 });

Route::get('/cron2', function() {
     $exitCode = Artisan::call('calculateCategoryCashBack');
     return 'Done';
 });
 
//Clear route cache:
 Route::get('/route-cache', function() {
     $exitCode = Artisan::call('route:cache');
     return 'Routes cache cleared';
 });

 //Clear config cache:
 Route::get('/config-cache', function() {
     $exitCode = Artisan::call('config:cache');
     return 'Config cache cleared';
 }); 
 //Clear config cache:
 Route::get('/config-clear', function() {
     $exitCode = Artisan::call('config:clear');
     return 'Config cache cleared';
 }); 


// Clear application cache:
 Route::get('/clear-cache', function() {
     $exitCode = Artisan::call('cache:clear');
     return 'Application cache cleared';
 });

 // Clear view cache:
 Route::get('/view-clear', function() {
     $exitCode = Artisan::call('view:clear');
     return 'View cache cleared';
 });
Route::get('/testemail', function() {
    return view('emails.order_confirmation_client');
});
//demo
Route::get('/testm', function() {
   return view('frontend.test'); 
});
Route::get('/testm1', function() {
   return view('frontend.test_bak'); 
});
Route::get('/demo/cron_1', 'DemoController@cron_1');
Route::get('/demo/cron_2', 'DemoController@cron_2');


Route::get('/resize', 'HomeController@imgResize')->name('img.resize');

Route::get('/rebuildredis', 'HomeController@rebuildRedis');

Auth::routes(['verify' => true]);
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('/language', 'LanguageController@changeLanguage')->name('language.change');
Route::post('/currency', 'CurrencyController@changeCurrency')->name('currency.change');

Route::get('/social-login/redirect/{provider}', 'Auth\LoginController@redirectToProvider')->name('social.login');
Route::get('/social-login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('social.callback');
Route::post('/social-login/apple/callback', 'Auth\LoginController@handleProviderCallback')->name('social.callback.apple');
Route::get('/users/redirect', 'HomeController@userRedirect')->name('users.redirect');
Route::get('/users/login', 'HomeController@login')->name('user.login');
Route::get('/users/registration', 'HomeController@registration')->name('user.registration');
//Route::post('/users/login', 'HomeController@user_login')->name('user.login.submit');
Route::post('/users/login/cart', 'HomeController@cart_login')->name('cart.login.submit');
Route::post('/users/login/generate/otp', 'HomeController@login_otp_generate')->name('generate.otp.login');
Route::post('/users/login', 'HomeController@user_login')->name('user.login.validate');

//Check pincode
Route::post('/ajax/checkpicode', array('as' => 'checkpicode', 'uses' => 'HomeController@checkpicode'));
Route::post('/ajax/getpincode', array('as' => 'getpincode', 'uses' => 'HomeController@getpincode'));
Route::post('/ajax/pharmacyStatus',  'CustomerController@updatePharmacy');



Route::post('/subcategories/get_subcategories_by_category', 'SubCategoryController@get_subcategories_by_category')->name('subcategories.get_subcategories_by_category');
Route::post('/subsubcategories/get_subsubcategories_by_subcategory', 'SubSubCategoryController@get_subsubcategories_by_subcategory')->name('subsubcategories.get_subsubcategories_by_subcategory');
Route::post('/subsubcategories/get_brands_by_subsubcategory', 'SubSubCategoryController@get_brands_by_subsubcategory')->name('subsubcategories.get_brands_by_subsubcategory');
Route::post('/subsubcategories/get_attributes_by_subsubcategory', 'SubSubCategoryController@get_attributes_by_subsubcategory')->name('subsubcategories.get_attributes_by_subsubcategory');

//Home Page
Route::get('/', 'HomeController@index')->name('home');
Route::post('/home/section/featured', 'HomeController@load_featured_section')->name('home.section.featured');
Route::post('/home/section/best_selling', 'HomeController@load_best_selling_section')->name('home.section.best_selling');
Route::post('/home/section/brand_banner', 'HomeController@load_brand_banner_section')->name('home.section.brand_banner');
Route::post('/home/section/home_categories', 'HomeController@load_home_categories_section')->name('home.section.home_categories');
Route::post('/home/section/home_categories2', 'HomeController@load_home_categories_section2')->name('home.section.home_categories2');
Route::post('/home/section/home_categories3', 'HomeController@load_home_categories_section3')->name('home.section.home_categories3');
Route::post('/home/section/best_sellers', 'HomeController@load_best_sellers_section')->name('home.section.best_sellers');
//category dropdown menu ajax call
Route::post('/categoryp/nav-element-list', 'HomeController@get_category_items')->name('category.elements');

//Flash Deal Details Page
Route::get('/flash-deal/{slug}', 'HomeController@flash_deal_details')->name('flash-deal-details');

Route::get('/feature-product-list', 'HomeController@featureproductList')->name('feature-product-list');

Route::get('/best-selling', 'HomeController@bestSelling')->name('best-selling');


Route::get('/customer-products', 'CustomerProductController@customer_products_listing')->name('customer.products');
Route::get('/customer-products?subsubcategory={subsubcategory_slug}', 'CustomerProductController@search')->name('customer_products.subsubcategory');
Route::get('/customer-products?subcategory={subcategory_slug}', 'CustomerProductController@search')->name('customer_products.subcategory');
Route::get('/customer-products?category={category_slug}', 'CustomerProductController@search')->name('customer_products.category');
Route::get('/customer-products?city={city_id}', 'CustomerProductController@search')->name('customer_products.city');
Route::get('/customer-products?q={search}', 'CustomerProductController@search')->name('customer_products.search');
Route::get('/customer-product/{slug}', 'CustomerProductController@customer_product')->name('customer.product');
Route::get('/customer-packages', 'HomeController@premium_package_index')->name('customer_packages_list_show');



Route::get('/product/{slug}', 'HomeController@product')->name('product');
Route::get('/products', 'HomeController@listing')->name('products');
Route::post('/products/url', 'ProductController@productURL')->name('products.url');
Route::get('/category/{category_slug}', 'HomeController@category_view')->name('products.category');
Route::get('/custom/{category_slug}/{custom_name_slug}', 'HomeController@product_custom')->name('products.custom');
Route::get('/subcategory/{subcategory_slug}', 'HomeController@category_view')->name('products.subcategory1');
Route::get('/category/{category_slug}/{subcategory_slug}', 'HomeController@category_view')->name('products.subcategory');
Route::get('/category/{category_slug}/{subcategory_slug}/{subsubcategory_slug}', 'HomeController@category_view')->name('products.subsubcategory');
//Route::get('/search?subsubcategory={subsubcategory_slug}', 'HomeController@search')->name('products.subsubcategory1');
//Route::get('/search?brand={brand_slug}', 'HomeController@category_view')->name('products.brand');
Route::post('/product/variant_price', 'HomeController@variant_price')->name('products.variant_price');
Route::get('/shops/visit/{slug}', 'HomeController@shop')->name('shop.visit');
Route::get('/shops/visit/{slug}/{type}', 'HomeController@filter_shop')->name('shop.visit.type');


Route::get('/cart', 'CartController@index')->name('cart');
Route::post('/cart/nav-cart-items', 'CartController@updateNavCart')->name('cart.nav_cart');
Route::post('/cart/show-cart-modal', 'CartController@showCartModal')->name('cart.showCartModal');
Route::post('/cart/addtocart', 'CartController@addToCart')->name('cart.addToCart');
Route::post('/cart/removeFromCart', 'CartController@removeFromCart')->name('cart.removeFromCart');
Route::post('/cart/updateQuantity', 'CartController@updateQuantity')->name('cart.updateQuantity');

//Checkout Routes
Route::group(['middleware' => ['checkout']], function(){
	Route::get('/checkout', 'CheckoutController@get_shipping_info')->name('checkout.shipping_info');
	Route::any('/checkout/delivery_info', 'CheckoutController@store_shipping_info')->name('checkout.store_shipping_infostore');
	
	Route::any('/checkout/updatePaymentOption', 'CheckoutController@paymentOption')->name('ajax.updatePaymentOption');
	Route::post('/checkout/payment_select', 'CheckoutController@store_delivery_info')->name('checkout.store_delivery_info');
	Route::get('/checkout/payment', 'CheckoutController@payment_select_get')->name('checkout.store_delivery_info_with_shipping');
});

Route::get('/checkout/order-confirmed', 'CheckoutController@order_confirmed')->name('order_confirmed');
Route::post('/checkout/user/verify', 'CheckoutController@cart_user_verify')->name('cart_user_verify');
Route::post('/checkout/user/verifying', 'CheckoutController@cart_user_verifying')->name('cart_user_verify.post');
Route::post('/checkout/payment', 'CheckoutController@checkout')->name('payment.checkout');
Route::post('/get_pick_ip_points', 'HomeController@get_pick_ip_points')->name('shipping_info.get_pick_ip_points');
Route::get('/checkout/payment_select', 'CheckoutController@get_payment_info')->name('checkout.payment_info');
Route::post('/checkout/apply_coupon_code', 'CheckoutController@apply_coupon_code')->name('checkout.apply_coupon_code');
Route::post('/checkout/remove_coupon_code', 'CheckoutController@remove_coupon_code')->name('checkout.remove_coupon_code');
Route::post('/checkout/apply_wallet_credit', 'CheckoutController@apply_wallet_credit')->name('checkout.apply_wallet_credit');
Route::post('/checkout/remove_wallet_credit', 'CheckoutController@remove_wallet_credit')->name('checkout.remove_wallet_credit');

//Paypal START
Route::get('/paypal/payment/done', 'PaypalController@getDone')->name('payment.done');
Route::get('/paypal/payment/cancel', 'PaypalController@getCancel')->name('payment.cancel');
//Paypal END

// SSLCOMMERZ Start
Route::get('/sslcommerz/pay', 'PublicSslCommerzPaymentController@index');
Route::POST('/sslcommerz/success', 'PublicSslCommerzPaymentController@success');
Route::POST('/sslcommerz/fail', 'PublicSslCommerzPaymentController@fail');
Route::POST('/sslcommerz/cancel', 'PublicSslCommerzPaymentController@cancel');
Route::POST('/sslcommerz/ipn', 'PublicSslCommerzPaymentController@ipn');
//SSLCOMMERZ END

//Stipe Start
Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
//Stripe END

Route::get('/compare', 'CompareController@index')->name('compare');
Route::get('/compare/reset', 'CompareController@reset')->name('compare.reset');
Route::post('/compare/addToCompare', 'CompareController@addToCompare')->name('compare.addToCompare');

Route::resource('subscribers','SubscriberController');

Route::get('/brands', 'HomeController@all_brands')->name('brands.all');
Route::get('/category', 'HomeController@category_view')->name('categories.all');
Route::get('/categoriesgrid', 'HomeController@all_categoriesgrid')->name('categories.all.mobilegrid');
Route::get('/categories', 'HomeController@all_categories')->name('categories.all.mobile');
Route::get('/subcategories', 'HomeController@all_subcategories')->name('subcategories.all.mobile');
Route::get('/search', 'HomeController@search')->name('search');
//Route::get('/search?q={search}', 'HomeController@search')->name('suggestion.search');
Route::get('/category?q={search}', 'HomeController@category_view')->name('suggestion.search');
Route::post('/ajax-search', 'HomeController@ajax_search')->name('search.ajax');
Route::post('/config_content', 'HomeController@product_content')->name('configs.update_status');

Route::get('/seller-policy', 'HomeController@sellerpolicy')->name('sellerpolicy');
Route::get('/return-policy', 'HomeController@returnpolicy')->name('returnpolicy');
Route::get('/support-policy', 'HomeController@supportpolicy')->name('supportpolicy');
Route::get('/terms', 'HomeController@terms')->name('terms');
Route::get('/privacy-policy', 'HomeController@privacypolicy')->name('privacypolicy');
/* Cashback Condition */
Route::get('/cashback-condition', 'HomeController@cashback')->name('cashback');
Route::get('/category-commission', 'HomeController@commission')->name('commission');
Route::get('/sellerorder-list', 'OrderController@sellerorderList')->name('sellerorder-list');
Route::get('orders-details/{id}', 'OrderController@sellerorderDetails')->name('orders-details');


Route::group(['middleware' => ['user', 'verified']], function(){
	Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
	Route::get('/profile', 'HomeController@profile')->name('profile');
	Route::post('/customer/update-profile', 'HomeController@customer_update_profile')->name('customer.profile.update');
	Route::post('/seller/update-profile', 'HomeController@seller_update_profile')->name('seller.profile.update');
	Route::post('addSellerCommission', 'HomeController@seller_update_profile')->name('addSellerCommission');
	

	Route::resource('purchase_history','PurchaseHistoryController');
	Route::post('/purchase_history/details', 'PurchaseHistoryController@purchase_history_details')->name('purchase_history.details');
	Route::post('/purchase_history/cancel', 'PurchaseHistoryController@purchase_order_cancel')->name('purchase_history.order_cancel');
	Route::get('/purchase_history/destroy/{id}', 'PurchaseHistoryController@destroy')->name('purchase_history.destroy');

	Route::resource('wishlists','WishlistController');
	Route::post('/wishlists/remove', 'WishlistController@remove')->name('wishlists.remove');

	Route::get('/wallet', 'WalletController@index')->name('wallet.index');
	Route::get('/cashback', 'WalletController@cashBackList')->name('cashback.index');
	Route::post('/recharge', 'WalletController@recharge')->name('wallet.recharge');

	Route::resource('support_ticket','SupportTicketController');
	Route::post('support_ticket/reply','SupportTicketController@seller_store')->name('support_ticket.seller_store');

	Route::post('/customer_packages/purchase', 'CustomerPackageController@purchase_package')->name('purchase_package');
	Route::resource('customer_products', 'CustomerProductController');
	Route::post('/customer_products/published', 'CustomerProductController@updatePublished')->name('customer_products.published');
	Route::post('/customer_products/status', 'CustomerProductController@updateStatus')->name('customer_products.update.status');

	Route::get('digital_purchase_history', 'PurchaseHistoryController@digital_index')->name('digital_purchase_history.index');
});

Route::get('/customer_products/destroy/{id}', 'CustomerProductController@destroy')->name('customer_products.destroy');

    Route::group(['prefix' =>'seller', 'middleware' => ['seller', 'verified']], function(){
	Route::get('/products', 'HomeController@seller_product_list')->name('seller.products');
	Route::get('/products/trash', 'HomeController@seller_trased_product_list')->name('seller.products.trash');
	Route::get('/product/upload', 'HomeController@show_product_upload_form')->name('seller.products.upload');
	Route::get('/product/{id}/edit', 'HomeController@show_product_edit_form')->name('seller.products.edit');
	
	//Add seller category,subcategory,subsub category
	Route::get('/category', 'SellerController@showCategory')->name('seller.category');
	Route::get('/subcategory', 'SellerController@showSubcategory')->name('seller.subcategory');
	Route::get('/subsubcategory', 'SellerController@showSubSubcategory')->name('seller.subsubcategory');
	
	Route::get('/addcategory', 'SellerController@addCategory')->name('seller.addcategory');
	Route::post('/savecategories', 'SellerController@saveCategory')->name('seller.savecategories');
	
	
	Route::resource('payments','PaymentController');

	Route::get('/shop/apply_for_verification', 'ShopController@verify_form')->name('shop.verify');
	Route::post('/shop/apply_for_verification', 'ShopController@verify_form_store')->name('shop.verify.store');

	Route::get('/reviews', 'ReviewController@seller_reviews')->name('reviews.seller');

	//digital Product
	Route::get('/digitalproducts', 'HomeController@seller_digital_product_list')->name('seller.digitalproducts');
	Route::get('/digitalproducts/upload', 'HomeController@show_digital_product_upload_form')->name('seller.digitalproducts.upload');
	Route::get('/digitalproducts/{id}/edit', 'HomeController@show_digital_product_edit_form')->name('seller.digitalproducts.edit');
});

Route::group(['middleware' => ['auth']], function(){
	Route::post('/products/store/','ProductController@store')->name('products.store');
	Route::post('/products/update/{id}','ProductController@update')->name('products.update');
	Route::get('/products/destroy/{id}', 'ProductController@destroy')->name('products.destroy');
	Route::get('/products/duplicate/{id}', 'ProductController@duplicate')->name('products.duplicate');
	Route::get('/products/trsnfertoseller/{id}', 'ProductController@transfersellerList')->name('products.trsnfertoseller');
	Route::post('/products/sku_combination', 'ProductController@sku_combination')->name('products.sku_combination');
	Route::post('/products/sku_combination_edit', 'ProductController@sku_combination_edit')->name('products.sku_combination_edit');
	Route::post('/products/variation', 'ProductController@variation')->name('products.variation');
	Route::post('/products/variation_edit', 'ProductController@variation_edit')->name('products.variation_edit');
	Route::post('/products/featured', 'ProductController@updateFeatured')->name('products.featured');
	Route::post('/products/published', 'ProductController@updatePublished')->name('products.published');
	Route::post('/products/restore', 'ProductController@restore')->name('products.restore');
	Route::post('/products/delete', 'ProductController@forceDelete')->name('products.forceDelete');

	Route::get('invoice/customer/{order_id}', 'InvoiceController@customer_invoice_download')->name('customer.invoice.download');
	Route::get('invoice/seller/{order_id}', 'InvoiceController@seller_invoice_download')->name('seller.invoice.download');

	Route::get('orders','OrderController@ordersTabular');
	Route::get('ordersTabular','OrderController@ordersTabular');
	Route::get('/orders/destroy/{id}', 'OrderController@destroy')->name('orders.destroy');
	Route::post('/orders/details', 'OrderController@order_details')->name('orders.details');
	Route::post('/orders/update_delivery_status', 'OrderController@update_delivery_status')->name('orders.update_delivery_status');
	Route::post('/orders/get_deducted_cancel_amount', 'OrderController@get_deducted_cancel_amount')->name('orders.get_deducted_cancel_amount');
	Route::post('/orders/denied', 'OrderController@orderDenied')->name('orders.denied');
	Route::post('/orders/update_payment_status', 'OrderController@update_payment_status')->name('orders.update_payment_status');
	Route::post('/orders/update_status', 'OrderController@update_status')->name('orders.update_status');
	Route::post('/orders/vendor/invoice/generate', 'OrderController@vendor_invoice_generate')->name('orders.vendor_invoice_generate');
	Route::post('/orders/vendor/invoice/generating', 'OrderController@vendor_invoice_generating')->name('orders.vendor_invoice_generating');
	Route::get('/orders/print/label/{id}', 'OrderController@OrderPrintLabel')->name('orders.print_label');
	Route::get('/order/seller/print/{seller_id}/{id}', 'OrderController@OrderPrintSellerLabel')->name('orders.printlabel.seller');
	Route::post('/orders/update/shipping', 'OrderController@OrderShipping')->name('orders.shipping');

	Route::resource('/reviews', 'ReviewController');

	Route::resource('/withdraw_requests', 'SellerWithdrawRequestController');
	Route::get('/withdraw_requests_all', 'SellerWithdrawRequestController@request_index')->name('withdraw_requests_all');
	Route::post('/withdraw_request/payment_modal', 'SellerWithdrawRequestController@payment_modal')->name('withdraw_request.payment_modal');
	Route::post('/withdraw_request/message_modal', 'SellerWithdrawRequestController@message_modal')->name('withdraw_request.message_modal');

	Route::resource('conversations','ConversationController');
	Route::post('conversations/refresh','ConversationController@refresh')->name('conversations.refresh');
	Route::resource('messages','MessageController');

	//Product Bulk Upload
	
	Route::get('/product-bulk-upload/list', 'ProductBulkUploadController@list')->name('product_bulk_upload.list');
	Route::post('/product-bulk-upload/delete', 'ProductBulkUploadController@remove')->name('product_bulk_upload.remove');
	Route::get('/product-bulk-upload/index', 'ProductBulkUploadController@index')->name('product_bulk_upload.index');
	Route::get('/product-bulk-upload/process', 'ProductBulkUploadController@process')->name('product_bulk_upload.process');
	Route::post('/product-bulk-upload/clear', 'ProductBulkUploadController@clear')->name('product_bulk_upload.clear');
	Route::post('/bulk-product-upload', 'ProductBulkUploadController@bulk_upload')->name('bulk_product_upload');
	Route::post('/bulk-product-upload_img', 'ProductBulkUploadController@bulk_upload_img')->name('bulk_product_upload_img');
	Route::post('/bulk-product-update', 'ProductBulkUploadController@bulk_update')->name('bulk_product_update');
	Route::get('/product-csv-download/{type}', 'ProductBulkUploadController@import_product')->name('product_csv.download');
	Route::get('/vendor-product-csv-download/{id}', 'ProductBulkUploadController@import_vendor_product')->name('import_vendor_product.download');
	Route::group(['prefix' =>'bulk-upload/download'], function(){
		Route::get('/category', 'ProductBulkUploadController@pdf_download_category')->name('pdf.download_category');
		Route::get('/sub_category', 'ProductBulkUploadController@pdf_download_sub_category')->name('pdf.download_sub_category');
		Route::get('/sub_sub_category', 'ProductBulkUploadController@pdf_download_sub_sub_category')->name('pdf.download_sub_sub_category');
		Route::get('/brand', 'ProductBulkUploadController@pdf_download_brand')->name('pdf.download_brand');
		Route::get('/seller', 'ProductBulkUploadController@pdf_download_seller')->name('pdf.download_seller');
	});

	//Product Export
	Route::get('/product-bulk-export', 'ProductBulkUploadController@export')->name('product_bulk_export.index');

	Route::resource('digitalproducts','DigitalProductController');
	Route::get('/digitalproducts/destroy/{id}', 'DigitalProductController@destroy')->name('digitalproducts.destroy');
	Route::get('/digitalproducts/download/{id}', 'DigitalProductController@download')->name('digitalproducts.download');
});

Route::resource('shops', 'ShopController');
Route::get('/track_your_order', 'HomeController@trackOrder')->name('orders.track');

Route::get('/instamojo/payment/pay-success', 'InstamojoController@success')->name('instamojo.success');

Route::post('rozer/payment/pay-success', 'RazorpayController@payment')->name('payment.rozer');

Route::get('/paystack/payment/callback', 'PaystackController@handleGatewayCallback');


Route::get('/vogue-pay', 'VoguePayController@showForm');
Route::get('/vogue-pay/success/{id}', 'VoguePayController@paymentSuccess');
Route::get('/vogue-pay/failure/{id}', 'VoguePayController@paymentFailure');

Route::get('/about-us', 'HomeController@aboutus')->name('aboutus');
Route::get('/career-opportunities', 'HomeController@career')->name('career');
Route::get('/contact-us', 'HomeController@contactus')->name('contactus');
Route::post('/contact_send', 'HomeController@contact_send')->name('contact_send');

Route::get('/blog', 'HomeController@blog')->name('blog');
Route::get('/blog/{slug}', array('as' => 'BlogView', 'uses' => 'HomeController@BlogView'));

//Agile book api authenctication
Route::get('/agilLogin', 'AgileBookApiController@Authenctication')->name('Authenctication');
Route::get('/getagilnStateList', 'AgileBookApiController@getagilnStateList')->name('getagilnStateList');

Route::get('/getagilnCityList/{id}', 'AgileBookApiController@getCityByStateId')->name('getCityByStateId');
Route::get('/addPostAlignCustomer', 'AgileBookApiController@addPostAlignCustomer')->name('addPostAlignCustomer');
Route::get('/addVenderAlignBook', 'AgileBookApiController@addVenderAlignBook')->name('addVenderAlignBook');




Route::post('product', 'HomeController@product_request')->name('product.product_request');
Route::get('updateRecord/{id}', 'OrderController@updatedPendingAmount')->name('updateRecord.updatedPendingAmount');

//Checkemail exist or not

Route::POST('checkemail', 'SellerController@checkEmail')->name('checkemail');
function d() {
        call_user_func_array( 'dump' , func_get_args() );
}
