<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', 'HomeController@admin_dashboard')->name('admin.dashboard')->middleware(['auth', 'admin']);
Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'admin']], function(){
    
//Marg Routes
Route::get('marg/sync', 'MargController@sync')->name('marg-sync');
Route::get('marg/product/unlisted', 'MargController@unlistedProduct')->name('unlistedProduct');
Route::post('marg/product/unlisted', 'MargController@unlistedProductMap')->name('unlistedProduct.Map');
Route::get('marg/product/unmapped', 'MargController@unmappedProduct');
Route::get('marg/customer/unlisted', 'MargController@unlistedCustomer')->name('unlistedCustomer');
Route::post('marg/customer/unlisted', 'MargController@unlistedCustomerMap')->name('unlistedCustomer.Map');
Route::get('marg/customer/unmapped', 'MargController@unmappedCustomer')->name('unmappedCustomer');
Route::post('marg/customer/unmapped', 'MargController@unmappedCustomerMap')->name('unmappedCustomer.Map');
Route::get('marg/order/pending', 'MargController@order_pending')->name('marg.order.pending');
Route::get('marg/order/processed', 'MargController@order_processed')->name('marg.order.processed');
Route::post('marg/customer', 'MargController@get_marg_customer')->name('marg.customer');
Route::post('marg/product', 'MargController@get_marg_product')->name('marg.product');
Route::post('marg/customer/map', 'MargController@marg_customer_map')->name('marg.customer.map');
Route::post('marg/order/send', 'MargController@marg_order_send')->name('marg.order.send');
Route::post('marg/order/product', 'MargController@marg_order_product')->name('marg.order.product');
Route::get('marg/debug', 'MargController@debug')->name('marg.debug');

    
//Expenses
	Route::get('/expenses', array('as' => 'Expenses', 'uses' => 'ExpensesController@Expenses'));
	Route::get('/expenses/create', array('as' => 'CreateExpense', 'uses' => 'ExpensesController@CreateExpense'));
	Route::post('/expenses/save', array('as' => 'SaveExpense', 'uses' => 'ExpensesController@SaveExpense'));
	Route::get('/expenses/edit/{id}', array('as' => 'ExpensesEdit', 'uses' => 'ExpensesController@ExpensesEditView'));
	Route::post('/expenses/update/{id}', array('as' => 'ExpensesEditPost', 'uses' => 'ExpensesController@ExpensesEditPost'));
	Route::get('/expenses/view/{id}', array('as' => 'ExpensesView', 'uses' => 'ExpensesController@ExpensesView'));
	Route::post('/expenses/delete', array('as' => 'ExpenseDelete', 'uses' => 'ExpensesController@ExpenseDelete'));
	Route::get('/expenses/addpayment', array('as' => 'ExpensePaymentAdd', 'uses' => 'ExpensesController@ExpensePaymentAdd'));
	Route::get('/expenses/addpayment/{id}', array('as' => 'ExpensePaymentAddWithID', 'uses' => 'ExpensesController@ExpensePaymentAddWithID'));
	Route::post('/expenses/savepayment', array('as' => 'ExpensePaymentSave', 'uses' => 'ExpensesController@ExpensePaymentSave'));
	Route::post('/expenses/deletepayment', array('as' => 'ExpensePaymentDelete', 'uses' => 'ExpensesController@ExpensePaymentDelete'));
	Route::get('/expenses/createcategory', array('as' => 'CreateCategory', 'uses' => 'ExpensesController@CreateCategory'));
	Route::post('/expenses/categorysave', array('as' => 'SaveCategory', 'uses' => 'ExpensesController@SaveCategory'));
	Route::get('/expenses/category', array('as' => 'ExpenseCategory', 'uses' => 'ExpensesController@Category'));
	Route::get('/expenses/categoryedit/{id}', array('as' => 'CategoryEditView', 'uses' => 'ExpensesController@CategoryEditView'));
	Route::post('/expenses/categoryupdate/{id}', array('as' => 'CategoryEditPost', 'uses' => 'ExpensesController@CategoryEditPost'));
	Route::post('/expenses/categorydelete', array('as' => 'ExpenseCategoryDelete', 'uses' => 'ExpensesController@CategoryDelete'));

//product purchase
	Route::get('/product/purchase', 'ProductPurchaseController@Product')->name('product.purchase.index');
	Route::get('/product/purchase/create', 'ProductPurchaseController@ProductCreate')->name('product.purchase.create');
	Route::post('/product/purchase/save', 'ProductPurchaseController@ProductAdd')->name('product.purchase.save');
	Route::get('/product/purchase/view/{id}', 'ProductPurchaseController@ProductPurchaseView')->name('product.purchase.view');
	Route::get('/product/purchase/delete', 'ProductPurchaseController@ProductDelete')->name('product.purchase.delete');
	
	Route::get('wallet-history/{id}', 'WalletController@walletHistory')->name('wallet-history');
    
    
	Route::resource('categories','CategoryController');
	Route::get('/categories/destroy/{id}', 'CategoryController@destroy')->name('categories.destroy');
	Route::post('/categories/featured', 'CategoryController@updateFeatured')->name('categories.featured');

	Route::resource('subcategories','SubCategoryController');
	Route::get('/subcategories/destroy/{id}', 'SubCategoryController@destroy')->name('subcategories.destroy');
	//die('kkggg');
	Route::post('/ajax/getsubcategory', 'SubCategoryController@getsubcats')->name('ajax.getsubcategory');
	Route::post('/ajax/getsubsubcategory', 'SubSubCategoryController@getSubsubcatsBy')->name('ajax.getsubsubcategory');
	Route::resource('subsubcategories','SubSubCategoryController');
	Route::get('/subsubcategories/destroy/{id}', 'SubSubCategoryController@destroy')->name('subsubcategories.destroy');


	Route::resource('brands','BrandController');
	Route::get('/brands/destroy/{id}', 'BrandController@destroy')->name('brands.destroy');

	
	Route::post('/ajax/getAllProduct', 'ProductController@getAllProductBySubCatId')->name('ajax.getAllProduct');

	Route::get('/products/admin','ProductController@admin_products')->name('products.admin');
	Route::get('/products/seller','ProductController@seller_products')->name('products.seller');
	Route::get('/products/sellerVerified/{id?}','ProductController@seller_products_verified')->name('products.sellerVerified');
	Route::get('/products/create','ProductController@create')->name('products.create');
	Route::get('/products/admin/{id}/edit','ProductController@admin_product_edit')->name('products.admin.edit');
	Route::get('/products/seller/{id}/edit','ProductController@seller_product_edit')->name('products.seller.edit');
	Route::post('/products/todays_deal', 'ProductController@updateTodaysDeal')->name('products.todays_deal');
	Route::post('/products/get_products_by_subsubcategory', 'ProductController@get_products_by_subsubcategory')->name('products.get_products_by_subsubcategory');
	Route::post('/products/get_products_by_category', 'ProductController@get_products_by_category')->name('products.get_products_by_category');
	Route::post('/products/vendor/assign', 'ProductController@vendor_assign')->name('products.vendor_assign');
	Route::get('/products/vendor/remove/{id}', 'ProductController@vendor_remove')->name('products.vendor_remove');
	
	Route::get('/bulk-product-remove', 'ProductController@bulk_remove_view')->name('bulk_remove_view');
	Route::post('/bulk-product-remove', 'ProductController@bulk_remove')->name('bulk_remove');

	Route::resource('sellers','SellerController');
	Route::get('/sellers/destroy/{id}', 'SellerController@destroy')->name('sellers.destroy');
	Route::get('/sellers/view/{id}/verification', 'SellerController@show_verification_request')->name('sellers.show_verification_request');
	Route::get('/sellers/approve/{id}', 'SellerController@approve_seller')->name('sellers.approve');
	Route::get('/sellers/reject/{id}', 'SellerController@reject_seller')->name('sellers.reject');
	Route::post('/sellers/payment_modal', 'SellerController@payment_modal')->name('sellers.payment_modal');
	Route::get('/seller/payments', 'PaymentController@payment_histories')->name('sellers.payment_histories');
	Route::get('/seller/payments/show/{id}', 'PaymentController@show')->name('sellers.payment_history');
    Route::post('/sellers/profile_modal', 'SellerController@profile_modal')->name('sellers.profile_modal');
    Route::post('/sellers/approved', 'SellerController@updateApproved')->name('sellers.approved');
    Route::get('/seller/sellercommission', 'SellerController@seller_commission')->name('sellers.sellercommission');
    Route::get('/seller/commissionview', 'SellerController@commission_view')->name('sellers.commissionview');
    Route::post('addSellerCommission', 'SellerController@addSellerCommission')->name('addSellerCommission');
    Route::post('editSellerCommission', 'SellerController@editSellerCommission')->name('editSellerCommission');
    
   // Route::post('SellerCommission/view/{id}', 'SellerController@SellerCommissionShow')->name('SellerCommission');
   Route::get('/sellercomission/{id}/show', 'SellerController@showComission')->name('sellercomission.show');
   //Route::get('/orders/{id}/show', 'OrderController@show')->name('orders.show');
    
    
    
    //Delete seller config
    Route::post('/deleteSellerCommisionConfig', 'SellerController@deleteSellerCommisionConfig')->name('deleteSellerCommisionConfig');
    
	Route::resource('vendors','VendorController');
    Route::post('/vendors/profile_modal', 'VendorController@profile_modal')->name('vendors.profile_modal');
    Route::post('/vendors/active', 'VendorController@active_vendor')->name('vendors.activated');
    Route::get('/products/vendor/{id}', 'ProductController@vendor_products')->name('products.vendor');
    Route::get('/vendors/view/expenses', 'VendorController@vendor_expenses')->name('vendor.expenses');
    Route::post('/vendors/create/login', 'VendorController@create_login')->name('vendor.create_login');


	Route::get('/customers/export', 'CustomerController@ExportExcel')->name('customers.export');
	//Order export
	Route::get(
	'/order/export', 'OrderController@ExportExcel')->name('order.export');
	Route::resource('customers','CustomerController');
	Route::get('/customers/destroy/{id}', 'CustomerController@destroy')->name('customers.destroy');

	Route::get('/newsletter', 'NewsletterController@index')->name('newsletters.index');
	Route::post('/newsletter/send', 'NewsletterController@send')->name('newsletters.send');

	Route::resource('profile','ProfileController');

	Route::post('/business-settings/update', 'BusinessSettingsController@update')->name('business_settings.update');
	Route::post('/business-settings/update/activation', 'BusinessSettingsController@updateActivationSettings')->name('business_settings.update.activation');
	Route::get('/activation', 'BusinessSettingsController@activation')->name('activation.index');
	Route::get('/payment-method', 'BusinessSettingsController@payment_method')->name('payment_method.index');
	Route::get('/social-login', 'BusinessSettingsController@social_login')->name('social_login.index');
	Route::get('/smtp-settings', 'BusinessSettingsController@smtp_settings')->name('smtp_settings.index');
	Route::get('/google-analytics', 'BusinessSettingsController@google_analytics')->name('google_analytics.index');
	Route::get('/facebook-chat', 'BusinessSettingsController@facebook_chat')->name('facebook_chat.index');
	Route::post('/env_key_update', 'BusinessSettingsController@env_key_update')->name('env_key_update.update');
	Route::post('/payment_method_update', 'BusinessSettingsController@payment_method_update')->name('payment_method.update');
	Route::post('/google_analytics', 'BusinessSettingsController@google_analytics_update')->name('google_analytics.update');
	Route::post('/facebook_chat', 'BusinessSettingsController@facebook_chat_update')->name('facebook_chat.update');
	Route::post('/facebook_pixel', 'BusinessSettingsController@facebook_pixel_update')->name('facebook_pixel.update');
	Route::get('/currency', 'CurrencyController@currency')->name('currency.index');
    Route::post('/currency/update', 'CurrencyController@updateCurrency')->name('currency.update');
    Route::post('/your-currency/update', 'CurrencyController@updateYourCurrency')->name('your_currency.update');
	Route::get('/currency/create', 'CurrencyController@create')->name('currency.create');
	Route::post('/currency/store', 'CurrencyController@store')->name('currency.store');
	Route::post('/currency/currency_edit', 'CurrencyController@edit')->name('currency.edit');
	Route::post('/currency/update_status', 'CurrencyController@update_status')->name('currency.update_status');
	Route::get('/verification/form', 'BusinessSettingsController@seller_verification_form')->name('seller_verification_form.index');
	Route::post('/verification/form', 'BusinessSettingsController@seller_verification_form_update')->name('seller_verification_form.update');
	Route::get('/vendor_commission', 'BusinessSettingsController@vendor_commission')->name('business_settings.vendor_commission');
	Route::post('/vendor_commission_update', 'BusinessSettingsController@vendor_commission_update')->name('business_settings.vendor_commission.update');
  
    
	Route::resource('/languages', 'LanguageController');
	Route::post('/languages/update_rtl_status', 'LanguageController@update_rtl_status')->name('languages.update_rtl_status');
	Route::get('/languages/destroy/{id}', 'LanguageController@destroy')->name('languages.destroy');
	Route::get('/languages/{id}/edit', 'LanguageController@edit')->name('languages.edit');
	Route::post('/languages/{id}/update', 'LanguageController@update')->name('languages.update');
	Route::post('/languages/key_value_store', 'LanguageController@key_value_store')->name('languages.key_value_store');

	Route::get('/frontend_settings/home', 'HomeController@home_settings')->name('home_settings.index');
	Route::post('/frontend_settings/home/top_10', 'HomeController@top_10_settings')->name('top_10_settings.store');
	Route::get('/sellerpolicy/{type}', 'PolicyController@index')->name('sellerpolicy.index');
	Route::get('/returnpolicy/{type}', 'PolicyController@index')->name('returnpolicy.index');
	Route::get('/supportpolicy/{type}', 'PolicyController@index')->name('supportpolicy.index');
	Route::get('/terms/{type}', 'PolicyController@index')->name('terms.index');
	Route::get('/privacypolicy/{type}', 'PolicyController@index')->name('privacypolicy.index');

	//Policy Controller
	Route::post('/policies/store', 'PolicyController@store')->name('policies.store');
	
	//cache clear controller
	Route::get('clearallcache', 'HomeController@clearcache')->name('clearallcache');
	

	Route::group(['prefix' => 'frontend_settings'], function(){
		Route::resource('menu','MenuController');
	    Route::get('/menu/destroy/{id}', 'MenuController@destroy')->name('menu.destroy');
		Route::post('/menu/update_status', 'MenuController@update_status')->name('menu.update_status');
		
		Route::resource('brandbanner','BrandbannerController');
	    Route::get('/brandbanner/destroy/{id}', 'BrandbannerController@destroy')->name('brandbanner.destroy');
		Route::post('/brandbanner/update_status', 'BrandbannerController@update_status')->name('brandbanner.update_status');
		
		Route::resource('sliders','SliderController');
		Route::post('/sliders/update_status', 'SliderController@update_status')->name('sliders.update_status');
	    Route::get('/sliders/destroy/{id}', 'SliderController@destroy')->name('sliders.destroy');

		Route::resource('home_banners','BannerController');
		Route::get('/home_banners/create/{position}', 'BannerController@create')->name('home_banners.create');
		Route::post('/home_banners/update_status', 'BannerController@update_status')->name('home_banners.update_status');
	    Route::get('/home_banners/destroy/{id}', 'BannerController@destroy')->name('home_banners.destroy');
		
		Route::resource('popup_offer','PopOfferController');
		Route::post('/popup_offer/update_status', 'PopOfferController@update_status')->name('popup_offer.update_status');
	    Route::get('/popup_offer/destroy/{id}', 'PopOfferController@destroy')->name('popup_offer.destroy');
		
		

		Route::resource('home_categories','HomeCategoryController');
		
	    Route::get('/home_categories/destroy/{id}', 'HomeCategoryController@destroy')->name('home_categories.destroy');
		Route::post('/home_categories/update_status', 'HomeCategoryController@update_status')->name('home_categories.update_status');
		Route::post('/home_categories/get_subsubcategories_by_category', 'HomeCategoryController@getSubSubCategories')->name('home_categories.get_subsubcategories_by_category');
		Route::post('/home_categories/get_products_by_subsubcategories', 'HomeCategoryController@getproductsbySubSubCategories')->name('home_categories.get_products_by_subsubcategories');

        Route::resource('best_selling','BestSellingController');
		Route::get('/best_selling/destroy/{id}', 'BestSellingController@destroy')->name('best_selling.destroy');
		Route::post('/best_selling/update_status', 'BestSellingController@update_status')->name('best_selling.update_status');
	});

	Route::resource('roles','RoleController');
    Route::get('/roles/destroy/{id}', 'RoleController@destroy')->name('roles.destroy');

	Route::resource('staffs','StaffController');
	Route::get('feedback','CustomerController@allfeedback')->name('feedback');
	Route::get('user-cashback-config','BusinessSettingsController@userCashbackConfig')->name('user-cashback-config');

	Route::post('add-cashback-config','BusinessSettingsController@addCashBackConfig')->name('add-cashback-config');
	Route::post('update-cashback-config','BusinessSettingsController@updateCashBackConfig')->name('update-cashback-config');
	Route::get('feedback-details/{id}','CustomerController@getFeedbackDetails')->name('feedback-details');
	Route::get('pharmacy','CustomerController@allpharmacy')->name('pharmacy');
	
    Route::get('/staffs/destroy/{id}', 'StaffController@destroy')->name('staffs.destroy');
    
    //sitemap
    
    Route::resource('sitemap','SitemapController');
    Route::get('/sitemap/destroy/{id}', 'SitemapController@destroy')->name('sitemap.destroy');
    Route::get('/sitemaps/generate', 'SitemapController@sitemap_generate')->name('sitemap.generate');
    

	Route::resource('flash_deals','FlashDealController');
    Route::resource('category_config','CategoryConfigControllers');
    Route::get('/flash_deals/destroy/{id}', 'FlashDealController@destroy')->name('flash_deals.destroy');
	Route::post('/flash_deals/update_status', 'FlashDealController@update_status')->name('flash_deals.update_status');
	Route::post('/flash_deals/update_featured', 'FlashDealController@update_featured')->name('flash_deals.update_featured');
	Route::post('/flash_deals/product_discount', 'FlashDealController@product_discount')->name('flash_deals.product_discount');
	Route::post('/flash_deals/product_discount_edit', 'FlashDealController@product_discount_edit')->name('flash_deals.product_discount_edit');

	Route::get('/orders', 'OrderController@admin_orders')->name('orders.index.admin');

	//Updated order
	Route::post('/ajax/updatedOrder', 'OrderController@updatedOrderByAdmin')->name('ajax.updatedOrder');
	Route::post('/ajax/searchProduct', 'OrderController@searchProductDetails')->name('ajax.searchProduct');
	
    
	Route::get('/orders/pending', 'OrderController@admin_orders_status')->name('orders.index.pending.admin');
	Route::get('/orders/confirmation_pending', 'OrderController@admin_orders_status_otc')->name('orders.index.confirmation_pending.admin');
	Route::get('/orders/hold', 'OrderController@admin_orders_status')->name('orders.index.hold.admin');
	Route::get('/orders/on_review', 'OrderController@admin_orders_status')->name('orders.index.on_review.admin');
	Route::get('/orders/ready_to_ship', 'OrderController@admin_orders_status')->name('orders.index.ready_to_ship.admin');
	Route::get('/orders/ready_to_delivery', 'OrderController@admin_orders_status')->name('orders.index.ready_to_delivery.admin');
	Route::get('/orders/shipped', 'OrderController@admin_orders_status')->name('orders.index.shipped.admin');
	Route::get('/orders/on_delivery', 'OrderController@admin_orders_status')->name('orders.index.on_delivery.admin');
	Route::get('/orders/on_ship', 'OrderController@admin_orders_status')->name('orders.index.on_ship.admin');
	Route::get('/orders/delivered', 'OrderController@admin_orders_status')->name('orders.index.delivered.admin');
	Route::get('/orders/processing_refund', 'OrderController@admin_orders_status')->name('orders.index.processing_refund.admin');
	Route::get('/orders/refunded', 'OrderController@admin_orders_status')->name('orders.index.refunded.admin');
	Route::get('/orders/cancel', 'OrderController@admin_orders_status')->name('orders.index.cancel.admin');
	Route::get('/orders/trash', 'OrderController@admin_orders_status')->name('orders.index.trash.admin');
    
	Route::post('/orders/{id}/update', 'OrderController@update')->name('orders.update');
	Route::get('/orders/{id}/show', 'OrderController@show')->name('orders.show');
	Route::get('/sales/{id}/show', 'OrderController@sales_show')->name('sales.show');
	Route::get('/orders/destroy/{id}', 'OrderController@destroy')->name('orders.destroy');
	Route::get('/sales', 'OrderController@sales')->name('sales.index');
	Route::post('/sales/download', 'ReportController@salesReportDownload')->name('sales.Downlaod');
	Route::post('/report/product/sales/download', 'ReportController@ProductsSalesReportDownload')->name('ProductsSalesReportDownload.Downlaod');
	Route::get('/orders/vendor', 'OrderController@admin_orders_vendor')->name('orders.admin.vendor');
	Route::get('/orders/vendor/{seller_id}/{id}', 'OrderController@vendor_show')->name('orders.vendor.show');
	
	Route::post('/orders/deliveryupdate', 'OrderController@deliveryupdate')->name('orders.deliveryupdate');
	
	
	Route::get('/order/seller', 'OrderController@admin_seller_orders_status')->name('orders.index.admin.seller');
	
	Route::get('/order/seller/{seller_id}/{id}', 'OrderController@seller_show')->name('orders.seller.show');
    
	
	Route::get('/order/seller/pending', 'OrderController@admin_seller_orders_status')->name('orders.index.pending.admin.seller');
	Route::get('/order/seller/confirmation_pending', 'OrderController@admin_seller_orders_status')->name('orders.index.confirmation_pending.admin.seller');
	Route::get('/order/seller/hold', 'OrderController@admin_seller_orders_status')->name('orders.index.hold.admin.seller');
	Route::get('/order/seller/on_review', 'OrderController@admin_seller_orders_status')->name('orders.index.on_review.admin.seller');
	Route::get('/order/seller/ready_to_ship', 'OrderController@admin_seller_orders_status')->name('orders.index.ready_to_ship.admin.seller');
	Route::get('/order/seller/ready_to_delivery', 'OrderController@admin_seller_orders_status')->name('orders.index.ready_to_delivery.admin.seller');
	Route::get('/order/seller/shipped', 'OrderController@admin_seller_orders_status')->name('orders.index.shipped.admin.seller');
	Route::get('/order/seller/on_delivery', 'OrderController@admin_seller_orders_status')->name('orders.index.on_delivery.admin.seller');
	Route::get('/order/seller/on_ship', 'OrderController@admin_seller_orders_status')->name('orders.index.on_ship.admin.seller');
	Route::get('/order/seller/delivered', 'OrderController@admin_seller_orders_status')->name('orders.index.delivered.admin.seller');
	Route::get('/order/seller/processing_refund', 'OrderController@admin_seller_orders_status')->name('orders.index.processing_refund.admin.seller');
	Route::get('/order/seller/refunded', 'OrderController@admin_seller_orders_status')->name('orders.index.refunded.admin.seller');
	Route::get('/order/seller/cancel', 'OrderController@admin_seller_orders_status')->name('orders.index.cancel.admin.seller');
	

	Route::resource('links','LinkController');
	Route::get('/links/destroy/{id}', 'LinkController@destroy')->name('links.destroy');

	Route::resource('generalsettings','GeneralSettingController');
	Route::get('/logo','GeneralSettingController@logo')->name('generalsettings.logo');
	Route::post('/logo','GeneralSettingController@storeLogo')->name('generalsettings.logo.store');
	Route::get('/color','GeneralSettingController@color')->name('generalsettings.color');
	Route::post('/color','GeneralSettingController@storeColor')->name('generalsettings.color.store');

	Route::resource('seosetting','SEOController');

	Route::post('/pay_to_seller', 'CommissionController@pay_to_seller')->name('commissions.pay_to_seller');

	//Reports
	Route::get('/stock_report', 'ReportController@stock_report')->name('stock_report.index');
	Route::get('/in_house_sale_report', 'ReportController@in_house_sale_report')->name('in_house_sale_report.index');
	Route::get('/seller_report', 'ReportController@seller_report')->name('seller_report.index');
	Route::get('/seller_sale_report', 'ReportController@seller_sale_report')->name('seller_sale_report.index');
	Route::get('/wish_report', 'ReportController@wish_report')->name('wish_report.index');
	Route::get('/reports/expenses', 'ReportController@ExpenseReport')->name('report.expenses');
	Route::get('/reports/sales', 'ReportController@SalesReport')->name('report.sales');


	
	//Stock Quantity update according to SubCategory
	Route::get('/stock', 'ProductStockController@index')->name('stock.index');
	//stock quantity update according to sub categories
	Route::post('/products/stock_qty/','ProductStockController@stock_qty')->name('products.stock_qty');
	//category Export view
	Route::get('/categoryexport', 'ProductStockController@export_index')->name('category.export');
	//category Export post route
	Route::post('/products/cat_export/','ProductStockController@cat_export')->name('products.cat_export');
	//Coupons
	Route::resource('coupon','CouponController');
	Route::get('/customer/list', 'CouponController@get_user')->name('coupon.get_user');
	Route::post('/coupon/get_form', 'CouponController@get_coupon_form')->name('coupon.get_coupon_form');
	Route::post('/coupon/get_form_edit', 'CouponController@get_coupon_form_edit')->name('coupon.get_coupon_form_edit');
	Route::get('/coupon/destroy/{id}', 'CouponController@destroy')->name('coupon.destroy');

	//Reviews
	Route::get('/reviews', 'ReviewController@index')->name('reviews.index');
	Route::post('/reviews/published', 'ReviewController@updatePublished')->name('reviews.published');

	//Support_Ticket
	Route::get('support_ticket/','SupportTicketController@admin_index')->name('support_ticket.admin_index');
	Route::get('support_ticket/{id}/show','SupportTicketController@admin_show')->name('support_ticket.admin_show');
	Route::post('support_ticket/reply','SupportTicketController@admin_store')->name('support_ticket.admin_store');

	//Pickup_Points
	Route::resource('pick_up_points','PickupPointController');
	Route::get('/pick_up_points/destroy/{id}', 'PickupPointController@destroy')->name('pick_up_points.destroy');


	Route::get('orders_by_pickup_point','OrderController@order_index')->name('pick_up_point.order_index');
	Route::get('/orders_by_pickup_point/{id}/show', 'OrderController@pickup_point_order_sales_show')->name('pick_up_point.order_show');

	Route::get('invoice/admin/{order_id}', 'InvoiceController@admin_invoice_download')->name('admin.invoice.download');

	//conversation of seller customer
	Route::get('conversations','ConversationController@admin_index')->name('conversations.admin_index');
	Route::get('conversations/{id}/show','ConversationController@admin_show')->name('conversations.admin_show');
	Route::get('/conversations/destroy/{id}', 'ConversationController@destroy')->name('conversations.destroy');



	Route::resource('attributes','AttributeController');
	Route::get('/attributes/destroy/{id}', 'AttributeController@destroy')->name('attributes.destroy');

	Route::resource('addons','AddonController');
	Route::post('/addons/activation', 'AddonController@activation')->name('addons.activation');

	Route::get('/customer-bulk-upload/index', 'CustomerBulkUploadController@index')->name('customer_bulk_upload.index');
	Route::get('/userimoprt', 'CustomerBulkUploadController@index')->name('userimoprt');
	Route::post('/bulk-user-upload', 'CustomerBulkUploadController@user_bulk_upload')->name('bulk-user-upload');
	
	//Route::post('/bulk-customer-upload', 'CustomerBulkUploadController@customer_bulk_file');
	Route::get('/user', 'CustomerBulkUploadController@pdf_download_user')->name('pdf.download_user');

	//Customer Package
	Route::resource('customer_packages','CustomerPackageController');
	Route::get('/customer_packages/destroy/{id}', 'CustomerPackageController@destroy')->name('customer_packages.destroy');
	//Classified Products
	Route::get('/classified_products', 'CustomerProductController@customer_product_index')->name('classified_products');
	Route::post('/classified_products/published', 'CustomerProductController@updatePublished')->name('classified_products.published');
	
	//blog
	Route::resource('blog','BlogController');
	Route::get('/blog/destroy/{id}', 'BlogController@destroy')->name('blog.destroy');
	Route::post('/blog/imageupload', 'BlogController@EditorUploadImg')->name('EditorUploadImg');
	
	//Product Requests
	Route::get('/productrequest', 'HomeController@show')->name('productrequest.show');
	Route::get('/PRSetStatus/{id}', 'HomeController@PRSetStatus')->name('productrequest.process');
	Route::get('/PRDestroy/{id}', 'HomeController@PRDestroy')->name('productrequest.destroy');
	//Route::get('/PRSendEmail/{id}', 'HomeController@PRSendEmail')->name('productrequest.email');
});
