<?php

/*
|--------------------------------------------------------------------------
| Affiliate Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admin
Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'admin']], function(){
    Route::get('/refund-reuest-all', 'RefundRequestController@admin_index')->name('refund_requests_all');
    Route::get('/refund-reuest-config', 'RefundRequestController@refund_config')->name('refund_time_config');
    Route::get('/paid-refund', 'RefundRequestController@paid_index')->name('paid_refund');
    Route::post('/refund-request-pay', 'RefundRequestController@refund_pay')->name('refund_request_money_by_admin');
    Route::post('/refund-request-time-store', 'RefundRequestController@refund_time_update')->name('refund_request_time_config');
    Route::post('/refund-request-sticker-store', 'RefundRequestController@refund_sticker_update')->name('refund_sticker_config');
});

//FrontEnd
Route::group(['middleware' => ['user', 'verified']], function(){
	Route::post('refund-request-send/{id}', 'RefundRequestController@request_store')->name('refund_request_send');
    Route::get('refund-request', 'RefundRequestController@vendor_index')->name('vendor_refund_request');
   // Route::get('sent-refund-reuest', 'RefundRequestController@customer_index')->name('customer_refund_request');
    Route::post('refund-reuest-vendor-approval', 'RefundRequestController@request_approval_vendor')->name('vendor_refund_approval');
    Route::get('refund-request/{id}', 'RefundRequestController@refund_request_send_page')->name('refund_request_send_page');
});

Route::group(['middleware' => ['auth']], function(){
    Route::get('refund-request-reason/{id}', 'RefundRequestController@reason_view')->name('reason_show');
});
Route::post('purchase_history/view/', 'ReturnController@orderDetails')->name('return.details');
 Route::post('sent-refund-reuest', 'ReturnController@save_request')->name('customer_refund_request');

 Route::post('sent-all-refund-reuest', 'ReturnController@save_all_request')->name('customer_all_refund_request');
 Route::get('refund-track', 'ReturnController@track_request')->name('customer_refund_track');
 Route::post('/return_history/details', 'ReturnController@return_history_details')->name('return_history.details');
 Route::get('/returnorders/{id}/show', 'ReturnController@show')->name('returnorders.show');
 Route::get('/return/order', 'ReturnController@return_orders')->name('return.index.admin');
Route::get('/return/{id}/show', 'ReturnController@show_return')->name('return_orders.show');
Route::get('/return/processing_refund', 'ReturnController@return_orders_status')->name('return.index.processing_refund.admin');
Route::get('/return/refunded', 'ReturnController@return_orders_status')->name('return.index.refunded.admin');
Route::get('/return/return_request', 'ReturnController@return_orders_status')->name('return.index.return_request.admin');
Route::get('/return/accepted', 'ReturnController@return_orders_status')->name('return.index.return_accepted.admin');
Route::get('/return/pickup/completed', 'ReturnController@return_orders_status')->name('return.index.pickup_completed.admin');
Route::get('/return/cancelled', 'ReturnController@return_orders_status')->name('return.index.return_cancelled.admin');
Route::post('/return/update_status', 'ReturnController@update_status')->name('returns.update_status');
Route::post('/return/update_return_status', 'ReturnController@update_return_status')->name('returns.update_return_status');
Route::post('/password/reset/mobile', 'PasswordResetController@reset_mobile_password')->name('password.reset.mobile');
Route::post('/password/create/mobile', 'PasswordResetController@create_mobile_password')->name('password.create.mobile');
Route::post('/users/create/generate/otp', 'HomeController@create_otp_generate')->name('generate.otp.create');
Route::post('/product/get_products_by_subsubcategory', 'ProductStockController@get_product_by_subsubcategory')->name('productsstock.get_product_by_subsubcategory');

//Route::get('/user/verify/otp', 'RegisterController@confirmOTP')->name('confirm.otp');
//Route::post('/user/verify/otp', 'RegisterController@ajax_confirming')->name('confirm.ajax_confirming.otp');