<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryConfig extends Model
{
    public function category_config_products()
    {
        return $this->hasMany(CategoryConfigProduct::class);
    }
}
