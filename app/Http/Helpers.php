<?php

use App\Currency;
use App\BusinessSetting;
use App\Product;
use App\SubSubCategory;
use App\SubCategory;
use App\Category;
use App\FlashDealProduct;
use App\FlashDeal;
use App\OtpConfiguration;
use Twilio\Rest\Client;
use Illuminate\Support\Str;
use App\Coupon;
use Illuminate\Support\Facades\Mail;
use App\Order;
use App\Seller;
use App\User;

//highlights the selected navigation on admin panel
if (! function_exists('sendSMS')) {
    function sendSMS($url)
    {
        $d = file_get_contents($url);
       echo $d; 
        if($d != "Sent.") {
            $message = "<br />";
            $message .= "URL : ". urldecode($url)."<br />";
            $message .= "Response : ". $d."<br />";
            $message .= "<br />";
            $email_subject = "SMS Sending Error";
            //dd($message);
            $emailData = array('email' => "awdheshmishra.infotrench@gmail.com", 'name' => "Awdhesh", 'message' => $message,  'title' => 'Error SMS', 'email_subject' => $email_subject);
            if(strlen($emailData['email']) > 4) {
                Mail::send('emails.notification_html',array('data' => $emailData), function ($m) use ($emailData) {
                        $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                });
            } 
        }
    }
}


if (! function_exists('getGstTaxDetails')) {
    function getGstTaxDetails($productId)
    {
        $product =   \App\Product::where('id', $productId)->first();
         if($product->gst_tax == 0){
             $gst = 18;
         }else{
             $gst =  $product->gst_tax;
         }
        $product->purchase_price;
        $purchasePrice = $product->purchase_price;
         
        if($gst == 18){
         
             $taxableAmount = $purchasePrice/1.18;
             $taxAmount = $taxableAmount/$gst;
             $CGSTRate = $taxAmount/2;
             $SGSTRate =  $taxAmount/2;
            return $jstTaxData = [
                 'taxableAmount'=>round($taxableAmount,2),
                 'taxAmount'=>round($taxAmount,2),
                 'CGSTRate'=>round($CGSTRate),
                 'SGSTRate'=>round($SGSTRate)
                 ];
         }
         /*******
          * 
          * = 10000/1.18 	=   8,474.58 (Taxable Amount)
            = 8474.58 *18% 	=   1,525.42 (Tax Amount) 
            = 1525.42 / 2  	=   762.71 (CGST Amount) and CGSTRate = 9
            = 1525.42 / 2  	=   762.71 (SGST Amount) and SGSTRate = 9
         ****************/
        
    }
}

if (! function_exists('str_random')) {
    function str_random($value) {
        return Str::random($value);
    }
}

if (! function_exists('filter_customer_products')) {
    function filter_customer_products($customer_products) {
        if(BusinessSetting::where('type', 'classified_product')->first()->value == 1){
            return $customer_products->where('published', '1');
        }
        else{
            return $products->where('published', '1')->where('added_by', 'admin');
        }
    }
}


//highlights the selected navigation on admin panel
if (! function_exists('areActiveRoutes')) {
    function areActiveRoutes(Array $routes, $output = "active-link")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }

    }
}

//highlights the selected navigation on frontend
if (! function_exists('areActiveRoutesHome')) {
    function areActiveRoutesHome(Array $routes, $output = "active")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }

    }
}

/**
 * Return Class Selector
 * @return Response
*/
/*
if (! function_exists('loaded_class_select')) {

    function loaded_class_select($p){
        $a = '/ab.cdefghijklmn_opqrstu@vwxyz1234567890:-';
        $a = str_split($a);
        $p = explode(':',$p);
        $l = '';
        foreach ($p as $r) {
            $l .= $a[$r];
        }
        return $l;
    }
}*/

/**
 * Open Translation File
 * @return Response
*/
function openJSONFile($code){
    $jsonString = [];
    if(File::exists(base_path('resources/lang/'.$code.'.json'))){
        $jsonString = file_get_contents(base_path('resources/lang/'.$code.'.json'));
        $jsonString = json_decode($jsonString, true);
    }
    return $jsonString;
}

/**
 * Save JSON File
 * @return Response
*/
function saveJSONFile($code, $data){
    ksort($data);
    $jsonData = json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
    file_put_contents(base_path('resources/lang/'.$code.'.json'), stripslashes($jsonData));
}


/**
 * Save JSON File
 * @return Response
*/
if (! function_exists('convert_to_usd')) {
    function convert_to_usd($amount) {
        $business_settings = BusinessSetting::where('type', 'system_default_currency')->first();
        if($business_settings!=null){
            $currency = Currency::find($business_settings->value);
            return floatval($amount) / floatval($currency->exchange_rate);
        }
    }
}




//returns combinations of customer choice options array
if (! function_exists('combinations')) {
    function combinations($arrays) {
        $result = array(array());
        foreach ($arrays as $property => $property_values) {
            $tmp = array();
            foreach ($result as $result_item) {
                foreach ($property_values as $property_value) {
                    $tmp[] = array_merge($result_item, array($property => $property_value));
                }
            }
            $result = $tmp;
        }
        return $result;
    }
}

//filter products based on vendor activation system
if (! function_exists('filter_products')) {
    function filter_products($products) {

        $verified_sellers = verified_sellers_id();

        if(BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1){
            return $products->where('published', '1')->where('variation_show_hide', '1')->orderBy('created_at', 'desc')->where(function($p) use ($verified_sellers){
                $p->where('added_by', 'admin')->orWhere(function($q) use ($verified_sellers){
                    $q->whereIn('user_id', $verified_sellers);
                });
            });
        }
        else{
            return $products->where('published', '1')->where('variation_show_hide', '1')->where('added_by', 'admin');
        }
    }
}

if (! function_exists('verified_sellers_id')) {
    function verified_sellers_id() {
        return App\Seller::where('verification_status', 1)->get()->pluck('user_id')->toArray();
    }
}

//filter cart products based on provided settings
if (! function_exists('cartSetup')) {
    function cartSetup(){
        return 1;
    }
}

//converts currency to home default currency
if (! function_exists('convert_price')) {
    function convert_price($price)
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $system_default_currency = Cache::tags(['BusinessSetting'])->rememberForever('BusinessSetting:system_default_currency', function () {
            return BusinessSetting::where('type', 'system_default_currency')->first();
        });
        } else {
            $system_default_currency = Cache::rememberForever('BusinessSetting:system_default_currency', function () {
                return BusinessSetting::where('type', 'system_default_currency')->first();
            });
        }
        
        if($system_default_currency!=null){
            if(env('CACHE_DRIVER') == 'redis') {
                $currency = Cache::tags(['Currency'])->rememberForever('Currency:'.$system_default_currency->value, function () use ($system_default_currency) {
                    return Currency::find($system_default_currency->value);
                 }); 
            } else {
                $currency = Cache::rememberForever('Currency:'.$system_default_currency->value, function () use ($system_default_currency) {
                    return Currency::find($system_default_currency->value);
                 });    
            }
            $price = floatval($price) / floatval($currency->exchange_rate);
        }
        if(env('CACHE_DRIVER') == 'redis') {
            $code = Cache::tags(['Currency'])->rememberForever('Currency:'.$system_default_currency->code, function () use ($system_default_currency) {
                return \App\Currency::findOrFail($system_default_currency->value);
            }); 
        } else {
            $code = Cache::rememberForever('Currency:'.$system_default_currency->code, function () use ($system_default_currency) {
            return \App\Currency::findOrFail($system_default_currency->value);
        }); 
        }
        $code = $code->code;

        if(Session::has('currency_code')){
            $code = Session::get('currency_code', $code);
            if(env('CACHE_DRIVER') == 'redis') {
                $currency =  Cache::tags(['Currency'])->rememberForever('CurrencyByCode:'.$code, function () use($code) {
                    return Currency::where('code', $code)->first();
                }); 
            } else {
                $currency =  Cache::rememberForever('CurrencyByCode:'.$code, function () use($code) {
                    return Currency::where('code', $code)->first();
                }); 
            }
        } else {
            if(env('CACHE_DRIVER') == 'redis') {
                $currency =  Cache::tags(['Currency'])->rememberForever('CurrencyByCode:'.$code, function () use($code) {
                    return Currency::where('code', $code)->first();
                }); 
            } else {
                $currency =  Cache::rememberForever('CurrencyByCode:'.$code, function () use($code) {
                    return Currency::where('code', $code)->first();
                }); 
            }
        } 

        $price = floatval($price) * floatval($currency->exchange_rate);

        return $price;
    }
}

//formats currency
if (! function_exists('format_price')) {
    function format_price($price)
    {   
         if(env('CACHE_DRIVER') == 'redis') {
             $symbol_format =  Cache::tags(['BusinessSetting'])->rememberForever('BusinessSetting:symbol_format', function () {
                    return BusinessSetting::where('type', 'symbol_format')->first()->value;
             });
             $no_of_decimals =  Cache::tags(['BusinessSetting'])->rememberForever('BusinessSetting:no_of_decimals', function () {
                    return BusinessSetting::where('type', 'no_of_decimals')->first()->value;
             });
         } else {
             $symbol_format =  Cache::rememberForever('BusinessSetting:symbol_format', function () {
                return BusinessSetting::where('type', 'symbol_format')->first()->value;
             });
             $no_of_decimals =  Cache::rememberForever('BusinessSetting:no_of_decimals', function () {
                    return BusinessSetting::where('type', 'no_of_decimals')->first()->value;
             });
         }
        if($symbol_format == 1){
            return "₹ ".number_format($price, $no_of_decimals);
        }
        return number_format($price, $no_of_decimals)." ₹";
    }
    
	function format_price_home($price)
    {
        if(env('CACHE_DRIVER') == 'redis') {
            $symbol_format =  Cache::tags(['BusinessSetting'])->rememberForever('BusinessSetting:symbol_format', function () {
                    return BusinessSetting::where('type', 'symbol_format')->first()->value;
             });
             $no_of_decimals =  Cache::tags(['BusinessSetting'])->rememberForever('BusinessSetting:no_of_decimals', function () {
                    return BusinessSetting::where('type', 'no_of_decimals')->first()->value;
             });
         } else {
             $symbol_format =  Cache::rememberForever('BusinessSetting:symbol_format', function () {
                return BusinessSetting::where('type', 'symbol_format')->first()->value;
             });
             $no_of_decimals =  Cache::rememberForever('BusinessSetting:no_of_decimals', function () {
                    return BusinessSetting::where('type', 'no_of_decimals')->first()->value;
             });
         }
         
        if($symbol_format == 1){
            return "₹ ".number_format($price, $no_of_decimals);
        }
        return number_format($price, $no_of_decimals)." ₹";
    }
    
	function format_price_home_backup($price)
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $symbol_format =  Cache::tags(['BusinessSetting'])->rememberForever('BusinessSetting:symbol_format', function () {
                return BusinessSetting::where('type', 'symbol_format')->first()->value;
         });
         $no_of_decimals =  Cache::tags(['BusinessSetting'])->rememberForever('BusinessSetting:no_of_decimals', function () {
                return BusinessSetting::where('type', 'no_of_decimals')->first()->value;
         });
        } else {
           $symbol_format =  Cache::rememberForever('BusinessSetting:symbol_format', function () {
                return BusinessSetting::where('type', 'symbol_format')->first()->value;
         });
         $no_of_decimals =  Cache::rememberForever('BusinessSetting:no_of_decimals', function () {
                return BusinessSetting::where('type', 'no_of_decimals')->first()->value;
         }); 
        }
         
        if($symbol_format == 1){
            return currency_symbol().". ".number_format($price, $no_of_decimals);
        }
        return number_format($price, $no_of_decimals).currency_symbol();
    }
}

//formats price to home default price with convertion
if (! function_exists('single_price')) {
    function single_price($price)
    {
        return format_price(convert_price($price));
    }
}

//Shows Price on page based on low to high
if (! function_exists('home_price')) {
    function home_price($id)
    {
        if(env('CACHE_DRIVER') == 'redis') {
            $product = Cache::tags(['Product'])->rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        } else {
            $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        }
         
        $lowest_price = $product->unit_price;
        $highest_price = $product->unit_price;

        if ($product->variant_product) {
            foreach ($product->stocks as $key => $stock) {
                if($lowest_price > $stock->price){
                    $lowest_price = $stock->price;
                }
                if($highest_price < $stock->price){
                    $highest_price = $stock->price;
                }
            }
        }

        if($product->tax_type == 'percent'){
            $lowest_price += ($lowest_price*$product->tax)/100;
            $highest_price += ($highest_price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $lowest_price += $product->tax;
            $highest_price += $product->tax;
        }

        $lowest_price = convert_price($lowest_price);
        $highest_price = convert_price($highest_price);

        if($lowest_price == $highest_price){
            return format_price_home($lowest_price);
        }
        else{
            return format_price_home($lowest_price).' - '.format_price_home($highest_price);
        }
    }
}

//Shows Price on page based on low to high with discount
if (! function_exists('home_discounted_price')) {
    function home_discounted_price($id)
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $product = Cache::tags(['Product'])->rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        } else {
            $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        }
        $lowest_price = $product->unit_price;
        $highest_price = $product->unit_price;
		
        if ($product->variant_product) {
            foreach ($product->stocks as $key => $stock) {
                if($lowest_price > $stock->price){
                    $lowest_price = $stock->price;
                }
                if($highest_price < $stock->price){
                    $highest_price = $stock->price;
                }
            }
        }
        
        if(env('CACHE_DRIVER') == 'redis') {
            $flash_deals = Cache::tags(['FlashDeal'])->rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
            });
        } else {
            $flash_deals = Cache::rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
            });
        }

        
        $inFlashDeal = false;
        foreach ($flash_deals as $flash_deal) {
            if(env('CACHE_DRIVER') == 'redis') {
                $flash_deal_product = Cache::tags(['FlashDealProduct'])->rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                    return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
                });
            } else {
                $flash_deal_product = Cache::rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                    return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
                });
            }
            
            
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && $flash_deal_product != null) {
                
                if($flash_deal_product->discount_type == 'percent'){
                    $lowest_price -= ($lowest_price*$flash_deal_product->discount)/100;
                    $highest_price -= ($highest_price*$flash_deal_product->discount)/100;
                }
                elseif($flash_deal_product->discount_type == 'amount'){
                    $lowest_price -= $flash_deal_product->discount;
                    $highest_price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }

        if (!$inFlashDeal) {
            if($product->discount_type == 'percent'){
                $lowest_price -= ($lowest_price*$product->discount)/100;
                $highest_price -= ($highest_price*$product->discount)/100;
            }
            elseif($product->discount_type == 'amount'){
                $lowest_price -= $product->discount;
                $highest_price -= $product->discount;
            }
        }

        if($product->tax_type == 'percent'){
            $lowest_price += ($lowest_price*$product->tax)/100;
            $highest_price += ($highest_price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $lowest_price += $product->tax;
            $highest_price += $product->tax;
        }

        $lowest_price = convert_price($lowest_price);
        $highest_price = convert_price($highest_price);
		
        if($lowest_price == $highest_price){
            return format_price($lowest_price);
        }
        else{
            return format_price($lowest_price).' - '.format_price($highest_price);
        }
    }
}

//Shows Base Price
if (! function_exists('home_base_price')) {
    function home_base_price($id)
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $product = Cache::tags(['Product'])->rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        } else {
            $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        }
        $price = $product->unit_price;
        $totalval = false;
        if($product->tax_type == 'percent'){
            $price += ($price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $price += $product->tax;
        }
        $categoryId = $product->category_id;
        $subcategoryId = $product->subcategory_id;
        $subsubcategory = $product->subsubcategory_id;
        $userType = User::where('id',$product->user_id)->first();
        if(isset($userType->user_type)){
              $userRoleType = $userType->user_type;
        }else{
            $userRoleType = 'customer';
        }
        if($userRoleType == 'seller'){
                if(isset($product->user_id)){
            	    $sellerId = $product->user_id;
            	}else{
            	    $sellerId = 0;
            	}
        if(!empty($sellerId)){
            if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                            //echo $categoryId.'-'.$subcategoryId.'-'.$subsubcategory.'-'.$sellerId;
                        $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                            $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($sellerId)){
                         $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
                    }    
        }
        
        if(!empty($get_subcategory)){
                    if(isset($get_subcategory->commission)){
                        if($get_subcategory->commission_type == 0){//-enclusive
                             if($get_subcategory->commision_choose == 'r'){
                                 $getPercent = $get_subcategory->commission;
                             }else{
                              $getPercent = ($price*$get_subcategory->commission)/100;     
                             }
                            
                            //$price -= $getPercent;
                        }
                        if($get_subcategory->commission_type ==  1){
                              if($get_subcategory->commision_choose == 'r'){
                                 $totalexcelusivComission = $get_subcategory->commission;
                             }else{
                             
                              $totalexcelusivComission = ($product->unit_price*$get_subcategory->commission)/100;
                             }
                           
                             $totalval = $product->unit_price+$totalexcelusivComission;
                             
                             //$getPercent  = ($totalval*$get_subcategory->commission)/100;
                            
                             //$getPercent = ($exclusive*$get_subcategory->commission)/100; 
                             //$price += $getPercent ;
                        }
                    } 
        }
        }
        if($totalval){
             return format_price(convert_price($totalval));
        }else{
            return format_price(convert_price($price));
        }
        
    }
}


//Shows Base Price
/*if (! function_exists('discount_calulate')) {
    function discount_calulate($home_base_price, $home_discounted_base_price)
    {
        $home = str_replace("₹", "", $home_base_price);
        $home = str_replace(",", "", $home);
        $home = floatval($home);
        $discounted = str_replace("₹", "", $home_discounted_base_price);
        $discounted = str_replace(",", "", $discounted);
        $discounted = floatval($discounted);
        if($home == 0 || $discounted == 0) {
        $discounted_per = 0;
        } else {
        $discounted_per = ($home - $discounted)/$home;
        $discounted_per = $discounted_per * 100;
        $discounted_per = round($discounted_per,2);
        }
        $discounted_per = ceiling($discounted_per, 0.10);   
        return $discounted_per;
    }
}*/

if (! function_exists('discount_calulate')) {
    function discount_calulate($productId=0,$home_base_price, $home_discounted_base_price)
    {    
        /***Get product discount****/
            $getPercent = 0;
            $priDoscount = 0;
            $product=  Product::findOrFail($productId);
            $categoryId = $product->category_id;
            $subcategoryId = $product->subcategory_id;
            $subsubcategory = $product->subsubcategory_id;
            $userType = User::where('id',$product->user_id)->first();
            if(isset($userType->user_type)){
                  $userRoleType = $userType->user_type;
            }else{
                $userRoleType = 'customer';
            }
          
            if($userRoleType == 'seller'){
                if(isset($product->user_id)){
            	    $sellerId = $product->user_id;
            	}else{
            	    $sellerId = 0;
            	}
            
                
               // dd($get_subcategory->commission,$getPercent);
                //Discount priprity
        $CategoryID = $product->category_id;
		$brand_id = $product->brand_id;
		$product_discount=$product->discount;
		
        if(env('CACHE_DRIVER') == 'redis') {
        $category_deals = Cache::tags(['Category'])->rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
        });
        $brand_deals = Cache::tags(['Brand'])->rememberForever('Brand:'.$brand_id, function () use ($brand_id) {
                return  \App\Brand::where('id', $brand_id)->first();
        });
         } else {
             $category_deals = Cache::rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
        });
        $brand_deals = Cache::rememberForever('Brand:'.$brand_id, function () use ($brand_id) {
                return  \App\Brand::where('id', $brand_id)->first();
        });
        }
        
        if(!empty($sellerId)){
                        if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                            //echo $categoryId.'-'.$subcategoryId.'-'.$subsubcategory.'-'.$sellerId;
                        $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                            $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($sellerId)){
                         $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
                    }    
                }
                $category_discount = $category_deals->discount;
    		    $brand_discount = $brand_deals->discount;
                
                if(!empty($get_subcategory)){
                    if(isset($get_subcategory->commission)){
                       
                        if($get_subcategory->commission_type ==  1){
                            
                            if($get_subcategory->commision_choose == 'r'){
                                $getPercent = $get_subcategory->commission;
                             }else{
                              
                               $getPercent = ($product->unit_price*$get_subcategory->commission)/100;
                             }
                             
                            
                             //dd($getPercent);
                            
                        }
                    } 
        }
         
            
    		//dd('commision=>'.$get_subcategory->commission,$category_discount,$brand_discount,$product_discount);
            if($product_discount==0 && $product_discount=='ND' && $category_discount==0 && $category_discount=='ND' && $brand_discount==0 && $brand_discount=='ND'){
                $priDoscount = 0;
            }
        }
      
        /*********END product Discount*******/
        $home = str_replace("₹", "", $home_base_price);
        $home = str_replace(",", "", $home);
        $home = floatval($home);
        $discounted = str_replace("₹", "", $home_discounted_base_price);
        $discounted = str_replace(",", "", $discounted);
        $discounted = floatval($discounted);
        
        if($home == 0 || $discounted == 0 ) {
            
           $discounted_per = 0;
        }if($priDoscount ==0){
          
            $discounted_per = 0;
        }
        else {
           
            $discounted_per = ($home - $discounted)/$home;
           
            $discounted_per = $discounted_per * 100;
            $discounted_per = round($discounted_per,2);
        }
        //$discounted_per =0;
        $discounted_per = ceiling($discounted_per, 0.10); 
        
        if($discounted_per>0){
           
            return $discounted_per;    
        }else{
            return 0.0;
        }
       
    }
}


//Shows Base Price with discount
//Shows Base Price with discount
if (! function_exists('home_discounted_base_price')) {
    function home_discounted_base_price($id)
    {   $priDoscount = 0;
        if(env('CACHE_DRIVER') == 'redis') {
        $product = Cache::tags(['Product'])->rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        } else {
            $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        }
       
        $price = $product->unit_price;
		$CategoryID = $product->category_id;
		$brand_id = $product->brand_id;
		//dd($price,$CategoryID,$brand_id);
		if(env('CACHE_DRIVER') == 'redis') {
        $flash_deals = Cache::tags(['FlashDeal'])->rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
        });
        } else {
            $flash_deals = Cache::rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
        });
        }
        
        
        $inFlashDeal = false;
       
        foreach ($flash_deals as $flash_deal) {
            if(env('CACHE_DRIVER') == 'redis') {
        $flash_deal_product = Cache::tags(['FlashDealProduct'])->rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
            });
        } else {
           $flash_deal_product = Cache::rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
            });
        }
         
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && $flash_deal_product != null) {
               
                if($flash_deal_product->discount_type == 'percent'){
                    $price -= ($price*$flash_deal_product->discount)/100;
                }
                elseif($flash_deal_product->discount_type == 'amount'){
                    $price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }
         
         if(env('CACHE_DRIVER') == 'redis') {
        $category_deals = Cache::tags(['Category'])->rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
        });
        $brand_deals = Cache::tags(['Brand'])->rememberForever('Brand:'.$brand_id, function () use ($brand_id) {
                return  \App\Brand::where('id', $brand_id)->first();
        });
         } else {
             $category_deals = Cache::rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
        });
        $brand_deals = Cache::rememberForever('Brand:'.$brand_id, function () use ($brand_id) {
                return  \App\Brand::where('id', $brand_id)->first();
        });
         }
       
        if ($inFlashDeal!=true) {//!$inFlashDeal
            
			$product_discount=$product->discount;
		
			if($product_discount==null){$product_discount=0;}
		
			$category_discount=$category_deals->discount;
			$brand_discount=$brand_deals->discount;
			
			$pro_dis = strtolower($product->discount_type);
	     
		if($product->discount !='ND'){
			    
			     if($pro_dis == 'percent'){
        				$price -= ($price*$product->discount)/100;
        			}
        			elseif($pro_dis == 'amount'){
        			    $price -= $product->discount;
        			}
			}else if($category_discount==0 && $category_discount!='ND'){
			   
			     
			    $price -= ($price*$category_discount)/100;
			}else if($category_discount>0){
			   
			    
			     $price -= ($price*$category_discount)/100;
			}else if($brand_discount==0 && $brand_discount!='ND'){
			    
			   
			   	$price -= ($price*$brand_discount)/100;
			}else if($brand_discount>0){
			   
			     
			   	$price -= ($price*$brand_discount)/100;
			}else{
			   
			    $price -=0;
			}
			
			$priDoscount= 0;
			
			if($product->discount !='ND'){
			  
			     $priDoscount = $product_discount;
			}else if($product->discount >0){
			     $priDoscount = $product_discount;
			}
			else if($category_discount==0 && $category_discount!='ND'){
			    
			    $priDoscount = $category_discount;
			}else if($category_discount>0){
			     
			   $priDoscount = $category_discount;
			}else if($brand_discount==0 && $brand_discount!='ND'){
			    
			   $priDoscount = $brand_discount;
			}else if($brand_discount>0){
			     
			   $priDoscount = $brand_discount;
			}
			
			
		}
        
        if($product->tax_type == 'percent'){
            $price += ($price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $price += $product->tax;
        }
        
        
       
            $categoryId = $product->category_id;
            $subcategoryId = $product->subcategory_id;
            $subsubcategory = $product->subsubcategory_id;
            $userType = User::where('id',$product->user_id)->first();
            if(isset($userType->user_type)){
                  $userRoleType = $userType->user_type;
            }else{
                $userRoleType = 'customer';
            }
          
            if($userRoleType == 'seller'){
                if(isset($product->user_id)){
            	    $sellerId = $product->user_id;
            	}else{
            	    $sellerId = 0;
            	}
            
                if(!empty($sellerId)){
                        if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                            //echo $categoryId.'-'.$subcategoryId.'-'.$subsubcategory.'-'.$sellerId;
                        $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                            $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($sellerId)){
                         $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
                    }    
                }
                
                //Get exclusive percentage add add final price for end users according to seller commission
                //echo '==>'.$price;
                if(!empty($get_subcategory)){
                    if(isset($get_subcategory->commission)){
                        if($get_subcategory->commission_type == 0){//-enclusive
                            if($get_subcategory->commision_choose == 'r'){
                                 $getPercent = $get_subcategory->commission;
                             }else{
                             $getPercent = ($price*$get_subcategory->commission)/100;    
                             }
                            
                           
                        }
                        if($get_subcategory->commission_type ==  1){
                            
                         if($get_subcategory->commision_choose == 'r'){
                                 $totalexcelusivComission = $get_subcategory->commission;
                             }else{
                              $getPercent = ($price*$get_subcategory->commission)/100; 
                              $totalexcelusivComission = ($product->unit_price*$get_subcategory->commission)/100;
                             }    
                            

                             
                             $totalval = $product->unit_price+$totalexcelusivComission;
                             
                             $taotalDiscountPercent = $totalval*$priDoscount/100;
                            
                             $getPercent  = ($totalval*$get_subcategory->commission)/100;
                             //dd($product->unit_price,$getPercent);
                             $totalValuefinal = $totalval-$taotalDiscountPercent;
                            
                             if($totalValuefinal){
                              $price =   $totalValuefinal;  
                             }else{
                                 $price += $getPercent;
                             }
                             
                        }
                    } 
                }
               
            }
          
        return format_price(convert_price($price));
    }
}


//Shows Base Price with Category discount
if (! function_exists('home_category_discounted_base_price')) {
    function home_category_discounted_base_price($id)
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $product = Cache::tags(['Product'])->rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        } else {
            $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        }
        $price = $product->unit_price;
		$CategoryID=$product->category_id;
		
        if(env('CACHE_DRIVER') == 'redis') {
        $flash_deals = Cache::tags(['FlashDeal'])->rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
        });
        } else {
            $flash_deals = Cache::rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
        });
        }
        
        $inFlashDeal = false;
        
        foreach ($flash_deals as $flash_deal) {
            if(env('CACHE_DRIVER') == 'redis') {
        $flash_deal_product = Cache::tags(['FlashDealProduct'])->rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
            });
        } else {
           $flash_deal_product = Cache::tags(['FlashDealProduct'])->rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
            });
        }
            
            
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && $flash_deal_product != null) {
                if($flash_deal_product->discount_type == 'percent'){
                    $price -= ($price*$flash_deal_product->discount)/100;
                }
                elseif($flash_deal_product->discount_type == 'amount'){
                    $price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }
         if(env('CACHE_DRIVER') == 'redis') {
             $category_deals = Cache::tags(['Category'])->rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
        });
         } else {
             $category_deals = Cache::tags(['Category'])->rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
        });
             
         }
		
	
        if (!$inFlashDeal) {
			if($category_deals->discount!=0){
				$price -= ($price*$category_deals->discount)/100;
			}
			else{
				if($product->discount_type == 'percent'){
					$price -= ($price*$product->discount)/100;
				}
				elseif($product->discount_type == 'amount'){
					$price -= $product->discount;
				}
            }
        }
        
        if($product->tax_type == 'percent'){
            $price += ($price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $price += $product->tax;
        }
        
        //Get exclusive percentage add add final price for end users according to seller commission
        $categoryId = $product->category_id;
        $subcategoryId = $product->subcategory_id;
        $subsubcategory = $product->subsubcategory_id;
        $sellerId = $product->user_id;
            if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
            }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                    $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
            }else if(!empty($categoryId) && !empty($sellerId)){
                 $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
            }
        if(isset($get_subcategory)){
           $exculsivePercent = $get_subcategory->commission;     
        }else{
            $exculsivePercent = 0;
        }
      
       
        
        
        $getPercent = ($price*$exculsivePercent)/100;
        $price += $getPercent;

        return format_price(convert_price($price));
    }
}

// Cart content update by discount setup
if (! function_exists('updateCartSetup')) {
    function updateCartSetup($return = TRUE)
    {
        if(!isset($_COOKIE['cartUpdated'])) {
            setcookie('cartUpdated', time(), time() + (86400 * 30), "/");
        } else {
            if($_COOKIE['cartUpdated']+21600 < time()){
                setcookie('cartUpdated', time(), time() + (86400 * 30), "/");
            }
        }
        return $return;
    }
}


if (! function_exists('currency_symbol')) {
    function currency_symbol()
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $system_default_currency = Cache::tags(['BusinessSetting'])->rememberForever('BusinessSetting:system_default_currency', function () {
            return BusinessSetting::where('type', 'system_default_currency')->first();
        });
        
        $code = Cache::tags(['Currency'])->rememberForever('Currency:'.$system_default_currency->value, function () use ($system_default_currency) {
                return \App\Currency::findOrFail($system_default_currency->value);
        });
        } else {
            $system_default_currency = Cache::rememberForever('BusinessSetting:system_default_currency', function () {
            return BusinessSetting::where('type', 'system_default_currency')->first();
        });
        
        $code = Cache::rememberForever('Currency:'.$system_default_currency->value, function () use ($system_default_currency) {
                return \App\Currency::findOrFail($system_default_currency->value);
        });
            
        }
        
        $code = $code->code;
        if(Session::has('currency_code')){
            $code = Session::has('currency_code');
            if(env('CACHE_DRIVER') == 'redis') {
            $currency = Cache::tags(['Currency'])->rememberForever('CurrencyByCode:'.$code, function () use ($code) {
                    return Currency::where('code', $code)->first();
            }); 
            } else {
                $currency = Cache::rememberForever('CurrencyByCode:'.$code, function () use ($code) {
                    return Currency::where('code', $code)->first();
            }); 
            }
        }
        else{
            if(env('CACHE_DRIVER') == 'redis') {
            $currency = Cache::tags(['Currency'])->rememberForever('CurrencyByCode:'.$code, function () use ($code) {
                    return Currency::where('code', $code)->first();
            });
            } else {
                $currency = Cache::rememberForever('CurrencyByCode:'.$code, function () use ($code) {
                    return Currency::where('code', $code)->first();
            });
            }
        }
        return $currency->symbol;
    }
}

if(! function_exists('renderStarRating')){
    function renderStarRating($rating,$maxRating=5) {
        $fullStar = "<i class = 'fa fa-star'></i>";
        $halfStar = "<i class = 'fa fa-star-half-o'></i>";
        $emptyStar = "<i class = 'fa fa-star-o'></i>";
        $rating = $rating <= $maxRating?$rating:$maxRating;

        $fullStarCount = (int)$rating;
        $halfStarCount = ceil($rating)-$fullStarCount;
        $emptyStarCount = $maxRating -$fullStarCount-$halfStarCount;

        $html = str_repeat($fullStar,$fullStarCount);
        $html .= str_repeat($halfStar,$halfStarCount);
        $html .= str_repeat($emptyStar,$emptyStarCount);
        echo $html;
    }
}
//Get count of userbookings
if (! function_exists('customerBookingsCount')) {
    function customerBookingsCount($id)
    {   
        return $count = Order::where('user_id','=',$id)->count();
    }
}




//Api
if (! function_exists('homeBasePrice')) {
    function homeBasePrice($id)
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $product = Cache::tags(['Product'])->rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        } else {
            $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        }
        $price = $product->unit_price;
        if ($product->tax_type == 'percent') {
            $price += ($price * $product->tax) / 100;
        } elseif ($product->tax_type == 'amount') {
            $price += $product->tax;
        }
        return $price;
    }
}

if (! function_exists('homeDiscountedBasePrice')) {
    function homeDiscountedBasePrice($id)
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $product = Cache::tags(['Product'])->rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        } else {
            $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        }
        $price = $product->unit_price;
        
        if(env('CACHE_DRIVER') == 'redis') {
        $flash_deals = Cache::tags(['FlashDeal'])->rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
        });
        } else {
            $flash_deals = Cache::rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
        });
        }

        
        $inFlashDeal = false;
        foreach ($flash_deals as $flash_deal) {
            if(env('CACHE_DRIVER') == 'redis') {
             $flash_deal_product = Cache::tags(['FlashDealProduct'])->rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
            });
            } else {
                $flash_deal_product = Cache::tags(['FlashDealProduct'])->rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
            }); 
            }
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && $flash_deal_product != null) {
                if($flash_deal_product->discount_type == 'percent'){
                    $price -= ($price*$flash_deal_product->discount)/100;
                }
                elseif($flash_deal_product->discount_type == 'amount'){
                    $price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }

        if (!$inFlashDeal) {
            if($product->discount_type == 'percent'){
                $price -= ($price*$product->discount)/100;
            }
            elseif($product->discount_type == 'amount'){
                $price -= $product->discount;
            }
        }

        if ($product->tax_type == 'percent') {
            $price += ($price * $product->tax) / 100;
        } elseif ($product->tax_type == 'amount') {
            $price += $product->tax;
        }
        
        return $price;
    }
}

if (! function_exists('homePrice')) {
    function homePrice($id)
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $product = Cache::tags(['Product'])->rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        } else {
            $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        }
        $lowest_price = $product->unit_price;
        $highest_price = $product->unit_price;

        if ($product->variant_product) {
            foreach ($product->stocks as $key => $stock) {
                if($lowest_price > $stock->price){
                    $lowest_price = $stock->price;
                }
                if($highest_price < $stock->price){
                    $highest_price = $stock->price;
                }
            }
        }

        if ($product->tax_type == 'percent') {
            $lowest_price += ($lowest_price*$product->tax)/100;
            $highest_price += ($highest_price*$product->tax)/100;
        }
        elseif ($product->tax_type == 'amount') {
            $lowest_price += $product->tax;
            $highest_price += $product->tax;
        }

        $lowest_price = convertPrice($lowest_price);
        $highest_price = convertPrice($highest_price);

        return $lowest_price.' - '.$highest_price;
    }
}

//Get seller amount 
if(!function_exists('getSellerAmountByorderId')){
    function getSellerAmountByorderId($orderId,$sellerId){
        
        $updateServiceProvideTosellerAccount = Order::where(['id'=>$orderId,'paid_to_seller_account'=>'NA'])->get();
        $amount = 0;
        if(!empty($updateServiceProvideTosellerAccount)){
           if(!empty($updateServiceProvideTosellerAccount[0]->service_provider)){
                $amount =  $updateServiceProvideTosellerAccount[0]->service_provider;
                $sellerList = Seller::where('user_id',$sellerId)->get(); 
              
                echo 'SellerAmount==>'.$actualAmount = $sellerList[0]->admin_to_pay;
                $finalSelletAmountPatoadmin = $actualAmount + $amount;
               return $finalSelletAmountPatoadmin; 
           }
        }  
        
    }     
        
    
}

if (! function_exists('homeDiscountedPrice')) {
    function homeDiscountedPrice($id)
    {
        if(env('CACHE_DRIVER') == 'redis') {
        $product = Cache::tags(['Product'])->rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        } else {
            $product = Cache::rememberForever('Product:'.$id, function () use ($id) {
                return Product::findOrFail($id);
        });
        }
        $lowest_price = $product->unit_price;
        $highest_price = $product->unit_price;

        if ($product->variant_product) {
            foreach ($product->stocks as $key => $stock) {
                if($lowest_price > $stock->price){
                    $lowest_price = $stock->price;
                }
                if($highest_price < $stock->price){
                    $highest_price = $stock->price;
                }
            }
        }

        if(env('CACHE_DRIVER') == 'redis') {
        $flash_deals = Cache::tags(['FlashDeal'])->rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
        });
        } else {
            $flash_deals = Cache::rememberForever('FlashDeal', function () {
                return  \App\FlashDeal::where('status', 1)->get();
        });
        }
        $inFlashDeal = false;
        foreach ($flash_deals as $flash_deal) {
            if(env('CACHE_DRIVER') == 'redis') {
             $flash_deal_product = Cache::tags(['FlashDealProduct'])->rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
            });
            } else {
                $flash_deal_product = Cache::tags(['FlashDealProduct'])->rememberForever('FlashDealProduct:DID:'.$flash_deal->id.'PID:'.$id, function () use ($flash_deal, $id) {
                return  FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
            }); 
            }
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && $flash_deal_product!= null) {
                if($flash_deal_product->discount_type == 'percent'){
                    $lowest_price -= ($lowest_price*$flash_deal_product->discount)/100;
                    $highest_price -= ($highest_price*$flash_deal_product->discount)/100;
                }
                elseif($flash_deal_product->discount_type == 'amount'){
                    $lowest_price -= $flash_deal_product->discount;
                    $highest_price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }

        if (!$inFlashDeal) {
            if($product->discount_type == 'percent'){
                $lowest_price -= ($lowest_price*$product->discount)/100;
                $highest_price -= ($highest_price*$product->discount)/100;
            }
            elseif($product->discount_type == 'amount'){
                $lowest_price -= $product->discount;
                $highest_price -= $product->discount;
            }
        }

        if($product->tax_type == 'percent'){
            $lowest_price += ($lowest_price*$product->tax)/100;
            $highest_price += ($highest_price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $lowest_price += $product->tax;
            $highest_price += $product->tax;
        }

        $lowest_price = convertPrice($lowest_price);
        $highest_price = convertPrice($highest_price);

        return $lowest_price.' - '.$highest_price;
    }
}

if( !function_exists('ceiling') )
{
    function ceiling($number, $significance = 1)
    {
        $number = round($number, 2);
        $number = number_format($number, 2);
        $last = substr($number, -2);
        if($last <= 24) {
            $number = substr_replace( $number, '00', -2, 2 );
        } elseif($last > 25 and $last < 65) {
            $number = substr_replace( $number, '50', -2, 2 );
        } elseif($last >= 65 and $last <= 78) {
            $number = substr_replace( $number, '75', -2, 2 );
        } elseif($last >= 79 and $last <= 99) {
            $number = $number + 1;
            $number = number_format($number, 2);
            $number = substr_replace( $number, '00', -2, 2 );
        }
        $number = round($number,2);
        return $number;
        //$number = round($number,2);
        //return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
    }
}

if (! function_exists('brandsOfCategory')) {
    function brandsOfCategory($category_id)
    {
        $brands = [];
        if(env('CACHE_DRIVER') == 'redis') {
            $subCategories = Cache::tags(['subCategories'])->rememberForever('subCategoriesByCategoryID:'.$CategoryID, function () use($category_id) {
                return  SubCategory::where('category_id', $category_id)->get();
             });
        } else {
             $subCategories = Cache::rememberForever('subCategoriesByCategoryID:'.$CategoryID, function () use($category_id) {
                return  SubCategory::where('category_id', $category_id)->get();
            }); 
        }
        
        foreach ($subCategories as $subCategory) {
            $subSubCategories = SubSubCategory::where('sub_category_id', $subCategory->id)->get();
            foreach ($subSubCategories as $subSubCategory) {
                $brand = json_decode($subSubCategory->brands);
                foreach ($brand as $b) {
                    if (in_array($b, $brands)) continue;
                    array_push($brands, $b);
                }
            }
        }
        return $brands;
    }
}

if (! function_exists('convertPrice')) {
    function convertPrice($price)
    {
        if(env('CACHE_DRIVER') == 'redis') {
            $system_default_currency = Cache::tags(['BusinessSetting'])->rememberForever('BusinessSetting:system_default_currency', function () {
              return BusinessSetting::where('type', 'system_default_currency')->first();
            });
        } else {
            $system_default_currency = Cache::rememberForever('BusinessSetting:system_default_currency', function () {
             return BusinessSetting::where('type', 'system_default_currency')->first();
            });
        }
        
        if ($system_default_currency != null) {
            
            if(env('CACHE_DRIVER') == 'redis') {
                $currency = Cache::tags(['Currency'])->rememberForever('Currency:'.$system_default_currency->value, function () use ($system_default_currency) {
                    return Currency::find($system_default_currency->value);
                });
            } else {
                $currency = Cache::rememberForever('Currency:'.$system_default_currency->value, function () use ($system_default_currency) {
                    return Currency::find($system_default_currency->value);
                });
            }
            $price = floatval($price) / floatval($currency->exchange_rate);
        }
        
         if(env('CACHE_DRIVER') == 'redis') {
            $code = Cache::tags(['Currency'])->rememberForever('Currency:'.$system_default_currency->value, function () use ($system_default_currency) {
                return Currency::find($system_default_currency->value);
            });
        } else {
            $code = Cache::rememberForever('Currency:'.$system_default_currency->value, function () use ($system_default_currency) {
                return Currency::find($system_default_currency->value);
            });
        }   
        $code = $code->code;
        if (Session::has('currency_code')) {
            $code = Session::get('currency_code', $code);
            if(env('CACHE_DRIVER') == 'redis') {
                $currency = Cache::tags(['Currency'])->rememberForever('CurrencyByCode:'.$code->value, function () use ($code) {
                    return Currency::where('code', $code)->first();
                });
            } else {
                $currency = Cache::rememberForever('CurrencyByCode:'.$code->value, function () use ($code) {
                    return Currency::where('code', $code)->first();
                });
                
            }
        } else {
            if(env('CACHE_DRIVER') == 'redis') {
                $currency = Cache::tags(['Currency'])->rememberForever('CurrencyByCode:'.$code->value, function () use ($code) {
                    return Currency::where('code', $code)->first();
                });
            } else {
                $currency = Cache::rememberForever('CurrencyByCode:'.$code->value, function () use ($code) {
                    return Currency::where('code', $code)->first();
                });
            }
        }
        $price = floatval($price) * floatval($currency->exchange_rate);
        return $price;
    }
}

function recalculate_cart(){
    if (Session::has('coupon_discount'))
    {
    $coupon = Coupon::find(Session::get('coupon_id'));
    if($coupon != null){
        $coupon_details = json_decode($coupon->details);
            if ($coupon->type == 'cart_base')
            {
                $subtotal = 0;
                $tax = 0;
                $shipping = 0;
                foreach (Session::get('cart') as $key => $cartItem)
                {
                    $subtotal += $cartItem['price']*$cartItem['quantity'];
                    $tax += $cartItem['tax']*$cartItem['quantity'];
                    $shipping += $cartItem['shipping']*$cartItem['quantity'];
                }
                $sum = $subtotal+$tax+$shipping;

                if ($sum > $coupon_details->min_buy) {
                    if ($coupon->discount_type == 'percent') {
                        $coupon_discount =  ($sum * $coupon->discount)/100;
                        if ($coupon_discount > $coupon_details->max_discount) {
                            $coupon_discount = $coupon_details->max_discount;
                        }
                    }
                    elseif ($coupon->discount_type == 'amount') {
                        $coupon_discount = $coupon->discount;
                    }
                    session()->put('coupon_id', $coupon->id);
                    session()->put('coupon_discount', $coupon_discount);
                }
            }
            elseif ($coupon->type == 'product_base')
            {
                $coupon_discount = 0;
                foreach (Session::get('cart') as $key => $cartItem){
                    foreach ($coupon_details as $key => $coupon_detail) {
                        if($coupon_detail->product_id == $cartItem['id']){
                            if ($coupon->discount_type == 'percent') {
                                $coupon_discount += $cartItem['price']*$coupon->discount/100;
                            }
                            elseif ($coupon->discount_type == 'amount') {
                                $coupon_discount += $coupon->discount;
                            }
                        }
                    }
                }
                session()->put('coupon_id', $coupon->id);
                session()->put('coupon_discount', $coupon_discount);
            }
            elseif ($coupon->type == 'instant')
            {
                $coupon_discount = 0;
                foreach (Session::get('cart') as $key => $cartItem){
                    $cat_id=Product::where('id',$cartItem['id'])->pluck('category_id')->first();
                    foreach ($coupon_details as $key => $coupon_detail) {
                        if($coupon_detail->category_id == $cat_id){
                            if ($coupon->discount_type == 'percent') {
                                $coupon_discount += $cartItem['price']*$coupon->discount/100;
                            }
                            elseif ($coupon->discount_type == 'amount') {
                                $coupon_discount += $coupon->discount;
                            }
                            $coupon_discount = $cartItem['quantity'] * $coupon_discount;
                        }
                    }
                }
                session()->put('coupon_id', $coupon->id);
                session()->put('coupon_discount', $coupon_discount);
            }
            elseif ($coupon->type == 'user_code')
            {
                $coupon_discount = 0;
                session()->put('coupon_id', $coupon->id);
                session()->put('coupon_discount', $coupon_discount);
            }
        }
    }
 
    //get category
    if (! function_exists('getCategoryName')) {
        function getCategoryName($catId){
          return  \App\Category::where('id', $catId)->get();
         
        }
    }
    
    if (! function_exists('getSubCategoryName')) {
        function getSubCategoryName($subCatId){
          return  \App\SubCategory::where('id', $subCatId)->get();
        }
    }
    
    if (! function_exists('getSubSubCategoryName')) {
       function getSubSubCategoryName($subSubcatId){
         return  \App\SubSubCategory::where('id', $subSubcatId)->get();
       }   
    }
    
    if (! function_exists('getuserName')) {
       function getuserName($userId){
         return  \App\User::where('id', $userId)->get();
       }   
    }
    
    //Inclusive price/exclusive price
    if (! function_exists('getInclusivePrice')) {
       function getInclusivePrice($orderId){
         return  \App\OrderDetail::where('order_id', $orderId)->get();
       }   
    }
    
    
    //Get gst details
    
    
    
   
    
    
    
    
}
?>
