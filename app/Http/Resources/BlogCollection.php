<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BlogCollection extends ResourceCollection
{
    public function toArray($request)
    {  
        return [
            'data' => $this->collection->map(function($data) {
                return [
                    'BlogTitle' => $data->BlogTitle,
                    'BlogSummary'=>$data->BlogSummary,
                    'BlogContent'=>$data->BlogContent,
                    'URLSlug'=>$data->URLSlug,
                    'FeaturedImage'=>$data->FeaturedImage,
                    'SEO_metatags'=>$data->SEO_metatags,
                    'SEO_metaDescription'=>$data->SEO_metaDescription
                   
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => "true",
            'status' => 200
        ];
    }
}
