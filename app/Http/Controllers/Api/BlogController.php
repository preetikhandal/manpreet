<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\BlogCollection;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index()
    {
        return new BlogCollection(Blog::all());
    }
    
    public function blogDetails($id){
        
         return new BlogCollection(Blog::where('id', $id)->get());
    }

    public function top()
    {
       // return new BrandCollection(Brand::where('top', 1)->get());
    }
}
