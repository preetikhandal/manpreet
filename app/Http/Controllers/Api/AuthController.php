<?php /** @noinspection PhpUndefinedClassInspection */

namespace App\Http\Controllers\Api;

use App\Models\BusinessSetting;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6',
            
        ]);
        $name = $request->name;
        $email = $request->email;
        $uniqueid  = Str::uuid()->toString();
        $postJsonData = array(
            	"is_new_mode"=> true,
            	"partyInformation"=> array(
            		"id"=> $uniqueid,
            		"name"=> $name,
            		"under_ledger_id"=> "ac7e815e-54fd-4702-af22-b4344e49120d",
            		"old_under_ledger_id"=> "",
            		"is_import_mode"=> false,
            		"print_name"=>$name,
            		"is_subparty"=> false,
            		"is_common"=> false,
            		"inactive"=> false,
            		"under_party_id"=> "",
            		"firm_type"=> 0,
            		"territory_id"=> "",
            		"contact_person"=> "",
            		"shipping_address_same"=> false,
            		"gst"=> "07AAACE7420R1ZB",
            		"gst_category"=> 0,
            		"distance"=> 0,
            		"gst_effective_from"=> "",
            		"tax_style"=> 0,
            		"cin"=> "",
            		"pan"=> "",
            		"tds_applicable"=> false,
            		"gst_based_tds"=> false,
            		"tds_section_id"=> "",
            		"payment_term_id"=> "",
            		"price_category_id"=> "",
            		"sales_executive_id"=> "",
            		"agent_id"=> "",
            		"transporter_id"=> "",
            		"credit_limit"=> 0,
            		"interest_rate"=> 0,
            		"bank_name"=> "",
            		"bank_branch"=> "",
            		"bank_account_no"=> "",
            		"bank_rtgs_no"=> "",
            		"party_category_id"=> "",
            		"allow_mobile_access"=> false,
            		"mobile_access_password"=> "",
            		"customer_on_watch"=> false,
            		"fca_622_id"=> "",
            		"fca_mdb_id"=> "",
            		"tally_id"=> "",
            		"billing_address"=> array(
            			"address"=> "",
            			"country"=> "IN",
            			"state"=> "f49337bc-a6ea-4183-89a4-3cb9c692e361",
            			"state_name"=> "Delhi",
            			"city"=> "",
            			"city_name"=> "",
            			"pin"=> "110052",
            			"phone"=> $request->phone,
            			"email"=> "",
            			"longitude"=>"",
            			"latitude"=>""
                		),
                		"shipping_address"=>array (
                			"address"=> "",
                			"country"=> "IN",
                			"state"=> "",
                			"state_name"=> "",
                			"city"=> "",
                			"city_name"=> "",
                			"pin"=> "",
                			"phone"=> "",
                			"email"=> $email,
                			"longitude"=> "",
                			"latitude"=> ""
                		),
                		"udf_list"=> ["", "", "", "", ""],
                		"fl_names"=> [],
                		"code_config"=> array(
                			"code"=> "",
                			"code_no"=> 0,
                			"code_prefix"=> ""
                		)
                	)
                );
            
       
        $user = new User([
            'name' => $name,
            'alignbook_customer_id' => $uniqueid,
            'email' => $email,
            'phone'=>$request->phone,
            'password' => bcrypt($request->password),
            'email_verified_at' => Carbon::now()
        ]);
        $endPoint = 'SaveUpdate_Customer';
        $addCustomeInAlignBook = $this->addCustomerInAlignBook(json_encode($postJsonData),$endPoint);
        $jsondRepons = json_encode($addCustomeInAlignBook,true);
        $respondData = json_decode($jsondRepons);
        if($respondData->ReturnCode == 0){
             $user->save();
            $customer = new Customer;
            $customer->user_id = $user->id;
            $customer->save();
             $userDetails = User::where('id',$user->id)->first();
            return response()->json([
                'status_cod'=>200,
                'status'=>'success',
                'data'=>$userDetails,
                'message' => 'Registration Successful. Please log in to your account'
            ], 201); 
        }else{
            return $this->loginFailed('Something went wrong',500);
        }
       
    }

    public function login(Request $request)
    {    
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
      
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json(['message' => 'Unauthorized'], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        return $this->loginSuccess($tokenResult, $user);
        
    }
    
    public function user(Request $request)
    {   dd($request->all());
        return response()->json($request->user());
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function socialLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email'
        ]);
        $uniqueid  = Str::uuid()->toString();
             $postJsonData = array(
            	"is_new_mode"=> true,
            	"partyInformation"=> array(
            		"id"=> $uniqueid,
            		"name"=> $name,
            		"under_ledger_id"=> "ac7e815e-54fd-4702-af22-b4344e49120d",
            		"old_under_ledger_id"=> "",
            		"is_import_mode"=> false,
            		"print_name"=>$name,
            		"is_subparty"=> false,
            		"is_common"=> false,
            		"inactive"=> false,
            		"under_party_id"=> "",
            		"firm_type"=> 0,
            		"territory_id"=> "",
            		"contact_person"=> "",
            		"shipping_address_same"=> false,
            		"gst"=> "07AAACE7420R1ZB",
            		"gst_category"=> 0,
            		"distance"=> 0,
            		"gst_effective_from"=> "",
            		"tax_style"=> 0,
            		"cin"=> "",
            		"pan"=> "",
            		"tds_applicable"=> false,
            		"gst_based_tds"=> false,
            		"tds_section_id"=> "",
            		"payment_term_id"=> "",
            		"price_category_id"=> "",
            		"sales_executive_id"=> "",
            		"agent_id"=> "",
            		"transporter_id"=> "",
            		"credit_limit"=> 0,
            		"interest_rate"=> 0,
            		"bank_name"=> "",
            		"bank_branch"=> "",
            		"bank_account_no"=> "",
            		"bank_rtgs_no"=> "",
            		"party_category_id"=> "",
            		"allow_mobile_access"=> false,
            		"mobile_access_password"=> "",
            		"customer_on_watch"=> false,
            		"fca_622_id"=> "",
            		"fca_mdb_id"=> "",
            		"tally_id"=> "",
            		"billing_address"=> array(
            			"address"=> "",
            			"country"=> "IN",
            			"state"=> "f49337bc-a6ea-4183-89a4-3cb9c692e361",
            			"state_name"=> "Delhi",
            			"city"=> "",
            			"city_name"=> "",
            			"pin"=> "110052",
            			"phone"=> "",
            			"email"=> "",
            			"longitude"=>"",
            			"latitude"=>""
                		),
                		"shipping_address"=>array (
                			"address"=> "",
                			"country"=> "IN",
                			"state"=> "",
                			"state_name"=> "",
                			"city"=> "",
                			"city_name"=> "",
                			"pin"=> "",
                			"phone"=> "",
                			"email"=> $email,
                			"longitude"=> "",
                			"latitude"=> ""
                		),
                		"udf_list"=> ["", "", "", "", ""],
                		"fl_names"=> [],
                		"code_config"=> array(
                			"code"=> "",
                			"code_no"=> 0,
                			"code_prefix"=> ""
                		)
                	)
                );
        if (User::where('email', $request->email)->count() > 0) {
            $user = User::where('email', $request->email)->first();
        } else {
            $addCustomeInAlignBook = $this->addCustomerInAlignBook(json_encode($postJsonData),$endPoint);
            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'alignbook_customer_id'=>$uniqueid,
                'provider_id' => $request->provider,
                'email_verified_at' => Carbon::now()
            ]);
            $jsondRepons = json_encode($addCustomeInAlignBook,true);
            $respondData = json_decode($jsondRepons);
             if($respondData->ReturnCode == 0){
                 $user->save();
                  $customer = new Customer;
                  $customer->user_id = $user->id;
                  $customer->save();
                  $tokenResult = $user->createToken('Personal Access Token');
                return $this->loginSuccess($tokenResult, $user);
             }else{
                 return $this->loginFailed('Something went wrong','500');
             }
           
           
        }
       
    }
    
    
    protected function loginFailed($msg,$code){
        $response = array(
            'Mesage'=>$msg,
            'code'=>$code
            );
            return json_encode($response);
    }
    
    protected function loginSuccess($tokenResult, $user)
    {
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user' => [
                'id' => $user->id,
                'type' => $user->user_type,
                'name' => $user->name,
                'email' => $user->email,
                'avatar' => $user->avatar,
                'avatar_original' => $user->avatar_original,
                'address' => $user->address,
                'country'  => $user->country,
                'city' => $user->city,
                'postal_code' => $user->postal_code,
                'phone' => $user->phone
            ]
        ]);
    }
    
    
    public function addCustomerInAlignBook($requestJson,$endPoint){
	   // dd($requestJson);
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
       return  $data = json_decode($response1);
	}
}
