<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\OTPVerificationController;
use App\Http\Controllers\ClubPointController;
use App\Http\Controllers\AffiliateController;
use App\Order;
use App\Product;
use App\Color;
use App\OrderDetail;
use App\CouponUsage;
use App\Coupon;
use App\OtpConfiguration;
use App\User;
use App\BusinessSetting;
use App\Expenses;
use App\ExpensesDetails;
use App\ExpensesPayments;
use App\DeliveryLog;
use App\Wallet;
use App\Category;
use Auth;
use Session;
use DB;
use PDF;
use Mail;
use Carbon\Carbon;
use App\Mail\InvoiceEmailManager;
use Excel;
use App\OrderExportExcel;
use Cookie;
use App\Log;
use App\Cashback;
use App\UserCashbackConfig;
use App\Seller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource to seller.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {     
        if(Auth::User()->user_type == "vendor") {
            $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('order_details.seller_id', Auth::user()->id)
                    ->whereIn('order_details.delivery_status', ["pending","on_review",'delivered'])
                    ->select('orders.id', 'orders.code')
                    ->distinct()
                    ->paginate(15);
            
            foreach ($orders as $key => $value) {
                $order = \App\Order::find($value->id);
                $order->viewed = 1;
                $order->save();
            }
            return view('frontend.vendor.orders', compact('orders'));
        } else {
            $deliveryStatus = array();
            $filter = $request->input('order_filter', 1);
            if($filter == 1) {
                $deliveryStatus[] = "pending";
                $deliveryStatus[] = "ready_to_ship";
                $deliveryStatus[] = "cancel";
            }
            if($filter == 2) {
                $deliveryStatus[] = "pending";
            }
            if($filter == 3) {
                $deliveryStatus[] = "ready_to_ship";
            }
            if($filter == 4) {
                $deliveryStatus[] = "shipped";
            }
            if($filter == 5) {
                $deliveryStatus[] = "delivered";
            }
            
            $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('order_details.seller_id', Auth::user()->id)
                    ->select('orders.id', 'orders.code');
                    
            if(count($deliveryStatus) > 0) {       
            $orders = $orders->whereIn('order_details.delivery_status', $deliveryStatus);
            }
            $orders = $orders->distinct()->paginate(15);
            
            return view('frontend.seller.orders', compact('orders','filter'));
        }
    }
    
    
    public function vendor_invoice_generate(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        return view('frontend.partials.vendor_generate_invoice', compact('order'));
    }
    
    public function vendor_invoice_generating(Request $request)
    {
        $vendor_id = Auth::id();
        $OrderDetails = OrderDetail::where('order_id',$request->order_id)->where('seller_id',$vendor_id)->get();
        
        $categoryID=$request->input("category");
		$expensename=$request->input("expensename");
		$invoicedate=$request->input("invoicedate");
		$invoiceno=$request->input("invoiceno");
		$igst=$request->input("igst");
		$cgst=$request->input("cgst");
		$sgst=$request->input("sgst");
		$invoiceamount=$request->input("invoiceamount");
		$expenses_types=$request->input("expenses_type",array());
		$category_id=$request->input("category_id");
		$item_description=$request->input("item_description");
		$unit_price=$request->input("unit_price");
		$quantity=$request->input("quantity");
		$sub_total=$request->input("sub_total");
		$invoicedate=Carbon::createFromFormat('d/m/Y', $invoicedate)->toDateString();
		$Status="Unpaid";
		$Expenses = new Expenses;
		$Expenses->vendor_id = $vendor_id;
		$Expenses->ExpenseName = $expensename;
		$Expenses->ECategoryID = $categoryID;
		$Expenses->ExpenseInvoiceNo = $invoiceno;
		$Expenses->ExpenseInvoiceDate = $invoicedate;
		$Expenses->igst = $igst;
		$Expenses->cgst = $cgst;
		$Expenses->sgst = $sgst;
		$Expenses->ExpensesInvoiceAmount = $invoiceamount;
		$Expenses->PaidAmount = 0;
		$Expenses->OutstandingAmount = $invoiceamount;
		$Expenses->PaymentStatus = $Status;
		$Expenses->save();
        
        foreach($expenses_types as $key => $expenses_type) {
            $ExpensesDetails = new ExpensesDetails;
            $ExpensesDetails->expenses_id = $Expenses->ExpenseID;
            $ExpensesDetails->expenses_type = $expenses_type;
            if($expenses_type == "Product Expenses") {
                $Product = \App\Product::where('id',$item_description[$key])->select('id', 'name', 'current_stock')->first();
                $ExpensesDetails->category_id = $category_id[$key];
                $ExpensesDetails->product_id = $item_description[$key];
                $ExpensesDetails->item_description = $Product->name;
            } else {
                $ExpensesDetails->category_id = 0;
                $ExpensesDetails->product_id = 0;
                $ExpensesDetails->item_description = $item_description[$key];
            }
            $ExpensesDetails->unit_price = $unit_price[$key];
            $ExpensesDetails->quantity = $quantity[$key];
            $ExpensesDetails->sub_total = $sub_total[$key];
            $ExpensesDetails->save();
        }
        
        foreach($OrderDetails as $OD) {
            $OD->vendor_status = "Invoice Generated";
            $OD->vendor_expenses_id = $Expenses->ExpenseID;
            $OD->save();
        }
		flash(__('Invoice generated successfully.'))->success();
		return back();
    }

    /**
     * Display a listing of the resource to admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_orders(Request $request)
    {   $searchUserId = 0;
        
         if($request->input('key')){
            $searchUserId = decrypt($request->input('key'));
         }
      
        $roles = \App\Role::where('permissions','["21"]')->select('id')->get()->pluck('id')->all();
        $delivery_agent_ids = \App\Staff::whereIn('role_id',$roles)->get()->pluck('user_id')->all();
        $delivery_agent = \App\User::whereIn('id', $delivery_agent_ids)->select('id','name','phone')->get();
        $payment_status = null;
        $delivery_status = null;
        $sort_search = null;
        $admin_user_id = User::where('user_type', 'admin')->first()->id;
        $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->join('users','orders.user_id','=','users.id')
                    ->where('order_details.seller_id', $admin_user_id)
                    ->select('orders.id', 'orders.code','users.name','users.email','users.phone','orders.payment_status')
                    ->distinct();
                    
        if ($request->payment_type != null){
            $orders = $orders->orwhere('order_details.payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        if ($request->delivery_status != null) {
            $orders = $orders->orwhere('order_details.delivery_status', $request->delivery_status);
            $delivery_status = $request->delivery_status;
        }
        if ($request->has('search')){
           
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%')
                    ->orWhere('name','like','%'.$sort_search.'%')
                    ->orWhere('email','like','%'.$sort_search.'%')
                    ->orWhere('phone','like','%'.$sort_search.'%');
                    
        }

        if(!empty($searchUserId)){
            $orders = $orders->where('user_id', '=', $searchUserId);
        }

        $orders = $orders->paginate(15);
       //dd($orders);
        return view('orders.index', compact('orders','payment_status','delivery_status', 'sort_search', 'admin_user_id', 'delivery_agent'));
    }
    /**
     * Display a listing of the resource to admin.
     *
     * @return \Illuminate\Http\Response
     */
     //OTC ORDER
     public function admin_orders_status_otc(Request $request)
    {
        $roles = \App\Role::where('permissions','["21"]')->select('id')->get()->pluck('id')->all();
        $delivery_agent_ids = \App\Staff::whereIn('role_id',$roles)->get()->pluck('user_id')->all();
        $delivery_agent = \App\User::whereIn('id', $delivery_agent_ids)->select('id','name','phone')->get();
        $RouteName = \Route::currentRouteName();
        if($RouteName == "orders.index.pending.admin") {
            $delivery_status = 'pending';
        } elseif($RouteName == "orders.index.confirmation_pending.admin") {
            $delivery_status = 'confirmation_pending';
        } elseif($RouteName == "orders.index.hold.admin") {
            $delivery_status = 'hold';
        } elseif($RouteName == "orders.index.on_review.admin") {
            $delivery_status = 'on_review';
        } elseif($RouteName == "orders.index.ready_to_ship.admin") {
            $delivery_status = 'ready_to_ship';
        } elseif($RouteName == "orders.index.ready_to_delivery.admin") {
            $delivery_status = 'ready_to_delivery';
        } elseif($RouteName == "orders.index.shipped.admin") {
            $delivery_status = 'shipped';
        } elseif($RouteName == "orders.index.on_delivery.admin") {
            $delivery_status = 'on_delivery';
        } elseif($RouteName == "orders.index.on_ship.admin") {
            $delivery_status = 'shipped';
        } elseif($RouteName == "orders.index.delivered.admin") {
            $delivery_status = 'delivered';
        } elseif($RouteName == "orders.index.processing_refund.admin") {
            $delivery_status = 'processing_refund';
        } elseif($RouteName == "orders.index.refunded.admin") {
            $delivery_status = 'refunded';
        }  elseif($RouteName == "orders.index.cancel.admin") {
            $delivery_status = 'cancel';
        }elseif($RouteName == "orders.index.trash.admin") {
            $delivery_status = 'trash';
        }  else {
            $delivery_status = null;
        }
       // dd($delivery_status);
        $payment_status = null;
        $sort_search = null;
        $cust_search = null;
        $admin_user_id = User::where('user_type', 'admin')->first()->id;
       /* $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('order_details.seller_id', $admin_user_id)
                    ->select('orders.id', 'orders.code')
                    ->distinct();*/
        $sort_search = $request->search;
        $orders = Order::distinct('order_id')
                    ->join('order_details','order_details.order_id','=','orders.id')
                    ->join('users','orders.user_id','=','users.id')
                    ->where('order_details.seller_id', $admin_user_id)
                    
                    ->where('order_details.delivery_status',$delivery_status)
                   
                    //->orWhere('name','like','%'.$sort_search.'%')
                    //->orWhere('email','like','%'.$sort_search.'%')
                    //->orWhere('phone','like','%'.$sort_search.'%')
                    //->where('code', 'like', '%'.$sort_search.'%')
                    ->where(function ($query) use ($sort_search) {
                        $query->orWhere('name','like','%'.$sort_search.'%')
                               ->orWhere('code', 'like', '%'.$sort_search.'%')
                              ->orWhere('email','like','%'.$sort_search.'%')
                              ->orWhere('phone','like','%'.$sort_search.'%');
                    })
                    
                    
                    //->orWhere('name','like','%'.$sort_search.'%')
                    //->orWhere('email','like','%'.$sort_search.'%')
                    //->orWhere('phone','like','%'.$sort_search.'%')
                    
                    ->orderBy('code', 'desc')
                   
                    ->select('orders.id', 'orders.code', 'orders.user_id','order_details.delivery_status');            
  
        //dd($orders->get()->toArray());         
        if($delivery_status != null) {
            //echo $delivery_status;exit;
            $orders = $orders->where('order_details.delivery_status', $delivery_status);
        }
        if ($request->payment_type != null){
            $orders = $orders->where('order_details.payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        if ($request->has('cust_search')){
            if($request->cust_search != "") {
                $cust_search = $request->cust_search;
                $user = User::select('id')->where('name','like', '%'.$cust_search.'%')->get()->pluck('id')->all();
                $orders = $orders->whereIn('user_id', $user);
            }
        }
        
        if ($request->has('search')){
            $sort_search = $request->search;
           
        }
        
        $orders = $orders->paginate(15);
        return view('orders.otc', compact('orders','payment_status','delivery_status', 'sort_search', 'admin_user_id', 'delivery_agent','cust_search'));
    }
    public function admin_orders_status(Request $request)
    {   
        $roles = \App\Role::where('permissions','["21"]')->select('id')->get()->pluck('id')->all();
        $delivery_agent_ids = \App\Staff::whereIn('role_id',$roles)->get()->pluck('user_id')->all();
        $delivery_agent = \App\User::whereIn('id', $delivery_agent_ids)->select('id','name','phone')->get();
        $RouteName = \Route::currentRouteName();
        if($RouteName == "orders.index.pending.admin") {
            $delivery_status = 'pending';
        } elseif($RouteName == "orders.index.confirmation_pending.admin") {
            $delivery_status = 'confirmation_pending';
        } elseif($RouteName == "orders.index.hold.admin") {
            $delivery_status = 'hold';
        } elseif($RouteName == "orders.index.on_review.admin") {
            $delivery_status = 'on_review';
        } elseif($RouteName == "orders.index.ready_to_ship.admin") {
            $delivery_status = 'ready_to_ship';
        } elseif($RouteName == "orders.index.ready_to_delivery.admin") {
            $delivery_status = 'ready_to_delivery';
        } elseif($RouteName == "orders.index.shipped.admin") {
            $delivery_status = 'shipped';
        } elseif($RouteName == "orders.index.on_delivery.admin") {
            $delivery_status = 'on_delivery';
        } elseif($RouteName == "orders.index.on_ship.admin") {
            $delivery_status = 'shipped';
        } elseif($RouteName == "orders.index.delivered.admin") {
            $delivery_status = 'delivered';
        } elseif($RouteName == "orders.index.processing_refund.admin") {
            $delivery_status = 'processing_refund';
        } elseif($RouteName == "orders.index.refunded.admin") {
            $delivery_status = 'refunded';
        }  elseif($RouteName == "orders.index.cancel.admin") {
            $delivery_status = 'cancel';
        }elseif($RouteName == "orders.index.trash.admin") {
            $delivery_status = 'trash';
        }  else {
            $delivery_status = null;
        }
       // dd($delivery_status);
        $payment_status = null;
        $sort_search = null;
        $cust_search = null;
        $admin_user_id = User::where('user_type', 'admin')->first()->id;
       /* $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('order_details.seller_id', $admin_user_id)
                    ->select('orders.id', 'orders.code')
                    ->distinct();*/
        $sort_search = $request->search;
        $orders = Order::distinct('order_id')
                    ->join('order_details','order_details.order_id','=','orders.id')
                    ->join('users','orders.user_id','=','users.id')
                    ->where('order_details.seller_id', $admin_user_id)
                    
                    ->where('order_details.delivery_status',$delivery_status)
                   
                    //->orWhere('name','like','%'.$sort_search.'%')
                    //->orWhere('email','like','%'.$sort_search.'%')
                    //->orWhere('phone','like','%'.$sort_search.'%')
                    //->where('code', 'like', '%'.$sort_search.'%')
                    ->where(function ($query) use ($sort_search) {
                        $query->orWhere('name','like','%'.$sort_search.'%')
                               ->orWhere('code', 'like', '%'.$sort_search.'%')
                              ->orWhere('email','like','%'.$sort_search.'%')
                              ->orWhere('phone','like','%'.$sort_search.'%');
                    })
                    
                    
                    //->orWhere('name','like','%'.$sort_search.'%')
                    //->orWhere('email','like','%'.$sort_search.'%')
                    //->orWhere('phone','like','%'.$sort_search.'%')
                    
                    ->orderBy('code', 'desc')
                   
                    ->select('orders.id', 'orders.code', 'orders.user_id','order_details.delivery_status');            
  
        //dd($orders->get()->toArray());         
        if($delivery_status != null) {
            //echo $delivery_status;exit;
            $orders = $orders->where('order_details.delivery_status', $delivery_status);
        }
        if ($request->payment_type != null){
            $orders = $orders->where('order_details.payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        if ($request->has('cust_search')){
            if($request->cust_search != "") {
                $cust_search = $request->cust_search;
                $user = User::select('id')->where('name','like', '%'.$cust_search.'%')->get()->pluck('id')->all();
                $orders = $orders->whereIn('user_id', $user);
            }
        }
        
        if ($request->has('search')){
            $sort_search = $request->search;
           
        }
        
        $orders = $orders->paginate(15);
        return view('orders.index', compact('orders','payment_status','delivery_status', 'sort_search', 'admin_user_id', 'delivery_agent','cust_search'));
    }
    //Export order
    public function ExportExcel(Request $request) {

        return Excel::download(new OrderExportExcel($request), 'Orders_'. time() .'.xlsx');
   }
    
    public function admin_seller_orders_status(Request $request)
    {   
        $roles = \App\Role::where('permissions','["21"]')->select('id')->get()->pluck('id')->all();
        
        $delivery_agent_ids = \App\Staff::whereIn('role_id',$roles)->get()->pluck('user_id')->all();
       
        $delivery_agent = \App\User::whereIn('id', $delivery_agent_ids)->select('id','name','phone')->get();
        $RouteName = \Route::currentRouteName();
        if($RouteName == "orders.index.pending.admin.seller") {
            $delivery_status = 'pending';
        } elseif($RouteName == "orders.index.confirmation_pending.admin.seller") {
            $delivery_status = 'confirmation_pending';
        } elseif($RouteName == "orders.index.hold.admin.seller") {
            $delivery_status = 'hold';
        } elseif($RouteName == "orders.index.on_review.admin.seller") {
            $delivery_status = 'on_review';
        } elseif($RouteName == "orders.index.ready_to_ship.admin.seller") {
            $delivery_status = 'ready_to_ship';
        } elseif($RouteName == "orders.index.ready_to_delivery.admin.seller") {
            $delivery_status = 'ready_to_delivery';
        } elseif($RouteName == "orders.index.shipped.admin.seller") {
            $delivery_status = 'shipped';
        } elseif($RouteName == "orders.index.on_delivery.admin.seller") {
            $delivery_status = 'on_delivery';
        } elseif($RouteName == "orders.index.on_ship.admin.seller") {
            $delivery_status = 'shipped';
        } elseif($RouteName == "orders.index.delivered.admin.seller") {
            $delivery_status = 'delivered';
        } elseif($RouteName == "orders.index.processing_refund.admin.seller") {
            $delivery_status = 'processing_refund';
        } elseif($RouteName == "orders.index.refunded.admin.seller") {
            $delivery_status = 'refunded';
        }  elseif($RouteName == "orders.index.cancel.admin.seller") {
            $delivery_status = 'cancel';
        } else {
            $delivery_status = null;
        }
        $payment_status = null;
        $sort_search = null;
        $seller_users_id = User::where('user_type', 'seller')->select('id')->get()->pluck('id')->all();
        
       /* $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('order_details.seller_id', $admin_user_id)
                    ->select('orders.id', 'orders.code')
                    ->distinct();*/
                    
        $orders = Order::distinct('order_id')
                    ->join('order_details','order_details.order_id','orders.id')
                    ->join('users','orders.user_id','=','users.id')
                    ->whereIn('order_details.seller_id', $seller_users_id)
                    ->orderBy('code', 'desc')
                    ->select('orders.id', 'orders.code', 'order_details.seller_id');            
        if($delivery_status != null) {
            $orders = $orders->where('order_details.delivery_status', $delivery_status);
        }
        if ($request->payment_type != null){
            $orders = $orders->where('order_details.payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%')
            ->orWhere('name','like','%'.$sort_search.'%')
            ->orWhere('email','like','%'.$sort_search.'%')
            ->orWhere('phone','like','%'.$sort_search.'%');
        }
        
        $orders = $orders->paginate(15);
       
        return view('orders.seller.index', compact('orders','payment_status','delivery_status', 'sort_search', 'seller_users_id', 'delivery_agent'));
    }
    /**
     * Display a listing of the resource to admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_orders_vendor(Request $request)
    {
        $payment_status = null;
        $delivery_status = null;
        $sort_search = null;
        $vendor_user_id = User::where('user_type', 'vendor')->select('id')->get()->pluck('id')->all();
        $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->whereIn('order_details.seller_id', $vendor_user_id)
                    ->select('orders.id', 'orders.code')
                    ->distinct();
        if ($request->payment_type != null){
            $orders = $orders->where('order_details.payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        if ($request->delivery_status != null) {
            $orders = $orders->where('order_details.delivery_status', $request->delivery_status);
            $delivery_status = $request->delivery_status;
        }
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
        }
        $orders = $orders->paginate(15);
        $OrderData = array();
        foreach($orders as $O) {
            $Price = 0;
            $seller_id = 0;
            $delivery_status = 0;
            $Order = Order::find($O->id);
            $OrderDetail = OrderDetail::where('order_id',$O->id)->get();
            $itemCount = 0;
            foreach($OrderDetail as $OD) {
                if(in_array($OD->seller_id,$vendor_user_id)) {
                    $Price = $Price + $OD->price + $OD->tax + $OD->shipping_cost;
                    $seller_id = $OD->seller_id;
                    $delivery_status = $OD->delivery_status;
                    $payment_status = $OD->payment_status;
                    $itemCount++;
                }
            }
            $OrderData[$O->id]['price'] = $Price;
            $OrderData[$O->id]['seller_id'] = $seller_id;
            $OrderData[$O->id]['delivery_status'] = $delivery_status;
            $OrderData[$O->id]['payment_status'] = $payment_status;
            $OrderData[$O->id]['items'] = $itemCount;
            $OrderData[$O->id]['order'] = Order::find($O->id);
        }
        return view('orders.vendor', compact('orders','OrderData','payment_status','delivery_status', 'sort_search', 'vendor_user_id'));
    }

    /**
     * Display a listing of the sales to admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function sales(Request $request)
    {
        $startDate = $request->input('startDate', null);
        $endDate = $request->input('endDate', null);
        $payment_status = $request->input('payment_status', 'all');
        $delivery_status = $request->input('delivery_status', 'all');
        if($startDate !== null) {
            $startDate = Carbon::createFromFormat('d/m/Y', $startDate);
        } else {
            $startDate = Carbon::now()->subDays(30);
        }
        if($endDate !== null) {
            $endDate = Carbon::createFromFormat('d/m/Y', $endDate);
        } else {
            $endDate = Carbon::now();
        }
        
        $sort_search = null;
        $orders = Order::whereBetween('created_at',[$startDate->toDateTimeString(), $endDate->toDateTimeString()])->orderBy('code', 'desc');
        
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
        }
        $orders = $orders->paginate(15);
        $startDate = $startDate->format('d/m/Y');
        $endDate = $endDate->format('d/m/Y');
        return view('sales.index', compact('orders', 'sort_search', 'startDate', 'endDate', 'payment_status', 'delivery_status'));
    }


    public function order_index(Request $request)
    {
        if (Auth::user()->user_type == 'staff') {
            //$orders = Order::where('pickup_point_id', Auth::user()->staff->pick_up_point->id)->get();
            $orders = DB::table('orders')
                        ->orderBy('code', 'desc')
                        ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                        ->where('order_details.pickup_point_id', Auth::user()->staff->pick_up_point->id)
                        ->select('orders.id','orders.code')
                        ->distinct()
                        ->paginate(15);

            return view('pickup_point.orders.index', compact('orders'));
        }
        else{
            //$orders = Order::where('shipping_type', 'Pick-up Point')->get();
            $orders = DB::table('orders')
                        ->orderBy('code', 'desc')
                        ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                        ->where('order_details.shipping_type', 'pickup_point')
                        ->select('orders.id','orders.code')
                        ->distinct()
                        ->paginate(15);

            return view('pickup_point.orders.index', compact('orders'));
        }
    }

    public function pickup_point_order_sales_show($id)
    {
        if (Auth::user()->user_type == 'staff') {
            $order = Order::findOrFail(decrypt($id));
            return view('pickup_point.orders.show', compact('order'));
        }
        else{
            $order = Order::findOrFail(decrypt($id));
            return view('pickup_point.orders.show', compact('order'));
        }
    }

    /**
     * Display a single sale to admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function sales_show($id)
    {
        $order = Order::findOrFail(decrypt($id));
        return view('sales.show', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function OrderPrintLabel($order_id) {
        $User = Auth::User();
        if($User->user_type == "seller" || $User->user_type == "admin" || $User->user_type == "vendor") {
            $order = Order::findOrFail($order_id);
            $OrderDetail = OrderDetail::where('order_id', $order_id)->get();
            $Products = array();
            $COD = 0;
            if($User->user_type == "admin") {
                foreach($OrderDetail as $OD) {
                    $user_type = \App\User::find($OD->seller_id)->user_type;
                    if ($user_type == "admin" || $user_type == "staff") {
                        $Products[] = $OD;
                        $COD = $COD + $OD->price + $OD->tax + $OD->shipping_cost;
                    }
                }
            } else {
                foreach($OrderDetail as $OD) {
                    $user_type = \App\User::find($OD->seller_id)->user_type;
                    if ($user_type == $User->user_type) {
                        $Products[] = $OD;
                        $COD = $COD + $OD->price + $OD->tax + $OD->shipping_cost;
                    }
                }
            }
            return view('orders.print_label',compact('Products','order','COD', 'User'));
        } else {
            return abort(404);
        }
    }
    
    public function OrderPrintSellerLabel($seller_id, $order_id) {
        $User = User::find($seller_id);
        if($User->user_type == "seller" || $User->user_type == "admin" || $User->user_type == "vendor") {
            $order = Order::findOrFail($order_id);
            $OrderDetail = OrderDetail::where('order_id', $order_id)->where('seller_id', $seller_id)->get();
            $Products = array();
            $COD = 0;
            foreach($OrderDetail as $OD) {
                $Products[] = $OD;
                $COD = $COD + $OD->price + $OD->tax + $OD->shipping_cost;
            }
            return view('orders.print_label',compact('Products','order','COD', 'User'));
        } else {
            return abort(404);
        }
    }
    
    public function OrderShipping(Request $request) {
        $order_id = $request->order_id;
        $seller_id = $request->seller_id;
        $courier = $request->courier;
        $awb = $request->awb;
        $User = Auth::User();
        $itemCount = 0;
        if($User->user_type == "seller" || $User->user_type == "admin" || $User->user_type == "vendor") {
            $order = Order::findOrFail($order_id);
            $OrderDetail = OrderDetail::where('order_id', $order_id)->get();
            $Products = array();
            $PName = "";
            if($User->user_type == "admin" || $User->user_type == "staff") {
                foreach($OrderDetail as $OD) {
                    $user_type = \App\User::find($OD->seller_id)->user_type;
                    if ($user_type == "admin" || $user_type == "staff") {
                        if($PName == "") {
                           $PName = $OD->product->name;
                        }
                        $other_info = json_decode($OD->other_info,true);
                        $other_info['awb'] = $awb;
                        $other_info['courier'] = $courier;
                        $OD->other_info = json_encode($other_info);
                        $OD->delivery_status = 'shipped';
                        $OD->save();
                        $itemCount++;
                    } elseif($user_type == "seller") {
                        if($OD->seller_id == $seller_id) {
                            if($PName == "") {
                               $PName = $OD->product->name;
                            }
                        $other_info = json_decode($OD->other_info,true);
                        $other_info['awb'] = $awb;
                        $other_info['courier'] = $courier;
                        $OD->other_info = json_encode($other_info);
                        $OD->delivery_status = 'shipped';
                        $OD->save();
                        $itemCount++;
                        }
                    }
                }
            } else {
                foreach($OrderDetail as $OD) {
                    $user_type = \App\User::find($OD->seller_id)->user_type;
                    if ($user_type == $User->user_type) {
                        if($PName == "") {
                           $PName = $OD->product->name;
                        }
                        $other_info = json_decode($OD->other_info,true);
                        $other_info['awb'] = $awb;
                        $other_info['courier'] = $courier;
                        $OD->other_info = json_encode($other_info);
                        $OD->delivery_status = 'shipped';
                        $OD->save();
                        $itemCount++;
                    }
                }
            }
            if($itemCount > 1) {
            $itemCount  = $itemCount - 1;
            $PName = \Illuminate\Support\Str::limit($PName, 20, '...')." and ". $itemCount ." more items";
            } else {
            $PName = \Illuminate\Support\Str::limit($PName, 40, '...');
            }
            $data = array('ProductName' => $PName, 'AWB' => $awb, 'courier' => $courier);
            $shipping = json_decode($order->shipping_address);
            $smsMsg = view('sms.order_shipped', $data)->render();
            $message = $smsMsg;
            $email_subject = "AldeBazaar : Order Shipped";
            $smsMsg = rawurlencode($smsMsg);
            $to = "91".$shipping->phone;
            if(strlen($to) == 12) {
             $SMS_URL = env('SMS_URL', null);
                if($SMS_URL != null) {
                    $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                    sendSMS($SMS_URL);
                }
            }
            
            if($message != null) {
                $emailData = array('email' => $order->user->email, 'name' => $order->user->name, 'message' => $message, 'email_subject' => $email_subject);
                if(strlen($emailData['email']) > 4) {
                    Mail::send('emails.order_notification',array('data' => $emailData), function ($m) use ($emailData) {
                            $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                    });
                }
           }
            flash(__('Shipping info added successfully.'))->success();
            return back();
        } else {
            return abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Products = array();
        $ProductQuantity = array();
        $ProductFree = array();
        $OrderUser = null;
        $aldeproDiscountper = 0;
        $order = new Order;
        if(Auth::check()){
            $order->user_id = Auth::user()->id;
            $OrderUser = $order->user_id;
        } else {
            $order->guest_id = mt_rand(100000, 999999);
        }

        $order->shipping_address = json_encode($request->session()->get('shipping_info'));
        $shipping_address = json_decode($order->shipping_address);

        // if (Session::get('delivery_info')['shipping_type'] == 'Home Delivery') {
        //     $order->shipping_type = Session::get('delivery_info')['shipping_type'];
        // }
        // elseif (Session::get('delivery_info')['shipping_type'] == 'Pick-up Point') {
        //     $order->shipping_type = Session::get('delivery_info')['shipping_type'];
        //     $order->pickup_point_id = Session::get('delivery_info')['pickup_point_id'];
        // }

        $order->payment_type = $request->payment_option;
        $order->delivery_viewed = '0';
        $order->payment_status_viewed = '0';
        $order->code = date('Ymd-His').rand(10,99);
        $order->date = strtotime('now');
        if($order->save()){
            if($request->hasFile('prescriptionFile')) {
                $extension = $request->prescriptionFile->extension();
                $c = $request->file('prescriptionFile')->storeAs('prescription',"prescription-".$order->code.".".$extension);
                $order->prescription_file = "prescription-".$order->code.".".$extension;
            } else {
                $order->prescription_file = null;
            }
            $order->save();
            $subtotal = 0;
            $tax = 0;
            $shipping = 0;
            $getPercent = 0;
            $serviceProviderTotal = 0;
            $sellerCommission = 0;
            $sellerId = 0;
            $totalSellerComission =0;
            foreach (Session::get('cart') as $key => $cartItem){
                $product = Product::find($cartItem['id']);
                $categoryId = $product->category_id;
                $subcategoryId = $product->subcategory_id;
                $subsubcategory = $product->subsubcategory_id;
             
            	if(isset($product->user_id)){
            	    $userType =User::where('id',$product->user_id)->first();
            	    if($userType->user_type =='seller'){
            	         $sellerId = $product->user_id;
            	    }
            	   
            	}else{
            	    $sellerId = 0;
            	}
            
                if(!empty($sellerId)){
                        if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                        $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                            $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($sellerId)){
                         $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
                    }
                    
                    /***************PRODUCT DISCOUNT PRECOENT GET*****************/
                    $CategoryID = $product->category_id;
            		$brand_id = $product->brand_id;
            		$product_discount=$product->discount;
            		
                    $category_deals = \App\Category::where('id', $CategoryID)->first();
                    $brand_deals = \App\Brand::where('id', $brand_id)->first();
                    
                    
         
                    $category_discount=$category_deals->discount;
    		        $brand_discount=$brand_deals->discount;
    		        $product_discount=$product->discount;
    		       
    		        /*if($product_discount==0 || $product_discount!='ND'){
    		            $aldeproDiscountper = $product_discount;
    		        }else if($category_discount==0 || $category_discount!='ND'){
    		             $aldeproDiscountper = $category_discount ;
    		        }else if($brand_discount==0 || $brand_discount!='ND'){
    		            $aldeproDiscountper = $brand_discount;
    		        }*/
    		        /*if($product->discount !='ND'){
			   
            			 if($product_discount!=0){
            			   $aldeproDiscountper = $product_discount;
            			}else{
            			    $aldeproDiscountper =0;
            			}
        			}
        			else if($category_discount !='ND'){
        			   
            			 if($category_discount!=0){
            			  $aldeproDiscountper = $category_discount;
            			}else{
            			  $aldeproDiscountper = 0;  
            			}
        			}
        			else if($brand_discount !='ND'){
        			   
            			 if($brand_discount!=0){
            			  $aldeproDiscountper = $brand_discount; 
            			}else{
            			  $aldeproDiscountper = 0;  
            			}
        			}*/
        		
        			if($product->discount !='ND'){
        			    echo 'pro';
        			    $aldeproDiscountper = $product->discount;
        			}else if($product->discount >0){
        			     $aldeproDiscountper= $product->discount;
        			}
        			else if($category_discount==0 && $category_discount!='ND'){
        			     echo 'cat';
        			    $aldeproDiscountper = $category_discount;
        			}else if($category_discount>0){
        			     echo 'cat';
        			   $aldeproDiscountper = $category_discount;
        			}else if($brand_discount==0 && $brand_discount!='ND'){
        			    echo 'brand';
        			   $aldeproDiscountper = $brand_discount;
        			}else if($brand_discount>0){
        			      echo 'brand';
        			   $aldeproDiscountper = $brand_discount;
        			}else{
        			    $aldeproDiscountper = 0;
        			}
    		       //dd($aldeproDiscountper);
    		        
                    /*****************END PRODUCT GET DISCOUNT********************/
                }
                


       
               
                if(isset($get_subcategory->commission) && $get_subcategory->commission_type==1){
                    $exculsivePercent = $get_subcategory->commission;
                    
                    
                }else{
                   
                    $exculsivePercent = 0; 
                }
                
                 if(isset($get_subcategory->commission) && $get_subcategory->commission_type==0){
                      $inclusive = $get_subcategory->commission;
                     
                    if(isset($inclusive) && $get_subcategory->commission_type==0){
                        $getPercent = ($product->unit_price*$inclusive)/100;
                    }
                 }
                    
                    
                
                if(!empty($exculsivePercent)){
                      $getPercent = ($product->unit_price*$exculsivePercent)/100;
                      $cartIteamPrice = $cartItem['price'];
                      
                      //get discount
                      $totalexcelusivComission = ($product->unit_price*$get_subcategory->commission)/100;
                      
                      $totalval = $product->unit_price+$totalexcelusivComission;
                   
                      $taotalDiscountPercent = $totalval*$aldeproDiscountper/100;
                      $totalSellerComission = $totalval-$product->unit_price-$taotalDiscountPercent;
                    // dd($totalSellerComission);
                      
                      
                      // $cartIteamPrice = $cartItem['price'];
                }else{
                   $cartIteamPrice = $cartItem['price'];
                }
                
                //inclusive Price 
                
                
                
                $subtotal += $cartIteamPrice*$cartItem['quantity'];
               
                //dd($product->unit_price,$cartItem['quantity']);
               // $serviceProviderTotal += $product->unit_price*$cartItem['quantity']-$productaldeDiscount;
                
                $productaldeDiscount = $product->unit_price*$cartItem['quantity']-$cartIteamPrice*$cartItem['quantity'];
                if($productaldeDiscount>0){
                    $productaldeDiscount = $productaldeDiscount;
                }else{
                    $productaldeDiscount = 0;
                }
               
                 $serviceProviderTotal += $product->unit_price*$cartItem['quantity']-$productaldeDiscount;
                
                $sellerCommission += $getPercent*$cartItem['quantity']-$productaldeDiscount;
                
                $tax += $cartItem['tax']*$cartItem['quantity'];
                if(isset($cartItem['shipping_type'])){
                    if ($cartItem['shipping_type'] == 'home_delivery') {
                      $shipping += \App\Product::find($cartItem['id'])->shipping_cost*$cartItem['quantity'];
                    }

                }
                
                $product_variation = $cartItem['variant'];
                $log = new Log;
                $log->module = "Order Create";
                $log->func = "Product Stock";
                $log->data1 = $product->product_id;
                $log->data2 = $product->current_stock;
                $log->data3 = $cartItem['quantity'];

                if($product_variation != null){
                    $product_stock = $product->stocks->where('variant', $product_variation)->first();
                    $product_stock->qty -= $cartItem['quantity'];
                    $product_stock->save();
                }
                else {
                    if($product->current_stock > 0) {
                        $product->current_stock -= $cartItem['quantity'];
                    }
                    $product->save();
                }
                
                $log->data4 = $product->current_stock;
                $log->data5 = $order->code;
                $log->save();

                $order_detail = new OrderDetail;
                $order_detail->order_id  =$order->id;
                $order_detail->seller_id = $product->user_id;
                $order_detail->product_id = $product->id;
                $order_detail->variation = $product_variation;
                $order_detail->price = $cartIteamPrice * $cartItem['quantity'];
                $order_detail->discount = $cartItem['discount'] * $cartItem['quantity'];
                $order_detail->tax = $cartItem['tax'] * $cartItem['quantity'];
                if(isset($get_subcategory->commission)){
                    if($get_subcategory->commission_type == 0){
                         $incluiveTotal = ($product->unit_price*$inclusive)/100;
                         $order_detail->inclusive_price = $incluiveTotal*$cartItem['quantity']; 
                    }
                    if($get_subcategory->commission_type == 1){
                         $exclusiveTotal =  ($product->unit_price*$exculsivePercent)/100;
                         
                         $order_detail->exclusive_price = $exclusiveTotal*$cartItem['quantity']; 
                    }
                 
                }
                
                
                if(isset($cartItem['shipping_type'])){
                    $order_detail->shipping_type = $cartItem['shipping_type'];
                }
                
                $prescriptioncategory = env('PRESCRIPTION_CATEGORY');
                $prescriptioncategory = explode(",",$prescriptioncategory);
                if(in_array($product->category_id, $prescriptioncategory)) {
                    $order_detail->delivery_status = "confirmation_pending";   
                }
                $order_detail->product_referral_code = $cartItem['product_referral_code'];
                if(isset($cartItem['shipping_type'])){
                     if ($cartItem['shipping_type'] == 'home_delivery') {
                    $order_detail->shipping_cost = \App\Product::find($cartItem['id'])->shipping_cost*$cartItem['quantity'];
                    } else {
                        $order_detail->shipping_cost = 0;
                        $order_detail->pickup_point_id = $cartItem['pickup_point'];
                    }
                }
               
                
                $order_detail->quantity = $cartItem['quantity'];
                $order_detail->save();
                
                array_push($Products,$product->marg_code);
                array_push($ProductQuantity,$cartItem['quantity']);
                array_push($ProductFree,0);

                $product->num_of_sale++;
                $product->save();
            }
            if(env('MARG')) {
                    //Send Order to Marg
                    $Products = implode(",",$Products);
                    $ProductQuantity = implode(",",$ProductQuantity);
                    $ProductFree = implode(",",$ProductFree);
                    if($OrderUser != null) {
                        $OrderUser = User::find($OrderUser)->marg_rid;
                    }
                    $total = $subtotal + $tax + $shipping;
                    if($OrderUser != null) {
                        $id = $order->id;
                        $payload = json_encode( array( "OrderID" => $id, "OrderNo" => $id, "CustomerID" => $OrderUser, "MargID" => env('MARG_ID'), "Type" => "S", "Sid" => env('SALES_USER_ID'), "ProductCode" => "[".$Products."]", "Quantity" => "[".$ProductQuantity."]", "Free" => "[".$ProductFree."]", "Lat" => "", "Lng" => "", "Address" => $shipping_address->address." ". $shipping_address->city, "GpsID" => "0", "UserType" => "1", "Points" => "0.00", "Discounts" => "0.00", "Transport" => "", "Delivery" => "COD", "Bankname" => "", "BankAdd1" => "", "BankAdd2" => "", "shipname" => "", "shipAdd1" => ".$shipping_address->address.", "shipAdd2" => ".$shipping_address->city.", "shipAdd3" => "", "paymentmode" => $request->payment_option, "paymentmodeAmount" => $total, "payment_remarks" => "", "order_remarks" => "", "CustMobile" => $shipping_address->phone, "CompanyCode" => env('COMPANY_CODE'), "OrderFrom" => env('COMPANY_CODE') ));
                        $url = "https://wservices.margcompusoft.com/api/eOnlineData/InsertOrderDetail";
                        $ch = curl_init($url);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                        $response = curl_exec($ch);
                        curl_close($ch);
                        $c = gzinflate(base64_decode($response));
                        $c = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $c), true );
                        if($c['Details']['Status'] == 'Sucess') {
                            $OrderNo = $c['Details']['OrderDetails'][0]['OrderNo'];
                            $order->marg_order_no = $OrderNo;
                            $order->save();
                        }
                    }
                }

            //$order->grand_total = $subtotal + $tax + $shipping;
            $order->grand_total = $subtotal + $tax + $shipping;
            //$order->service_provider = $serviceProviderTotal;
            if(!empty($sellerId)){
                
               if($get_subcategory->commission_type == 1){
                 $order->service_provider =   $product->unit_price;
               }else{
                 $order->service_provider = $serviceProviderTotal-$sellerCommission;     
               }
               if($totalSellerComission>0){
                   $order->seller_commission = $totalSellerComission; 
               }else{
                 $order->seller_commission = $sellerCommission;     
               }
                   
            }
           
            

            if(Session::has('coupon_discount')){
                $order->grand_total -= Session::get('coupon_discount');
                $order->coupon_discount = Session::get('coupon_discount');
                $coupon = Coupon::find(Session::get('coupon_id'));
                if ($coupon->type != 'user_code') {
                    $coupon_usage = new CouponUsage;
                    $coupon_usage->user_id = Auth::user()->id;
                    $coupon_usage->coupon_id = Session::get('coupon_id');
                    $coupon_usage->save();   
                }
                $order->coupon_code = $coupon->code;
            }
            $order->save();
            
            
            if($order->grand_total < env('FREE_CART_VALUE',0)) {
                    session(['cart_shipping' => env('BELOW_FREE_CART_CHARGE',0)]);
            } else {
                session(['cart_shipping' => 0 ]);
            }
            $order->grand_total += session('cart_shipping');

            /**********
             * Apply Delivery Shipping according to payment option
             * 
             */
            $deliveryCharge =0;
            $shipData = Session::get('shippingData');
           
            $calPrice = $subtotal + $tax + $shipping;
            if(isset($shipData['paymentOption'])){
                $shippingData = [
                        'paymentOption'=>'razorpay',
                        'totalAmount'=>$shipData['totalAmount'],
                        'payoption'=>'DBC'
                    ];
            }
            if(isset($shipData['paymentOption'])){
                if($shipData['paymentOption'] == 'COD'){

                    if($calPrice >= 1 && $calPrice <= 499){
                        
                        $shipType = true;
                        $deliveryCharge = 50;
                    }else if($calPrice  >= 500 && $calPrice <=  999){
                        $shipType = true;
                        $deliveryCharge = 30;
                    }else if($calPrice >=  1000){
                        $shipType = true;
                        $deliveryCharge= 0;
                    }
                }else if($shipData['paymentOption'] == 'razorpay'){
                 
                    if($calPrice >= 1 && $calPrice <= 499){
                        
                        $shipType = true;
                        $deliveryCharge = 30;
                    }
                }
               
             
        }else{  
            if(isset($calPrice)){
                if($calPrice < 1000){
                  $shipType = true;
                  $deliveryCharge = 30;
                }
            }
                   
        }
           
            $order->grand_total += $deliveryCharge;
            $order->cart_shipping = $deliveryCharge;
            $cashBackConfig = UserCashbackConfig::all();
            $subtotalCashBack = Session::get('cart_cashback_Product_subtotal');
            if(Session::has('wallet_credit')) {
                if($subtotalCashBack > $cashBackConfig[0]->cashback_credit_amount){
                    $user = Auth::user();
                $walletCreditAmount  = ($cashBackConfig[0]->casback_use_percentage / 100) * $subtotalCashBack;
                
                $crediteachOrder = $walletCreditAmount;
               // $cashWallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','cashback')->sum('amount');
                //$paywallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','Wallet Payment')->sum('amount');
                //$totalWallet  =  $cashWallet-$paywallet;
                $cashWallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','cashback')->sum('amount');
                $paywallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','Wallet Payment')->sum('amount');
                $totalWallet  =  $cashWallet-$paywallet;
                if($walletCreditAmount > $totalWallet){
                    $wallet_credit = $totalWallet;
                }else{
                    $wallet_credit = $walletCreditAmount;
                }
               
                
                
                $order->grand_total -= $wallet_credit;
                $order->wallet_credit = $wallet_credit;
                $wallet = new Wallet;
                $wallet->user_id = $user->id;
                $wallet->amount = $wallet_credit;
                $wallet->payment_status = 'Debit';
                $wallet->payment_method = 'Wallet Payment';
                $wallet->payment_details = 'Credit used for order #'. $order->code;
                $wallet->add_status = 1;
                $wallet->order_id =$order->id;
                $wallet->save();
                $user->balance -= $wallet_credit;
                $user->save();
                $smsMsg = "Hi, The amount of Rs. ".$wallet_credit." is debited from Alde wallet for use in your order #".$order->code.". You can manage your wallet from your My Account section.";
                $message = $smsMsg;
                $smsMsg = rawurlencode($smsMsg);
                $email_subject = "AldeBazaar : Wallet Debit";
                $to = "91".$user->phone;
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        sendSMS($SMS_URL);
                    }
                }
                $emailData = array('email' => $user->email, 'name' => $user->name, 'message' => $message,  'title' => 'Wallet Update', 'email_subject' => $email_subject);
                if(strlen($emailData['email']) > 4) {
                    // Mail::send('emails.notification',array('data' => $emailData), function ($m) use ($emailData) {
                    //         $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                    // });
                }
            }
                    
                }
                
            $order->save();
            
            /*****Get Category Config Cashack Amount Subtotal***/
         $cartIteamList = Session::get('cart');
         $cartProductId = [];
         $cartQuantity = [];
         foreach($cartIteamList as $cartProduct){
             array_push($cartProductId,$cartProduct['id']);
             array_push($cartQuantity,$cartProduct['quantity']);
         }
        
       
         $cashbackProduct = Product::whereIn('id', $cartProductId)->get();
         $cashBackConfig = UserCashbackConfig::all();
         $cashbackCategory = $cashBackConfig[0]->category_id;
         $subtotalCashBack = 0;
         
         foreach($cashbackProduct as $cashBackCatProduct){
                 
                if ($cashBackCatProduct->category_id == $cashbackCategory){ 
                       
                        $cashBackMatchProduct = $cashbackProduct = Product::where('id', $cashBackCatProduct->id)->get();
                            foreach($cartIteamList as $productPriceSum){
                                
                                 if(in_array($cashBackMatchProduct[0]->id,$productPriceSum)){
                                      $subtotalCashBack +=  $productPriceSum['quantity']*$productPriceSum['price'];
                                 }
                            }
                }
             
         }
            
           
           
            if($subtotalCashBack   >=  $cashBackConfig[0]->casback_minimum_amount_limit){ //Add user cashback if shooping amount more than 500
               
                $cashBack = new cashback;
                $user = Auth::user();
                $cashBack->order_id = $order->id;
                $walletCreditAmount  = ($cashBackConfig[0]->casback_amount / 100) * $subtotalCashBack;//$calPrice
                $cashBack->cashback_amount = $walletCreditAmount;
                $cashBack->user_id = $user->id;
                $cashBack->status = 'pending';
                $cashBack->save();
                
            }
            
            if($order->payment_type == 'cash_on_delivery') {
                 $pdf = PDF::setOptions([
                            'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                            'logOutputFile' => storage_path('logs/log.htm'),
                            'tempDir' => storage_path('logs/')
                        ])->loadView('invoices.customer_invoice', compact('order'));
                $output = $pdf->output();
        		file_put_contents('invoices/'.'Order#'.$order->code.'.pdf', $output);
    
                $array['view'] = 'emails.invoice';
                $array['order'] = $order;
                $array['subject'] = 'Order Placed - '.$order->code;
                $array['from'] = env('MAIL_FROM_ADDRESS');
                $array['content'] = 'Hi. A new order has been placed. Please check the attached order confirmation pdf.';
                $array['file'] = 'invoices/Order#'.$order->code.'.pdf';
                $array['file_name'] = 'Order#'.$order->code.'.pdf';
                $admin_products = array();
                $seller_products = array();
                $vendor_products = array();
                foreach ($order->orderDetails as $key => $orderDetail){
                    if($orderDetail->product->added_by == 'admin'){
                        array_push($admin_products, $orderDetail->product->id);
                    }
                    elseif($orderDetail->product->added_by == 'vendor') {
                        $product_ids = array();
                        if(array_key_exists($orderDetail->product->user_id, $vendor_products)){
                            $product_ids = $vendor_products[$orderDetail->product->user_id]; // checking old data for seller.
                        }
                        array_push($product_ids, $orderDetail->product->id);
                        $vendor_products[$orderDetail->product->user_id] = $product_ids;
                    }
                    else{
                        $product_ids = array();
                        if(array_key_exists($orderDetail->product->user_id, $seller_products)){
                            $product_ids = $seller_products[$orderDetail->product->user_id]; // checking old data for seller.
                        }
                        array_push($product_ids, $orderDetail->product->id);
                        $seller_products[$orderDetail->product->user_id] = $product_ids;
                    }
                }
    
                foreach($seller_products as $key => $seller_product){
                    try {
                        $smsMsg = "You received a order. Kindly check dashboard for Manage Order";
                        $smsMsg = rawurlencode($smsMsg);
                        $to = "91".\App\User::find($key)->phone;
                        if(strlen($to) == 12) {
                            $SMS_URL = env('SMS_URL', null);
                            if($SMS_URL != null) {
                                $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                                sendSMS($SMS_URL);
                            }
                        }
                        if(\App\User::find($key)->email != "") {
                            Mail::to(\App\User::find($key)->email)->queue(new InvoiceEmailManager($array));
                        }
                    } catch (\Exception $e) {
    
                    }
                }
    
                foreach($vendor_products as $key => $vendor_product){
                    try {
                        if(\App\User::find($key)->email != "") {
                            Mail::to(\App\User::find($key)->email)->queue(new InvoiceEmailManager($array));
                        }
                        $smsMsg = "You received a order. Kindly check dashboard for Manage Order";
                        $smsMsg = rawurlencode($smsMsg);
                        $to = "91".\App\User::find($key)->phone;
                        if(strlen($to) == 12) {
                            $SMS_URL = env('SMS_URL', null);
                            if($SMS_URL != null) {
                                $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                                sendSMS($SMS_URL);
                            }
                        }
                    } catch (\Exception $e) {
    
                    }
                }
                
                    $smsMsg = view('sms.order_confirmation', array('order' => $order))->render();;
                    $smsMsg = rawurlencode($smsMsg);
                    $to = "91".$request->session()->get('shipping_info')['phone'];
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        sendSMS($SMS_URL);
                    }
                //send email to Admin
                try {
                        if(User::where('user_type', 'admin')->first()->email != "") {
                            Mail::to(User::where('user_type', 'admin')->first()->email)->queue(new InvoiceEmailManager($array));
                        }
                } catch (\Exception $e) {
                    
                }
                
                //send email to User
                $array['view'] = 'emails.order_confirmation_client';
                if(!empty($shipping_address->email)){
                    Mail::to($shipping_address->email)->queue(new InvoiceEmailManager($array));    
                }
               
    
    
                //sends email to customer with the invoice pdf attached
                if(env('MAIL_USERNAME') != null){
                    
                }
                unlink($array['file']);
                
                
            }
            Cookie::queue('cart', json_encode(array()));
            $request->session()->put('order_id', $order->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {    
        $order = Order::findOrFail(decrypt($id));
        $order->viewed = 1;
        $order->save();
        $catLIst = Category::where('display',1)->get();
        
        return view('orders.show', compact('order','catLIst'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function seller_show($seller_id, $id)
    {
        $order = Order::findOrFail(decrypt($id));
        $order->viewed = 1;
        $order->save();
        return view('orders.show', compact('order', 'seller_id'));
    }
    
    public function sellerorderList(){
        
        $seller_id = Auth::user()->id;
        $orders = Order::distinct('order_id')
                    ->join('order_details','order_details.order_id','orders.id')
                    ->join('users','orders.user_id','=','users.id')
                    ->where('order_details.seller_id', $seller_id)
                    ->orderBy('code', 'desc')
                    ->select('orders.id', 'orders.code', 'order_details.seller_id'); 
        
        $orders = $orders->paginate(10);
        
        return view("frontend.seller.order-list",compact('orders'));
    }
    
    public function sellerorderDetails($orderId){
        dd(decrypt($orderId));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function vendor_show($seller_id, $id)
    {
        $order = Order::findOrFail(decrypt($id));
        $order->viewed = 1;
        $order->save();
        return view('orders.show', compact('order', 'seller_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        
        $order_details = $request->order_detail;
        $quantity = $request->quantity;
        $unit_price = $request->unit_price;
        $discount = $request->discount;
        foreach($order_details as $od) {
            $order_detail = OrderDetail::withTrashed()->find($od);
            if($quantity[$od] == 0) {
                $order_detail->forceDelete();
            } else {
                $old_quantity = $order_detail->quantity;
                $old_tax = $order_detail->tax;
                if($old_tax > 0) {
                    $unit_tax = $old_tax/$old_quantity;
                } else {
                    $unit_tax = 0;
                }
                $order_detail->price = ($unit_price[$od]-$discount[$od]) * $quantity[$od];
                $order_detail->discount = $discount[$od] * $quantity[$od];
                $order_detail->tax = $unit_tax * $quantity[$od];
                $order_detail->quantity = $quantity[$od];
                $order_detail->save();
            }
        }
        //Re-Calculate Discount;
        $coupon_discount = 0;
        if($order->coupon_code != "") {
            $coupon = Coupon::where('code', $order->coupon_code)->first();
            $coupon_details = json_decode($coupon->details);
            if ($coupon->type == 'cart_base') {
                $sum = $order->orderDetails->sum('price') + $order->orderDetails->sum('tax') + $order->orderDetails->sum('shipping_cost');
                if ($sum > $coupon_details->min_buy) {
                    if ($coupon->discount_type == 'percent') {
                        $coupon_discount =  ($sum * $coupon->discount)/100;
                        if ($coupon_discount > $coupon_details->max_discount) {
                            $coupon_discount = $coupon_details->max_discount;
                        }
                    }
                    elseif ($coupon->discount_type == 'amount') {
                        $coupon_discount = $coupon->discount;
                    }
                }
            } elseif ($coupon->type == 'product_base') {
                foreach ($order->orderDetails as $key => $cartItem) {
                    foreach ($coupon_details as $key => $coupon_detail) {
                        if($coupon_detail->product_id == $cartItem->product_id){
                            if ($coupon->discount_type == 'percent') {
                                $coupon_discount += $cartItem->price * $coupon->discount/100;
                            }
                            elseif ($coupon->discount_type == 'amount') {
                                $coupon_discount += $coupon->discount;
                            }
                        }
                    }
                }
            } elseif ($coupon->type == 'instant') {
                //Instant Category Based Instant Discount
                foreach ($order->orderDetails as $key => $cartItem) {
                    $cat_id=Product::where('id',$cartItem->product_id)->pluck('category_id')->first();
                    foreach ($coupon_details as $key => $coupon_detail) {
                        if($coupon_detail->category_id == $cat_id){
                            if ($coupon->discount_type == 'percent') {
                                $coupon_discount += $cartItem->price * $coupon->discount/100;
                            }
                            elseif ($coupon->discount_type == 'amount') {
                                $coupon_discount += $coupon->discount;
                            }
                        }
                    }
                }
            }
        } else {
            $coupon_discount = 0;
        }
        
        $order->grand_total = $order->orderDetails->sum('price') + $order->orderDetails->sum('tax') + $order->orderDetails->sum('shipping_cost');

        if($coupon_discount > 0){
            $order->grand_total -= $coupon_discount;
            $order->coupon_discount = $coupon_discount;
        }
        if($order->grand_total < env('FREE_CART_VALUE',0)) {
                $cart_shipping = env('BELOW_FREE_CART_CHARGE',0);
        } else {
           $cart_shipping = 0;
        }
        $order->grand_total += $cart_shipping;
        $order->cart_shipping = $cart_shipping;
        if($order->wallet_credit > 0) {
            $order->grand_total -= $order->wallet_credit;
        }
        $order->save();
        flash('Order has been updated successfully')->success();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        if($order != null){
            foreach($order->orderDetails as $key => $orderDetail){
                $orderDetail->delete();
            }
            $order->delete();
            flash('Order has been deleted successfully')->success();
        }
        else{
            flash('Something went wrong')->error();
        }
        return back();
    }

    public function order_details(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        
        //$order->viewed = 1;
        //$order->save();
        
        return view('frontend.partials.order_details_seller', compact('order'));
    }

    
    public function update_status(Request $request)
    {    
        $seller_id = $request->input('seller_id',null);
        $seller_ids = $request->input('seller_ids',array());
        $setFlash = $request->input('setFlash',null);
        $status = $request->input('status',null);
        $agent_id = $request->input('agent_id',null);
        $undelivery_notes = $request->input('undelivery_notes',null);
        if($agent_id != null) {
            $agent = \App\User::find($agent_id);
        }
        $order_ids = $request->order_ids;
        $action = $request->action;
        if($seller_id == null) {
            $seller_id = Auth::user()->id;
        }
        $n = 0;
        $Price = 0;
        foreach($order_ids as $key => $order_id) {
            $order = Order::findOrFail($order_id);
            $item = 0;
            $sellerCount = count($seller_ids);
            if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'seller' || Auth::user()->user_type == 'vendor') {
                if($sellerCount > 0) {
                    $seller_id = $seller_ids[$key];
                }
                foreach($order->orderDetails->where('seller_id', $seller_id) as $key => $orderDetail) {
                    if($action == "delivery") {
                        if($status == 'cancel_refund') {
                            $orderDetail->delivery_status = 'processing_refund';
                        } else {
                            $orderDetail->delivery_status = $status;
                        }
                    } elseif($action == "delivered") {
                        $orderDetail->delivery_status = 'delivered';
                        $status  = 'delivered';
                    } elseif($action == "payment") {
                        $orderDetail->payment_status = $status;
                    } elseif($action == "assign") {
                        $orderDetail->delivery_status = "on_delivery";
                        $other_info = json_decode($orderDetail->other_info,true);
                        $other_info['agent_id'] = $agent->id;
                        $other_info['deliveryBy'] = $agent->name;
                        $other_info['phone'] = $agent->phone;
                        $other_info['awb'] = "";
                        $other_info['courier'] = "";
                        $orderDetail->other_info = json_encode($other_info);
                    } elseif($action == "undelivery") {
                        $orderDetail->delivery_status = "ready_to_delivery";
                        $other_info = json_decode($orderDetail->other_info,true);
                        $agent_id = $other_info['agent_id'];
                        $other_info['agent_id'] = "";
                        $other_info['deliveryBy'] = "";
                        $other_info['phone'] = "";
                        $other_info['awb'] = "";
                        $other_info['courier'] = "";
                    }
                    $orderDetail->save();
                    $item++;
                }
            } else {
                foreach($order->orderDetails->where('seller_id', \App\User::where('user_type', 'admin')->first()->id) as $key => $orderDetail) {
                    if($action == "delivery") {
                        $orderDetail->delivery_status = $status;
                    } elseif($action == "delivered") {
                        $orderDetail->delivery_status = 'delivered';
                        $status  = 'delivered';
                    }  elseif($action == "payment") {
                        $orderDetail->payment_status = $status;
                    } elseif($action == "assign") {
                        $orderDetail->delivery_status = "on_delivery";
                        $other_info = json_decode($orderDetail->other_info,true);
                        $other_info['agent_id'] = $agent->id;
                        $other_info['deliveryBy'] = $agent->name;
                        $other_info['phone'] = $agent->phone;
                        $other_info['awb'] = "";
                        $other_info['courier'] = "";
                        $orderDetail->other_info = json_encode($other_info);
                    } elseif($action == "undelivery") {
                        $orderDetail->delivery_status = "ready_to_delivery";
                        $other_info = json_decode($orderDetail->other_info,true);
                        $agent_id = $other_info['agent_id'];
                        $other_info['agent_id'] = "";
                        $other_info['deliveryBy'] = "";
                        $other_info['phone'] = "";
                        $other_info['awb'] = "";
                        $other_info['courier'] = "";
                    }
                    $orderDetail->save();
                    $item++;
                }
            }
            
            if($action == "delivered") {
                $TotalItemCount = $order->orderDetails->count();
                $delivery_status_item = $order->orderDetails->where('delivery_status', 'delivered')->count();
                if($delivery_status_item == $TotalItemCount) {
                    $order->Delivery_Confirmed = Carbon::now();
                    $order->save();
                }
            }
            
            $shipping = json_decode($order->shipping_address);
            $smsMsg = null;
            
            if ($status == "delivered") {
                $DeliveryLog = DeliveryLog::where('order_id', $order_id)->where('status', 'pending')->first();
                if($DeliveryLog == null) {
                    $DeliveryLog = new DeliveryLog;
                    $DeliveryLog->order_id = $order_id;
                    $DeliveryLog->agent_id = 0;
                }
                $DeliveryLog->status = 'delivered';
                $DeliveryLog->log = $item." item(s) has been delivered";
                $DeliveryLog->save();
                $smsMsg = "Order Delivered: Hey ". $order->user->name ." ! It's time to celebrate. Your ".$order->code." has been delievered successfully. Thank you for shopping at Alde Bazaar.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Order Delivered";
            } elseif ($status == "refunded") {
                $amount = $order->grand_total - $order->cart_shipping;
                $smsMsg = "Rs ".$amount." has been successfully refunded in your source account. We regret any inconvenience caused to you. For any assistance contact: +91 9776-9776-12.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Amount Refunded";
                $wallet=new Wallet;
                $wallet->user_id=$order->user_id;
                $wallet->amount=$amount;
                $wallet->payment_method='Refund';
                $wallet->payment_details="Order #".$order->code." Cancallation";
                $wallet->add_status=1;
                $wallet->save();
                $User = User::find($order->user_id);
                $User->balance=$User->balance+$amount;
                $User->save();
            } elseif ($status == 'cancel_refund') {
                $smsMsg = "Hey, ". $order->user->name ." ! Your Order No. ".$order->code.", has been cancelled. We regret inconvenience caused to you. In case it's a prepaid order, we have initiated a full refund into your wallet account.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Order Cancelled";
            } elseif ($status == 'cancel') {
                $this->DeleteCashBack($order->id,$status);
                $smsMsg = "Your Order No. ".$order->code.", has been cancelled. We regret inconvenience caused to you. For any assistance contact +91 9776-9776-12.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Order Cancelled";
            } elseif ($status == 'ready_to_ship') {
                $Pcount = $order->orderDetails->where('seller_id', $seller_id)->where('delivery_status','ready_to_ship')->count();
                if($Pcount > 1) {
                    $Pcount  = $Pcount - 1;
                    $PName = \Illuminate\Support\Str::limit($order->orderDetails->first()->product->name, 20, '...')." and ". $Pcount ." more items";
                } else {
                    $PName = \Illuminate\Support\Str::limit($order->orderDetails->first()->product->name, 40, '...');
                }
                $smsMsg = "ALDE BAZAAR: Hi ". $order->user->name ." ! we have received your order for ".$PName." (Order No. ".$order->code."). We will notify you once it's shipped.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Order Confirmed";
            }
            if($action == "assign") {
                $DeliveryLog = new DeliveryLog;
                $DeliveryLog->order_id = $order_id;
                $DeliveryLog->agent_id = $agent_id;
                $DeliveryLog->status = 'pending';
                $DeliveryLog->log = $item." item(s) assigned for delivery";
                $DeliveryLog->save();
                $data = array('order' => $order, 'items' => $item, 'agent' => $agent);
                $smsMsg = view('sms.order_assign_out_for_delivery', $data)->render();
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Out for Delivery";
            }
            if($action == "undelivery") {
                $DeliveryLog = DeliveryLog::where('order_id', $order_id)->where('status', 'pending')->first();
                if($DeliveryLog == null) {
                    $DeliveryLog = new DeliveryLog;
                    $DeliveryLog->order_id = $order_id;
                    $DeliveryLog->agent_id = 0;
                }
                if($DeliveryLog != null) {
                    $DeliveryLog->status = 'not_delivered';
                    $DeliveryLog->log = $item." item(s) not delivered due to :: ".$undelivery_notes[$n];
                    $DeliveryLog->save();   
                }
                $smsMsg = "Your order no. ".$order->code." with ".$item." item(s) not delivered by today. We will attempt next delivery in next working days.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Missed Delivery";
            }
            
            if($smsMsg != null) {
                $smsMsg = rawurlencode($smsMsg);
                $to = "91".$shipping->phone;
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        sendSMS($SMS_URL);
                    }
                }
            }
           if(isset($message)) {
            if($message != null) {
                $emailData = array('email' => $order->user->email, 'name' => $order->user->name, 'message' => $message, 'email_subject' => $email_subject);
                if(strlen($emailData['email']) > 4) {
                Mail::send('emails.order_notification',array('data' => $emailData), function ($m) use ($emailData) {
                        $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                });
                }
           }
           }
            $n++;
        }
        
        if($setFlash != null) {
            flash($setFlash)->success();
        }
        return 1;
    }
    
    public function get_deducted_cancel_amount(Request $request){
          $order = Order::findOrFail($request->order_id);
          $finalPenalty = '';
          $minimumPenaltiyAmount = 50;
          $totalOrderAmount =  $order->grand_total;
          $perectPenalty = $totalOrderAmount*5/100;
         
          if($perectPenalty >  50){
              $finalPenalty = $perectPenalty;
          }else{
              $finalPenalty = $minimumPenaltiyAmount;
          }
          return $finalPenalty;
    }

    public function update_delivery_status(Request $request)
    {   
        $seller_id = $request->input('seller_id',null);
        $setFlash = $request->input('setFlash',null);
        $order = Order::findOrFail($request->order_id);
        $order->delivery_viewed = '0';
        $order->save();
        if($seller_id == null) {
            $seller_id = Auth::user()->id;
        }
        $item = 0;
        if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'seller' || Auth::user()->user_type == 'vendor'){
            foreach($order->orderDetails->where('seller_id', $seller_id) as $key => $orderDetail){
                if($request->status == 'cancel_refund') {
                    $orderDetail->delivery_status = 'processing_refund';
                } else {
                   
                    $orderDetail->delivery_status = $request->status;
                     
                    $this->DeleteCashBack($orderDetail->order_id,$request->status);
                }
                $orderDetail->save();
                $item++;
            }
        }
        else{
            foreach($order->orderDetails->where('seller_id', \App\User::where('user_type', 'admin')->first()->id) as $key => $orderDetail){
                if($request->status == 'cancel_refund') {
                    $orderDetail->delivery_status = 'processing_refund';
                } else {
                    
                    $orderDetail->delivery_status = $request->status;
                    $this->DeleteCashBack($orderDetail->order_id,$request->status);
                }
                $orderDetail->save();
                $item++;
            }
        }
        $shipping = json_decode($order->shipping_address);
      
        if ($request->status == 'cancel_refund') {
               
                $smsMsg = "Hey, ". $order->user->name ." ! Your Order No. ".$order->code.", and Order Amount Rs. ".$order->grand_total."has been cancelled. We regret inconvenience caused to you. In case it's a prepaid order, Refund will be initiated in your Alde Bazaar Wallet. For any assistance WhatsApp: 80068-80055 or call .";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Order Cancelled";
                
            } elseif($request->status == 'cancel') {
           
            $orderDetails =  OrderDetail::where('order_id',$request->order_id)->get();
        
            $sellerId = $orderDetails[0]->seller_id;
     
        
        if(isset($sellerId) && Auth::user()->user_type == 'seller' ){
             $orderCreditLimit = User::findOrFail($sellerId);
            if($orderCreditLimit->user_type  == 'seller'){
                $sellerCreditLimit = Seller::where('user_id',$sellerId)->get();
                $sellerCreditLimit[0]->order_cancel_credit;
                $credit = $sellerCreditLimit[0]->order_cancel_credit;
            if($credit >= 1 ){
                $lastCredit = $credit -1;
                $affected = DB::table('sellers')
                  ->where('user_id', $sellerId)
                  ->update(['order_cancel_credit' => $lastCredit]);
            }else{
                
                  $finalPenalty = '';
                  $minimumPenaltiyAmount = 50;
                  $totalOrderAmount =  $order->grand_total;
                  $perectPenalty = $totalOrderAmount*5/100;
                 
                  if($perectPenalty >  50){
                      $finalPenalty = $perectPenalty;
                  }else{
                      $finalPenalty = $minimumPenaltiyAmount;
                  }
                $wallet = new Wallet;
                $wallet->user_id = $sellerId;
                $wallet->amount = $finalPenalty;
                $wallet->payment_status = 'Debit';
                $wallet->payment_method = 'Wallet Payment';
                $wallet->payment_details = 'Cancel order used for order #'. $order->code;
                $wallet->add_status = 1;
                $wallet->order_id =$order->id;
                $wallet->save();
                    
                
            }
            
        }
             
        }
                 $PName = \Illuminate\Support\Str::limit($order->orderDetails->first()->product->name, 40, '...');
                //$smsMsg = "Your Order No. ".$order->code.", and Order Amount Rs. ".$order->grand_total." has been cancelled. We regret inconvenience caused to you. In case it's a prepaid order, Refund will be initiated in your Alde Bazaar Wallet. For any assistance WhatsApp: 80068-80055 or call .";
                $smsMsg = "Alde Bazaar: Your order with ".$PName.", has been cancelled. We regret inconvenience caused to you. For any assistance contact +91 9776-9776-12 or WhatsApp: 80068-80055";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Order Cancelled";
            } elseif ($request->status == "refunded") {
                $amount = $order->grand_total - $order->cart_shipping;
                $smsMsg = "Rs ".$amount." has been successfully refunded in your source account. We regret any inconvenience caused to you. For any assistance contact: +91 9776-9776-12.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Amount Refunded";
                $wallet=new Wallet;
                $wallet->user_id=$order->user_id;
                $wallet->amount=$amount;
                $wallet->payment_method='Refund';
                $wallet->payment_details="Order #".$order->code." Cancallation";
                $wallet->add_status=1;
                $wallet->save();
                $User = User::find($order->user_id);
                $User->balance=$User->balance+$amount;
                $User->save();
            } elseif ($request->status == 'delivered') {
                
               // $this->cashBackStatus($request->order_id);
                
                $DeliveryLog = DeliveryLog::where('order_id', $request->order_id)->where('status', 'pending')->first();
                if($DeliveryLog == null) {
                    $DeliveryLog = new DeliveryLog;
                    $DeliveryLog->order_id = $request->order_id;
                    $DeliveryLog->agent_id = 0;
                }
                if($DeliveryLog != null) {
                $DeliveryLog->status = 'delivered';
                $DeliveryLog->log = $item." item(s) has been delivered";
                $DeliveryLog->save();
                }
                
                $smsMsg = "Order Delivered: Hey ". $order->user->name ." ! It's time to celebrate. Your ".$order->code." has been delievered successfully. Thank you for shopping at Alde Bazaar.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Order Delivered";
            } elseif ($request->status == 'ready_to_ship') {
                $Pcount = $order->orderDetails->where('seller_id', $seller_id)->where('delivery_status','ready_to_ship')->count();
                    if($Pcount > 1) {
                        $Pcount  = $Pcount - 1;
                        $PName = \Illuminate\Support\Str::limit($order->orderDetails->first()->product->name, 20, '...')." and ". $Pcount ." more items";
                    } else {
                        $PName = \Illuminate\Support\Str::limit($order->orderDetails->first()->product->name, 40, '...');
                    }
                $smsMsg = "ALDE BAZAAR: Hi ". $order->user->name ." ! we have received your order for ".$PName." (Order No. ".$order->code."). We will notify you once it's shipped.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Order Confirmed";
            } else {
             //$smsMsg = "We update your order status to ".$request->status." for order no. ".$order->code.".";
             $smsMsg='';
            }
            
            
            
            if($request->status == 'delivered') {
                $this->cashBackStatus($order->id);
                $TotalItemCount = $order->orderDetails->count();
                $delivery_status_item = $order->orderDetails->where('delivery_status', 'delivered')->count();
                if($delivery_status_item == $TotalItemCount) {
                    $order->Delivery_Confirmed = Carbon::now();
                    $order->save();
                }
            }
        
        
            if($smsMsg != '') {
                $smsMsg = rawurlencode($smsMsg);
                $to = "91".$shipping->phone;
                if(strlen($to) == 12) {
                 $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                       sendSMS($SMS_URL);
                    }
                } 
            }
            if(isset($message)) {
            if($message != null) {
                $emailData = array('email' => $order->user->email, 'name' => $order->user->name, 'message' => $message, 'email_subject' => $email_subject);
                if(strlen($emailData['email']) > 4) {
                Mail::send('emails.order_notification',array('data' => $emailData), function ($m) use ($emailData) {
                        $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                });
                }
           }
           }
            
        if($setFlash != null) {
            flash($setFlash)->success();
        }
        return 1;
    }
    
     /***
     * Delete CashBack Croosponding Order id
     * 
     */
    public function DeleteCashBack($orderId,$status){
       
        
       // DB::table('cashbacks')->where('order_id', $orderId)->delete();
         $affected = DB::table('cashbacks')
              ->where('order_id', $orderId)
              ->update(['status' => 'cancel']);
              if($status == 'cancel' || $status == 'trash' || $status == 'denied'){
                  
                  $this->DeleteWalletCredit($orderId);
              }
          
    } 
    
    public function DeleteWalletCredit($orderId){
       /*$affected = DB::table('wallets')
              ->where('order_id', $orderId)
              ->update(['payment_status' => 'Credit']);*/
        $order = Wallet::where(['order_id'=>$orderId,'payment_status'=>'debit'])->first();
        if(!empty($order)){
            $wallet = new Wallet;
            $wallet->user_id = $order->user_id;
            $wallet->amount = $order->amount;
            $wallet->payment_method = 'cashback';
            $wallet->payment_status = 'credit'; 
            $wallet->payment_details = 'cashback has been credit in wallet';
            $wallet->add_status = 1;
            $wallet->order_id = $orderId;
            $wallet->save(); 
        }
        
        
        
    } 

    /****
     * Update user cashback Status
     * 
     */
    public function cashBackStatus($orderid){
       
        $affected = DB::table('cashbacks')
              ->where('order_id', $orderid)
              ->update(['status' => 'completed']);
        $cashackAmount = cashback::where('order_id',$orderid)->get();
        $walletCreditAmount = $cashackAmount[0]->cashback_amount;
         
        
        $wallet = new Wallet;
        $wallet->user_id = $cashackAmount[0]->user_id;
        $wallet->amount = $walletCreditAmount;
        $wallet->payment_method = 'cashback';
        $wallet->payment_status = 'Credit'; 
        $wallet->payment_details = 'cashback has been credit in wallet';
        $wallet->add_status = 1;
        $wallet->order_id = $orderid;
        $wallet->save();
        DB::table('cashbacks')->where('order_id', $orderid)->delete();
        //return redirect()->route('/orders/pending');      
    }

    public function update_payment_status(Request $request)
    {
        $setFlash = $request->input('setFlash',null);
        $seller_id = $request->input('seller_id',null);
        if($seller_id == null) {
            $seller_id = Auth::user()->id;
        }
        $order = Order::findOrFail($request->order_id);
        $order->payment_status_viewed = '0';
        $order->save();

        if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'seller' || Auth::user()->user_type == 'vendor'){
            foreach($order->orderDetails->where('seller_id', $seller_id) as $key => $orderDetail){
                $orderDetail->payment_status = $request->status;
                $orderDetail->save();
            }
        }
        else{
            foreach($order->orderDetails->where('seller_id', \App\User::where('user_type', 'admin')->first()->id) as $key => $orderDetail){
                $orderDetail->payment_status = $request->status;
                $orderDetail->save();
            }
        }

        $status = 'paid';
        foreach($order->orderDetails as $key => $orderDetail){
            if($orderDetail->payment_status != 'paid'){
                $status = 'unpaid';
            }
        }
        $order->payment_status = $status;
        $order->save();
        if($order->payment_status == 'paid' && $order->commission_calculated == 0){
          
            if ($order->payment_type == 'cash_on_delivery') {
                if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
                    $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                    foreach ($order->orderDetails as $key => $orderDetail) {
                        $orderDetail->payment_status = 'paid';
                        $orderDetail->save();
                        
                        if($orderDetail->product->user->user_type == 'seller'){
                            // $seller = $orderDetail->product->user->seller;
                            // $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100;
                            // $seller->save();
                        }
                    }
                }
                else{
                    foreach ($order->orderDetails as $key => $orderDetail) {
                        $orderDetail->payment_status = 'paid';
                        $orderDetail->save();
                        if($orderDetail->product->user->user_type == 'seller'){
                            $commission_percentage = $orderDetail->product->category->commision_rate;
                          
                            $seller = $orderDetail->product->user->seller;
                            //100*20/100=20
                            //($orderDetail->price*(100-$commission_percentage))/100=? (100 * (80))/100
                            $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100;
                            $seller->save();
                        }
                    }
                }
            }
            elseif($order->manual_payment) {
                if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
                    $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                    foreach ($order->orderDetails as $key => $orderDetail) {
                        $orderDetail->payment_status = 'paid';
                        $orderDetail->save();
                        if($orderDetail->product->user->user_type == 'seller'){
                            $seller = $orderDetail->product->user->seller;
                            $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100;
                            $seller->save();
                        }
                    }
                }
                else{
                    foreach ($order->orderDetails as $key => $orderDetail) {
                        $orderDetail->payment_status = 'paid';
                        $orderDetail->save();
                        if($orderDetail->product->user->user_type == 'seller'){
                            $commission_percentage = $orderDetail->product->category->commision_rate;
                            $seller = $orderDetail->product->user->seller;
                            $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100;
                            $seller->save();
                        }
                    }
                }
            }
            if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
                $affiliateController = new AffiliateController;
                $affiliateController->processAffiliatePoints($order);
            }

            if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated) {
                $clubpointController = new ClubPointController;
                $clubpointController->processClubPoints($order);
            }

            $order->commission_calculated = 1;
            $order->save();
        }
        if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_paid_status')->first()->value){
            try {
                $otpController = new OTPVerificationController;
                $otpController->send_payment_status($order);
            } catch (\Exception $e) {
            }
        }
        
        if($setFlash != null) {
            flash($setFlash)->success();
        }
        return 1;
    }
    
    public function deliveryupdate(Request $request) {
        $order = Order::findOrFail($request->order_id);
        $oldcharges = $order->cart_shipping;
        $delivery_charges = $request->delivery_charges;
        $order->grand_total -= $oldcharges;
        $order->grand_total += $delivery_charges;
        $order->cart_shipping = $delivery_charges;
        $order->save();
		flash(__('Delivery charges successfully updated.'))->success();
		return back();
    }

    //Update order by admin
    public function updatedOrderByAdmin(Request $request){
        DB::enableQueryLog();
         //get productDetails
         $product = Product::orWhere('product_id', $request->productId)
         ->orWhere('name', $request->productId)->get();

        $finalArray = [];
        $dataArray = [];
        foreach($product as $productDetails){
            
            $dataArray['productId'] = $productDetails->id;
            $dataArray['product_id'] = $productDetails->product_id;
            $dataArray['marg_code'] = $productDetails->marg_code;
            $dataArray['name'] = $productDetails->name;
            $dataArray['user_id'] = $productDetails->user_id;
            $dataArray['purchase_price'] = $productDetails->purchase_price;
            $dataArray['variant_product'] = $productDetails->variant_product;
            $dataArray['discount'] = $productDetails->discount;
            $dataArray['shipping_cost'] = $productDetails->shipping_cost;
            $dataArray['product_name'] = $productDetails->name;
            $dataArray['photos'] = $productDetails->photos;
            $dataArray['description'] = $productDetails->description;
            
        }
        array_push($finalArray,$dataArray);
        
        //print_r($finalArray[0]['productId']);die;
        $order = Order::findOrFail($request->order_id);
        $order_detail = new OrderDetail;
        $order_detail->order_id  =$order->id;
        $order_detail->seller_id = $finalArray[0]['user_id'];
        $order_detail->product_id = $finalArray[0]['productId'];
        $order_detail->variation = $finalArray[0]['variant_product'];
        $order_detail->price = $finalArray[0]['purchase_price'];
        $order_detail->discount = $finalArray[0]['discount'];
        $order_detail->tax = 0;
        
        $order_detail->shipping_cost = $finalArray[0]['shipping_cost'];
        $order_detail->quantity = 1;
        $order_detail->payment_status = 'Unpaid';
        $order_detail->delivery_status = 'shipped';
        //$order_detail->vendor_expenses_id = ;
        $order_detail->shipping_type = 'home_delivery';
        $order_detail->save();
        echo json_encode($productDetails);

    }

    public function searchProductDetails(Request $request){
        DB::enableQueryLog();
        $product = Product::orWhere('product_id', $request->productId)
         ->orWhere('name','like','%'.$request->productId.'%')->get();
        
       if(count($product) > 0){
           
        $finalArray = [];
        $dataArray = [];

        foreach($product as $productDetails){
            
            $dataArray['productId'] = $productDetails->id;
            $dataArray['product_id'] = $productDetails->product_id;
            $dataArray['marg_code'] = $productDetails->marg_code;
            $dataArray['name'] = $productDetails->name;
            $dataArray['user_id'] = $productDetails->user_id;
            $dataArray['purchase_price'] = $productDetails->purchase_price;
            $dataArray['variant_product'] = $productDetails->variant_product;
            $dataArray['discount'] = $productDetails->discount;
            $dataArray['shipping_cost'] = $productDetails->shipping_cost;
            $dataArray['product_name'] = $productDetails->name;
            $dataArray['photos'] = $productDetails->photos;
            $dataArray['description'] = $productDetails->description;
            
        }

        array_push($finalArray,$dataArray);
        
        //print_r($finalArray[0]['productId']);die;
        $order = Order::findOrFail($request->order_id);
      
        
        $productDetails = [
            'product_name'=>$finalArray[0]['product_name'],
            'photos'=>$finalArray[0]['photos'],
            'description'=>$finalArray[0]['description'],
            'price'=>$finalArray[0]['purchase_price'],
            'status'=>200
        ];
       }else{
        $productDetails = [
            'product_name'=>'Product Not Found',
            'status'=>500
        ];
       }
       
       echo json_encode($productDetails);

    }
    
    //OrderDenied
    public function orderDenied(Request $request){
       
        $orderId = $request->orderId;
        $deniedMessage = $request->deniedMessage ;
        
        
         $affected = OrderDetail::where('order_id', $orderId)->update(['delivery_status' => 'denied']);
         $affected = Order::where('id', $orderId)->update(['reason'=>$deniedMessage]);
         
         $this->DeleteCashBack($orderId,'denied');
         flash(__('Order has been Denied succesfully.'))->success();
    }
    
    //Update pending amount with actual amount
    public function updatedPendingAmount($oderId=0){
      
        $ordeList = Order::whereDate('updated_at', Carbon::now()->subDays(1))->get();
        $ordeList = Order::where(['id'=>$oderId])->get();
         echo '<pre>';print_r($ordeList);
        
     //order_details
        $productAmountToSeller = 0;
        $updation =false;
        foreach($ordeList as $updateList){
           echo $updateList->id;
           echo '<br>';
            $orderDetails = OrderDetail::where(['order_id'=>$updateList->id,'delivery_status'=>'delivered','payment_status'=>'paid'])->get();
           
           
            if(isset($orderDetails)){
              foreach($orderDetails as $updatOrderDetails){
                  
                 echo 'sellerid=>'.$updatOrderDetails->seller_id;
                 $getSellerAmount = getSellerAmountByorderId($updatOrderDetails->order_id,$updatOrderDetails->seller_id);
              }
              
            }
            
        }
      // dd($updation,$productAmountToSeller,$orderId);
            if(!empty($getSellerAmount)){
                Seller::where('user_id', $updatOrderDetails->seller_id)
                ->update(['admin_to_pay' => $getSellerAmount]);
                    if($updatOrderDetails->order_id){
                        Order::where('id', $updatOrderDetails->order_id)
                        ->update(['paid_to_seller_account' => 'done']); 
                    }
                 dd('updation has been done last 7 days order');        
                
            }else{
                dd('no any order avilable for updation');
            }
        
        
    }
    
}
