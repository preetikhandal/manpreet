<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\popup_offer;

class PopOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $popup_offer = popup_offer::all();
        return view('popup_offer.index', compact('popup_offer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('popup_offer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if($request->hasFile('photo')){
            $popup_offer = new popup_offer;
            $popup_offer->photo = $request->photo->store('uploads/popup_offer');
            $popup_offer->url = $request->url;
            $popup_offer->save();
            flash(__('Popup offer has been inserted successfully'))->success();
        }
        return redirect()->route('home_settings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $popup_offer = popup_offer::findOrFail($id);
        return view('popup_offer.edit', compact('popup_offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
		$popup_offer = popup_offer::find($id);
        $oldimage = $popup_offer->photo;
        if($request->hasFile('photo')){
			if(file_exists($oldimage)){
				unlink($oldimage);
			}
            $popup_offer->photo = $request->photo->store('uploads/popup_offer');
        }
        $popup_offer->url = $request->url;
        $popup_offer->save();
        flash(__('Popup offer has been updated successfully'))->success();
        return redirect()->route('home_settings.index');
		
    }
	public function update_status(Request $request)
    {
		foreach (popup_offer::all() as $key => $popup_offer) {
            $popup_offer->published = 0;
            $popup_offer->save();
        }
        $popup_offer = popup_offer::findOrFail($request->id);
        $popup_offer->published = $request->status;
        if($popup_offer->save()){
            return '1';
        }
		else {
			return '0';
		}
		return 0;
		
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $popup_offer = popup_offer::findOrFail($id);
        if(popup_offer::destroy($id)){
			if(file_exists($popup_offer))
				unlink($popup_offer->photo);
            flash(__('Popup offer has been deleted successfully'))->success();
        }
        else{
            flash(__('Something went wrong'))->error();
        }
        return redirect()->route('home_settings.index');
    }
}
