<?php

namespace App\Http\Controllers;

set_time_limit(0);
use Illuminate\Http\Request;
use App\Category;
use App\User;
use App\Product;
use App\Order;
use App\OrderDetail;
use App\Language;
use App\MargUnlistedProduct;
use App\MargUnlistedCustomer;
use ImageOptimizer;
use Illuminate\Support\Facades\Cache;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Log;

class MargController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function debug(Request $request)
    {
        $key = env('API_KEY');
        $payload = json_encode(array("CompanyCode" => env('COMPANY_CODE'), "MargID" => env('MARG_ID'), "Datetime" => "", "index" => "0"));
        $url = "https://wservices.margcompusoft.com/api/eOnlineData/MargMST2017";
        $ch = curl_init($url);
        # Setup request to send json via POST.
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        //Retrieve the size of the remote file in bytes.
        $response = curl_exec($ch);
        $fileSize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        $size = (int)$fileSize;
        curl_close($ch);
        if($size > 300) {
            $response = $this->decrypt($key, $response);
        }
        $c = gzinflate(base64_decode($response));
        $c = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $c), true );
        $n = 0;
        //dd($c['Details']['Party']);
        $sku = $request->sku;
        if($c['Details']['Status'] == 'Sucess') {
            $Products = $c['Details']['pro_N'];
            
            foreach($Products as $P) {
                if($P['ProductCode'] == $sku) {
                    dd($P);
                    die();
                }
            }
        } else {
            echo $c['Details']['Message'];
        }
        echo "<br /> Processing has been done.";
    }
    
    public function sync(Request $request)
    {
        $time_start = microtime(true);
        $syncTime = $request->input('s',time());
        $f = $request->input('f', 0);
        $unlistproduct = $request->input('unlistproduct', 0);
        $logging = $request->input('logging', 1);
        
        $time = filemtime(public_path("marg/margsync.db"));
        $ctime = time() - $time;
        if($f == 1) {
            $ctime = 999999;
        }
        if($ctime > 1800) {
            $key = env('API_KEY');
            $payload = json_encode(array("CompanyCode" => env('COMPANY_CODE'), "MargID" => env('MARG_ID'), "Datetime" => "", "index" => "0"));
            $url = "https://wservices.margcompusoft.com/api/eOnlineData/MargMST2017";
            $ch = curl_init($url);
            # Setup request to send json via POST.
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            # Return response instead of printing.
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            # Send request.
            //Retrieve the size of the remote file in bytes.
            $response = curl_exec($ch);
            $fileSize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
            $size = (int)$fileSize;
            curl_close($ch);
            if($size > 300) {
                $response = $this->decrypt($key, $response);
            }
            $c = gzinflate(base64_decode($response));
            $c = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $c), true );
            $margData = json_encode($c);
            file_put_contents(public_path("marg/margsync.db"), $margData);
        } else {
            $c = file_get_contents(public_path("marg/margsync.db"));
            $c = json_decode($c, true);
        }
        $n = 0;
        //dd($c['Details']['Party']);
        if($c['Details']['Status'] == 'Sucess') {
            
            $Products = $c['Details']['pro_N'];
            /*
            foreach($Products as $P) {
                if($P['ProductCode'] == "HUL392") {
                    dd($P);
                    die();
                }
            }*/
            $marg_prog = file_get_contents(public_path("marg/margsyncprg.db"));
            if($marg_prog != "") {
                $marg_prog = json_decode($marg_prog, true);
                $d = $marg_prog['CurrentNo'];
            } else {
                $marg_prog = array();
                $d = 1;
            }
            
            $TotalProductCount = count($Products);
            foreach($Products as $P) {
                $margsyncData = array("syncTime" => $syncTime, 'ProductCode' => $P['ProductCode'], 'TotalProduct' => $TotalProductCount, "CurrentNo" => $d);
                if(count($marg_prog) > 0) {
                    if($marg_prog['syncTime'] > 0 && $marg_prog['syncTime'] != $syncTime) {
                        if($P['ProductCode'] == $marg_prog['ProductCode']) {
                        } else {
                            continue;
                        }
                    }
                }
               $marg_prog = $margsyncData;
               $margsyncData = json_encode($margsyncData);
               file_put_contents(public_path("marg/margsyncprg.db"), $margsyncData);
                /*if($P['ProductCode'] == "PCH001") {
                    dd($P);
                    die();
                }*/
             $marg_code = $P['code'];   
             $ProductCode = $P['ProductCode'];
                $Pro = Product::where('product_id', $ProductCode)->first();
                if($Pro == null) {
                    $Pro = Product::where('marg_code',$marg_code)->first();
                } else {
                    $Pro->marg_code = $P['code'];
                }
                if($Pro == null) {
                    if($unlistproduct == 1) {
                        $count = MargUnlistedProduct::where('marg_code',$P['code'])->count();
                        if($count == 0) {
                            $MargUnlistedProduct = new MargUnlistedProduct;
                            $MargUnlistedProduct->product_code = $ProductCode;
                            $MargUnlistedProduct->marg_code = $P['code'];
                            $MargUnlistedProduct->name = $P['name'];
                            $MargUnlistedProduct->stock = (int)$P['stock'];
                            $MargUnlistedProduct->mrp = $P['MRP'];
                            $MargUnlistedProduct->rate = $P['Rate'];
                            $MargUnlistedProduct->save();
                        }
                    }
                } else {
                    MargUnlistedProduct::where('marg_code',$P['code'])->delete();
                    $STOCK_UPDATE_CATEGORY = env('STOCK_UPDATE_CATEGORY');
                    $STOCK_UPDATE_CATEGORY = explode(',', $STOCK_UPDATE_CATEGORY);
                    $MRP_UPDATE_CATEGORY = env('MRP_UPDATE_CATEGORY');
                    $MRP_UPDATE_CATEGORY = explode(',', $MRP_UPDATE_CATEGORY);
                    
                    if(in_array($Pro->category_id, $STOCK_UPDATE_CATEGORY)) {
                        if($Pro->current_stock != "-1") {
                            if($logging == 1) {
                            $log = new Log;
                            $log->module = "Marg Sync";
                            $log->func = "Product Stock";
                            $log->data1 = $Pro->product_id;
                            $log->data2 = $Pro->current_stock;
                            $log->data3 = (int)$P['stock'];
                            }
                            $Pro->current_stock = (int)$P['stock'];
                            if($logging == 1) {
                            $log->data4 = $Pro->current_stock;
                            $log->save();
                            }
                        }
                    }
                    if(in_array($Pro->category_id, $MRP_UPDATE_CATEGORY)) {
                        $MRP = floatval($P['MRP']);
                        if($MRP > 0) {
                            $Pro->unit_price = $MRP;   
                        }
                    }
                    
                    $Pro->save();
                    echo "*";
                    $n++;
                    if($n == 80) {
                        echo "<br />";
                        $n = 0;
                    }
                }
                $time_end = microtime(true);
                $execution_time = $time_end - $time_start;
                if( $execution_time > 100 ) {
                    echo '<br /> Execution Time up, page will auto-refresh to continue sync.<br />Total Product : '.$TotalProductCount.' <br /> Proccessed : '.$d.'<br /><script> window.location.href = "'. url('admin/marg/sync?s='). time().'";</script>';
                    die();
                }
                $d++;
            }
            $Parties = $c['Details']['Party']; 
            foreach($Parties as $Party) {
                $rid = $Party['rid'];
                $count = User::where('marg_rid', $rid)->count();
                if($count == 0) {
                    $c = MargUnlistedCustomer::where('marg_rid', $rid)->count();
                    if($c == 0) {
                        $MargUnlistedCustomer = new MargUnlistedCustomer;
                        $MargUnlistedCustomer->marg_rid = $rid;
                        $MargUnlistedCustomer->name = $Party['name'];
                        $MargUnlistedCustomer->save(); 
                    }
                } else {
                    MargUnlistedCustomer::where('marg_rid', $rid)->delete();
                }
                echo "*";
                $n++;
                if($n == 80) {
                    echo "<br />";
                    $n = 0;
                }
            }
        } else {
            echo $c['Details']['Message'];
        }
        $margsyncData = array("syncTime" => 0, 'ProductCode' => "", 'CurrentNo' => 1);
        $marg_prog = $margsyncData;
        $margsyncData = json_encode($margsyncData);
        file_put_contents(public_path("marg/margsyncprg.db"), $margsyncData);
        echo "<br /> Processing has been done.";
    }
    
    private function decrypt($key,$encrypted)
    {
        $rawdata = base64_decode($encrypted);
        $iv = $key. "\0\0\0\0";
        $decrypted =  rtrim(openssl_decrypt($rawdata, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $iv), "\0");
        return $decrypted;
    }
    
    public function unlistedProduct(Request $request)
    {
        $query = null;
        $sort_search = null;
        $MargUnlistedProduct = MargUnlistedProduct::query();

        if ($request->search != null){
            $MargUnlistedProduct = $MargUnlistedProduct
                        ->where('name', 'like', '%'.$request->search.'%')->orWhere('product_code',$request->search);
            $sort_search = $request->search;
        }

        $MargUnlistedProduct = $MargUnlistedProduct->orderBy('created_at', 'desc')->paginate(15);
        $Products = Product::where('marg_code', null)->get();
        $ProductsOption = '';
        foreach($Products as $P) {
            $ProductsOption .= '<option value="'.$P->id.'">'.$P->name.'</option>';
        
        }
        return view('marg.unlistedProduct', compact('MargUnlistedProduct', 'query', 'sort_search', 'ProductsOption'));
    }
    
    public function unlistedProductMap(Request $request) {
        $MargUnlistedProductID = $request->MargUnlistedProductID;
        $ProductID = $request->ProductID;
        foreach($ProductID as $key => $pid) {
            if($pid > 0) {
                $MargUnlistedProduct = MargUnlistedProduct::find($MargUnlistedProductID[$key]);
                $Product = Product::find($pid);
                $Product->marg_code = $MargUnlistedProduct->marg_code;
                $STOCK_UPDATE_CATEGORY = env('STOCK_UPDATE_CATEGORY');
                $STOCK_UPDATE_CATEGORY = explode(',', $STOCK_UPDATE_CATEGORY);
                if(in_array($Product->category_id, $STOCK_UPDATE_CATEGORY)) {
                    $Product->current_stock = $MargUnlistedProduct->stock;
                }
                $Product->save();
                $MargUnlistedProduct->delete();
            }
        }
        flash(__('Product has been assigned successfully'))->success();
        return back();
    }
    
    public function unmappedProduct()
    {
        return view('marg.unmappedProduct');
    }
    
    public function unlistedCustomer(Request $request)
    {
        $query = null;
        $sort_search = null;
        $MargUnlistedCustomer = MargUnlistedCustomer::query();

        if ($request->search != null){
            $MargUnlistedCustomer = $MargUnlistedCustomer
                        ->where('name', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }
        $MargUnlistedCustomer = $MargUnlistedCustomer->orderBy('created_at', 'desc')->paginate(15);
        $User = User::where('user_type', 'customer')->where('marg_rid', null)->get();
        $CustomerOption = '';
        foreach($User as $U) {
            $CustomerOption .= '<option value="'.$U->id.'">'.$U->name.'</option>';
        
        }
        return view('marg.unlistedCustomer', compact('MargUnlistedCustomer', 'query', 'sort_search', 'CustomerOption'));
    }
    
    
    public function unlistedCustomerMap(Request $request) {
        $MargUnlistedCustomerID = $request->MargUnlistedCustomerID;
        $UserID = $request->UserID;
        foreach($UserID as $key => $uid) {
            if($uid > 0) {
                $MargUnlistedCustomer = MargUnlistedCustomer::find($MargUnlistedCustomerID[$key]);
                $User = User::find($uid);
                $User->marg_rid = $MargUnlistedCustomer->marg_rid;
                $User->save();
                $MargUnlistedCustomer->delete();
            }
        }
        flash(__('Customer has been assigned successfully'))->success();
        return back();
    }
    
    public function unmappedCustomer(Request $request)
    {
        $query = null;
        $sort_search = null;
        $User = User::where('user_type', 'customer')->where('marg_rid', null);

        if ($request->search != null){
            $User = $User
                        ->where('name', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }
        $User = $User->orderBy('created_at', 'desc')->paginate(15);
        $MargUnlistedCustomer = MargUnlistedCustomer::get();
        $MargUnlistedCustomerOption = '';
        foreach($MargUnlistedCustomer as $U) {
            $MargUnlistedCustomerOption .= '<option value="'.$U->id.'">'.$U->name.'</option>';
        }
        return view('marg.unmappedCustomer', compact('User', 'query', 'sort_search', 'MargUnlistedCustomerOption'));
    }
    
    
    public function unmappedCustomerMap(Request $request)
    {
        $MargUnlistedCustomerID = $request->MargUnlistedCustomerID;
        $UserID = $request->UserID;
        foreach($MargUnlistedCustomerID as $key => $marg_id) {
            if($marg_id > 0) {
                $MargUnlistedCustomer = MargUnlistedCustomer::find($marg_id);
                $User = User::find($UserID[$key]);
                $User->marg_rid = $MargUnlistedCustomer->marg_rid;
                $User->save();
                $MargUnlistedCustomer->delete();
            }
        }
        flash(__('Customer has been assigned successfully'))->success();
        return back();
    }
    
    public function marg_order_send(Request $request)
    {
        $flash_message_success = "Following order processed successfully: ";
        $flash_message_error = "Following order can't processed: ";
        $OrderIDs = $request->order_ids;
        $Orders = Order::whereIn('id',$OrderIDs)->get();
        foreach($Orders as $Order){
            $Products = array();
            $ProductQuantity = array();
            $ProductFree = array();
            $OrderUser = $Order->user_id;
            $shipping_address = json_decode($Order->shipping_address);
            $orderDetails = $Order->orderDetails;
            $ProductIDs = $orderDetails->pluck('product_id')->all();
            $ProductMargCode = Product::select('id','name')->whereIn('id',$ProductIDs)->where('marg_code',null)->count();
            if($ProductMargCode == 0) {
                foreach($orderDetails as $OD) {
                $product = Product::find($OD->product_id);
                $quantity = $OD->quantity;
                $Products[] = $product->marg_code;
                $ProductQuantity[] = $quantity;
                $ProductFree[] = 0;
                }
                if(env('MARG')) {
                    $Products = implode(",",$Products);
                    $ProductQuantity = implode(",",$ProductQuantity);
                    $ProductFree = implode(",",$ProductFree);
                    if($OrderUser != null) {
                        $OrderUser = User::find($OrderUser)->marg_rid;
                    }
                    $total = $Order->grand_total;
                    if($OrderUser != null) {
                        $id = $Order->id;
                        $payment_option = $Order->payment_type;
                        $payload = json_encode( array( "OrderID" => $id, "OrderNo" => $id, "CustomerID" => $OrderUser, "MargID" => env('MARG_ID'), "Type" => "S", "Sid" => env('SALES_USER_ID'), "ProductCode" => "[".$Products."]", "Quantity" => "[".$ProductQuantity."]", "Free" => "[".$ProductFree."]", "Lat" => "", "Lng" => "", "Address" => $shipping_address->address." ". $shipping_address->city, "GpsID" => "0", "UserType" => "1", "Points" => "0.00", "Discounts" => "0.00", "Transport" => "", "Delivery" => "COD", "Bankname" => "", "BankAdd1" => "", "BankAdd2" => "", "shipname" => "", "shipAdd1" => "$shipping_address->address", "shipAdd2" => "$shipping_address->city", "shipAdd3" => "", "paymentmode" => "$payment_option", "paymentmodeAmount" => "$total", "payment_remarks" => "", "order_remarks" => "", "CustMobile" => $shipping_address->phone, "CompanyCode" => env('COMPANY_CODE'), "OrderFrom" => env('COMPANY_CODE') ));
                       
                        $url = "https://wservices.margcompusoft.com/api/eOnlineData/InsertOrderDetail";
                        $ch = curl_init($url);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                        $response = curl_exec($ch);
                        curl_close($ch);
                        $c = gzinflate(base64_decode($response));
                        $c = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $c), true );
                        if($c['Details']['Status'] == 'Sucess') {
                            $OrderNo = $c['Details']['OrderDetails'][0]['OrderNo'];
                            $Order->marg_order_no = $OrderNo;
                            $Order->save();
                        }
                        $flash_message_success .= $Order->code ." ";
                    } else {
                        $flash_message_error .= $Order->code ." ";
                    }
                }
            } else {
                $flash_message_error .= $Order->code." ";
            }
        }
        if($flash_message_success != "Following order processed successfully: ") {
        flash(__($flash_message_success))->success();
        }
        if($flash_message_error != "Following order can't processed: ") {
            flash(__($flash_message_error))->error();
        }
        
        return response()->json(array('status' => true));
    }
    
    public function marg_customer_map(Request $request)
    {
        $MargUnlistedCustomerID = $request->modal_marg_id;
        $UserID = $request->userid;
        $MargUnlistedCustomer = MargUnlistedCustomer::find($MargUnlistedCustomerID);
        $User = User::find($UserID);
        $User->marg_rid = $MargUnlistedCustomer->marg_rid;
        $User->save();
        $MargUnlistedCustomer->delete();
        return response()->json(array('status' => true));
    }
    
    public function order_pending(Request $request)
    {
        $sort_search = null;
        $orders = DB::table('orders')
                    ->where('marg_order_no', null)
                    ->orderBy('code', 'desc')
                    ->select('orders.id');
       
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
        }
        $orders = $orders->paginate(15);
        return view('marg.order_pending', compact('orders', 'sort_search'));
    }
    
    
    public function order_processed(Request $request)
    {
        $sort_search = null;
        $orders = DB::table('orders')
                    ->where('marg_order_no', '!=',null)
                    ->orderBy('code', 'desc')
                    ->select('orders.id');
       
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
        }
        $orders = $orders->paginate(15);
        return view('marg.order_pending', compact('orders', 'sort_search'));
    }
    
    public function get_marg_customer(Request $request)
    {
        $key = env('API_KEY');
        $datetime = $request->datetime;
        $date = Carbon::createFromFormat('d/m/Y', $datetime)->subHour(2);
        $payload = json_encode(array("CompanyCode" => env('COMPANY_CODE'), "MargID" => env('MARG_ID'), "Datetime" => $date->toDateTimeString(), "index" => "0"));
        $url = "https://wservices.margcompusoft.com/api/eOnlineData/MargMST2017";
        $ch = curl_init($url);
        # Setup request to send json via POST.
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        //Retrieve the size of the remote file in bytes.
        $response = curl_exec($ch);
        $fileSize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        $size = (int)$fileSize;
        curl_close($ch);
        if($size > 250) {
            $response = $this->decrypt($key, $response);
        }
        $c = gzinflate(base64_decode($response));
        $c = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $c), true );
        $n = 0;
        if($c['Details']['Status'] == 'Sucess') {
            $Parties = $c['Details']['Party'];
            foreach($Parties as $Party) {
                $rid = $Party['rid'];
                $count = User::where('marg_rid', $rid)->count();
                if($count == 0) {
                    $c = MargUnlistedCustomer::where('marg_rid', $rid)->count();
                    if($c == 0) {
                        $MargUnlistedCustomer = new MargUnlistedCustomer;
                        $MargUnlistedCustomer->marg_rid = $rid;
                        $MargUnlistedCustomer->name = $Party['name'];
                        $MargUnlistedCustomer->save(); 
                    }
                } else {
                    MargUnlistedCustomer::where('marg_rid', $rid)->delete();
                }
            }
        }
        $MargUnlistedCustomer = MargUnlistedCustomer::select('id','name')->get();
        return response()->json(array('status' => true, 'MargUnlistedCustomer' => $MargUnlistedCustomer));
    }
    public function get_marg_product(Request $request)
    {
        $key = env('API_KEY');
        $datetime = $request->datetime;
        $date = Carbon::createFromFormat('d/m/Y', $datetime)->subHour(2);
        $payload = json_encode(array("CompanyCode" => env('COMPANY_CODE'), "MargID" => env('MARG_ID'), "Datetime" => $date->toDateTimeString(), "index" => "0"));
        $url = "https://wservices.margcompusoft.com/api/eOnlineData/MargMST2017";
        $ch = curl_init($url);
        # Setup request to send json via POST.
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        //Retrieve the size of the remote file in bytes.
        $response = curl_exec($ch);
        $fileSize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        $size = (int)$fileSize;
        curl_close($ch);
        if($size > 250) {
            $response = $this->decrypt($key, $response);
        }
        $c = gzinflate(base64_decode($response));
        $c = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $c), true );
        $n = 0;
        if($c['Details']['Status'] == 'Sucess') {
            $Products = $c['Details']['pro_N'];
            foreach($Products as $P) {
             $marg_code = $P['code'];   
             $ProductCode = $P['ProductCode'];
                $Pro = Product::where('product_id', $ProductCode)->first();
                if($Pro == null) {
                    $Pro = Product::where('marg_code',$marg_code)->first();
                } else {
                    $Pro->marg_code = $P['code'];
                }
                if($Pro == null) {
                    $count = MargUnlistedProduct::where('marg_code',$P['code'])->count();
                    if($count == 0) {
                        $MargUnlistedProduct = new MargUnlistedProduct;
                        $MargUnlistedProduct->product_code = $ProductCode;
                        $MargUnlistedProduct->marg_code = $P['code'];
                        $MargUnlistedProduct->name = $P['name'];
                        $MargUnlistedProduct->stock = (int)$P['stock'];
                        $MargUnlistedProduct->mrp = $P['MRP'];
                        $MargUnlistedProduct->rate = $P['Rate'];
                        $MargUnlistedProduct->save();
                    }
                } else {
                    MargUnlistedProduct::where('marg_code',$P['code'])->delete();
                    $STOCK_UPDATE_CATEGORY = env('STOCK_UPDATE_CATEGORY');
                    $STOCK_UPDATE_CATEGORY = explode(',', $STOCK_UPDATE_CATEGORY);
                    if(in_array($Pro->category_id, $STOCK_UPDATE_CATEGORY)) {
                        $Pro->current_stock = (int)$P['stock'];
                    }
                    $Pro->save();
                }
            }
        }
        $MargUnlistedProduct = MargUnlistedProduct::select('id','name')->get();
        return response()->json(array('status' => true, 'MargUnlistedProduct' => $MargUnlistedProduct));
    }
    
    public function marg_order_product(Request $request) {
        $order_id = $request->order_id;
        $order = Order::find($order_id);
        $Product = Product::select('id','name','product_id','unit_price')->whereIn('id',$order->orderDetails->pluck('product_id'))->where('marg_code',null)->get();
        return response()->json(array('status' => true, 'Product' => $Product));
    }
}
 