<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\User;
use App\Seller;
use App\BusinessSetting;
use Auth;
use Hash;
use Mail;

class ShopController extends Controller
{

    public function __construct()
    {
        $this->middleware('user', ['only' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $shop = Auth::user()->shop;
       
        return view('frontend.seller.shop', compact('shop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {     
        if(Auth::check() && Auth::user()->user_type == 'admin'){
            flash(__('Admin can not be a seller'))->error();
            return back();
        }
        else{
            return view('frontend.seller_form');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   //dd($request->input());
        if(Auth::check()){
            $validatedData = $request->validate([
			'name' => 'required|max:200',
			'address' => 'required|max:500',
		
			'landmark' => 'max:200',
			'city' => 'required|max:100',
			'state' => 'required|max:100',
			'pincode' => 'required|min:6|max:6',
			'contact_person_name' => 'required|max:255',
			'contact_person_phone' => 'required|min:10|max:10',
			'firm_type' => 'required'
		]);
        }else{
            $validatedData = $request->validate([
			'name' => 'required|max:200',
			'address' => 'required|max:500',
			'phone' => 'required|min:10|max:10',
			'landmark' => 'max:200',
			'city' => 'required|max:100',
			'state' => 'required|max:100',
			'pincode' => 'required|min:6|max:6',
			'contact_person_name' => 'required|max:255',
			'contact_person_phone' => 'required|min:10|max:10',
			'firm_type' => 'required'
		]);
        }
        
        
        $user = null;
        if(!Auth::check()){
            $validatedData = $request->validate([
    			'user_name' => 'required|min:3|max:200',
    			//'email' => 'email:rfc,dns',
    			'password' => 'required||min:8|max:200|same:password_confirmation',
			    'password_confirmation' => 'min:8'
    		]);
            
            if(User::where('email', $request->email)->first() != null){
                flash(__('Email already exists!'))->error();
                return back();
            }
			if(User::where('phone', $request->phone)->first() != null){
                flash(__('Mobile Number already exists!'))->error();
                return back();
            }
            if($request->password == $request->password_confirmation){
                $user = new User;
                $user->name = $request->user_name;
                $user->email = $request->email;
				$user->phone = $request->phone;
                $user->user_type = "seller";
                $user->password = Hash::make($request->password);
                $user->save();
            }
            else{
                flash(__('Sorry! Password did not match.'))->error();
                return back();
            }
        }
        else{
		
            $user = Auth::user();
            if($user->customer != null){
                $user->customer->delete();
            }
            $user->phone = $request->phone;
            $user->user_type = "seller";
            $user->save();
        }

        if(BusinessSetting::where('type', 'email_verification')->first()->value != 1){
            $user->email_verified_at = date('Y-m-d H:m:s');
            $user->save();
        }
      
        $seller = new Seller;
        $seller->user_id = $user->id;
        $seller->save();

        if(Shop::where('user_id', $user->id)->first() == null){
            $shop = new Shop;
            $shop->user_id = $user->id;
            $shop->name = $request->name;
            $shop->address = $request->address;
            $shop->landmark = $request->landmark;
            $shop->city = $request->city;
            $shop->state = $request->state;
            $shop->pincode = $request->pincode;
            $shop->contact_person_name = $request->contact_person_name;
            $shop->contact_person_phone = $request->contact_person_phone;
            $shop->firm_type = $request->firm_type;
            $shop->gstin = $request->gstin;
            $shop->trade_license_no = $request->trade_license_no;
            $shop->business_registration_in = $request->business_registration_in;
            $shop->slug = preg_replace('/\s+/', '-', $request->name).'-'.$shop->id;

            if($shop->save()){
                
                $data = array('email' => $user->email, 'name' => $user->name);
                
                Mail::send('emails.shop_register', ['data' => $data], function ($m) use ($data) {
                        $m->to($data['email'], $data['name'])->subject('Alde Bazaar :: Seller Registration Confirmation');
                    });
                auth()->login($user, false);
                auth::logout();
                abort(401,"Thank you for registering as a seller with Alde Bazaar. We will notify you once your account gets activated as a seller.");
                //flash(__('Your Shop has been created successfully!'))->success();
                //return redirect()->route('shops.index');
            }
            else{
                $seller->delete();
                $user->user_type == 'customer';
                $user->save();
            }
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider=$request->sliderinput;
        $socialmediainput=$request->socialmediainput;
      if($slider!="slider" && $socialmediainput!="socialmedia"){
             $validatedData = $request->validate([
    			'name' => 'required|max:200',
    			'address' => 'required|max:500',
    			'landmark' => 'max:200',
    			'city' => 'required|max:100',
    			'state' => 'required|max:100',
    			'pincode' => 'required|digits:6',
    			'contact_person_name' => 'required|max:255',
    			'contact_person_phone' => 'required|min:10|max:10',
    			
    		]);
      }
        $shop = Shop::find($id);

        if($request->has('name') && $request->has('address')){
            $shop->name = $request->name;
            $shop->address = $request->address;
            $shop->landmark = $request->landmark;
            $shop->city = $request->city;
            $shop->state = $request->state;
            $shop->pincode = $request->pincode;
            $shop->contact_person_name = $request->contact_person_name;
            $shop->contact_person_phone = $request->contact_person_phone;
            if($shop->firm_type  == "") {
                $shop->firm_type = $request->firm_type;
            }
            if($shop->gstin  == "") {
                $shop->gstin = $request->gstin;
            }
            if($shop->trade_license_no  == "") {
                $shop->trade_license_no = $request->trade_license_no;
            }
            if($shop->business_registration_in  == "") {
                $shop->business_registration_in = $request->business_registration_in;
            }
            $shop->slug = preg_replace('/\s+/', '-', $request->name).'-'.$shop->id;

            $shop->meta_title = $request->meta_title;
            $shop->meta_description = $request->meta_description;

            if($request->hasFile('logo')){
                $shop->logo = $request->logo->store('uploads/shop/logo');
            }

            if ($request->has('pick_up_point_id')) {
                $shop->pick_up_point_id = json_encode($request->pick_up_point_id);
            }
            else {
                $shop->pick_up_point_id = json_encode(array());
            }
        }

        elseif($request->has('facebook') || $request->has('google') || $request->has('twitter') || $request->has('youtube') || $request->has('instagram')){
            $shop->facebook = $request->facebook;
            $shop->google = $request->google;
            $shop->twitter = $request->twitter;
            $shop->youtube = $request->youtube;
        }

        else{
            if($request->has('previous_sliders')){
                $sliders = $request->previous_sliders;
            }
            else{
                $sliders = array();
            }

            if($request->hasFile('sliders')){
                foreach ($request->sliders as $key => $slider) {
                    array_push($sliders, $slider->store('uploads/shop/sliders'));
                }
            }

            $shop->sliders = json_encode($sliders);
        }

        if($shop->save()){
            flash(__('Your Shop has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify_form(Request $request)
    {
        if(Auth::user()->seller->verification_info == null){
            $shop = Auth::user()->shop;
            return view('frontend.seller.verify_form', compact('shop'));
        }
        else {
            flash(__('Sorry! You have sent verification request already.'))->error();
            return back();
        }
    }

    public function verify_form_store(Request $request)
    {
        $data = array();
        $i = 0;
        foreach (json_decode(BusinessSetting::where('type', 'verification_form')->first()->value) as $key => $element) {
            $item = array();
            if ($element->type == 'text') {
                $item['type'] = 'text';
                $item['label'] = $element->label;
                $item['value'] = $request['element_'.$i];
            }
            elseif ($element->type == 'select' || $element->type == 'radio') {
                $item['type'] = 'select';
                $item['label'] = $element->label;
                $item['value'] = $request['element_'.$i];
            }
            elseif ($element->type == 'multi_select') {
                $item['type'] = 'multi_select';
                $item['label'] = $element->label;
                $item['value'] = json_encode($request['element_'.$i]);
            }
            elseif ($element->type == 'file') {
                $item['type'] = 'file';
                $item['label'] = $element->label;
                $item['value'] = $request['element_'.$i]->store('uploads/verification_form');
            }
            array_push($data, $item);
            $i++;
        }
        $seller = Auth::user()->seller;
        $seller->verification_info = json_encode($data);
        if($seller->save()){
            flash(__('Your shop verification request has been submitted successfully!'))->success();
            return redirect()->route('dashboard');
        }
        
        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }
}
