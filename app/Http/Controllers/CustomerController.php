<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use App\Order;
use App\CustomerExportExcel;
use App\Wallet;
use Hash;
use Mail;
use Excel;
use DB;
use App\Feedback;
use App\Pharmacy;
use Cookie;
use Illuminate\Support\Str;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   DB::enableQueryLog();

        // and then you can get query log
        
       
        $sort_search = null;
        $customers = Customer::orderBy('created_at', 'desc');
        
        if ($request->has('search')){
            $sort_search = $request->search;
            $user_ids = User::where('user_type', 'customer')->where(function($user) use ($sort_search){
                $user->where('name', 'like', '%'.$sort_search.'%')->orWhere('email', 'like', '%'.$sort_search.'%')->orWhere('phone', 'like', '%'.$sort_search.'%');
            })->pluck('id')->toArray();
            $customers = $customers->where(function($customer) use ($user_ids){
                $customer->whereIn('user_id', $user_ids);
            });
        }
        
        $customers = $customers->paginate(15);
        
       
        return view('customers.index', compact('customers', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
       $validatedData = $request->validate([
			'name' => 'required|string|min:4|max:191',
			'mobile' => 'required|digits:10',
			'address' => 'max:300',
			'country' => 'max:30',
			'city' => 'max:30',
			'state' => 'max:30',
            'postal_code' => 'digits:6'
            
		]);
		
	//	dd(User::where('email',$request->email)->first());
	    if($request->email != "") {
	        if(User::where('email',$request->email)->first() != null)
    		{
    		    flash(__('Email Id already Exist.'))->error();
                return back()->withInput();
    		}
	    }
		if(User::where('phone',$request->mobile)->first() != null)
		{
		    flash(__('Phone Number already Exist.'))->error();
            return back()->withInput();
        }
        
        if($request->balance  > 50000){
            flash(__('Your Wallet Balance not more than 50000.'))->error();
            return back()->withInput();
        }
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->mobile;
        $user->user_type = "customer";
        $user->email_verified_at = date('Y-m-d H:m:s');
        if(strlen($request->password) > 0){
        $user->password = Hash::make($request->password);
        } else {
            $user->password = Hash::make(time());
        }
        $user->address = $request->address;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->postal_code = $request->postal_code;
        $bytes = random_bytes(30);
        
        $uniqueid =  Str::uuid()->toString();
      
        $user->alignbook_customer_id = $uniqueid;
        $endPoint = 'SaveUpdate_POS_Customer';
            
            $postJsonData = array(
                	"is_new_mode"=> true,
                	"pos_customer"=> array(
            		"id"=> $uniqueid,
            		"name"=> $request->name,
            		"gender"=> 0,
            		"dob"=> "1753-01-01 00:00:00",
            		"card_no"=> "",
            		"opening_loyality_points"=> 0,
            		"marital_status"=> 0,
            		"anniversary"=> "1753-01-01 00:00:00",
            		"spouse_name"=> "",
            		"shipping_address"=> "",
            		"password"=> "",
            		"referal_code"=> "",
            		"code"=> "",
            		"other_area"=> "",
            		"bank_name"=> "",
            		"ifsc_code"=> "",
            		"account_no"=> "",
            		"fl_names"=> [],
            		"address"=> array(
            			"address"=> "",
            			"country"=> "IN",
            			"state"=> "f49337bc-a6ea-4183-89a4-3cb9c692e361",
            			"state_name"=> "Delhi",
            			"city"=> "2f88a466-fa7c-2049-0279-5ebb025ba5ca",
            			"city_name"=> "New Delhi",
            			"pin"=> "",
            			"phone"=> "9811144018",
            			"email"=> "",
            			"longitude"=> "",
            			"latitude"=> ""
                		),
            		"loyalty_card_type"=>array(
            			"id"=> "",
            			"name"=> ""
            		)
            	)
            );
          
       
        $addCustomeToalignBook =$this->addCustomerInAlignBook(json_encode($postJsonData),$endPoint);
       
        $jsondRepons = json_encode($addCustomeToalignBook,true);
          //dd(json_decode($jsondRepons));
          $respondData = json_decode($jsondRepons);
         if($respondData->ReturnCode == 0){
             if($user->save()){
            $balance = $request->input('balance',0);
            if($balance > 0) {
                $user->balance = $balance;
                $payment_details = $request->input('payment_details', 'AldeBazaar Credit');
                $user->save();
                $wallet = new Wallet;
                $wallet->user_id = $user->id;
                $wallet->amount = $balance;
                $wallet->payment_method = 'AldeBazaar Credit';
                $wallet->payment_details = $payment_details;
                $wallet->add_status = 1;
                $wallet->save();
                $smsMsg = "Hi, The amount of Rs. ".$balance." is credited in your Alde wallet. You can manage your wallet from your My Account section.";
                $message = $smsMsg;
                $smsMsg = rawurlencode($smsMsg);
                $email_subject = "AldeBazaar : Wallet Credit";
                $to = "91".$user->phone;
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        sendSMS($SMS_URL);
                    }
                }
                $emailData = array('email' => $user->email, 'name' => $user->name, 'message' => $message,  'title' => 'Wallet Update', 'email_subject' => $email_subject);
                if(strlen($emailData['email']) > 4) {
                    Mail::send('emails.notification',array('data' => $emailData), function ($m) use ($emailData) {
                            $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                    });
                }
            }
            $Customer = new Customer;
            $Customer->user_id = $user->id;
            if($Customer->save()){
                flash(__('Customer has been inserted successfully'))->success();
                return redirect()->route('customers.index');
            }
        }
         }
       

        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    public function addCustomerInAlignBook($requestJson,$endPoint){
	  
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
        $data = json_decode($response1);
        return $data;
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $customer = Customer::findOrFail(decrypt($id));
        return view('customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {    
        $validatedData = $request->validate([
			'name' => 'required|string|min:4|max:191',
			'mobile' => 'required|digits:10',
			'address' => 'max:300',
			'country' => 'max:30',
			'state' => 'max:30',
			'city' => 'max:30',
            'postal_code' => 'digits:6',
            
		]);
	
        $customer = Customer::findOrFail($id);
        $user = $customer->user;
        $user->name = $request->name;
       // $user->email = $request->email;
        $user->phone = $request->mobile;
        $user->order_cancel_credit = $request->order_cancel_credit;
        if(strlen($request->password) > 0){
            if(strlen($request->password) < 8) {
                flash(__('Password should be more than 8 character.'))->error();
                return back()->withInput();
            }
            $user->password = Hash::make($request->password);
        }
        
        if($request->balance  > 50000){
            flash(__('Your Wallet Balance not more than 50000.'))->error();
            return back()->withInput();
        }
        
        $user->address = $request->address;
        if($user->email_verified_at  == null) {
            $user->email_verified_at = date('Y-m-d H:m:s');
        }
        $user->country = $request->country;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->postal_code = $request->postal_code;
        $bytes = random_bytes(30);
        $bytes = random_bytes(30);
        $uniqueid =   Str::uuid()->toString();
        
        if($user->alignbook_customer_id == '' || $user->alignbook_customer_id==null){
            $user->alignbook_customer_id = $uniqueid;     
        }
       
        $endPoint = 'SaveUpdate_POS_Customer';
            
            $postJsonData = array(
                	"is_new_mode"=> true,
                	"pos_customer"=> array(
            		"id"=> $uniqueid,
            		"name"=> $request->name,
            		"gender"=> 0,
            		"dob"=> "1753-01-01 00:00:00",
            		"card_no"=> "",
            		"opening_loyality_points"=> 0,
            		"marital_status"=> 0,
            		"anniversary"=> "1753-01-01 00:00:00",
            		"spouse_name"=> "",
            		"shipping_address"=> "",
            		"password"=> "",
            		"referal_code"=> "",
            		"code"=> "",
            		"other_area"=> "",
            		"bank_name"=> "",
            		"ifsc_code"=> "",
            		"account_no"=> "",
            		"fl_names"=> [],
            		"address"=> array(
            			"address"=> "",
            			"country"=> "IN",
            			"state"=> "f49337bc-a6ea-4183-89a4-3cb9c692e361",
            			"state_name"=> "Delhi",
            			"city"=> "2f88a466-fa7c-2049-0279-5ebb025ba5ca",
            			"city_name"=> "New Delhi",
            			"pin"=> "",
            			"phone"=> "9811144018",
            			"email"=> "",
            			"longitude"=> "",
            			"latitude"=> ""
                		),
            		"loyalty_card_type"=>array(
            			"id"=> "",
            			"name"=> ""
            		)
            	)
            );
          
       
        $addCustomeToalignBook =$this->addCustomerInAlignBook(json_encode($postJsonData),$endPoint);
         $jsondRepons = json_encode($addCustomeToalignBook,true);
          //dd(json_decode($jsondRepons));
          $respondData = json_decode($jsondRepons);
         if($respondData->ReturnCode == 0){
             if($user->save()) {
            $balance_type = $request->balance_type;
            $balance = $request->input('balance',0);
            $payment_details = $request->payment_details;
            if($balance != "") {
                if($balance > 0) {
                    $wallet = new Wallet;
                    $wallet->user_id = $user->id;
                    $wallet->amount = $balance;
                    if($balance_type == "add") {
                       $wallet->payment_method = 'AldeBazaar Credit'; 
                       $user->balance += $balance;
                        $smsMsg = "Hi, The amount of Rs. ".$balance." is credited in your Alde wallet. You can manage your wallet from your My Account section.";
                        $message = $smsMsg;
                        $smsMsg = rawurlencode($smsMsg);
                        $email_subject = "AldeBazaar : Wallet Credit";
                    } else {
                        $wallet->payment_method = 'AldeBazaar Debit'; 
                        $user->balance -= $balance;
                        $smsMsg = "Hi, The amount of Rs. ".$balance." is debited from Alde wallet. You can manage your wallet from your My Account section.";
                        $message = $smsMsg;
                        $smsMsg = rawurlencode($smsMsg);
                        $email_subject = "AldeBazaar : Wallet Debit";
                    }
                    $user->save();
                    $wallet->payment_details = $payment_details;
                    $wallet->add_status = 1;
                    $wallet->save();
                    $to = "91".$user->phone;
                    if(strlen($to) == 12) {
                        $SMS_URL = env('SMS_URL', null);
                        if($SMS_URL != null) {
                            $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                            sendSMS($SMS_URL);
                        }
                    }
                    $emailData = array('email' => $user->email, 'name' => $user->name, 'message' => $message,  'title' => 'Wallet Update', 'email_subject' => $email_subject);
                    if(strlen($emailData['email']) > 4) {
                        Mail::send('emails.notification',array('data' => $emailData), function ($m) use ($emailData) {
                                $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                        });
                    }
                }
            }
            flash(__('Customer has been updated successfully'))->success();
            return redirect()->route('customers.index');
        }
        }else{
           flash(__('Something went wrong'))->error();
           return back();    
        }
        

        
    }
    
    
    public function ExportExcel(Request $request) {
         return Excel::download(new CustomerExportExcel($request), 'customer_'. time() .'.xlsx');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy(Customer::findOrFail($id)->user->id);
        if(Customer::destroy($id)){
            flash(__('Customer has been deleted successfully'))->success();
            return redirect()->route('customers.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
    }

    public function feedbackForm(){
      
        return view('feedback.index');
    }

    public function savefeedback(Request $request){
        
        $validatedData = $request->validate([
			'name' => 'required',
            'phone' => 'required|digits:10',
            'q1' => 'required',
            'q2' => 'required|max:1000',
            'q3' => 'required|max:1000',
            'q4' => 'required',
            'q5' => 'required|max:200',
            'q6' => 'required',
            'q7' => 'required|max:1000',
            'q8' => 'required|max:1000',
            'q9' => 'required',
            'q10' => 'required|max:200',
            'q12' => 'required',
		]);
        
        $Feedback =  new Feedback;
        $Feedback->name = $request->name;
        $Feedback->email = $request->email;
        $Feedback->mobile = $request->phone;
        $Feedback->Q1 =  $request->q1;
        $Feedback->Q2 =  $request->q2;
        $Feedback->Q3 =  $request->q3;
        $Feedback->Q4 =  $request->q4;
        $Feedback->Q5 =  $request->q5;
        $Feedback->Q6 =  $request->q6;
        $Feedback->Q7 =  $request->q7;
        $Feedback->Q8 =  $request->q8;
        $Feedback->Q9 =  $request->q9;
        $Feedback->Q10 = $request->q10;
        $Feedback->Q11 = 'NA';
        $Feedback->Q12 = $request->q12;
        if($Feedback->save()){
                $feedbackData = Feedback::find($Feedback->id);
                $emailData = array('email' => $feedbackData->email, 'name' => $feedbackData->name,
                'message' => 'Email msg', 
                'email_subject' => 'Feedback form details',
                'q1'=>$feedbackData->Q1,
                'q2'=>$feedbackData->Q2,
                'q3'=>$feedbackData->Q3,
                'q4'=>$feedbackData->Q4,
                'q5'=>$feedbackData->Q5,
                'q6'=>$feedbackData->Q6,
                'q7'=>$feedbackData->Q7,
                'q8'=>$feedbackData->Q8,
                'q9'=>$feedbackData->Q9,
                'q10'=>$feedbackData->Q10,
                'q11'=>'NA',
                'q12'=>$feedbackData->Q12
            );
            
            //Send sms feedback
            
        
            flash(__('Feedback form has been save succesfully.'))->success();
            Mail::send('emails.feedback',array('data' => $emailData), function ($m) use ($emailData) {
                $m->to($emailData['email'],$emailData['name'])->cc('support@aldebazaar.com','awdheshmishra.infotrench@gmail.com')->subject($emailData['email_subject']);
            });
            return back();
        }
        flash(__('Something went wrong'))->error();
        return back();
        
    }

    public function allfeedback(Request $request){
       // dd($request->input('search'));
        
        $feedback = Feedback::orderBy('created_at', 'desc')
                             ->orWhere('name','like','%'.$request->input('search').'%')
                             ->orWhere('email',$request->input('search'))
                             ->orWhere('mobile',$request->input('search'));
        $feedbacks = $feedback->paginate(15);
        return view('feedback.show', compact('feedbacks'));
    }

    public function pharmacykForm(){
        return view('pharmacy.index');        
    }

    public function pharmacykFormAdd(Request $request){
       
        $validatedData = $request->validate([
			'name' => 'required',
            'phone' => 'required|digits:10',
            'email' => 'required',
            'comment' => 'required|max:1000',
            'docter_name'=>'required',
            'address'=>'required'    
        ]);   
        $Pharmacy = new Pharmacy;
        $Pharmacy->name = $request->name;
        $Pharmacy->email = $request->email;
        $Pharmacy->phone = $request->phone;
        $Pharmacy->comment = $request->comment;
        $Pharmacy->docter_name = $request->docter_name;
        $Pharmacy->adress = $request->address;

        if($request->hasFile('prescriptionFile')) {
            //$extension = $request->prescriptionFile->extension();
            //$imageName = time().'.'.$request->prescriptionFile->extension();  
   
       // $request->prescriptionFile->move(public_path('prescription'), "prescription-".time().".".$extension);
            

           // $c = $request->file('prescriptionFile')->storeAs('prescription',"prescription-".time().".".$extension);

           // $Pharmacy->documents = "prescription-".time().".".$extension;
           $extension = $request->prescriptionFile->extension();
                $c = $request->file('prescriptionFile')->storeAs('prescription',"prescription-".time().".".$extension);
                $Pharmacy->documents = "prescription-".time().".".$extension;
        } else {
            $Pharmacy->documents = null;
        }

        if($Pharmacy->save()){
            
            flash(__('Pharmacy details has been save succesfully.'))->success();
            $PharmacyData = Pharmacy::find($Pharmacy->id);
            $emailData = array('email' => $PharmacyData->email, 'name' => $PharmacyData->name,
            'message' => 'GREETINGS!!!

            Thank you for your pharmacy inquiry.
            You will get your order confirmation within 6-8 hours', 
            'email_subject' => 'Pharmacy Order Enquiry' 
        );
    
        flash(__('Your order has been submited. You will get your order confirmation within 6-8 hours'))->success();
        Mail::send('emails.pharmacy',array('data' => $emailData), function ($m) use ($emailData) {
            $m->to($emailData['email'],$emailData['name'])->cc('support@aldebazaar.com','awdheshmishra.infotrench@gmail.com')->subject($emailData['email_subject']);
        });

         //Send sms

         $smsMsg = "Thank you for your pharmacy inquiry.
         You will get your order confirmation within 6-8 hours";
         $smsMsg = rawurlencode($smsMsg);
         $to = "91".$PharmacyData->phone;
        
             $SMS_URL = env('SMS_URL', null);
             if($SMS_URL != null) {
                 $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                 sendSMS($SMS_URL);
             }
        
          return back();     
        }

        flash(__('Something went wrong'))->error();
        return back();
    }

    public function allpharmacy(Request $request){
        $feedback = Pharmacy::orderBy('created_at', 'desc')
            ->orWhere('name','like','%'.$request->input('search').'%')
            ->orWhere('email',$request->input('search'))
            ->orWhere('phone',$request->input('search'));
        $feedbacks = $feedback->paginate(15);
        return view('pharmacy.show', compact('feedbacks'));
        
    }

    public function updatePharmacy(Request $request){
        
        $arrayData = explode('/',$request->input('status'));
        $status = $arrayData[0];
        $updateId = $arrayData[1];

        //Send sms
        $pharmacyDetails = Pharmacy::find($updateId);
        $smsMsg = "Your Pharmacy order has been '.$status.' you will get back to you";
        $smsMsg = rawurlencode($smsMsg);
        $to = "91".$pharmacyDetails->phone;
       
            $SMS_URL = env('SMS_URL', null);
            if($SMS_URL != null) {
                $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
               // dd($SMS_URL);
                sendSMS($SMS_URL);
            }
         

        $affectedRows = Pharmacy::where('id', $updateId)->update(array('status' => $status,'status_historyco'=>$status));
        $affectedRows = Pharmacy::where('id', $updateId)->update(array('status' => $status,'status_historyco'=>$status));    
        // if($status == 'Cancel'){
        //     $affectedRows = Pharmacy::where('id', $updateId)->update(array('status' => $status,'status_historycan'=>$status));
        // }else if($status == 'Confirm'){
        //     $affectedRows = Pharmacy::where('id', $updateId)->update(array('status' => $status,'status_historyco'=>$status));   
        // }else{
        //     return false;
        // }
       
        if($affectedRows){
            
            
            flash(__('Pharmacy order status has been updated succesfully'))->success();
            $PharmacyData = Pharmacy::find($updateId);
            $emailData = array('email' => $PharmacyData->email, 'name' => $PharmacyData->name,
            'message' => 'GREETINGS!!!
             Your Pharmacy order has been '.$status.' you will get back to you 
           ', 
            'email_subject' => 'Pharmacy Order Status'

            
           
                
        );
    
        Mail::send('emails.pharmacy',array('data' => $emailData), function ($m) use ($emailData) {
            $m->to($emailData['email'],$emailData['name'])->cc('support@aldebazaar.com','awdheshmishra.infotrench@gmail.com')->subject($emailData['email_subject']);
        });
       
       
        
        
                
        }
    }

    public function getFeedbackDetails($id){
        
        $Pharmacy = Pharmacy::findOrFail(decrypt($id));
        
        return view('pharmacy.details', compact('Pharmacy'));


    }
}
