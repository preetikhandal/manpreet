<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;
use App\User;
use App\Shop;
use App\Product;
use App\Order;
use App\OrderDetail;
use Carbon\Carbon;
use App\ExpensesCategory;
use App\Expenses;
use App\ExpensesDetails;
use App\ExpensesPayments;
use Illuminate\Support\Facades\Hash;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search = null;
        $vendors = Vendor::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $vendors = $vendors->where('name', 'like', '%'.$sort_search.'%')->orWhere('email', 'like', '%'.$sort_search.'%');
        }
        $vendors = $vendors->paginate(15);
        return view('vendors.index', compact('vendors', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Vendor::where('email', $request->email)->first() != null){
            flash(__('Email already exists!'))->error();
            return back();
        }
         $vendor = new Vendor;
         $vendor->user_id = 0;
         $vendor->company_name = $request->company_name;
         $vendor->address = $request->address;
         $vendor->city = $request->city;
         $vendor->state = $request->state;
         $vendor->pincode = $request->pincode;
         $vendor->email = $request->email;
         $vendor->phone = $request->phone;
         $vendor->gstin = $request->gstin;
         $vendor->pan = $request->pan;
         $vendor->bank_name = $request->input('bank_name',null);
         $vendor->bank_account_name = $request->input('bank_account_name',null);
         $vendor->bank_account_no = $request->input('bank_account_no',null);
         $vendor->bank_ifsc_code = $request->input('bank_ifsc_code',null);
         if($vendor->save()) {
            if($request->user_login == 1) {
                $user = new User;
                $user->name = $request->company_name;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->user_type = "vendor";
                $user->email_verified_at = Carbon::now();
                $user->password = Hash::make($request->password);
                if($user->save()){
                    $vendor->user_id = $user->id;
                    $vendor->save();
                }
            }
            flash(__('Vendor has been inserted successfully'))->success();
            return redirect()->route('vendors.index');
         }
        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function edit($id)
    {
        $vendor = Vendor::findOrFail(decrypt($id));
        return view('vendors.edit', compact('vendor'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vendor = Vendor::findOrFail($id);
        $vendor->company_name = $request->company_name;
        $vendor->address = $request->address;
        $vendor->city = $request->city;
        $vendor->state = $request->state;
        $vendor->pincode = $request->pincode;
        $vendor->email = $request->email;
        $vendor->phone = $request->phone;
        $vendor->gstin = $request->gstin;
        $vendor->pan = $request->pan;
        $vendor->bank_name = $request->input('bank_name',null);
        $vendor->bank_account_name = $request->input('bank_account_name',null);
        $vendor->bank_account_no = $request->input('bank_account_no',null);
        $vendor->bank_ifsc_code = $request->input('bank_ifsc_code',null);
        $vendor->save();
        if($vendor->user_id != 0) {
            $user = User::find($vendor->user_id);
            if(strlen($request->password) > 0){
                $user->password = Hash::make($request->password);
            }
            $user->name = $request->company_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->save();
        }
        
        if($user->save()){
            if($vendor->save()){
                flash(__('Vendor has been updated successfully'))->success();
                return redirect()->route('vendors.index');
            }
        }
        flash(__('Something went wrong'))->error();
        return back();
    }
    
    public function create_login(Request $request) {
        $vendor_id = $request->vendor_id;
        $vendor = Vendor::findOrFail($vendor_id);
        $user = new User;
        $user->password = Hash::make($request->password);
        $user->name = $vendor->company_name;
        $user->email = $vendor->email;
        $user->phone = $vendor->phone;
        $user->save();
        $vendor->user_id = $user->id;
        $vendor->save();
        flash(__('Vendor\'s login has been created successfully'))->success();
        return redirect()->route('vendors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$vendorr = Vendor::findOrFail($id);
        //Product::where('user_id', $vendor->user->id)->delete();
        //Order::where('user_id', $vendor->user->id)->delete();
        //OrderDetail::where('seller_id', $seller->user->id)->delete();
        //User::destroy($seller->user->id);
        /*if(Seller::destroy($id)){
            flash(__('Seller has been deleted successfully'))->success();
            return redirect()->route('sellers.index');
        }
        else {
            flash(__('Something went wrong'))->error();
            return back();
        }*/
    }

    /*public function active_vendor($id)
    {
        $vendor = Vendor::findOrFail($id);
        $vendor->status = 1;
        if($vendor->save()){
            flash(__('Vender status has been changed successfully'))->success();
            return redirect()->route('vendors.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
    }*/
    
    public function profile_modal(Request $request)
    {
        $vendor = Vendor::findOrFail($request->id);
        return view('vendors.profile_modal', compact('vendor'));
    }

    public function active_vendor(Request $request)
    {
        $vendor = Vendor::findOrFail($request->id);
        $vendor->status = $request->status;
        if($vendor->save()){
            return 1;
        }
        return 0;
    }
    
    
    public function vendor_expenses() {
        $ExpensesCategory=ExpensesCategory::get();
		$Expenses=Expenses::where('vendor_id','!=',null)->where('PaymentStatus','Unpaid')->paginate( env('RECORDPERPAGE', '10'));
		$data = array("categories"=>$ExpensesCategory,"Expenses"=>$Expenses);
		return view('vendors.expenses',$data);
    }
    
}
