<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use App\User;
use App\Seller;
use Auth;
use DB;
use Mail;
use App\Cashback;
use App\Wallet;

class PurchaseHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where('user_id', Auth::user()->id)->orderBy('code', 'desc')->paginate(9);
        return view('frontend.purchase_history', compact('orders'));
    }

    public function digital_index()
    {
        $orders = DB::table('orders')
                        ->orderBy('code', 'desc')
                        ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                        ->join('products', 'order_details.product_id', '=', 'products.id')
                        ->where('orders.user_id', Auth::user()->id)
                        ->where('products.digital', '1')
                        ->where('order_details.payment_status', 'paid')
                        ->select('order_details.id','order_details.code')
                        ->paginate(1);
        return view('frontend.digital_purchase_history', compact('orders'));
    }

    public function purchase_history_details(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->delivery_viewed = 1;
        $order->payment_status_viewed = 1;
        $order->save();
        return view('frontend.partials.order_details_customer', compact('order'));
    }
    
    public function purchase_order_cancel(Request $request) {
        $order_id = $request->order_id;

        $order = Order::findOrFail($request->order_id);
        $orderDetails =  OrderDetail::where('order_id',$request->order_id)->get();
        
        $sellerId = $orderDetails[0]->seller_id;
     
        
        if(isset($sellerId)){
             $orderCreditLimit = User::findOrFail($sellerId);
            if($orderCreditLimit->user_type  == 'seller'){
                $sellerCreditLimit = Seller::where('user_id',$sellerId)->get();
                $sellerCreditLimit[0]->order_cancel_credit;
                $credit = $sellerCreditLimit[0]->order_cancel_credit;
            if($credit >= 1 ){
                $lastCredit = $credit -1;
                $affected = DB::table('sellers')
                  ->where('user_id', $sellerId)
                  ->update(['order_cancel_credit' => $lastCredit]);
            }else{
                
                  $finalPenalty = '';
                  $minimumPenaltiyAmount = 50;
                  $totalOrderAmount =  $order->grand_total;
                  $perectPenalty = $totalOrderAmount*5/100;
                 
                  if($perectPenalty >  50){
                      $finalPenalty = $perectPenalty;
                  }else{
                      $finalPenalty = $minimumPenaltiyAmount;
                  }
                $wallet = new Wallet;
                $wallet->user_id = $sellerId;
                $wallet->amount = $finalPenalty;
                $wallet->payment_status = 'Debit';
                $wallet->payment_method = 'Wallet Payment';
                $wallet->payment_details = 'Cancel order used for order #'. $order->code;
                $wallet->add_status = 1;
                $wallet->order_id =$order->id;
                $wallet->save();
                    
                
            }
            
        }
             
        }
       
  
       
        
        
        
        $shipping = json_decode($order->shipping_address);
        if($order->payment_status == 'paid') {
            $status = "cancel_refund";
        } else {
            $status = "cancel";
        }
        foreach($order->orderDetails as $key => $orderDetail) {
            if($status == 'cancel_refund') {
                $orderDetail->delivery_status = 'processing_refund';
            } else {
                $orderDetail->delivery_status = $status;
            }
            $orderDetail->save();
        }
       // Hey, {buyer name} ! Your {Order No. 20210324-16072156} , has been cancelled. We regret inconvenience caused to you. In case it's a prepaid order, Refund will be initiated in your Alde Bazaar Wallet. For any assistance WhatsApp: 80068-80055 or call 9776-9776-12
        if ($status == 'cancel_refund') {
            $this->DeleteCashBack($order->id);
            $smsMsg = "Hey, ". $order->user->name ." ! Your Order With. ".$order->code.", has been cancelled. We regret inconvenience caused to you. In case it's a prepaid order, Refund will be initiated in your Alde Bazaar Wallet. For any assistance WhatsApp: 80068-80055 or call 9776-9776-12.";
            $message = $smsMsg;
            $email_subject = "AldeBazaar : Order Cancelled";
        } elseif ($status == 'cancel') {
            $this->DeleteCashBack($order->id);
            $smsMsg = "Your Order No. ".$order->code.", has been cancelled. We regret inconvenience caused to you. For any assistance contact +91 9776-9776-12 or WhatsApp: 80068-80055.";
            $message = $smsMsg;
            $email_subject = "AldeBazaar : Order Cancelled";
        }

        
        if($smsMsg != null) {
            $smsMsg = rawurlencode($smsMsg);
            $to = "91".$shipping->phone;
            if(strlen($to) == 12) {
                $SMS_URL = env('SMS_URL', null);
                if($SMS_URL != null) {
                   // echo $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                    //file_get_contents($SMS_URL);
                }
            }
        }
       if(isset($message)) {
        if($message != null) {
            $emailData = array('email' => $order->user->email, 'name' => $order->user->name, 'message' => $message, 'email_subject' => $email_subject);
            Mail::send('emails.order_notification',array('data' => $emailData), function ($m) use ($emailData) {
                    $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
            });
       }
       }
       
       flash(__('Your order has been cancelled successfully.'))->success();
       return back();
    }

    /***
     * Delete CashBack Croosponding Order id
     * 
     */
    public function DeleteCashBack($orderId){
        
        
       // DB::table('cashbacks')->where('order_id', $orderId)->delete();
         $affected = DB::table('cashbacks')
              ->where('order_id', $orderId)
              ->update(['status' => 'cancel']);
               $this->DeleteWalletCredit($orderId);
    } 
    
    public function DeleteWalletCredit($orderId){
        
       $affected = DB::table('wallets')
              ->where('order_id', $orderId)
              ->update(['payment_status' => 'Credit']);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
