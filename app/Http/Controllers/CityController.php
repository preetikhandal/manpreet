<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use File;
use App\Blog;
use App\State;
use App\City;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Storage;
use Illuminate\Support\Str;

class CityController extends Controller
{
 
	public function index(Request $request)
	{   
		$sort_search = null;
		$state = State::all();
        $citys = City::orderBy('created_at', 'desc')->get();
        //$citys = $citys->$Blog->paginate(15);
        return view('City.index', compact('citys', 'state'));
    }
	public function create()
    {   	$state = State::all();
         
        return view('City.create',compact('state'));
    }
	public function store(Request $request)
    {   
        $getAlignBookStateparentId = State::find($request->stateid);
       
        $validatedData = $request->validate([
			'stateid' => 'required',
			'city' => 'required',
			
		]);
	
	 
        $uniqueid =  Str::uuid()->toString();
		$City = new City;
		$City->city_id = $uniqueid;
		$City->state_id= $request->stateid;
		$City->parent_id = $getAlignBookStateparentId->align_book_state_id;
		$City->name = $request->input('city');
		$City->code = $request->input('code');
		$City->status = 'active';
		$endPoint = 'SaveUpdate_City';
	
		$postArray = array(
		    "is_new_mode"=>true,
		    "master"=>array(
		        "id"=>$uniqueid,
		        "name"=>$request->input('city'),
		        "inactive"=>'false',
		        "parent"=>$getAlignBookStateparentId->align_book_state_id,
		        "sequence"=>0,
		        "tally_id"=>0,
		        "code_config"=>array(
		            "code_prefix"=>"",
		            "code_no"=>$request->input('code'),
		            ),
		        )
		    );
		   
		  
		$alignResponse = $this->addCityInAlignBook(json_encode($postArray),$endPoint);
	
		 $jsondRepons = json_encode($alignResponse,true);
          //dd(json_decode($jsondRepons));
          $respondData = json_decode($jsondRepons);
         if($respondData->ReturnCode == 0){
             	if($City->save()){
        			Cache::forget('City');
        			flash(__('City added successfully'))->success();
        			return redirect()->route('city.index');
        		}
         }
	
		flash(__('Something went wrong'))->error();
        return back();
		
	}
	
	public function addCityInAlignBook($requestJson,$endPoint){
	   
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
       return $data = json_decode($response1);
	}
	
	public function EditorUploadImg(Request $request)
    {
        if($request->hasFile('upload')) {
			$originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
			$img_path = 'uploads/blog';
            //$request->file('upload')->move($img_path, $fileName);
             
		
			$thumbnail = $fileName;
			$img = Image::make($request->upload);
			
			$img->save(base_path('public/')."temp/".$thumbnail);
			
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/blog/'.$thumbnail);
				
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/blog', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
			}
			$url = asset($img_path."/".$fileName);
			unlink(base_path('public/').'temp/'.$thumbnail);
			
			$CKEditorFuncNum = $request->input('CKEditorFuncNum');
			
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }
	
	public function edit($id)
	{
		$Blog = Blog::find($id);
		$data = array("Blog" => $Blog);
		return view('Blog.edit', $data);
	}
	 
	public function update(Request $request, $id)
	{
	    $validatedData = $request->validate([
			'BlogTitle' => 'required|min:4|max:500',
			'BlogSummary' => 'required|min:4|max:1000',
			'BlogContent' => 'required|min:50',
			'SEO_metatags' => 'max:300',
			'SEO_metaDescription' => 'max:500'
		]);
		if ($request->input('URLSlug') == null) {
            $URLSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->input('BlogTitle')));
        }
        else{
            $URLSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->input('URLSlug')));
        }
		$count = Blog::where('id', '!=', $id)->where('URLSlug', $URLSlug)->count();
		if($count > 0)
		{
			flash(__('Blog Slug already in use, Use Different slug for this Blog.'))->error();
            return back(); 
		}
		$Blog = Blog::find($id);
		$Blog->BlogTitle = $request->input('BlogTitle');
		$Blog->BlogSummary = $request->input('BlogSummary');
		$Blog->BlogContent = $request->input('BlogContent');
		$Blog->SEO_metatags = $request->input('SEO_metatags');
		$Blog->SEO_metaDescription = $request->input('SEO_metaDescription');
		$Blog->URLSlug = $URLSlug;
		if($request->hasFile('FeaturedImage')){
            $extension = $request->FeaturedImage->extension();
            $thumbnail = $URLSlug."_".time().".".$extension;
            $img = Image::make($request->FeaturedImage);
			$width = Image::make($request->FeaturedImage)->width();
			$height = Image::make($request->FeaturedImage)->height();
			
			if($width != 1200 && $height != 600){
				$img->resizeCanvas(1200, 600, 'center', false, 'fff');
			}
            $img->save(base_path('public/')."temp/".$thumbnail);
           // ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);

           if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/blog/'.$thumbnail);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/blog', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
            }
			
			$oldImg = $Blog->FeaturedImage;
			if(!empty($oldImg))
			{
				if(File::exists($oldImg)) {
				   unlink($oldImg);
				}
			}
            $Blog->FeaturedImage = "uploads/blog/".$thumbnail;
            unlink(base_path('public/').'temp/'.$thumbnail);
        }
		
		if($Blog->save()){
			Cache::forget('Blog');
			flash(__('Blog details update successfully'))->success();
			return redirect()->route('blog.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
	}
	public function destroy($id)
    {
		$Blog = Blog::find($id);
		if(File::exists($Blog->FeaturedImage)) {
			if(env('FILESYSTEM_DRIVER') == "local") {
				if(file_exists(base_path('public/').$Blog->FeaturedImage))
				unlink(base_path('public/').$Blog->FeaturedImage);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->delete($Blog->FeaturedImage);
			}
			$Blog->FeaturedImage = null;
		}
		if($Blog->delete()){
			Cache::forget('Blog');
			flash(__('Blog has been deleted successfully'))->success();
			return redirect()->route('blog.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
	}
	
	 
}
