<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductStock;
use App\Category;
use App\Language;
use Auth;
use App\SubSubCategory;
use Session;
use ImageOptimizer;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
use App\OrderDetail;
use App\ProductsBulkDelete;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Excel;
use App\Review;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_products(Request $request)
    {
        $type = 'In House';
        $col_name = null;
        $query = null;
        $sort_search = null;
        $trashed = $request->input('trashed', 0);

        $products = Product::where('added_by', 'admin');

        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%')->orWhere('product_id','like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }
        $products = $products->where('digital', 0);
        
        if($trashed == 1) {
            $products = $products->onlyTrashed();
            $products = $products->orderBy('deleted_at', 'desc');
        } else {
            $products = $products->orderBy('created_at', 'desc');
        }

        $products = $products->paginate(15);

        return view('products.index', compact('products','type', 'col_name', 'query', 'sort_search', 'trashed'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function seller_products(Request $request)
    {  
        $col_name = null;
        $query = null;
        $seller_id = null;
        $sort_search = null;
        $trashed = $request->input('trashed', 0);
        $products = Product::where('added_by', 'seller');
        if ($request->has('user_id') && $request->user_id != null) {
            $products = $products->where('user_id', $request->user_id);
            $seller_id = $request->user_id;
        }
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%')->orWhere('product_id',$request->search);
            $sort_search = $request->search;
        }
        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }
        if($trashed == 1) {
            $products = $products->onlyTrashed();
            $products = $products->orderBy('deleted_at', 'desc');
        } else {
            $products = $products->orderBy('created_at', 'desc');
        }
        $products = $products->paginate(15);
        $type = 'Seller';

        return view('products.index', compact('products','type', 'col_name', 'query', 'seller_id', 'sort_search', 'trashed'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function seller_products_verified(Request $request)
    {   $url = $request->fullURL();
        $parseUrl = parse_url($url);
        $urlPath = explode('/',$parseUrl['path']);
        $sellerId = $urlPath[4];
        $col_name = null;
        $query = null;
        $seller_id = null;
        $sort_search = null;
        $trashed = $request->input('trashed', 0);
         if(isset($sellerId)){
              $products = Product::where(['added_by'=> 'seller','user_id'=>$sellerId,'published'=>1]);
         }else{
              $products = Product::where(['added_by'=> 'seller','user_id'=>$sellerId]);
         }
       
         
        if ($request->has('user_id') && $request->user_id != null) {
            $products = $products->where('user_id', $request->user_id);
            $seller_id = $request->user_id;
        }
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%')->orWhere('product_id',$request->search);
            $sort_search = $request->search;
        }
        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }
        if($trashed == 1) {
            $products = $products->onlyTrashed();
            $products = $products->orderBy('deleted_at', 'desc');
        } else {
            $products = $products->orderBy('created_at', 'desc');
        }
        $products = $products->paginate(15);
        $type = 'Seller';
        
        return view('products.index', compact('products','type', 'col_name', 'query', 'seller_id', 'sort_search', 'trashed'));
    }
     
     
    public function vendor_products(Request $request, $id)
    {
        $type = 'In House';
        $col_name = null;
        $query = null;
        $sort_search = null;
        $vendor_user_id = decrypt($id);
        $user_id = $request->input('user_id',null);
        
        $products = Product::where('added_by', 'vendor');
        
        if($user_id == null && $vendor_user_id > 0) {
            $products = $products->where('user_id', $vendor_user_id);
        } else {
            $vendor_user_id = 0;
        }
        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%')->orWhere('product_id',$request->search);
            $sort_search = $request->search;
        }

        $products = $products->where('digital', 0)->orderBy('created_at', 'desc')->paginate(15);

        return view('products.vendor_products', compact('products','type', 'col_name', 'query', 'sort_search', 'vendor_user_id'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_assign(Request $request)
    {
        
        $user_id = $request->user_id;
        $products = $request->products;
        foreach($products as $P) {
            $product = Product::find($P);
            $product->user_id = $user_id;
            $product->added_by = 'vendor';
            $product->save();
            if(env('CACHE_DRIVER') == 'redis') {
                Cache::tags(['Product'])->forever('Product:'.$P, $product);
            } else {  
                Cache::forever('Product:'.$P, $product);
            }
        }
        
        flash(__('Product has been assigned successfully'))->success();
        return back();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_remove($product_id)
    {
        $product = Product::find($product_id);
        $product->user_id = Auth::id();
        $product->added_by = 'admin';
        $product->save();
        if(env('CACHE_DRIVER') == 'redis') {
                Cache::tags(['Product'])->forever('Product:'.$product_id, $product);
        } else {  
            Cache::forever('Product:'.$product_id, $product);
        }
        
        flash(__('Product has been un-assigned successfully'))->success();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validatedData = $request->validate([
			'product_id' => 'required|min:2|max:30',
			'name' => 'required|string|min:4|max:200',
			'category_id' => 'required',
			'subcategory_id' => 'required',
			'subsubcategory_id' => 'required',
			'brand_id' => 'required',
			'unit' => 'required|min:1|max:20',
			'tags' => 'max:20',
			'video_link' => 'max:100',
			'pdf' => 'file|max:10240|mimes:pdf',
			'discount' => 'required'
		]);
		
        //$productName =ucwords(strtolower($request->name));
        $productName =$request->name;
        $productId=$request->product_id;
		//check 
		$prod_count=Product::where('product_id',$productId)->count();
		if($prod_count!=0){
           
			flash(__('Product Id already exists'))->error();
           // return back();
		}
		
		   
        
        $product = new Product;
        
        $product->product_id = $productId;
       
        $product->name = $productName;
        $product->added_by = $request->added_by;
        $product->min_purchased_quantity = $request->min_purchased_quantity;
        if(Auth::user()->user_type == 'seller'){
            $product->user_id = Auth::user()->id;
        }
        else{
            $product->user_id = \App\User::where('user_type', 'admin')->first()->id;
        }
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->subsubcategory_id = $request->subsubcategory_id;
        $product->brand_id = $request->brand_id;
        $product->current_stock = $request->current_stock;
        $product->barcode = $request->barcode;
        $product->gst_tax = $request->gstpercent;
      

        $photos = array();
        if($request->hasFile('photos')){
            foreach ($request->photos as $photo) {
				$extension = strtolower($photo->extension());
		
				if($extension == "jpg" or $extension == "png" or $extension == "gif" or  $extension == "jpeg")
				{
					$mainimage = Str::slug($productName,"-").time().".".$extension;
					$img = Image::make($photo);
					$width = $img->width();
					$height = $img->height();
					if($width == 800 && $height == 800) {
        			    $photo->move(base_path('public/')."temp/",$mainimage);
        			} else {
            			if($width <= 800 && $height <= 800) {
    						$img->resizeCanvas(800, 800, 'center', false, 'fff');
    					} else {
    						 $img->resize(800, 800, function ($constraint) {
    							$constraint->aspectRatio();
    						});
    						$img->resizeCanvas(800, 800, 'center', false, 'fff');
    					}
    					$img->save(base_path('public/')."temp/".$mainimage, 100);
        			}
					if(env('FILESYSTEM_DRIVER') == "local") {
						copy(base_path('public/').'temp/'.$mainimage, base_path('public/').'uploads/products/photos/'.$mainimage);
					} else {
						Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/photos', new \Illuminate\Http\File(base_path('public/').'temp/'.$mainimage), $mainimage);
					}
					sleep(1);
					array_push($photos, 'uploads/products/photos/'.$mainimage);
					unlink(base_path('public/').'temp/'.$mainimage);
				} else {
					flash(__('Invalid file type. Please upload jpg/png/jpeg/gif format files only.'))->error();
					if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
						return redirect()->route('products.admin');
					} else {
						return redirect()->route('seller.products');
					}
				}
			}
			$product->photos = json_encode($photos);
        }
		/*
		if($request->hasFile('photos')){
            foreach ($request->photos as $key => $photo) {
				ImageOptimizer::optimize($photo);
                $path = $photo->store('uploads/products/photos');
                array_push($photos, $path);
            }
			$product->photos = json_encode($photos);
        }
		*/
		// if($request->hasFile('thumbnail_img')){
			// ImageOptimizer::optimize($request->thumbnail_img);
            // $product->thumbnail_img = $request->thumbnail_img->store('uploads/products/thumbnail');
        // }
		
		if($request->hasFile('thumbnail_img')){
            $extension = $request->thumbnail_img->extension();
            $thumbnail = Str::slug($product->name,"-")."thumb".time().".".$extension;
            $img = Image::make($request->thumbnail_img);
			$width = $img->width();
			$height = $img->height();
			if($width == 200 && $height == 200) {
			    $request->thumbnail_img->move(base_path('public/')."temp/",$thumbnail);
			} else {
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$thumbnail, 100);
			}
            //ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/products/thumbnail/'.$thumbnail);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/thumbnail', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
            }
            $product->thumbnail_img = "uploads/products/thumbnail/".$thumbnail;
            unlink(base_path('public/').'temp/'.$thumbnail);
        }
		
		if($request->hasFile('featured_img')){
            $extension = $request->featured_img->extension();
            $featured = Str::slug($product->name,"-")."featured".time().".".$extension;
            $img = Image::make($request->featured_img);
			$width = $img->width();
			$height = $img->height();
            
            if($width == 200 && $height == 200) {
			    $request->featured_img->move(base_path('public/')."temp/",$featured);
			} else {
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$featured, 100);
			}
			
            //ImageOptimizer::optimize(base_path('public/')."temp/".$featured);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$featured, base_path('public/').'uploads/products/featured/'.$featured);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/featured', new \Illuminate\Http\File(base_path('public/').'temp/'.$featured), $featured);
            }
            $product->featured_img = "uploads/products/featured/".$featured;
            unlink(base_path('public/').'temp/'.$featured);
        }
		
		if($request->hasFile('flash_deal_img')){
            $extension = $request->flash_deal_img->extension();
            $flash_deal = Str::slug($product->name,"-")."flash_deal".time().".".$extension;
            $img = Image::make($request->flash_deal_img);
			$width = $img->width();
			$height = $img->height();
            if($width == 200 && $height == 200) {
			    $request->flash_deal_img->move(base_path('public/')."temp/",$flash_deal);
			} else {
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$flash_deal, 100);
			}
            
            //ImageOptimizer::optimize(base_path('public/')."temp/".$flash_deal);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$flash_deal, base_path('public/').'uploads/products/flash_deal/'.$flash_deal);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/flash_deal', new \Illuminate\Http\File(base_path('public/').'temp/'.$flash_deal), $flash_deal);
            }
            $product->flash_deal_img = "uploads/products/flash_deal/".$flash_deal;
            unlink(base_path('public/').'temp/'.$flash_deal);
        }
		
		// if($request->hasFile('featured_img')){
			// ImageOptimizer::optimize($request->featured_img);
            // $product->featured_img = $request->featured_img->store('uploads/products/featured');
        // }
		
		
		// if($request->hasFile('flash_deal_img')){
			// ImageOptimizer::optimize($request->flash_deal_img);
            // $product->flash_deal_img = $request->flash_deal_img->store('uploads/products/flash_deal');
        // }
    
		
        $product->unit = $request->unit;
        $product->tags = implode('|',$request->tags);
        $product->description = $request->description;
        $product->video_provider = $request->video_provider;
        $product->video_link = $request->video_link;
        $product->unit_price = $request->unit_price;
        $product->purchase_price = $request->purchase_price;
        $product->tax = $request->tax;
        $product->tax_type = $request->tax_type;
        $product->discount = $request->discount;
        $product->discount_type = $request->discount_type;
        $product->shipping_type = $request->shipping_type;
        if($request->shipping_type == 'free'){
            $product->shipping_cost = 0;
        }
        elseif ($request->shipping_type == 'flat_rate') {
            $product->shipping_cost = $request->flat_shipping_cost;
        }
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        if($request->hasFile('meta_img')){
            // $product->meta_img = $request->meta_img->store('uploads/products/meta');
            // ImageOptimizer::optimize(base_path('public/').$product->meta_img);
			
			$extension = $request->meta_img->extension();
            $meta_img = Str::slug($product->name,"-")."meta_img".time().".".$extension;
            $img = Image::make($request->meta_img);
            $width = $img->width();
			$height = $img->height();
			
			if($width == 200 && $height == 200) {
			    $request->meta_img->move(base_path('public/')."temp/",$meta_img);
			} else {
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$meta_img, 100);
			}
			
            //ImageOptimizer::optimize(base_path('public/')."temp/".$meta_img);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$meta_img, base_path('public/').'uploads/products/meta/'.$meta_img);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/meta', new \Illuminate\Http\File(base_path('public/').'temp/'.$meta_img), $meta_img);
            }
            $product->meta_img = "uploads/products/meta/".$meta_img;
            unlink(base_path('public/').'temp/'.$meta_img);
			
        }

        if($request->hasFile('pdf')){
            $product->pdf = $request->pdf->store('uploads/products/pdf');
        }
        

        $product->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $productName)).'-'.str_random(5));

        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
        }
        else {
            $colors = array();
            $product->colors = json_encode($colors);
        }

        $choice_options = array();

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;

                $item['attribute_id'] = $no;
                $item['values'] = explode(',', implode('|', $request[$str]));

                array_push($choice_options, $item);
            }
        }

        if (!empty($request->choice_no)) {
            $product->attributes = json_encode($request->choice_no);
        }
        else {
            $product->attributes = json_encode(array());
        }

        $product->choice_options = json_encode($choice_options);

        //$variations = array();
        $product->variation = json_encode(array());
        $product->variation_order = json_encode(array());
        $product->variation_combinations = json_encode(array());
        $product->variation_product = json_encode(array());
        $product->published = 0;
         /***************ADD ITEAM IN ALIGN BOOK*********************************/
        
       
        
        $uniqueid =  Str::uuid()->toString();
       
        $product->alignbook_pro_id = $uniqueid;
        $postJsonData = array(
            	"is_new_mode"=> true,
            	"item_information"=> array(
            		"id"=> $uniqueid,
            		"name"=> $request->name,
            		"item_group_id"=> "a0931c2e-4d27-4b1b-bf6e-b63f8b6db2c4",
            		"item_category_id"=> "",
            		"type"=> 0,
            		"item_mode"=> 0,
            		"inactive"=> false,
            		"separate_pack_unit"=> false,
            		"input_dimension"=> false,
            		"gst_input_not_applicable"=> false,
            		"sub_item_applicable"=> false,
            		"stock_unit_id"=> "b9239fc8-bc7b-499f-9aca-d0635741295e",
            		"pack_unit_id"=> "",
            		"stock_vs_pack"=> 1,
            		"rate_per"=> 0,
            		"procurement"=> 0,
            		"garment_item_type"=> 0,
            		"gst_classification_id"=> "0eeb53e3-aeac-4a4f-870e-ab5d1d275081",
            		"vat_tax_id"=> "",
            		"sales_description"=> "XYZ",
            		"barcode"=> $product->product_id,
            		"markup_percentage"=> 0,
            		"sales_rate"=> $request->unit_price,
            		"mrp"=> 0,
            		"min_rate"=> 0,
            		"sales_misc_1"=> 0,
            		"sales_misc_2"=> 0,
            		"sales_misc_3"=> 0,
            		"sales_gl_id"=> "ee113c08-68fb-4bbb-993a-6309a4937fb3",
            		"purchase_description"=> $request->description,
            		"purchase_rate"=> 0,
            		"purchase_misc_1"=> 0,
            		"purchase_misc_2"=> 0,
            		"purchase_misc_3"=> 0,
            		"purchase_gl_id"=> "5dbdad5a-6b2c-463d-a78f-ee0cf45c6dfa",
            		"minimum_level"=> 0,
            		"inventory_gl_id"=> "",
            		"specification_template_id"=> "",
            		"free_template_id"=> "",
            		"suggested_template_id"=> "",
            		"rm_item_id"=> "",
            		"batch_wise_inventory"=> false,
            		"batch_wise_rate"=> false,
            		"serial_tracking"=> false,
            		"predefined"=> false,
            		"ask_udf_in_document"=> false,
            		"depreciation_gl_id"=> "",
            		"fca_622_id"=> "",
            		"fca_mdb_id"=> "",
            		"tally_id"=> "",
            		"salt"=> "",
            		"rack_box"=> "",
            		"metal_purity"=> 0,
            		"udf_list"=> ["", "", "", "", ""],
            		"itemset_template_list"=> [],
            		"attribute_list"=> [array(
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		),array (
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		)],
            		"fl_names"=> [],
            		"code_config"=>array (
            			"code"=> '0000000',
            			"code_no"=> 0,
            			"code_prefix"=> ""
            		),
            		"bundles"=> [],
            		"mapping_codes"=> []
            	)
            );
          $endPoints = 'SaveUpdate_Item';
          
        $addIteam = $this->addUpdateIteam(json_encode($postJsonData),$endPoints);
         $jsondRepons = json_encode($addIteam,true);
          //dd(json_encode($postJsonData),json_decode($jsondRepons));
          $respondData = json_decode($jsondRepons);
          //dd($postJsonData,$respondData);
         if($respondData->ReturnCode != 0){
             	flash(__('Product is not uploading in align book systeam'))->error();
             	return back();
         }

        $product->save();

        //combinations start
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('|',$request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        //Generates the combinations of customer choice options
        $combinations = combinations($options);
        if(count($combinations[0]) > 0){
            $product->variant_product = 1;
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                            $color_name = \App\Color::where('name', $item)->first()->name;
                            $str .= $color_name;
                        }
                        else{
                            $str .= str_replace(' ', '', $item);
                        }
                    }
                }
                // $item = array();
                // $item['price'] = $request['price_'.str_replace('.', '_', $str)];
                // $item['sku'] = $request['sku_'.str_replace('.', '_', $str)];
                // $item['qty'] = $request['qty_'.str_replace('.', '_', $str)];
                // $variations[$str] = $item;

                $product_stock = ProductStock::where('product_id', $product->id)->where('variant', $str)->first();
                if($product_stock == null){
                    $product_stock = new ProductStock;
                    $product_stock->product_id = $product->id;
                }

                $product_stock->variant = $str;
                $product_stock->price = $request['price_'.str_replace('.', '_', $str)];
                $product_stock->sku = $request['sku_'.str_replace('.', '_', $str)];
                $product_stock->qty = $request['qty_'.str_replace('.', '_', $str)];
                $product_stock->save();
            }
        }
        //combinations end

        //$product->variations = json_encode($variations);

        // foreach (Language::all() as $key => $language) {
        //     $data = openJSONFile($language->code);
        //     $data[$product->name] = $product->name;
        //     saveJSONFile($language->code, $data);
        // }

	    $product->save();
		if(env('CACHE_DRIVER') == 'redis') {
                Cache::tags(['Product'])->forever('Product:'.$product->id, $product);
        } else {  
            Cache::forever('Product:'.$product->id, $product);
        }
		$prefix = env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_');
        Redis::set('ProductID:'.$product->id.':ProductTag:'.strtolower($product->tags), $product->id);
        Redis::set('ProductID:'.$product->id.':ProductName:'.strtolower($product->name), $product->id);
        Redis::set('ProductID:'.$product->id.':ProductSalt:'.strtolower($product->salt), $product->id);
		
        flash(__('Product has been inserted successfully'))->success();
        if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
            return redirect()->route('products.admin');
        }
        else{
            return redirect()->route('seller.products');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    public function productURL(Request $request) {
        $product_id = $request->product_id;
        $product = Product::find($product_id)->slug;
        $url = route('product', $product);
        return $url;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_product_edit($id)
    {
        $product = Product::findOrFail(decrypt($id));
        //dd(json_decode($product->price_variations)->choices_0_S_price);
        $tags = json_decode($product->tags);
        $categories = Category::all();
        return view('products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function seller_product_edit($id)
    {
        $product = Product::findOrFail(decrypt($id));
        //dd(json_decode($product->price_variations)->choices_0_S_price);
        $tags = json_decode($product->tags);
        $categories = Category::all();
        return view('products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 
	 
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
       
        //dd($request->input(),$product);
        $validatedData = $request->validate([
			'product_id' => 'required|min:2|max:30',
			'name' => 'required|string|min:4|max:200',
			'category_id' => 'required',
			'subcategory_id' => 'required',
			'subsubcategory_id' => 'required',
			'brand_id' => 'required',
			'unit' => 'required|min:1|max:20',
			'tags' => 'max:20',
			'video_link' => 'max:100',
			'pdf' => 'file|size:4096|mimes:pdf',
			'discount' => 'required'
		]);
        $variation_product = json_decode($product->variation_product,true);
        if($variation_product == null) {
            $variation_product = array();
        }
        
        foreach($variation_product as $v) {
            if($v > 0) {
                $p = product::find($v);
                if($p != null) {
                    $p->variation_show_hide = 1;
                    $p->variation = json_encode(array());
                    $p->variation_order = json_encode(array());
                    $p->variation_combinations = json_encode(array());
                    $p->variation_product = json_encode(array());
                    $p->colors = json_encode(array());
                    $p->save();
                }
            }
        }
        
        $colors = $request->colors;
        $colors_active = $request->colors_active;
        $choice_attributes_ids = $request->choice_attributes;
        $choice = $request->choice;
        $variation = $request->variation;
        $product_variation_select = $request->product_variation_select;
        
        if(!isset($product_variation_select)){
            $product_variation_select=array();
        }
        $product_variation_show = $request->product_variation_show;
        if(!isset($product_variation_show)){
            $product_variation_show=array();
        }
        $choice_options = array();
        $options = array();
        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;
                $choice_options[$no] = $request[$str];
            }
        }
        $variation = array();
        $variation_order = array();
        if($colors_active) {
            $variation['Colors'] = $colors;
            $variation_order[] = 'Colors';
            array_push($options, $request->colors);
        }
        if(!isset($choice_attributes_ids)){
            $choice_attributes_ids=array();
        }
        foreach ($choice_attributes_ids as $aid) {
            $name = \App\Attribute::find($aid)->name;
            $variation[$name] = explode(',',$choice_options[$aid]);
            $variation_order[] = $name;
            array_push($options, $variation[$name]);
        }
        //dd($variation);
        $combinations = combinations($options);
        foreach($combinations as $key => $c) {
            $combinations[$key] = implode("-", $c);
        }
        $product_variation = array();
        if(count($product_variation_select) > 0) {
            foreach($combinations as $c) {
                if(isset($product_variation_select[$c])) {
                 $product_variation[$c] = $product_variation_select[$c];
                 }
            }
        }
        //dd(json_encode($variation)); //{"Colors":["Red","White"],"Size":["S","M","L"]}
        //dd(json_encode($variation_order));//["Colors","Size"]
        //dd(json_encode($combinations)); // ["Red-S","Red-M","Red-L","White-S","White-M","White-L"]
        
        $variation = json_encode($variation); //{"Colors":["Red","White"],"Size":["S","M","L"]}
        $variation_order = json_encode($variation_order); //["Colors","Size"]
        $variation_combinations = json_encode($combinations); //["Red-S","Red-M","Red-L","White-S","White-M","White-L"]
        $variation_product = json_encode($product_variation); //{"Blue-S":"11","Blue-M":"12","White-S":"13","White-M":"17"}

        if(count($product_variation_select) > 0) {
        foreach($combinations as $c) {
            if(isset($product_variation_select[$c])) {
                if($product_variation_select[$c] > 0) {
                    $p = product::find($product_variation_select[$c]);
                    $p->variation_show_hide = $product_variation_show[$c];
                    $p->variation = $variation;
                    $p->variation_order = $variation_order;
                    $p->variation_combinations = $variation_combinations;
                    $p->variation_product = $variation_product;
                    if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                        $p->colors = json_encode($request->colors);
                    }
                    else {
                        $colors = array();
                        $p->colors = json_encode($colors);
                    }
                    $p->save();
                }
            }
        }
        }
        
        //$productName =ucwords(strtolower($request->name));
        $productName =$request->name;
        
        $oldProductName = $product->name;
        $productId=$request->product_id;
		//check 
		$prod_count=Product::where('id','!=',$id)->where('product_id',$productId)->get()->count();
		if($prod_count!=0){
			flash(__('Product Id already exists'))->error();
            return back();
		}

		$product->product_id = $productId;
        if($oldProductName !== $productName) {
            $product->name = $productName;
        }
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->subsubcategory_id = $request->subsubcategory_id;
        $product->brand_id = $request->brand_id;
        $product->current_stock = $request->current_stock;
        $product->barcode = $request->barcode;
        $product->min_purchased_quantity = $request->min_purchased_quantity;
        $product->gst_tax = $request->gstpercent;
         
        if($request->has('previous_photos')){
            $photos = $request->previous_photos;
            $product->photos = json_encode($photos);
        }
        else{
            $photos = array();
            $product->photos = json_encode($photos);
        }
        $delete_photos = $request->input('delete_photos',array());
        foreach($delete_photos as $p) {
            if(env('FILESYSTEM_DRIVER') == "local") {
                if(file_exists(base_path('public/').$p))
                unlink(base_path('public/').$p);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($p);
            }
        }
        
        if($request->has('delete_thumbnail_img')){
            $delete_thumbnail_img = $request->delete_thumbnail_img;
            if(env('FILESYSTEM_DRIVER') == "local") {
                if(file_exists(base_path('public/').$delete_thumbnail_img))
                unlink(base_path('public/').$delete_thumbnail_img);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($delete_thumbnail_img);
            }
            $product->thumbnail_img = null;
        }
        
        if($request->has('delete_featured_img')){
            $delete_featured_img = $request->delete_featured_img;
            if(env('FILESYSTEM_DRIVER') == "local") {
                if(file_exists(base_path('public/').$delete_featured_img))
                unlink(base_path('public/').$delete_featured_img);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($delete_featured_img);
            }
            $product->featured_img = null;
        }
        
        if($request->has('delete_flash_deal_img')){
            $delete_flash_deal_img = $request->delete_flash_deal_img;
            if(env('FILESYSTEM_DRIVER') == "local") {
                if(file_exists(base_path('public/').$delete_flash_deal_img))
                unlink(base_path('public/').$delete_flash_deal_img);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($delete_flash_deal_img);
            }
            $product->flash_deal_img = null;
        }
        
        if($request->has('delete_meta_img')){
            $delete_meta_img = $request->delete_meta_img;
            if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$delete_meta_img))
                    unlink(base_path('public/').$delete_meta_img);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($delete_meta_img);
            }
            $product->meta_img = null;
        }	
	    
        
        if($request->hasFile('photos')){
			foreach ($request->photos as $photo) {
				$extension = strtolower($photo->extension());
		
				if($extension == "jpg" or $extension == "png" or $extension == "gif" or  $extension == "jpeg")
				{
					$mainimage = Str::slug($productName,"-").time().".".$extension;
					$img = Image::make($photo);
					$width = $img->width();
					$height = $img->height();
					if($width == 800 && $height == 800) {
        			    $photo->move(base_path('public/')."temp/",$mainimage);
        			} else {
            			if($width <= 800 && $height <= 800) {
    						$img->resizeCanvas(800, 800, 'center', false, 'fff');
    					} else {
    						 $img->resize(800, 800, function ($constraint) {
    							$constraint->aspectRatio();
    						});
    						$img->resizeCanvas(800, 800, 'center', false, 'fff');
    					}
    					$img->save(base_path('public/')."temp/".$mainimage, 100);
        			}
					
					if(env('FILESYSTEM_DRIVER') == "local") {
						copy(base_path('public/').'temp/'.$mainimage, base_path('public/').'uploads/products/photos/'.$mainimage);
					} else {
						Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/photos', new \Illuminate\Http\File(base_path('public/').'temp/'.$mainimage), $mainimage);
					}
					sleep(1);
					array_push($photos, 'uploads/products/photos/'.$mainimage);
					unlink(base_path('public/').'temp/'.$mainimage);
				}
				else{
					flash(__('Invalid file type. Please upload jpg/png/jpeg/gif format files only.'))->error();
					if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
						return redirect()->route('products.admin');
					} else {
						return redirect()->route('seller.products');
					}
				}
			}
			$product->photos = json_encode($photos);
		}
		
		if($request->hasFile('thumbnail_img')){
			if($product->thumbnail_img!=null){
			    if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$product->thumbnail_img))
                    unlink(base_path('public/').$product->thumbnail_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->thumbnail_img);
                }
			}
            $extension = $request->thumbnail_img->extension();
            $thumbnail = Str::slug($product->name,"-")."thumb".time().".".$extension;
            $img = Image::make($request->thumbnail_img);
            $width = $img->width();
			$height = $img->height();
			if($width == 200 && $height == 200) {
			    $request->thumbnail_img->move(base_path('public/')."temp/",$thumbnail);
			} else {
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$thumbnail, 100);
			}
            //ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/products/thumbnail/'.$thumbnail);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/thumbnail', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
            }
            $product->thumbnail_img = "uploads/products/thumbnail/".$thumbnail;
            unlink(base_path('public/').'temp/'.$thumbnail);
        } 
	
		
		if($request->hasFile('featured_img')){
			if($product->featured_img!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$product->featured_img))
                    unlink(base_path('public/').$product->featured_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->featured_img);
                }
			}
            $extension = $request->featured_img->extension();
            $featured = Str::slug($product->name,"-")."featured".time().".".$extension;
            $img = Image::make($request->featured_img);
            $width = $img->width();
			$height = $img->height();
			if($width == 200 && $height == 200) {
			    $request->featured_img->move(base_path('public/')."temp/",$featured);
			} else {
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$featured, 100);
			}
           // ImageOptimizer::optimize(base_path('public/')."temp/".$featured);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$featured, base_path('public/').'uploads/products/featured/'.$featured);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/featured', new \Illuminate\Http\File(base_path('public/').'temp/'.$featured), $featured);
            }
            $product->featured_img = "uploads/products/featured/".$featured;
            unlink(base_path('public/').'temp/'.$featured);
        }
		
		if($request->hasFile('flash_deal_img')){
			if($product->flash_deal_img!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$product->flash_deal_img))
                    unlink(base_path('public/').$product->flash_deal_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->flash_deal_img);
                }
			}
			
            $extension = $request->flash_deal_img->extension();
            $flash_deal = Str::slug($product->name,"-")."flash_deal".time().".".$extension;
            $img = Image::make($request->flash_deal_img);
            $width = $img->width();
			$height = $img->height();
			
			if($width == 200 && $height == 200) {
			    $request->flash_deal_img->move(base_path('public/')."temp/",$flash_deal);
			} else {
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$flash_deal, 100);
			}
			
            //ImageOptimizer::optimize(base_path('public/')."temp/".$flash_deal);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$flash_deal, base_path('public/').'uploads/products/flash_deal/'.$flash_deal);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/flash_deal', new \Illuminate\Http\File(base_path('public/').'temp/'.$flash_deal), $flash_deal);
            }
            $product->flash_deal_img = "uploads/products/flash_deal/".$flash_deal;
            unlink(base_path('public/').'temp/'.$flash_deal);
        }

        $product->unit = $request->unit;
        $product->tags = implode('|',$request->tags);
        $product->description = $request->description;
        $product->video_provider = $request->video_provider;
        $product->video_link = $request->video_link;
        $product->unit_price = $request->unit_price;
        $product->purchase_price = $request->purchase_price;
        $product->tax = $request->tax;
        $product->tax_type = $request->tax_type;
        $product->discount = $request->discount;
        $product->shipping_type = $request->shipping_type;
        if($request->shipping_type == 'free'){
            $product->shipping_cost = 0;
        }
        elseif ($request->shipping_type == 'flat_rate') {
            $product->shipping_cost = $request->flat_shipping_cost;
        }
        $product->discount_type = $request->discount_type;
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;
        $product->variation = $variation;
        $product->variation_order = $variation_order;
        $product->variation_combinations = $variation_combinations;
        $product->variation_product = $variation_product;
		
        if($request->hasFile('meta_img')){
            
			if($product->meta_img!=null){
				$image_path = public_path().'/'.$product->meta_img;
				if(file_exists($image_path))
					unlink($image_path);
			}
			$extension = $request->meta_img->extension();
            $meta_img = Str::slug($product->name,"-")."meta_img".time().".".$extension;
            $img = Image::make($request->meta_img);
            $width = $img->width();
			$height = $img->height();
			
			if($width == 200 && $height == 200) {
			    $request->meta_img->move(base_path('public/')."temp/",$meta_img);
			} else {
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$meta_img, 100);
			}
			
            //ImageOptimizer::optimize(base_path('public/')."temp/".$meta_img);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$meta_img, base_path('public/').'uploads/products/meta/'.$meta_img);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/meta', new \Illuminate\Http\File(base_path('public/').'temp/'.$meta_img), $meta_img);
            }
            $product->meta_img = "uploads/products/meta/".$meta_img;
            unlink(base_path('public/').'temp/'.$meta_img);
        }
	

        if($request->hasFile('pdf')){
            $product->pdf = $request->pdf->store('uploads/products/pdf');
        }
        
        if($oldProductName !== $productName) {
            $product->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $productName)).'-'.str_random(5));
        }

        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
        }
        else {
            $colors = array();
            $product->colors = json_encode($colors);
        }
        /***************ADD ITEAM IN ALIGN BOOK*********************************/
        
       
        if($product->alignbook_pro_id != '' || $product->alignbook_pro_id!=null){
             $uniqueid = $product->alignbook_pro_id; 
             $product->alignbook_pro_id = $uniqueid;     
        }else{
              $uniqueid =  Str::uuid()->toString();
              $product->alignbook_pro_id = $uniqueid;
        }
	   
        $postJsonData = array(
            	"is_new_mode"=> true,
            	"item_information"=> array(
            		"id"=> $uniqueid,
            		"name"=> $request->name,
            		"item_group_id"=> "a0931c2e-4d27-4b1b-bf6e-b63f8b6db2c4",
            		"item_category_id"=> "",
            		"type"=> 0,
            		"item_mode"=> 0,
            		"inactive"=> false,
            		"separate_pack_unit"=> false,
            		"input_dimension"=> false,
            		"gst_input_not_applicable"=> false,
            		"sub_item_applicable"=> false,
            		"stock_unit_id"=> "b9239fc8-bc7b-499f-9aca-d0635741295e",
            		"pack_unit_id"=> "",
            		"stock_vs_pack"=> 1,
            		"rate_per"=> 0,
            		"procurement"=> 0,
            		"garment_item_type"=> 0,
            		"gst_classification_id"=> "0eeb53e3-aeac-4a4f-870e-ab5d1d275081",
            		"vat_tax_id"=> "",
            		"sales_description"=> "XYZ",
            		"barcode"=> $product->product_id,
            		"markup_percentage"=> 0,
            		"sales_rate"=> $request->unit_price,
            		"mrp"=> 0,
            		"min_rate"=> 0,
            		"sales_misc_1"=> 0,
            		"sales_misc_2"=> 0,
            		"sales_misc_3"=> 0,
            		"sales_gl_id"=> "ee113c08-68fb-4bbb-993a-6309a4937fb3",
            		"purchase_description"=> $request->description,
            		"purchase_rate"=> 0,
            		"purchase_misc_1"=> 0,
            		"purchase_misc_2"=> 0,
            		"purchase_misc_3"=> 0,
            		"purchase_gl_id"=> "5dbdad5a-6b2c-463d-a78f-ee0cf45c6dfa",
            		"minimum_level"=> 0,
            		"inventory_gl_id"=> "",
            		"specification_template_id"=> "",
            		"free_template_id"=> "",
            		"suggested_template_id"=> "",
            		"rm_item_id"=> "",
            		"batch_wise_inventory"=> false,
            		"batch_wise_rate"=> false,
            		"serial_tracking"=> false,
            		"predefined"=> false,
            		"ask_udf_in_document"=> false,
            		"depreciation_gl_id"=> "",
            		"fca_622_id"=> "",
            		"fca_mdb_id"=> "",
            		"tally_id"=> "",
            		"salt"=> "",
            		"rack_box"=> "",
            		"metal_purity"=> 0,
            		"udf_list"=> ["", "", "", "", ""],
            		"itemset_template_list"=> [],
            		"attribute_list"=> [array(
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		),array (
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		)],
            		"fl_names"=> [],
            		"code_config"=>array (
            			"code"=> '0000000',
            			"code_no"=> 0,
            			"code_prefix"=> ""
            		),
            		"bundles"=> [],
            		"mapping_codes"=> []
            	)
            );
          $endPoints = 'SaveUpdate_Item';
          
          $addIteam = $this->addUpdateIteam(json_encode($postJsonData),$endPoints);
          
          
           $jsondRepons = json_encode($addIteam,true);
          //dd(json_encode($postJsonData),json_decode($jsondRepons));
          $respondData = json_decode($jsondRepons);
          //dd($postJsonData,$respondData);
         if($respondData->ReturnCode == 0){
            /*******************END ADD ITEAM IN ALIGN BOOK**************************/
            $product->save();
    		if(env('CACHE_DRIVER') == 'redis') {
                    Cache::tags(['Product'])->forever('Product:'.$product->id, $product);
            } else {  
                Cache::forever('Product:'.$product->id, $product);
            }
            $prefix = env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_');
    		$OldCache = Redis::command('keys',['*ProductID:'.$product->id.':*']);
            foreach($OldCache as $value) {
                $value = str_replace($prefix, '', $value);
                Redis::del($value);
            }
            Redis::set('ProductID:'.$product->id.':ProductTag:'.strtolower($product->tags), $product->id);
            Redis::set('ProductID:'.$product->id.':ProductName:'.strtolower($product->name), $product->id);
            Redis::set('ProductID:'.$product->id.':ProductSalt:'.strtolower($product->salt), $product->id);
        
            flash(__('Product has been updated successfully'))->success();
            if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
                return redirect()->route('products.admin');
               // return back();
                //echo 'updated succ';
            }
            else{
               // return redirect()->route('seller.products');
               return back();
            }
         }else{
              flash(__($respondData->Message))->error();
              return back();
         }
    }
    
    public function addUpdateIteam($requestJson,$endPoint){
	   // dd($requestJson);
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
        $data = json_decode($response1);
        return $data; 
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if(Product::destroy($id)){
            if($product->thumbnail_img != null) {
                if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$product->thumbnail_img))
                    unlink(base_path('public/').$product->thumbnail_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->thumbnail_img);
                }
                $product->thumbnail_img = null;
            }
			
            if($product->featured_img != null) {
                if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$product->featured_img))
                    unlink(base_path('public/').$product->featured_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->featured_img);
                }
                $product->featured_img = null;
            }
            
            if($product->flash_deal_img != null) {
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$product->flash_deal_img))
                    unlink(base_path('public/').$product->flash_deal_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->flash_deal_img);
                }
                $product->flash_deal_img = null;
            }
			if($product->meta_img !=null) {
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$product->meta_img))
                    unlink(base_path('public/').$product->meta_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->meta_img);
                }
                $product->meta_img = null;
			}
			if(json_decode($product->photos) != null) {
                foreach (json_decode($product->photos) as $key => $photo) {
                    if(env('FILESYSTEM_DRIVER') == "local") {
                        if(file_exists(base_path('public/').$photo))
                        unlink(base_path('public/').$photo);
                    } else {
                        Storage::disk(env('FILESYSTEM_DRIVER'))->delete($photo);
                    }
                    $product->photos = json_encode(array());
                }
			}
			$product->save();
    		if(env('CACHE_DRIVER') == 'redis') {
                    Cache::tags(['Product'])->forget('Product:'.$product->id);
            } else {  
               Cache::forget('Product:'.$product->id);
            }
            $prefix = env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_');
    		$OldCache = Redis::command('keys',['*ProductID:'.$product->id.':*']);
            foreach($OldCache as $value) {
                $value = str_replace($prefix, '', $value);
                Redis::del($value);
            }
            $OrderDetail = OrderDetail::where('product_id', $product->id)->count();
            if($OrderDetail == 0) {
                $product->forceDelete();
            }
            
            flash(__('Product has been deleted successfully'))->success();
            if(Auth::user()->user_type == 'admin') {
                return redirect()->route('products.admin');
            } else {
                return redirect()->route('seller.products');
            }
        } else {
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
    
    public function forceDelete(Request $request) {
        $product = Product::withTrashed()->findOrFail($request->id);
        $OrderDetail = OrderDetail::where('product_id', $product->id)->count();
        if($OrderDetail == 0) {
            $product->forceDelete();
            flash(__('Product has been deleted successfully'))->success();
        } else {
            flash(__('Product not able to get deleted due to Orders.'))->error();
        }
        return back();
    }

    /**
     * Duplicates the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($id)
    {
        $product = Product::find($id);
        $product_new = $product->replicate();
        $n = 1;
        while(1) {
            $pp = Product::where('product_id', $product->product_id."-".$n)->count();
            if($pp == 0) {
                break;
            } else {
                $n++;
            }
        }
        
        $product_new->slug = strtolower(substr($product_new->slug, 0, -5).str_random(5));
        $product_new->product_id = $product->product_id."-".$n;

        if($product_new->save()){
            flash(__('Product has been duplicated successfully'))->success();
            if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
                return redirect()->route('products.admin');
            }
            else{
                return redirect()->route('seller.products');
            }
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /****
     * getSeelerList By Product
     */
    public function transfersellerList($id){
        dd($id);

    }

    public function get_products_by_subsubcategory(Request $request)
    {
        $products = Product::where('subsubcategory_id', $request->subsubcategory_id)->get();
        return $products;
    }

    public function get_products_by_category(Request $request)
    {
        $products = Product::where('category_id', $request->category_id)->select('id','product_id','name','category_id')->get();
        
        return $products;
    }

    public function get_products_by_brand(Request $request)
    {
        $products = Product::where('brand_id', $request->brand_id)->get();
        return view('partials.product_select', compact('products'));
    }
    

    public function restore(Request $request)
    {
        $product = Product::withTrashed()->findOrFail($request->id);
        $product->restore();
        flash(__('Product has been restored successfully'))->success();
        return back();
    }

    public function updateTodaysDeal(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->todays_deal = $request->status;
        if($product->save()){
			Cache::forget('num_todays_deal');
			Cache::forget('todays_deal_prods');
            return 1;
        }
        return 0;
    }

    public function updatePublished(Request $request)
    {
        $product = Product::findOrFail($request->id);
       
        $product->published = $request->status;
        if($product->save()){
            if($request->status==1){
                Redis::set('ProductID:'.$product->id.':ProductTag:'.strtolower($product->tags), $product->id);
                Redis::set('ProductID:'.$product->id.':ProductName:'.strtolower($product->name), $product->id);
                Redis::set('ProductID:'.$product->id.':ProductSalt:'.strtolower($product->salt), $product->id);
            }
            else{
    			$prefix = env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_');
        		$OldCache = Redis::command('keys',['*ProductID:'.$product->id.':*']);
                foreach($OldCache as $value) {
                    $value = str_replace($prefix, '', $value);
                    Redis::del($value);
                }
            }
            return 1;
        }
        return 0;
    }
    

    public function updateFeatured(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->featured = $request->status;
        if($product->save()){
			Cache::forget('featured_product');
            return 1;
        }
        return 0;
    }

    public function sku_combination(Request $request)
    {
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $unit_price = $request->unit_price;
        $product_name = $request->name;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('', $request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        $combinations = combinations($options);
        return view('partials.sku_combinations', compact('combinations', 'unit_price', 'colors_active', 'product_name'));
    }

    public function variation_edit(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $product_name = $request->name;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = $request[$name];
                array_push($options, explode(',', $my_str));
            }
        }
        
        $products = Product::where('subsubcategory_id', $product->subsubcategory_id)->select('name','id', 'variation_show_hide', 'product_id')->get();

        $combinations = combinations($options);
        $product_variation = json_decode($product->variation_product,true);
        //dd($product_variation);
        //print_r($combinations);
        //die();
        return view('partials.product_variation_edit', compact('combinations', 'colors_active', 'product_variation', 'product', 'products'));
    }

    public function sku_combination_edit(Request $request)
    {
        $product = Product::findOrFail($request->id);

        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $product_name = $request->name;
        $unit_price = $request->unit_price;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('|', $request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        $combinations = combinations($options);
        return view('partials.sku_combinations_edit', compact('combinations', 'unit_price', 'colors_active', 'product_name', 'product'));
    }
    
    public function bulk_remove_view()
    {
        return view('products.bulk_remove');
    }

    public function bulk_remove(Request $request)
    {
       // dd($request->all());
        $validatedData = $request->validate([
			'bulk_file' => 'file|mimes:xlsx,xls'
		]);
        $validatedData = $request->validate([
			'bulk_file' => 'file|max:10240'
		]);
        if($request->hasFile('bulk_file')){
           Excel::import(new ProductsBulkDelete, request()->file('bulk_file'));
        }
       flash('Products Removed successfully')->success();
		//$output=array('success' => true,'mesg'=>'Products imported successfully');
       return back();
		//return response()->json($output);
    }


    public function getAllProductBySubCatId(Request $request){
        DB::enableQueryLog();
        $where =[
            'category_id'=>$request->catId,
            'subcategory_id'=>$request->subCatId,
            'subsubcategory_id'=>$request->subsubcategorySearchID
        ];
        
        $AllProdcuct = $products = Product::where('subsubcategory_id', $request->subsubcategorySearchID)->get();
       // dd(DB::getQueryLog());
        $data="<option value='all'>All</option>";
		foreach($AllProdcuct as $AllProdcuctList){
			$data.="<option value='".$AllProdcuctList->id."'>".strtoupper($AllProdcuctList->name)."</option>";
        }

      
		echo $data;        

    }
    public function reviewList(Request $request, $slug){
        
        $detailedProduct  = Product::where('slug', $slug)->first();
        $productReview = Review::where('product_id',$detailedProduct->id)->orderBy('id','desc')->get();
        return view('frontend.review-list', compact('detailedProduct','productReview'));
    }
}
