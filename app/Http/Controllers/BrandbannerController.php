<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brandbanner;
use Illuminate\Support\Facades\Cache;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use ImageOptimizer;
use Storage;

class BrandbannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Brandbanner = Brandbanner::all();
        return view('Brandbanner.index', compact('Brandbanner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Brandbanner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if($request->hasFile('photo')){
		    
		    $extension = $request->photo->extension();
            $thumbnail = "BBanner".time().".".$extension;
            $request->photo->move(base_path('public/')."temp/",$thumbnail);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/Brandbanner/'.$thumbnail);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/Brandbanner', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
            }
            $Brandbannerr = "uploads/Brandbanner/".$thumbnail;
            unlink(base_path('public/').'temp/'.$thumbnail);
            
            $Brandbanner = new Brandbanner;
            $Brandbanner->photo = $Brandbannerr;
            $Brandbanner->link = $request->url;
            $Brandbanner->save();
			Cache::forget('Brandbanner');
            flash(__('Brand Banner has been inserted successfully'))->success();
        }
        return redirect()->route('home_settings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Brandbanner = Brandbanner::findOrFail($id);
		//dd($Brandbanner);
        return view('Brandbanner.edit', compact('Brandbanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
		$Brandbanner = Brandbanner::find($id);
        
      
		// if($request->hasFile('photo')){
            // $Brandbanner->photo = $request->photo->store('uploads/Brandbanner');
        // }
		
		if($request->hasFile('photo')){
			if($Brandbanner->photo!=null){
				$image_path = public_path().'/'.$Brandbanner->photo;
				if(file_exists($image_path))
					unlink($image_path);
			}
			
            $extension = $request->photo->extension();
            $thumbnail = "BBanner".time().".".$extension;
            //$img = Image::make($request->photo);
            //$img->save(base_path('public/')."temp/".$thumbnail);
            $request->photo->move(base_path('public/')."temp/",$thumbnail);
            //ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);
            if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/Brandbanner/'.$thumbnail);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/Brandbanner', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
            }
            $Brandbanner->photo = "uploads/Brandbanner/".$thumbnail;
            unlink(base_path('public/').'temp/'.$thumbnail);
        }
		else{ $Brandbanner->photo = $request->previous_photo; }
		
        $Brandbanner->link = $request->url;
        $Brandbanner->save();
		Cache::forget('Brandbanner');
        flash(__('BrandBanner has been updated successfully'))->success();
        return redirect()->route('home_settings.index');
		
    }
	public function update_status(Request $request)
    {
		$Brandbanner = Brandbanner::find($request->id);
        $Brandbanner->published = $request->status;
		if($Brandbanner->save()){
			Cache::forget('Brandbanner');
			return '1';
		}
		else {
			return '0';
		}
		return '0';
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Brandbanner = Brandbanner::findOrFail($id);
		if(Brandbanner::destroy($id)){
			if($Brandbanner->photo!=null){
			   $image_path = public_path().'/'.$Brandbanner->photo;
			   if(file_exists($image_path))
					unlink($image_path);
			}
			Cache::forget('Brandbanner');
            flash(__('Brand Banner has been deleted successfully'))->success();
        }
        else{
            flash(__('Something went wrong'))->error();
        }
        return redirect()->route('home_settings.index');

		
    }
}
