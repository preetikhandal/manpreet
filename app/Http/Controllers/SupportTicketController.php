<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\User;
use Auth;
use App\TicketReply;

class SupportTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(9);
        return view('frontend.support_ticket.index', compact('tickets'));
    }

    public function admin_index(Request $request)
    {
        $sort_search =null;
        $tickets = Ticket::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $tickets = $tickets->where('code', 'like', '%'.$sort_search.'%');
        }
        $tickets = $tickets->paginate(15);
        return view('support_tickets.index', compact('tickets', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd();
        $files = array();
        if($request->hasFile('attachments')){
            foreach ($request->attachments as $key => $attachment) {
                $size = $attachment->getSize();
                $size = $size/1024;
                $size = $size/1024;
                if($size > 2.99) {
                    flash('File size should not be more than 3 MB.')->warning();
                    return back()->withInput($request->all());
                }
                $extension = $attachment->extension();
                $extension = strtolower($extension);
                if($extension == 'jpeg' or $extension == 'jpg' or $extension == 'pdf' or $extension == 'png') {
                    
                } else {
                    flash('Only jpg/png and pdf allows.')->warning();
                    return back()->withInput($request->all());
                }
            }
        }
        $NextT = Ticket::latest()->first();
        $ticket = new Ticket;
        $ticket->code = max(100000, ($NextT != null ? $NextT->code + 1 : 0));
        $ticket->user_id = Auth::user()->id;
        $ticket->subject = $request->subject;
        $ticket->details = $request->details;
        $files = array();

        if($request->hasFile('attachments')){
            foreach ($request->attachments as $key => $attachment) {
                $item['name'] = $attachment->getClientOriginalName();
                if(env('FILESYSTEM_DRIVER') == "local") {
						$item['path'] = $attachment->store('uploads/support_tickets/');
					} else {
					    $item['path'] = $attachment->store('uploads/support_tickets/',env('FILESYSTEM_DRIVER'));
					}
                array_push($files, $item);
            }
            $ticket->files = json_encode($files);
        }
        
        if($ticket->save()){
            flash('Ticket has been sent successfully')->success();
            return redirect()->route('support_ticket.index');
        }
        else{
            flash('Something went wrong')->error();
        }
    }

    public function admin_store(Request $request)
    {
        if($request->hasFile('attachments')){
            foreach ($request->attachments as $key => $attachment) {
                $size = $attachment->getSize();
                $size = $size/1024;
                $size = $size/1024;
                if($size > 2.99) {
                    flash('File size should not be more than 3 MB.')->warning();
                    return back()->withInput($request->all());
                }
                $extension = $attachment->extension();
                $extension = strtolower($extension);
                if($extension == 'jpeg' or $extension == 'jpg' or $extension == 'pdf' or $extension == 'png') {
                    
                } else {
                    flash('Only jpg/png and pdf allows.')->warning();
                    return back()->withInput($request->all());
                }
            }
        }
        
        //dd($request->all());
        $ticket_reply = new TicketReply;
        $ticket_reply->ticket_id = $request->ticket_id;
        $ticket_reply->user_id = Auth::user()->id;
        $ticket_reply->reply = $request->reply;

        $files = array();

        if($request->hasFile('attachments')){
            foreach ($request->attachments as $key => $attachment) {
                $item['name'] = $attachment->getClientOriginalName();
                if(env('FILESYSTEM_DRIVER') == "local") {
					$item['path'] = $attachment->store('uploads/support_tickets/');
				} else {
				    $item['path'] = $attachment->store('uploads/support_tickets/',env('FILESYSTEM_DRIVER'));
				}
                array_push($files, $item);
            }
            $ticket_reply->files = json_encode($files);
        }

        $ticket_reply->ticket->client_viewed = 0;
        $ticket_reply->ticket->status = $request->status;
        $ticket_reply->ticket->save();
        if($ticket_reply->save()){
            flash('Reply has been sent successfully')->success();
            return back();
        }
        else{
            flash('Something went wrong')->error();
        }
    }

    public function seller_store(Request $request)
    {
        if($request->hasFile('attachments')){
            foreach ($request->attachments as $key => $attachment) {
                $size = $attachment->getSize();
                $size = $size/1024;
                $size = $size/1024;
                if($size > 2.99) {
                    flash('File size should not be more than 3 MB.')->warning();
                    return back()->withInput($request->all());
                }
                $extension = $attachment->extension();
                $extension = strtolower($extension);
                if($extension == 'jpeg' or $extension == 'jpg' or $extension == 'pdf' or $extension == 'png') {
                    
                } else {
                    flash('Only jpg/png and pdf allows.')->warning();
                    return back()->withInput($request->all());
                }
            }
        }
        
        $ticket_reply = new TicketReply;
        $ticket_reply->ticket_id = $request->ticket_id;
        $ticket_reply->user_id = $request->user_id;
        $ticket_reply->reply = $request->reply;

        $files = array();

        if($request->hasFile('attachments')){
            foreach ($request->attachments as $key => $attachment) {
                $item['name'] = $attachment->getClientOriginalName();
                if(env('FILESYSTEM_DRIVER') == "local") {
					$item['path'] = $attachment->store('uploads/support_tickets/');
				} else {
				    $item['path'] = $attachment->store('uploads/support_tickets/',env('FILESYSTEM_DRIVER'));
				}
                array_push($files, $item);
            }
            $ticket_reply->files = json_encode($files);
        }

        $ticket_reply->ticket->viewed = 0;
        $ticket_reply->ticket->status = 'pending';
        $ticket_reply->ticket->save();
        if($ticket_reply->save()){
            flash('Reply has been sent successfully')->success();
            return back();
        }
        else{
            flash('Something went wrong')->error();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::findOrFail(decrypt($id));
        $ticket->client_viewed = 1;
        $ticket->save();
        $ticket_replies = $ticket->ticketreplies;
        return view('frontend.support_ticket.show', compact('ticket','ticket_replies'));
    }

    public function admin_show($id)
    {
        $imageUrl = [];
        $ticket = Ticket::findOrFail(decrypt($id));
        $filePath = $ticket->files;
        if(isset($ticket->files)){
            $imageArray = json_decode($filePath);
        
            if(isset($imageArray[0]->path)){
                $imageUrl = $imageArray[0]->path;
            }
        }      
        $ticket->viewed = 1; 
        $ticket->save();
        $ticket->imageUrl = $imageUrl;
        return view('support_tickets.show', compact('ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
