<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\Brand;
use App\Product;
use App\Language;
use ImageOptimizer;
use Illuminate\Support\Facades\Cache;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;

class SubSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search =null; $categorySearchID=null; $subcategorySearchID=null;
        $subsubcategories = SubSubCategory::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $subsubcategories = $subsubcategories->where('name', 'like', '%'.$sort_search.'%');
        }
		if($request->has('searchdata')){
			$categorySearchID = $request->input('categorySearchID',0);
			if($categorySearchID!=null){
				$subcategorySearchID = $request->subcategorySearchID;
				if($subcategorySearchID=="all"){
					$allsubcats=SubCategory::select('id')->where('category_id',$categorySearchID)->get()->pluck('id')->all();
					
					$subsubcategories = $subsubcategories->whereIn('sub_category_id', $allsubcats);
				}
				else{
					$subsubcategories = $subsubcategories->where('sub_category_id', $subcategorySearchID);
				}
			}
			
		}
		
        $subsubcategories = $subsubcategories->paginate(50);
        return view('subsubcategories.index', compact('subsubcategories', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('subsubcategories.create', compact('categories', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
			'name' => 'required|min:3|max:50',
			'meta_title' => 'max:255',
			'icon' => 'file|max:2048|mimes:jpeg,jpg,png'
		]);
		$subsubcategoryname = $request->name;
		$tags = $request->tags;
		$subsubcategoryname=ucwords(strtolower($subsubcategoryname));
        $sub_category_id = $request->sub_category_id;
		//check in db
		$count=SubSubCategory::where('sub_category_id',$sub_category_id)->where('name',$subsubcategoryname)->count();
		if($count!=0){
			flash(__('Sub Subcategory already exists'))->error();
            return back();
		}

        $subsubcategory = new SubSubCategory;
        $subsubcategory->name = $request->name;
		$subsubcategory->display = $request->display;
        $subsubcategory->tags = implode('|',$request->tags);
        
        $subsubcategory->sub_category_id = $request->sub_category_id;
        //$subsubcategory->attributes = json_encode($request->choice_attributes);
        //$subsubcategory->brands = json_encode($request->brands);
        if($request->meta_title==""){
			$subsubcategory->meta_title = $subsubcategoryname.": Buy ".$subsubcategoryname." products online at best prices in India - Aldebazaar.com";
		}
		else{ $subsubcategory->meta_title = $request->meta_title; }
		if($subsubcategory->meta_description==""){
			$subsubcategory->meta_description = "Buy ".$subsubcategoryname." products online at best prices from Aldebazaar.com. Check out ".$subsubcategoryname." products from top brands at Aldebazaar.com.";
		}
		else{ $subsubcategory->meta_description = $request->meta_description; }
		
        if ($request->slug != null) {
            $subsubcategory->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->slug)));
        }
        else {
            $subsubcategory->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $subsubcategoryname)));
        }

        $subsubcategory->slug = str_ireplace('--','-', $subsubcategory->slug);
        if($request->hasFile('icon')){
        	$extension = $request->icon->extension();
        	$icon = Str::slug($subsubcategoryname,"-")."icon".time().".".$extension;
        	$img = Image::make($request->icon);
        	$width = Image::make($request->icon)->width();
        	$height = Image::make($request->icon)->height();
        	
        	if($width == 150 && $height == 150) {
    			 $request->icon->move(base_path('public/')."temp/",$icon);
    		} else {
        		$img->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
        		$img->save(base_path('public/')."temp/".$icon);
    		}
        	if(env('FILESYSTEM_DRIVER') == "local") {
        		copy(base_path('public/').'temp/'.$icon, base_path('public/').'uploads/subsubcategories/'.$icon);
        	} else {
        		Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/subsubcategories', new \Illuminate\Http\File(base_path('public/').'temp/'.$icon), $icon);
        	}
        	$subsubcategory->icon = "uploads/subsubcategories/".$icon;
        	unlink(base_path('public/').'temp/'.$icon);
        }
        
        $top_sku = $request->top_sku;
        if($top_sku == "") {
            $top_sku = array();
        } else {
            $top_sku = explode(",", $top_sku);
            foreach($top_sku as $key => $sku) {
                $top_sku[$key] = trim($top_sku[$key]);
            }
        }
        
        $subsubcategory->top_sku = json_encode($top_sku);

        if($subsubcategory->save()){
            flash(__('SubSubCategory has been inserted successfully'))->success();
            return redirect()->route('subsubcategories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subsubcategory = SubSubCategory::findOrFail(decrypt($id));
        $categories = Category::all();
        $brands = Brand::all();
        return view('subsubcategories.edit', compact('subsubcategory', 'categories', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
			'name' => 'required|min:3|max:50',
			'meta_title' => 'max:255',
			'icon' => 'file|max:2048|mimes:jpeg,jpg,png'
		]);
        $subsubcategory = SubSubCategory::findOrFail($id);

        $subsubcategory->name = $request->name;
		$subsubcategory->display = $request->display;
        $subsubcategory->tags = implode('|',$request->tags);
        $subsubcategory->sub_category_id = $request->sub_category_id;
        //$subsubcategory->attributes = json_encode($request->choice_attributes);
        //$subsubcategory->brands = json_encode($request->brands);
        if($request->meta_title==""){
			$subsubcategory->meta_title = $subsubcategoryname.": Buy ".$subsubcategoryname." products online at best prices in India - Aldebazaar.com";
		}
		else{ $subsubcategory->meta_title = $request->meta_title; }
		if($subsubcategory->meta_description==""){
			$subsubcategory->meta_description = "Buy ".$subsubcategoryname." products online at best prices from Aldebazaar.com. Check out ".$subsubcategoryname." products from top brands at Aldebazaar.com.";
		}
		else{ $subsubcategory->meta_description = $request->meta_description; }
		
        if ($request->slug != null) {
            $subsubcategory->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->slug)));
        }
        else {
            $subsubcategory->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $subsubcategoryname)));
        }

        $subsubcategory->slug = str_ireplace('--','-', $subsubcategory->slug);
        if($request->hasFile('icon')){
        	if($subsubcategory->icon!=null){
        		if(env('FILESYSTEM_DRIVER') == "local") {
        			if(file_exists(base_path('public/').$subsubcategory->icon))
        			unlink(base_path('public/').$subsubcategory->icon);
        		} else {
        			Storage::disk(env('FILESYSTEM_DRIVER'))->delete($subsubcategory->icon);
        		}
        	}
        	$extension = $request->icon->extension();
        	$icon = Str::slug($request->name,"-")."icon".time().".".$extension;
        	$img = Image::make($request->icon);
        	$width = Image::make($request->icon)->width();
        	$height = Image::make($request->icon)->height();
        
        	if($width == 150 && $height == 150) {
    			 $request->icon->move(base_path('public/')."temp/",$icon);
    		} else {
        		$img->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
        		$img->save(base_path('public/')."temp/".$icon);
    		}
        				
        	if(env('FILESYSTEM_DRIVER') == "local") {
        		copy(base_path('public/').'temp/'.$icon, base_path('public/').'uploads/subsubcategories/'.$icon);
        	} else {
        		Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/subsubcategories', new \Illuminate\Http\File(base_path('public/').'temp/'.$icon), $icon);
        	}
        	$subsubcategory->icon = "uploads/subsubcategories/".$icon;
        	unlink(base_path('public/').'temp/'.$icon);
        }
        
        $top_sku = $request->top_sku;
        if($top_sku == "") {
            $top_sku = array();
        } else {
            $top_sku = explode(",", $top_sku);
            foreach($top_sku as $key => $sku) {
                $top_sku[$key] = trim($top_sku[$key]);
            }
        }
        
        $subsubcategory->top_sku = json_encode($top_sku);
        if($subsubcategory->save()){
            flash(__('SubSubCategory has been updated successfully'))->success();
            return redirect()->route('subsubcategories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subsubcategory = SubSubCategory::findOrFail($id);
        $ProductCount = Product::where('subsubcategory_id', $subsubcategory->id)->count();
        if($ProductCount > 0) {
		    flash(__('Sub Sub Category has been assigned to Products. Unassigned before delete this sub sub category'))->warning();
		    return back();
		}
        if(SubSubCategory::destroy($id)){
            if($subsubcategory->icon != null){
            	if(env('FILESYSTEM_DRIVER') == "local") {
            		if(file_exists(base_path('public/').$subsubcategory->icon))
            		unlink(base_path('public/').$subsubcategory->icon);
            	} else {
            		Storage::disk(env('FILESYSTEM_DRIVER'))->delete($subsubcategory->icon);
            	}
            	$subsubcategory->icon = null;
            }
            flash(__('SubSubCategory has been deleted successfully'))->success();
            return redirect()->route('subsubcategories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function get_subsubcategories_by_subcategory(Request $request)
    {
        $subsubcategories = SubSubCategory::where('sub_category_id', $request->subcategory_id)->get();
        return $subsubcategories;
    }

    public function getSubsubcatsBy(Request $request){
		$subsubcats = SubSubCategory::where('sub_category_id', $request->subcategorySearchID)->get();
        $data="<option value='all'>All</option>";
		foreach($subsubcats as $subsubcatgy){
			$data.="<option value='".$subsubcatgy->id."'>".strtoupper($subsubcatgy->name)."</option>";
		}
		echo $data;
	}
}
