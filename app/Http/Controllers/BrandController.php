<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Product;
use Illuminate\Support\Facades\Cache;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search =null;
        $brands = Brand::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
           $brands = $brands->where('name', 'like', '%'.$sort_search.'%')->orWhere('id',$request->search);
        }
        $brands = $brands->paginate(15);
        return view('brands.index', compact('brands', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
			'name' => 'required|min:2|max:30',
			'brand_discount' => 'required',
			'meta_title' => 'max:255'
		]);
        $brand = new Brand;
        $brand->name = $request->name;
        $brand->tags = implode('|',$request->tags);
       
		if($request->meta_title==""){
		    $brand->meta_title="Buy ".$request->name." products online at lowest price in india.";
		}
		else{
		    $brand->meta_title = $request->meta_title;
		}
		if($request->meta_description==""){
	    	$brand->meta_description="Aldebazaar.com Buy ".$request->name." online at low price in India on Aldebazaar.com. Check out ".$request->name." reviews, specifications and more at Aldebazaar.com.";
		}
		else{
		    $brand->meta_description = $request->meta_description;
		}
        if ($request->slug != null) {
            $brand->slug = str_replace(' ', '-', $request->slug);
        }
        else {
            $brand->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.str_random(5);
        }
        if($request->hasFile('logo')){
            $brand->logo = $request->file('logo')->store('uploads/brands');
        }
        $brand->discount = $request->brand_discount;

        if($brand->save()){
			Cache::forget('$brands');
            flash(__('Brand has been inserted successfully'))->success();
            return redirect()->route('brands.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::findOrFail(decrypt($id));
        return view('brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
			'name' => 'required|min:2|max:30',
			'brand_discount' => 'required',
			'meta_title' => 'max:255'
		]);
        $brand = Brand::findOrFail($id);
        $brand->name = $request->name;
        $brand->tags = implode('|',$request->tags);
        $brand->meta_title = $request->meta_title;
        $brand->meta_description = $request->meta_description;
        if ($request->slug != null) {
            $brand->slug = str_replace(' ', '-', $request->slug);
        }
        else {
            $brand->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.str_random(5);
        }
        if($request->hasFile('logo')){
			if($brand->logo!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$brand->logo))
                    unlink(base_path('public/').$brand->logo);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($brand->logo);
                }
			}
            $brand->logo = $request->file('logo')->store('uploads/brands');
        }
        
        $brand->discount = $request->brand_discount;

        if($brand->save()){
			Cache::forget('$brands');
            flash(__('Brand has been updated successfully'))->success();
            return redirect()->route('brands.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);
        $Product = Product::where('brand_id', $brand->id)->count();
        if($Product > 0) {
		    flash(__('Brand has been assigned to Products. Unassigned before delete this brand'))->warning();
		    return back();
		}
        if(Brand::destroy($id)){
            if($brand->logo != null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$brand->logo))
                    unlink(base_path('public/').$brand->logo);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($brand->logo);
                }
            }
			Cache::forget('$brands');
            flash(__('Brand has been deleted successfully'))->success();
            return redirect()->route('brands.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function getBrandBySubCatId(Request $request){

        $subsubcats = Brand::where('sub_category_id', $request->subcategorySearchID)->get();
        $data="<option value='all'>All</option>";
		foreach($subsubcats as $subsubcatgy){
			$data.="<option value='".$subsubcatgy->id."'>".strtoupper($subsubcatgy->name)."</option>";
		}
		echo $data;
        
    }
}
