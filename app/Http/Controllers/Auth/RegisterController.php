<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Customer;
use App\BusinessSetting;
use App\OtpConfiguration;
use App\Http\Controllers\Controller;
use App\Http\Controllers\OTPVerificationController;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Cookie;
use Nexmo;
use Twilio\Rest\Client;
use Mail;
use Session;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:8|confirmed'
        ]);
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return Validator::make($data, [
                'email' => 'email:rfc,dns',
            ]);
        } else {
            return Validator::make($data, [
                'email' => 'min:10|max:10',
            ]);
         }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $user = User::create([
                'name' => $data['name'],
                'email' => strtolower($data['email']),
                'password' => Hash::make($data['password']),
            ]);
            $user->email_verified_at = date('Y-m-d H:m:s');
            $user->save();
            $customer = new Customer;
            $customer->user_id = $user->id;
            $customer->save();
            if($data['email']!="" && filter_var($data['email'], FILTER_VALIDATE_EMAIL))
            {
                $data1 = array('email' => strtolower($data['email']), 'name' => $data['name']);
                    
                 Mail::send('emails.user_register', ['data' => $data1], function ($m) use ($data1) {
                    $m->to($data1['email'], $data1['name'])->subject('Alde Bazaar :: User Registration Confirmation');
                });
            }
            if(Cookie::has('referral_code')){
                $referral_code = Cookie::get('referral_code');
                $referred_by_user = User::where('referral_code', $referral_code)->first();
                if($referred_by_user != null){
                    $user->referred_by = $referred_by_user->id;
                    $user->save();
                }
            }
            flash(__('Registration successfull.'))->success();
            return $user;
        } else {
           
           $currentTime = time();
        
        if($currentTime <= session('login_otp_expire_time')) {
            if(session('login_otp') == $data['otp']) {
           
            $user = User::create([
            'name' => $data['name'],
            'phone' => session('login_phone'),
            'password' => Hash::make($data['password'])
            ]);
            
            $user->email_verified_at = date('Y-m-d H:m:s');
            $user->save();
            $customer = new Customer;
            $customer->user_id = $user->id;
            $customer->save();
            if(Cookie::has('referral_code')){
                $referral_code = Cookie::get('referral_code');
                $referred_by_user = User::where('referral_code', $referral_code)->first();
                if($referred_by_user != null){
                    $user->referred_by = $referred_by_user->id;
                    $user->save();
                }
            }
            Session::forget('login_otp_expire_time');
            Session::forget('login_otp');
            Session::forget('login_phone');
            Session::forget('login_name');
               
                if(Session::get('link') != null){
                    $redirect = Session::get('link');
                } else {
                    $redirect = route('home');
                }
            //return response()->json(array('result' => true, 'redirectTo' => $redirect, 'name' => $user->name));
            
                return $user;
            } else {
            return response()->json(array('result' => false, 'message' => "Wrong OTP entered."));
            }
        } else {
        return response()->json(array('result' => false, 'message' => "OTP has been expired."));
        }
           /*  
            $user = User::create([
                    'name' => $data['name'],
                    'phone' => $data['email'],
                    'password' => Hash::make($data['password'])
                ]);
            $user->email_verified_at = date('Y-m-d H:m:s');
            $user->save();
            if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated){
                 $user->email_verified_at = date('Y-m-d H:m:s');
                $user->save();
                if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated){
                    $otpController = new OTPVerificationController;
                    $otpController->send_code($user);
                }
            }*/
        }
            
    }
   
    
    public function register(Request $request)
    {     
            $name = $request->name;
            $email = $request->email;
            $password = $request->password;
            
            //add cutomer in align book
             $uniqueid  = Str::uuid()->toString();
             $postJsonData = array(
            	"is_new_mode"=> true,
            	"partyInformation"=> array(
            		"id"=> $uniqueid,
            		"name"=> $name,
            		"under_ledger_id"=> "ac7e815e-54fd-4702-af22-b4344e49120d",
            		"old_under_ledger_id"=> "",
            		"is_import_mode"=> false,
            		"print_name"=>$name,
            		"is_subparty"=> false,
            		"is_common"=> false,
            		"inactive"=> false,
            		"under_party_id"=> "",
            		"firm_type"=> 0,
            		"territory_id"=> "",
            		"contact_person"=> "",
            		"shipping_address_same"=> false,
            		"gst"=> "07AAACE7420R1ZB",
            		"gst_category"=> 0,
            		"distance"=> 0,
            		"gst_effective_from"=> "",
            		"tax_style"=> 0,
            		"cin"=> "",
            		"pan"=> "",
            		"tds_applicable"=> false,
            		"gst_based_tds"=> false,
            		"tds_section_id"=> "",
            		"payment_term_id"=> "",
            		"price_category_id"=> "",
            		"sales_executive_id"=> "",
            		"agent_id"=> "",
            		"transporter_id"=> "",
            		"credit_limit"=> 0,
            		"interest_rate"=> 0,
            		"bank_name"=> "",
            		"bank_branch"=> "",
            		"bank_account_no"=> "",
            		"bank_rtgs_no"=> "",
            		"party_category_id"=> "",
            		"allow_mobile_access"=> false,
            		"mobile_access_password"=> "",
            		"customer_on_watch"=> false,
            		"fca_622_id"=> "",
            		"fca_mdb_id"=> "",
            		"tally_id"=> "",
            		"billing_address"=> array(
            			"address"=> "",
            			"country"=> "IN",
            			"state"=> "f49337bc-a6ea-4183-89a4-3cb9c692e361",
            			"state_name"=> "Delhi",
            			"city"=> "",
            			"city_name"=> "",
            			"pin"=> "110052",
            			"phone"=> "",
            			"email"=> "",
            			"longitude"=>"",
            			"latitude"=>""
                		),
                		"shipping_address"=>array (
                			"address"=> "",
                			"country"=> "IN",
                			"state"=> "",
                			"state_name"=> "",
                			"city"=> "",
                			"city_name"=> "",
                			"pin"=> "",
                			"phone"=> "",
                			"email"=> $email,
                			"longitude"=> "",
                			"latitude"=> ""
                		),
                		"udf_list"=> ["", "", "", "", ""],
                		"fl_names"=> [],
                		"code_config"=> array(
                			"code"=> "",
                			"code_no"=> 0,
                			"code_prefix"=> ""
                		)
                	)
                );
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            if(User::where('email', $request->email)->first() != null){
                flash('Email already exists.');
                return back();
            }
            //dd('hi');
            $this->validator($request->all())->validate();
             $endPoint = 'SaveUpdate_Customer';
            $addCustomeInAlignBook = $this->addCustomerInAlignBook(json_encode($postJsonData),$endPoint);
           //dd(json_encode($postJsonData),$addCustomeInAlignBook);
            $saveUser = new User;
            $saveUser->alignbook_customer_id = $uniqueid;
            $saveUser->name = $name;
            $saveUser->email = $email;
            $saveUser->password = Hash::make($request->password);
            $jsondRepons = json_encode($addCustomeInAlignBook,true);
          //dd(json_decode($jsondRepons));
          $respondData = json_decode($jsondRepons);
         if($respondData->ReturnCode == 0){
            $saveUser->save();
         }
           
           // dd($user->name);
            $this->guard()->login($saveUser);
            $cookie = cookie('name', $saveUser->name, 180);
            return $this->registered($request, $saveUser);
            //?: redirect($this->redirectPath())->cookie($cookie);
            
        } elseif (User::where('phone',$request->email)->first() != null) {
            flash('Phone already exists.');
            return back();
        } else {
            
             $addCustomeInAlignBook = $this->addCustomerInAlignBook(json_encode($postJsonData),$endPoint);
       
            //event(new Registered($user = $this->create($request->all())));
           
            $saveUser = new Registered;
            $saveUser->alignbook_customer_id = $uniqueid;
            $saveUser->name = $name;
            $saveUser->email = $email;
            $saveUser->password = $password;
            $jsondRepons = json_encode($addCustomeInAlignBook,true);
            $respondData = json_decode($jsondRepons);
             if($respondData->ReturnCode == 0){
                $saveUser->save();
             }
            //dd('hi i am here');
            $this->guard()->login($user);
            $cookie = cookie('name', $user->name, 180);
            return $this->registered($request, $user);
        }
        
    }

    protected function registered(Request $request, $user)
    {
       // if ($user->email == null) {
       //     return redirect()->route('verification');
       // }
      //  else {
            $cookie = cookie('name', $user->name, 180);
            return redirect()->route('home')->cookie($cookie);
      //  }
    }
    
    public function addCustomerInAlignBook($requestJson,$endPoint){
	   // dd($requestJson);
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
       return  $data = json_decode($response1);
	}
}
