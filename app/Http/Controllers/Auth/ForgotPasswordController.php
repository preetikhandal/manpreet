<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
       
        
       
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $this->validateEmail($request);
        $validatedData = $request->validate([
			'email' => 'email:rfc,dns'
			
		]);
            // We will send the password reset link to this user. Once we have attempted
            // to send the link, we will examine the response then see the message we
            // need to show to the user. Finally, we'll send out a proper response.
            $response = $this->broker()->sendResetLink(
                $this->credentials($request)
            );
//dd($response." ~ ".Password::RESET_LINK_SENT);
             if($response == Password::RESET_LINK_SENT){
                 flash('Reset Password link is successfully sent to your Registered Email Id.')->success();
                     return redirect()->route('user.login');
             } else {
                  flash('Please Enter Registed Email Id.')->error();
                      return $this->sendResetLinkFailedResponse($request, $response);
             }
                        
        }
        elseif (is_numeric($request->input('email'))){
            if(strlen($request->email)!=10 ){
            flash('Enter the Valid phone number.')->error();
                return back();
                
            }
            $user = User::where('phone', $request->email)->first();
            
                if ($user != null) {
                     $login_otp = random_int(100000, 999999);
                    $login_otp_expire_time = time() + 310;
                    session(['login_otp' => $login_otp]);
                    session(['login_otp_expire_time' => $login_otp_expire_time]);
                    session(['login_phone' => $request->email]);
                    $smsMsg = "OTP is ".$login_otp." and is valid for 5 minutes.";
                    $smsMsg = rawurlencode($smsMsg);
                    $to = "91".$request->email;
                    if(strlen($to) == 12) {
                     $SMS_URL = env('SMS_URL', null);
                        if($SMS_URL != null) {
                            $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                            file_get_contents($SMS_URL);
                        }
                }
                flash('OTP sent to your register phone number.')->success();
                return view('otp_systems.frontend.auth.passwords.reset_with_phone');
            }
            else {
                flash('No account exists with this phone number.')->error();
                return back();
            }
        }else{
            flash('Enter the valid email id.')->error();
                return back();
        }
    }
}
