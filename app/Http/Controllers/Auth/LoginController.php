<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use App\UserCartSession;
use App\Customer;
use Illuminate\Http\Request;
use Auth;
use Mail;
use Cookie;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    /*protected $redirectTo = '/';*/


    /**
      * Redirect the user to the Google authentication page.
      *
      * @return \Illuminate\Http\Response
      */
    public function redirectToProvider($provider)
    {
        if(stristr(url()->previous(),"cart")) {
            session(['link' => url()->previous()]);
        }
        $re = Socialite::driver($provider)->redirect();
        return $re;
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request, $provider = 'apple')
    {
        try {
            if($provider == 'twitter'){
                $user = Socialite::driver('twitter')->user();
            } else {
                $user = Socialite::driver($provider)->stateless()->user();
            }
        } catch (\Exception $e) {
            flash("Something Went wrong. Please try again.")->error();
            return redirect()->route('user.login');
        }
        
        // check if they're an existing user
        $existingUser = User::where('provider_id', $user->id)->orWhere('email', $user->email)->first();

        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            if($user->name == null) {
                $user->name = $user->email;
            }
            // create a new user
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->email_verified_at = date('Y-m-d H:m:s');
            $newUser->provider_id     = $user->id;

            // $extension = pathinfo($user->avatar_original, PATHINFO_EXTENSION);
            // $filename = 'uploads/users/'.str_random(5).'-'.$user->id.'.'.$extension;
            // $fullpath = 'public/'.$filename;
            // $file = file_get_contents($user->avatar_original);
            // file_put_contents($fullpath, $file);
            //
            // $newUser->avatar_original = $filename;
            $newUser->save();

            $customer = new Customer;
            $customer->user_id = $newUser->id;
            $customer->save();

           $data = array('email' => $user->email, 'name' => $user->name);
                
            Mail::send('emails.user_register', ['data' => $data], function ($m) use ($data) {
                        $m->to($data['email'], $data['name'])->subject('Alde Bazaar :: User Registration Confirmation');
                    });


            auth()->login($newUser, true);
        }
        
        if(Auth::User()) {
        $name = Auth::User()->name;
            $cookie = cookie('name', $name, 180);
        }
        
        $user_id = Auth()->User()->id;
        $UserCartSession = UserCartSession::where('user_id', $user_id)->first();
        $cart_items_count = 0;
        if($UserCartSession != null) {
            $cart = json_decode($UserCartSession->cart, true);
            if(session('cart') != null) {
                $sessionCart = session('cart');
                foreach($cart as $c) {
                    foreach($sessionCart  as $key=>$sc) {
                        if($c['id'] == $sc['id']) {
                            $cart_items_count = $c['quantity'] + $sc['quantity'] + $cart_items_count;
                            $cart[$key]['quantity'] = $c['quantity'] + $sc['quantity'];
                            unset($sessionCart[$key]);
                        }
                    }
                    if(!isset($cart[$key]['coupon_category_discount'])){
                        $cart[$key]['coupon_category_discount'] = 0; 
                    }
                }
                foreach($sessionCart as $sc) {
                    array_push($cart, $sc);
                }
            } else {
                $cart = json_decode($UserCartSession->cart, true);
                foreach($cart as $key=>$c) {
                    if(!isset($cart[$key]['coupon_category_discount'])){
                        $cart[$key]['coupon_category_discount'] = 0; 
                    }
                }
            }
            
            $cart = collect($cart);
            session(['cart' => $cart ]);
        }
        if(session('link') != null){
             if(session('mobileapp') != null) {
                $mobileapp = session('mobileapp');
                if($mobileapp == 1) {
                    return redirect(session('link')."?android=1")->cookie($cookie);
                } else {
                    return redirect(session('link'))->cookie($cookie);
                }
             } else {
                 return redirect(session('link'))->cookie($cookie);
             }
        } else {
            if(session('mobileapp') != null) {
                $mobileapp = session('mobileapp');
                if($mobileapp == 1) {
                   return redirect()->route('dashboard', ['android' => 1])->cookie($cookie);
                } else {
                   return redirect()->route('dashboard')->cookie($cookie);
                }
             } else {
                 return redirect()->route('dashboard')->cookie($cookie);
             }
        }
        
    }

    /**
        * Get the needed authorization credentials from the request.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return array
        */
       protected function credentials(Request $request)
       {
           if(filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)){
               return $request->only($this->username(), 'password');
           }
           return ['phone'=>$request->get('email'),'password'=>$request->get('password')];
       }

    /**
     * Check user's role and redirect user based on their role
     * @return
     */
    public function authenticated()
    {
        if(auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'staff')
        {
            $cookie = cookie('name', auth()->user()->name, 180);
            return redirect()->route('admin.dashboard')->cookie($cookie);
        }
        elseif(session('link') != null){
            $cookie = cookie('name', auth()->user()->name, 180);
            return redirect(session('link'))->cookie($cookie);
        }
        else{
            $cookie = cookie('name', auth()->user()->name, 180);
            return redirect()->route('dashboard')->cookie($cookie);
        }
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
     /*
    protected function sendFailedLoginResponse(Request $request)
    {
        flash(__('Invalid email or password'))->error();
        return back();
    }
    */
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        if(session('cart') != null) {
            $user_id = Auth()->User()->id;
            UserCartSession::where('user_id', $user_id)->delete();
            $UserCartSession = new UserCartSession;
            $UserCartSession->user_id = $user_id;
            $UserCartSession->cart = json_encode(session('cart'));
            $UserCartSession->save();
        }
        
        if(auth()->user() != null && (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'staff')){
            $redirect_route = 'login';
        }
        else{
            $redirect_route = 'home';
        }
        if(session('mobileapp') != null) {
        $mobileapp = session('mobileapp');
        } else {
            $mobileapp = 0;
        }

        $this->guard()->logout();

        $request->session()->invalidate();
        Cookie::queue('cart_items_count', 0);
        $cookie = cookie('name', "", 0);
        if($mobileapp == 1) {
           $redirect_route = route($redirect_route)."?android=1"; 
        } else {
            $redirect_route = route($redirect_route);
        }
        return $this->loggedOut($request) ?: redirect($redirect_route)->cookie($cookie);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
