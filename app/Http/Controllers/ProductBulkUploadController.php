<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\Brand;
use App\User;
use App\ProductUploadQueue;
use Auth;
use App\ProductsImport;
use App\ProductsImportImg;
use App\ProductsImportUpdate;
use App\ProductsExport;
use PDF;
use Excel;
use File;
use Illuminate\Support\Str;
use Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use App\SellerCommision;


class ProductBulkUploadController extends Controller
{
    public function index()
    {
        if (Auth::user()->user_type == 'seller') {
            return view('frontend.seller.product_bulk_upload.index');
        }
        elseif (Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff') {
            return view('bulk_upload.index');
        }
    }
    
    public function list() {
        $ProductUploadQueue = ProductUploadQueue::paginate(20);
        $data = array("ProductUploadQueue" => $ProductUploadQueue);
        return view('bulk_upload.list', $data);
    }
    
    public function process() {
        $imgg = asset('frontend/images/product-thumb.jpg');
        $img = @file_get_contents('http://143.110.191.24/api/image?url='.$imgg.'&width=100&height=100&format=jpg');
        if($img === false) {
            dd("Image Server not able to get connected, Kindly let to know to Server Administrator.");
        }
        //dd($row);
        $ProductUploadQueue = ProductUploadQueue::paginate(10);
        foreach($ProductUploadQueue as $P) {
            $thumbnail_img = null;
    		$featured_img = null;
    		$flash_deal_img = null;
    		$meta_img = null;
            $productName = $P->name;
            $P->trycount = $P->trycount + 1;
            $P->save();
    		$trycount = $P->trycount;
    		$ProductCount = Product::where('product_id',$P->product_id)->count();
    		if ($ProductCount === 0 ) {
        		if ($P->meta_title == "" ){
        		    $meta_title="Buy ".$productName." Online at Low Prices in India - Aldebazaar.com";
        		} else {
        		   $meta_title= $P->meta_title;
        		}
        		
        		if($P->meta_description == "" ) {
        		    $meta_description="Aldebazaar.com Buy ".$productName." online at low price in India on Aldebazaar.com. Check out ".$productName." reviews, specifications and more at Aldebazaar.com. Cash on Delivery Available.";
        		} else {
        		   $meta_description= $P->meta_description;
        		}
    			$description = "";
    			$photos = array();
    			$thumbnail = "";
    			if($trycount == 1) {
    			    $thumb = $P->thumb;
    			} else {
    			    $thumb = "";
    			}
    			if($thumb != "") {
    			    $part = explode(".", $thumb);
        			$extension = end($part);
        			$extension = strtolower($extension);
    			    $thumbnail = Str::slug($productName,"-")."-thumb-".time().".".$extension;
    			    $featured = Str::slug($productName,"-")."-featured-".time().".".$extension;
    			    $flash_deal = Str::slug($productName,"-")."-flash_deal-".time().".".$extension;
    			    $meta = Str::slug($productName,"-")."-meta-".time().".".$extension;
    			    $response = Http::get($thumb);
                    if($response->status() == 200) {
                        $img = @file_get_contents('http://143.110.191.24/api/image?url='.$thumb.'&width=200&height=200&format=jpg');
                    } else {
                        $img = false;
                    }
                    if($img !== false) {
                        file_put_contents(base_path('public/').'temp/'.$thumbnail, $img);
                        if(env('FILESYSTEM_DRIVER') == "local") {
                            copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/products/thumbnail/'.$thumbnail);
                        } else {
                            Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/thumbnail', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
                        }
                        $thumbnail_img = "uploads/products/thumbnail/".$thumbnail;
                        unlink(base_path('public/').'temp/'.$thumbnail);
                    }
                    if($response->status() == 200) {
                        $img = @file_get_contents('http://143.110.191.24/api/image?url='.$thumb.'&width=200&height=200&format=jpg');
                    } else {
                        $img = false;
                    }
                    if($img !== false) {
                    file_put_contents(base_path('public/').'temp/'.$featured, $img);
        			
                    if(env('FILESYSTEM_DRIVER') == "local") {
                        copy(base_path('public/').'temp/'.$featured, base_path('public/').'uploads/products/featured/'.$featured);
                    } else {
                        Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/featured', new \Illuminate\Http\File(base_path('public/').'temp/'.$featured), $featured);
                    }
                    $featured_img = "uploads/products/featured/".$featured;
                    unlink(base_path('public/').'temp/'.$featured);
                    }
                    
                    if($response->status() == 200) {
                        $img = @file_get_contents('http://143.110.191.24/api/image?url='.$thumb.'&width=200&height=200&format=jpg');
                    } else {
                        $img = false;
                    }
                    if($img !== false) {
                        file_put_contents(base_path('public/').'temp/'.$flash_deal, $img);
                        if(env('FILESYSTEM_DRIVER') == "local") {
                            copy(base_path('public/').'temp/'.$flash_deal, base_path('public/').'uploads/products/flash_deal/'.$flash_deal);
                        } else {
                            Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/flash_deal', new \Illuminate\Http\File(base_path('public/').'temp/'.$flash_deal), $flash_deal);
                        }
                        $flash_deal_img = "uploads/products/flash_deal/".$flash_deal;
                        unlink(base_path('public/').'temp/'.$flash_deal);
                    }
                    
                    if($response->status() == 200) {
                        $img = @file_get_contents('http://143.110.191.24/api/image?url='.$thumb.'&width=200&height=200&format=jpg');
                    } else {
                        $img = false;
                    }
                    if($img !== false) {
                        file_put_contents(base_path('public/').'temp/'.$meta, $img);
            			
                        if(env('FILESYSTEM_DRIVER') == "local") {
                            copy(base_path('public/').'temp/'.$meta, base_path('public/').'uploads/products/meta/'.$meta);
                        } else {
                            Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/meta', new \Illuminate\Http\File(base_path('public/').'temp/'.$meta), $meta);
                        }
                        $meta_img = "uploads/products/meta/".$meta;
                        unlink(base_path('public/').'temp/'.$meta);
                    }
                    
                    $mainimage = Str::slug($productName,"-").time().rand(1111, 9999).".".$extension;
                    
                    if($response->status() == 200) {
                        $img = @file_get_contents('http://143.110.191.24/api/image?url='.$thumb.'&width=200&height=200&format=jpg');
                    } else {
                        $img = false;
                    }
                    if($img !== false) {
                        file_put_contents(base_path('public/').'temp/'.$mainimage, $img);
        				if (env('FILESYSTEM_DRIVER') == "local") {
        					copy(base_path('public/').'temp/'.$mainimage, base_path('public/').'uploads/products/photos/'.$mainimage);
        				} else {
        					Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/photos', new \Illuminate\Http\File(base_path('public/').'temp/'.$mainimage), $mainimage);
        				}
        				array_push($photos, 'uploads/products/photos/'.$mainimage);
        				unlink(base_path('public/').'temp/'.$mainimage);
                    }
    			}
                
    			if($trycount == 1) {
    			    $photosss = $P->photos;
    			} else {
    			    $photosss = "";
    			}
            if($photosss != "") {
                $main_images = explode(";", $P->photos);
                foreach ($main_images as $main_image) {
                    $part = explode(".", $main_image);
        			$extension = end($part);
        			$extension = strtolower($extension);
        			$mainimage = Str::slug($productName,"-").time().rand(1111, 9999).".".$extension;
        			$response = Http::get($main_image);
                    if($response->status() == 200) {
                    $img = @file_get_contents('http://143.110.191.24/api/image?url='.$main_image.'&width=800&height=800&format=jpg');
                    } else {
                        $img = false;
                    }
                    if($img !== false) {
                        file_put_contents(base_path('public/').'temp/'.$mainimage, $img);
                        
        				if (env('FILESYSTEM_DRIVER') == "local") {
        					copy(base_path('public/').'temp/'.$mainimage, base_path('public/').'uploads/products/photos/'.$mainimage);
        				} else {
        					Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/photos', new \Illuminate\Http\File(base_path('public/').'temp/'.$mainimage), $mainimage);
        				}
        				array_push($photos, 'uploads/products/photos/'.$mainimage);
        				unlink(base_path('public/').'temp/'.$mainimage);
                    }
    			}
            }
                
    			if(empty($photos)) {
    			    $photos = null;
    			}
    			$ProductCount = Product::where('product_id',$P->product_id)->count();
    		    if ($ProductCount === 0 ) {
    			$product = Product::create([
    			   'product_id'     => $P->product_id,
    			   'name' => $productName,
    			   'added_by'  => Auth::user()->user_type == 'seller' ? 'seller' : 'admin',
    			   'user_id'  => Auth::user()->user_type == 'seller' ? Auth::user()->id : User::where('user_type', 'admin')->first()->id,
    			   'category_id'    => $P->category_id,
    			   'subcategory_id'    => $P->subcategory_id,
    			   'subsubcategory_id'    => $P->subsubcategory_id,
    			   'brand_id'    => $P->brand_id,
    			   'video_provider'    => "",
    			   'video_link'    => "",
    			   'unit_price'    => $P->unit_price,
    			   'purchase_price'    => $P->purchase_price,
    			   'shipping_cost'    => $P->shipping_cost,
    			   'unit'    => $P->unit,
    			   'current_stock' => $P->current_stock, 
    			   'discount' => $P->discount,
    			   'discount_type' => $P->discount_type,
    			   'tax' => $P->tax,
    			   'tax_type' => $P->tax_type,
    			   'meta_title' => $meta_title,               
    			   'meta_description' => $meta_description,   
    			   'colors' => json_encode(array()),            
    			   'choice_options' => json_encode(array()),    
    			   'variations' => json_encode(array()),        
    			   'slug' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $productName)).'-'.str_random(5)),
    			   'description' => $P->description,
    			   'photos' => json_encode($photos),
    			   'salt' => $P->salt,
    			   'thumbnail_img' => $thumbnail_img,
    			   'featured_img' => $featured_img,
    			   'flash_deal_img' => $flash_deal_img,
    			   'meta_img' => $meta_img,
    			   'tags' => $P->tags,
    			   'variation' => json_encode(array()),
    			   'variation_order' => json_encode(array()),
    			   'variation_combinations' => json_encode(array()),
    			   'variation_product' => json_encode(array()),
    			]);
    			    echo "Proceesed ".$P->product_id;
    		    } else {
    		        echo "Product already in Database ".$P->product_id;
    		    }
    			echo "<br />";
    			$P->delete();
    		} else {
    			echo "Product already in Database ".$P->product_id;
    			echo "<br />";
    			$P->delete();
    		}
        }
        if($ProductUploadQueue->hasPages()) {
            $ProductCount = $ProductUploadQueue->total();
           echo "Page Will refresh, Total Products to be proccessed ". ( $ProductCount - 10 ) ;
           echo '<meta http-equiv="refresh" content="5">'; 
        } else {
            echo "Queue completed";
        }
    }

    public function export(){
        return Excel::download(new ProductsExport, 'products.xlsx');
    }

    public function pdf_download_category()
    {
        if(Auth::user()->id){
             $sellerId = Auth::user()->id;      
        }else{
          return redirect()->route('sellers.index');
        }
        $SellerCommision = SellerCommision::where('user_id',$sellerId)->get();
        $sellercatId = [];
        foreach($SellerCommision as $catId){
            array_push($sellercatId,$catId->category_id);
        }
        //print_r($sellercatId);die;
        $categories = Category::whereIn('id',$sellercatId)->get();
        
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('downloads.category', compact('categories'));

        return $pdf->download('category.pdf');
    }

    public function pdf_download_sub_category()
    {
        if(Auth::user()->id){
             $sellerId = Auth::user()->id;      
        }else{
          return redirect()->route('sellers.index');
        }
        $SellerCommision = SellerCommision::where('user_id',$sellerId)->get();
        $sellercatId = [];
        foreach($SellerCommision as $catId){
            array_push($sellercatId,$catId->category_id);
        }
        //print_r($sellercatId);die;
       
        $sub_categories = Subcategory::whereIn('category_id',$sellercatId)->get();
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('downloads.sub_category', compact('sub_categories'));

        return $pdf->download('sub_category.pdf');
    }

    public function pdf_download_sub_sub_category()
    {
        if(Auth::user()->id){
             $sellerId = Auth::user()->id;      
        }else{
          return redirect()->route('sellers.index');
        }
        $SellerCommision = SellerCommision::where('user_id',$sellerId)->get();
        $sellercatId = [];
        foreach($SellerCommision as $catId){
            array_push($sellercatId,$catId->category_id);
        }
        
       
        $sub_categories = Subcategory::whereIn('category_id',$sellercatId)->get();
        $subcategoryIdsArray = [];
        foreach($sub_categories as $subCategoryIds){
            array_push($subcategoryIdsArray,$subCategoryIds->id);
        }
        
        $sub_sub_categories = SubSubCategory::whereIn('sub_category_id',$subcategoryIdsArray)->get();
       
        $subsubcategoryIdsArray = [];
        foreach($sub_sub_categories as $subsubcategoryIds){
            //dd($subsubcategoryIds->id);
             array_push($subsubcategoryIdsArray,$subsubcategoryIds->id);
        }
       //Array match susubcatgory with seller comission
       
        $SellerCommisionSubsubcat = SellerCommision::whereIn('subsubcategory_id',$subsubcategoryIdsArray)->where('user_id',$sellerId)->get();
        $subsubsellercatId = [];
        foreach($SellerCommisionSubsubcat as $subsubcatId){
            array_push($subsubsellercatId,$subsubcatId->subsubcategory_id);
        }
       $subsubcatDetail =  $sub_sub_categories = SubSubCategory::whereIn('id',$subsubsellercatId)->get();
     
        //$sub_sub_categories = SubSubCategory::all();
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('downloads.sub_sub_category', compact('subsubcatDetail'));

        return $pdf->download('sub_sub_category.pdf');
    }

    public function pdf_download_brand()
    {
        $brands = Brand::all();
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('downloads.brand', compact('brands'));
        return $pdf->download('brands.pdf');
    }

    public function pdf_download_seller()
    {
        $users = User::where('user_type','seller')->get();
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('downloads.user', compact('users'));

        return $pdf->download('user.pdf');

    }

    public function bulk_upload(Request $request)
    {  
       
        $validatedData = $request->validate([
			'bulk_file' => 'file|mimes:xlsx,xls'
		]);
        $validatedData = $request->validate([
			'bulk_file' => 'file|max:10240'
		]);
        if($request->hasFile('bulk_file')){
          $save =  Excel::import(new ProductsImport, request()->file('bulk_file'));
          
            if($save){
              flash('Products imported successfully')->success();  
            }else{
               flash('You category is not approve from adlbazzar so please ask to approve then try')->error();     
            }
              
        }else{
            flash('Please selecte csv file')->error();
        }
    
		//$output=array('success' => true,'mesg'=>'Products imported successfully');
       return back();
		//return response()->json($output);
    }

    public function bulk_upload_img(Request $request)
    {
        $validatedData = $request->validate([
			'bulk_file' => 'file|mimes:xlsx,xls'
		]);
        $validatedData = $request->validate([
			'bulk_file' => 'file|max:10240'
		]);
        if($request->hasFile('bulk_file')){
           Excel::import(new ProductsImportImg, request()->file('bulk_file'));
        }
       flash('Products imported successfully')->success();
		//$output=array('success' => true,'mesg'=>'Products imported successfully');
       return back();
		//return response()->json($output);
    }

    public function bulk_update(Request $request)
    {
        $validatedData = $request->validate([
			'bulk_file' => 'file|mimes:xlsx,xls'
		]);
        $validatedData = $request->validate([
			'bulk_file' => 'file|max:10240'
		]);
        if($request->hasFile('bulk_file')){
           Excel::import(new ProductsImportUpdate, request()->file('bulk_file'));
        }
       flash('Products updated successfully')->success();
       return back();
    }
    
    public function remove(Request $request) {
        $product = ProductUploadQueue::find($request->id);
        $product->delete();
        flash(__('Product has been deleted from bulk upload queue.'))->success();
        return back();
    }
    
    public function clear(Request $request) {
        $product = ProductUploadQueue::truncate();
        flash(__('Product has been cleared from bulk upload queue.'))->success();
        return back();
    }
    
    
    public function addUpdateIteam($requestJson,$endPoint){
	   // dd($requestJson);
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
        $data = json_decode($response1);
        return $data; 
	}

}
