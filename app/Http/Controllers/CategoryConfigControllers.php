<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\CategoryConfig;
use App\CategoryConfig;
use App\CategoryConfigProduct;
use Illuminate\Support\Facades\Cache;
use ImageOptimizer;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
class CategoryConfigControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        $sort_search =null;
        
        $category_config = CategoryConfig::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $category_config = $flash_deals->where('title', 'like', '%'.$sort_search.'%');
        }
        $category_config = $category_config->paginate(15);
        return view('category_config.index', compact('category_config', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category_config.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
			'title' => 'required|min:3|max:255',
			'background_color' => 'required|min:2|max:255',
			'text_color' => 'required|min:2|max:255',
			'products' => 'required'
		]);
        $flash_deal = new CategoryConfig;
        $flash_deal->title = $request->title;
        $flash_deal->text_color = $request->text_color;
        $flash_deal->start_date = strtotime($request->start_date);
        $flash_deal->end_date = strtotime($request->end_date);
        $flash_deal->background_color = $request->background_color;
        $flash_deal->slug = strtolower(str_replace(' ', '-', $request->title));
        $flash_deal->slug = str_ireplace('--','-',$flash_deal->slug);
        if($request->hasFile('banner')){
            $flash_deal->banner = $request->file('banner')->store('uploads/offers/banner');
        }
        
        if($request->hasFile('bg_img')){
            $extension = strtolower($request->bg_img->extension());
            if($extension == "jpg" or $extension == "png" or $extension == "gif" or  $extension == "jpeg")
			{
                $thumbnail = Str::slug($request->title,"-")."back".time().".".$extension;
                $img = Image::make($request->bg_img);
    			$width = $img->width();
    			$height = $img->height();
    			if($width == 222 && $height == 250) {
    			    $request->bg_img->move(base_path('public/')."temp/",$thumbnail);
    			} else {
        			if($width <= 222 && $height <= 250) {
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			} else {
        				 $img->resize(222, 250, function ($constraint) {
        					$constraint->aspectRatio();
        				});
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			}
                    $img->save(base_path('public/')."temp/".$thumbnail, 100);
    			}
                ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);
                if(env('FILESYSTEM_DRIVER') == "local") {
                    copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/offers/banner/'.$thumbnail);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/offers/banner/', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
                }
                $flash_deal->background_img = "uploads/offers/banner/".$thumbnail;
                unlink(base_path('public/').'temp/'.$thumbnail);
			}
        }

        if($flash_deal->save()){
            foreach ($request->products as $key => $product) {
                $flash_deal_product = new CategoryConfigProduct;
                $flash_deal_product->flash_deal_id = $flash_deal->id;
                $flash_deal_product->product_id = $product;
                $flash_deal_product->discount = $request['discount_'.$product];
                $flash_deal_product->discount_type = $request['discount_type_'.$product];
                $flash_deal_product->save();
            }
			Cache::forget('flash_deal');
            flash(__('Flash Deal has been inserted successfully'))->success();
            return redirect()->route('category_config.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $flash_deal = CategoryConfig::findOrFail(decrypt($id));
        return view('category_config.edit', compact('flash_deal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
			'title' => 'required|min:3|max:255',
			'background_color' => 'required|min:2|max:255',
			'text_color' => 'required|min:2|max:255',
			'products' => 'required'
		]);
		$start_date=strtotime($request->start_date);
		$end_date=strtotime($request->end_date);
		
        $flash_deal = CategoryConfig::findOrFail($id);
        $flash_deal->title = $request->title;
        $flash_deal->text_color = $request->text_color;
        $flash_deal->start_date = strtotime($request->start_date);
        $flash_deal->end_date = strtotime($request->end_date);
        $flash_deal->background_color = $request->background_color;
        if (($flash_deal->slug == null) || ($flash_deal->title != $request->title)) {
            $flash_deal->slug = strtolower(str_replace(' ', '-', $request->title));
        }
        $flash_deal->slug = str_ireplace('--','-',$flash_deal->slug);
        if($request->hasFile('banner')){
            $flash_deal->banner = $request->file('banner')->store('uploads/offers/banner');
        }
        foreach ($flash_deal->category_config_products as $key => $flash_deal_product) {
            $flash_deal_product->delete();
        }
        
        if($request->hasFile('bg_img')){
            $extension = strtolower($request->bg_img->extension());
            if($flash_deal->background_img!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$flash_deal->background_img))
                    unlink(base_path('public/').$flash_deal->background_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($flash_deal->background_img);
                }
			}
            if($extension == "jpg" or $extension == "png" or $extension == "gif" or  $extension == "jpeg")
			{
                $thumbnail = Str::slug($request->title,"-")."back".time().".".$extension;
                $img = Image::make($request->bg_img);
    			$width = $img->width();
    			$height = $img->height();
    			if($width == 222 && $height == 250) {
    			    $request->bg_img->move(base_path('public/')."temp/",$thumbnail);
    			} else {
        			if($width <= 222 && $height <= 250) {
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			} else {
        				 $img->resize(222, 250, function ($constraint) {
        					$constraint->aspectRatio();
        				});
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			}
                    $img->save(base_path('public/')."temp/".$thumbnail, 100);
    			}
                ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);
                if(env('FILESYSTEM_DRIVER') == "local") {
                    copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/offers/banner/'.$thumbnail);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/offers/banner/', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
                }
                $flash_deal->background_img = "uploads/offers/banner/".$thumbnail;
                unlink(base_path('public/').'temp/'.$thumbnail);
			}
        }

        if($flash_deal->save()){
            foreach ($request->products as $key => $product) {
                $flash_deal_product = new CategoryConfigProduct;
                $flash_deal_product->flash_deal_id = $flash_deal->id;
                $flash_deal_product->product_id = $product;
                $flash_deal_product->discount = $request['discount_'.$product];
                $flash_deal_product->discount_type = $request['discount_type_'.$product];
                $flash_deal_product->save();
            }
			Cache::forget('flash_deal');
            flash(__('Category Config has been updated successfully'))->success();
            return redirect()->route('category_config.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $flash_deal = CategoryConfig::findOrFail($id);
        foreach ($flash_deal->CategoryConfigProduct as $key => $flash_deal_product) {
            $flash_deal_product->delete();
        }
        if($flash_deal->background_img != null){
		    if(env('FILESYSTEM_DRIVER') == "local") {
                if(file_exists(base_path('public/').$flash_deal->background_img))
                unlink(base_path('public/').$flash_deal->background_img);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($flash_deal->background_img);
            }
            $flash_deal->background_img = null;
        }
        if(CategoryConfig::destroy($id)){
			Cache::forget('flash_deal');
            flash(__('Category Config has been deleted successfully'))->success();
            return redirect()->route('category_config.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function update_status(Request $request)
    {
        $flash_deal = CategoryConfig::findOrFail($request->id);
        $flash_deal->status = $request->status;
        if($flash_deal->save()){
			Cache::forget('category_config');
            flash(__('Category Config status updated successfully'))->success();
            return 1;
        }
        return 0;
    }

    public function update_featured(Request $request)
    {
        foreach (CategoryConfig::all() as $key => $flash_deal) {
            $flash_deal->featured = 0;
            $flash_deal->save();
        }
        $flash_deal = CategoryConfig::findOrFail($request->id);
        $flash_deal->featured = $request->featured;
        if($flash_deal->save()){
			Cache::forget('category_config');
            flash(__('Category Config status updated successfully'))->success();
            return 1;
        }
        return 0;
    }

    public function product_discount(Request $request){
        $product_ids = $request->product_ids;
        return view('partials.flash_deal_discount', compact('product_ids'));
    }

    public function product_discount_edit(Request $request){
        $product_ids = $request->product_ids;
        $flash_deal_id = $request->flash_deal_id;
        return view('partials.flash_deal_discount_edit', compact('product_ids', 'flash_deal_id'));
    }
    
   
}
