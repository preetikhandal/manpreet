<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BestSelling;
use App\HomeCategory;
use App\Category;
use Illuminate\Support\Facades\Cache;
use App\Product;

class BestSellingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $best_selling = BestSelling::all();
        return view('best_selling.index', compact('best_selling'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('best_selling.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bestselling=BestSelling::all();
        foreach($bestselling as $bs){
            $bs->background_color = $request->bgcolor;
            $bs->text_color = $request->textcolor;
            $bs->save();
        }
        
        $best_selling = new BestSelling;
        $best_selling->category_id = $request->category_id;
        $best_selling->products = json_encode($request->products);
        if($best_selling->save()){
			Cache::forget('bestselling');
            flash(__('Best Selling Product(s) has been inserted successfully'))->success();
            return redirect()->route('home_settings.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bestSelling = BestSelling::findOrFail($id);
        return view('best_selling.edit', compact('bestSelling'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bestselling=BestSelling::all();
        foreach($bestselling as $bs){
            $bs->background_color = $request->bgcolor;
            $bs->text_color = $request->textcolor;
            $bs->save();
        }
		$best_selling = BestSelling::findOrFail($id);
		$best_selling->background_color = $request->bgcolor;
        $best_selling->text_color = $request->textcolor;
        $best_selling->category_id = $request->category_id;
        $best_selling->products = json_encode($request->products);
        if($best_selling->save()){
			Cache::forget('bestselling');
            flash(__('Best Selling Product(s) has been inserted successfully'))->success();
            return redirect()->route('home_settings.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(BestSelling::destroy($id)){
			Cache::forget('bestselling');
            flash(__('Best Selling Category has been deleted successfully'))->success();
            return redirect()->route('home_settings.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function update_status(Request $request)
    {
        $best_selling = BestSelling::findOrFail($request->id);
        $best_selling->status = $request->status;
        if($best_selling->save()){
			Cache::forget('bestselling');
            return 1;
        }
        return 0;
    }

   /* public function getSubSubCategories(Request $request)
    {
        $result = array();
        $subcategories = Category::find($request->category_id)->subcategories;
        foreach ($subcategories as $key => $subcategory) {
            foreach ($subcategory->subsubcategories as $key => $subsubcategory) {
                $subsubcategory['number_of_products'] = count($subsubcategory->products);
                array_push($result, $subsubcategory);
            }
        }
        return $result;
    }
    */
	 public function getproductsbySubSubCategories(Request $request)
    {
        $result = array();
        $category_id = $request->category_id;
        $subsubcategory_id = $request->subsubcategory_id;
        $product = Product::where('category_id',$category_id)->where('subsubcategory_id',$subsubcategory_id)->get();
        return $product;
    }

}
