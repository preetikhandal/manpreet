<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExpensesCategory;
use App\Expenses;
use App\ExpensesDetails;
use App\ExpensesPayments;
use Auth;
use Carbon\Carbon;
use Mail;
use App\Mail\AlertEmailManager;

class ExpensesController extends Controller
{
	public function Expenses(){
		$ExpensesCategory=ExpensesCategory::get();
		$Expenses=Expenses::paginate( env('RECORDPERPAGE', '10'));
		$data = array("categories"=>$ExpensesCategory,"Expenses"=>$Expenses);
		return view('expenses.Expenses',$data);
	}
    
	function Recaluclate($ExpenseID){
		$Expenses = Expenses::find($ExpenseID);
		$InvoiceAmount=$Expenses->ExpensesInvoiceAmount;
		$Paid_amount_old=$Expenses->PaidAmount;
		$ExpensesPayments = ExpensesPayments::where('ExpenseID',$ExpenseID)->get();
		if ($ExpensesPayments == NULL) {
			$Total_PaidAmount=0;
			$OutstandingAmount=$InvoiceAmount;
		} else {
			$Expense_Payment=$ExpensesPayments->pluck('ExpensesPaidAmount');
			$Total_PaidAmount=$Expense_Payment->sum();
			$OutstandingAmount=$InvoiceAmount-$Total_PaidAmount;
		}
		
		if($OutstandingAmount==0) $Payment_Status="Paid"; 
		elseif ($Total_PaidAmount==0) $Payment_Status="Unpaid";
		else $Payment_Status="Partially Paid";
		$data=array("PaymentStatus"=>$Payment_Status,"OutstandingAmount"=>$OutstandingAmount,"PaidAmount"=>$Total_PaidAmount);
		return $data;
	}
    
	public function CreateExpense(){
	    
		$ExpensesCategory=ExpensesCategory::get();
		$data = array("categories"=>$ExpensesCategory);
		return view('expenses.ExpensesAdd',$data);
	}
    
	public function SaveExpense(Request $request){
	     $validatedData = $request->validate([
			'expensename' => 'required|min:3|max:30',
			'invoicedate' => 'required',
			'expenses_type' => 'required',
			'invoiceno' => 'required',
			'unit_price' => 'required',
			'item_description' => 'required',
			'paymentmethod' => 'required',
			'paymentdate' => 'required'
		]);
	
		if($request->input("expenses_type")==null){
		    flash(__('Please select one expense item details.'))->error();
		return redirect('admin/expenses/create');
		    
		}
		$categoryID=$request->input("category");
		$expensename=$request->input("expensename");
		$invoicedate=$request->input("invoicedate");
		$invoiceno=$request->input("invoiceno");
		$igst=$request->input("igst");
		$cgst=$request->input("cgst");
		$sgst=$request->input("sgst");
		$invoiceamount=$request->input("invoiceamount");
		$expenses_types=$request->input("expenses_type",array());
		$category_id=$request->input("category_id");
		$item_description=$request->input("item_description");
		$unit_price=$request->input("unit_price");
		$quantity=$request->input("quantity");
		$subtotal=$request->input("subtotal");
		$invoicedate=Carbon::createFromFormat('d/m/Y', $invoicedate)->toDateString();
		
		$paidamount=$request->input("paidamount");
		$outstandingamount = $invoiceamount - $paidamount;
		
		if(empty($paidamount)){$paidamount=0;}
			
		if($outstandingamount==0) $Status="Paid" ; 
		else if($paidamount==0) $Status="Unpaid";
		else $Status="Partially Paid";
		
		$Expenses = new Expenses;
		$Expenses->ExpenseName = $expensename;
		$Expenses->ECategoryID = $categoryID;
		$Expenses->ExpenseInvoiceNo = $invoiceno;
		$Expenses->ExpenseInvoiceDate = $invoicedate;
		$Expenses->igst = $igst;
		$Expenses->cgst = $cgst;
		$Expenses->sgst = $sgst;
		$Expenses->ExpensesInvoiceAmount = $invoiceamount;
		$Expenses->PaidAmount = $paidamount;
		$Expenses->OutstandingAmount = $outstandingamount;
		$Expenses->PaymentStatus = $Status;
		$Expenses->save();
        
        foreach($expenses_types as $key => $expenses_type) {
            $ExpensesDetails = new ExpensesDetails;
            $ExpensesDetails->expenses_id = $Expenses->ExpenseID;
            $ExpensesDetails->expenses_type = $expenses_type;
            if($expenses_type == "Product Expenses") {
                $Product = \App\Product::where('id',$item_description[$key])->select('id', 'name', 'current_stock')->first();
                $Product->current_stock = $Product->current_stock + $quantity[$key];
                $Product->save();
                $ExpensesDetails->category_id = $category_id[$key];
                $ExpensesDetails->product_id = $item_description[$key];
                $ExpensesDetails->item_description = $Product->name;
            } else {
                $ExpensesDetails->category_id = 0;
                $ExpensesDetails->product_id = 0;
                $ExpensesDetails->item_description = $item_description[$key];
            }
            $ExpensesDetails->unit_price = $unit_price[$key];
            $ExpensesDetails->quantity = $quantity[$key];
            $ExpensesDetails->sub_total = $subtotal[$key];
            $ExpensesDetails->save();
        }

		$paymentdate=$request->input("paymentdate");
		$paymentmethod=$request->input("paymentmethod");
		$refID=$request->input("refID");
		if($paidamount>0){
			if($paymentdate == ""){ $paymentdate = $invoicedate; }
			else{ $paymentdate=Carbon::createFromFormat('d/m/Y', $paymentdate)->toDateString(); }
			
			$ExpensesPayments = new ExpensesPayments;
			$ExpensesPayments->ExpenseID = $Expenses->ExpenseID;
			$ExpensesPayments->ExpensesPaidAmount = $paidamount;
			$ExpensesPayments->ExpesnesPaidDate = $paymentdate;
			$ExpensesPayments->ExpensesPaymentMethod = $paymentmethod;
			$ExpensesPayments->ExpensesPaymentRefID = $refID;
			$ExpensesPayments->save();
		}
        
		flash(__('Expense added successfully.'))->success();
		return redirect('admin/expenses');
	}
    
	public function ExpensesEditView(Request $request, $id){
		$Expenses=Expenses::find($id);
		$ExpensesCategory=ExpensesCategory::get();
		$ExpensesDetails=ExpensesDetails::where('expenses_id',$id)->get();
		$data = array("categories"=>$ExpensesCategory,"Expenses"=>$Expenses, "ExpensesDetails" => $ExpensesDetails);
		return view('expenses.ExpensesEdit',$data);
	}
    
	public function ExpensesEditPost(Request $request, $id){
	    $validatedData = $request->validate([
			'expensename' => 'required|min:3|max:30',
			'invoicedate' => 'required',
			'expenses_type' => 'required',
			'invoiceno' => 'required',
			'unit_price' => 'required',
			'item_description' => 'required',
			
		]);
	
		$Expenses=Expenses::find($id);
		$categoryID=$request->input("category");
		$expensename=$request->input("expensename");
		$invoicedate=$request->input("invoicedate");
		$invoiceno=$request->input("invoiceno");
		$igst=$request->input("igst");
		$cgst=$request->input("cgst");
		$sgst=$request->input("sgst");
		$invoiceamount=$request->input("invoiceamount");
        
		$deleted_expenses_details_ids=$request->input("deleted_expenses_details_ids",array());
		$expenses_details_id=$request->input("expenses_details_id",array());
		$expenses_types=$request->input("expenses_type");
		$category_id=$request->input("category_id");
		$item_description=$request->input("item_description");
		$unit_price=$request->input("unit_price");
		$current_stock=$request->input("current_stock");
		$quantity=$request->input("quantity");
		$subtotal=$request->input("subtotal");
        
		$invoicedate=Carbon::createFromFormat('d/m/Y', $invoicedate)->toDateString();
		
		$TotalAmount_old=$Expenses->ExpensesInvoiceAmount;
		$PaidAmount_old=$Expenses->PaidAmount;
		$OutstandingAmount_old=$Expenses->OutstandingAmount;
		$PaymentStatus=$Expenses->PaymentStatus;
		
		if($invoiceamount<$PaidAmount_old){
            flash(__('Invoice Amount can not be less than paid amount or either remove the payment details.'))->error();
			return back();
		}
		
		$Expenses->ExpenseName = $expensename;
		$Expenses->ECategoryID = $categoryID;
		$Expenses->ExpenseInvoiceNo = $invoiceno;
		$Expenses->ExpenseInvoiceDate = $invoicedate;
		$Expenses->igst = $igst;
		$Expenses->cgst = $cgst;
		$Expenses->sgst = $sgst;
		$Expenses->ExpensesInvoiceAmount = $invoiceamount;
		$Expenses->save();
		
		$array = $this->Recaluclate($id);
			
		$PaymentStatus=$array['PaymentStatus'];
		$OutstandingAmount=$array['OutstandingAmount'];
		$PaidAmount=$array['PaidAmount'];
		
		$Expenses->PaidAmount =$PaidAmount;
		$Expenses->OutstandingAmount =$OutstandingAmount;
		$Expenses->PaymentStatus =$PaymentStatus;
		$Expenses->save();
        foreach($expenses_details_id as $key => $ed_id) {
            if($ed_id == 0) {
                $ExpensesDetails = new ExpensesDetails;
            } else {
                $ExpensesDetails = ExpensesDetails::find($ed_id);
            }
            $ExpensesDetails->expenses_id = $Expenses->ExpenseID;
            $ExpensesDetails->expenses_type = $expenses_types[$key];
            if($expenses_types[$key] == "Product Expenses") {
                $Product = \App\Product::where('id',$item_description[$key])->select('id', 'name','current_stock')->first();
                $Product->current_stock = $Product->current_stock - $current_stock[$key];
                $Product->current_stock = $Product->current_stock + $quantity[$key];
                $Product->save();
                $ExpensesDetails->category_id = $category_id[$key];
                $ExpensesDetails->product_id = $item_description[$key];
                $ExpensesDetails->item_description = $Product->name;
            } else {
                $ExpensesDetails->category_id = 0;
                $ExpensesDetails->product_id = 0;
                $ExpensesDetails->item_description = $item_description[$key];
            }
            $ExpensesDetails->unit_price = $unit_price[$key];
            $ExpensesDetails->quantity = $quantity[$key];
            $ExpensesDetails->sub_total = $subtotal[$key];
            $ExpensesDetails->save();
        }
        foreach($deleted_expenses_details_ids as $d) {
            $ExpensesDetails = ExpensesDetails::find($d)->delete();
        }
		flash(__('Expense updated successfully.'))->success();
		return back();
	}
    
	public function ExpensesView(Request $request, $id){
		$Expenses=Expenses::find($id);
		$ExpensesCategory=ExpensesCategory::get();
		$ExpensesPayments=ExpensesPayments::where('ExpenseID',$id)->orderBy('ExpesnesPaidDate')->get();
		$data = array("categories"=>$ExpensesCategory,"Expenses"=>$Expenses,"ExpensesPayments"=>$ExpensesPayments);
		return view('expenses.ExpenseView',$data);
		
	}
    
	public function ExpenseDelete(Request $request){
		$id = $request->input('id');
		$Expenses = Expenses::find($id);
		$Expenses->delete();
		ExpensesPayments::where('ExpenseID',$id)->delete();
		ExpensesDetails::where('expenses_id',$id)->delete();
		flash(__('Expense has been deleted successfully.'))->success();
		return back(); 
	}
    
	public function ExpensePaymentAdd(){
		$Expenses = Expenses::where('OutstandingAmount','!=',0)->orderBy('ExpenseID')->get();
		$SelectedExpenseID=null;
		$data = array("Expenses" => $Expenses,"SelectedExpense"=>$SelectedExpenseID);
		return view('expenses.ExpensePaymentAdd', $data);
	}
    
	public function ExpensePaymentAddWithID(Request $request, $id){
		$Expenses = Expenses::where('OutstandingAmount','!=',0)->orderBy('ExpenseID')->get();
		$SelectedExpenseID=Expenses::find($id);
		$data = array("Expenses" => $Expenses,"SelectedExpense"=>$SelectedExpenseID);
		return view('expenses.ExpensePaymentAdd', $data);
	}
    
	public function ExpensePaymentSave(Request $request){
	   
	    
		$expenseID=$request->input("ExpenseID");
		$outstandingamount=$request->input("outstandingamount");
		$payingamount=$request->input("payingamount");
		$paymentdate=$request->input("paymentdate");
		$paymentmethod=$request->input("paymentmethod");
		$refID=$request->input("refID");
		$paymentdate=Carbon::createFromFormat('d/m/Y', $paymentdate)->toDateString();
		
		$ExpensesPayments = new ExpensesPayments;
		$ExpensesPayments->ExpenseID=$expenseID;
		$ExpensesPayments->ExpensesPaidAmount=$payingamount;
		$ExpensesPayments->ExpesnesPaidDate=$paymentdate;
		$ExpensesPayments->ExpensesPaymentMethod=$paymentmethod;
		$ExpensesPayments->ExpensesPaymentRefID=$refID;
		$ExpensesPayments->save();
		
		//get PaidAmount and OutstandingAmount From Expense Table
		$Expenses = Expenses::find($expenseID);
		$Paid_amount_old=$Expenses->PaidAmount;
		$balance_amount_old=$Expenses->OutstandingAmount;
		
		$newPaid=$Paid_amount_old+$payingamount;
		$newBalance=$balance_amount_old-$payingamount;
		
		if($newBalance==0) $Status="Paid" ; 
		else if($newPaid==0) $Status="Unpaid";
		else $Status="Partially Paid";
	
		$Expenses->PaidAmount=$newPaid;
		$Expenses->OutstandingAmount=$newBalance;
		$Expenses->PaymentStatus=$Status;
		$Expenses->save();
        if($Status == "Paid") {
            $vendor_id = $Expenses->vendor_id;
            $OrderDetail = \App\OrderDetail::where('seller_id',$vendor_id)->where('vendor_expenses_id', $Expenses->ExpenseID)->get();
  
            if(count($OrderDetail) > 0) {
                foreach($OrderDetail as $OD) {
                    $OD->vendor_status = "Invoice Paid";
                    $OD->save();
                }
                $array['view'] = 'emails.alert_vendor';
                $array['subject'] = 'Invoice Paid.';
                $array['from'] = env('MAIL_FROM_ADDRESS');
                $array['content'] = 'Hi. Your invoice has been paid. Kindly proccess the order for shipping.';
                Mail::to(\App\User::find($vendor_id)->email)->queue(new AlertEmailManager($array));
                $smsMsg = "Your invoice has been paid. Kindly proccess the order for shipping.";
                $smsMsg = rawurlencode($smsMsg);
                $to = "91".\App\User::find($vendor_id)->phone;
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        file_get_contents($SMS_URL);
                    }
                }
            }
        }
        flash(__('Expense Payment added successfully.'))->success();
		return redirect()->route('ExpensesView',['id'=>$expenseID]);
	}
    
	public function ExpensePaymentDelete(Request $request) {
		$id = $request->input('id'); 
		$ExpensesPayments = ExpensesPayments::find($id);
		$ExpenseID=$ExpensesPayments->ExpenseID;
		$ExpensesPayments->delete();
		
		$Expenses = Expenses::find($ExpenseID);
		$PaymentCaluclate = $this->Recaluclate($ExpenseID);
		
		$payment_Status=$PaymentCaluclate['PaymentStatus'];
		$OutstandingAmount=$PaymentCaluclate['OutstandingAmount'];
		
		$PaidAmount=$PaymentCaluclate['PaidAmount'];
		$Expenses->PaidAmount =$PaidAmount;
		$Expenses->OutstandingAmount =$OutstandingAmount;
		$Expenses->PaymentStatus =$payment_Status;
		$Expenses->save();
        flash(__('Payment has been deleted successfully.'))->success();
	    return back(); 
	}
    
	public function Category(){
		$ExpensesCategory=ExpensesCategory::paginate( env('RECORDPERPAGE', '10'));
		$data = array("categories"=>$ExpensesCategory);
		return view('expenses.ExpenseCategoryList',$data);
	}
    
	public function CreateCategory(){
		$data = array();
		return view('expenses.ExpenseCategoryAdd',$data);
	}
    
	public function SaveCategory(Request $request){
	    $validatedData = $request->validate([
			'name' => 'required|min:3|max:255',
		]);
		$name=$request->input("name");
		$ExpensesCategory = new ExpensesCategory;
		$ExpensesCategory->ECategoryName = $name;
		$ExpensesCategory->save();
        flash(__('Category added successfully.'))->success();
		return back();
	}
    
	public function CategoryEditView(Request $request, $id){
		$ExpensesCategory=ExpensesCategory::find($id);
		$data = array("category"=>$ExpensesCategory);
		return view('expenses.ExpenseCategoryEdit',$data);
	}
    
	public function CategoryEditPost(Request $request, $id){
	     $validatedData = $request->validate([
			'name' => 'required|min:3|max:255',
		]);
		$ExpensesCategory=ExpensesCategory::find($id);
		$name=$request->input("name");
		$ExpensesCategory->ECategoryName = $name;
		$ExpensesCategory->save();
        flash(__('Category Updated successfully.'))->success();
		return redirect('admin/expenses/category');
	}
    
	public function CategoryDelete(Request $request){
		$id = $request->input('id');
		$ExpensesCategory = ExpensesCategory::find($id);
		$ExpensesCategory->delete();
        flash(__('Category has been deleted successfully.'))->success();
		return redirect()->back(); 
	}
}
