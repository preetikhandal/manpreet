<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use Hash;
use App\User;
use App\Customer;
use App\Http\Controllers\SearchController;
use Cookie;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;
use Mail;
use App\ProductRequest;

class PasswordResetController extends Controller
{
	public function reset_mobile_password(Request $request){
			$validatedData = $request->validate([
			'password' => 'required|string|min:8|max:200|same:password_confirmation',
			'password_confirmation' => 'required|min:8',
		]);
			$currentTime = time();
            if($currentTime <= session('login_otp_expire_time')) {
                    if(session('login_otp') == $request->get('code')) {
						$user=User::where('phone',session('login_phone'))->first();
						if($user==null){
							flash(__('Wrong Deatil entered. Please do your Process Again.'))->error();
							return redirect()->route('password.request');
						}
                    $request->session()->forget('login_otp_expire_time');
                    $request->session()->forget('login_otp');
                    $request->session()->forget('login_phone');
					
                    $user->password=Hash::make($request->password);
					$user->save();
					flash(__('Your Password has been updated successfully!'))->success();
					//return redirect()->route('password.request');
					$redirect = route('user.login');
					return response()->json(array('result' => true, 'redirectTo' => $redirect, 'name' => $user->name));
                } else {
                    return response()->json(array('result' => false, 'message' => "Wrong OTP entered."));
                    //flash(__('Wrong otp enterd! Please do your Process Again.'))->error();
					//return redirect()->route('password.request');
                }
            } else {
                    return response()->json(array('result' => false, 'message' => "OTP has been expired."));
                   // flash(__('OTP has been expired.Please do your Process Again'))->error();
					//return redirect()->route('password.request');
            }
	}
  
 }
