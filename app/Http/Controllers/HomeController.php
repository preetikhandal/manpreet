<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use DB;
use Hash;
use App\Blog;
use App\Category;
use App\FlashDeal;
use App\Brand;
use App\SubCategory;
use App\SubSubCategory;
use App\Product;
use App\PickupPoint;
use App\CustomerPackage;
use App\CustomerProduct;
use App\User;
use App\UserCartSession;
use App\Customer;
use App\Seller;
use App\Shop;
use App\Color;
use App\Order;
use App\BusinessSetting;
use App\popup_offer;
use App\Review;
use App\Http\Controllers\SearchController;
use ImageOptimizer;
use Cookie;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;
use Mail;
use App\ProductRequest;
use Image;
use File;
use Storage;
use App\SellerCommision;
use App\HomeCategory;




class HomeController extends Controller
{
    public function imgResize(Request $request) {
        $from = $request->from;
        $to = $request->to;
$n = 0;
        $Products = Product::where('id','>=', $from)->where('id','<=', $to)->get();
        foreach($Products as $Product) {
            if($Product->thumbnail_img != null) {
                $extension = pathinfo($Product->thumbnail_img, PATHINFO_EXTENSION);
                $rand = rand(1000,9999);
                $thumbnail = Str::slug($Product->name,"-")."-thumb-".time().$rand.".".$extension;
                $img = Image::make(asset($Product->thumbnail_img));
                if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$Product->thumbnail_img))
                    unlink(base_path('public/').$Product->thumbnail_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($Product->thumbnail_img);
                }
                $width = $img->width();
    			$height = $img->height();
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $i = $img->save(base_path('public/')."temp/".$thumbnail);
                ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);
                if(env('FILESYSTEM_DRIVER') == "local") {
                    copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/products/thumbnail/'.$thumbnail);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/thumbnail', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
                }
                $Product->thumbnail_img = "uploads/products/thumbnail/".$thumbnail;
                unlink(base_path('public/').'temp/'.$thumbnail);
            }
            
            
            if($Product->featured_img != null) {
                $extension = pathinfo($Product->featured_img, PATHINFO_EXTENSION);
                $rand = rand(1000,9999);
                $featured = Str::slug($Product->name,"-")."-featured-".time().$rand.".".$extension;
                $img = Image::make(asset($Product->featured_img));
                if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$Product->featured_img))
                    unlink(base_path('public/').$Product->featured_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($Product->featured_img);
                }
                $width = $img->width();
    			$height = $img->height();
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$featured);
                ImageOptimizer::optimize(base_path('public/')."temp/".$featured);
                if(env('FILESYSTEM_DRIVER') == "local") {
                    copy(base_path('public/').'temp/'.$featured, base_path('public/').'uploads/products/featured/'.$featured);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/featured', new \Illuminate\Http\File(base_path('public/').'temp/'.$featured), $featured);
                }
                $Product->featured_img = "uploads/products/featured/".$featured;
                unlink(base_path('public/').'temp/'.$featured);
            }
            
            
            if($Product->flash_deal_img != null) {
                $extension = pathinfo($Product->flash_deal_img, PATHINFO_EXTENSION);
                $rand = rand(1000,9999);
                $flash_deal = Str::slug($Product->name,"-")."-flash_deal-".time().$rand.".".$extension;
                $img = Image::make(asset($Product->flash_deal_img));
                if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$Product->flash_deal_img))
                    unlink(base_path('public/').$Product->flash_deal_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($Product->flash_deal_img);
                }
                $width = $img->width();
    			$height = $img->height();
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$flash_deal);
                ImageOptimizer::optimize(base_path('public/')."temp/".$flash_deal);
                if(env('FILESYSTEM_DRIVER') == "local") {
                    copy(base_path('public/').'temp/'.$flash_deal, base_path('public/').'uploads/products/flash_deal/'.$flash_deal);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/flash_deal', new \Illuminate\Http\File(base_path('public/').'temp/'.$flash_deal), $flash_deal);
                }
                $Product->flash_deal_img = "uploads/products/flash_deal/".$flash_deal;
                unlink(base_path('public/').'temp/'.$flash_deal);
            }
            
            if($Product->meta_img != null) {
                $extension = pathinfo($Product->meta_img, PATHINFO_EXTENSION);
                $rand = rand(1000,9999);
                $meta_img = Str::slug($Product->name,"-")."-meta-".time().$rand.".".$extension;
                $img = Image::make(asset($Product->meta_img));
                if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$Product->meta_img))
                    unlink(base_path('public/').$Product->meta_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($Product->meta_img);
                }
                $width = $img->width();
    			$height = $img->height();
    			if($width <= 200 && $height <= 200) {
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			} else {
    				 $img->resize(200, 200, function ($constraint) {
    					$constraint->aspectRatio();
    				});
    				$img->resizeCanvas(200, 200, 'center', false, 'fff');
    			}
                $img->save(base_path('public/')."temp/".$meta_img);
                ImageOptimizer::optimize(base_path('public/')."temp/".$meta_img);
                if(env('FILESYSTEM_DRIVER') == "local") {
                    copy(base_path('public/').'temp/'.$meta_img, base_path('public/').'uploads/products/meta/'.$meta_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/meta', new \Illuminate\Http\File(base_path('public/').'temp/'.$meta_img), $meta_img);
                }
                $Product->meta_img = "uploads/products/meta/".$meta_img;
                unlink(base_path('public/').'temp/'.$meta_img);
            }
            $Product->save();
            echo $Product->id."*";
$n++;
if($n == 50) {
echo "<br />";
$n =0;
}
        }
    }
    
    public function rebuildRedis() 
    {
        $prefix = env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_');
        $ProductIDs = Product::select('id')->where('published', 1)->get()->pluck('id')->all();
        $totalRedis = (int)count(Redis::command('keys',['*ProductID:*:ProductName*']));
        $totalDBProduct = (int)count($ProductIDs);
        $diff =  $totalDBProduct - $totalRedis;
        if($diff != 0) {
            $keys = Redis::command('keys',['*ProductID:*']);
            foreach($keys as $key => $value) {
                $keys[$key] = str_replace($prefix, '', $value);
            }
            Redis::del($keys);
            $n = 1;
            foreach($ProductIDs as $P) {
                if(env('CACHE_DRIVER') == 'redis') {
                $product = Cache::tags(['Product'])->rememberForever('Product:'.$P, function () use ($P) {
                    return Product::findOrFail($P);
                });
                } else {
                   $product = Cache::rememberForever('Product:'.$P, function () use ($P) {
                    return Product::findOrFail($P);
                }); 
                }
                Redis::set('ProductID:'.$P.':ProductTag:'.strtolower($product->tags), $P);
                Redis::set('ProductID:'.$P.':ProductName:'.strtolower($product->name), $P);
                Redis::set('ProductID:'.$P.':ProductSalt:'.strtolower($product->salt), $P);
                echo "$n :: $P : $product->name";
                echo "<br />";
                $n++;
            }
        }
        echo "Done";
    }
    
    public function login()
    {
        if(Auth::check()){
            return redirect()->route('home');
        }
        return view('frontend.user_login');
    }

    public function registration(Request $request)
    {
        if(Auth::check()){
            return redirect()->route('home');
        }
        if($request->has('referral_code')){
            Cookie::queue('referral_code', $request->referral_code, 43200);
        }
        return view('frontend.user_registration');
    }

    // public function user_login(Request $request)
    // {
    //     $user = User::whereIn('user_type', ['customer', 'seller'])->where('email', $request->email)->first();
    //     if($user != null){
    //         if(Hash::check($request->password, $user->password)){
    //             if($request->has('remember')){
    //                 auth()->login($user, true);
    //             }
    //             else{
    //                 auth()->login($user, false);
    //             }
    //             return redirect()->route('dashboard');
    //         }
    //     }
    //     return back();
    // }

    public function cart_login(Request $request)
    {
        
       $validatedData = $request->validate([
			'email' => 'required|email:rfc,dns',
			'password' => 'required|string|min:8|max:200'
		
		]);
       
       
        if(filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
            $user = User::whereIn('user_type', ['customer', 'seller'])->where('email', $request->email)->first();
        } else {
            $user = User::whereIn('user_type', ['customer', 'seller'])->where('phone', $request->email)->first();
        }
     //   $user = User::whereIn('user_type', ['customer', 'seller'])->where('email', $request->email)->first();
        if($user != null){
            updateCartSetup();
            if(Hash::check($request->password, $user->password)){
                if($request->has('remember')){
                    auth()->login($user, true);
                }
                else{
                    auth()->login($user, false);
                }
            }
        }
        return back();
    }

    public function user_login(Request $request)
    {
        /*
        $validatedData = $request->validate([
			'email' => 'required|email',
			'password' => 'required|max:200'
		]);
		*/
		$cart_login = $request->input('cart_login', 0);
		if(!isset($request->email) || $request->email==null || $request->email==''){
            return response()->json(array('result' => false, 'message' => "Email id or Phone is required."));
         } elseif (is_numeric($request->email)){
             if(strlen($request->email)!=10){
             return response()->json(array('result' => false, 'message' => "Enter valid Phone number."));
            }
         } elseif (!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
              return response()->json(array('result' => false, 'message' => "Enter a valid Email Id."));
         }
        if(filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $request->email)->first();
        } else {
            $user = User::where('phone', $request->email)->first();
        }
        if($request->password == '' AND $request->otp == ''){
            return response()->json(array('result' => false, 'message' => "OTP or Password is required."));
        }
        if($request->otp == "")
        {
            if(!isset($request->password) || $request->password==null || $request->password==''){
                return response()->json(array('result' => false, 'message' => "Password is required."));
            } 
            if($user != null){
                updateCartSetup();
                $MASTER_OTP = env('MASTER_OTP','88712ws52');
                if(Hash::check($request->password, $user->password) || $MASTER_OTP == $request->password){
                    if($request->has('remember')){
                        auth()->login($user, true);
                    } else {
                        auth()->login($user, false);
                    }
                    if(auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'staff') {
                        $redirect = route('admin.dashboard');
                    } elseif(auth()->user()->user_type == 'vendor' || auth()->user()->user_type == 'seller') {
                        $redirect = route('dashboard');
                    } elseif(session('link') != null){
                        $redirect = session('link');
                    } else {
                        $redirect = route('home');
                    }
                    if($cart_login == 0) {
                        $this->setCart();
                    }
                    return response()->json(array('result' => true, 'redirectTo' => $redirect, 'name' => $user->name));
                } else {
                    return response()->json(array('result' => false, 'message' => "Wrong email & password combination."));
                }
            }
            return response()->json(array('result' => false, 'message' => "Wrong email & password combination."));
        } elseif($user == null) {
            if(session('login_phone') != $request->get('email')) {
                return response()->json(array('result' => false, 'message' => "Somethings goes wrong. Kindly refresh the page."));
            }
            $currentTime = time();
            if($currentTime <= session('login_otp_expire_time')) {
                    $MASTER_OTP = env('MASTER_OTP','88712ws52');
                    if(session('login_otp') == $request->get('otp') || $MASTER_OTP == $request->get('otp') ) {
                    $request->session()->forget('login_otp_expire_time');
                    $request->session()->forget('login_otp');
                    $request->session()->forget('login_phone');
                    $user = User::create([
                    'name' => $request->email,
                    'phone' => $request->email,
                    'password' => Hash::make(time())
                    ]);
                    
                    $user->email_verified_at = date('Y-m-d H:m:s');
                    $user->save();
                    $customer = new Customer;
                    $customer->user_id = $user->id;
                    $customer->save();
                    if(Cookie::has('referral_code')){
                        $referral_code = Cookie::get('referral_code');
                        $referred_by_user = User::where('referral_code', $referral_code)->first();
                        if($referred_by_user != null){
                            $user->referred_by = $referred_by_user->id;
                            $user->save();
                        }
                    }
                    if($request->has('remember')) {
                        auth()->login($user, true);
                    } else {
                        auth()->login($user, false);
                    }
                    $user->login_otp = null;
                    $user->login_otp_expire_time = null;
                    $user->save();
                    if(auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'staff') {
                        $redirect = route('admin.dashboard');
                    } elseif(auth()->user()->user_type == 'vendor' || auth()->user()->user_type == 'seller') {
                        $redirect = route('dashboard');
                    } elseif(session('link') != null){
                        $redirect = session('link');
                    } else {
                        $redirect = route('home');
                    }
                    if($cart_login == 0) {
                        $this->setCart();
                    }
                    return response()->json(array('result' => true, 'redirectTo' => $redirect, 'name' => $user->name));
                } else {
                    return response()->json(array('result' => false, 'message' => "Wrong OTP entered."));
                }
            } else {
                return response()->json(array('result' => false, 'message' => "OTP has been expired."));
            }
        } else {
            $currentTime = time();
            if($currentTime <= $user->login_otp_expire_time) {
                $MASTER_OTP = env('MASTER_OTP','88712ws52');
                if($user->login_otp == $request->get('otp') || $MASTER_OTP == $request->get('otp')) {
                    if($request->has('remember')) {
                        auth()->login($user, true);
                    } else {
                        auth()->login($user, false);
                    }
                    $user->login_otp = null;
                    $user->login_otp_expire_time = null;
                    $user->save();
                    if(auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'staff') {
                        $redirect = route('admin.dashboard');
                    } elseif(auth()->user()->user_type == 'vendor' || auth()->user()->user_type == 'seller') {
                        $redirect = route('dashboard');
                    } elseif(session('link') != null){
                        $redirect = session('link');
                    } else {
                        $redirect = route('home');
                    }
                    if($cart_login == 0) {
                        $this->setCart();
                    }
                    return response()->json(array('result' => true, 'redirectTo' => $redirect, 'name' => $user->name));
                } else {
                    return response()->json(array('result' => false, 'message' => "Wrong OTP entered."));
                }
            } else {
                return response()->json(array('result' => false, 'message' => "OTP has been expired."));
            }
        }
    }
    
    private function setCart() {
        $user_id = Auth()->User()->id;
        $UserCartSession = UserCartSession::where('user_id', $user_id)->first();
        if($UserCartSession != null) {
            $cart = json_decode($UserCartSession->cart, true);
            if(session('cart') != null) {
                $sessionCart = session('cart');
                foreach($cart as $c) {
                    foreach($sessionCart  as $key=>$sc) {
                        if($c['id'] == $sc['id']) {
                            $cart[$key]['quantity'] = $c['quantity'] + $sc['quantity'];
                            unset($sessionCart[$key]);
                        }
                    }
                    if(!isset($cart[$key]['coupon_category_discount'])){
                        $cart[$key]['coupon_category_discount'] = 0; 
                    }
                }
                foreach($sessionCart as $sc) {
                    array_push($cart, $sc);
                }
            } else {
                $cart = json_decode($UserCartSession->cart, true);
                foreach($cart as $key=>$c) {
                    if(!isset($cart[$key]['coupon_category_discount'])){
                        $cart[$key]['coupon_category_discount'] = 0; 
                    }
                }
            }
            
            $cart = collect($cart);
            session(['cart' => $cart ]);
        }
    }
    
    public function create_otp_generate(Request $request)
    {
            $login_otp = random_int(100000, 999999);
            $login_otp_expire_time = time() + 310;
            session(['login_otp' => $login_otp]);
            session(['login_otp_expire_time' => $login_otp_expire_time]);
            session(['login_phone' => $request->phone]);
             
            $smsMsg = "ALDE BAZAAR: ".$login_otp." is your One-Time-Password (OTP) to login at your ALDE BAZAAR account. It is valid for 10 minutes.";
            $smsMsg = rawurlencode($smsMsg);
            $to = "91".$request->phone;
            if(strlen($to) == 12) {
             $SMS_URL = env('SMS_URL', null);
                if($SMS_URL != null) {
                    $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                    sendSMS($SMS_URL);
                }
            }
            return response()->json(array('result' => true, 'newuser' => true));
        
    }
    
    public function login_otp_generate(Request $request)
    {
        $user = User::where('phone', $request->phone)->first();
        if(strlen($request->phone)!=10){
             return response()->json(array('result' => false, 'message' => "Enter valid Phone number."));
        }
        if($user != null){
            $login_otp = random_int(100000, 999999);
            $login_otp_expire_time = time() + 310;
            $user->login_otp = $login_otp;
            $user->login_otp_expire_time = $login_otp_expire_time;
            $user->save();
            $smsMsg = "ALDE BAZAAR: ".$login_otp." is your One-Time-Password (OTP) to login at your ALDE BAZAAR account. It is valid for 10 minutes.";
            $smsMsg = rawurlencode($smsMsg);
            $to = "91".$user->phone;
            if(strlen($to) == 12) {
             $SMS_URL = env('SMS_URL', null);
                if($SMS_URL != null) {
                    $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                    sendSMS($SMS_URL);
                }
            }
            return response()->json(array('result' => true));
        } else {
            $login_otp = random_int(100000, 999999);
            $login_otp_expire_time = time() + 310;
            session(['login_otp' => $login_otp]);
            session(['login_otp_expire_time' => $login_otp_expire_time]);
            session(['login_phone' => $request->phone]);
            $smsMsg = "ALDE BAZAAR: ".$login_otp." is your One-Time-Password (OTP) to login at your ALDE BAZAAR account. It is valid for 10 minutes.";
            $smsMsg = rawurlencode($smsMsg);
            $to = "91".$request->phone;
            if(strlen($to) == 12) {
             $SMS_URL = env('SMS_URL', null);
                if($SMS_URL != null) {
                    $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                    sendSMS($SMS_URL);
                }
            }
            return response()->json(array('result' => true, 'newuser' => true));
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_dashboard()
    {
        return view('dashboard');
    }

    /**
     * Show the customer/seller dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {   
        if(Auth::user()->user_type == 'seller'){
            return view('frontend.seller.dashboard');
        }
        elseif(Auth::user()->user_type == 'customer'){
            
            return view('frontend.customer.dashboard');
        }
        elseif(Auth::user()->user_type == 'vendor'){
            return view('frontend.vendor.dashboard');
        }
        else {
            abort(404);
        }
    }

    public function profile(Request $request)
    {
        if(Auth::user()->user_type == 'customer'){
            return view('frontend.customer.profile');
        }
        elseif(Auth::user()->user_type == 'seller'){
            return view('frontend.seller.profile');
        }
    }

    public function customer_update_profile(Request $request)
    {
        $user = Auth::user();
         $validatedData = $request->validate([
			'name' => 'required|min:3|max:190',
	        'address' => 'max:300',
			'country' => 'max:30',
			'city' => 'max:30'
		]);
		if($request->phone!=null || $request->phone!=''){
    		$validatedData = $request->validate([
    			'phone' => 'min:10|max:10'
    		]);
        }
        if($request->new_password!=null){
    		$validatedData = $request->validate([
    			'new_password' => 'min:8|max:200|same:confirm_password',
			    'confirm_password' => 'min:8'
    		]);
        }
		if($request->postal_code!=null){
    		$validatedData = $request->validate([
    			'postal_code' => 'min:6|max:6',
    		]);
        }
        $user->name = $request->name;
        $user->address = $request->address;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->postal_code = $request->postal_code;
        $user->phone = $request->phone;

        if($request->new_password != null && ($request->new_password == $request->confirm_password)){
            $user->password = Hash::make($request->new_password);
        }

        if($request->hasFile('photo')){
            $user->avatar_original = $request->photo->store('uploads/users');
        }

        if($user->save()){
            flash(__('Your Profile has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }


    public function seller_update_profile(Request $request)
    {
        
        $user = Auth::user();
         $validatedData = $request->validate([
			'name' => 'required|min:2|max:190',
			'phone' => 'min:10|max:10',
			'postal_code' => 'min:6|max:6',
			'address' => 'max:300',
			'country' => 'max:30',
			'city' => 'max:30'
		]);
		if($request->new_password){
		    $validatedData = $request->validate([
		     'new_password' => 'string|min:8|max:200|same:confirm_password',
		    	'confirm_password' => 'min:8',
		    ]);
		}
		if($request->get('bank_payment_status',0)==1){
		    $validatedData = $request->validate([
			'bank_name' => 'required|max:255',
			'bank_acc_name' => 'required|max:200',
			'bank_acc_no' => 'required|max:50',
			'bank_routing_no' => 'max:50',
		]);
		}
        $user->name = $request->name;
        $user->address = $request->address;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->postal_code = $request->postal_code;
        $user->phone = $request->phone;

        if($request->new_password != null && ($request->new_password == $request->confirm_password)){
            $user->password = Hash::make($request->new_password);
        }

        if($request->hasFile('photo')){
            $user->avatar_original = $request->photo->store('uploads');
        }

        $seller = $user->seller;
        $seller->cash_on_delivery_status = $request->input('cash_on_delivery_status', 0);
        $seller->bank_payment_status = $request->input('bank_payment_status', 0);
        $seller->bank_name = $request->bank_name;
        $seller->bank_acc_name = $request->bank_acc_name;
        $seller->bank_acc_no = $request->bank_acc_no;
        $seller->bank_routing_no = $request->bank_routing_no;
       

        if($user->save() && $seller->save()){
            flash(__('Your Profile has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Show the application frontend home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(env('CACHE_DRIVER') == 'redis') {
		$MobileCategories = Cache::tags(['Category'])->rememberForever('CategoryByPosition', function () {
                return  Category::orderBy('position')->get();
        });
        } else {
           $MobileCategories = Cache::rememberForever('CategoryByPosition', function () {
                return  Category::orderBy('position')->get();
        }); 
        }
		return view('frontend.index', compact('MobileCategories'));
    }

    public function flash_deal_details($slug)
    {  
        if (Cache::has('flash_deal_details')){
          
        	   $flash_deal =  Cache::get('flash_deal_details');
    	} else {
    	   
    		$flash_deal = FlashDeal::where('slug', $slug)->first();
    		Cache::forever('flash_deal_details', $flash_deal);
    	}
        if($flash_deal != null)
            return view('frontend.flash_deal_details', compact('flash_deal'));
        else {
            abort(404);
        }
    }
    
    public function featureproductList()
    {   	if (Cache::has('featured_product')){
        	   $featured_product =  Cache::get('featured_product');
        	} else {
        	   $featured_product = filter_products(\App\Product::where('published', 1)->where('featured', '1'))->get();
        	   Cache::forever('featured_product', $featured_product);
        	}
      
        if($featured_product != null)
            return view('frontend.feature_product_list', compact('featured_product'));
        else {
            abort(404);
        }
    }
    
    
    public function bestSelling(){
        return view('frontend.best_selling_product_list');
       
    }

    public function load_featured_section(){
        return view('frontend.partials.featured_products_section');
    }

    public function load_best_selling_section(){
        return view('frontend.partials.best_selling_section');
    }
    
	public function load_brand_banner_section(){
        return view('frontend.partials.brand_banner_section');
    }

    public function load_home_categories_section(){
        
        return view('frontend.partials.home_categories_section');
    }
	public function load_home_categories_section2(){
        return view('frontend.partials.home_categories_section2');
    }
	public function load_home_categories_section3(){
        return view('frontend.partials.home_categories_section3');
    }

    public function load_best_sellers_section(){
        return view('frontend.partials.best_sellers_section');
    }

    public function trackOrder(Request $request)
    {
        if($request->has('order_code')){
            $order = Order::where('code', $request->order_code)->first();
            if($order != null) {
                return view('frontend.track_order', compact('order'));
            }
        }
        return view('frontend.track_order');
    }

    public function product(Request $request, $slug)
    {  
        $detailedProduct  = Product::where('slug', $slug)->first();
	//	dd($detailedProduct->category_id);
    	if($detailedProduct == null) {
    	    abort(404);
    	}
		$CategoryID= $detailedProduct->category_id;
		$SubCategoryID= $detailedProduct->subcategory_id;
        $SubSubCategory= $detailedProduct->subsubcategory_id;
        $productReview = Review::where('product_id',$detailedProduct->id)->orderBy('id','desc')->limit(3)->get();
        
        if($detailedProduct!=null && $detailedProduct->published) {
            updateCartSetup();
            if($request->has('product_referral_code')) {
                Cookie::queue('product_referral_code', $request->product_referral_code, 43200);
                Cookie::queue('referred_product_id', $detailedProduct->id, 43200);
            }
            if ($detailedProduct->digital == 1) {
                return view('frontend.digital_product_details', compact('detailedProduct,CategoryID,SubCategoryID,SubSubCategory'));
            } else {
                return view('frontend.product_details', compact('detailedProduct','productReview'));
            }
            // return view('frontend.product_details', compact('detailedProduct'));
        }
        abort(404);
    }

    public function shop($slug)
    {
        $shop  = Shop::where('slug', $slug)->first();
        if($shop!=null){
            $seller = Seller::where('user_id', $shop->user_id)->first();
            if ($seller->verification_status != 0){
                return view('frontend.seller_shop', compact('shop'));
            }
            else{
                return view('frontend.seller_shop_without_verification', compact('shop', 'seller'));
            }
        }
        abort(404);
    }

    public function filter_shop($slug, $type)
    {
        $shop  = Shop::where('slug', $slug)->first();
        if($shop!=null && $type != null){
            return view('frontend.seller_shop', compact('shop', 'type'));
        }
        abort(404);
    }

    public function listing(Request $request)
    {
        // $products = filter_products(Product::orderBy('created_at', 'desc'))->paginate(12);
        // return view('frontend.product_listing', compact('products'));
        return $this->search($request);
    }
     public function all_categoriesgrid(Request $request)
    {
        if(env('CACHE_DRIVER') == 'redis') {
    		$categories = Cache::tags(['Category'])->rememberForever('CategoryByPosition', function () {
                    return  Category::orderBy('position')->get();
            });
        } else {
           $categories = Cache::rememberForever('CategoryByPosition', function () {
                return  Category::orderBy('position')->get();
            }); 
        }
    	return view('frontend.all_categorygrid', compact('categories'));
    }
    
    public function all_categories(Request $request)
    {
    	if(env('CACHE_DRIVER') == 'redis') {
    		$categories = Cache::tags(['Category'])->rememberForever('CategoryByPosition', function () {
                    return  Category::orderBy('position')->get();
            });
        } else {
           $categories = Cache::rememberForever('CategoryByPosition', function () {
                return  Category::orderBy('position')->get();
            }); 
        }
        return view('frontend.all_category', compact('categories'));
    }
    
    public function all_subcategories(Request $request)
    {
        $category_slug = $request->category_slug;
        if(env('CACHE_DRIVER') == 'redis') {
            $Category = Cache::tags(['Category'])->rememberForever('CategorySlug:'.$category_slug, function () use ($category_slug) {
                    return  \App\Category::where('slug',$category_slug)->first();
            });
            $catId = $Category->id;
            $subcategories = Cache::tags(['subCategories'])->rememberForever('subCategoriesByCategoryID:'.$catId, function () use($catId) {
                    return  SubCategory::where('category_id', $catId)->get();
            });
        } else {
            $Category = Cache::rememberForever('CategorySlug:'.$category_slug, function () use ($category_slug) {
                    return  \App\Category::where('slug',$category_slug)->first();
            });
            $catId = $Category->id;
            $subcategories = Cache::rememberForever('subCategoriesByCategoryID:'.$catId, function () use($catId) {
                    return  SubCategory::where('category_id', $catId)->get();
            });  
        }
        return view('frontend.all_subcategory', compact('subcategories'));
    }
    
    public function all_brands(Request $request)
    {
        if(env('CACHE_DRIVER') == 'redis') {
    		$categories = Cache::tags(['Category'])->rememberForever('CategoryByPosition', function () {
                    return  Category::orderBy('position')->get();
            });
        } else {
           $categories = Cache::rememberForever('CategoryByPosition', function () {
                return  Category::orderBy('position')->get();
            }); 
        }
        return view('frontend.all_brand', compact('categories'));
    }

    public function show_product_upload_form(Request $request)
    {
    //     if(env('CACHE_DRIVER') == 'redis') {
    // 		$categories = Cache::tags(['Category'])->rememberForever('CategoryByPosition', function () {
    //                 return  Category::orderBy('position')->get();
    //         });
    //     } else {
    //       $categories = Cache::rememberForever('CategoryByPosition', function () {
    //             return  Category::orderBy('position')->get();
    //         }); 
    //     }
        if(Auth::user()->id){
             $sellerId = Auth::user()->id;      
        }else{
            
          return redirect()->route('sellers.index');
        }
        $SellerCommision = SellerCommision::where('user_id',$sellerId)->get();
        $sellercatId = [];
        foreach($SellerCommision as $catId){
            array_push($sellercatId,$catId->category_id);
        }
        //print_r($sellercatId);die;
        $categories = Category::whereIn('id',$sellercatId)->get();
       
        return view('frontend.seller.product_upload', compact('categories'));
    }

    public function show_product_edit_form(Request $request, $id)
    {
        if(env('CACHE_DRIVER') == 'redis') {
    		$categories = Cache::tags(['Category'])->rememberForever('CategoryByPosition', function () {
                    return  Category::orderBy('position')->get();
            });
        } else {
           $categories = Cache::rememberForever('CategoryByPosition', function () {
                return  Category::orderBy('position')->get();
            }); 
        }
        $product = Product::find(decrypt($id));
        return view('frontend.seller.product_edit', compact('categories', 'product'));
    }

    public function seller_product_list(Request $request)
    {
        $products = Product::where(['user_id'=> Auth::user()->id])->orderBy('created_at', 'desc')->paginate(10);
        
        return view('frontend.seller.products', compact('products'));
    }
    
    public function seller_trased_product_list(Request $request)
    {
        $products = Product::onlyTrashed()->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(10);
        return view('frontend.seller.trashed', compact('products'));
    }

    public function ajax_search(Request $request)
    {
        $redis = Redis::connection();
        
       // dd($values = Redis::sscan('name','match', '*am*'));
        
        $start_time = microtime(true); 
        
        
        /*$ProductIDs = Cache::rememberForever('ProductIDs', function () {
                return Product::select('id')->where('published', 1)->get()->pluck('id')->all();
        });
        foreach($ProductIDs as $P) {
            $product = Cache::tags(['Product'])->rememberForever('Product:'.$P, function () use ($P) {
                return Product::findOrFail($P);
            });
            Redis::set('ProductID:'.$P.':ProductTag:'.strtolower($product->tags), $P);
            Redis::set('ProductID:'.$P.':ProductName:'.strtolower($product->name), $P);
            Redis::set('ProductID:'.$P.':ProductSalt:'.strtolower($product->salt), $P);
        }*/
		$k = explode(" ",$request->search);
        $prefix = env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_');
        $search = str_ireplace(" ", "*", strtolower($request->search));
        $ProductTag = Redis::keys("*ProductTag:*".$search."*");
        
        if(count($k) > 1) {
            $ProductName = Redis::keys("*ProductName:*".$search."*");
        } else {
            $ProductName = Redis::keys("*ProductName:*".$search."*");
        }
        if(count($ProductName) == 0) {
            $ProductName = Redis::keys("*ProductName:*".$search." *");
        }

        $ProductSalt = Redis::keys("*ProductSalt:*".$search."*");
        $ProductTag = array_slice($ProductTag, 0, 30);
        $ProductName = array_slice($ProductName, 0, 30);
        $ProductSalt = array_slice($ProductSalt, 0, 30);
        $titleids = array();
        foreach($ProductName as $i) {
            $i = str_replace($prefix, '', $i);
            $i = $redis->get($i);
            if(isset($titleids[$i])) {
                $titleids[$i] = $titleids[$i] + 3;
            } else {
                $titleids[$i] = 3;
            }
        }
        foreach($ProductTag as $i) {
            $i = str_replace($prefix, '', $i);
            $i = $redis->get($i);
            if(isset($titleids[$i])) {
                $titleids[$i] = $titleids[$i] + 1;
            } else {
                $titleids[$i] = 1;
            }
        }
        foreach($ProductSalt as $i) {
            $i = str_replace($prefix, '', $i);
            $i = $redis->get($i);
            if(isset($titleids[$i])) {
                $titleids[$i] = $titleids[$i] + 2;
            } else {
                $titleids[$i] = 2;
            }
        }
        arsort($titleids);
        $end_time = microtime(true);
        $execution_time = ($end_time - $start_time); 
        //echo $execution_time;
        //dd($titleids);
        $keywords = array();
        $q = $request->q;
		//$ProductIDs = Product::where('published', 1)->select('id')->where('tags', 'like', '%'.$request->search.'%')->get()->pluck('id')->all();
		//dd($ProductIDs);
		
        foreach ($ProductTag as $key => $product) {
            $product = str_replace($prefix, '', $product);
            $product = Redis::get($product);
            if(env('CACHE_DRIVER') == 'redis') {
            $product = Cache::tags(['Product'])->rememberForever('Product:'.$product, function () use ($product) {
                    return Product::findOrFail($product);
            });
            } else {
                $product = Cache::rememberForever('Product:'.$product, function () use ($product) {
                    return Product::findOrFail($product);
            });
            }
            foreach (explode(',',$product->tags) as $key => $tag) {
                if(stripos($tag, $request->search) !== false){
                    if(sizeof($keywords) > 5){
                        break;
                    }
                    else{
                        if(!in_array(strtolower($tag), $keywords)){
                            array_push($keywords, strtolower($tag));
                        }
                    }
                }
            }
        }
        
        /*
        $keywords = array();
        $q = $request->q;
		$k = explode(" ",$request->search);
        $products = Product::where('published', 1)->where('tags', 'like', '%'.$request->search.'%')->get();
        foreach ($products as $key => $product) {
            foreach (explode(',',$product->tags) as $key => $tag) {
                if(stripos($tag, $request->search) !== false){
                    if(sizeof($keywords) > 5){
                        break;
                    }
                    else{
                        if(!in_array(strtolower($tag), $keywords)){
                            array_push($keywords, strtolower($tag));
                        }
                    }
                }
            }
        }*/
        
		//$product = Product::select('id')->where('published', 1)->where('name', 'like', '%'.$k[0].'%');
		/*$start_time = microtime(true); 
		$product = Product::select('id','name','slug','salt','variations','thumbnail_img')->where('published', 1);
        foreach($k as $key => $value) {
            $product = $product->where('name', 'like', '%'.$value.'%');
        }
        
        foreach($k as $key => $value) {
            $product = $product->orWhere('salt', 'like', '%'.$value.'%');
        }
        
        $product = $product->orWhere('tags', 'like', '%'.$request->search.'%');
		$productss = filter_products($product)->get()->take(20);
	//	$ProductArray = array();
		$products = array();
		foreach($productss as $P) {
		    $match = similar_text($request->search, $P->name, $percent);
            $percent = round($percent, 0);
          //  $ProductArray[$P->id] = $percent;
            $products[$percent] = $P;
		}
	//	rsort($ProductArray);
		krsort($products);*/
		$products = array();
		$n = 0;
		foreach($titleids as $key => $value) {
		    if(env('CACHE_DRIVER') == 'redis') {
                    $products[] = Cache::tags(['Product'])->rememberForever('Product:'.$key, function () use ($key) {
                        return Product::findOrFail($key);
                });
                } else {
                    $products[] = Cache::rememberForever('Product:'.$key, function () use ($key) {
                        return Product::findOrFail($key);
                });
            }
            if($n == 4) {
               // break;
            }
            $n++;
		}
		$end_time = microtime(true);
        $execution_time = ($end_time - $start_time); 
		//$categories = Category::select('name','slug')->where('name', 'like', '%'.$request->search.'%')->orWhere('tag', 'like', '%'.$request->search.'%')->get()->take(3);
		//$subcategories = SubCategory::select('name','slug')->where('name', 'like', '%'.$request->search.'%')->get()->take(3);
		$subsubcategories = SubSubCategory::select('name', 'slug', 'sub_category_id')->where('name', 'like', '%'.$request->search.'%')->orWhere('tags', 'like', '%'.$request->search.'%')->get()->take(3);
		$categories = array();
		$subcategories = array();
		$brands = Brand::select('name','slug')->where('name','like', '%'.$request->search.'%')->orWhere('tags', 'like', '%'.$request->search.'%')->get();
	    $shops = Shop::whereIn('user_id', verified_sellers_id())->where('name', 'like', '%'.$request->search.'%')->get()->take(3);
		$end_time = microtime(true);
        $execution_time = ($end_time - $start_time);
        //var_dump($execution_time); 
        if(sizeof($keywords)>0 || sizeof($subsubcategories)>0 || sizeof($products)>0 || sizeof($brands) >0) {
            $data = view('frontend.partials.search_content', compact('products','categories','subcategories' ,'subsubcategories', 'keywords', 'shops', 'brands'))->render();
            $data = array('q' => $q, 'data' => $data);
		//$end_time = microtime(true);
        //$execution_time = ($end_time - $start_time);
        return response()->json($data);
        }
        $data = "0";
        $data = array('q' => $q, 'data' => $data);
        return response()->json($data);
    }
    
    //Product custom list
    function RemoveSpecialChar($str) {
      
    // Using str_replace() function 
    // to replace the word 
    $res = str_replace( array( '\'', '"',
    ',' , ';', '<', '>','[',']'), '', $str);
      
    // Returning the result 
    return $res;
    }
    public function product_custom(Request $request,$category_slug = null, $subcategory_slug = null, $subsubcategory_slug = null){
        
        $Category = Category::where('slug', $category_slug)->first();
        $category_id = ($Category != null) ? $Category->id : null;
        if(isset($category_id)){
            //DB::enableQueryLog();
            $customTitle = str_replace('-',' ',$subcategory_slug);
            $selectedProductId  = HomeCategory::where(['display_title'=>$customTitle])->first();
            
          //$query = DB::getQueryLog();
            //dd($query);
           
            $productIdArray = explode(',',$selectedProductId->products);
            $arrayFinalIndex = [];
            foreach($productIdArray as $key=>$val){
                $index =  $this->RemoveSpecialChar($val);
                array_push($arrayFinalIndex,$index);
            }
           
            DB::enableQueryLog();
            $productList  = Product::whereIn('id', $arrayFinalIndex)//["9794","9796","9798","9799"]
                    ->get();
            

            $query = DB::getQueryLog();
            //dd($query);
            //dd($productList);
            $catPro = $productList;
            
        }
        
      
        
        $query = $request->q;
        
//dd("test");

        if($category_slug == 'pharmacy'){
            return redirect()->route('pharmacy-enquiry');
        }
        $brand_id = (Brand::where('slug', $request->brand)->first() != null) ? Brand::where('slug', $request->brand)->first()->id : null;
        $sort_by = $request->sort_by;
        
        $brand_slug=$request->input('brand',null);
        if($category_slug == null) {
           $category_id = null;
        } else {
            $Category = Category::where('slug', $category_slug)->first();
           $category_id = ($Category != null) ? $Category->id : null;
        }
        
        if($subcategory_slug == null) {
           $subcategory_id = null;
        } else {
            $SubCategory = SubCategory::where('slug', $subcategory_slug)->first();
            $subcategory_id = ($SubCategory!= null) ? $SubCategory->id : null;
        }
        
        if($subsubcategory_slug == null) {
           $subsubcategory_id = null;
        } else {
            $SubSubCategory = SubSubCategory::where('slug', $subsubcategory_slug)->first();
            $subsubcategory_id = ($SubSubCategory != null) ? $SubSubCategory->id : null;
        }
        
     
        if($category_id != null) {
            $temp = SubCategory::where('category_id', $category_id)->where('slug', $subcategory_slug)->first();
            $subcategory_id = ($temp != null) ? $temp->id : null;
        } else {
            $temp = SubCategory::where('slug', $subcategory_slug)->first();
            $subcategory_id = ($temp != null) ? $temp->id : null;
        }
        
        if($subcategory_id != null) {
            $temp = SubSubCategory::where('sub_category_id', $subcategory_id)->where('slug', $subsubcategory_slug)->first();
            $subsubcategory_id = ($temp != null) ? $temp->id : null;
        } else {
            $subsubcategory_id = (SubSubCategory::where('slug', $subsubcategory_slug)->first() != null) ? SubSubCategory::where('slug', $subsubcategory_slug)->first()->id : null;
        }
        
        $cat_sort = true;
        
        $min_price = $request->min_price;
        $max_price = $request->max_price;
        $seller_id = $request->seller_id;
        $out_of_stock = $request->input('out_of_stock', 0);

        $conditions = ['published' => 1];
        
        if($brand_id != null){
            $conditions = array_merge($conditions, ['brand_id' => $brand_id]);
        }
        if($category_id != null){
            $conditions = array_merge($conditions, ['category_id' => $category_id]);
        }
        if($subcategory_id != null){
            $conditions = array_merge($conditions, ['subcategory_id' => $subcategory_id]);
        }
        if($subsubcategory_id != null){
            $conditions = array_merge($conditions, ['subsubcategory_id' => $subsubcategory_id]);
        }
        if($seller_id != null){
            $conditions = array_merge($conditions, ['user_id' => Seller::findOrFail($seller_id)->user->id]);
        }

        $products = Product::where($conditions)->where('variation_show_hide', 1);
        
        if($out_of_stock == 0){
            $products = $products->where('current_stock', '!=', '0')->where('unit_price', '!=', '0.00');
        } else {
            $cat_sort = false;
        }

        if($query != null){
            $searchController = new SearchController;
            $searchController->store($request);
            $products = $products->where('name', 'like', '%'.$query.'%')->orWhere('tags', 'like', '%'.$query.'%');
             $cat_sort = false;
        }
        
        
        
       // dd($sort_by);
        if($sort_by != null){
            switch ($sort_by) {
                case '1':
                    $products->orderBy('created_at', 'desc');
                    $cat_sort = false;
                    break;
                case '2':
                    $products->orderBy('created_at', 'asc');
                    $cat_sort = false;
                    break;
                case '3':
                    $products->orderBy('unit_price', 'asc');
                    $cat_sort = false;
                    break;
                case '4':
                    $products->orderBy('unit_price', 'desc');
                    $cat_sort = false;
                    break;
                default:
                    $products->orderBy('unit_price', 'asc');
                    $sort_by = 3;
                    $cat_sort = false;
                    break;
            }
        }
        
        $min_max = $products;
		$max = $min_max->max('unit_price');
		$min = $min_max->min('unit_price');

        if($min_price != null && $max_price != null){
            $products = $products->where('unit_price', '>=', $min_price)->where('unit_price', '<=', $max_price);
             $cat_sort = false;
        }
        
        if($cat_sort) {
            if($subsubcategory_id != null) {
                if($SubSubCategory->top_sku != null) {
                    $top_sku = $SubSubCategory->top_sku;
                    $top_sku = json_decode($top_sku, true);
                    krsort($top_sku);
                    if(count($top_sku) > 0) {
                        $top_sku = implode("\",\"", $top_sku);
                        $top_sku = '"'.$top_sku.'"';
                        $top_sku = str_replace(',""',"", $top_sku);
                        $products->orderByRaw("field(product_id, $top_sku) desc");
                    }
                }
            
            } elseif ($subcategory_id != null) {
                if($SubCategory->top_sku != null) {
                    $top_sku = $SubCategory->top_sku;
                    $top_sku = json_decode($top_sku, true);
                    krsort($top_sku);
                    if(count($top_sku) > 0) {
                        $top_sku = implode("\",\"", $top_sku);
                        $top_sku = '"'.$top_sku.'"';
                        $top_sku = str_replace(',""',"", $top_sku);
                        $products->orderByRaw("field(product_id, $top_sku) desc");
                    }
                }
                
            } elseif ($category_id != null) {
                if($Category->top_sku != null) {
                    $top_sku = $Category->top_sku;
                    $top_sku = json_decode($top_sku, true);
                    krsort($top_sku);
                    if(count($top_sku) > 0) {
                        $top_sku = implode("\",\"", $top_sku);
                        $top_sku = '"'.$top_sku.'"';
                        $top_sku = str_replace(',""',"", $top_sku);
                        $products->orderByRaw("field(product_id, $top_sku) desc");
                    }
                }
            }
            
            $products->orderBy('unit_price', 'asc');
            $sort_by = 3;
        }

        $non_paginate_products = filter_products($products)->get();
        
        //Attribute Filter

        $attributes = array();
        foreach ($non_paginate_products as $key => $product) {
            if($product->attributes != null && is_array(json_decode($product->attributes))){
                foreach (json_decode($product->attributes) as $key => $value) {
                    $flag = false;
                    $pos = 0;
                    foreach ($attributes as $key => $attribute) {
                        if($attribute['id'] == $value){
                            $flag = true;
                            $pos = $key;
                            break;
                        }
                    }
                    if(!$flag){
                        $item['id'] = $value;
                        $item['values'] = array();
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if($choice_option->attribute_id == $value){
                                $item['values'] = $choice_option->values;
                                break;
                            }
                        }
                        array_push($attributes, $item);
                    }
                    else {
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if($choice_option->attribute_id == $value){
                                foreach ($choice_option->values as $key => $value) {
                                    if(!in_array($value, $attributes[$pos]['values'])){
                                        array_push($attributes[$pos]['values'], $value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $selected_attributes = array();

        foreach ($attributes as $key => $attribute) {
            if($request->has('attribute_'.$attribute['id'])){
                foreach ($request['attribute_'.$attribute['id']] as $key => $value) {
                    $str = '"'.$value.'"';
                    $products = $products->where('choice_options', 'like', '%'.$str.'%');
                }

                $item['id'] = $attribute['id'];
                $item['values'] = $request['attribute_'.$attribute['id']];
                array_push($selected_attributes, $item);
            }
        }


        //Color Filter
        $all_colors = array();

        foreach ($non_paginate_products as $key => $product) {
            if ($product->colors != null) {
                foreach (json_decode($product->colors) as $key => $color) {
                    if(!in_array($color, $all_colors)){
                        array_push($all_colors, $color);
                    }
                }
            }
        }

        $selected_color = null;

        if($request->has('color')){
            $str = '"'.$request->color.'"';
            $products = $products->where('colors', 'like', '%'.$str.'%');
            $selected_color = $request->color;
        }

	//dd($products->get());	
        $products = filter_products($products)->paginate(20)->appends(request()->query());
        if ($request->ajax()) {
			return view('frontend.product_listing_ajax', compact('products'));
		}
        return view('frontend.product_custom', compact('catPro', 'query', 'out_of_stock', 'category_id', 'subcategory_id', 'subsubcategory_id', 'brand_id', 'sort_by', 'seller_id','min_price', 'max_price', 'attributes', 'selected_attributes', 'all_colors', 'selected_color','max','min','brand_slug'));
       // return view('frontend.product_custom', compact('products'));
    
         
        
        
    }
    
    
    
	public function category_view(Request $request,$category_slug = null, $subcategory_slug = null, $subsubcategory_slug = null) 
	{   
        $query = $request->q;
        

        
        if($category_slug == 'pharmacy'){
            return redirect()->route('pharmacy-enquiry');
        }
        $brand_id = (Brand::where('slug', $request->brand)->first() != null) ? Brand::where('slug', $request->brand)->first()->id : null;
        $sort_by = $request->sort_by;
        
        $brand_slug=$request->input('brand',null);
        if($category_slug == null) {
           $category_id = null;
        } else {
            $Category = Category::where('slug', $category_slug)->first();
           $category_id = ($Category != null) ? $Category->id : null;
        }
        
        if($subcategory_slug == null) {
           $subcategory_id = null;
        } else {
            $SubCategory = SubCategory::where('slug', $subcategory_slug)->first();
            $subcategory_id = ($SubCategory!= null) ? $SubCategory->id : null;
        }
        
        if($subsubcategory_slug == null) {
           $subsubcategory_id = null;
        } else {
            $SubSubCategory = SubSubCategory::where('slug', $subsubcategory_slug)->first();
            $subsubcategory_id = ($SubSubCategory != null) ? $SubSubCategory->id : null;
        }
        
     
        if($category_id != null) {
            $temp = SubCategory::where('category_id', $category_id)->where('slug', $subcategory_slug)->first();
            $subcategory_id = ($temp != null) ? $temp->id : null;
        } else {
            $temp = SubCategory::where('slug', $subcategory_slug)->first();
            $subcategory_id = ($temp != null) ? $temp->id : null;
        }
        
        if($subcategory_id != null) {
            $temp = SubSubCategory::where('sub_category_id', $subcategory_id)->where('slug', $subsubcategory_slug)->first();
            $subsubcategory_id = ($temp != null) ? $temp->id : null;
        } else {
            $subsubcategory_id = (SubSubCategory::where('slug', $subsubcategory_slug)->first() != null) ? SubSubCategory::where('slug', $subsubcategory_slug)->first()->id : null;
        }
        
        $cat_sort = true;
        
        $min_price = $request->min_price;
        $max_price = $request->max_price;
        $seller_id = $request->seller_id;
        $out_of_stock = $request->input('out_of_stock', 0);

        $conditions = ['published' => 1];
        
        if($brand_id != null){
            $conditions = array_merge($conditions, ['brand_id' => $brand_id]);
        }
        if($category_id != null){
            $conditions = array_merge($conditions, ['category_id' => $category_id]);
        }
        if($subcategory_id != null){
            $conditions = array_merge($conditions, ['subcategory_id' => $subcategory_id]);
        }
        if($subsubcategory_id != null){
            $conditions = array_merge($conditions, ['subsubcategory_id' => $subsubcategory_id]);
        }
        if($seller_id != null){
            $conditions = array_merge($conditions, ['user_id' => Seller::findOrFail($seller_id)->user->id]);
        }

        $products = Product::where($conditions)->where('variation_show_hide', 1);
        
        if($out_of_stock == 0){
            $products = $products->where('current_stock', '!=', '0')->where('unit_price', '!=', '0.00');
        } else {
            $cat_sort = false;
        }

        if($query != null){
            $searchController = new SearchController;
            $searchController->store($request);
            $products = $products->where('name', 'like', '%'.$query.'%')->orWhere('tags', 'like', '%'.$query.'%');
             $cat_sort = false;
        }
        
        
        
       // dd($sort_by);
        if($sort_by != null){
            switch ($sort_by) {
                case '1':
                    $products->orderBy('created_at', 'desc');
                    $cat_sort = false;
                    break;
                case '2':
                    $products->orderBy('created_at', 'asc');
                    $cat_sort = false;
                    break;
                case '3':
                    $products->orderBy('unit_price', 'asc');
                    $cat_sort = false;
                    break;
                case '4':
                    $products->orderBy('unit_price', 'desc');
                    $cat_sort = false;
                    break;
                default:
                    $products->orderBy('unit_price', 'asc');
                    $sort_by = 3;
                    $cat_sort = false;
                    break;
            }
        }
        
        $min_max = $products;
		$max = $min_max->max('unit_price');
		$min = $min_max->min('unit_price');

        if($min_price != null && $max_price != null){
            $products = $products->where('unit_price', '>=', $min_price)->where('unit_price', '<=', $max_price);
             $cat_sort = false;
        }
        
        if($cat_sort) {
            if($subsubcategory_id != null) {
                if($SubSubCategory->top_sku != null) {
                    $top_sku = $SubSubCategory->top_sku;
                    $top_sku = json_decode($top_sku, true);
                    krsort($top_sku);
                    if(count($top_sku) > 0) {
                        $top_sku = implode("\",\"", $top_sku);
                        $top_sku = '"'.$top_sku.'"';
                        $top_sku = str_replace(',""',"", $top_sku);
                        $products->orderByRaw("field(product_id, $top_sku) desc");
                    }
                }
            
            } elseif ($subcategory_id != null) {
                if($SubCategory->top_sku != null) {
                    $top_sku = $SubCategory->top_sku;
                    $top_sku = json_decode($top_sku, true);
                    krsort($top_sku);
                    if(count($top_sku) > 0) {
                        $top_sku = implode("\",\"", $top_sku);
                        $top_sku = '"'.$top_sku.'"';
                        $top_sku = str_replace(',""',"", $top_sku);
                        $products->orderByRaw("field(product_id, $top_sku) desc");
                    }
                }
                
            } elseif ($category_id != null) {
                if($Category->top_sku != null) {
                    $top_sku = $Category->top_sku;
                    $top_sku = json_decode($top_sku, true);
                    krsort($top_sku);
                    if(count($top_sku) > 0) {
                        $top_sku = implode("\",\"", $top_sku);
                        $top_sku = '"'.$top_sku.'"';
                        $top_sku = str_replace(',""',"", $top_sku);
                        $products->orderByRaw("field(product_id, $top_sku) desc");
                    }
                }
            }
            
            $products->orderBy('unit_price', 'asc');
            $sort_by = 3;
        }

        $non_paginate_products = filter_products($products)->get();
        
        //Attribute Filter

        $attributes = array();
        foreach ($non_paginate_products as $key => $product) {
            if($product->attributes != null && is_array(json_decode($product->attributes))){
                foreach (json_decode($product->attributes) as $key => $value) {
                    $flag = false;
                    $pos = 0;
                    foreach ($attributes as $key => $attribute) {
                        if($attribute['id'] == $value){
                            $flag = true;
                            $pos = $key;
                            break;
                        }
                    }
                    if(!$flag){
                        $item['id'] = $value;
                        $item['values'] = array();
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if($choice_option->attribute_id == $value){
                                $item['values'] = $choice_option->values;
                                break;
                            }
                        }
                        array_push($attributes, $item);
                    }
                    else {
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if($choice_option->attribute_id == $value){
                                foreach ($choice_option->values as $key => $value) {
                                    if(!in_array($value, $attributes[$pos]['values'])){
                                        array_push($attributes[$pos]['values'], $value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $selected_attributes = array();

        foreach ($attributes as $key => $attribute) {
            if($request->has('attribute_'.$attribute['id'])){
                foreach ($request['attribute_'.$attribute['id']] as $key => $value) {
                    $str = '"'.$value.'"';
                    $products = $products->where('choice_options', 'like', '%'.$str.'%');
                }

                $item['id'] = $attribute['id'];
                $item['values'] = $request['attribute_'.$attribute['id']];
                array_push($selected_attributes, $item);
            }
        }


        //Color Filter
        $all_colors = array();

        foreach ($non_paginate_products as $key => $product) {
            if ($product->colors != null) {
                foreach (json_decode($product->colors) as $key => $color) {
                    if(!in_array($color, $all_colors)){
                        array_push($all_colors, $color);
                    }
                }
            }
        }

        $selected_color = null;

        if($request->has('color')){
            $str = '"'.$request->color.'"';
            $products = $products->where('colors', 'like', '%'.$str.'%');
            $selected_color = $request->color;
        }

	//dd($products->get());	
        $products = filter_products($products)->paginate(20)->appends(request()->query());
        if ($request->ajax()) {
			return view('frontend.product_listing_ajax', compact('products'));
		}
        return view('frontend.product_listing', compact('products', 'query', 'out_of_stock', 'category_id', 'subcategory_id', 'subsubcategory_id', 'brand_id', 'sort_by', 'seller_id','min_price', 'max_price', 'attributes', 'selected_attributes', 'all_colors', 'selected_color','max','min','brand_slug'));
    
	}
	public function subsubcategories_product(Request $request,$subcategory_slug){
		$SubCategory=SubCategory::where('slug',$subcategory_slug)->first();
		//dd($SubCategory);
		$SubCategoryID=$SubCategory->id;
		$CategoryID=$SubCategory->category_id;
		//dd($CategoryID);
		//Find subSubcategories of CategoryID
		$SubSubCategory=SubSubCategory::where('sub_category_id',$SubCategoryID)->get();
		return view('frontend.product_sub_subcategories', compact('SubCategory','SubSubCategory','SubCategoryID','CategoryID'));
	}
	
	public function subsubcategories_product1(Request $request,$category_slug, $subcategory_slug){
	    $Category=Category::where('slug',$category_slug)->first();
		$CategoryID=$Category->id;
		$SubCategory=SubCategory::where('category_id', $CategoryID)->where('slug',$subcategory_slug)->first();
		//dd($SubCategory);
		$SubCategoryID=$SubCategory->id;
		//dd($CategoryID);
		//Find subSubcategories of CategoryID
		$SubSubCategory=SubSubCategory::where('sub_category_id',$SubCategoryID)->get();
		return view('frontend.product_sub_subcategories', compact('SubCategory','SubSubCategory','SubCategoryID','CategoryID'));
	}
	
    public function search(Request $request)
    {
        $query = $request->q;
       
        $brand_id = (Brand::where('slug', $request->brand)->first() != null) ? Brand::where('slug', $request->brand)->first()->id : null;
       $sort_by = $request->sort_by;
     
        $category_id = (Category::where('slug', $request->category)->first() != null) ? Category::where('slug', $request->category)->first()->id : null;
     
        if($category_id != null) {
            $temp = SubCategory::where('category_id', $category_id)->where('slug', $request->subcategory)->first();
            $subcategory_id = ($temp != null) ? $temp->id : null;
        } else {
            $temp = SubCategory::where('slug', $request->subcategory)->first();
            $subcategory_id = ($temp != null) ? $temp->id : null;
        }
        if($subcategory_id != null) {
            $temp = SubSubCategory::where('sub_category_id', $subcategory_id)->where('slug', $request->subsubcategory)->first();
             $subsubcategory_id = ($temp != null) ? $temp->id : null;
        } else {
            $subsubcategory_id = (SubSubCategory::where('slug', $request->subsubcategory)->first() != null) ? SubSubCategory::where('slug', $request->subsubcategory)->first()->id : null;
        }
        $min_price = $request->min_price;
        $max_price = $request->max_price;
        $seller_id = $request->seller_id;

        $conditions = ['published' => 1];
        
        if($brand_id != null){
            $conditions = array_merge($conditions, ['brand_id' => $brand_id]);
        }
        if($category_id != null){
            $conditions = array_merge($conditions, ['category_id' => $category_id]);
        }
        if($subcategory_id != null){
            $conditions = array_merge($conditions, ['subcategory_id' => $subcategory_id]);
        }
        if($subsubcategory_id != null){
            $conditions = array_merge($conditions, ['subsubcategory_id' => $subsubcategory_id]);
        }
        if($seller_id != null){
            $conditions = array_merge($conditions, ['user_id' => Seller::findOrFail($seller_id)->user->id]);
        }

        $products = Product::where($conditions);

        if($min_price != null && $max_price != null){
            $products = $products->where('unit_price', '>=', $min_price)->where('unit_price', '<=', $max_price);
        }

        if($query != null){
            $searchController = new SearchController;
            $searchController->store($request);
            $products = $products->where('name', 'like', '%'.$query.'%')->orWhere('tags', 'like', '%'.$query.'%');
        }
       // dd($sort_by);
        if($sort_by != null){
            switch ($sort_by) {
                case '1':
                    $products->orderBy('created_at', 'desc');
                    break;
                case '2':
                    $products->orderBy('created_at', 'asc');
                    break;
                case '3':
                    $products->orderBy('unit_price', 'asc');
                    break;
                case '4':
                    $products->orderBy('unit_price', 'desc');
                    break;
                default:
                    // code...
                    break;
            }
        }


        $non_paginate_products = filter_products($products)->get();

        //Attribute Filter

        $attributes = array();
        foreach ($non_paginate_products as $key => $product) {
            if($product->attributes != null && is_array(json_decode($product->attributes))){
                foreach (json_decode($product->attributes) as $key => $value) {
                    $flag = false;
                    $pos = 0;
                    foreach ($attributes as $key => $attribute) {
                        if($attribute['id'] == $value){
                            $flag = true;
                            $pos = $key;
                            break;
                        }
                    }
                    if(!$flag){
                        $item['id'] = $value;
                        $item['values'] = array();
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if($choice_option->attribute_id == $value){
                                $item['values'] = $choice_option->values;
                                break;
                            }
                        }
                        array_push($attributes, $item);
                    }
                    else {
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if($choice_option->attribute_id == $value){
                                foreach ($choice_option->values as $key => $value) {
                                    if(!in_array($value, $attributes[$pos]['values'])){
                                        array_push($attributes[$pos]['values'], $value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $selected_attributes = array();

        foreach ($attributes as $key => $attribute) {
            if($request->has('attribute_'.$attribute['id'])){
                foreach ($request['attribute_'.$attribute['id']] as $key => $value) {
                    $str = '"'.$value.'"';
                    $products = $products->where('choice_options', 'like', '%'.$str.'%');
                }

                $item['id'] = $attribute['id'];
                $item['values'] = $request['attribute_'.$attribute['id']];
                array_push($selected_attributes, $item);
            }
        }


        //Color Filter
        $all_colors = array();

        foreach ($non_paginate_products as $key => $product) {
            if ($product->colors != null) {
                foreach (json_decode($product->colors) as $key => $color) {
                    if(!in_array($color, $all_colors)){
                        array_push($all_colors, $color);
                    }
                }
            }
        }

        $selected_color = null;

        if($request->has('color')){
            $str = '"'.$request->color.'"';
            $products = $products->where('colors', 'like', '%'.$str.'%');
            $selected_color = $request->color;
        }

        $min_max = $products;
		$max = $min_max->max('unit_price');
		$min = $min_max->min('unit_price');
		
        $products = filter_products($products)->paginate(20)->appends(request()->query());
        if ($request->ajax()) {
			return view('frontend.product_listing_ajax', compact('products'));
		}
        return view('frontend.product_listing', compact('products', 'query', 'category_id', 'subcategory_id', 'subsubcategory_id', 'brand_id', 'sort_by', 'seller_id','min_price', 'max_price', 'attributes', 'selected_attributes', 'all_colors', 'selected_color','max','min'));
    }

    public function product_content(Request $request){
        $connector  = $request->connector;
        $selector   = $request->selector;
        $select     = $request->select;
        $type       = $request->type;
        productDescCache($connector,$selector,$select,$type);
    }

    public function home_settings(Request $request)
    {  
       return view('home_settings.index');
    }

    public function top_10_settings(Request $request)
    {   
        foreach (Category::all() as $key => $category) {
            if(in_array($category->id, $request->top_categories)){
                $category->top = 1;
                $category->save();
				Cache::forget('topcategories');
            }
            else{
                $category->top = 0;
                $category->save();
				Cache::forget('topcategories');
            }
        }

        foreach (Brand::all() as $key => $brand) {
            if(in_array($brand->id, $request->top_brands)){
                $brand->top = 1;
                $brand->save();
            }
            else{
                $brand->top = 0;
                $brand->save();
            }
        }

        flash(__('Top 20 categories and brands have been updated successfully'))->success();
        return redirect()->route('home_settings.index');
    }

    public function variant_price(Request $request)
    {
        $product = Product::find($request->id);
		$brand_id=$product->brand_id;
		$CategoryID=$product->category_id;
        $str = '';
        $quantity = 0;

        if($request->has('color')){
            $data['color'] = $request['color'];
            $str = $request['color'];
        }

        if(json_decode(Product::find($request->id)->choice_options) != null){
            foreach (json_decode(Product::find($request->id)->choice_options) as $key => $choice) {
                if($str != null){
                    $str .= '-'.str_replace(' ', '', $request['attribute_id_'.$choice->attribute_id]);
                }
                else{
                    $str .= str_replace(' ', '', $request['attribute_id_'.$choice->attribute_id]);
                }
            }
        }



        if($str != null && $product->variant_product){
            $product_stock = $product->stocks->where('variant', $str)->first();
            $price = $product_stock->price;
            $quantity = $product_stock->qty;
        }
        else{
            $price = $product->unit_price;
            if($product->current_stock > -1) {
                $quantity = $product->current_stock;
            } else {
                $quantity = 100;
            }
        }

        //discount calculation
        $flash_deals = \App\FlashDeal::where('status', 1)->get();
        $inFlashDeal = false;
        foreach ($flash_deals as $key => $flash_deal) {
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first() != null) {
                $flash_deal_product = \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first();
                if($flash_deal_product->discount_type == 'percent'){
                    $price -= ($price*$flash_deal_product->discount)/100;
                }
                elseif($flash_deal_product->discount_type == 'amount'){
                    $price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }
		
        
        if(env('CACHE_DRIVER') == 'redis') {
           $category_deals = Cache::tags(['Category'])->rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
            });
        } else {
             $category_deals = Cache::rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
            });
        }
        
        if(env('CACHE_DRIVER') == 'redis') {
            $brand_deals = Cache::tags(['Brand'])->rememberForever('Brand:'.$brand_id, function () use ($brand_id) {
                return  \App\Brand::where('id', $brand_id)->first();
            });
        } else {
            $brand_deals = Cache::rememberForever('Brand:'.$brand_id, function () use ($brand_id) {
                return  \App\Brand::where('id', $brand_id)->first();
             });
        }
        if (!$inFlashDeal) {
			$product_discount=$product->discount;
			if($product_discount==null){$product_discount=0;}
			// if(!isset($category_deals->discount))
			// {
				// dd($id);
			// }
			$category_discount=$category_deals->discount;
			$brand_discount=$brand_deals->discount;
			$pro_dis = strtolower($product->discount_type);
			if($pro_dis == 'percent'){
				if(($product_discount>=$category_discount) and($product_discount>=$brand_discount)){
					$price -= ($price*$product->discount)/100;
				}
				elseif($category_discount>=$brand_discount){
					$price -= ($price*$category_discount)/100;
				}
				else{
					$price -= ($price*$brand_discount)/100;
				}
			}
			elseif($pro_dis == 'amount'){
				
				$category_discount_amount=($price*$category_discount)/100;
				$brand_discount_amount=($price*$brand_discount)/100;
				
				if(($product_discount>=$category_discount_amount) and($product_discount>=$brand_discount_amount)){
					$price -= $product->discount;
				}
				elseif($category_discount_amount>=$brand_discount_amount){
					$price -= $category_discount_amount;
				}
				else{
					$price -= $brand_discount_amount;
				}
			}
        
		}

        if($product->tax_type == 'percent'){
            $price += ($price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $price += $product->tax;
        }

        return array('price' => single_price($price*$request->quantity), 'quantity' => $quantity, 'digital' => $product->digital);
    }

    public function sellerpolicy(){
        return view("frontend.policies.sellerpolicy");
    }

    public function returnpolicy(){
        return view("frontend.policies.returnpolicy");
    }

    public function supportpolicy(){
        return view("frontend.policies.supportpolicy");
    }

    public function terms(){
        
        return view("frontend.policies.terms");
    }
    
    public function cashback(){
        return view("frontend.policies.cashback");
    }
    
    public function commission(){
        $categories = Category::all();
        $sellerCommission = SellerCommision::where('user_id',Auth::user()->id)->orderBy('id', 'desc')->get();
       // dd('seller_list');
        return view("frontend.commission",compact('categories','sellerCommission'));
    }
    public function privacypolicy(){
        return view("frontend.policies.privacypolicy");
    }

    public function get_pick_ip_points(Request $request)
    {
        $pick_up_points = PickupPoint::all();
        return view('frontend.partials.pick_up_points', compact('pick_up_points'));
    }

    public function get_category_items(Request $request){
        $category = Category::findOrFail($request->id);
        return view('frontend.partials.category_elements', compact('category'));
    }

    public function premium_package_index()
    {
        $customer_packages = CustomerPackage::all();
        return view('frontend.partials.customer_packages_lists',compact('customer_packages'));
    }

    public function seller_digital_product_list(Request $request)
    {
        $products = Product::where('user_id', Auth::user()->id)->where('digital', 1)->orderBy('created_at', 'desc')->paginate(10);
        return view('frontend.seller.digitalproducts.products', compact('products'));
    }
    
    public function show_digital_product_upload_form(Request $request)
    {
        $business_settings = BusinessSetting::where('type', 'digital_product_upload')->first();
        $categories = Category::where('digital', 1)->get();
        return view('frontend.seller.digitalproducts.product_upload', compact('categories'));
    }

    public function show_digital_product_edit_form(Request $request, $id)
    {
        $categories = Category::where('digital', 1)->get();
        $product = Product::find(decrypt($id));
        return view('frontend.seller.digitalproducts.product_edit', compact('categories', 'product'));
    }
	//Pincode checking

	public function checkpicode(Request $request)
	{	
		$pincode=$request->input('pincode');
		$url="https://api.postalpincode.in/pincode/".$pincode;
		$request = file_get_contents($url);
		$response=json_decode($request,true);
		if($response[0]['Message'] =="No records found" || $response[0]['PostOffice']=="null" || $response[0]['Status']=="Error")
			$response=['success' => false, 'value' => "Error"];
		else{
			$districtName=$response[0]['PostOffice'][0]['District'];
			$response=['success' => true, 'value' => $districtName];
		}
		return response()->json($response);
	}
	public function getpincode(Request $request)
	{	
		$cityname=$request->input('cityname');
		$cityname=str_ireplace(" ", "%20", $cityname);
		$url="https://api.postalpincode.in/postoffice/".$cityname;
		$request = file_get_contents($url);
		$response=json_decode($request,true);
		
		if($response[0]['Message'] =="No records found" || $response[0]['PostOffice']=="null" || $response[0]['Status']=="Error")
			$response=['success' => false, 'value' => "Error"];
		else{
			$Pincode=$response[0]['PostOffice'][0]['Pincode'];
			$response=['success' => true, 'value' => $Pincode];
		}
		
		return response()->json($response);
	}
	
	public function clearcache (){
		Cache::flush();
		flash(__('Cache has been Cleared successfully'))->success();
		return back();
	}
	public function aboutus(){
		return view('frontend.aboutUs');
	}
	public function career(){
		return view('frontend.career');
	}
	public function contactus(){
		return view('frontend.contactus');
	}
	public function contact_send(Request $request){
		$Name=$request->Name;
		$Email=$request->Email;
		if($Name=="" && $Email=="")
		{
			$name=$request->name;
			$email=$request->email;
			$phone=$request->phone;
			$subject=$request->subject;
			$message=$request->message;
			
			$sub = "Contact Inquiry from Website";
			$msg = "Dear Admin, <br /> You have received a Contact Inquiery from User, <br /> ============== <br />Name : <b>$name</b> <br />Mobile : <b>$phone</b> <br />Email : <b>$email</b> <br />Subject : <b>$subject</b> <br />Message : <b>$message</b> <br /><br />======================";
				
	        $data = array('msg' => $msg, 'sub' => $sub, "Email" => $email, "Name" => $name);
	       
			Mail::send('emails.confirmation', $data, function($message) use ($data) {
    				 $message->to("deeps17may@gmail.com", "Alde Bazaar")->subject('User Message from Alde Bazaar');
    				 $message->replyto($data['Email'],$data['Name']);
    				 $message->from("no-reply@aldebazaar.com","Alde Bazaar");
			    });
		
			return view('frontend.contactus');
		} else {
			return view('frontend.contactus');
		}
	}
	
	public function blog()
	{
//         if (Cache::has('Blog')){
// 		   $Blog =  Cache::get('Blog');
// 		} else {
// 			$Blog = Blog::orderBy('created_at','desc')->paginate(12);//list
// 			Cache::forever('Blog', $Blog);
// 		}
		$Blog = Blog::orderBy('created_at','desc')->paginate(12);//list
        $PopularBlog = Blog::orderBy('BlogView','desc')->paginate(15);
		$data =array("Blog" => $Blog, "PopularBlog" => $PopularBlog);
		return view('frontend.blog', $data);
	}
	public function BlogView($Slug)
	{
        $Blog = Blog::where('URLSlug', $Slug)->first();
		$Blog->BlogView = $Blog->BlogView + 1;
		$Blog->save();
		$thisblogid=$Blog->id;
		$PopularBlog = Blog::where('id','!=',$thisblogid)->orderBy('BlogView','desc')->paginate(10);
		$data =array("Blog" => $Blog,"PopularBlog" => $PopularBlog);
		return view('frontend.blogview', $data);
	}
	
	public function product_request(Request $request){
		$phone=$request->phone;
		$subject=$request->subject;
		if($phone!="" || $subject!=""){
			$result = array('success' => true);
		}else{
			$productSlug=$request->productname;
			$name=$request->name;
			$mobile=$request->mobile;
			$email=$request->email;
			$message=$request->message;
			
			$ProductRequest=new ProductRequest;
			$ProductRequest->product=$productSlug;
			$ProductRequest->name=$name;
			$ProductRequest->phone=$mobile;
			$ProductRequest->email=$email;
			$ProductRequest->message=$message;
			if($ProductRequest->save()){
				$result = array('success' => true);
			}
			else{
				$result = array('success' => false);
			}
		}
		return response()->json($result);
	}
	public function show(){
		$ProductRequest=ProductRequest::orderBy('created_at','desc')->get();
		return view('ProductRequest.index', compact('ProductRequest'));
	}
	public function PRSetStatus($id){
		$explode=explode("~",$id);
		$id=$explode[0];
		$status=$explode[1];
	
		$ProductRequest=ProductRequest::find($id);
		if($status=="Email")
			$status="Precure";
			$productSlug=$ProductRequest->product;
			$product=Product::where('slug',$productSlug)->first();
			if($product==null){
				flash(__('Requested Product does not exist.'))->error();
				return back();
				}
		$ProductRequest->status=$status;
		$ProductRequest->save();
		if($status=="Precure"){
			if($ProductRequest->email!="")
			{
				$productName=$product->name;
				$productImage=$product->thumbnail_img;
				
				$home_base_price = home_base_price($product->id);
				$home_discounted_base_price = home_discounted_base_price($product->id);
				if($home_base_price != $home_discounted_base_price){
					$Price=$home_base_price;
					$discountedPrice=$home_discounted_base_price;
					$total_discount=discount_calulate($home_base_price, $home_discounted_base_price );
				}
				else{
					$Price=""; $total_discount="";
					$discountedPrice=$home_discounted_base_price;
				}
				
				$data = array('email' => $ProductRequest->email, 'name' => $ProductRequest->name,'ProductName'=>$productName,'slug'=>$productSlug,'image'=>$productImage,'Price'=>$Price,'total_discount'=>$total_discount,'discountedPrice'=>$discountedPrice);	
				Mail::send('emails.notify_product_request', ['data' => $data], function ($m) use ($data) {
					$m->to($data['email'], $data['name'])->subject("Alde Bazaar :: ".$data['ProductName']." is in stock. Now you can buy it from Aldebazaar.com");
				});
				return back();
			}
		}
		flash(__('Status Changed successfully'))->success();
		return redirect()->route('productrequest.show',compact('ProductRequest'));
	}
	/*public function PRSendEmail($id){
		$ProductRequest=ProductRequest::find($id);
		if($ProductRequest->email!="")
		{
			$productSlug=$ProductRequest->product;
			$product=Product::where('slug',$productSlug)->first();
			$productName=$product->name;
			$productImage=$product->thumbnail_img;
			
			$home_base_price = home_base_price($product->id);
			$home_discounted_base_price = home_discounted_base_price($product->id);
			if($home_base_price != $home_discounted_base_price){
				$Price=$home_base_price;
				$discountedPrice=$home_discounted_base_price;
				$total_discount=discount_calulate($home_base_price, $home_discounted_base_price );
			}
			else{
				$Price=""; $total_discount="";
				$discountedPrice=$home_discounted_base_price;
			}
			
			$data = array('email' => $ProductRequest->email, 'name' => $ProductRequest->name,'ProductName'=>$productName,'slug'=>$productSlug,'image'=>$productImage,'Price'=>$Price,'total_discount'=>$total_discount,'discountedPrice'=>$discountedPrice);	
			Mail::send('emails.notify_product_request', ['data' => $data], function ($m) use ($data) {
				$m->to($data['email'], $data['name'])->subject("Alde Bazaar :: ".$data['ProductName']." is in stock. Now you can buy it from Aldebazaar.com");
			});
			return back();
		}
	}*/

	public function PRDestroy($id){
		$ProductRequest=ProductRequest::find($id);
		$ProductRequest->delete();
		flash(__('Deleted successfully'))->success();
		return redirect()->route('productrequest.show',compact('ProductRequest'));
	}

}
