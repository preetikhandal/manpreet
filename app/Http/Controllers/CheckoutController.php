<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\InstamojoController;
use App\Http\Controllers\ClubPointController;
use App\Http\Controllers\StripePaymentController;
use App\Http\Controllers\PublicSslCommerzPaymentController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\AffiliateController;
use App\Http\Controllers\PaytmController;
use App\Order;
use App\BusinessSetting;
use App\Coupon;
use App\CouponUsage;
use App\User;
use App\Customer;
use App\Wallet;
use PDF;
use Mail;
use Session;
use Carbon\Carbon;
use App\Mail\InvoiceEmailManager;
use Hash;
use App\Product;
use App\Cashback;
use App\UserCashbackConfig;
use DB;
class CheckoutController extends Controller
{

    public function __construct()
    {
        //
    }

    //check the selected payment gateway and redirect to that controller accordingly
    public function checkout(Request $request)
    {   
        if ($request->payment_option != null) {

            $orderController = new OrderController;
            $orderController->store($request);

            $request->session()->put('payment_type', 'cart_payment');

            if($request->session()->get('order_id') != null){
                if($request->payment_option == 'paypal'){
                    $paypal = new PaypalController;
                    return $paypal->getCheckout();
                }
                elseif ($request->payment_option == 'stripe') {
                    $stripe = new StripePaymentController;
                    return $stripe->stripe();
                }
                elseif ($request->payment_option == 'sslcommerz') {
                    $sslcommerz = new PublicSslCommerzPaymentController;
                    return $sslcommerz->index($request);
                }
                elseif ($request->payment_option == 'instamojo') {
                    $instamojo = new InstamojoController;
                    return $instamojo->pay($request);
                }
                elseif ($request->payment_option == 'razorpay') {
                    $razorpay = new RazorpayController;
                    return $razorpay->payWithRazorpay($request);
                }
                elseif ($request->payment_option == 'paystack') {
                    $paystack = new PaystackController;
                    return $paystack->redirectToGateway($request);
                }
                elseif ($request->payment_option == 'voguepay') {
                    $voguePay = new VoguePayController;
                    return $voguePay->customer_showForm();
                }
                elseif ($request->payment_option == 'paytm') {
                    $paytm = new PaytmController;
                    return $paytm->index();
                }

                elseif ($request->payment_option == 'cash_on_delivery') {
                    $request->session()->put('cart', collect([]));
                    // $request->session()->forget('order_id');
                    $request->session()->forget('delivery_info');
                    $request->session()->forget('coupon_id');
                    $request->session()->forget('coupon_discount');
                    $request->session()->forget('wallet_credit');

                    flash("Your order has been placed successfully")->success();
                	return redirect()->route('order_confirmed');
                }
                elseif ($request->payment_option == 'wallet') {
                    $user = Auth::user();
                    $user->balance -= Order::findOrFail($request->session()->get('order_id'))->grand_total;
                    $user->save();
                    $wallet = new Wallet;
                    $wallet->user_id = $user->id;
                    $wallet->amount = Order::findOrFail($request->session()->get('order_id'))->grand_total;
                    $wallet->payment_method = 'Wallet Payment';
                    $wallet->payment_details = 'Purchase For Order #'. Order::findOrFail($request->session()->get('order_id'))->code;
                    $wallet->add_status = 1;
                    $wallet->save();
                    return $this->checkout_done($request->session()->get('order_id'), null);
                }
                else{
                    $order = Order::findOrFail($request->session()->get('order_id'));
                    $order->manual_payment = 1;
                    $order->save();

                    $request->session()->put('cart', collect([]));
                    // $request->session()->forget('order_id');
                    $request->session()->forget('delivery_info');
                    $request->session()->forget('coupon_id');
                    $request->session()->forget('coupon_discount');
                    $request->session()->forget('wallet_credit');
                    

                    flash(__('Your order has been placed successfully. Please submit payment information from purchase history'))->success();
                	return redirect()->route('order_confirmed');
                }
            }
        }else {
            flash(__('Select Payment Option.'))->success();
            return back();
        }
    }

    //redirects to this method after a successfull checkout
    public function checkout_done($order_id, $payment)
    {
        $order = Order::findOrFail($order_id);
        $order->payment_status = 'paid';
        $order->payment_details = $payment;
        $order->save();
        $shipping_address = json_decode($order->shipping_address);
        
        //Process start
        $pdf = PDF::setOptions([
                            'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                            'logOutputFile' => storage_path('logs/log.htm'),
                            'tempDir' => storage_path('logs/')
                        ])->loadView('invoices.customer_invoice', compact('order'));
                $output = $pdf->output();
        		file_put_contents('invoices/'.'Order#'.$order->code.'.pdf', $output);
        		
                $array['view'] = 'emails.invoice';
                $array['order'] = $order;
                $array['subject'] = 'Order Placed - '.$order->code;
                $array['from'] = env('MAIL_FROM_ADDRESS');
                $array['content'] = 'Hi. A new order has been placed. Please check the attached invoice.';
                $array['file'] = 'invoices/Order#'.$order->code.'.pdf';
                $array['file_name'] = 'Order#'.$order->code.'.pdf';
                $admin_products = array();
                $seller_products = array();
                $vendor_products = array();
                foreach ($order->orderDetails as $key => $orderDetail){
                    if($orderDetail->product->added_by == 'admin'){
                        array_push($admin_products, $orderDetail->product->id);
                    }
                    elseif($orderDetail->product->added_by == 'vendor') {
                        $product_ids = array();
                        if(array_key_exists($orderDetail->product->user_id, $vendor_products)){
                            $product_ids = $vendor_products[$orderDetail->product->user_id]; // checking old data for seller.
                        }
                        array_push($product_ids, $orderDetail->product->id);
                        $vendor_products[$orderDetail->product->user_id] = $product_ids;
                    }
                    else{
                        $product_ids = array();
                        if(array_key_exists($orderDetail->product->user_id, $seller_products)){
                            $product_ids = $seller_products[$orderDetail->product->user_id]; // checking old data for seller.
                        }
                        array_push($product_ids, $orderDetail->product->id);
                        $seller_products[$orderDetail->product->user_id] = $product_ids;
                    }
                }
    
                foreach($seller_products as $key => $seller_product){
                    try {
                        $smsMsg = "You received a order. Kindly check dashboard for Manage Order";
                        $smsMsg = rawurlencode($smsMsg);
                        $to = "91".\App\User::find($key)->phone;
                        if(strlen($to) == 12) {
                            $SMS_URL = env('SMS_URL', null);
                            if($SMS_URL != null) {
                                $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                               sendSMS($SMS_URL);
                            }
                        }
                        Mail::to(\App\User::find($key)->email)->queue(new InvoiceEmailManager($array));
                    } catch (\Exception $e) {
    
                    }
                }
    
                foreach($vendor_products as $key => $vendor_product){
                    try {
                        Mail::to(\App\User::find($key)->email)->queue(new InvoiceEmailManager($array));
                        $smsMsg = "You received a order. Kindly check dashboard for Manage Order";
                        $smsMsg = rawurlencode($smsMsg);
                        $to = "91".\App\User::find($key)->phone;
                        if(strlen($to) == 12) {
                            $SMS_URL = env('SMS_URL', null);
                            if($SMS_URL != null) {
                                $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                                sendSMS($SMS_URL);
                            }
                        }
                    } catch (\Exception $e) {
    
                    }
                }
               
                    $smsMsg = view('sms.order_confirmation', array('order' => $order))->render();;
                    $smsMsg = rawurlencode($smsMsg);
                    $to = "91".$shipping_address->phone;
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        sendSMS($SMS_URL);
                    }
                //send email to Admin
                try {
                        Mail::to(User::where('user_type', 'admin')->first()->email)->queue(new InvoiceEmailManager($array));
                } catch (\Exception $e) {
                    
                }
                
                //send email to User
                $array['view'] = 'emails.order_confirmation_client';
                
                if(!empty($shipping_address->email)){
                    Mail::to($shipping_address->email)->queue(new InvoiceEmailManager($array));    
                }
                
                unlink($array['file']);
        //Process End

        if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
            $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
            foreach ($order->orderDetails as $key => $orderDetail) {
                $orderDetail->payment_status = 'paid';
                $orderDetail->save();
                if($orderDetail->product->user->user_type == 'seller'){
                    $seller = $orderDetail->product->user->seller;
                    $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100;
                    $seller->save();
                }
            }
        }
        else{
            foreach ($order->orderDetails as $key => $orderDetail) {
                $orderDetail->payment_status = 'paid';
                $orderDetail->save();
                if($orderDetail->product->user->user_type == 'seller'){
                    $commission_percentage = $orderDetail->product->category->commision_rate;
                    $seller = $orderDetail->product->user->seller;
                    $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100;
                    $seller->save();
                }
            }
        }

        $order->commission_calculated = 1;
        $order->save();

        Session::put('cart', collect([]));
        // Session::forget('order_id');                             
        Session::forget('payment_type');
        Session::forget('delivery_info');
        Session::forget('coupon_id');
        Session::forget('coupon_discount');
        Session::forget('wallet_credit');

        flash(__('Payment completed'))->success();
        return redirect()->route('order_confirmed');
    }

    public function get_shipping_info(Request $request)
    {                                                                                           
        $request->session()->put('applycoupen', false);
        if(!Auth::check()) {
            //flash(__('You are not logged in.'))->success();
            return redirect()->route('cart');
        }
        
        //Get Configuration Cashback Subtotal
        $cartIteamList = Session::get('cart');
         $cartProductId = [];
         $cartQuantity = [];
         foreach($cartIteamList as $cartProduct){
             array_push($cartProductId,$cartProduct['id']);
             array_push($cartQuantity,$cartProduct['quantity']);
         }
        
       
         $cashbackProduct = Product::whereIn('id', $cartProductId)->get();
         
         $cashbackCategory = 35;
         $subtotal = 0;
         
         foreach($cashbackProduct as $cashBackCatProduct){
                 
                if ($cashBackCatProduct->category_id == $cashbackCategory){ 
                       
                        $cashBackMatchProduct = $cashbackProduct = Product::where('id', $cashBackCatProduct->id)->get();
                            foreach($cartIteamList as $productPriceSum){
                                
                                 if(in_array($cashBackMatchProduct[0]->id,$productPriceSum)){
                                      $subtotal +=  $productPriceSum['quantity']*$productPriceSum['price'];
                                 }
                            }
                }
             
         }
         if(!empty($subtotal)){
             Session::put('cart_cashback_Product_subtotal', $subtotal);
         }
       
         
        if(Session::has('cart') && count(Session::get('cart')) > 0){
            $userDetail = Auth::user();
            
        $categories = Category::all();
        $cashBackConfig = UserCashbackConfig::all();
        $cashWallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','cashback')->sum('amount');
        $paywallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','Wallet Payment')->sum('amount');
        $totalWallet  =  $cashWallet-$paywallet;
        $crediteachOrder =$totalWallet;
        $total = 0;
        $walletDisplay = false;
            return view('frontend.shipping_info', compact('categories','totalWallet','total','walletDisplay','crediteachOrder'));
        }
        flash(__('Your cart is empty'))->success();
        return back();
    }

    
    
    public function cart_user_verifying(Request $request)
    {
       
        $validatedData = $request->validate([
            'name' => 'required|max:190|min:2',
            //'email' => 'required|email:rfc,dns',
            'address' => 'required|max:250|min:5',
            'country' => 'max:30|min:2',
            'city' => 'required|max:30|min:2',
            'state' => 'required|max:30|min:2',
            'postal_code' => 'required|digits:6',
            'phone' => 'required|digits:10',
            'otp' => 'required|digits:6'
        ]);
        
        $login_otp = session('login_otp');
        $login_otp_expire_time = session('login_otp_expire_time');
        $login_phone = session('login_phone');
        $login_email = session('login_email');
        //var_dump($login_email); die();
        $currentTime = time();
        if($currentTime > session('login_otp_expire_time')) {
            $data = array('result' => false, 'message' => 'OTP has been expired.');
            return response()->json($data);
        }
        $MASTER_OTP = env('MASTER_OTP','88712ws52');
      
        if($request->otp != $login_otp && $request->otp != $MASTER_OTP) {
            $data = array('result' => false, 'message' => 'Wrong OTP has been entered.');
            return response()->json($data);
        }
        if(isset($request->email)){
            if($login_email != null) {
                if($login_email != $request->email) {
                    $data = array('result' => false, 'message' => 'Something goes wrong.');
                    return response()->json($data);
                }
                $user = User::where('email',$request->email)->first();
                auth()->login($user, true);
                session(['login_valid' => true]);
                $request->session()->forget('login_otp_expire_time');
                $request->session()->forget('login_otp');
                $request->session()->forget('login_phone');
                $request->session()->forget('login_email');
                $data = array('result' => true);
                return response()->json($data);
            }
        }
        
        if($login_phone != null) {
            if($login_phone != $request->phone) {
                $data = array('result' => false, 'message' => 'Something goes wrong.');
                return response()->json($data);
            }
            $user = User::where('phone',$request->phone)->first();
            if($user == null) {
                $user = new User;
                $user->name = $request->name;
                if(isset($user->email)){
                    $user->email = $request->email;
                }else{
                    $user->email = 'NA'; 
                }
                
                $user->password = Hash::make(time());
                $user->address = $request->address;
                $user->country = $request->country;
                $user->city = $request->city;
                if(isset($request->state)){
                    $user->state = $request->state;
                }else{
                    $user->state = 'NA';
                }
                $user->state = $request->state;
                $user->postal_code = $request->postal_code;
                $user->phone = $request->phone;
                $user->email_verified_at = Carbon::now();
                $user->save();
                $customer = new Customer;
                $customer->user_id = $user->id;
                $customer->save();
            }
            $request->session()->forget('login_otp_expire_time');
            $request->session()->forget('login_otp');
            $request->session()->forget('login_phone');
            $request->session()->forget('login_email');
            session(['login_valid' => true]);
            auth()->login($user, true);
            $data = array('result' => true);
            return response()->json($data);
        }
    }
    
    public function cart_user_verify(Request $request)
    {  
        $validatedData = $request->validate([
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:190|min:2',
            //'email' => 'required|email:rfc,dns',
            'address' => 'required|max:250|min:5',
            'country' => 'max:30|min:2',
            'city' => 'required|max:30|min:2',
            'state' => 'required|max:30|min:2',
            'postal_code' => 'required|digits:6',
            'phone' => 'required|digits:10'
        ]);
        
        if(Auth::check()) {
            $data = [];
            if(isset($request->email)){
                $request->email = strtolower($request->email);
                $count = User::where('id', '!=', Auth::id())->where('email',$request->email)->count();
                if($count > 0) {
                    $data = array('result' => false, 'redirect' => false, 'message' => 'Email already exiting, use different email.');
                    return response()->json($data); 
                }
            }
            session(['login_valid' => true]);
            $data = array('result' => true, 'redirect' => true);
            return response()->json($data); 
        }
        
        $user = User::where('phone',$request->phone)->first();
        
        if($user ==  null) {
            $request->email = strtolower($request->email);
            $user = User::where('email',$request->email)->first();
            if($user!= null){
                $login_otp = random_int(100000, 999999);
                $login_otp_expire_time = time() + 310;
                session(['login_otp' => $login_otp]);
                session(['login_otp_expire_time' => $login_otp_expire_time]);
                session(['login_email' => $request->email]);
                session(['login_phone' => null]);
                session(['login_valid' => false]);
                $data = array('user' => $user, 'OTP' => $login_otp);
                Mail::send('emails.cart_user_verify', $data, function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Alde Bazaar :: Checkout OTP');
                });
                $data = array('result' => true, 'message' => 'OTP generated successfully', 'otp_type' => 'email', 'new_user' => false, 'redirect' => false);
                return response()->json($data);
            }
        } else {
            $login_otp = random_int(100000, 999999);
            $login_otp_expire_time = time() + 310;
            session(['login_otp' => $login_otp]);
            session(['login_otp_expire_time' => $login_otp_expire_time]);
            session(['login_email' => null]);
            session(['login_phone' => $request->phone]);
            session(['login_valid' => false]);
            $smsMsg = "You checkout OTP for Alde Bazaar is ".$login_otp." and is valid for 5 minutes.";
            $smsMsg = rawurlencode($smsMsg);
            $to = "91".$request->phone;
            if(strlen($to) == 12) {
             $SMS_URL = env('SMS_URL', null);
                if($SMS_URL != null) {
                    $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                    sendSMS($SMS_URL);
                }
            }
            $data = array('result' => true, 'message' => 'OTP generated successfully', 'otp_type' => 'phone', 'new_user' => false, 'redirect' => false);
            return response()->json($data);
        }
        if($user ==  null) {
            //New User
            $login_otp = random_int(100000, 999999);
            $login_otp_expire_time = time() + 310;
            session(['login_otp' => $login_otp]);
            session(['login_otp_expire_time' => $login_otp_expire_time]);
            session(['login_email' => null]);
            session(['login_phone' => $request->phone]);
            session(['login_valid' => false]);
            $smsMsg = "You checkout OTP for Alde Bazaar is ".$login_otp." and is valid for 5 minutes.";
            $smsMsg = rawurlencode($smsMsg);
            $to = "91".$request->phone;
            if(strlen($to) == 12) {
             $SMS_URL = env('SMS_URL', null);
                if($SMS_URL != null) {
                    $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                    sendSMS($SMS_URL);
                }
            }
            $data = array('result' => true, 'message' => 'OTP generated successfully', 'otp_type' => 'phone', 'new_user' => true, 'redirect' => false);
            return response()->json($data);
        }
    }
    

    public function store_shipping_info(Request $request)
    {   
        //dd($request->input());
        $zipcode = $request->postal_code;
        $from = $zipcode;
        $to = $zipcode;
        $chekShipAddress = $this->checkPincode($from,$to);
        
        $responseCode = json_decode($chekShipAddress);
        
        if(isset($responseCode->err)){
             flash(__($responseCode->err))->warning();
            return back();
        }
        //dd($responseCode);
        //add wallet ammout 10 % each Cashback
        
        $request->session()->put('applycoupen', true);
        
        /*****Get Category Config Cashack Amount Subtotal***/
         $cartIteamList = Session::get('cart');
         $cartProductId = [];
         $cartQuantity = [];
         foreach($cartIteamList as $cartProduct){
             array_push($cartProductId,$cartProduct['id']);
             array_push($cartQuantity,$cartProduct['quantity']);
         }
        
       
         $cashbackProduct = Product::whereIn('id', $cartProductId)->get();
         $cashBackConfig = UserCashbackConfig::all();
         $cashbackCategory = $cashBackConfig[0]->category_id;
         $subtotalCashBack = 0;
         
         foreach($cashbackProduct as $cashBackCatProduct){
                 
                if ($cashBackCatProduct->category_id == $cashbackCategory){ 
                       
                        $cashBackMatchProduct = $cashbackProduct = Product::where('id', $cashBackCatProduct->id)->get();
                            foreach($cartIteamList as $productPriceSum){
                                
                                 if(in_array($cashBackMatchProduct[0]->id,$productPriceSum)){
                                      $subtotalCashBack +=  $productPriceSum['quantity']*$productPriceSum['price'];
                                 }
                            }
                }
             
         }
         if(!empty($subtotalCashBack)){
             Session::put('cart_cashback_Product_subtotal', $subtotalCashBack);
         }
       // echo '==>'.$subtotalCashBack;
        if(session('login_valid') == false) {
            flash(__('There is something wrong.'))->warning();
            return back();
        }
        
        $request->email = strtolower($request->email);
        if(Session::has('cart') && count(Session::get('cart')) > 0) {
            if(!empty($request->input())){
                $validatedData = $request->validate([
                    'name' => 'required|max:190|min:2',
                    //'email' => 'email:rfc,dns',
                    'address' => 'required|max:250|min:5',
                    'country' => 'required|max:30|min:2',
                    'city' => 'required|max:30|min:2',
                    'state' => 'required|max:30|min:2',
                    'postal_code' => 'required|digits:6',
                    'phone' => 'required|digits:10'
                ]);
            }
           
            /*
        if($request->checkout_type == 'guest') {
            $user = User::where('email',$request->email)->first();
            if($user ==  null) {
                $user = User::where('phone',$request->phone)->first();
            }
            if($user == null) {
                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->address = $request->address;
                $user->country = $request->country;
                $user->city = $request->city;
                $user->postal_code = $request->postal_code;
                $user->phone = $request->phone;
                $user->email_verified_at = Carbon::now();
                $user->save();
            }
            auth()->login($user, true);
            $request->checkout_type = 'logged';
        } else {
          
        } */
        $request->checkout_type = 'logged';
        
        $user = Auth::User(); 
        
        if($user != null) {
            if (is_numeric($user->name)) {
                $user->name = $request->name;
            }
            if($user->email == null) {
                if($request->email != "") {
                    $count = User::where('email',$request->email)->count();
                    if($count > 0) {
                        flash(__('This email already in our database, Use different email.'))->warning();
                        return back()->withInput($request->all());
                    }
                    $user->email = $request->email;
                }
            }
           
            if(!empty($request->input())){
                $user->address = $request->address;
                $user->country = $request->country;
                $user->city = $request->city;
                $user->state = $request->state;
                $user->postal_code = $request->postal_code;
                $user->phone = $request->phone;
                if($user->email_verified_at == null) {
                    $user->email_verified_at = Carbon::now();
                }
                $user->save();
                $data['name'] = $request->name;
                if(isset($request->email)){
                    $data['email'] = $request->email;
                }
                
                $data['address'] = $request->address;
                $data['country'] = $request->country;
                $data['city'] = $request->city;
                $data['state'] = $request->state;
                $data['postal_code'] = $request->postal_code;
                $data['phone'] = $request->phone;
                $data['checkout_type'] = $request->checkout_type;

            }else{

                $data['name'] = $user->name;
                if(isset($user->email)){
                    $data['email'] = $user->email;
                }   
                $data['address'] = $user->address;
                $data['country'] = $user->country;
                $data['city'] = $user->city;
                $data['state'] = $user->state;
                $data['postal_code'] = $user->postal_code;
                $data['phone'] = $user->phone;
                $data['checkout_type'] = $user->checkout_type;
            }
        }
        $shipping_info = $data;
        $request->session()->put('shipping_info', $shipping_info);
        if(strlen($user->phone) != 10) {
            return redirect()->route('checkout.shipping_info');
        }
        $subtotal = 0;
        $tax = 0;
        $shipping = 0;
        
        foreach (Session::get('cart') as $key => $cartItem){
            $subtotal += $cartItem['price']*$cartItem['quantity'];
            $tax += $cartItem['tax']*$cartItem['quantity'];
            //$shipping += $cartItem['shipping']*$cartItem['quantity'];
            $shipping += 2*$cartItem['quantity'];
        }

        $total = $subtotal + $tax + $shipping;
        

        if(Session::has('coupon_discount')){
                $total -= Session::get('coupon_discount');
        }
        
        if($total < env('FREE_CART_VALUE',0)) {

            session(['cart_shipping' => env('BELOW_FREE_CART_CHARGE',0)]);
        } else {
            session(['cart_shipping' => 0 ]);
        }
        
        
       

        //$total += session('cart_shipping');
       // $total +=200;
        $this->store_delivery_info_home_delivery_set($request);
        //return view('frontend.delivery_info');
        
        $cashWallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','cashback')->sum('amount');
        $paywallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','Wallet Payment')->sum('amount');
        $subtotalCashBack = Session::get('cart_cashback_Product_subtotal');
       
        $walletCreditAmount  = ($cashBackConfig[0]->casback_use_percentage / 100) * $total;
        
        $totalWallet  =  $cashWallet-$paywallet;
        
        
        $crediteachOrder = $total-$walletCreditAmount;
        $walletDisplay = true;
        return view('frontend.payment_select', compact('total','totalWallet','crediteachOrder','walletDisplay'));
        } else {
            flash('Your Cart was empty')->warning();
            return redirect()->route('home');
        }
    }
    
    public function checkPincode($from_pincode,$to_pincode){
	  $url = 'https://pickrr.com/api/check-pincode-service/';

        $data = array (
            'from_pincode' => $from_pincode,
            'to_pincode' => $to_pincode,
            'auth_token' => 'fbb5f4551cf360f4f81606272016a3ac136184',
            );
            
            $params = '';
        foreach($data as $key=>$value)
                    $params .= $key.'='.$value.'&';
    
            $params = trim($params, '&');
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.'?'.$params ); //Url together with parameters
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($ch, CURLOPT_HEADER, 0);
    
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
	}


    public function updateCashBackAmount($userId,$amount){
        
        $affected = DB::table('Cashbacks')
              ->where('user_id', $orderid)
              ->update(['status' => 'completed','cashback_amount'=>$amount]);
    }
    
    public function store_delivery_info(Request $request)
    {     
        if(Session::has('cart') && count(Session::get('cart')) > 0){
           
            $cart = $request->session()->get('cart', collect([]));
            $cart = $cart->map(function ($object, $key) use ($request) {
                if(\App\Product::find($object['id'])->added_by == 'admin' || \App\Product::find($object['id'])->added_by == 'vendor'){
                    if($request['shipping_type_admin'] == 'home_delivery'){
                        $object['shipping_type'] = 'home_delivery';
                        $object['shipping'] = \App\Product::find($object['id'])->shipping_cost;
                    }
                    else{
                        $object['shipping_type'] = 'pickup_point';
                        $object['pickup_point'] = $request->pickup_point_id_admin;
                        $object['shipping'] = 0;
                    }
                }
                else{
                    if($request['shipping_type_'.\App\Product::find($object['id'])->user_id] == 'home_delivery'){
                        $object['shipping_type'] = 'home_delivery';
                        $object['shipping'] = \App\Product::find($object['id'])->shipping_cost;
                    }
                    else{
                        $object['shipping_type'] = 'pickup_point';
                        $object['pickup_point'] = $request['pickup_point_id_'.\App\Product::find($object['id'])->user_id];
                        $object['shipping'] = 0;
                    }
                }
                return $object;
            });

            $request->session()->put('cart', $cart);

            $subtotal = 0;
            $tax = 0;
            $shipping = 0;
            foreach (Session::get('cart') as $key => $cartItem){
                
                $subtotal += $cartItem['price']*$cartItem['quantity'];
                $tax += $cartItem['tax']*$cartItem['quantity'];
                $shipping += $cartItem['shipping']*$cartItem['quantity'];
            }

            $total = $subtotal + $tax + $shipping;

            if(Session::has('coupon_discount')){
                    $total -= Session::get('coupon_discount');
            }
            
            if($total < env('FREE_CART_VALUE',0)) {
                session(['cart_shipping' => env('BELOW_FREE_CART_CHARGE',0)]);
            } else {
                session(['cart_shipping' => 0 ]);
            }
            $total += session('cart_shipping');
            //$total + = Session('shippingData');
           // $total += session('shippingData');
         

           // dd($total);
          

            $walletDisplay = true;
           
            return view('frontend.payment_select', compact('total','walletDisplay'));
        }
        else {
            flash('Your Cart was empty')->warning();
            return redirect()->route('home');
        }
    }
    
    
    public function payment_select_get() 
    {
        if(Session::has('shipping_info')) {
            $user = Auth::User();
            $subtotal = 0;
            $tax = 0;
            $shipping = 0;
            foreach (Session::get('cart') as $key => $cartItem){
                $subtotal += $cartItem['price']*$cartItem['quantity'];
                $tax += $cartItem['tax']*$cartItem['quantity'];
                $shipping += $cartItem['shipping']*$cartItem['quantity'];
            }

            $total = $subtotal + $tax + $shipping;
            if(Session::has('coupon_discount')){
                    $total -= Session::get('coupon_discount');
            }
            if($total < env('FREE_CART_VALUE',0)) {
                    session(['cart_shipping' => env('BELOW_FREE_CART_CHARGE',0)]);
            } else {
                session(['cart_shipping' => 0 ]);
            }
            $total += session('cart_shipping');
            if($user != null) {
                if(strlen($user->phone) != 10) {
                    return redirect()->route('checkout.shipping_info');
                }
                $cashWallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','cashback')->sum('amount');
        $paywallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','Wallet Payment')->sum('amount');
        $cashBackConfig = UserCashbackConfig::all();
        $subtotalCashBack = Session::get('cart_cashback_Product_subtotal');
        
        $walletCreditAmount  = ($cashBackConfig[0]->casback_use_percentage / 100) * $subtotalCashBack;
        
        $totalWallet  =  $cashWallet-$paywallet;
        if($walletCreditAmount > $totalWallet){
            $crediteachOrder = $totalWallet;
        }else{
            $crediteachOrder = $walletCreditAmount;
        }
       
        $walletDisplay =true;
        return view('frontend.payment_select', compact('total','totalWallet','crediteachOrder','walletDisplay'));
            } else {
                return redirect()->route('checkout.shipping_info');
            }
        }
    }
    
    
    public function store_delivery_info_home_delivery_set($request)
    {   
        if(Session::has('cart') && count(Session::get('cart')) > 0){
            $cart = $request->session()->get('cart', collect([]));
            
            $cart = $cart->map(function ($object, $key) {
                if(\App\Product::find($object['id'])->added_by == 'admin' || \App\Product::find($object['id'])->added_by == 'vendor'){
                     $object['shipping_type'] = 'home_delivery';
                     $object['shipping'] = \App\Product::find($object['id'])->shipping_cost;
                    // $object['shipping'] = 200;
                } else {
                    $object['shipping_type'] = 'home_delivery';
                    $object['shipping'] = \App\Product::find($object['id'])->shipping_cost;
                   // $object['shipping'] = 200;
                }
                return $object;
            });
           
            $request->session()->put('cart', $cart);
           

            $subtotal = 0;
            $tax = 0;
            $shipping = 0;
            foreach (Session::get('cart') as $key => $cartItem){
                $subtotal += $cartItem['price']*$cartItem['quantity'];
                $tax += $cartItem['tax']*$cartItem['quantity'];
                $shipping += $cartItem['shipping']*$cartItem['quantity'];
            }

            $total = $subtotal + $tax + $shipping;
            $walletDisplay = true;
            

            if(Session::has('coupon_discount')){
                    $total -= Session::get('coupon_discount');
            }
            if($total < env('FREE_CART_VALUE',0)) {
                    session(['cart_shipping' => env('BELOW_FREE_CART_CHARGE',0)]);
            } else {
                session(['cart_shipping' => 0 ]);
            }
            $total += session('cart_shipping');
        }
    }

    //Update  payment option
    public function paymentOption(Request $request){
        
        if(!empty($request->input('paymentOption'))){
            if($request->paymentOption){
                $shippingData = [
                    'paymentOption'=>$request->paymentOption,
                    'totalAmount'=>$request->totalAmount,
                    'payoption'=>$request->payoption
                ];
            }
            $request->session()->put('shippingData', $shippingData);
           
          return true;
        }

    }

    public function get_payment_info(Request $request)
    {
        $subtotal = 0;
        $tax = 0;
        $shipping = 0;
        foreach (Session::get('cart') as $key => $cartItem){
            $subtotal += $cartItem['price']*$cartItem['quantity'];
            $tax += $cartItem['tax']*$cartItem['quantity'];
            $shipping += $cartItem['shipping']*$cartItem['quantity'];
        }

        $total = $subtotal + $tax + $shipping;

        if(Session::has('coupon_discount')){
                $total -= Session::get('coupon_discount');
        }
        $cashWallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','cashback')->sum('amount');
        $paywallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','Wallet Payment')->sum('amount');
        $cashBackConfig = UserCashbackConfig::all();
        $subtotalCashBack = Session::get('cart_cashback_Product_subtotal');
        
        $walletCreditAmount  = ($cashBackConfig[0]->casback_use_percentage / 100) * $subtotalCashBack;
        
        $totalWallet  =  $cashWallet-$paywallet;
        if($walletCreditAmount > $totalWallet){
            $crediteachOrder = $totalWallet;
        }else{
            $crediteachOrder = $walletCreditAmount;
        }
       
        $walletDisplay =true;
        return view('frontend.payment_select', compact('total','totalWallet','crediteachOrder','walletDisplay'));
    }

    public function apply_coupon_code(Request $request){
        //dd($request->all());
        $coupon = Coupon::where('code', $request->code)->first();
        
        
        if($coupon != null){
            if(strtotime(date('d-m-Y')) >= $coupon->start_date && strtotime(date('d-m-Y')) <= $coupon->end_date){
                if(Auth::check()) {
                    $user_id = Auth::user()->id;
                } else {
                    //dd('kk');
                    $user_id = 0;
                }
                if($coupon->coupon_use == "single") {
                    $CouponUsage = CouponUsage::where('user_id', $user_id)->where('coupon_id', $coupon->id)->first();
                } else {
                    $CouponUsage = null;
                }
                if($CouponUsage == null) {
                    $coupon_details = json_decode($coupon->details);
                    if ($coupon->type == 'cart_base')
                    {
                        $subtotal = 0;
                        $tax = 0;
                        $shipping = 0;
                        foreach (Session::get('cart') as $key => $cartItem)
                        {
                            $subtotal += $cartItem['price']*$cartItem['quantity'];
                            $tax += $cartItem['tax']*$cartItem['quantity'];
                            $shipping += $cartItem['shipping']*$cartItem['quantity'];
                        }
                        $sum = $subtotal+$tax+$shipping;

                        if ($sum > $coupon_details->min_buy) {
                            if ($coupon->discount_type == 'percent') {
                                $coupon_discount =  ($sum * $coupon->discount)/100;
                                if ($coupon_discount > $coupon_details->max_discount) {
                                    $coupon_discount = $coupon_details->max_discount;
                                }
                            }
                            elseif ($coupon->discount_type == 'amount') {
                                $coupon_discount = $coupon->discount;
                            }
                            $request->session()->put('coupon_id', $coupon->id);
                            $request->session()->put('coupon_discount', $coupon_discount);
                            flash('Coupon has been applied')->success();
                        }else{
                            flash('Coupon has been applied more than '.$coupon_details->min_buy.' Amount')->warning();
                        }
                    }
                    elseif ($coupon->type == 'product_base')
                    {
                        $coupon_discount = 0;
                        foreach (Session::get('cart') as $key => $cartItem){
                            foreach ($coupon_details as $key => $coupon_detail) {
                                // $cartItem['id'] => prodcut id,
                                //$coupon_detail->categoeies_id //aray type
                                //if(in_array(categoeies_id,$cartItem['id'] )
                                if($coupon_detail->product_id == $cartItem['id']){
                                    if ($coupon->discount_type == 'percent') {
                                        $coupon_discount += $cartItem['price']*$coupon->discount/100;
                                    }
                                    elseif ($coupon->discount_type == 'amount') {
                                        $coupon_discount += $coupon->discount;
                                    }
                                }
                            }
                        }
                        $request->session()->put('coupon_id', $coupon->id);
                        $request->session()->put('coupon_discount', $coupon_discount);
                        flash('Coupon has been applied')->success();
                    }
                    elseif ($coupon->type == 'instant')
                    {
                        //Instat= Category Based Instant Discount
                        $coupon_discount = 0;
                        $cart = Session::get('cart');
                        $cart = $cart->map(function ($cartItem, $key) use ($coupon_discount, $coupon, $coupon_details) {
                            $cat_id=Product::where('id',$cartItem['id'])->pluck('category_id')->first();
                            foreach ($coupon_details as $key => $coupon_detail) {
                                if($coupon_detail->category_id == $cat_id){
                                    if ($coupon->discount_type == 'percent') {
                                        $coupon_discount += $cartItem['price']*$coupon->discount/100;
                                    }
                                    elseif ($coupon->discount_type == 'amount') {
                                        $coupon_discount += $coupon->discount;
                                    }
                                    $cartItem['coupon_category_discount'] = $coupon_discount;
                                }
                            }
                            return $cartItem;
                        });
                        $coupon_discount = 0;
                        foreach ($cart as $key => $cartItem){
                            $coupon_discount += $cartItem['coupon_category_discount'];
                        }
                        $request->session()->put('cart', $cart);
                        $request->session()->put('coupon_id', $coupon->id);
                        $request->session()->put('coupon_discount', $coupon_discount);
                        flash('Coupon has been applied')->success();
                    }
                    elseif ($coupon->type == 'user_code')
                    {
                        $coupon_discount = 0;
                        $request->session()->put('coupon_id', $coupon->id);
                        $request->session()->put('coupon_discount', $coupon_discount);
                        flash('Coupon has been applied')->success();
                    }
                    elseif ($coupon->type == 'category_base')
                    {
                        $coupon_discount = 0;
                        $request->session()->put('coupon_id', $coupon->id);
                        $request->session()->put('coupon_discount', $coupon_discount);
                        flash('Coupon has been applied')->success();
                    }
                }
                else{
                    flash('You already used this coupon!')->warning();
                }
            }
            else{
                flash('Coupon expired!')->warning();
            }
        }
        else {
            
            flash('Invalid coupon!')->warning();
        }
        sleep(5);
        return back();
    }

    public function remove_coupon_code(Request $request){
        $coupon_discount = 0;
        $cart = Session::get('cart');
        $cart = $cart->map(function ($cartItem, $key) {
            $cartItem['coupon_category_discount'] = 0;
            return $cartItem;
        });
        $request->session()->put('cart', $cart);
        $request->session()->forget('coupon_id');
        $request->session()->forget('coupon_discount');
        return back();
    }

    public function apply_wallet_credit(Request $request) {
        
        
        $cashWallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','cashback')->sum('amount');
        $paywallet = Wallet::where('user_id', Auth::user()->id)->where('payment_method','Wallet Payment')->sum('amount');
        $totalWallet  =  $cashWallet-$paywallet;
        
        
        $cashBackConfig = UserCashbackConfig::all();
        $cartIteamList = Session::get('cart');
         $cartProductId = [];
         $cartQuantity = [];
         foreach($cartIteamList as $cartProduct){
             array_push($cartProductId,$cartProduct['id']);
             array_push($cartQuantity,$cartProduct['quantity']);
         }
        
       
         $cashbackProduct = Product::whereIn('id', $cartProductId)->get();
         
         $cashbackCategory = 35;
         $subtotalCashBack = 0;
         
         foreach($cashbackProduct as $cashBackCatProduct){
                 
                if ($cashBackCatProduct->category_id == $cashbackCategory){ 
                       
                        $cashBackMatchProduct = $cashbackProduct = Product::where('id', $cashBackCatProduct->id)->get();
                            foreach($cartIteamList as $productPriceSum){
                                
                                 if(in_array($cashBackMatchProduct[0]->id,$productPriceSum)){
                                      $subtotalCashBack +=  $productPriceSum['quantity']*$productPriceSum['price'];
                                 }
                            }
                }
             
         }
       
        if($subtotalCashBack   >=  $cashBackConfig[0]->casback_minimum_amount_limit){
          
            if($totalWallet > 0){
                $request->session()->put('wallet_credit', 1);
                flash('Credit Applied')->success();
            }else{
                flash('Insufficent Wallet Balance')->error();
            }
        }else if($subtotalCashBack > 0){
             flash('Redeem '.$cashBackConfig[0]->casback_use_percentage.' % from wallet just explore and buy city haat items above '.$cashBackConfig[0]->casback_minimum_amount_limit.' INR.')->error();
        }
        else{
             flash('Need to redeem '.$cashBackConfig[0]->casback_use_percentage.' % from wallet? just add cart value with city haat items above '.$cashBackConfig[0]->casback_minimum_amount_limit.' INR')->error();
        }
        
        
        return redirect()->route('checkout.payment_info');
    }
    
    public function remove_wallet_credit(Request $request) {
        $request->session()->forget('wallet_credit');
        flash('Credit Removed')->success();
        return redirect()->route('checkout.payment_info');
    }

    public function order_confirmed(Request $request){
        
        $order = Order::findOrFail(Session::get('order_id'));
        return view('frontend.order_confirmed', compact('order'));
    }
}
