<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\BusinessSetting;
use App\Seller;
use Session;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\WalletController;
use PaytmWallet;
use Auth;
use Redirect;
use App\OrderDetail;

class PaytmController extends Controller
{
    public function index(){
        if(Session::has('payment_type')){
            if(Session::get('payment_type') == 'cart_payment'){
                $order = Order::findOrFail(Session::get('order_id'));
                $amount = $order->grand_total;

                $payment = PaytmWallet::with('receive');
                $UserData = array();
                $UserData['order'] = "OrderCode@".$order->code;
                $UserData['user'] = Auth::user()->id;
                $UserData['mobile_number'] = Session::get('shipping_info')['phone'];
                if(Session::get('shipping_info')['email'] != "") {
                $UserData['email'] = Session::get('shipping_info')['email'];
                } else {
                    $UserData['email'] = " ";
                }
                $UserData['amount'] = $amount;
                $UserData['callback_url'] = route('paytm.callback');
                $payment->prepare($UserData);
                return $payment->receive();
            }
            elseif (Session::get('payment_type') == 'wallet_payment') {
                if(Auth::user()->phone != null){
                    $amount= Session::get('payment_data')['amount'];
                    $payment = PaytmWallet::with('receive');
                    
                    $UserData = array();
                    $UserData['order'] = "WalletRecharge@".rand(10000,99999);
                    $UserData['user'] = Auth::user()->id;
                    $UserData['mobile_number'] = Auth::user()->phone;
                    if(Auth::user()->email != "") {
                    $UserData['email'] = Auth::user()->email;
                    }
                    $UserData['amount'] = $amount;
                    $UserData['callback_url'] = route('paytm.callback');
                    
                    $payment->prepare($UserData);
                    return $payment->receive();
                }
                else {
                    flash('Please add phone number to your profile')->warning();
                    return back();
                }
            }
        }
    }

    public function callback2(Request $request){
        $transaction = PaytmWallet::with('receive');
        $response = $transaction->response();
       // dd(Session::all());
        $Order =  $response['ORDERID'];
            if(!Auth::check()) {
                $order = Order::where('id', $Order)->first();
                Auth::loginUsingId($order->user_id);
                $OrderDetail = OrderDetail::where('order_id', $order->id)->get();
                $cart = array();
                foreach($OrderDetail as $OD) {
                    $a = array("id" => $OD->product_id, 
                            "variant" => $OD->variant,
                            "quantity" => $OD->quantity,
                            "price" => $OD->price,
                            "discount" => $OD->discount,
                            "coupon_category_discount" => 0,
                            "tax" => $OD->tax,
                            "shipping" =>$OD->shipping,
                            "product_referral_code" => $OD->product_referral_code,
                            "digital" => 0,
                            "shipping_type" => "home_delivery"
                            );
                        array_push($cart, $a);
                }
                $cart = collect($cart);
                session(['cart' => $cart ]);
            }
        dd(Session::all());
        $data = array('data' => $request->all());
        return view('paytm.response', $data);
    }

    public function callback(Request $request) {
        $transaction = PaytmWallet::with('receive');
        $response = $transaction->response(); // To get raw response as array
        //dd($request->session());
        //dd("test");
        $Order = explode("@", $response['ORDERID']);
        if($Order[0] == 'OrderCode') {
            if(!Auth::check()) {
                $order = Order::where('code', $Order[1])->first();
                Auth::loginUsingId($order->user_id);
                $OrderDetail = OrderDetail::where('order_id', $order->id)->get();
                $cart = array();
                foreach($OrderDetail as $OD) {
                    $a = array("id" => $OD->product_id, 
                            "variant" => $OD->variant,
                            "quantity" => $OD->quantity,
                            "price" => $OD->price,
                            "discount" => $OD->discount,
                            "coupon_category_discount" => 0,
                            "tax" => $OD->tax,
                            "shipping" =>$OD->shipping,
                            "product_referral_code" => $OD->product_referral_code,
                            "digital" => 0,
                            "shipping_type" => "home_delivery"
                            );
                        array_push($cart, $a);
                }
                $cart = collect($cart);
                session(['cart' => $cart, 'order_id' => $order->id ]);
            }
        }
        
        if($transaction->isSuccessful()){
            if($Order[0] == 'OrderCode') {
                    $checkoutController = new CheckoutController;
                    return $checkoutController->checkout_done(Session::get('order_id'), json_encode($response));
            }
            elseif ($Order[0] == 'WalletRecharge') {
                $walletController = new WalletController;
                return $walletController->wallet_payment_done(Session::get('payment_data'), json_encode($response));
            }
        } else {
            $request->session()->forget('order_id');
            $request->session()->forget('payment_data');
            if($Order[0] == 'OrderCode') {
                flash('We are getting error while processing your payment.')->error();
            	return redirect()->route('checkout.payment_info');
            }
            elseif ($Order[0] == 'WalletRecharge') {
                flash('We are getting error while processing your payment.')->error();
            	return redirect()->route('wallet.index');
            }
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function credentials_index()
    {
        return view('paytm.index');
    }

    /**
     * Update the specified resource in .env
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_credentials(Request $request)
    {
        foreach ($request->types as $key => $type) {
                $this->overWriteEnvFile($type, $request[$type]);
        }

        flash("Settings updated successfully")->success();
        return back();
    }

    /**
    *.env file overwrite
    */
    public function overWriteEnvFile($type, $val)
    {
        $path = base_path('.env');
        if (file_exists($path)) {
            $val = '"'.trim($val).'"';
            if(is_numeric(strpos(file_get_contents($path), $type)) && strpos(file_get_contents($path), $type) >= 0){
                file_put_contents($path, str_replace(
                    $type.'="'.env($type).'"', $type.'='.$val, file_get_contents($path)
                ));
            }
            else{
                file_put_contents($path, file_get_contents($path)."\r\n".$type.'='.$val);
            }
        }
    }
}
