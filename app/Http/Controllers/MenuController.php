<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Illuminate\Support\Facades\Cache;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Menu = Menu::all();
        return view('menu.index', compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$Menu = new Menu;
		$Menu->link = $request->url;
		$Menu->name = $request->menu_name;
		$Menu->position = $request->menu_position;
		$Menu->save();
		flash(__('Menu has been Created successfully'))->success();
		Cache::forget('menu');
        return redirect()->route('home_settings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::findOrFail($id);
        return view('menu.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
		$Menu = Menu::find($id);
        $Menu->link = $request->url;
		$Menu->name = $request->menu_name;
		$Menu->position = $request->menu_position;
        $Menu->save();
		Cache::forget('menu');
        flash(__('Menu has been updated successfully'))->success();
        return redirect()->route('home_settings.index');
		
    }
	public function update_status(Request $request)
    {
		$Menu = Menu::find($request->id);
        $Menu->published = $request->status;
		if($Menu->save()){
			Cache::forget('menu');
			return '1';
		}
		else {
			return '0';
		}
		return '0';
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Menu = Menu::findOrFail($id);
        if(Menu::destroy($id)){
            flash(__('Menu has been deleted successfully'))->success();
			Cache::forget('menu');
        }
        else{
            flash(__('Something went wrong'))->error();
        }
        return redirect()->route('home_settings.index');
    }
}
