<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use ImageOptimizer;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
use Illuminate\Support\Facades\Cache;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('sliders.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$desktopSlider="";
		$mobileSlider="";
		if($request->hasFile('photo')){
			$extension = $request->photo->extension();
			$main = Str::slug("MainSlider","-").time().".".$extension;
			$img = Image::make($request->photo);
			$width = Image::make($request->photo)->width();
			$height = Image::make($request->photo)->height();
			
			if($width != 1341 && $height != 280){
				$img->resizeCanvas(1341, 280, 'center', false, 'fff');
			}
						
			$img->save(base_path('public/')."temp/".$main);
			ImageOptimizer::optimize(base_path('public/')."temp/".$main);
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$main, base_path('public/').'uploads/sliders/'.$main);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/sliders', new \Illuminate\Http\File(base_path('public/').'temp/'.$main), $main);
			}
			$desktopSlider = "uploads/sliders/".$main;
			unlink(base_path('public/').'temp/'.$main);
		}
		if($request->hasFile('mobile_photo')){
			//Mobile Image
			$extension = $request->mobile_photo->extension();
			$mobile = Str::slug("MobileSlider","-").time().".".$extension;
			$img = Image::make($request->mobile_photo);

			$width1 = Image::make($request->mobile_photo)->width();
			$height1 = Image::make($request->mobile_photo)->height();
			
			if($width1 != 1080 && $height1 != 400){
				$img->resizeCanvas(1080, 400, 'center', false, 'fff');
			}
			
			$img->save(base_path('public/')."temp/".$mobile);
			ImageOptimizer::optimize(base_path('public/')."temp/".$mobile);
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$mobile, base_path('public/').'uploads/sliders/mobile/'.$mobile);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/sliders/mobile', new \Illuminate\Http\File(base_path('public/').'temp/'.$mobile), $mobile);
			}
			$mobileSlider = "uploads/sliders/mobile/".$mobile;
			unlink(base_path('public/').'temp/'.$mobile);
		}
		$slider = new Slider;
		$url=$request->url;
		$host=parse_url($url,PHP_URL_HOST);
		if($host==request()->getHost())
		{
			$link=str_replace(url('/'),"",$url);
		}
		else{ $link=$request->url; }
		$slider->link = $link;
		$slider->photo=$desktopSlider;
		$slider->mobile_photo=$mobileSlider;
		$slider->save();

		Cache::forget('Sliders');
		flash(__('Slider has been inserted successfully'))->success();
	
        return redirect()->route('home_settings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Slider = Slider::findOrFail($id);
        return view('sliders.edit', compact('Slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id){
		$url=$request->url;
		$host=parse_url($url,PHP_URL_HOST);
		if($host==request()->getHost())
		{
			$link=str_replace(url('/'),"",$url);
		}
		else{ $link=$request->url; }
		$slider = Slider::find($id);
		
		if($request->hasFile('photo')){
			$extension = $request->photo->extension();
			$main = Str::slug("MainSlider","-").time().".".$extension;
			$img = Image::make($request->photo);
			$width = Image::make($request->photo)->width();
			$height = Image::make($request->photo)->height();
			
			if($width != 1341 && $height != 280){
				$img->resizeCanvas(1341, 280, 'center', false, 'fff');
			}
						
			$img->save(base_path('public/')."temp/".$main);
			ImageOptimizer::optimize(base_path('public/')."temp/".$main);
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$main, base_path('public/').'uploads/sliders/'.$main);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/sliders', new \Illuminate\Http\File(base_path('public/').'temp/'.$main), $main);
			}
			$olddesktopslider=$slider->photo;
			if(!empty($olddesktopslider))
			{
				if(File::exists($olddesktopslider)) {
				   unlink($olddesktopslider);
				}
			}
			$desktopSlider = "uploads/sliders/".$main;
			unlink(base_path('public/').'temp/'.$main);
			$slider->photo=$desktopSlider;
		}
		if($request->hasFile('mobile_photo')){
			//Mobile Image
			$extension = $request->mobile_photo->extension();
			$mobile = Str::slug("MobileSlider","-").time().".".$extension;
			$img = Image::make($request->mobile_photo);

			$width1 = Image::make($request->mobile_photo)->width();
			$height1 = Image::make($request->mobile_photo)->height();
			
			if($width1 != 1080 && $height1 != 400){
				$img->resizeCanvas(1080, 400, 'center', false, 'fff');
			}
			
			$img->save(base_path('public/')."temp/".$mobile);
			ImageOptimizer::optimize(base_path('public/')."temp/".$mobile);
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$mobile, base_path('public/').'uploads/sliders/mobile/'.$mobile);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/sliders/mobile', new \Illuminate\Http\File(base_path('public/').'temp/'.$mobile), $mobile);
			}
			$oldmobileSlider=$slider->mobile_photo;
			if(!empty($oldmobileSlider))
			{
				if(File::exists($oldmobileSlider)) {
				   unlink($oldmobileSlider);
				}
			}
			$mobileSlider = "uploads/sliders/mobile/".$mobile;
			unlink(base_path('public/').'temp/'.$mobile);
			$slider->mobile_photo=$mobileSlider;
		}
		$slider->link = $link;
		$slider->save();
		Cache::forget('Sliders');
		flash(__('Slider has been Updated successfully'))->success();
	
        return redirect()->route('home_settings.index');
	}
	public function update_status(Request $request)
    {
        $slider = Slider::find($request->id);
        $slider->published = $request->status;
        if($slider->save()){
			Cache::forget('Sliders');
            return '1';
        }
        else {
            return '0';
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$slider = Slider::findOrFail($id);
		if(slider::destroy($id)){
			if($slider->photo!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
				   if(file_exists(base_path('public/').$slider->photo))
				      unlink(base_path('public/').$slider->photo);
				} else {
				   Storage::disk(env('FILESYSTEM_DRIVER'))->delete($slider->photo);
				}
				$slider->photo = null;
			}
			if($slider->mobile_photo!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
				   if(file_exists(base_path('public/').$slider->mobile_photo))
				      unlink(base_path('public/').$slider->mobile_photo);
				} else {
				   Storage::disk(env('FILESYSTEM_DRIVER'))->delete($slider->mobile_photo);
				}
				$slider->mobile_photo = null;
			}
			Cache::forget('Sliders');
			flash(__('Slider has been deleted successfully'))->success();
        }
        else{
            flash(__('Something went wrong'))->error();
        }
        return redirect()->route('home_settings.index');
		
    }
}
