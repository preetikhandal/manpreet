<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('permissions')){
            $role = new Role;
            $role->name = $request->name;
            if(in_array("21", $request->permissions)) {
                $request->permissions = array("21");
            }
            $role->permissions = json_encode($request->permissions);
            if($role->save()){
                flash(__('Role has been inserted successfully'))->success();
                return redirect()->route('roles.index');
            }
        }
        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail(decrypt($id));
        return view('roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);

        if($request->has('permissions')){
            $role->name = $request->name;
            $role->permissions = json_encode($request->permissions);
            if($role->save()){
                flash(__('Role has been updated successfully'))->success();
                return redirect()->route('roles.index');
            }
        }
        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = \App\Staff::where('role_id', $id)->count();
        if($staff > 0) {
            flash(__('Role already assigned to staff. Remove the role first'))->warning();
            return redirect()->route('roles.index');
        }
        if(Role::destroy($id)){
            flash(__('Role has been deleted successfully'))->success();
            return redirect()->route('roles.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
}
