<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use Hash;
use App\Category;
use App\FlashDeal;
use App\Brand;
use App\SubCategory;
use App\SubSubCategory;
use App\Product;
use App\PickupPoint;
use App\CustomerPackage;
use App\CustomerProduct;
use App\User;
use App\Customer;
use App\Seller;
use App\Shop;
use App\Color;
use App\Order;
use App\Wallet;
use App\BusinessSetting;
use App\popup_offer;
use App\ReturnProduct;
use App\OrderDetail;
use App\Http\Controllers\SearchController;
use ImageOptimizer;
use Cookie;
use DB;
use PDF;
use Carbon\Carbon;
use App\Mail\InvoiceEmailManager;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;
use Mail;

class ReturnController extends Controller
{
    

    public function orderDetails(Request $request)
    {
        if($request->has('order_id')){
            $order = Order::where('id', $request->order_id)->first();
            if($order != null){
                return view('frontend.return_details', compact('order'));
            }
        }
        return view('frontend.return_details');
    }
    
    public function return_orders(Request $request)
    {
        $roles = \App\Role::where('permissions','["21"]')->select('id')->get()->pluck('id')->all();
        $delivery_agent_ids = \App\Staff::whereIn('role_id',$roles)->get()->pluck('user_id')->all();
        $delivery_agent = \App\User::whereIn('id', $delivery_agent_ids)->select('id','name','phone')->get();
        $payment_status = null;
        $delivery_status = null;
        $sort_search = null;
        $admin_user_id = User::where('user_type', 'admin')->first()->id;
        $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('order_details.seller_id', $admin_user_id)
                    ->select('orders.id','orders.code')
                    ->distinct();
        $orders = $orders->where('order_details.delivery_status', 'processing_refund')->orWhere('order_details.delivery_status', 'refunded');
        if ($request->payment_type != null){
            $orders = $orders->where('order_details.payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        if ($request->delivery_status != null) {
            $orders = $orders->where('order_details.delivery_status', $request->delivery_status);
            $delivery_status = $request->delivery_status;
        }
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
        }
        $orders = $orders->paginate(15);
        return view('return.index', compact('orders','payment_status','delivery_status', 'sort_search', 'admin_user_id', 'delivery_agent'));
    }
     public function return_orders_status(Request $request)
    {
        $roles = \App\Role::where('permissions','["21"]')->select('id')->get()->pluck('id')->all();
        $delivery_agent_ids = \App\Staff::whereIn('role_id',$roles)->get()->pluck('user_id')->all();
        $delivery_agent = \App\User::whereIn('id', $delivery_agent_ids)->select('id','name','phone')->get();
        $RouteName = \Route::currentRouteName();
        if($RouteName == "return.index.processing_refund.admin") {
            $delivery_status = 'processing_refund';
        } elseif($RouteName == "return.index.return_request.admin") {
            $delivery_status = 'return_request';
        } elseif($RouteName == "return.index.return_accepted.admin") {
            $delivery_status = 'return_accepted';
        }elseif($RouteName == "return.index.pickup_completed.admin") {
            $delivery_status = 'pickup_completed';
        }elseif($RouteName == "return.index.return_cancelled.admin") {
            $delivery_status = 'return_cancelled';
        } elseif($RouteName == "return.index.refunded.admin") {
            $delivery_status = 'refunded';
        } else {
            $delivery_status = null;
        }
        $payment_status = null;
        $sort_search = null;
        $admin_user_id = User::where('user_type', 'admin')->first()->id;
        $orders = DB::table('orders')
                    ->orderBy('code', 'desc')
                    ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                    ->where('order_details.seller_id', $admin_user_id)
                    ->where('order_details.delivery_status', $delivery_status)
                    ->select('orders.id', 'orders.code')
                    ->distinct();
        if ($request->payment_type != null){
            $orders = $orders->where('order_details.payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        if ($request->has('search')){
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%'.$sort_search.'%');
        }
        $orders = $orders->paginate(15);
        return view('return.index', compact('orders','payment_status','delivery_status', 'sort_search', 'admin_user_id', 'delivery_agent'));
    }
        
    public function update_status(Request $request)
    {
        $seller_id = $request->input('seller_id',null);
        $setFlash = $request->input('setFlash',null);
        $status = $request->input('status',null);
        $agent_id = $request->input('agent_id',null);
        $undelivery_notes = $request->input('undelivery_notes',null);
        if($agent_id != null) {
            $agent = \App\User::find($agent_id);
        }
        $order_ids = $request->order_ids;
        $action = $request->action;
        if($seller_id == null) {
            $seller_id = Auth::user()->id;
        }
        $n = 0;
        $price = 0;
        foreach($order_ids as $order_id) {
            $order = Order::findOrFail($order_id);
            $item = 0;
            if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'seller' || Auth::user()->user_type == 'vendor') {
                foreach($order->orderDetails->where('seller_id', $seller_id) as $key => $orderDetail) {
                      $returnproduct=ReturnProduct::where('order_id',$orderDetail->order_id)->where('product_id',$orderDetail->product_id)->first();
                      if( $returnproduct!=null) {
                        $returnproduct->status=$request->status;
                         $orderDetail->delivery_status = $request->status;
                         $price = $price + $orderDetail->price;
                         $returnproduct->save();
                        }
                    $orderDetail->save();
                    if($item == 0) {
                        $PName = \Illuminate\Support\Str::limit(Product::find($orderDetail->product_id)->name, 40, '');
                    }
                    $item++;
                }
            } else {
                foreach($order->orderDetails->where('seller_id', \App\User::where('user_type', 'admin')->first()->id) as $key => $orderDetail) {
                    $returnproduct=ReturnProduct::where('order_id',$orderDetail->order_id)->where('product_id',$orderDetail->product_id)->first();
                   if( $returnproduct!=null){
                                
                        $returnproduct->status=$request->status;
                         $orderDetail->delivery_status = $request->status;
                         $price = $price + $orderDetail->price;
                         $returnproduct->save();
                        }
                   
                    $orderDetail->save();
                    if($item == 0) {
                        $PName = \Illuminate\Support\Str::limit(Product::find($orderDetail->product_id)->name, 40, '');
                    }
                    $item++;
                }
            }
            
            $shipping = json_decode($order->shipping_address);
            $smsMsg = null;
            
            if ($status == "return_accepted") {
                $smsMsg = "Confirmed: Hi ".$order->user->name." !, your return request for order no. ".$order->code." has been successfully placed. You can expect Pick up in next 24-48 Hrs.";
            } elseif ($status == "refunded") {
                if($item > 1) {
                    $item  = $item - 1;
                    $PName = \Illuminate\Support\Str::limit($PName, 20, '...')." and ". $item ." more items";
                } else {
                    $PName = \Illuminate\Support\Str::limit($PName, 40, '...');
                }
                $smsMsg = "Hi, we have successfully processed your refund for your return item '.$PName.'. The refund amount is Rs. ".$price." is credited in your Alde wallet. You can manage your wallet from your My Account section.";
            } elseif ($status == 'cancel_return') {
                $smsMsg = "We cancelled your return request for order no. ".$order->code.".";
            }
            
            if($smsMsg != null) {
                $smsMsg = rawurlencode($smsMsg);
                $to = "91".$shipping->phone;
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        file_get_contents($SMS_URL);
                    }
                }
            }
            $n++;
        }
        
        if($setFlash != null) {
            flash($setFlash)->success();
        }
        return 1;
    }

    public function update_return_status(Request $request)
    {
        //dd($request->all());
        $seller_id = $request->input('seller_id',null);
        $setFlash = $request->input('setFlash',null);
        $order = Order::findOrFail($request->order_id);
        $status=$request->status;;
        $order->delivery_viewed = '0';
        $order->save();
        $user_id=$order->user_id;
       
        $User=User::findOrFail($user_id);
        $balance=$User->balance;
        $amount= $request->input('amount',null);
        $return_product_id=array();
        if($seller_id == null) {
            $seller_id = Auth::user()->id;
        }
        $item = 0;
        $price = 0;
        // $returnproduct_all=ReturnProduct::where('order_id',$orderDetail->order_id);
        if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'seller' || Auth::user()->user_type == 'vendor'){
            foreach($order->orderDetails->where('seller_id', $seller_id) as $key => $orderDetail){
               $returnproduct=ReturnProduct::where('order_id',$orderDetail->order_id)->where('product_id',$orderDetail->product_id)->first();
                     array_push($return_product_id,$returnproduct->id);
                      if( $returnproduct!=null){
                        $returnproduct->status=$request->status;
                         $orderDetail->delivery_status = $request->status;
                         $price = $price + $orderDetail->price;
                         $returnproduct->save();
                    }
                    if($item == 0) {
                        $PName = \Illuminate\Support\Str::limit(Product::find($orderDetail->product_id)->name, 40, '');
                    }
                $orderDetail->save();
                $item++;
            }
        }
        else{
            foreach($order->orderDetails->where('seller_id', \App\User::where('user_type', 'admin')->first()->id) as $key => $orderDetail){
                $returnproduct=ReturnProduct::where('order_id',$orderDetail->order_id)->where('product_id',$orderDetail->product_id)->first();
                      array_push($return_product_id,$returnproduct->id);
                      if( $returnproduct!=null){
                        $returnproduct->status=$request->status;
                         $orderDetail->delivery_status = $request->status;
                         $price = $price + $orderDetail->price;
                         $returnproduct->save();
                        }
                        if($item == 0) {
                        $PName = \Illuminate\Support\Str::limit(Product::find($orderDetail->product_id)->name, 40, '');
                    }
                $orderDetail->save();
                $item++;
            }
        }
         $smsMsg = null;
             $shipping = json_decode($order->shipping_address);
             $detail=array('order_id'=>$request->order_id,'return_order_id'=>$return_product_id,'method'=>'Refund for return Product','amount'=>$amount);
            $detail=json_encode($detail);
            if ($status == "return_accepted") {
                $smsMsg = "Confirmed: Hi ".$order->user->name." !, your return request for order no. ".$order->code." has been successfully placed. You can expect Pick up in next 24-48 Hrs.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Return Accepted";
            } elseif ($status == "refunded") {
                 $wallet=new Wallet;
                $wallet->user_id=$user_id;
                $wallet->amount=$amount;
                $wallet->payment_method='refund';
                $wallet->payment_details=$detail;
                 $wallet->add_status=1;
                $wallet->save();
                 $User->balance=$balance+$amount;
                 $User->save();
                 
                if($item > 1) {
                    $item  = $item - 1;
                    $PName = \Illuminate\Support\Str::limit($PName, 20, '...')." and ". $item ." more items";
                } else {
                    $PName = \Illuminate\Support\Str::limit($PName, 40, '...');
                }
                $smsMsg = "Hi, we have successfully processed your refund for your return item '.$PName.'. The refund amount is Rs. ".$price." is credited in your Alde wallet. You can manage your wallet from your My Account section.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Refund Completed";
            } elseif ($status == 'cancel_return') {
                $smsMsg = "We cancelled your return request for order no. ".$order->code.".";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Return Cancelled";
            }
            

            
            
            if($smsMsg != null) {
                $smsMsg = rawurlencode($smsMsg);
                $to = "91".$shipping->phone;
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        file_get_contents($SMS_URL);
                    }
                }
            }
            if(isset($message)) {
            if($message != null) {
                $emailData = array('email' => $order->user->email, 'name' => $order->user->name, 'message' => $message, 'email_subject' => $email_subject);
                Mail::send('emails.order_notification',array('data' => $emailData), function ($m) use ($emailData) {
                        $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                });
           }
           }
           
        if($setFlash != null) {
            flash($setFlash)->success();
        }
        return 1;
    }
    
    public function save_request(Request $request)
    {
        $order_id=$request->order_id;
        $product_id=$request->product_id;
        $return_type='refund';
        $status='processing_refund';//Pickup Process,Pickup Completed,Refund Processed,Refunded
        $return_reason=$request->return_reason;
        $retunproduct=new ReturnProduct;
         $OrderDetail = OrderDetail::where('order_id',$request->order_id)->where('product_id',$request->product_id)->first();
         $retunproduct->order_id=$order_id;
         $retunproduct->product_id=$product_id;
         $retunproduct->return_type=$return_type;
         $retunproduct->status=$status;
         $retunproduct->return_reason=$return_reason;
         $OrderDetail->delivery_status=$status;
          $OrderDetail->save();
         $retunproduct->save();
         $orders = Order::where('user_id', Auth::user()->id)->orderBy('code', 'desc')->paginate(9);
         
       
        // $orders = Order::where('id', $request->order_id)->get();
        return view('frontend.purchase_history',compact('orders'));
    }
    
    public function save_all_request(Request $request)
    {
        $order_id=$request->order_id;
        $product_id=$request->product_id;
        $return_reason=$request->return_reason;
        $return_type='refund';
        $status='return_request';
        $retunproduct=new ReturnProduct;
       //dd($product_id);
       foreach($product_id as $p_id)
       {
        //Pickup Process,Pickup Completed,Refund Processed,Refunded
        $retunproduct->order_id=$order_id;
         $retunproduct->product_id=$p_id;
         $retunproduct->return_type=$return_type;
         $retunproduct->status=$status;
         $retunproduct->return_reason=$return_reason;
         $retunproduct->save();
          $OrderDetail = OrderDetail::where('order_id',$order_id)->where('product_id',$p_id)->first();
          $OrderDetail->delivery_status=$status;
          $OrderDetail->save();
       }
       $order = Order::find($order_id);
       $shipping = json_decode($order->shipping_address);
       
         if ($status == "return_request") {
            $smsMsg = "Hi ".$order->user->name." !, your return request for order no. ".$order->code." has been successfully placed. We will notify you once we confirmed the return request.";
            $message = $smsMsg;
            $email_subject = "AldeBazaar : Return Request Received";
        }
        if(isset($message)) {
            if($message != null) {
                $emailData = array('email' => $order->user->email, 'name' => $order->user->name, 'message' => $message, 'email_subject' => $email_subject);
                Mail::send('emails.order_notification',array('data' => $emailData), function ($m) use ($emailData) {
                        $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                });
           }
        }
        flash(__('Request submitted successfully'))->success();
         $orders = Order::where('id', $request->order_id)->get();
        return view('frontend.return_details',compact('orders'));
    }
    
    public function return_history_details(Request $request)
    {
      //  dd($request->all());
        $OrderDetail = OrderDetail::where('order_id',$request->order_id)->where('product_id',$request->product_id)->first();
       // dd($OrderDetail);
      //  $order->delivery_viewed = 1;
       // $order->payment_status_viewed = 1;
       // $order->save();
        return view('frontend.partials.return_details_customer', compact('OrderDetail'));
    }

    public function track_request(Request $request)
    {
         $orders = Order::where('user_id', Auth::user()->id)->orderBy('code', 'desc')->pluck('id');
         $ReturnProduct=ReturnProduct::whereIn('order_id', $orders)->paginate(10);
        // dd($ReturnProduct);
         return view('frontend.return_history', compact('ReturnProduct'));
        
    }
    
    public function show($id)
    {
        $order = Order::findOrFail(decrypt($id));
        $order->viewed = 1;
        $order->save();
        
        return view('orders.show_return', compact('order'));
    }
    
    public function show_return($id)
    {
        $order = Order::findOrFail(decrypt($id));
        $order->viewed = 1;
        $order->save();
        return view('return.show_return', compact('order'));
    }
}
