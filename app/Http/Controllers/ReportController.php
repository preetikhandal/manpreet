<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Seller;
use App\Order;
use App\OrderDetail;
use App\Expenses;
use App\Wishlist;
use App\ExpensesCategory;
use App\User;
use Carbon\Carbon;
use App\SalesReportExport;
use App\ProductsSalesReportDownload;
use Excel;

class ReportController extends Controller
{
    public function ExpenseReport(Request $request) {
        $ExpensesCategorySelected = $request->input('category',0);
        $date = $request->input('date', 0);
        $ExpensesPaymentStatusSelected = $request->input('paymentStatus', 'Paid');
        if ($date != 0) {
           $date = explode("-", $date);
           $date = array_map('trim',$date);
           $dateStart = Carbon::createFromFormat('d/m/Y', $date[0])->toDateString();
           $dateEnd = Carbon::createFromFormat('d/m/Y', $date[1])->toDateString();
        } else {
           $dateStart = Carbon::now()->toDateString();
           $dateEnd = Carbon::now()->toDateString();
        }
		$Expenses = Expenses::whereBetween('ExpenseInvoiceDate', [$dateStart, $dateEnd]);
        if($ExpensesCategorySelected != 0) {
            $Expenses = $Expenses->where('ECategoryID',$ExpensesCategorySelected);
        }
        if($ExpensesPaymentStatusSelected != "All") {
            $Expenses = $Expenses->where('PaymentStatus',$ExpensesPaymentStatusSelected);
        }
        $Expenses = $Expenses->paginate(env('RECORDPERPAGE', '15'));
        
        $ExpensesInvoiceAmount = $Expenses->pluck('ExpensesInvoiceAmount')->sum();
        $PaidAmount = $Expenses->pluck('PaidAmount')->sum();
		$ExpensesCategory=ExpensesCategory::get();
		$data = array("Expenses" => $Expenses, "PaidAmount" => $PaidAmount, "ExpensesInvoiceAmount" => $ExpensesInvoiceAmount, "ExpensesCategory" => $ExpensesCategory, 'ExpensesCategorySelected' => $ExpensesCategorySelected, 'dateStart' => $dateStart, 'dateEnd' => $dateEnd, 'ExpensesPaymentStatusSelected' => $ExpensesPaymentStatusSelected);
		return view('reports.ExpenseReport', $data);
	}
    
    public function SalesReport(Request $request) {
        $date = $request->input('date', 0);
           if ($date != 0) {
           $date = explode("-", $date);
           $date = array_map('trim',$date);
           $dateStart = Carbon::createFromFormat('d/m/Y', $date[0])->toDateString();
            $dateEnd = Carbon::createFromFormat('d/m/Y', $date[1])->toDateString();
          } else {
         $dateStart =Carbon::now();//->toDateString();
           $dateEnd = Carbon::now();//->toDateString();
		   
        }
				$Order=Order::whereDate('created_at','>=',$dateStart)->whereDate('created_at','<=',$dateEnd)->paginate(10);
       // foreach($Order as $O) {
         //   $Order->OrderDetail 
           //$Order[$n]->OrderAmount = number_format($O->OrderAmount - ($O->OrderAmount * ($O->OrderDiscount/100)),2,".","");
           //$Order[$n]->OrderTotalTax1 = number_format($O->OrderTotalTax1 - ($O->OrderTotalTax1 * ($O->OrderDiscount/100)),2,".","");
           //$Order[$n]->OrderTotalTax2 = number_format($O->OrderTotalTax2 - ($O->OrderTotalTax2 * ($O->OrderDiscount/100)),2,".","");
           //$n++;
    //    }//
		$data = array("Order" => $Order, 'dateStart' => $dateStart, 'dateEnd' => $dateEnd);
		//dd($data);
		return view('reports.ReportSales', $data);
	}
	
    
    public function stock_report(Request $request)
    {
        if($request->has('category_id')){
            $products = Product::where('category_id', $request->category_id)->paginate(10);
        }
        else{
            $products = Product::paginate(10);
        }
        return view('reports.stock_report', compact('products'));
    }

    public function in_house_sale_report(Request $request)
    {
        if($request->has('category_id')){
            $products = Product::where('category_id', $request->category_id)->orderBy('num_of_sale', 'desc')->paginate(10);
        }
        else{
            $products = Product::orderBy('num_of_sale', 'desc')->paginate(10);
        }
        return view('reports.in_house_sale_report', compact('products'));
    }

    public function seller_report(Request $request)
    {
        if($request->has('verification_status')){
            $sellers = Seller::where('verification_status', $request->verification_status)->paginate(10);
        }
        else{
            $sellers = Seller::paginate(10);
        }
        
        return view('reports.seller_report', compact('sellers'));
    }

    public function seller_sale_report(Request $request)
    {
        if($request->has('verification_status')){
            $sellers = Seller::where('verification_status', $request->verification_status)->paginate(10);
        }
        else{
            $sellers = Seller::paginate(10);
        }
        return view('reports.seller_sale_report', compact('sellers'));
    }

    public function wish_report(Request $request)
    {/*
     $wishlists=Wishlist::select('product_id','COUNT(product_id)')->groupBy('product_id')->paginate(20);
    
        if($request->has('category_id')){
            $products = Product::where('category_id', $request->category_id)->paginate(20);
        }
        else{
            $products = Product::paginate(20);
        }
        
        $wishlists=Wishlist::paginate(20); ->select('department', DB::raw('SUM(price) as total_sales'))
                ->groupBy('department')
                ->havingRaw('SUM(price) > ?', [2500])
                ->get();
                 $wishlists=Wishlist::select('product_id', DB::raw('COUNT(product_id) as wish_count')->groupBy('product_id')->paginate(20);
                 $wishlists = Wishlist::paginate(20);
                */
                $wishlists=DB::select("select `product_id`, COUNT(product_id) as 'wish_count' from `wishlists` group by `product_id`");
               // dd( $wishlists);
        return view('reports.wish_report', compact('wishlists'));
    }
    
    public function salesReportDownload(Request $request) {
         return Excel::download(new SalesReportExport($request), 'sales_report_'. time() .'.xlsx');
    }
    
    public function productsSalesReportDownload(Request $request) {
         return Excel::download(new ProductsSalesReportDownload($request), 'product_sales_report_'. time() .'.xlsx');
    }
}
