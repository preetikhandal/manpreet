<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCategory;
use App\SubSubCategory;
use App\Category;
use App\Product;
use App\Language;
use ImageOptimizer;
use Illuminate\Support\Facades\Cache;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $sort_search =null;
        $subcategories = SubCategory::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $subcategories = $subcategories->where('name', 'like', '%'.$sort_search.'%');
        }
		if($request->has('searchdata')){
			$categorySearchID = $request->input('categorySearchID',0);
			
			if($categorySearchID!=null){
				$subcategories=SubCategory::where('category_id',$categorySearchID);
			}
			
		}
		
        $subcategories = $subcategories->paginate(50);
        return view('subcategories.index', compact('subcategories', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('subcategories.create', compact('categories'));
    }


	public function getsubcats(Request $request){
		$subcats = SubCategory::where('category_id', $request->categorySearchID)->get();
        $data="<option value='all'>All</option>";
		foreach($subcats as $subcatgy){
			$data.="<option value='".$subcatgy->id."'>".strtoupper($subcatgy->name)."</option>";
		}
		echo $data;
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
			'name' => 'required|min:3|max:50',
			'meta_title' => 'max:255',
			'meta_keywords' => 'max:1000',
			'icon' => 'file|max:2048|mimes:jpeg,jpg,png'
		]);
        $subcategoryname = $request->name;
        
		$subcategoryname=ucwords(strtolower($subcategoryname));
        $category_id = $request->category_id;
		//check sub category in db
		$count= SubCategory::where('category_id',$category_id)->where('name',$subcategoryname)->count();
		if($count!=0){
			flash(__('Sub Category already Exists'))->error();
            return back();
		}
		$subcategory = new SubCategory;
        $subcategory->name = $subcategoryname;
		$subcategory->display = $request->display;
        $subcategory->category_id = $request->category_id;
		if($request->meta_title==""){
		    $subcategory->meta_title = $subcategoryname.": Buy ".$subcategoryname." products online at best prices in India - Aldebazaar.com";
		}
		else{
		    $subcategory->meta_title =$request->meta_title;
		}
		if($request->meta_description==""){
		   $subcategory->meta_description = "Buy ".$subcategoryname." products online at best prices from Aldebazaar.com. Check out ".$subcategoryname." products from top brands at Aldebazaar.com.";
        
		}
		else{
		    $subcategory->meta_description =$request->meta_description;
		}
        
        if ($request->slug != null) {
            $subcategory->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->slug)));
        }
        else {
            $subcategory->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $subcategoryname)));
        }
        $subcategory->meta_keywords = implode('|',$request->meta_keywords);
        $subcategory->slug = str_ireplace('--','-', $subcategory->slug);
        if($request->hasFile('icon')){
        	$extension = $request->icon->extension();
        	$icon = Str::slug($subcategoryname,"-")."icon".time().".".$extension;
        	$img = Image::make($request->icon);
        	$width = Image::make($request->icon)->width();
        	$height = Image::make($request->icon)->height();
        	
        	if($width == 150 && $height == 150) {
    			 $request->icon->move(base_path('public/')."temp/",$icon);
    		} else {
        		$img->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
        		$img->save(base_path('public/')."temp/".$icon);
    		}
        	
        	if(env('FILESYSTEM_DRIVER') == "local") {
        		copy(base_path('public/').'temp/'.$icon, base_path('public/').'uploads/subcategories/'.$icon);
        	} else {
        		Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/subcategories', new \Illuminate\Http\File(base_path('public/').'temp/'.$icon), $icon);
        	}
        	$subcategory->icon = "uploads/subcategories/".$icon;
        	unlink(base_path('public/').'temp/'.$icon);
        }
        $top_sku = $request->top_sku;
        if($top_sku == "") {
            $top_sku = array();
        } else {
            $top_sku = explode(",", $top_sku);
            foreach($top_sku as $key => $sku) {
                $top_sku[$key] = trim($top_sku[$key]);
            }
        }
        
        $subcategory->top_sku = json_encode($top_sku);
        
        
        if($subcategory->save()){
            flash(__('Subcategory has been inserted successfully'))->success();
            return redirect()->route('subcategories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory = SubCategory::findOrFail(decrypt($id));
        $categories = Category::all();
        return view('subcategories.edit', compact('categories', 'subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
			'name' => 'required|min:3|max:50',
			'meta_title' => 'max:255',
			'meta_keywords' => 'max:1000',
			'icon' => 'file|max:2048|mimes:jpeg,jpg,png'
		]);
        $subcategory = SubCategory::findOrFail($id);
	
		$subcategoryname = $request->name;
		$subcategoryname=ucwords(strtolower($subcategoryname));
        $subcategory->name = $subcategoryname;
		$subcategory->display = $request->display;
        $subcategory->category_id = $request->category_id;
  
	    $subcategory->meta_title =$request->meta_title;
	    $subcategory->meta_description =$request->meta_description;
	
       	if ($request->slug != null) {
            $subcategory->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->slug)));
        }
        else {
            $subcategory->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $subcategoryname)));
        }
        $subcategory->slug = str_ireplace('--','-', $subcategory->slug);
        $subcategory->meta_keywords = implode('|',$request->meta_keywords);
        
        if($request->hasFile('icon')){
        	if($subcategory->icon!=null){
        		if(env('FILESYSTEM_DRIVER') == "local") {
        			if(file_exists(base_path('public/').$subcategory->icon))
        			unlink(base_path('public/').$subcategory->icon);
        		} else {
        			Storage::disk(env('FILESYSTEM_DRIVER'))->delete($subcategory->icon);
        		}
        	}
        	$extension = $request->icon->extension();
        	$icon = Str::slug($subcategoryname,"-")."icon".time().".".$extension;
        	$img = Image::make($request->icon);
        	$width = Image::make($request->icon)->width();
        	$height = Image::make($request->icon)->height();
        
            if($width == 150 && $height == 150) {
    			 $request->icon->move(base_path('public/')."temp/",$icon);
    		} else {
        		$img->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
        		$img->save(base_path('public/')."temp/".$icon);
    		}
        				
        	if(env('FILESYSTEM_DRIVER') == "local") {
        		copy(base_path('public/').'temp/'.$icon, base_path('public/').'uploads/subcategories/'.$icon);
        	} else {
        		Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/subcategories', new \Illuminate\Http\File(base_path('public/').'temp/'.$icon), $icon);
        	}
        	$subcategory->icon = "uploads/subcategories/".$icon;
        	unlink(base_path('public/').'temp/'.$icon);
        }
        
        $top_sku = $request->top_sku;
        if($top_sku == "") {
            $top_sku = array();
        } else {
            $top_sku = explode(",", $top_sku);
            foreach($top_sku as $key => $sku) {
                $top_sku[$key] = trim($top_sku[$key]);
            }
        }
        
        $subcategory->top_sku = json_encode($top_sku);
        
        if($subcategory->save()){
            flash(__('Subcategory has been updated successfully'))->success();
            return redirect()->route('subcategories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = SubCategory::findOrFail($id);
        
        if($subcategory->subsubcategories->count() > 0) {
		    flash(__('Sub Category has been assigned to sub sub category. Unassigned before delete this sub category'))->warning();
		    return back();
		}
        $ProductCount = Product::where('subcategory_id', $subcategory->id)->count();
        if($ProductCount > 0) {
		    flash(__('Sub Category has been assigned to Products. Unassigned before delete this sub category'))->warning();
		    return back();
		}
        if(SubCategory::destroy($id)){
            if($subcategory->icon != null){
            	if(env('FILESYSTEM_DRIVER') == "local") {
            		if(file_exists(base_path('public/').$subcategory->icon))
            		unlink(base_path('public/').$subcategory->icon);
            	} else {
            		Storage::disk(env('FILESYSTEM_DRIVER'))->delete($subcategory->icon);
            	}
            	$subcategory->icon = null;
            }
            flash(__('Subcategory has been deleted successfully'))->success();
            return redirect()->route('subcategories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function get_subcategories_by_category(Request $request)
    {
        $subcategories = SubCategory::where('category_id', $request->category_id)->get();
        
        return $subcategories;
    }
}
