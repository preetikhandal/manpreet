<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SitemapSetting;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use App\Blog;
use App\Category;
use App\FlashDeal;
use App\Brand;
use App\SubCategory;
use App\SubSubCategory;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use File;
use Spatie\Sitemap\SitemapIndex;

class SitemapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sitemaps = SitemapSetting::all();
        return view('sitemap_settings.index', compact('sitemaps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sitemap_settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
			'type' => 'required',
		]);

        $sitemap = new SitemapSetting;
        $sitemap->type = $request->type;
        if($request->type == "Product" || $request->type == "Category" || $request->type == "SubCategory" || $request->type == "SubSubCategory" || $request->type == "Brand" || $request->type == "Blog") {
            $count = SitemapSetting::where('type', $request->type)->count();
            if($count > 0) {
                flash(__('You able to add only one setting for '.$request->type." Kindly change the setting exiting."))->error();
                return  back()->withInput();
            }
        }
        $data = $request->input('data',null);
        if($sitemap->type == "link") {
            if(strlen($data) > 2) {
            $url = url('/');
            $data = str_replace($url, "", $data);
            } else {
                flash(__('Please enter the link data.'))->error();
                return  back()->withInput();
            }
        }
        $sitemap->data = $request->input('data',null);
        $sitemap->changefreq = $request->input('changefreq',null);
        $sitemap->priority = $request->input('priority',null);
        if($sitemap->save()){
           flash(__('Sitemap setting has been created successfully'))->success();
                return redirect()->route('sitemap.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sitemap_generate()
    {
        $sitemap_path = 'sitemaps';
        $allFiles = Storage::files($sitemap_path);
        foreach($allFiles as $file) {
            Storage::delete($file);
        }
        $types = array('link', 'Product', 'Category', 'SubCategory', 'SubSubCategory', 'Brand', 'Blog');
        foreach($types as $type) {
            $sitemaps = SitemapSetting::where('type', $type)->get();
            $SitemapXML = Sitemap::create();
            $file_count = 0;
            $n = 0;
            foreach($sitemaps as $sitemap) {
                if($type == "link") {
                    $SitemapXML = $SitemapXML->add(Url::create($sitemap->data)->setLastModificationDate(Carbon::yesterday())->setChangeFrequency($sitemap->changefreq)->setPriority($sitemap->priority));
                    $n++;
                    if($n >= 5000) {
                        if($file_count == 0) {
                            $fileName = $sitemap_path.'/'.strtolower($type).'.xml';
                        } else {
                            $fileName = $sitemap_path.'/'.strtolower($type).'_'.$file_count.'.xml';
                        }
                        $SitemapXML->writeToDisk(env('FILESYSTEM_DRIVER'), $fileName);
                        $SitemapXML = Sitemap::create();
                        $file_count++;
                        $n = 0;
                    }  
                } elseif($type == "Product") {
                    $Products = Product::where('published', 1)->get();
                    foreach($Products as $P) {
                        $SitemapXML = $SitemapXML->add(Url::create("/product/".$P->slug)->setLastModificationDate(Carbon::parse($P->updated_at))->setChangeFrequency($sitemap->changefreq)->setPriority($sitemap->priority));
                        $n++;
                        if($n >= 5000) {
                            if($file_count == 0) {
                                $fileName = $sitemap_path.'/'.strtolower($type).'.xml';
                            } else {
                                $fileName = $sitemap_path.'/'.strtolower($type).'_'.$file_count.'.xml';
                            }
                            $SitemapXML->writeToDisk(env('FILESYSTEM_DRIVER'), $fileName);
                            $SitemapXML = Sitemap::create();
                            $file_count++;
                            $n = 0;
                        }
                    }
                } elseif($type == "Category") {
                    $Category = Category::get();
                    foreach($Category as $C) {
                        $SitemapXML = $SitemapXML->add(Url::create("/category/".$C->slug)->setLastModificationDate(Carbon::parse($C->updated_at))->setChangeFrequency($sitemap->changefreq)->setPriority($sitemap->priority));
                        $n++;
                        if($n >= 5000) {
                            if($file_count == 0) {
                                $fileName = $sitemap_path.'/'.strtolower($type).'.xml';
                            } else {
                                $fileName = $sitemap_path.'/'.strtolower($type).'_'.$file_count.'.xml';
                            }
                            $SitemapXML->writeToDisk(env('FILESYSTEM_DRIVER'), $fileName);
                            $SitemapXML = Sitemap::create();
                            $file_count++;
                            $n = 0;
                        }
                    }
                } elseif($type == "SubCategory") {
                    $SubCategory = SubCategory::get();
                    foreach($SubCategory as $C) {
                        $Category = $C->category->slug;
                        $SitemapXML = $SitemapXML->add(Url::create("/category/$Category/".$C->slug)->setLastModificationDate(Carbon::parse($C->updated_at))->setChangeFrequency($sitemap->changefreq)->setPriority($sitemap->priority));
                        $n++;
                        if($n >= 5000) {
                            if($file_count == 0) {
                                $fileName = $sitemap_path.'/'.strtolower($type).'.xml';
                            } else {
                                $fileName = $sitemap_path.'/'.strtolower($type).'_'.$file_count.'.xml';
                            }
                            $SitemapXML->writeToDisk(env('FILESYSTEM_DRIVER'), $fileName);
                            $SitemapXML = Sitemap::create();
                            $file_count++;
                            $n = 0;
                        }
                    }
                } elseif($type == "SubSubCategory") {
                    $SubSubCategory = SubSubCategory::get();
                    foreach($SubSubCategory as $C) {
                            $SubCategory = $C->subcategory->slug;
                            $Category = $C->subcategory->category->slug;
                            $SitemapXML = $SitemapXML->add(Url::create("/category/$Category/$SubCategory/".$C->slug)->setLastModificationDate(Carbon::parse($C->updated_at))->setChangeFrequency($sitemap->changefreq)->setPriority($sitemap->priority));
                        $n++;
                        if($n >= 5000) {
                            if($file_count == 0) {
                                $fileName = $sitemap_path.'/'.strtolower($type).'.xml';
                            } else {
                                $fileName = $sitemap_path.'/'.strtolower($type).'_'.$file_count.'.xml';
                            }
                            $SitemapXML->writeToDisk(env('FILESYSTEM_DRIVER'), $fileName);
                            $SitemapXML = Sitemap::create();
                            $file_count++;
                            $n = 0;
                        }
                    }
                } elseif($type == "Brand") {
                    $Brand = Brand::get();
                    foreach($Brand as $C) {
                        $SitemapXML = $SitemapXML->add(Url::create("/category?brand=".$C->slug)->setLastModificationDate(Carbon::parse($C->updated_at))->setChangeFrequency($sitemap->changefreq)->setPriority($sitemap->priority));
                        $n++;
                        if($n >= 5000) {
                            if($file_count == 0) {
                                $fileName = $sitemap_path.'/'.strtolower($type).'.xml';
                            } else {
                                $fileName = $sitemap_path.'/'.strtolower($type).'_'.$file_count.'.xml';
                            }
                            $SitemapXML->writeToDisk(env('FILESYSTEM_DRIVER'), $fileName);
                            $SitemapXML = Sitemap::create();
                            $file_count++;
                            $n = 0;
                        }
                    }
                } elseif($type == "Blog") {
                    $Blog = Blog::get();
                    foreach($Blog as $C) {
                        $SitemapXML = $SitemapXML->add(Url::create("/blog/".$C->URLSlug)->setLastModificationDate(Carbon::parse($C->updated_at))->setChangeFrequency($sitemap->changefreq)->setPriority($sitemap->priority));
                        $n++;
                        if($n >= 5000) {
                            if($file_count == 0) {
                                $fileName = $sitemap_path.'/'.strtolower($type).'.xml';
                            } else {
                                $fileName = $sitemap_path.'/'.strtolower($type).'_'.$file_count.'.xml';
                            }
                            $SitemapXML->writeToDisk(env('FILESYSTEM_DRIVER'), $fileName);
                            $SitemapXML = Sitemap::create();
                            $file_count++;
                            $n = 0;
                        }
                    }
                }
            }
            if($n > 0) {
                if($file_count == 0) {
                    $fileName = $sitemap_path.'/'.strtolower($type).'.xml';
                } else {
                    $fileName = $sitemap_path.'/'.strtolower($type).'_'.$file_count.'.xml';
                }
                $SitemapXML->writeToDisk(env('FILESYSTEM_DRIVER'), $fileName);
            }
        }
        
        $allFiles = Storage::files($sitemap_path);
        $SitemapIndex = SitemapIndex::create();
        foreach($allFiles as $files) {
            $SitemapIndex = $SitemapIndex->add(url($files));
        }
        $SitemapIndex->writeToDisk(env('FILESYSTEM_DRIVER'), 'sitemap.xml');
        
        flash(__('Sitemap has been generated successfully'))->success();
        return redirect()->route('sitemap.index');
    }
    
    public function sitemap() {
        header('Content-Type: application/xml');
        echo Storage::get('sitemap.xml');
    }
    public function sitemaps($fileName) {
        header('Content-Type: application/xml');
        echo Storage::get('sitemaps/'.$fileName);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sitemap = SitemapSetting::findOrFail(decrypt($id));
        return view('sitemap_settings.edit', compact('sitemap'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sitemap = SitemapSetting::findOrFail($id);
        
        $data = $request->input('data',null);
        if($sitemap->type == "link") {
            if(strlen($data) > 2) {
            $url = url('/');
            $data = str_replace($url, "", $data);
            } else {
                flash(__('Please enter the link data.'))->error();
                return  back()->withInput();
            }
        }
        $sitemap->data = $data;
        $sitemap->changefreq = $request->input('changefreq',null);
        $sitemap->priority = $request->input('priority',null);
        if($sitemap->save()){
           flash(__('Sitemap data has been updated successfully'))->success();
           return redirect()->route('sitemap.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(SitemapSetting::destroy($id)){
            flash(__('Sitemap data has been deleted successfully'))->success();
            return redirect()->route('sitemap.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }
}
