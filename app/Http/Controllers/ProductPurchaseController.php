<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductPurchase;
use App\ProductPurchaseDetail;
use App\ProductPurchasePayment;
use App\VendorDetails;
use App\Vendor;
use Auth;
use Carbon\Carbon;
use Mail;
use App\Mail\AlertEmailManager;

class ProductPurchaseController extends Controller
{
	public function Product(){
		$ProductPurchase=ProductPurchase::paginate( env('RECORDPERPAGE', '10'));
		$data = array("ProductPurchase"=>$ProductPurchase);
		return view('product_purchases.index',$data);
	}
    
	function Recaluclate($ExpenseID){
		$Expenses = Expenses::find($ExpenseID);
		$InvoiceAmount=$Expenses->ExpensesInvoiceAmount;
		$Paid_amount_old=$Expenses->PaidAmount;
		$ExpensesPayments = ExpensesPayments::where('ExpenseID',$ExpenseID)->get();
		if ($ExpensesPayments == NULL) {
			$Total_PaidAmount=0;
			$OutstandingAmount=$InvoiceAmount;
		} else {
			$Expense_Payment=$ExpensesPayments->pluck('ExpensesPaidAmount');
			$Total_PaidAmount=$Expense_Payment->sum();
			$OutstandingAmount=$InvoiceAmount-$Total_PaidAmount;
		}
		
		if($OutstandingAmount==0) $Payment_Status="Paid"; 
		elseif ($Total_PaidAmount==0) $Payment_Status="Unpaid";
		else $Payment_Status="Partially Paid";
		$data=array("PaymentStatus"=>$Payment_Status,"OutstandingAmount"=>$OutstandingAmount,"PaidAmount"=>$Total_PaidAmount);
		return $data;
	}
    
	public function ProductCreate(){
		$Vendor=Vendor::get();
		$data = array("Vendor"=>$Vendor);
		return view('product_purchases.add',$data);
	}
    
	public function ProductAdd(Request $request) {
		$vendor_id=$request->input("campany_name");
		$invoice_date=$request->input("invoice_date");
		$invoice_no=$request->input("invoice_no");
		$igst=$request->input("igst");
		$cgst=$request->input("cgst");
		$sgst=$request->input("sgst");
		$invoice_amount=$request->input("invoice_amount");
		$category_id=$request->input("category_id");
		$product_ids=$request->input("product_id");
		$mrp_price=$request->input("mrp_price");
		$unit_price=$request->input("unit_price");
		$discount=$request->input("discount");
		$quantity=$request->input("quantity");
		$sgst_price=$request->input("sgst_price");
		$sgst_per=$request->input("sgst_per");
		$cgst_price=$request->input("cgst_price");
		$cgst_per=$request->input("cgst_per");
		$igst_price=$request->input("igst_price");
		$igst_per=$request->input("igst_per");
		$stock_update=$request->input("stock_update");
		$subtotal=$request->input("subtotal");
		$invoice_date=Carbon::createFromFormat('d/m/Y', $invoice_date)->toDateString();
		$paid_amount=$request->input("paid_amount");
		$outstanding_amount = $invoice_amount - $paid_amount;
		
		if(empty($paid_amount)){$paid_amount=0;}
			
		if($outstanding_amount==0) $status="Paid" ; 
		else if($paid_amount==0) $status="Unpaid";
		else $status="Partially Paid";
		
		$ProductPurchase = new ProductPurchase;
		$ProductPurchase->vendor_id = $vendor_id;
		$ProductPurchase->invoice_no = $invoice_no;
		$ProductPurchase->invoice_date = $invoice_date;
		$ProductPurchase->igst = $igst;
		$ProductPurchase->cgst = $cgst;
		$ProductPurchase->sgst = $sgst;
		$ProductPurchase->invoice_amount = $invoice_amount;
		$ProductPurchase->paid_amount = $paid_amount;
		$ProductPurchase->outstanding_amount = $outstanding_amount;
		$ProductPurchase->status = $status;
		$ProductPurchase->save();
        
        foreach($product_ids as $key => $product_id)
        {
            $ProductPurchaseDetail = new ProductPurchaseDetail;
            $ProductPurchaseDetail->product_purchase_id = $ProductPurchase->product_purchase_id;

            $Product = \App\Product::where('id',$product_id)->select('id', 'name', 'current_stock')->first();
            if($stock_update[$key]) {
                $Product->current_stock = $Product->current_stock + $quantity[$key];
                $Product->save();
            }
            $ProductPurchaseDetail->category_id = $category_id[$key];
            $ProductPurchaseDetail->product_id = $product_id;
            $ProductPurchaseDetail->product_name = $Product->name;
            $ProductPurchaseDetail->mrp_price = $mrp_price[$key];
            $ProductPurchaseDetail->unit_price = $unit_price[$key];
            $ProductPurchaseDetail->discount = $discount[$key];
            $ProductPurchaseDetail->quantity = $quantity[$key];
            $ProductPurchaseDetail->igst_per = $igst_per[$key];
            $ProductPurchaseDetail->igst_price = $igst_price[$key];
            $ProductPurchaseDetail->cgst_price = $cgst_price[$key];
            $ProductPurchaseDetail->cgst_per = $cgst_per[$key];
            $ProductPurchaseDetail->sgst_price = $sgst_price[$key];
            $ProductPurchaseDetail->sgst_per = $sgst_per[$key];
            $ProductPurchaseDetail->sub_total = $subtotal[$key];
            $ProductPurchaseDetail->save();
        }

		$payment_date=$request->input("payment_date");
		$payment_method=$request->input("payment_method");
		$refID=$request->input("refID");
		if($paid_amount>0){
			if($payment_date == ""){ $payment_date = $invoice_date; }
			else{ $payment_date=Carbon::createFromFormat('d/m/Y', $payment_date)->toDateString(); }
			
			$ProductPurchasePayment = new ProductPurchasePayment;
			$ProductPurchasePayment->product_purchase_id = $ProductPurchase->product_purchase_id;
			$ProductPurchasePayment->paid_amount = $paid_amount;
			$ProductPurchasePayment->payment_date = $payment_date;
			$ProductPurchasePayment->payment_method = $payment_method;
			$ProductPurchasePayment->refID = $refID;
			$ProductPurchasePayment->save();
		}
        
		flash(__('Product Purchase added successfully.'))->success();
		return redirect('admin/product/purchase');
	}
    
	public function ExpensesEditView(Request $request, $id){
		$Expenses=Expenses::find($id);
		$ExpensesCategory=ExpensesCategory::get();
		$ExpensesDetails=ExpensesDetails::where('expenses_id',$id)->get();
		$data = array("categories"=>$ExpensesCategory,"Expenses"=>$Expenses, "ExpensesDetails" => $ExpensesDetails);
		return view('expenses.ExpensesEdit',$data);
	}
    
	public function ExpensesEditPost(Request $request, $id){
		$Expenses=Expenses::find($id);
		$categoryID=$request->input("category");
		$expensename=$request->input("expensename");
		$invoicedate=$request->input("invoicedate");
		$invoiceno=$request->input("invoiceno");
		$igst=$request->input("igst");
		$cgst=$request->input("cgst");
		$sgst=$request->input("sgst");
		$invoiceamount=$request->input("invoiceamount");
        
		$deleted_expenses_details_ids=$request->input("deleted_expenses_details_ids",array());
		$expenses_details_id=$request->input("expenses_details_id",array());
		$expenses_types=$request->input("expenses_type");
		$category_id=$request->input("category_id");
		$item_description=$request->input("item_description");
		$unit_price=$request->input("unit_price");
		$current_stock=$request->input("current_stock");
		$quantity=$request->input("quantity");
		$subtotal=$request->input("subtotal");
        
		$invoicedate=Carbon::createFromFormat('d/m/Y', $invoicedate)->toDateString();
		
		$TotalAmount_old=$Expenses->ExpensesInvoiceAmount;
		$PaidAmount_old=$Expenses->PaidAmount;
		$OutstandingAmount_old=$Expenses->OutstandingAmount;
		$PaymentStatus=$Expenses->PaymentStatus;
		
		if($invoiceamount<$PaidAmount_old){
            flash(__('Invoice Amount can not be less than paid amount or either remove the payment details.'))->error();
			return back();
		}
		
		$Expenses->ExpenseName = $expensename;
		$Expenses->ECategoryID = $categoryID;
		$Expenses->ExpenseInvoiceNo = $invoiceno;
		$Expenses->ExpenseInvoiceDate = $invoicedate;
		$Expenses->igst = $igst;
		$Expenses->cgst = $cgst;
		$Expenses->sgst = $sgst;
		$Expenses->ExpensesInvoiceAmount = $invoiceamount;
		$Expenses->save();
		
		$array = $this->Recaluclate($id);
			
		$PaymentStatus=$array['PaymentStatus'];
		$OutstandingAmount=$array['OutstandingAmount'];
		$PaidAmount=$array['PaidAmount'];
		
		$Expenses->PaidAmount =$PaidAmount;
		$Expenses->OutstandingAmount =$OutstandingAmount;
		$Expenses->PaymentStatus =$PaymentStatus;
		$Expenses->save();
        foreach($expenses_details_id as $key => $ed_id) {
            if($ed_id == 0) {
                $ExpensesDetails = new ExpensesDetails;
            } else {
                $ExpensesDetails = ExpensesDetails::find($ed_id);
            }
            $ExpensesDetails->expenses_id = $Expenses->ExpenseID;
            $ExpensesDetails->expenses_type = $expenses_types[$key];
            if($expenses_types[$key] == "Product Expenses") {
                $Product = \App\Product::where('id',$item_description[$key])->select('id', 'name','current_stock')->first();
                $Product->current_stock = $Product->current_stock - $current_stock[$key];
                $Product->current_stock = $Product->current_stock + $quantity[$key];
                $Product->save();
                $ExpensesDetails->category_id = $category_id[$key];
                $ExpensesDetails->product_id = $item_description[$key];
                $ExpensesDetails->item_description = $Product->name;
            } else {
                $ExpensesDetails->category_id = 0;
                $ExpensesDetails->product_id = 0;
                $ExpensesDetails->item_description = $item_description[$key];
            }
            $ExpensesDetails->unit_price = $unit_price[$key];
            $ExpensesDetails->quantity = $quantity[$key];
            $ExpensesDetails->sub_total = $subtotal[$key];
            $ExpensesDetails->save();
        }
        foreach($deleted_expenses_details_ids as $d) {
            $ExpensesDetails = ExpensesDetails::find($d)->delete();
        }
		flash(__('Expense updated successfully.'))->success();
		return back();
	}
    
	public function ProductPurchaseView(Request $request, $id){
		$ProductPurchase=ProductPurchase::find($id);
		$ProductPurchasePayment=ProductPurchasePayment::where('product_purchase_id',$id)->orderBy('payment_date')->get();
		$data = array("ProductPurchase"=>$ProductPurchase, "ProductPurchasePayment"=>$ProductPurchasePayment);
		return view('product_purchases.view',$data);
	}
    
	public function ExpenseDelete(Request $request){
		$id = $request->input('id');
		$Expenses = Expenses::find($id);
		$Expenses->delete();
		ExpensesPayments::where('ExpenseID',$id)->delete();
		ExpensesDetails::where('expenses_id',$id)->delete();
		flash(__('Expense has been deleted successfully.'))->success();
		return back(); 
	}
    
	public function ExpensePaymentAdd(){
		$Expenses = Expenses::where('OutstandingAmount','!=',0)->orderBy('ExpenseID')->get();
		$SelectedExpenseID=null;
		$data = array("Expenses" => $Expenses,"SelectedExpense"=>$SelectedExpenseID);
		return view('expenses.ExpensePaymentAdd', $data);
	}
    
	public function ExpensePaymentAddWithID(Request $request, $id){
		$Expenses = Expenses::where('OutstandingAmount','!=',0)->orderBy('ExpenseID')->get();
		$SelectedExpenseID=Expenses::find($id);
		$data = array("Expenses" => $Expenses,"SelectedExpense"=>$SelectedExpenseID);
		return view('expenses.ExpensePaymentAdd', $data);
	}
    
	public function ExpensePaymentSave(Request $request){
		$expenseID=$request->input("ExpenseID");
		$outstandingamount=$request->input("outstandingamount");
		$payingamount=$request->input("payingamount");
		$paymentdate=$request->input("paymentdate");
		$paymentmethod=$request->input("paymentmethod");
		$refID=$request->input("refID");
		$paymentdate=Carbon::createFromFormat('d/m/Y', $paymentdate)->toDateString();
		
		$ExpensesPayments = new ExpensesPayments;
		$ExpensesPayments->ExpenseID=$expenseID;
		$ExpensesPayments->ExpensesPaidAmount=$payingamount;
		$ExpensesPayments->ExpesnesPaidDate=$paymentdate;
		$ExpensesPayments->ExpensesPaymentMethod=$paymentmethod;
		$ExpensesPayments->ExpensesPaymentRefID=$refID;
		$ExpensesPayments->save();
		
		//get PaidAmount and OutstandingAmount From Expense Table
		$Expenses = Expenses::find($expenseID);
		$Paid_amount_old=$Expenses->PaidAmount;
		$balance_amount_old=$Expenses->OutstandingAmount;
		
		$newPaid=$Paid_amount_old+$payingamount;
		$newBalance=$balance_amount_old-$payingamount;
		
		if($newBalance==0) $Status="Paid" ; 
		else if($newPaid==0) $Status="Unpaid";
		else $Status="Partially Paid";
	
		$Expenses->PaidAmount=$newPaid;
		$Expenses->OutstandingAmount=$newBalance;
		$Expenses->PaymentStatus=$Status;
		$Expenses->save();
        if($Status == "Paid") {
            $vendor_id = $Expenses->vendor_id;
            $OrderDetail = \App\OrderDetail::where('seller_id',$vendor_id)->where('vendor_expenses_id', $Expenses->ExpenseID)->get();
  
            if(count($OrderDetail) > 0) {
                foreach($OrderDetail as $OD) {
                    $OD->vendor_status = "Invoice Paid";
                    $OD->save();
                }
                $array['view'] = 'emails.alert_vendor';
                $array['subject'] = 'Invoice Paid.';
                $array['from'] = env('MAIL_FROM_ADDRESS');
                $array['content'] = 'Hi. Your invoice has been paid. Kindly proccess the order for shipping.';
                Mail::to(\App\User::find($vendor_id)->email)->queue(new AlertEmailManager($array));
                $smsMsg = "Your invoice has been paid. Kindly proccess the order for shipping.";
                $smsMsg = rawurlencode($smsMsg);
                $to = "91".\App\User::find($vendor_id)->phone;
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        file_get_contents($SMS_URL);
                    }
                }
            }
        }
        flash(__('Expense Payment added successfully.'))->success();
		return redirect()->route('ExpensesView',['id'=>$expenseID]);
	}
    
	public function ExpensePaymentDelete(Request $request) {
		$id = $request->input('id'); 
		$ExpensesPayments = ExpensesPayments::find($id);
		$ExpenseID=$ExpensesPayments->ExpenseID;
		$ExpensesPayments->delete();
		
		$Expenses = Expenses::find($ExpenseID);
		$PaymentCaluclate = $this->Recaluclate($ExpenseID);
		
		$payment_Status=$PaymentCaluclate['PaymentStatus'];
		$OutstandingAmount=$PaymentCaluclate['OutstandingAmount'];
		
		$PaidAmount=$PaymentCaluclate['PaidAmount'];
		$Expenses->PaidAmount =$PaidAmount;
		$Expenses->OutstandingAmount =$OutstandingAmount;
		$Expenses->PaymentStatus =$payment_Status;
		$Expenses->save();
        flash(__('Payment has been deleted successfully.'))->success();
	    return back(); 
	}
    
	public function Category(){
		$ExpensesCategory=ExpensesCategory::paginate( env('RECORDPERPAGE', '10'));
		$data = array("categories"=>$ExpensesCategory);
		return view('expenses.ExpenseCategoryList',$data);
	}
    
	public function CreateCategory(){
		$data = array();
		return view('expenses.ExpenseCategoryAdd',$data);
	}
    
	public function SaveCategory(Request $request){
		$name=$request->input("name");
		$ExpensesCategory = new ExpensesCategory;
		$ExpensesCategory->ECategoryName = $name;
		$ExpensesCategory->save();
        flash(__('Category added successfully.'))->success();
		return back();
	}
    
	public function CategoryEditView(Request $request, $id){
		$ExpensesCategory=ExpensesCategory::find($id);
		$data = array("category"=>$ExpensesCategory);
		return view('expenses.ExpenseCategoryEdit',$data);
	}
    
	public function CategoryEditPost(Request $request, $id){
		$ExpensesCategory=ExpensesCategory::find($id);
		$name=$request->input("name");
		$ExpensesCategory->ECategoryName = $name;
		$ExpensesCategory->save();
        flash(__('Category Updated successfully.'))->success();
		return redirect('admin/expenses/category');
	}
    
	public function CategoryDelete(Request $request){
		$id = $request->input('id');
		$ExpensesCategory = ExpensesCategory::find($id);
		$ExpensesCategory->delete();
        flash(__('Category has been deleted successfully.'))->success();
		return redirect()->back(); 
	}
}
