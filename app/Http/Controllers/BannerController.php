<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use ImageOptimizer;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
use Illuminate\Support\Facades\Cache;
class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();
        return view('banners.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($position)
    {
        return view('banners.create', compact('position'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('photo')){
            $banner = new Banner;
           
            $banner->url = $request->url;
            $banner->position = $request->position;
            $banner->displayposition = $request->displayposition;
            
            $extension = $request->photo->extension();
			$main = Str::slug("Fullbanner","-").time().".".$extension;
			$img = Image::make($request->photo);
			$width = Image::make($request->photo)->width();
			$height = Image::make($request->photo)->height();
	
			if($request->position==3){
			    if($width == 1920 && $height == 400) {
			        $request->photo->move(base_path('public/')."temp/",$main);
			    } else {
        			if($width != 1920 && $height != 400){
        				$img->resizeCanvas(1920, 400, 'center', false, 'fff');
        				$img->save(base_path('public/')."temp/".$main);
        			}
			    }
			} else {
			    if($width == 450 && $height == 200) {
			        $request->photo->move(base_path('public/')."temp/",$main);
			    } else {
    				$img->resizeCanvas(450, 200, 'center', false, 'fff');
    				$img->save(base_path('public/')."temp/".$main);
    			} 
			}
			
			//ImageOptimizer::optimize(base_path('public/')."temp/".$main);
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$main, base_path('public/').'uploads/banners/'.$main);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/banners', new \Illuminate\Http\File(base_path('public/').'temp/'.$main), $main);
			}
			$bannername = "uploads/banners/".$main;
			unlink(base_path('public/').'temp/'.$main);
            $banner->photo=$bannername;
            
            $banner->save();
			
			Cache::forget('Banners');
			Cache::forget('Banners2');
			Cache::forget('Banners3');
			Cache::forget('Mobile_Banners');
			Cache::forget('Mobile_Banners2');
			Cache::forget('Mobile_Banners3');
            flash(__('Banner has been inserted successfully'))->success();
        }
        return redirect()->route('home_settings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return view('banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::find($id);

        if($request->hasFile('photo')){
			if($banner->photo!=null){
    			if(env('FILESYSTEM_DRIVER') == "local") {
                    if($banner->photo!=null)
                        unlink(base_path('public/').$banner->photo);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($banner->photo);
                }
			}
			
			$extension = $request->photo->extension();
			$main = Str::slug("Fullbanner","-").time().".".$extension;
			$img = Image::make($request->photo);
			$width = Image::make($request->photo)->width();
			$height = Image::make($request->photo)->height();
			if($banner->position==3){
			    if($width == 1920 && $height == 400) {
			        $request->photo->move(base_path('public/')."temp/",$main);
			    } else {
    			if($width <= 1920 && $height <= 400) {
					$img->resizeCanvas(1920, 400, 'center', false, 'fff');
				} else {
					 $img->resize(1920, 400, function ($constraint) {
						$constraint->aspectRatio();
					});
					$img->resizeCanvas(1920, 400, 'center', false, 'fff');
				}
				$img->save(base_path('public/')."temp/".$main, 100);
			    }
			}
			else{
			    if($width == 450 && $height == 200) {
			        $request->photo->move(base_path('public/')."temp/",$main);
			    } else {
        			if($width <= 450 && $height <= 200) {
    					$img->resizeCanvas(450, 200, 'center', false, 'fff');
    				} else {
    					 $img->resize(450, 200, function ($constraint) {
    						$constraint->aspectRatio();
    					});
    					$img->resizeCanvas(450, 200, 'center', false, 'fff');
    				}
    				$img->save(base_path('public/')."temp/".$main, 100);
			    }
			}
			//ImageOptimizer::optimize(base_path('public/')."temp/".$main);
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$main, base_path('public/').'uploads/banners/'.$main);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/banners', new \Illuminate\Http\File(base_path('public/').'temp/'.$main), $main);
			}
			$bannername = "uploads/banners/".$main;
			unlink(base_path('public/').'temp/'.$main);

            $banner->photo = $bannername;
        }
		
        $banner->url = $request->url;
        $banner->displayposition = $request->displayposition;
        $banner->save();
		Cache::forget('Banners');
		Cache::forget('Banners2');
		Cache::forget('Banners3');
		Cache::forget('Mobile_Banners');
		Cache::forget('Mobile_Banners2');
		Cache::forget('Mobile_Banners3');
        flash(__('Banner has been updated successfully'))->success();
        return redirect()->route('home_settings.index');
    }


    public function update_status(Request $request)
    {
        $banner = Banner::find($request->id);
		$position= $banner->position;
		$displayposition= $banner->displayposition;
		
		if($displayposition=="Mobile"){
		/*	foreach (Banner::where('displayposition',"Mobile")->where('position',$position)->get() as $key => $bannero) {
				$bannero->published = 0;
				$bannero->save();
			}*/
			$banner->published = $request->status;
			if($banner->save()){
				Cache::forget('Mobile_Banners');
				Cache::forget('Mobile_Banners2');
				Cache::forget('Mobile_Banners3');
				return '1';
			}
			else {
				return '0';
			}
		}
		else{
			$banner->published = $request->status;
			/*if($request->status == 1){
				if(count(Banner::where('published', 1)->where('position', $position)->get()) < 4)
				{
					if($banner->save()){
						Cache::forget('Banners');
						Cache::forget('Banners2');
						Cache::forget('Banners3');
						return '1';
					}
					else {
						return '0';
					}
				}
			}
			else{
			*/
				if($banner->save()){
					Cache::forget('Banners');
					Cache::forget('Banners2');
					Cache::forget('Banners3');
					return '1';
				}
				else {
					return '0';
				}
		//	}
		}

        return '0';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        if(Banner::destroy($id)){
			if($banner->photo!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$banner->photo))
                    unlink(base_path('public/').$banner->photo);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($banner->photo);
                }
			}
			Cache::forget('Banners');
			Cache::forget('Banners2');
			Cache::forget('Banners3');
			Cache::forget('Mobile_Banners');
			Cache::forget('Mobile_Banners2');
			Cache::forget('Mobile_Banners3');
            flash(__('Banner has been deleted successfully'))->success();
        }
        else{
            flash(__('Something went wrong'))->error();
        }
        return redirect()->route('home_settings.index');
    }
}
