<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductStock;
use App\Category;
use App\Language;
use Auth;
use App\SubSubCategory;
use App\User;
use App\Brand;
use Session;
use ImageOptimizer;
use DB;
use Image;
use File;
use Excel;
use App\ProductsCatExport;
use Illuminate\Support\Str;
use Storage;
use Illuminate\Support\Facades\Cache;

class ProductStockController extends Controller
{
public function index(Request $request)
    {
	   $categories=Category::get();
	   $user=User::whereIn('user_type',['seller','vendor'])->get();
        return view('products.stock',compact('categories'),compact('user'));
    }

	public function export_index(Request $request)
    {
	   $categories=Category::get();
	  $brands=Brand::get();
        return view('products.categoryexport',compact('categories'),compact('brands'));
    }

	public function cat_export(Request $request){
		$data=array('marg'=>$request->marg,'brand_id'=>$request->brand_id,'category_id'=>$request->category_id,'subcategory_id'=>$request->subcategory_id,'subsubcategory_id'=>$request->subsubcategory_id);
		return Excel::download(new ProductsCatExport($data), 'products.xlsx');
    }
	
	public function stock_qty(Request $request){
		$validatedData = $request->validate([
		'category_id' => 'required',
		'subcategory_id' => 'required',
		'subsubcategory_id' => 'required',
		'user_type' => 'required',
		]);
		
		if( $request->subsubcategory_id != '' && $request->user_type != '' ) {
		    $subsubcategory_id = $request->subsubcategory_id;
		    $subcategory_id = $request->subcategory_id;
		    $category_id = $request->category_id;
		    $qty=$request->qty;
    		if ($request->user_type=='in_house') {
    			 $user_id=User::whereNotIn('user_type',['seller','vendor','customer'])->get()->pluck('id')->all();
    		} else {
    			$user_id=array($request->user_type);
    		}
    		$product = Product::where('category_id',$category_id);
    		
    		if($subcategory_id != "all_sub_cat") {
    		    $product = $product->where('subcategory_id',$subcategory_id);
    		    if($subsubcategory_id != 'all_sub_sub_cat' ) {
    		        $product = $product->where('subsubcategory_id',$subsubcategory_id);
    		    }
    		}
    		
    		if ($request->user_type != 'all_user' ) {
    			$product = $product->whereIn('user_id',$user_id);
    		}
    		$product = $product->update(['current_stock' => $qty]);
    	
    		flash(__('Stock has been updated successfully'))->success();
    		
		} else {
		    flash('Please select one user type and sub category')->error();
		}
		return back();
    }
    public function get_product_by_subsubcategory(Request $request)
    {
        $product = Product::where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->where('subsubcategory_id', $request->subsubcategory_id)->get();
        return $product;
    }
}