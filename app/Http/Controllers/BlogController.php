<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use File;
use App\Blog;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Storage;

class BlogController extends Controller
{
 
	public function index(Request $request)
	{   
		$sort_search = null;
        $Blog = Blog::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $Blog = $Blog->where('BlogTitle', 'like', '%'.$sort_search.'%');
        }
        $Blogs = $Blog->paginate(15);
        return view('Blog.index', compact('Blogs', 'sort_search'));
    }
	public function create()
    {
        return view('Blog.create');
    }
	public function store(Request $request)
    {	$validatedData = $request->validate([
			'BlogTitle' => 'required|min:4|max:500',
			'BlogSummary' => 'required|min:4|max:1000',
			'BlogContent' => 'required|min:50',
			'SEO_metatags' => 'max:300',
			'SEO_metaDescription' => 'max:500'
		]);
		if ($request->input('URLSlug') == null) {
            $URLSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->input('BlogTitle')));
        }
        else{
            $URLSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->input('URLSlug')));
        }
		
		$BlogCount = Blog::where('URLSlug', $URLSlug)->count();
		if($BlogCount > 0)
		{
			flash(__('Blog Slug already in use, Use Different slug for this Blog.'))->error();
            return back();
		}
		$Blog = new Blog;
		$Blog->BlogTitle = $request->input('BlogTitle');
		$Blog->BlogSummary = $request->input('BlogSummary');
		$Blog->BlogContent = $request->input('BlogContent');
		$Blog->SEO_metatags = $request->input('SEO_metatags');
		$Blog->SEO_metaDescription = $request->input('SEO_metaDescription');
		$Blog->URLSlug = $URLSlug;
		
		if($request->hasFile('FeaturedImage')){
            $extension = $request->FeaturedImage->extension();
            $thumbnail = $URLSlug."_".time().".".$extension;
            $img = Image::make($request->FeaturedImage);
			$width = Image::make($request->FeaturedImage)->width();
			$height = Image::make($request->FeaturedImage)->height();
			
			if($width != 1200 && $height != 600){
				$img->resizeCanvas(1200, 600, 'center', false, 'fff');
			}
            $img->save(base_path('public/')."temp/".$thumbnail);
           // ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);
			if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/blog/'.$thumbnail);
			} else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/blog', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
            }
            $Blog->FeaturedImage = "uploads/blog/".$thumbnail;
            unlink(base_path('public/').'temp/'.$thumbnail);
        }
		if($Blog->save()){
			Cache::forget('Blog');
			flash(__('Blog added successfully'))->success();
			return redirect()->route('blog.index');
		}
		flash(__('Something went wrong'))->error();
        return back();
		
	}
	
	public function EditorUploadImg(Request $request)
    {
        if($request->hasFile('upload')) {
			$originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
			$img_path = 'uploads/blog';
            //$request->file('upload')->move($img_path, $fileName);
             
		
			$thumbnail = $fileName;
			$img = Image::make($request->upload);
			
			$img->save(base_path('public/')."temp/".$thumbnail);
			
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/blog/'.$thumbnail);
				
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/blog', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
			}
			$url = asset($img_path."/".$fileName);
			unlink(base_path('public/').'temp/'.$thumbnail);
			
			$CKEditorFuncNum = $request->input('CKEditorFuncNum');
			
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }
	
	public function edit($id)
	{
		$Blog = Blog::find($id);
		$data = array("Blog" => $Blog);
		return view('Blog.edit', $data);
	}
	 
	public function update(Request $request, $id)
	{
	    $validatedData = $request->validate([
			'BlogTitle' => 'required|min:4|max:500',
			'BlogSummary' => 'required|min:4|max:1000',
			'BlogContent' => 'required|min:50',
			'SEO_metatags' => 'max:300',
			'SEO_metaDescription' => 'max:500'
		]);
		if ($request->input('URLSlug') == null) {
            $URLSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->input('BlogTitle')));
        }
        else{
            $URLSlug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->input('URLSlug')));
        }
		$count = Blog::where('id', '!=', $id)->where('URLSlug', $URLSlug)->count();
		if($count > 0)
		{
			flash(__('Blog Slug already in use, Use Different slug for this Blog.'))->error();
            return back(); 
		}
		$Blog = Blog::find($id);
		$Blog->BlogTitle = $request->input('BlogTitle');
		$Blog->BlogSummary = $request->input('BlogSummary');
		$Blog->BlogContent = $request->input('BlogContent');
		$Blog->SEO_metatags = $request->input('SEO_metatags');
		$Blog->SEO_metaDescription = $request->input('SEO_metaDescription');
		$Blog->URLSlug = $URLSlug;
		if($request->hasFile('FeaturedImage')){
            $extension = $request->FeaturedImage->extension();
            $thumbnail = $URLSlug."_".time().".".$extension;
            $img = Image::make($request->FeaturedImage);
			$width = Image::make($request->FeaturedImage)->width();
			$height = Image::make($request->FeaturedImage)->height();
			
			if($width != 1200 && $height != 600){
				$img->resizeCanvas(1200, 600, 'center', false, 'fff');
			}
            $img->save(base_path('public/')."temp/".$thumbnail);
           // ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);

           if(env('FILESYSTEM_DRIVER') == "local") {
                copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/blog/'.$thumbnail);
            } else {
                Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/blog', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
            }
			
			$oldImg = $Blog->FeaturedImage;
			if(!empty($oldImg))
			{
				if(File::exists($oldImg)) {
				   unlink($oldImg);
				}
			}
            $Blog->FeaturedImage = "uploads/blog/".$thumbnail;
            unlink(base_path('public/').'temp/'.$thumbnail);
        }
		
		if($Blog->save()){
			Cache::forget('Blog');
			flash(__('Blog details update successfully'))->success();
			return redirect()->route('blog.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
	}
	public function destroy($id)
    {
		$Blog = Blog::find($id);
		if(File::exists($Blog->FeaturedImage)) {
			if(env('FILESYSTEM_DRIVER') == "local") {
				if(file_exists(base_path('public/').$Blog->FeaturedImage))
				unlink(base_path('public/').$Blog->FeaturedImage);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->delete($Blog->FeaturedImage);
			}
			$Blog->FeaturedImage = null;
		}
		if($Blog->delete()){
			Cache::forget('Blog');
			flash(__('Blog has been deleted successfully'))->success();
			return redirect()->route('blog.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
	}
	
	 
}
