<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Seller;
use App\User;
use App\Shop;
use App\Product;
use App\Order;
use App\OrderDetail;
use App\SellerCommision;
use App\Category;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Storage;
use DB;
use Image;
use File;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
	   
        $sort_search = null;
        $approved = null;
        $sellers = Seller::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $user_ids = User::where('user_type', 'seller')->where(function($user) use ($sort_search){
                $user->where('name', 'like', '%'.$sort_search.'%')->orWhere('email', 'like', '%'.$sort_search.'%');
            })->pluck('id')->toArray();
            $sellers = $sellers->where(function($seller) use ($user_ids){
                $seller->whereIn('user_id', $user_ids);
            });
        }
        if ($request->approved_status != null) {
            $approved = $request->approved_status;
            $sellers = $sellers->where('verification_status', $approved);
        }
        
        $sellers = $sellers->paginate(15);
        return view('sellers.index', compact('sellers', 'sort_search', 'approved'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('sellers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validatedData = $request->validate([
			'name' => 'required|min:3|max:191',
			'password' => 'required|min:8|max:191',
			'email' => 'email:rfc,dns'
			
		]);
        if(User::where('email', $request->email)->first() != null){
            flash(__('Email already exists!'))->error();
            return back();
        }
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->user_type = "seller";
        $user->password = Hash::make($request->password);
        $user->email_verified_at = date('Y-m-d H:m:s');
        
        if($user->save()){
            $seller = new Seller;
            $seller->user_id = $user->id;
            if($seller->save()){
                $Shop = new Shop;
                $Shop->user_id = $user->id;
                $Shop->slug = 'shop-'.$user->id;
                $Shop->name = $request->name;
                $Shop->contact_person_name = $request->contact_person_name;
                $Shop->contact_person_phone = $request->contact_person_phone;
                $Shop->address = $request->address;
                $Shop->landmark = $request->landmark;
                $Shop->city = $request->city;
                $Shop->state = $request->state;
                $Shop->pincode = $request->pincode;
                $Shop->firm_type = $request->firm_type;
                $Shop->gstin = $request->gstin;
                $Shop->trade_license_no = $request->trade_license_no;
                $Shop->business_registration_in = $request->business_registration_in;
                $Shop->meta_title = $request->meta_title;
                $Shop->meta_description = $request->meta_description;
                /**********ADD SELLER IN ALIGN BOOK*****************************/
                
                /********************END SELLER IN ALIGN BOOK*********************/
                $Shop->save();
                flash(__('Seller has been inserted successfully'))->success();
                return redirect()->route('sellers.index');
            }
        }

        flash(__('Something went wrong'))->error();
        return back();
    }
    
    public function addUpdateSellerInAlignBook($requestJson,$endPoint){
	  
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
        $data = json_decode($response1);
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {    
        $seller = Seller::findOrFail(decrypt($id));
       
        return view('sellers.edit', compact('seller'));
    }
    public function seller_commission()
    {
        $categories = Category::all();
        $sellerCommission = SellerCommision::all();
        return view('sellers.seller_commission',compact('categories','sellerCommission'));
    }
    public function commission_view()
    {
        
        return view('sellers.commission_view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        $validatedData = $request->validate([
			'name' => 'required|min:3|max:191',
			'email' => 'email:rfc,dns',
			'contact_person_name' => 'max:191',
			'contact_person_phone' => 'digits:10',
			'address' => 'max:500',
			'city' => 'max:100',
			'state' => 'max:100',
			'pincode' => 'digits:6',
			'firm_type' => 'max:50',
			'gstin' => 'size:15',
			'trade_license_no' => 'max:50',
			'business_registration_in' => 'max:50',
			'meta_title' => 'max:191',
			'meta_description' => 'max:600'
		]);
        $seller = Seller::findOrFail($id);
        $seller->order_cancel_credit = $request->order_cancel_credit;
        $user = $seller->user;
        $user->name = $request->name;
        $user->email = $request->email;
        if(strlen($request->password) > 0){
            $user->password = Hash::make($request->password);
        }
        $Shop = Shop::where('user_id', $user->id)->first();
        $Shop->name = $request->name;
        $Shop->contact_person_name = $request->contact_person_name;
        $Shop->contact_person_phone = $request->contact_person_phone;
        $Shop->address = $request->address;
        $Shop->landmark = $request->landmark;
        $Shop->city = $request->city;
        $Shop->state = $request->state;
        $Shop->pincode = $request->pincode;
        $Shop->firm_type = $request->firm_type;
        $Shop->gstin = $request->gstin;
        $Shop->trade_license_no = $request->trade_license_no;
        $Shop->business_registration_in = $request->business_registration_in;
        $Shop->meta_title = $request->meta_title;
        $Shop->meta_description = $request->meta_description;
        $Shop->save();
        if($user->save()){
            if($seller->save()){
                flash(__('Seller has been updated successfully'))->success();
                return back();
            }
        }

        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seller = Seller::findOrFail($id);
        Shop::where('user_id', $seller->user->id)->delete();
        Product::where('user_id', $seller->user->id)->delete();
        Order::where('user_id', $seller->user->id)->delete();
        OrderDetail::where('seller_id', $seller->user->id)->delete();
        User::destroy($seller->user->id);
        if(Seller::destroy($id)){
            flash(__('Seller has been deleted successfully'))->success();
            return redirect()->route('sellers.index');
        }
        else {
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function show_verification_request($id)
    {
        $seller = Seller::findOrFail($id);
        return view('sellers.verification', compact('seller'));
    }

    public function approve_seller($id)
    {
        $seller = Seller::findOrFail($id);
        $seller->verification_status = 1;
        if($seller->save()){
            flash(__('Seller has been approved successfully'))->success();
            return redirect()->route('sellers.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
    }

    public function reject_seller($id)
    {
        $seller = Seller::findOrFail($id);
        $seller->verification_status = 0;
        $seller->verification_info = null;
        if($seller->save()){
            flash(__('Seller verification request has been rejected successfully'))->success();
            return redirect()->route('sellers.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
    }


    public function payment_modal(Request $request)
    {
        $seller = Seller::findOrFail($request->id);
		
        return view('sellers.payment_modal', compact('seller'));
    }

    public function profile_modal(Request $request)
    {
        $seller = Seller::findOrFail($request->id);
        return view('sellers.profile_modal', compact('seller'));
    }

    public function updateApproved(Request $request)
    {
        $seller = Seller::findOrFail($request->id);
        $seller->verification_status = $request->status;
        if($seller->save()){
            return 1;
        }
        return 0;
    }
    
    //add seller commission
    public function addSellerCommission(Request $request){
        
       $checkRecord = SellerCommision::where(['user_id'=>$request->sellerId,'category_id'=>$request->category_id,'subcategory_id'=>$request->subcategory_id,'subsubcategory_id'=>$request->subsubcategory_id])->get();
      
      $count = count($checkRecord); 
      if(empty($request->sellerId)){
          flash(__('Please select seller  '))->error();
           return back();
      }
      if(empty($request->category_id)){
          flash(__('Please select Category  '))->error();
           return back();
      }
      if(empty($request->subcategory_id)){
          flash(__('Please select Subcategory  '))->error();
           return back();
      }
       
       if(empty($request->subsubcategory_id)){
          flash(__('Please select Sub Subcategory  '))->error();
           return back();
      }

       if($count == 0){
           
           $sellerCommission = new SellerCommision;
           $sellerCommission->commission_type = $request->commissionType;
           $sellerCommission->category_id = $request->category_id;
           $sellerCommission->subcategory_id = $request->subcategory_id;
           $sellerCommission->subsubcategory_id =  $request->subsubcategory_id;
           if($request->commision_choose == 'r' && $request->commision_rupease!=''){
                      //dd( $request->commision_rupease);
             $sellerCommission->commission =  $request->commision_rupease;    
           }else{
               $sellerCommission->commission =  $request->commission_percent;
           }
           
           $sellerCommission->user_id =  $request->sellerId;
           $sellerCommission->commision_choose =  $request->commision_choose;
           $sellerCommission->save();
           flash(__('Seller Commission has been save '))->success();
           return back();
       }else{
           flash(__('This category allready added with this seller try another one'))->error();
           return back();
       }
       
    }
    
    //Edit sellerComission
    
     public function editSellerCommission(Request $request){
       //$checkRecord = SellerCommision::where(['user_id'=>$request->sellerId,'category_id'=>$request->category_id])->get();
       //$count = count($checkRecord); 
       
       $sellerCommission = SellerCommision::findOrFail($request->commissionId);
       $sellerCommission->commission_type = $request->commissionType;
       $sellerCommission->category_id = $request->category_id;
       $sellerCommission->subcategory_id = $request->subcategory_id;
       $sellerCommission->subsubcategory_id =  $request->subsubcategory_id;
       if($request->commision_choose == 'r' && $request->commision_rupease!=''){
                      //dd( $request->commision_rupease);
             $sellerCommission->commission =  $request->commision_rupease;    
           }else{
               $sellerCommission->commission =  $request->commission_percent;
           }
       //$sellerCommission->commission =  $request->commission;
       $sellerCommission->user_id =  $request->sellerId;
       $sellerCommission->commision_choose =  $request->commision_choose;
       $sellerCommission->save();
       flash(__('Seller Commission has been updated successfully '))->success();
       return redirect ('admin/seller/sellercommission');
    }
    
    
    
    public function deleteSellerCommisionConfig(Request $request){
        
        $configcomissionid = $request->sellerId;
         
        if(SellerCommision::destroy($configcomissionid)){
            
            flash(__('Seller configuration has been deleted successfully'))->success();
            
        }
        else {
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
    
    public function showComission($id){
       
        $sellerComissionDetails = SellerCommision::findOrFail(decrypt($id));
        $categories = Category::all();
        $sellerCommission = SellerCommision::all();
        return view('sellers.seller_commission_edit',compact('categories','sellerCommission','sellerComissionDetails'));
        
    }
    
    
    public function showCategory(Request $request){
        //dd(Auth::user()->id);
        if(Auth::user()->id){
             $sellerId = Auth::user()->id;      
        }else{
          return redirect()->route('sellers.index');
        }
      
       $sort_search = null;
       $categories = Category::where('seller_id',$sellerId)->orderBy('position');
        if ($request->has('search')){
            $sort_search = $request->search;
            $categories = $categories->where('name', 'like', '%'.$sort_search.'%');
        }
        $categories = $categories->paginate(50);
       // return view('categories.index', compact('categories', 'sort_search'));
        return view("frontend.seller.category.index",compact('categories','sort_search'));
    }
    
    public function showSubcategory(Request $request){
        dd('sub kkkk');
    }
    
    public function showSubSubcategory(Request $request){
        
        //dd('sub sub kkkk');
    }
    
    
     public function addCategory(Request $request){
        
       
        return view('frontend.seller.category.create');
    }
    
    public function saveCategory(Request $request){
        if(Auth::user()->id){
             $sellerId = Auth::user()->id;      
        }else{
          return redirect()->route('sellers.index');
        }
        $validatedData = $request->validate([
			'name' => 'required|string|min:4|max:50',
	        'meta_title' => 'max:255',
			'category_discount' => 'required|int|gte:0',
			'policy_heading' => 'max:255',
			'bg_color' => 'max:100',
			'text_color' => 'max:100',
			'meta_keywords'=> 'max:1000',
		]);
        $catname= $request->name;
        $catname=ucwords(strtolower($catname));
		//check category in db
		$count=Category::where('name',$catname)->count();
		if($count!=0){
			flash(__('Category Already Exists'))->error();
            return back();
		}
		
        $category = new Category;
		$category->name = $catname;
		$category->display = $request->display;
		$category->background_color = $request->bg_color;
        $category->text_color = $request->text_color;
        $category->return_days = $request->return_days;
        $category->seller_id = $sellerId;
        
        $top_sku = $request->top_sku;
        if($top_sku == "") {
            $top_sku = array();
        } else {
            $top_sku = explode(",", $top_sku);
            foreach($top_sku as $key => $sku) {
                $top_sku[$key] = trim($top_sku[$key]);
            }
        }
        
        $category->top_sku = json_encode($top_sku);
        
		if($request->meta_title==""){
		    $category->meta_title = $catname.": Buy ".$catname." products online at best prices in India - Aldebazaar.com";
		}
		else{
		    $category->meta_title =$request->meta_title;
		}
		if($request->meta_description==""){
		   $category->meta_description = "Buy ".$catname." products online at best prices from Aldebazaar.com. Check out ".$catname." products from top brands at Aldebazaar.com.";
		
		}
		else{
		    $category->meta_description =$request->meta_description;
		}
         $category->meta_keywords= implode('|',$request->meta_keywords);
        $category->position = $request->position;
       $category->policy_heading = $request->policy_heading;
		$category->policy_content = $request->policy_content;
        $category->content = $request->category_content;
		$category->discount = $request->category_discount;
        if ($request->slug != null) {
            $category->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->slug)));
        }
        else {
            $category->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', ucwords(strtolower($request->name)))));
        }
        
        $category->slug = str_ireplace('--','-', $category->slug);
        
        if ($request->commision_rate != null) {
            $category->commision_rate = $request->commision_rate;
        }


		if($request->hasFile('menu_banner')){
			$extension = $request->menu_banner->extension();
			$MenuBanner = Str::slug($catname,"-")."MenuBanner".time().".".$extension;
			$img = Image::make($request->menu_banner);
			$width = Image::make($request->menu_banner)->width();
			$height = Image::make($request->menu_banner)->height();
			
			if($width == 490 && $height == 550) {
    			 $request->menu_banner->move(base_path('public/')."temp/",$MenuBanner);
    		} else {
        		$img->resizeCanvas(490, 550, 'center', false, 'fff');
        		$img->save(base_path('public/')."temp/".$MenuBanner);
    		}
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$MenuBanner, base_path('public/').'uploads/categories/menubanner/'.$MenuBanner);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/menubanner', new \Illuminate\Http\File(base_path('public/').'temp/'.$MenuBanner), $MenuBanner);
			}
			$category->menu_banner = "uploads/categories/menubanner/".$MenuBanner;
			unlink(base_path('public/').'temp/'.$MenuBanner);
		}


		// if($request->hasFile('menu_banner')){
			// ImageOptimizer::optimize($request->menu_banner);
            // $category->menu_banner = $request->menu_banner->store('uploads/categories/menubanner');
        // }

		// if($request->hasFile('banner')){
			// ImageOptimizer::optimize($request->banner);
            // $category->banner = $request->file('banner')->store('uploads/categories/banner');
        // }
		
		if($request->hasFile('banner')){
			$extension = $request->banner->extension();
			$banner = Str::slug($catname,"-")."banner".time().".".$extension;
			$img = Image::make($request->banner);
			$width = Image::make($request->banner)->width();
			$height = Image::make($request->banner)->height();
			if($width == 197 && $height == 186) {
    			 $request->banner->move(base_path('public/')."temp/",$banner);
    		} else {
        		$img->resizeCanvas(197, 186, 'center', false, 'fff');
        		$img->save(base_path('public/')."temp/".$banner);
    		}
    		
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$banner, base_path('public/').'uploads/categories/banner/'.$banner);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/banner', new \Illuminate\Http\File(base_path('public/').'temp/'.$banner), $banner);
			}
			$category->banner = "uploads/categories/banner/".$banner;
			unlink(base_path('public/').'temp/'.$banner);
		}
		
        // if($request->hasFile('icon')){
            // $category->icon = $request->file('icon')->store('uploads/categories/icon');
        // }
		
		if($request->hasFile('icon')){
			$extension = $request->icon->extension();
			$icon = Str::slug($catname,"-")."icon".time().".".$extension;
			$img = Image::make($request->icon);
			$width = Image::make($request->icon)->width();
			$height = Image::make($request->icon)->height();
			
			if($width == 197 && $height == 186) {
    			 $request->icon->move(base_path('public/')."temp/",$icon);
    		} else {
        		$img->resizeCanvas(197, 186, 'center', false, 'fff');
        		$img->save(base_path('public/')."temp/".$icon);
    		}
    		
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$icon, base_path('public/').'uploads/categories/icon/'.$icon);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/icon', new \Illuminate\Http\File(base_path('public/').'temp/'.$icon), $icon);
			}
			$category->icon = "uploads/categories/icon/".$icon;
			unlink(base_path('public/').'temp/'.$icon);
		}
	
		if($request->hasFile('bg_img')){
            $extension = strtolower($request->bg_img->extension());
            if($extension == "jpg" or $extension == "png" or $extension == "gif" or  $extension == "jpeg")
			{
                $thumbnail = Str::slug($catname,"-")."back".time().".".$extension;
                $img = Image::make($request->bg_img);
    			$width = $img->width();
    			$height = $img->height();
    			if($width == 222 && $height == 250) {
    			    $request->bg_img->move(base_path('public/')."temp/",$thumbnail);
    			} else {
        			if($width <= 222 && $height <= 250) {
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			} else {
        				 $img->resize(222, 250, function ($constraint) {
        					$constraint->aspectRatio();
        				});
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			}
                    $img->save(base_path('public/')."temp/".$thumbnail, 100);
    			}
                if(env('FILESYSTEM_DRIVER') == "local") {
                    copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/categories/background/'.$thumbnail);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/background/', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
                }
                $category->background_img = "uploads/categories/background/".$thumbnail;
                unlink(base_path('public/').'temp/'.$thumbnail);
			}
        }
		
        $category->digital = $request->digital;
        if($category->save()){
			Cache::forget('Categories');
            flash(__('Category has been inserted successfully'))->success();
            return redirect()->route('categories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
    
    //checkemail exist or not
    public function checkEmail(Request $request){
       //$email = User::where('email',$request->email)->first();
     
        if(User::where('email',$request->email)->first() != null){
             $data = [
               'status'=>'success',
               'status_code'=>'500',
               'msg'=>'This Email id already exist'];
        }else{
          $data = [
              'status'=>'success',
              'status_code'=>'200',
              'msg'=>'email id not exist'];
        }
        echo json_encode($data);
    }

}
