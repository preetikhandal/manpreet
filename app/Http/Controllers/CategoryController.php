<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\HomeCategory;
use App\Product;
use App\Language;
use ImageOptimizer;
use Illuminate\Support\Facades\Cache;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search = null;
       $categories = Category::orderBy('position');
        if ($request->has('search')){
            $sort_search = $request->search;
            $categories = $categories->where('name', 'like', '%'.$sort_search.'%');
        }
        $categories = $categories->paginate(50);
        return view('categories.index', compact('categories', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
			'name' => 'required|string|min:4|max:50',
	        'meta_title' => 'max:255',
			'category_discount' => 'required|int',
			'policy_heading' => 'max:255',
			'bg_color' => 'max:100',
			'text_color' => 'max:100',
			'meta_keywords'=> 'max:1000',
		]);
        $catname= $request->name;
        $catname=ucwords(strtolower($catname));
		//check category in db
		$count=Category::where('name',$catname)->count();
		if($count!=0){
			flash(__('Category Already Exists'))->error();
            return back();
		}
		
        $category = new Category;
		$category->name = $catname;
		$category->display = $request->display;
		$category->background_color = $request->bg_color;
        $category->text_color = $request->text_color;
        $category->return_days = $request->return_days;
        $top_sku = $request->top_sku;
        if($top_sku == "") {
            $top_sku = array();
        } else {
            $top_sku = explode(",", $top_sku);
            foreach($top_sku as $key => $sku) {
                $top_sku[$key] = trim($top_sku[$key]);
            }
        }
        
        $category->top_sku = json_encode($top_sku);
        
		if($request->meta_title==""){
		    $category->meta_title = $catname.": Buy ".$catname." products online at best prices in India - Aldebazaar.com";
		}
		else{
		    $category->meta_title =$request->meta_title;
		}
		if($request->meta_description==""){
		   $category->meta_description = "Buy ".$catname." products online at best prices from Aldebazaar.com. Check out ".$catname." products from top brands at Aldebazaar.com.";
		
		}
		else{
		    $category->meta_description =$request->meta_description;
		}
         $category->meta_keywords= implode('|',$request->meta_keywords);
        $category->position = $request->position;
       $category->policy_heading = $request->policy_heading;
		$category->policy_content = $request->policy_content;
        $category->content = $request->category_content;
		$category->discount = $request->category_discount;
        if ($request->slug != null) {
            $category->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->slug)));
        }
        else {
            $category->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', ucwords(strtolower($request->name)))));
        }
        
        $category->slug = str_ireplace('--','-', $category->slug);
        
        if ($request->commision_rate != null) {
            $category->commision_rate = $request->commision_rate;
        }


		if($request->hasFile('menu_banner')){
			$extension = $request->menu_banner->extension();
			$MenuBanner = Str::slug($catname,"-")."MenuBanner".time().".".$extension;
			$img = Image::make($request->menu_banner);
			$width = Image::make($request->menu_banner)->width();
			$height = Image::make($request->menu_banner)->height();
			
			if($width == 490 && $height == 550) {
    			 $request->menu_banner->move(base_path('public/')."temp/",$MenuBanner);
    		} else {
        		$img->resizeCanvas(490, 550, 'center', false, 'fff');
        		$img->save(base_path('public/')."temp/".$MenuBanner);
    		}
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$MenuBanner, base_path('public/').'uploads/categories/menubanner/'.$MenuBanner);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/menubanner', new \Illuminate\Http\File(base_path('public/').'temp/'.$MenuBanner), $MenuBanner);
			}
			$category->menu_banner = "uploads/categories/menubanner/".$MenuBanner;
			unlink(base_path('public/').'temp/'.$MenuBanner);
		}


		// if($request->hasFile('menu_banner')){
			// ImageOptimizer::optimize($request->menu_banner);
            // $category->menu_banner = $request->menu_banner->store('uploads/categories/menubanner');
        // }

		// if($request->hasFile('banner')){
			// ImageOptimizer::optimize($request->banner);
            // $category->banner = $request->file('banner')->store('uploads/categories/banner');
        // }
		
		if($request->hasFile('banner')){
			$extension = $request->banner->extension();
			$banner = Str::slug($catname,"-")."banner".time().".".$extension;
			$img = Image::make($request->banner);
			$width = Image::make($request->banner)->width();
			$height = Image::make($request->banner)->height();
			if($width == 197 && $height == 186) {
    			 $request->banner->move(base_path('public/')."temp/",$banner);
    		} else {
        		$img->resizeCanvas(197, 186, 'center', false, 'fff');
        		$img->save(base_path('public/')."temp/".$banner);
    		}
    		
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$banner, base_path('public/').'uploads/categories/banner/'.$banner);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/banner', new \Illuminate\Http\File(base_path('public/').'temp/'.$banner), $banner);
			}
			$category->banner = "uploads/categories/banner/".$banner;
			unlink(base_path('public/').'temp/'.$banner);
		}
		
        // if($request->hasFile('icon')){
            // $category->icon = $request->file('icon')->store('uploads/categories/icon');
        // }
		
		if($request->hasFile('icon')){
			$extension = $request->icon->extension();
			$icon = Str::slug($catname,"-")."icon".time().".".$extension;
			$img = Image::make($request->icon);
			$width = Image::make($request->icon)->width();
			$height = Image::make($request->icon)->height();
			
			if($width == 197 && $height == 186) {
    			 $request->icon->move(base_path('public/')."temp/",$icon);
    		} else {
        		$img->resizeCanvas(197, 186, 'center', false, 'fff');
        		$img->save(base_path('public/')."temp/".$icon);
    		}
    		
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$icon, base_path('public/').'uploads/categories/icon/'.$icon);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/icon', new \Illuminate\Http\File(base_path('public/').'temp/'.$icon), $icon);
			}
			$category->icon = "uploads/categories/icon/".$icon;
			unlink(base_path('public/').'temp/'.$icon);
		}
	
		if($request->hasFile('bg_img')){
            $extension = strtolower($request->bg_img->extension());
            if($extension == "jpg" or $extension == "png" or $extension == "gif" or  $extension == "jpeg")
			{
                $thumbnail = Str::slug($catname,"-")."back".time().".".$extension;
                $img = Image::make($request->bg_img);
    			$width = $img->width();
    			$height = $img->height();
    			if($width == 222 && $height == 250) {
    			    $request->bg_img->move(base_path('public/')."temp/",$thumbnail);
    			} else {
        			if($width <= 222 && $height <= 250) {
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			} else {
        				 $img->resize(222, 250, function ($constraint) {
        					$constraint->aspectRatio();
        				});
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			}
                    $img->save(base_path('public/')."temp/".$thumbnail, 100);
    			}
                if(env('FILESYSTEM_DRIVER') == "local") {
                    copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/categories/background/'.$thumbnail);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/background/', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
                }
                $category->background_img = "uploads/categories/background/".$thumbnail;
                unlink(base_path('public/').'temp/'.$thumbnail);
			}
        }
		
        $category->digital = $request->digital;
        if($category->save()){
			Cache::forget('Categories');
            flash(__('Category has been inserted successfully'))->success();
            return redirect()->route('categories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail(decrypt($id));
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
			'name' => 'required|string|min:4|max:50',
	        'meta_title' => 'max:255',
			'category_discount' => 'required',
			'policy_heading' => 'max:255',
			'bg_color' => 'max:100',
			'text_color' => 'max:100',
			'meta_keywords'=>'max:1000',
		]);
        $category = Category::findOrFail($id);
		$catname= $request->name;
		$catname=ucwords(strtolower($catname));

        $category->name = $catname;
		$category->display = $request->display;
        $category->background_color = $request->bg_color;
        $category->text_color = $request->text_color;
        $category->return_days = $request->return_days;
        $category->meta_title = $request->meta_title;
        $category->meta_description = $request->meta_description;
	    $category->meta_keywords = implode('|',$request->meta_keywords);
        $category->position = $request->position;
        $category->policy_heading = $request->policy_heading;
		$category->policy_content = $request->policy_content;
		$category->content = $request->category_content;
		$category->discount = $request->category_discount;
		
		$top_sku = $request->top_sku;
        if($top_sku == "") {
            $top_sku = array();
        } else {
            $top_sku = explode(",", $top_sku);
            foreach($top_sku as $key => $sku) {
                $top_sku[$key] = trim($top_sku[$key]);
            }
        }
        
        $category->top_sku = json_encode($top_sku);
        
        if ($request->slug != null) {
            $category->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->slug)));
        }
        else {
            $category->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', ucwords(strtolower($request->name)))));
        }
        $category->slug = str_ireplace('--','-', $category->slug);


		if($request->hasFile('menu_banner')){
			if($category->menu_banner!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$category->menu_banner))
                    unlink(base_path('public/').$category->menu_banner);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($category->menu_banner);
                }
			}
			
			$extension = $request->menu_banner->extension();
			$MenuBanner = Str::slug($catname,"-")."MenuBanner".time().".".$extension;
			$img = Image::make($request->menu_banner);
			$width = Image::make($request->menu_banner)->width();
			$height = Image::make($request->menu_banner)->height();
			
			if($width == 490 && $height == 550) {
    			 $request->menu_banner->move(base_path('public/')."temp/",$MenuBanner);
    		} else {
        		$img->resizeCanvas(490, 550, 'center', false, 'fff');
        		$img->save(base_path('public/')."temp/".$MenuBanner);
    		}
    		
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$MenuBanner, base_path('public/').'uploads/categories/menubanner/'.$MenuBanner);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/menubanner', new \Illuminate\Http\File(base_path('public/').'temp/'.$MenuBanner), $MenuBanner);
			}
			$category->menu_banner = "uploads/categories/menubanner/".$MenuBanner;
			unlink(base_path('public/').'temp/'.$MenuBanner);
		}
		
		if($request->hasFile('banner')){
			if($category->banner!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$category->banner))
                    unlink(base_path('public/').$category->banner);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($category->banner);
                }
			}
			$extension = $request->banner->extension();
			$banner = Str::slug($catname,"-")."banner".time().".".$extension;
			$img = Image::make($request->banner);
			$width = Image::make($request->banner)->width();
			$height = Image::make($request->banner)->height();
			if($width == 197 && $height == 186) {
    			 $request->banner->move(base_path('public/')."temp/",$banner);
    		} else {
        		$img->resizeCanvas(197, 186, 'center', false, 'fff');
        		$img->save(base_path('public/')."temp/".$banner);
    		}
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$banner, base_path('public/').'uploads/categories/banner/'.$banner);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/banner', new \Illuminate\Http\File(base_path('public/').'temp/'.$banner), $banner);
			}
			$category->banner = "uploads/categories/banner/".$banner;
			unlink(base_path('public/').'temp/'.$banner);
		}
		
		if($request->hasFile('icon')){
			if($category->icon!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$category->icon))
                    unlink(base_path('public/').$category->icon);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($category->icon);
                }
			}
			$extension = $request->icon->extension();
			$icon = Str::slug($catname,"-")."icon".time().".".$extension;
			$img = Image::make($request->icon);
			$width = Image::make($request->icon)->width();
			$height = Image::make($request->icon)->height();
			
			if($width == 197 && $height == 186) {
    			 $request->icon->move(base_path('public/')."temp/",$icon);
    		} else {
        		$img->resizeCanvas(197, 186, 'center', false, 'fff');
        		$img->save(base_path('public/')."temp/".$icon);
    		}
			if(env('FILESYSTEM_DRIVER') == "local") {
				copy(base_path('public/').'temp/'.$icon, base_path('public/').'uploads/categories/icon/'.$icon);
			} else {
				Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/icon', new \Illuminate\Http\File(base_path('public/').'temp/'.$icon), $icon);
			}
			$category->icon = "uploads/categories/icon/".$icon;
			unlink(base_path('public/').'temp/'.$icon);
		}
		
		if($request->hasFile('bg_img')){
		    if($category->background_img!=null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$category->background_img))
                    unlink(base_path('public/').$category->background_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($category->background_img);
                }
			}
            $extension = strtolower($request->bg_img->extension());
            if($extension == "jpg" or $extension == "png" or $extension == "gif" or  $extension == "jpeg")
			{
                $thumbnail = Str::slug($catname,"-")."back".time().".".$extension;
                $img = Image::make($request->bg_img);
    			$width = $img->width();
    			$height = $img->height();
    			if($width == 222 && $height == 250) {
    			    $request->bg_img->move(base_path('public/')."temp/",$thumbnail);
    			} else {
        			if($width <= 222 && $height <= 250) {
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			} else {
        				 $img->resize(222, 250, function ($constraint) {
        					$constraint->aspectRatio();
        				});
        				$img->resizeCanvas(222, 250, 'center', false, 'fff');
        			}
                    $img->save(base_path('public/')."temp/".$thumbnail, 100);
    			}
                ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);
                if(env('FILESYSTEM_DRIVER') == "local") {
                    copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/categories/background/'.$thumbnail);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/categories/background/', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
                }
                $category->background_img = "uploads/categories/background/".$thumbnail;
                unlink(base_path('public/').'temp/'.$thumbnail);
			}
        }
		
        if ($request->commision_rate != null) {
            $category->commision_rate = $request->commision_rate;
        }

        $category->digital = $request->digital;
        if($category->save()){
			Cache::forget('Categories');
            flash(__('Category has been updated successfully'))->success();
            return redirect()->route('categories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$category = Category::findOrFail($id);
		if($category->subcategories->count() > 0) {
		    flash(__('Category has been assigned to sub category. Unassigned before delete this category'))->warning();
		    return back();
		}

        $ProductCount = Product::where('category_id', $category->id)->count();
        
        if($ProductCount > 0) {
		    flash(__('Category has been assigned to Products. Unassigned before delete this category'))->warning();
		    return back();
		}
		
        $HomeCategoryCount = HomeCategory::where('category_id', $category->id)->count();
        if($HomeCategoryCount > 0) {
		    flash(__('Category has been assigned to Home Category. Unassigned before delete this category'))->warning();
		    return back();
		}

        if(Category::destroy($id)){

            if($category->menu_banner != null){
			    if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$category->menu_banner))
                    unlink(base_path('public/').$category->menu_banner);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($category->menu_banner);
                }
                $category->menu_banner = null;
            }
			if($category->banner != null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$category->banner))
                    unlink(base_path('public/').$category->banner);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($category->banner);
                }
                $category->banner = null;
            }
            if($category->icon != null){
				if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$category->icon))
                    unlink(base_path('public/').$category->icon);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($category->icon);
                }
                $category->icon = null;
            }
            if($category->background_img != null){
			    if(env('FILESYSTEM_DRIVER') == "local") {
                    if(file_exists(base_path('public/').$category->background_img))
                    unlink(base_path('public/').$category->background_img);
                } else {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->delete($category->background_img);
                }
                $category->background_img = null;
            }
            $category->save();
			Cache::forget('Categories');
            flash(__('Category has been deleted successfully'))->success();
            return redirect()->route('categories.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function updateFeatured(Request $request)
    {
        $category = Category::findOrFail($request->id);
        $category->featured = $request->status;
        if($category->save()){
			Cache::forget('Categories');
            return 1;
        }
        return 0;
    }
}
