<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\SubSubCategory;
use App\Category;
use Session;
use App\Color;
use App\Coupon;
use App\User;
use Cookie;
use Auth;
use App\UserCartSession;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class CartController extends Controller
{
    public function index(Request $request)
    {   
         $cartIteamList = Session::get('cart');
         $cartProductId = [];
         $cartQuantity = [];
         foreach($cartIteamList as $cartProduct){
             array_push($cartProductId,$cartProduct['id']);
             array_push($cartQuantity,$cartProduct['quantity']);
         }
        
       
         $cashbackProduct = Product::whereIn('id', $cartProductId)->get();
         
         $cashbackCategory = 35;
         $subtotal = 0;
         
         foreach($cashbackProduct as $cashBackCatProduct){
                 
                if ($cashBackCatProduct->category_id == $cashbackCategory){ 
                       
                        $cashBackMatchProduct = $cashbackProduct = Product::where('id', $cashBackCatProduct->id)->get();
                            foreach($cartIteamList as $productPriceSum){
                                
                                 if(in_array($cashBackMatchProduct[0]->id,$productPriceSum)){
                                      $subtotal +=  $productPriceSum['quantity']*$productPriceSum['price'];
                                 }
                            }
                }
             
         }
         if(!empty($subtotal)){
             Session::put('cart_cashback_Product_subtotal', $subtotal);
         }
       
         
        $cCart = $request->cookie('cart', null);
        
        if(Session::has('cart')) {
            if(count(Session::get('cart')) == 0) {
                if($cCart != null) {
                    $cart = json_decode($cCart, true);
                    
                    $cart = collect($cart);
                    session(['cart' => $cart ]);
                } 
            }
        }else {
            
             if($cCart != null) {
                $cart = json_decode($cCart, true);
                
                $cart = collect($cart);
                
                session(['cart' => $cart ]);
             } 
        }
       
       // $request->session()->put('coupon_discount', false);
       //session()->put('coupon_id', false);
       //session()->put('coupon_discount', false);
        Session::forget('coupon_id');
        Session::forget('coupon_discount');
        $categories = Category::all();
       // dd($categories);
        return view('frontend.view_cart', compact('categories'));
    }

    public function showCartModal(Request $request)
    {
        $product = Product::find($request->id);
        return view('frontend.partials.addToCart', compact('product'));
    }

    public function updateNavCart(Request $request)
    {
        $total = 0;
        $cCart = $request->cookie('cart', null);
         if(Session::has('cart')) {
             if(count(Session::get('cart')) == 0) {
                if($cCart != null) {
                $cart = json_decode($cCart, true);
                $cart = collect($cart);
                session(['cart' => $cart ]);
             } 
          }
        }
        
        if(Session::has('cart')) {
            if(count($cart = Session::get('cart')) > 0) {
                foreach($cart as $key => $cartItem) {
                    $total = $total + $cartItem['quantity'];
                }
            }
        }
        $data = view('frontend.partials.cart')->render();
        
        if(Auth::check()) {
             if(session('cart') != null) {
                $user_id = Auth()->User()->id;
                $UserCartSession = UserCartSession::where('user_id', $user_id)->first();
                if($UserCartSession == null) {
                    $UserCartSession = new UserCartSession;
                }
                $UserCartSession->user_id = $user_id;
                $UserCartSession->cart = json_encode(session('cart'));
                $UserCartSession->save();
            }
        }
        
        $data = array('data' => $data, 'cart_items_count' => $total);
        return response()->json($data)->cookie('cart_items_count', $total, 21600);
    }

    public function addToCart(Request $request)
    {
        $product = Product::find($request->id);
        $data = array();
        $data['id'] = $product->id;
        $str = '';
        $tax = 0;
        $priDoscount = 0;

        //check the color enabled or disabled for the product
        if($request->has('color')){
            $data['color'] = $request['color'];
            $str = Color::where('code', $request['color'])->first()->name;
        }

        if ($product->digital != 1) {
            //Gets all the choice values of customer choice option and generate a string like Black-S-Cotton
            foreach (json_decode(Product::find($request->id)->choice_options) as $key => $choice) {
                if($str != null){
                    $str .= '-'.str_replace(' ', '', $request['attribute_id_'.$choice->attribute_id]);
                }
                else{
                    $str .= str_replace(' ', '', $request['attribute_id_'.$choice->attribute_id]);
                }
            }
        }

        $data['variant'] = $str;

        if($str != null && $product->variant_product){
            $product_stock = $product->stocks->where('variant', $str)->first();
            $price = $product_stock->price;
            $quantity = $product_stock->qty;

            if($quantity >= $request['quantity']){
                // $variations->$str->qty -= $request['quantity'];
                // $product->variations = json_encode($variations);
                // $product->save();
            }
            else{
                return view('frontend.partials.outOfStockCart');
            }
        }
        else{
            $price = $product->unit_price;
            $unit_price = $price;
        }

        //discount calculation based on flash deal and regular discount
        //calculation of taxes
		$CategoryID=$product->category_id;
		$brand_id=$product->brand_id;
        $flash_deals = \App\FlashDeal::where('status', 1)->get();
        $inFlashDeal = false;
        foreach ($flash_deals as $flash_deal) {
            if ($flash_deal != null && $flash_deal->status == 1  && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first() != null) {
                $flash_deal_product = \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first();
                if($flash_deal_product->discount_type == 'percent'){
                    $price -= ($price*$flash_deal_product->discount)/100;
                }
                elseif($flash_deal_product->discount_type == 'amount'){
                    $price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }
       
          if(env('CACHE_DRIVER') == 'redis') {
        $category_deals = Cache::tags(['Category'])->rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
        });
        $brand_deals = Cache::tags(['Brand'])->rememberForever('Brand:'.$brand_id, function () use ($brand_id) {
                return  \App\Brand::where('id', $brand_id)->first();
        });
         } else {
             $category_deals = Cache::rememberForever('Category:'.$CategoryID, function () use ($CategoryID) {
                return  \App\Category::where('id', $CategoryID)->first();
        });
        $brand_deals = Cache::rememberForever('Brand:'.$brand_id, function () use ($brand_id) {
                return  \App\Brand::where('id', $brand_id)->first();
        });
         }
        
        if (!$inFlashDeal) {
			$product_discount = $product->discount;
			if($product_discount == null) { 
			    $product_discount=0;
			}
			
			$category_discount=$category_deals->discount;
			$brand_discount=$brand_deals->discount;
			$pro_dis = strtolower($product->discount_type);
		
		    /*if($product_discount!=0){
		        
			    if($pro_dis == 'percent'){
			        
    				$price -= ($price*$product->discount)/100;
    			}
    			elseif($pro_dis == 'amount'){
    			    
    			    $price -= $product->discount;
    			}
			}*/
			echo '===>'.$product_discount.'-'.$category_discount.'-'.$brand_discount;
				/**********PRICE DISCOUNT cALCULATION**************************************/
		if($product->discount !='ND'){
			    
			     if($pro_dis == 'percent'){
        				$price -= ($price*$product->discount)/100;
        			}
        			elseif($pro_dis == 'amount'){
        			    $price -= $product->discount;
        			}
			}else if($category_discount==0 && $category_discount!='ND'){
			   
			     echo 'cat';
			    $price -= ($price*$category_discount)/100;
			}else if($category_discount>0){
			   
			     echo 'cat';
			     $price -= ($price*$category_discount)/100;
			}else if($brand_discount==0 && $brand_discount!='ND'){
			    
			    echo 'brand';
			   	$price -= ($price*$brand_discount)/100;
			}else if($brand_discount>0){
			   
			      echo 'brand';
			   	$price -= ($price*$brand_discount)/100;
			}else{
			   
			    $price -=0;
			}
			/*********************END PRICE DISCOUNT***********************************/
			
		   // dd($price);
			if($product->discount !='ND'){
			    echo 'pro';
			     $priDoscount = $product_discount;
			}else if($product->discount >0){
			     $priDoscount = $product_discount;
			}
			else if($category_discount==0 && $category_discount!='ND'){
			     echo 'cat';
			    $priDoscount = $category_discount;
			}else if($category_discount>0){
			     echo 'cat';
			   $priDoscount = $category_discount;
			}else if($brand_discount==0 && $brand_discount!='ND'){
			    echo 'brand';
			   $priDoscount = $brand_discount;
			}else if($brand_discount>0){
			      echo 'brand';
			   $priDoscount = $brand_discount;
			}
		
			   
    			 /*if(isset($product_discount)){
    			    $priDoscount = $product_discount;
    			}else if(isset($category_discount)){
    			    $priDoscount = $category_discount; 
    			}else if(isset($brand_discount)){
    			     $priDoscount = $brand_discount; 
    			}*/
		
			
			
			 if($product_discount==0 && $product_discount=='ND' && $category_discount==0 && $category_discount=='ND' && $brand_discount==0 && $brand_discount=='ND'){
                $priDoscount = 0;
            }
            //dd($priDoscount,'===>',$product_discount,$category_discount,$brand_discount);	
			$categoryId = $product->category_id;
            $subcategoryId = $product->subcategory_id;
            $subsubcategory = $product->subsubcategory_id;
            $userType = User::where('id',$product->user_id)->first();
            if(isset($userType->user_type)){
                  $userRoleType = $userType->user_type;
            }else{
                $userRoleType = 'customer';
            }
             if($userRoleType == 'seller'){
                if(isset($product->user_id)){
            	    $sellerId = $product->user_id;
            	}else{
            	    $sellerId = 0;
            	}
            
                if(!empty($sellerId)){
                        if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                            //echo $categoryId.'-'.$subcategoryId.'-'.$subsubcategory.'-'.$sellerId;
                        $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                            $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
                    }else if(!empty($categoryId) && !empty($sellerId)){
                         $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
                    }    
                }

                //Get exclusive percentage add add final price for end users according to seller commission
                //echo '==>'.$price;
                if(!empty($get_subcategory)){
                    if(isset($get_subcategory->commission)){
                        if($get_subcategory->commission_type == 0){//-enclusive
                            $getPercent = ($price*$get_subcategory->commission)/100;
                            //$price -= $getPercent;
                        }
                        if($get_subcategory->commission_type ==  1){
                             $totalexcelusivComission = ($product->unit_price*$get_subcategory->commission)/100;
                             
                             $totalval = $product->unit_price+$totalexcelusivComission;
                             
                             $taotalDiscountPercent = $totalval*$priDoscount/100;
                            
                             $getPercent  = ($totalval*$get_subcategory->commission)/100;
                             //dd($product->unit_price,$getPercent);
                             $totalValuefinal = $totalval-$taotalDiscountPercent;
                            
                             if($totalValuefinal){
                              $price =   $totalValuefinal;  
                             }else{
                                 $price += $getPercent;
                             }
                             
                        }
                    } 
                }
               
            }
		  //dd($price);
		/*
			if($pro_dis == 'percent'){
				if(($product_discount>=$category_discount) and($product_discount>=$brand_discount)){
					$price -= ($price*$product->discount)/100;
				}
				elseif($brand_discount>=$category_discount){
					$price -= ($price*$brand_discount)/100;
				}
				else{
					$price -= ($price*$category_discount)/100;
				}
			}
			elseif($pro_dis == 'amount'){
				
				$category_discount_amount=($price*$category_discount)/100;
				$brand_discount_amount=($price*$brand_discount)/100;
				
				if(($product_discount>=$category_discount_amount) and($product_discount>=$brand_discount_amount)){
					$price -= $product->discount;
				}
				elseif($brand_discount_amount>=$category_discount_amount){
					$price -= $brand_discount_amount;
				}
				else{
					$price -= $category_discount_amount;
				}
			}
			*/
        
		}
		
        if($product->tax_type == 'percent'){
            $tax = ($price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $tax = $product->tax;
        }

        $data['quantity'] = $request['quantity'];
        $data['price'] = $price;
        $data['discount'] = $unit_price - $price;
        $coupon_category_discount = 0;
        if(Session::has('coupon_id')) {
            $coupon = Coupon::find(Session::get('coupon_id'));
            $coupon_details = json_decode($coupon->details);
            if ($coupon->type == 'categories_base') {
                $cat_id = $product->category_id;
                foreach ($coupon_details as $key => $coupon_detail) {
                    if($coupon_detail->category_id == $cat_id) {
                        if ($coupon->discount_type == 'percent') {
                            $coupon_category_discount = $data['price']*$coupon->discount/100;
                        } elseif ($coupon->discount_type == 'amount') {
                            $coupon_category_discount = $coupon->discount;
                        }
                    }
                }
            }
        }
        
        $data['coupon_category_discount'] = $coupon_category_discount;
        $data['tax'] = $tax;
        $data['shipping'] = $product->shipping_cost;
        $data['product_referral_code'] = null;
        $data['digital'] = $product->digital;

        if ($request['quantity'] == null){
            $data['quantity'] = 1;
        }

        if(Cookie::has('referred_product_id') && Cookie::get('referred_product_id') == $product->id) {
            $data['product_referral_code'] = Cookie::get('product_referral_code');
        }
        
        $cCart = $request->cookie('cart', null);
         if(Session::has('cart')) {
             if(count(Session::get('cart')) == 0) {
                if($cCart != null) {
                $cart = json_decode($cCart, true);
                $cart = collect($cart);
                session(['cart' => $cart ]);
             } 
          }
        }

        if(Session::has('cart')){
            $foundInCart = false;
            $cart = collect();

            foreach (Session::get('cart') as $key => $cartItem){
                if($cartItem['id'] == $request->id){
                    if($cartItem['variant'] == $str){
                        $foundInCart = true;
                        $cartItem['quantity'] += $request['quantity'];
                    }
                }
                $cart->push($cartItem);
            }

            if (!$foundInCart) {
                $cart->push($data);
            }
            Session::put('cart', $cart);
            Cookie::queue('cart', json_encode($cart->toArray()));
            Session::save();
        }
        else{
            $cart = collect([$data]);
            Session::put('cart', $cart);
            Cookie::queue('cart', json_encode($cart->toArray()));
            Session::save();
        }
        return view('frontend.partials.addedToCart', compact('product', 'data'));
    }

    //removes from Cart
    public function removeFromCart(Request $request)
    {
        $cCart = $request->cookie('cart', null);
         if(Session::has('cart')) {
             if(count(Session::get('cart')) == 0) {
                if($cCart != null) {
                $cart = json_decode($cCart, true);
                $cart = collect($cart);
                session(['cart' => $cart ]);
             } 
          }
        }
        if(Session::has('cart')){
            $cart = $request->session()->get('cart', collect([]));
            $cart->forget($request->key);
            Session::put('cart', $cart);
            Cookie::queue('cart', json_encode($cart->toArray()));
        }
        $total = 0;
        if(Session::has('cart')) {
            if(count($cart = Session::get('cart')) > 0) {
                foreach($cart as $key => $cartItem) {
                    $total = $total + $cartItem['quantity'];
                }
            }
        }
        $data = view('frontend.partials.cart_details')->render();
        $data = array('data' => $data, 'cart_items_count' => $total);
        return response()->json($data);
    }

    //updated the quantity for a cart item
    public function updateQuantity(Request $request)
    {  
        $cCart = $request->cookie('cart', null);
            if(Session::has('cart')) {
                if(count(Session::get('cart')) == 0) {
                    if($cCart != null) {
                    $cart = json_decode($cCart, true);
                    $cart = collect($cart);
                    session(['cart' =>$cart ]);
                } 
            }
        }
        
        $cart = Session::get('cart', collect([]));
        
        $cart = $cart->map(function ($object, $key) use ($request) {
            if($key == $request->key){
                $object['quantity'] = $request->quantity;
                
            }
            return $object;
        });
      
        Session::put('cart', $cart);
        Cookie::queue('cart', json_encode($cart->toArray()));
        Session::save();

        return view('frontend.partials.cart_details');
    }
}
