<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {    
        if (Auth::check() && (Auth::user()->user_type == 'customer' || Auth::user()->user_type == 'seller' || Auth::user()->user_type == 'vendor')) {
            if(Auth::user()->user_type == 'seller') {
                if(Auth::user()->seller->verification_status == 0) {
                    auth::logout();
                    abort(401,"Your seller account not activated.");
                }
            }
            return $next($request);
        }
        else{
            session(['link' => url()->current()]);
            return redirect()->route('user.login');
        }
    }
}
