<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
			$User = Auth::User();
			if($User->user_type == "customer" || $User->user_type=="seller" || $User->user_type=="vendor"){
				return redirect('/dashboard');
			}
			else{
				return redirect('/admin');				
			}
        }
		

        return $next($request);
    }
}
