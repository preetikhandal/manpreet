<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class MobileAppSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(session('mobileapp') != null) {
        $mobileapp = session('mobileapp');
        } else {
            $mobileapp = $request->input('android', 0);
            if($mobileapp == 0) {
                $mobileapp = $request->input('ios', 0);
            }
            session(['mobileapp' => $mobileapp]);
        }
        if(session('debug') != null) {
        $debug = session('debug');
        } else {
            $debug = $request->input('debug', 0);
            session(['debug' => $debug]);
        }
    view()->share(['mobileapp' => $mobileapp, 'debug' => $debug]);
       return $next($request);
    }
}
