<?php

namespace App;

use App\Product;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Auth;
use ImageOptimizer;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class ProductsImportUpdate implements ToModel, WithHeadingRow
{
    public function model(array $row)
    { //dd($row);
		$discount_type = strtolower($row['discount_type']);
		$tax_type = strtolower($row['tax_type']);
		$unit_price = $row['unit_price'];
		$purchase_price = $row['purchase_price'];
		if(isset($row['shipping_cost'])){
	        	$shipping_cost = $row['shipping_cost'];	    
		}else{
		    	$shipping_cost = 0;
		}
	    if(isset($row['description'])){
	     $description = $row['description'];    
	    }else{
	        $description = 'NA';
	    }
		
		if($discount_type == "") {
		    $discount_type = 'percent';
		}
		if($tax_type == "") {
		    $tax_type = 'percent';
		}
		$Product =Product::where('product_id',$row['product_id'])->first();
	
		if($Product != null ) {
		    if($row['name']!=""){ $Product->name=$row['name']; }
    		$Product->discount_type = $discount_type;
    		$Product->tax_type = $tax_type;
    		if($row['discount']!=""){ $Product->discount=$row['discount']; }
    		if($row['tax']!=""){ $Product->tax=$row['tax']; }
    		if($row['category_id']!=""){ $Product->category_id=$row['category_id']; }
    		if($row['subcategory_id']!=""){ $Product->subcategory_id=$row['subcategory_id']; }
    		if($row['subsubcategory_id']!=""){ $Product->subsubcategory_id=$row['subsubcategory_id']; }
    		if($row['unit']!=""){ $Product->unit=$row['unit']; }
    		if($row['salt']!=""){ $Product->salt=$row['salt']; }
    		if(strlen($row['current_stock']) > 0) { $Product->current_stock=$row['current_stock']; }
    		if($row['meta_title']!=""){ $Product->meta_title=$row['meta_title']; }
	    	if($row['meta_description']!=""){ $Product->meta_description= $row['meta_description']; }
	    	if($Product->tags != "") {
	    	    if($Product->tags[-1] == ",") {
	    	        $tags = $Product->tags.$row['tags'];
	    	    } else {
	    	        $tags = $Product->tags.",".$row['tags'];
	    	    }
	    	} else {
	    	    $tags = $row['tags'];
	    	}
	    	if($Product->alignbook_pro_id!=null){
	    	     $uniqueid =  $Product->alignbook_pro_id;
	    	}else{
	    	     $uniqueid =  Str::uuid()->toString();
	    	}
	    	if($row['tags']!=""){ $Product->tags = $tags; }
			if($unit_price!=""){ $Product->unit_price = $unit_price; }
			if($purchase_price!=""){ $Product->purchase_price = $purchase_price; }
			if($shipping_cost!=""){ $Product->shipping_cost = $shipping_cost; }
			if($description!=""){ $Product->description = $description; }
			if(isset($row['gst_tax_in_percent'])){
			    $Product->gst_tax = $row['gst_tax_in_percent'];
			}
			
			//add Product in align booke
			/***************ADD ITEAM IN ALIGN BOOK*********************************/
        //dd($uniqueid);
       
        $postJsonData = array(
            	"is_new_mode"=> true,
            	"item_information"=> array(
            		"id"=> $uniqueid,
            		"name"=> $row['name'],
            		"item_group_id"=> "a0931c2e-4d27-4b1b-bf6e-b63f8b6db2c4",
            		"item_category_id"=> "",
            		"type"=> 0,
            		"item_mode"=> 0,
            		"inactive"=> false,
            		"separate_pack_unit"=> false,
            		"input_dimension"=> false,
            		"gst_input_not_applicable"=> false,
            		"sub_item_applicable"=> false,
            		"stock_unit_id"=> "b9239fc8-bc7b-499f-9aca-d0635741295e",
            		"pack_unit_id"=> "",
            		"stock_vs_pack"=> 1,
            		"rate_per"=> 0,
            		"procurement"=> 0,
            		"garment_item_type"=> 0,
            		"gst_classification_id"=> "0eeb53e3-aeac-4a4f-870e-ab5d1d275081",
            		"vat_tax_id"=> "",
            		"sales_description"=> "XYZ",
            		"barcode"=> $row['product_id'],
            		"markup_percentage"=> 0,
            		"sales_rate"=> $unit_price,
            		"mrp"=> 0,
            		"min_rate"=> 0,
            		"sales_misc_1"=> 0,
            		"sales_misc_2"=> 0,
            		"sales_misc_3"=> 0,
            		"sales_gl_id"=> "ee113c08-68fb-4bbb-993a-6309a4937fb3",
            		"purchase_description"=> $description,
            		"purchase_rate"=> 0,
            		"purchase_misc_1"=> 0,
            		"purchase_misc_2"=> 0,
            		"purchase_misc_3"=> 0,
            		"purchase_gl_id"=> "5dbdad5a-6b2c-463d-a78f-ee0cf45c6dfa",
            		"minimum_level"=> 0,
            		"inventory_gl_id"=> "",
            		"specification_template_id"=> "",
            		"free_template_id"=> "",
            		"suggested_template_id"=> "",
            		"rm_item_id"=> "",
            		"batch_wise_inventory"=> false,
            		"batch_wise_rate"=> false,
            		"serial_tracking"=> false,
            		"predefined"=> false,
            		"ask_udf_in_document"=> false,
            		"depreciation_gl_id"=> "",
            		"fca_622_id"=> "",
            		"fca_mdb_id"=> "",
            		"tally_id"=> "",
            		"salt"=> "",
            		"rack_box"=> "",
            		"metal_purity"=> 0,
            		"udf_list"=> ["", "", "", "", ""],
            		"itemset_template_list"=> [],
            		"attribute_list"=> [array(
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		),array (
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		)],
            		"fl_names"=> [],
            		"code_config"=>array (
            			"code"=> '00000000',
            			"code_no"=> 0,
            			"code_prefix"=> ""
            		),
            		"bundles"=> [],
            		"mapping_codes"=> []
            	)
            );
          $endPoints = 'SaveUpdate_Item';
          
          $addIteam = $this->addUpdateIteam(json_encode($postJsonData),$endPoints);
           $jsondRepons = json_encode($addIteam,true);
          //dd(json_decode($jsondRepons));
          $respondData = json_decode($jsondRepons);
        
          if($respondData->ReturnCode == 0){
                $Product->alignbook_pro_id = $uniqueid;
        		$Product->save();
        		if(env('CACHE_DRIVER') == 'redis') {
    			    Cache::tags(['Product'])->forever('Product:'.$Product->id, $Product);
        		} else {
        		    Cache::forever('Product:'.$Product->id, $Product);
        		}
                $prefix = env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_');
        		$OldCache = Redis::command('keys',['*ProductID:'.$Product->id.':*']);
                foreach($OldCache as $value) {
                    $value = str_replace($prefix, '', $value);
                    Redis::del($value);
                }
          }
         
            Redis::set('ProductID:'.$Product->id.':ProductTag:'.strtolower($Product->tags), $Product->id);
            Redis::set('ProductID:'.$Product->id.':ProductName:'.strtolower($Product->name), $Product->id);
            Redis::set('ProductID:'.$Product->id.':ProductSalt:'.strtolower($Product->salt), $Product->id);
    		return $Product;
		}
    }
    
    //Bulk update product in align booked
    public function addUpdateIteam($requestJson,$endPoint){
	   // dd($requestJson);
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
        $data = json_decode($response1);
        return $data; 
	}

    /*public function rules(): array
    {
        return [
             // Can also use callback validation rules
             'unit_price' => function($attribute, $value, $onFailure) {
                  if (!is_numeric($value)) {
                       $onFailure('Unit price is not numeric');
                  }
              }
        ];
    }*/
}
