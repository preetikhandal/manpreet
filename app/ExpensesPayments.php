<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpensesPayments extends Model
{
    protected $table = 'expensespayments';
    protected $primaryKey = 'EPaymentID';
}
