<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnProduct extends Model
{
	protected $table = 'return_product';
	protected $primaryKey = 'id';
	public $timestamps = true;
}

