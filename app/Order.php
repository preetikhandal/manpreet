<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class)->withTrashed();
    }

    public function refund_requests()
    {
        return $this->hasMany(RefundRequest::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function pickup_point()
    {
        return $this->belongsTo(PickupPoint::class)->withTrashed();
    }
}
