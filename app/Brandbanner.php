<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brandbanner extends Model
{
    protected $table = 'brandbanner';
	protected $primaryKey = 'id';
}
