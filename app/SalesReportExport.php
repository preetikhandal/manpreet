<?php

namespace App;

use App\Product;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\Brand;
use App\User;
use Carbon\Carbon;
use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SalesReportExport implements FromCollection, WithMapping, WithHeadings
{
	function __construct($data) {
        $this->data = $data;
    }
	
    public function collection()
    {
        $startDate = $this->data->input('startDate', null);
        $endDate = $this->data->input('endDate', null);
        $payment_status = $this->data->input('payment_status', 'all');
        $delivery_status = $this->data->input('delivery_status', 'all');
        if($startDate !== null) {
            $startDate = Carbon::createFromFormat('d/m/Y', $startDate);
        } else {
            $startDate = Carbon::now()->subDays(30);
        }
        if($endDate !== null) {
            $endDate = Carbon::createFromFormat('d/m/Y', $endDate);
        } else {
            $endDate = Carbon::now();
        }
        
        $sort_search = null;
        $orders = Order::whereBetween('created_at',[$startDate->toDateTimeString(), $endDate->toDateTimeString()])->orderBy('code', 'desc');
        
        return $orders = $orders->get();
    }

   public function headings(): array
    {
        return [
            'Order Code',
            'Order Date',
            'No of Product',
            'Customer',
			'Total Order Value',
            'Discount Amount',
			'Amount',
            'Delivery Status',
            'Payment Status'
        ];
    }

    /**
    * @var Order $orders
    */
    public function map($order): array
    {
        if ($order->user_id != null) {
            $customer = User::find($order->user_id)->name;
        } else {
            $customer = $order->guest_id;
        }
        $price = $order->grand_total + $order->coupon_discount;
        $discount = $order->coupon_discount;
        $grand_total= $order->grand_total;
        $status = 'Delivered';
        foreach ($order->orderDetails as $key => $orderDetail) {
            if($orderDetail->delivery_status != 'delivered'){
                $status = 'Pending';
            }
        }
        
        if ($order->payment_status == 'paid') {
            $payment_status = "Paid";
        } else {
            $payment_status = "Unpaid";
        }
        $order_date = Carbon::parse($order->created_at)->format('d/m/Y');
        
        return [
            $order->code,
			$order_date,
            count($order->orderDetails),
            $customer,
			$price,
            $discount,
			$grand_total,
            $status,
			$payment_status
        ];
    }
}
