<?php

namespace App;

use App\Product;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\Brand;
use App\User;
use Carbon\Carbon;
use App\Order;
use App\OrderDetail;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsSalesReportDownload implements FromCollection, WithMapping, WithHeadings
{
	function __construct($data) {
        $this->data = $data;
    }
	
    public function collection()
    {
        $startDate = $this->data->input('startDate', null);
        $endDate = $this->data->input('endDate', null);
        $payment_status = $this->data->input('payment_status', 'all');
        $delivery_status = $this->data->input('delivery_status', 'all');
        if($startDate !== null) {
            $startDate = Carbon::createFromFormat('d/m/Y', $startDate);
        } else {
            $startDate = Carbon::now()->subDays(30);
        }
        if($endDate !== null) {
            $endDate = Carbon::createFromFormat('d/m/Y', $endDate);
        } else {
            $endDate = Carbon::now();
        }
        
        $sort_search = null;
        $OrderDetail = OrderDetail::whereBetween('created_at',[$startDate->toDateTimeString(), $endDate->toDateTimeString()])->selectRaw('product_id, sum(quantity) as productCount')->groupBy('product_id')->orderBy('productCount', 'desc');
        
        //dd($OrderDetail);
        //$orders = Order::whereBetween('created_at',[$startDate->toDateTimeString(), $endDate->toDateTimeString()])->orderBy('code', 'desc');
        
        return $OrderDetail = $OrderDetail->get();
    }

   public function headings(): array
    {
        return [
            'Product Name',
            'Total Sales'
        ];
    }

    /**
    * @var Order $orders
    */
    public function map($OrderDetail): array
    {
        $product = Product::withTrashed()->find($OrderDetail->product_id);
        if($product != null) {
            $ProductName = $product->name;
        } else {
            $ProductName = "Deleted Product";
        }
        return [
            $ProductName,
			$OrderDetail->productCount
        ];
    }
}
