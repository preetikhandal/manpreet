<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpensesDetails extends Model
{
    protected $table = 'expenses_details';
    protected $primaryKey = 'expenses_details_id';
}
