<?php

namespace App;

use App\Product;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\Brand;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsCatExport implements FromCollection, WithMapping, WithHeadings
{
	function __construct($data) {
    $this->data = $data;
  }
    public function collection()
    {
		$data = $this->data;
		$Product = Product::query();
		if($data['brand_id'] != null && $data['brand_id'] !=0) {
			$Product = $Product->where('brand_id', $data['brand_id']);
		}
		if($data['marg'] == 1) {
			$Product = $Product->whereNotNull('marg_code');
		}elseif($data['marg'] == 2) {
			$Product = $Product->whereNull('marg_code');
		}
		if($data['category_id'] != null && $data['category_id'] !=0) {
			$Product = $Product->where('category_id', $data['category_id']);
		}
		
		if($data['subcategory_id'] != null && $data['subcategory_id'] !=0) {
			$Product = $Product->where('subcategory_id', $data['subcategory_id']);
		}
		
		if($data['subsubcategory_id'] != null && $data['subsubcategory_id'] !=0) {
			$Product = $Product->where('subsubcategory_id', $data['subsubcategory_id']);
		}
		
        return $Product->get();
    }

    public function headings(): array
    {
        return [
            'Product Code',
            'Name',
            'Added By',
            'User ID',
			'User Name',
            'Category ID',
			'Category Name',
            'Subcategory ID',
           'Subcategory Name',
            'Subsubcategory ID',
            'Subsubcategory Name',
            'Brand ID',
            'Brand Name',
            'Thumbnail Image',
            'Images',
            'Unit Price',
            'Purchase Price',
            'Unit',
            'Current Stock',
            'Meta Title',
            'Meta Description',
			'Marg Code',
			'Product Description'
        ];
    }

    /**
    * @var Product $product
    */
    public function map($product): array
    {        
			//$username=User::find($product->user_id);
			//$categoryname=Category::find($product->category_id);
			// $subcategoryname=SubCategory::find($product->subcategory_id);
			 $subsubcategoryname=SubSubCategory::find($product->subsubcategory_id);
			 if($subsubcategoryname != null) {
				 $subsubcategoryname = $subsubcategoryname->name;
			 } else {
				 $subsubcategoryname = "";
			 }
			 $subcategoryname=SubCategory::find($product->subcategory_id);
			 if($subcategoryname != null) {
				 $subcategoryname = $subcategoryname->name;
			 } else {
				 $subcategoryname = "";
			 }
			 
			 
			 $categoryname=Category::find($product->category_id);
			 if($categoryname != null) {
				 $categoryname = $categoryname->name;
			 } else {
				 $categoryname = "";
			 }
			 
			 
			 $brandname=Brand::find($product->brand_id);
			 if($brandname != null) {
				 $brandname = $brandname->name;
			 } else {
				 $brandname = "";
			 }
			 
			 
			 $username=User::find($product->user_id);
			 if($username != null) {
				 $username = $username->name;
			 } else {
				 $username = "";
			 }
			 if( strlen($product->photos) > 10 ) {
			     $ImagesA = json_decode($product->photos, true);
    			 $images = array();
    			 foreach($ImagesA as $I) {
    			     $images[] = asset($I);
    			 }
    			 if(count($images) > 0) {
    			     $images = implode(",", $images);
    			 } else {
    			     $images = "";
    			 }
			 } else {
			    $images = "";
			 }
			 if($product->thumbnail_img != null) {
			     $thumbnail_img = asset($product->thumbnail_img);
			 } else {
			     $thumbnail_img = "";
			 }
			 
			 
			//	echo $product->user_id.'~'.$user;
        return [
            $product->product_id,
            $product->name,
            $product->added_by,
            $product->user_id,
			$username,
            $product->category_id,
			$categoryname,
            $product->subcategory_id,
			$subcategoryname,
			$product->subsubcategory_id,
			$subsubcategoryname,
			$product->brand_id,
            $brandname,
            $thumbnail_img,
            $images,
            $product->unit_price,
            $product->purchase_price,
            $product->unit,
            $product->current_stock,
            $product->meta_title,
            $product->meta_description,
            $product->marg_code,
            $product->description
        ];
    }
}
