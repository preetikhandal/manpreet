<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PickupPoint extends Model
{
    use SoftDeletes;
    
    public function staff(){
    	return $this->belongsTo(Staff::class)->withTrashed();
    }
}
