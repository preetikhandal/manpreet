<?php

namespace App;

use App\Product;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Auth;
use ImageOptimizer;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
use Illuminate\Support\Facades\Cache;

class ProductsImport11 implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
		$meta_title="Buy ".$row['name']." Online at Low Prices in India - Aldebazaar.com";
		$meta_description="Aldebazaar.com Buy ".$row['name']." online at low price in India on Aldebazaar.com. Check out ".$row['name']." reviews, specifications and more at Aldebazaar.com. Cash on Delivery Available.";
		
		$Product=Product::where('product_id',$row['product_id'])->first();
		
		if($Product == null ){
			$path = 'bulkproductupload/'.$row['brand_id'].'/'.$row['product_id'];
			$path = public_path($path);
			$description=""; $photos = array(); $thumbnail="";
			
			if(file_exists($path)){
				$files = scandir($path);
				unset($files[0]); 
				unset($files[1]);
				foreach($files as $key => $file){
					
					$part = explode(".", $file);
					$extension = end($part);
					$extension = strtolower($extension);
					if($extension == "jpg" or $extension == "png" or $extension == "gif" or  $extension == "jpeg") {
						$photo=$path."/".$file;
						
						$mainimage = Str::slug($row['name'],"-")."main".time().".".$extension;
						$img1 = Image::make($photo);
						$width = Image::make($photo)->width();
						$height = Image::make($photo)->height();
						if($width <= 800 && $height <= 800) {
							$img1->resizeCanvas(800, 800, 'center', false, 'fff');
						} else {
							 $img1->resize(800, 800, function ($constraint) {
								$constraint->aspectRatio();
							});
							$img1->resizeCanvas(800, 800, 'center', false, 'fff');
						}
						
						
						$img1->save(base_path('public/')."temp/".$mainimage);
						//ImageOptimizer::optimize(base_path('public/')."temp/".$mainimage);
						if(env('FILESYSTEM_DRIVER') == "local") {
							copy(base_path('public/').'temp/'.$mainimage, base_path('public/').'uploads/products/photos/'.$mainimage);
						} else {
							Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/photos', new \Illuminate\Http\File(base_path('public/').'temp/'.$mainimage), $mainimage);
						}
						array_push($photos, 'uploads/products/photos/'.$mainimage);
						unlink(base_path('public/').'temp/'.$mainimage);
						
						if($key==2){
							$thumbnail = Str::slug($row['name'],"-")."thumb".time().".".$extension;
							$img = Image::make($photo);
							
							$img->resize(null, 150, function ($constraint) {
								$constraint->aspectRatio();
							});
							$img->save(base_path('public/')."temp/".$thumbnail);
							//ImageOptimizer::optimize(base_path('public/')."temp/".$thumbnail);
							
							if(env('FILESYSTEM_DRIVER') == "local") {
								//thumbnail
								copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/products/thumbnail/'.$thumbnail);
								
								//featured
								copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/products/featured/'.$thumbnail);
								
								//Flash Deal
								copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/products/flash_deal/'.$thumbnail);
								
								//meta_img
								copy(base_path('public/').'temp/'.$thumbnail, base_path('public/').'uploads/products/meta/'.$thumbnail);
								
							} else {
								//thumbnail
								Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/thumbnail', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
								
								//featured
								Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/featured', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
								
								//flash_deal
								Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/flash_deal', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
								
								//meta_img
								Storage::disk(env('FILESYSTEM_DRIVER'))->putFileAs('uploads/products/meta', new \Illuminate\Http\File(base_path('public/').'temp/'.$thumbnail), $thumbnail);
							}
							unlink(base_path('public/').'temp/'.$thumbnail);
						}
					}
					elseif($extension == "txt" && $file!="image.txt") {
						$filename=$path."/".$file;
						if(file_exists($filename)){
							$myfile = fopen($filename, "r") or die("Unable to open file!");
							while(!feof($myfile)) {
							  $description.= fgets($myfile);
							}
							$description=nl2br($description);
						}
					}
					sleep(1);
					$i=1;
				}		
			}
			
			if($thumbnail!=null) {
				$thumbnail_img= "uploads/products/thumbnail/".$thumbnail;
				$featured_img= "uploads/products/featured/".$thumbnail;
				$flash_deal_img= "uploads/products/flash_deal/".$thumbnail;
				$meta_img= "uploads/products/meta/".$thumbnail;
			} else {
				$thumbnail_img=NULL; $featured_img=NULL; $flash_deal_img=NULL; $meta_img=NULL;
			}
			
			if(empty($photos)){$photos=NULL;}
			$product = Product::create([
			   'product_id'     => $row['product_id'],
			   'name' => $row['name'],
			   'added_by'  => Auth::user()->user_type == 'seller' ? 'seller' : 'admin',
			   'user_id'  => Auth::user()->user_type == 'seller' ? Auth::user()->id : User::where('user_type', 'admin')->first()->id,
			   'category_id'    => $row['category_id'],
			   'subcategory_id'    => $row['subcategory_id'],
			   'subsubcategory_id'    => $row['subsubcategory_id'],
			   'brand_id'    => $row['brand_id'],
			   'video_provider'    => "",
			   'video_link'    => "",
			   'unit_price'    => $row['unit_price'],
			   'purchase_price'    => $row['purchase_price'],
			   'unit'    => $row['unit'],
			   'current_stock' => $row['current_stock'],
			   'meta_title' => $meta_title,
			   'meta_description' => $meta_description,
			   'colors' => json_encode(array()),
			   'choice_options' => json_encode(array()),
			   'variations' => json_encode(array()),
			   'slug' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $row['name'])).'-'.str_random(5)),
			   'description' => $description,
			   'photos' => json_encode($photos),
			   'salt' => $row['salt'],
			   'thumbnail_img' => $thumbnail_img,
			   'featured_img' => $featured_img,
			   'flash_deal_img' => $flash_deal_img,
			   'meta_img' => $meta_img,
			]);
			return $product;
		}
		else{
			$Product->update([
				'product_id'     => $row['product_id'],
				'name' => $row['name'],
				'added_by'  => Auth::user()->user_type == 'seller' ? 'seller' : 'admin',
				'user_id'  => Auth::user()->user_type == 'seller' ? Auth::user()->id : User::where('user_type', 'admin')->first()->id,
				'category_id'    => $row['category_id'],
				'subcategory_id'    => $row['subcategory_id'],
				'subsubcategory_id'    => $row['subsubcategory_id'],
				'brand_id'    => $row['brand_id'],
				'unit_price'    => $row['unit_price'],
				'purchase_price'    => $row['purchase_price'],
				'unit'    => $row['unit'],
				'current_stock' => $row['current_stock'],
				'meta_title' => $meta_title,
				'meta_description' => $meta_description,
				'colors' => json_encode(array()),
				'choice_options' => json_encode(array()),
				'variations' => json_encode(array()),
				'slug' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $row['name'])).'-'.str_random(5)),
			]);
			$Product->save();
			return $Product;
		}
    }

    /*public function rules(): array
    {
        return [
             // Can also use callback validation rules
             'unit_price' => function($attribute, $value, $onFailure) {
                  if (!is_numeric($value)) {
                       $onFailure('Unit price is not numeric');
                  }
              }
        ];
    }*/
}
