<?php

namespace App;

use App\Customer;
use App\Brand;
use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomerExportExcel implements FromCollection, WithMapping, WithHeadings
{
	function __construct($data) {
        $this->data = $data;
    }
	
    public function collection()
    {
        $customer = Customer::orderBy('created_at', 'desc');
        return $customer = $customer->get();
    }

   public function headings(): array
    {
        return [
            'Name',
            'Email Address',
            'Phone',
            'Date',
            'Wallet Balance',
        ];
    }

    /**
    * @var Order $orders
    */
    public function map($customer): array
    {
        if($customer->user != null) {
            return [
                $customer->user->name,
    			$customer->user->email,
    			$customer->user->phone,
    			date('d-M-Y', strtotime($customer->user->created_at)),
    			$customer->user->balance
            ];
        } else {
            return [];
        }
    }
}
