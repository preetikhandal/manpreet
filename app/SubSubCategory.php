<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubSubCategory extends Model
{
    use SoftDeletes;
  public function subcategory(){
  	return $this->belongsTo(SubCategory::class, 'sub_category_id')->withTrashed();
  }

  public function products(){
  	return $this->hasMany(Product::class, 'subsubcategory_id');
  }

  public function classified_products(){
  	return $this->hasMany(CustomerProduct::class, 'subsubcategory_id');
  }
}
