<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    protected $fillable = [
      'user_id',
    ];
    public function user(){
    	return $this->belongsTo(user::class)->withTrashed();
    }
}
