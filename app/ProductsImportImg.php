<?php

namespace App;

use App\Product;
use App\User;
use App\ProductUploadQueue;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Auth;
use ImageOptimizer;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
use Illuminate\Support\Facades\Cache;

class ProductsImportImg implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {    
        if($row['product_id'] != null) {
            $ProductUploadQueue = new ProductUploadQueue;
            $ProductUploadQueue->product_id = $row['product_id'];
            $ProductUploadQueue->name = $row['name'];
            $ProductUploadQueue->description = $row['description'];
            $ProductUploadQueue->thumb = $row['thumb'];
            $ProductUploadQueue->photos = $row['photos'];
            $ProductUploadQueue->category_id = $row['category_id'];
            $ProductUploadQueue->subcategory_id = $row['subcategory_id'];
            $ProductUploadQueue->subsubcategory_id = $row['subsubcategory_id'];
            $ProductUploadQueue->brand_id = $row['brand_id'];
            $ProductUploadQueue->unit_price = $row['unit_price'];
            $ProductUploadQueue->purchase_price = $row['purchase_price'];
            $ProductUploadQueue->shipping_cost = $row['shipping_cost'];
            $ProductUploadQueue->unit = $row['unit'];
            $ProductUploadQueue->current_stock = $row['current_stock'];
            $ProductUploadQueue->salt = $row['salt'];
            $ProductUploadQueue->discount = $row['discount'];
            $ProductUploadQueue->discount_type = $row['discount_type'];
            $ProductUploadQueue->tax = $row['tax'];
            $ProductUploadQueue->tax_type = $row['tax_type'];
            $ProductUploadQueue->tags = $row['tags'];
            $ProductUploadQueue->meta_title = $row['meta_title'];
            $ProductUploadQueue->meta_description = $row['meta_description'];
            if(isset($row['gst_tax_in_percent'])){
			   $ProductUploadQueue->gst_tax = $row['gst_tax_in_percent'];
			}
            
            $ProductUploadQueue->save();
            return $ProductUploadQueue;
        }
    }
    
    public function addUpdateIteam($requestJson,$endPoint){
	   // dd($requestJson);
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
        $data = json_decode($response1);
        return $data; 
	}
}