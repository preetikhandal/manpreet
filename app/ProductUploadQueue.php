<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductUploadQueue extends Model
{
     public $timestamps = false;
}
