<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
    use SoftDeletes;
  public function category(){
  	return $this->belongsTo(Category::class)->withTrashed();
  }

  public function subsubcategories(){
  	return $this->hasMany(SubSubCategory::class);
  }

  public function products(){
      return $this->hasMany(Product::class, 'subcategory_id');
  }

  public function classified_products(){
  	return $this->hasMany(CustomerProduct::class, 'subcategory_id');
  }
}
