<?php
namespace App;
use App\Order;
use App\Brand;
use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrderExportExcel implements FromCollection, WithMapping, WithHeadings
{
	function __construct($data) {
        $this->data = $data;
    }
	
    public function collection()
    {
        $Order = Order::orderBy('created_at', 'desc');
        return $Order = $Order->get();
    }

   public function headings(): array
    {
        return [
            'order id',
            'Name',
            'Email Address',
            'Phone',
            'Address',
            'Country',
            'City',
            'State',
            'postal code',
            'Guest id',
            'payment type',
            'payment_status',
            'payment_details',
            'cart_shipping',
            'grand_total',
            'coupon_discount',
            'coupon_code',
            'code',
            'order date',
            'prescription_file',
            'viewed',
            'delivery_viewed',
            'payment_status_viewed',
            'commission_calculated',
            'Delivery_Confirmed',
            'Referral_Commission',
			'Tags',
            'created_at',
            'updated_at',
            'deleted_at'
        ];
    }

    /**
    * @var Order $orders
    */
    public function map($Order): array
    {
        
        $shippingAddress = json_decode($Order->shipping_address);
        if(isset($shippingAddress->name)){
            $name = $shippingAddress->name; 
        }else{
             $name = 'NA';
        }
        if(isset($shippingAddress->email)){
            $email = $shippingAddress->email;
        }else{
            $email = 'NA';
        }
        if(isset($shippingAddress->address)){
            $address = $shippingAddress->address;    
        }else{
            $address = 'NA';
        }
        if(isset($shippingAddress->country)){
            $country = $shippingAddress->country;
        }else{
            $country = 'NA';
        }
        if(isset($shippingAddress->city)){
            $city = $shippingAddress->city;
        }else{
            $city = 'NA';
        }
        if(isset($shippingAddress->state)){
            $state = $shippingAddress->state;
        }else{
            $state = 'NA';
        }
        if(isset($shippingAddress->postal_code)){
            $postalCode = $shippingAddress->postal_code;
        }else{
            $postalCode = 'NA';
        }
		if(isset($Order->tags)){
            $tags = $Order->tags;
        }else{
            $tags = 'NA';
        }
		
        $state = $state;
        if(isset($shippingAddress->phone)){
            $phone = $shippingAddress->phone;
        }else{
            $phone = 'NA';
        }
        
        if($Order->id != null) {
          
            return [
                $Order->id,
                $name,
                $email,
                $phone,
                $address,
                $country,
                $city,
    			$state,
                $postalCode,
                $Order->guest_id,
                $Order->payment_type,
                $Order->payment_status,
                $Order->payment_details,
                $Order->cart_shipping,
                $Order->grand_total,
                $Order->coupon_discount,
                $Order->coupon_code,
                $Order->code,
                $Order->date,
                $Order->prescription_file,
                $Order->viewed,
                $Order->delivery_viewed,
                $Order->payment_status_viewed,
                $Order->commission_calculated,
                $Order->Delivery_Confirmed,
                $Order->Referral_Commission,
				$tags,
                $Order->created_at,
                $Order->updated_at,
                $Order->deleted_at,
            ];
        } else {
            return [];
        }
    }
}
