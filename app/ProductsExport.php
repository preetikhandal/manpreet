<?php

namespace App;

use App\Product;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\Brand;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsExport implements FromCollection, WithMapping, WithHeadings
{
	
    public function collection()
    {
        return Product::all();
    }

   public function headings(): array
    {
        return [
            'name',
            'added_by',
            'user_id',
			'user_name',
            'category_id',
			'category_name',
            'subcategory_id',
           'subcategory_name',
            'subsubcategory_id',
            'subsubcategory_name',
            'brand_id',
            'brand_name',
            'tags',
            'video_provider',
            'video_link',
            'unit_price',
            'purchase_price',
            'unit',
            'current_stock',
            'meta_title',
            'meta_description',
			'marg_code'
        ];
    }

    /**
    * @var Product $product
    */
    public function map($product): array
    {
			$subsubcategoryname=SubSubCategory::find($product->subsubcategory_id);
			 if($subsubcategoryname != null) {
				 $subsubcategoryname = $subsubcategoryname->name;
			 } else {
				 $subsubcategoryname = "";
			 }
			 $subcategoryname=SubCategory::find($product->subcategory_id);
			 if($subcategoryname != null) {
				 $subcategoryname = $subcategoryname->name;
			 } else {
				 $subcategoryname = "";
			 }
			$categoryname=Category::find($product->category_id);
			 if($categoryname != null) {
				 $categoryname = $categoryname->name;
			 } else {
				 $categoryname = "";
			 }
			 
			 
			 $brandname=Brand::find($product->brand_id);
			 if($brandname != null) {
				 $brandname = $brandname->name;
			 } else {
				 $brandname = "";
			 }
			 
			 
			 $username=User::find($product->user_id);
			 if($username != null) {
				 $username = $username->name;
			 } else {
				 $username = "";
			 }
			//	echo $product->user_id.'~'.$user;
        return [
            $product->name,
            $product->added_by,
            $product->user_id,
			$username,
            $product->category_id,
			$categoryname,
            $product->subcategory_id,
			$subcategoryname,
			$product->subsubcategory_id,
			$subsubcategoryname,
			$product->brand_id,
            $brandname,
			$product->tags,
			$product->video_provider,
            $product->video_link,
            $product->unit_price,
            $product->purchase_price,
            $product->unit,
            $product->current_stock,
            $product->marg_code
        ];
    }
}
