<?php
namespace App;
use App\Product;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Auth;
use ImageOptimizer;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
use Illuminate\Support\Facades\Cache;
use App\SellerCommision;

class ProductsImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {   
        $userid = Auth::user()->id;
        if(!empty($row['category_id'])){
             $sellerComission = SellerCommision::where(['category_id'=>$row['category_id'],'user_id'=>$userid])->get();     
        }
       
        //dd($sellerComission);
        $categoryIds = [];
        if(!empty($sellerComission)){
            
            foreach($sellerComission as $catIds){
                array_push($categoryIds,$catIds->category_id);
            }
            
        }
        if(!empty($row['subcategory_id'])){
            $sellerComission1 = SellerCommision::where(['subcategory_id'=>$row['subcategory_id'],'user_id'=>$userid])->get();    
        }
        
        //dd($sellerComission);
        $subcategoryIds = [];
        if(!empty($sellerComission1)){
            
            foreach($sellerComission1 as $subcatIds){
                array_push($subcategoryIds,$subcatIds->subcategory_id);
            }
            
        }
        
        $process = false;
          //dd($categoryIds,$subcategoryIds,$subsubcategoryIds);
        if(!empty($categoryIds) && !empty($subcategoryIds)){
            $process = true;
           
        }else{
            
            $process = false;
        }
        //dd($process);
        if($process){
                $productName=$row['name'];
        		if($row['meta_title']==""){
        		    $meta_title="Buy ".$productName." Online at Low Prices in India - Aldebazaar.com";
        		} else {
        		   $meta_title= $row['meta_title'];
        		}
        		if($row['meta_description']==""){
        		    $meta_description="Aldebazaar.com Buy ".$productName." online at low price in India on Aldebazaar.com. Check out ".$productName." reviews, specifications and more at Aldebazaar.com. Cash on Delivery Available.";
        		}
        		else{
        		   $meta_description= $row['meta_description'];
        		}
			
    		$Product = Product::where('product_id',$row['product_id'])->first();
    		//dd($Product);
    		$thumbnail_img=NULL; $featured_img=NULL; $flash_deal_img=NULL; $meta_img=NULL;
    		if($Product == null && $row['unit_price']!='' && $row['purchase_price']!='' && $row['unit']!=''  && $row['current_stock']!=''){
    		    
    		    //dd($row['unit_price']);
    			/*$path = 'bulkproductupload/'.$row['brand_id'].'/'.$row['product_id'];
    			$path = public_path($path);*/
    			$description=""; $photos = array(); $thumbnail="";
    			if(empty($photos)){$photos=NULL;}
    			$uniqueid =  Str::uuid()->toString();
    			/********************ADD BULK UPLOAD ITEAM IN ALIGN BOOKED********************************************/
    			  $postJsonData = array(
            	"is_new_mode"=> true,
            	"item_information"=> array(
            		"id"=> $uniqueid,
            		"name"=> $row['name'],
            		"item_group_id"=> "a0931c2e-4d27-4b1b-bf6e-b63f8b6db2c4",
            		"item_category_id"=> "",
            		"type"=> 0,
            		"item_mode"=> 0,
            		"inactive"=> false,
            		"separate_pack_unit"=> false,
            		"input_dimension"=> false,
            		"gst_input_not_applicable"=> false,
            		"sub_item_applicable"=> false,
            		"stock_unit_id"=> "b9239fc8-bc7b-499f-9aca-d0635741295e",
            		"pack_unit_id"=> "",
            		"stock_vs_pack"=> 1,
            		"rate_per"=> 0,
            		"procurement"=> 0,
            		"garment_item_type"=> 0,
            		"gst_classification_id"=> "0eeb53e3-aeac-4a4f-870e-ab5d1d275081",
            		"vat_tax_id"=> "",
            		"sales_description"=> "XYZ",
            		"barcode"=> $row['product_id'],
            		"markup_percentage"=> 0,
            		"sales_rate"=> $row['unit_price'],
            		"mrp"=> 0,
            		"min_rate"=> 0,
            		"sales_misc_1"=> 0,
            		"sales_misc_2"=> 0,
            		"sales_misc_3"=> 0,
            		"sales_gl_id"=> "ee113c08-68fb-4bbb-993a-6309a4937fb3",
            		"purchase_description"=> $description,
            		"purchase_rate"=> 0,
            		"purchase_misc_1"=> 0,
            		"purchase_misc_2"=> 0,
            		"purchase_misc_3"=> 0,
            		"purchase_gl_id"=> "5dbdad5a-6b2c-463d-a78f-ee0cf45c6dfa",
            		"minimum_level"=> 0,
            		"inventory_gl_id"=> "",
            		"specification_template_id"=> "",
            		"free_template_id"=> "",
            		"suggested_template_id"=> "",
            		"rm_item_id"=> "",
            		"batch_wise_inventory"=> false,
            		"batch_wise_rate"=> false,
            		"serial_tracking"=> false,
            		"predefined"=> false,
            		"ask_udf_in_document"=> false,
            		"depreciation_gl_id"=> "",
            		"fca_622_id"=> "",
            		"fca_mdb_id"=> "",
            		"tally_id"=> "",
            		"salt"=> "",
            		"rack_box"=> "",
            		"metal_purity"=> 0,
            		"udf_list"=> ["", "", "", "", ""],
            		"itemset_template_list"=> [],
            		"attribute_list"=> [array(
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		),array (
            			"id"=> "",
            			"name"=> ""
            		), array(
            			"id"=> "",
            			"name"=> ""
            		)],
            		"fl_names"=> [],
            		"code_config"=>array (
            			"code"=> '00000000',
            			"code_no"=> 0,
            			"code_prefix"=> ""
            		),
            		"bundles"=> [],
            		"mapping_codes"=> []
            	)
            );
          $endPoints = 'SaveUpdate_Item';
          
          $addIteam = $this->addUpdateIteam(json_encode($postJsonData),$endPoints);
           $jsondRepons = json_encode($addIteam,true);
          //dd(json_decode($jsondRepons));
          $respondData = json_decode($jsondRepons);
    			/*******************************END ADD BULK IN ALIGN BOOKED******************************************/
    			if($respondData->ReturnCode == 0){
    			   $product = Product::create([
    			   'product_id'     => $row['product_id'],
    			   'alignbook_pro_id'=>$uniqueid,
    			   'name' => $productName,
    			   'added_by'  => Auth::user()->user_type == 'seller' ? 'seller' : 'admin',
    			   'user_id'  => Auth::user()->user_type == 'seller' ? Auth::user()->id : User::where('user_type', 'admin')->first()->id,
    			   'category_id'    => $row['category_id'],
    			   'subcategory_id'    => $row['subcategory_id'],
    			   'subsubcategory_id'    => $row['subsubcategory_id'],
    			   'brand_id'    => $row['brand_id'],
    			   'video_provider'    => "",
    			   'video_link'    => "",
    			   'unit_price'    => $row['unit_price'],
    			   'purchase_price'    => $row['purchase_price'],
    			   'unit'    => $row['unit'],
    			   'current_stock' => $row['current_stock'], 
    			   'discount' => $row['discount'],
    			   'discount_type' => $row['discount_type'],
    			   'tax' => $row['tax'],
    			   'tax_type' => $row['tax_type'],
    			   'meta_title' => $meta_title,               
    			   'meta_description' => $meta_description,   
    			   'colors' => json_encode(array()),            
    			   'choice_options' => json_encode(array()),    
    			   'variations' => json_encode(array()),        
    			   'slug' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $productName)).'-'.str_random(5)),
    			   'description' => null,
    			   'photos' => json_encode($photos),
    			   'salt' =>$row['salt'],
    			   'thumbnail_img' => $thumbnail_img,
    			   'featured_img' => $featured_img,
    			   'flash_deal_img' => $flash_deal_img,
    			   'meta_img' => $meta_img,
    			   'tags' => $row['tags'],
    			   'variation' => json_encode(array()),
    			   'variation_order' => json_encode(array()),
    			   'variation_combinations' => json_encode(array()),
    			   'variation_product' => json_encode(array()),
    			   'gst_tax'=>$row['gst_tax_in_percent'],
    			  
    			]);
    		
    			return $product;    
    			}else{
    			    return false;
    			}
    			
    		}
		
            
        }else{
          
            //dd('Category is not white listed with this seller'.$row['category_id']);
            //flash(__('Spmething went wrong '))->error();
            //return redirect('product-bulk-upload/index');
            ?>
            
            <!-- Modal -->
            <style>
            .oppS { 
                position: relative;
                height: 100%;
            }
            .oppS .item {   
                position: absolute;
                left: 25%;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
            width: 50%;
            margin: 0 auto;
            border: 1px solid #ccc;
            border-radius: 19px;
            padding: 20px; }
            .oppS .item .images { text-align: center; }
            .oppS .item img { height: 100;      height: 6rem;}
            .oppS .item .btn {
                background: #282563;
    padding: 11px 50px;
    border-radius: 19px;
    color: #fff;
    font-weight: 600;
    font-size: 16px;
    text-decoration: none;
            }
            .oppS .item p {
           font-size: 18px;
    line-height: 24px;
    color: #212326;}
            </style>
            <div class="oppS">
              <div class="row">
                  <div class="item">
                      <div class="images">
                          <img src="https://staging.aldebazaar.com/frontend/images/opps.png" alt="opps">
                      </div>
                        <p>Seller Id:- <?php echo Auth::user()->id;?>   Opps.....you are enter wrong Details in excel sheet which is not approve by This seller so please choose another category which is mention in download category</p>
                  <?php
                      if(!isset($row['category_id'])){
                          ?>
                          <p>Please add category id in excel sheet</p>
                          <?php
                      }
                       if(!isset($row['subcategory_id'])){
                          ?>
                          <p>Please add Sub category id in excel sheet</p>
                          <?php
                      }
                     if(empty($categoryIds)){
                         ?>
                            <p>Your category Name is:-<?php echo $row['category_id']?> which is not approve so please approve this category for this seller and try</p>
                         <?php
                     }
                     
                     if(empty($subcategoryIds)){
                         ?>
                           <p >Your Sub category Name is:-<?php echo $row['subcategory_id']?> which is not approve so please approve this category for this seller and try</p>
                         <?php
                     }
                     
                     
                    
                  ?>
                 
                 
                 
                  <p style="text-align: center;"><a class="btn btn primary" href="https://staging.aldebazaar.com/">Back</a></p>
                  </div>
                
                  
              </div>
            </div>
            <?
            
            die;
            //return false;
        }
		
    }
    
    //Reponse msg of function 
    public function responseMsg($data){
        return $data;
    }
    
    
    public function addUpdateIteam($requestJson,$endPoint){
	   // dd($requestJson);
	    $curl1 = curl_init();
          curl_setopt_array($curl1, array(
          CURLOPT_URL => 'https://service.alignbooks.com/ABDataService.svc/'.$endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$requestJson,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: info@aldebazaar.com',
            'user_id:97e9e6a8-4767-409c-9ed1-8f69c41f6fa8',
            'company_id:9dabe13a-9e3c-4641-8f78-641c2c8e9550',
            'enterprise_id:496e999a-50c0-4d50-95bc-729fd73be083',
            'apikey: caf767a7-f9ab-11eb-9560-00505698b97d'
          ),
        ));
        
        $response1 = curl_exec($curl1);
        curl_close($curl1);
        $data = json_decode($response1);
        return $data; 
	}

    /*public function rules(): array
    {
        return [
             // Can also use callback validation rules
             'unit_price' => function($attribute, $value, $onFailure) {
                  if (!is_numeric($value)) {
                       $onFailure('Unit price is not numeric');
                  }
              }
        ];
    }*/
}
