<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Coupon;
use App\Order;
use App\ReturnProduct;
use App\Wallet;
use Mail;
use Carbon\Carbon;

class calculateCategoryCashBack extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculateCategoryCashBack';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To calculate category coupon cashback';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::now();
        $date = Carbon::now()->subDays(20);
        $Orders = Order::where('coupon_code', '!=', null)->where('coupon_discount',0)->where('Delivery_Confirmed', '!=', null)->where('category_discount_cashback', 0)->whereDate('Delivery_Confirmed', [Carbon::now()->subDays(0)->toDateString(), Carbon::now()->subDays(0)->toDateString()])->get();
        foreach($Orders as $Order) {
            $coupon = Coupon::select('details', 'code', 'discount', 'discount_type')->where('code', $Order->coupon_code)->first();
            $order_user = User::find($Order->user_id);
            $data = json_decode($coupon->details, true);
           
            $return_order = ReturnProduct::select('product_id')->where('order_id', $Order->id)->get();
            
            if(count($return_order) > 0) {
                $return_order = $return_order->pluck('product_id')->all();
                $orderDetails = $Order->orderDetails->whereNotIn('product_id', $return_order);
                $sum = $orderDetails->sum('price') + $orderDetails->sum('tax') + $orderDetails->sum('shipping_cost');
            } else {
                $sum = $Order->orderDetails->sum('price') + $Order->orderDetails->sum('tax') + $Order->orderDetails->sum('shipping_cost');
            }
            $discount = $coupon->discount;
            if($coupon->discount_type=="percent")
            {
                $cash_back = $sum * ($discount/100);
                $cash_back = number_format($cash_back, '2',".","");
                $cash_back = floatval($cash_back);
            }
            else{
               $cash_back = $discount;
            }
            $wallet = new Wallet;
            $wallet->user_id = $order_user->id;
            $wallet->amount = $cash_back;
            $wallet->payment_method = 'Cash Back Credit';
            $wallet->payment_details = 'Cash Back For Order #'. $Order->code;
            $wallet->add_status = 1;
            $wallet->save();
            $order_user->balance = $order_user->balance + $cash_back;
            $order_user->save();
            $smsMsg = "Hi, we have successfully processed your cash back for your order #".$Order->code.". The amount of Rs. ".$cash_back." is credited in your Alde wallet. You can manage your wallet from your My Account section.";
            $message = $smsMsg;
            $smsMsg = rawurlencode($smsMsg);
            $email_subject = "AldeBazaar : Wallet Credit";
            $to = "91".$order_user->phone;
            if(strlen($to) == 12) {
                $SMS_URL = env('SMS_URL', null);
                if($SMS_URL != null) {
                    $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                    file_get_contents($SMS_URL);
                }
            }
            $emailData = array('email' => $order_user->email, 'name' => $order_user->name, 'message' => $message,  'title' => 'Wallet Update', 'email_subject' => $email_subject);
            Mail::send('emails.notification',array('data' => $emailData), function ($m) use ($emailData) {
                    $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
            });
        
            $Order->category_discount_cashback = 1;
            $Order->save();
        }
    }
    

}
