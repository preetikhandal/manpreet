<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Coupon;
use App\Order;
use App\ReturnProduct;
use App\Wallet;
use Mail;
use Carbon\Carbon;

class calculateReferralCommision extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculateReferralCommision';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To calculate of Referral';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $referral_Email = array();
        $today = Carbon::now();
        $date = Carbon::now()->subDays(20);
        $Orders = Order::where('coupon_code', '!=', null)->where('coupon_discount',0)->where('Delivery_Confirmed', '!=', null)->where('Referral_Commission', 0)->whereDate('Delivery_Confirmed', [Carbon::now()->subDays(0)->toDateString(), Carbon::now()->subDays(0)->toDateString()])->get();
        foreach($Orders as $Order) {
            $coupon = Coupon::select('user_id', 'details', 'code')->where('code', $Order->coupon_code)->first();
            $order_user = User::find($Order->user_id);
            $data = json_decode($coupon->details, true);
            $return_order = ReturnProduct::select('product_id')->where('order_id', $Order->id)->get();
            
            if(count($return_order) > 0) {
                $return_order = $return_order->pluck('product_id')->all();
                $orderDetails = $Order->orderDetails->whereNotIn('product_id', $return_order);
                $sum = $orderDetails->sum('price') + $orderDetails->sum('tax') + $orderDetails->sum('shipping_cost');
            } else {
                $sum = $Order->orderDetails->sum('price') + $Order->orderDetails->sum('tax') + $Order->orderDetails->sum('shipping_cost');
            }
            $discount = $this->coupon_discount_search($data,$sum);
            if($Order->user_id == $coupon->user_id) {
                $cash_back = $sum * ($discount/100);
                $cash_back = number_format($cash_back, '2',".","");
                $cash_back = floatval($cash_back);
                $wallet = new Wallet;
                $wallet->user_id = $order_user->id;
                $wallet->amount = $cash_back;
                $wallet->payment_method = 'Cash Back Credit';
                $wallet->payment_details = 'Cash Back For Order #'. $Order->code;
                $wallet->add_status = 1;
                $wallet->save();
                $order_user->balance = $order_user->balance + $cash_back;
                $order_user->save();
                $smsMsg = "Hi, we have successfully processed your cash back for your order #".$Order->code.". The amount of Rs. ".$cash_back." is credited in your Alde wallet. You can manage your wallet from your My Account section.";
                $message = $smsMsg;
                $smsMsg = rawurlencode($smsMsg);
                $email_subject = "AldeBazaar : Wallet Credit";
                $to = "91".$order_user->phone;
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        file_get_contents($SMS_URL);
                    }
                }
                $emailData = array('email' => $order_user->email, 'name' => $order_user->name, 'message' => $message,  'title' => 'Wallet Update', 'email_subject' => $email_subject);
                Mail::send('emails.notification',array('data' => $emailData), function ($m) use ($emailData) {
                        $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                });
            } else {
                $coupon_user = User::find($coupon->user_id);
                $discount = $discount/2;
                $cash_back = $sum * ($discount/100);
                $cash_back = number_format($cash_back, '2',".","");
                $cash_back = floatval($cash_back);
                $wallet = new Wallet;
                $wallet->user_id = $order_user->id;
                $wallet->amount = $cash_back;
                $wallet->payment_method = 'Cash Back Credit';
                $wallet->payment_details = 'Cash Back For Order #'. $Order->code;
                $wallet->add_status = 1;
                $wallet->save();
                $order_user->balance = $order_user->balance + $cash_back;
                $order_user->save();
                $smsMsg = "Hi, we have successfully processed your cash back for your order #".$Order->code.". The amount of Rs. ".$cash_back." is credited in your Alde wallet. You can manage your wallet from your My Account section.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Wallet Credit";
                $smsMsg = rawurlencode($smsMsg);
                $to = "91".$order_user->phone;
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        file_get_contents($SMS_URL);
                    }
                }
                $emailData = array('email' => $order_user->email, 'name' => $order_user->name, 'message' => $message,  'title' => 'Wallet Update', 'email_subject' => $email_subject);
                Mail::send('emails.notification',array('data' => $emailData), function ($m) use ($emailData) {
                        $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                });
                
                //Referral
                $wallet = new Wallet;
                $wallet->user_id = $coupon_user->id;
                $wallet->amount = $cash_back;
                $wallet->payment_method = 'Referr Cash Back Credit';
                $wallet->payment_details = 'Cash Back For Order #'. $Order->code;
                $wallet->add_status = 1;
                $wallet->save();
                $coupon_user->balance = $coupon_user->balance + $cash_back;
                $coupon_user->save();
                if(isset($referral_Email[$coupon_user->id])) {
                    $referral_Email[$coupon_user->id]['cash_back'] = $referral_Email[$coupon_user->id]['cash_back'] + $cash_back;
                } else {
                    $referral_Email[$coupon_user->id] = array('coupon_user_name' => $coupon_user->name, 'coupon_user_phone' => $coupon_user->phone, 'coupon_user_email' => $coupon_user->email, 'cash_back' => $cash_back);
                }
            }
            $Order->Referral_Commission = 1;
            $Order->save();
        }
        foreach($referral_Email as $r) {
                $smsMsg = "Hi, we have successfully processed your referral cash back. The amount of Rs. ".$r['cash_back']." is credited in your Alde wallet. You can manage your wallet from your My Account section.";
                $message = $smsMsg;
                $email_subject = "AldeBazaar : Wallet Credit for Referral";
                $smsMsg = rawurlencode($smsMsg);
                $to = "91".$r['coupon_user_phone'];
                if(strlen($to) == 12) {
                    $SMS_URL = env('SMS_URL', null);
                    if($SMS_URL != null) {
                        $SMS_URL = $SMS_URL."&to=".$to."&text=".$smsMsg;
                        file_get_contents($SMS_URL);
                    }
                }
                $emailData = array('email' => $r['coupon_user_email'], 'name' => $r['coupon_user_name'], 'message' => $message, 'title' => 'Wallet Update', 'email_subject' => $email_subject);
                Mail::send('emails.notification',array('data' => $emailData), function ($m) use ($emailData) {
                        $m->to($emailData['email'], $emailData['name'])->subject($emailData['email_subject']);
                });
        }
    }
    
    private function coupon_discount_search($datas, $value)
    {
        $value = floatval($value);
        foreach($datas as $data) {
            if($data['from'] == -1) {
                if($data['to'] >= $value) {
                    return $data['discount'];
                }
            } elseif($data['to'] == -1) {
                if($data['from'] <= $value) {
                    return $data['discount'];
                }
            } else {
                if($data['from'] <= $value && $data['to'] >= $value) {
                    return $data['discount'];
                }
            }
        }
        return 0;
    }

}
