<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpensesCategory extends Model
{
    use SoftDeletes;
    protected $table = 'expensescategory';
    protected $primaryKey = 'ECategoryID';
}
