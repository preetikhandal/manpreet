<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerProduct extends Model
{
    use SoftDeletes;
    public function category(){
    	return $this->belongsTo(Category::class)->withTrashed();
    }

    public function subcategory(){
    	return $this->belongsTo(SubCategory::class)->withTrashed();
    }

    public function subsubcategory(){
    	return $this->belongsTo(SubSubCategory::class)->withTrashed();
    }

    public function brand(){
    	return $this->belongsTo(Brand::class)->withTrashed();
    }

    public function user(){
    	return $this->belongsTo(User::class)->withTrashed();
    }

    public function state(){
    	return $this->belongsTo(State::class)->withTrashed();
    }

    public function city(){
    	return $this->belongsTo(City::class)->withTrashed();
    }
}
