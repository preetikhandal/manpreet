<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPurchasePayment extends Model
{
    protected $primaryKey = 'product_purchase_payment_id';
}
