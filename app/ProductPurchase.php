<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Vendor;

class ProductPurchase extends Model
{
    protected $primaryKey = 'product_purchase_id';
    
    
    public function vendor(){
    	return $this->belongsTo(Vendor::class);
    }
}
