<?php

namespace App\Exceptions;

use Mail;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use App\Mail\ExceptionMail;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Request;
use Route;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        if ($this->shouldReport($exception)) {
           $this->sendEmail($exception); // sends an email
        }
        //$this->sendEmail($exception); 
        
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }
    
 
    public function sendEmail($exception)
    {
        try {
            $data = array();
            $data['url'] = url(Route::current()->uri());
            $data['method'] = json_encode(Route::current()->methods());
            $data['param'] = json_encode(Request::all());
            $data['message'] = $exception->getMessage();
            $data['line'] = $exception->getLine();
            $data['File'] = $exception->getFile();
            $data['previous_url'] = url()->previous();
            $data['Trace'] = nl2br($exception->getTraceAsString());
            $html = "";
            $html .= "URL : ". $data['url']."<br />";
            $html .= "Preivous URL : ". $data['previous_url']."<br />";
            $html .= "Methods : ". $data['method']."<br />";
            $html .= "Params : ". $data['param']."<br />";
            $html .= "Message : ". $data['message']."<br />";
            $html .= "Line No : ". $data['line']."<br />";
            $html .= "File : ". $data['File']."<br />";
            $html .= "Traces : <br />". $data['Trace']."<br />";
        //    Mail::to('imarjun12@gmail.com')->send(new ExceptionMail($html));
        } catch (Exception $ex) {
           // dd($ex);
        }
    }
}
