<?php

namespace App;

use App\Product;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Auth;
use ImageOptimizer;
use DB;
use Image;
use File;
use Illuminate\Support\Str;
use Storage;
use App\OrderDetail;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class ProductsBulkDelete implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
		$product = Product::where('product_id',$row['product_id'])->first();
		
        $c = file_get_contents("text.log");
        $c = "Product : ".$row['product_id']." ". date("D M j G:i:s T Y") ."\r\n". $c;
		file_put_contents("text.log", $c);
            		
		if($product != null) {
           // if (Product::where('product_id',$row['product_id'])->delete()) {
                if($product->thumbnail_img != null) {
                    if(env('FILESYSTEM_DRIVER') == "local") {
                        if(file_exists(base_path('public/').$product->thumbnail_img))
                        unlink(base_path('public/').$product->thumbnail_img);
                    } else {
                        Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->thumbnail_img);
                        usleep(100000);
                    }
                    $product->thumbnail_img = null;
                    
                }
                if($product->featured_img != null) {
                    if(env('FILESYSTEM_DRIVER') == "local") {
                        if(file_exists(base_path('public/').$product->featured_img))
                        unlink(base_path('public/').$product->featured_img);
                    } else {
                        Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->featured_img);
                        usleep(100000);
                    }
                    $product->featured_img = null;
                }
                
                if($product->flash_deal_img != null) {
    				if(env('FILESYSTEM_DRIVER') == "local") {
                        if(file_exists(base_path('public/').$product->flash_deal_img))
                        unlink(base_path('public/').$product->flash_deal_img);
                    } else {
                        Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->flash_deal_img);
                        usleep(100000);
                    }
                    $product->flash_deal_img = null;
                }
    			if($product->meta_img !=null) {
    				if(env('FILESYSTEM_DRIVER') == "local") {
                        if(file_exists(base_path('public/').$product->meta_img))
                        unlink(base_path('public/').$product->meta_img);
                    } else {
                        Storage::disk(env('FILESYSTEM_DRIVER'))->delete($product->meta_img);
                        usleep(100000);
                    }
    			}
    			if(json_decode($product->photos) != null) {
                    foreach (json_decode($product->photos) as $key => $photo) {
                        if(env('FILESYSTEM_DRIVER') == "local") {
                            if(file_exists(base_path('public/').$photo))
                            unlink(base_path('public/').$photo);
                        } else {
                            Storage::disk(env('FILESYSTEM_DRIVER'))->delete($photo);
                        usleep(100000);
                        }
                        $product->photos = json_encode(array());
                    }
    			}
    			$product->save();
    			$product->delete();
        		if(env('CACHE_DRIVER') == 'redis') {
                        Cache::tags(['Product'])->forget('Product:'.$product->id);
                } else {  
                   Cache::forget('Product:'.$product->id);
                }
            		
                $prefix = env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_');
        		$OldCache = Redis::command('keys',['*ProductID:'.$product->id.':*']);
                foreach($OldCache as $value) {
                    $value = str_replace($prefix, '', $value);
                    Redis::del($value);
                }
                $OrderDetail = OrderDetail::where('product_id', $product->id)->count();
                if($OrderDetail == 0) {
                    $product->forceDelete();
                }
                DB::commit();
           // }
		}
    }
}
