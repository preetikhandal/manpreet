<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitemapSetting extends Model
{
     public $timestamps = false;
}
