<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = [
         'name','added_by', 'user_id', 'category_id', 'subcategory_id', 'subsubcategory_id', 'brand_id', 'video_provider', 'video_link', 'unit_price',
        'purchase_price', 'unit', 'discount','discount_type','tax','tax_type', 'slug', 'colors', 'choice_options', 'variations', 'current_stock','product_id','description','photos','salt','thumbnail_img','featured_img','flash_deal_img','meta_title','meta_description','meta_img','tags'
        ];
    public function category(){
    	return $this->belongsTo(Category::class)->withTrashed();
    }

    public function subcategory(){
    	return $this->belongsTo(SubCategory::class)->withTrashed();
    }

    public function subsubcategory(){
    	return $this->belongsTo(SubSubCategory::class)->withTrashed();
    }

    public function brand(){
    	return $this->belongsTo(Brand::class)->withTrashed();
    }

    public function user(){
    	return $this->belongsTo(User::class)->withTrashed();
    }

    public function orderDetails(){
    return $this->hasMany(OrderDetail::class);
    }

    public function reviews(){
    return $this->hasMany(Review::class)->where('status', 1);
    }

    public function wishlists(){
    return $this->hasMany(Wishlist::class);
    }

    public function stocks(){
    return $this->hasMany(ProductStock::class);
    }
}
