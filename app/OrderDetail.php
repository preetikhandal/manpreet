<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    
    use SoftDeletes;
    public function order()
    {
        return $this->belongsTo(Order::class)->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function pickup_point()
    {
        return $this->belongsTo(PickupPoint::class)->withTrashed();
    }

    public function refund_request()
    {
        return $this->hasOne(RefundRequest::class);
    }
}
