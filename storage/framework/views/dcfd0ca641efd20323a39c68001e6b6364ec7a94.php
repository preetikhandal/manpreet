<?php $__env->startSection('content'); ?>
<!-- Seller Style -->
<link type="text/css" href="<?php echo e(asset('frontend/css/seller.css')); ?>" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" <?php if($mobileapp == 1): ?> style="margin-top:75px" <?php endif; ?>>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    <?php if(Auth::user()->user_type == 'seller'): ?>
                        <?php echo $__env->make('frontend.inc.seller_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php elseif(Auth::user()->user_type == 'customer'): ?>
                        <?php echo $__env->make('frontend.inc.customer_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <?php if($errors->any()): ?>
							<div class="alert alert-danger">
								<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
								<ul>
									<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<li><?php echo e($error); ?></li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							</div>
						<?php endif; ?>
                        <!-- Page title -->
                        <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-12">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 p-0 text-white">
                                        <?php echo e(__('Manage Profile')); ?>

                                    </h2>
                                </div>
                            </div>
                        </div>
                        <form class="" action="<?php echo e(route('customer.profile.update')); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <div class="form-box bg-white mt-4" style="border:1px solid #282563">
                                <div class="form-box-title px-3 py-2 bg-blue text-white">
                                    <?php echo e(__('Basic info')); ?>

                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Your Name')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Your Name')); ?>" name="name" value="<?php echo e(Auth::user()->name); ?>">
                                        </div>
                                       
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Your Email')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="email" class="form-control mb-3" placeholder="<?php echo e(__('Your Email')); ?>" name="email" value="<?php echo e(Auth::user()->email); ?>" <?php if(isset(Auth::user()->email)): ?> readonly <?php endif; ?>>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Photo')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <span class="err text-danger"></span>
                                            <input type="file" name="photo" id="file-3" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/jpg,image/jpeg,image/x-png" />
                                            <label for="file-3" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    <?php echo e(__('Choose image')); ?>

                                                </strong>
                                            </label>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Your Password')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control mb-3" placeholder="<?php echo e(__('New Password')); ?>" name="new_password">
                                        </div>
                                    	
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Confirm Password')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control mb-3" placeholder="<?php echo e(__('Confirm Password')); ?>" name="confirm_password">
                                        </div>
                                    	
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4" style="border:1px solid #282563">
                                <div class="form-box-title px-3 py-2 bg-blue text-white">
                                    <?php echo e(__('Shipping info')); ?>

                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Address')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea class="form-control textarea-autogrow mb-3" placeholder="<?php echo e(__('Your Address')); ?>" rows="1" name="address"><?php echo e(Auth::user()->address); ?></textarea>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Country')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <select class="form-control mb-3 selectpicker" data-placeholder="<?php echo e(__('Select your country')); ?>" name="country">
                                                    <?php $__currentLoopData = \App\Country::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($country->name); ?>" <?php if(Auth::user()->country == $country->name) echo "selected";?> ><?php echo e($country->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('City')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Your City')); ?>" name="city" value="<?php echo e(Auth::user()->city); ?>">
                                        </div>
                                    	
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Postal Code')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="<?php echo e(__('Your Postal Code')); ?>" name="postal_code"  value="<?php echo e(Auth::user()->postal_code); ?>" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6">
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Phone')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="<?php echo e(__('Your Phone Number')); ?>" name="phone" value="<?php echo e(Auth::user()->phone); ?>" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10"  <?php if(isset(Auth::user()->phone)): ?> readonly <?php endif; ?>>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="text-right mt-4">
                                <button type="submit" class="btn btn-styled btn-base-1"><?php echo e(__('Update Profile')); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
    	$('INPUT[type="file"]').change(function () {
    		var ext = this.value.match(/\.(.+)$/)[1];
    		var FileSize = this.files[0].size / 1024 / 1024; // in MB
    		if (FileSize > 4) {
    			$(".err").text('Max file size: 4MB');
    			this.value = '';
    		}
    		switch (ext) {
    			case 'jpg':
    			case 'jpeg':
    			case 'png':
    				$('#uploadButton').attr('disabled', false);
    				break;
    			default:
    				$(".err").text('This is not an allowed file type.Only jpg/png files are allowed to upload');
    				this.value = '';
    		}
    	});
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/customer/profile.blade.php ENDPATH**/ ?>