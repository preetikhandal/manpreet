<?php $__env->startSection('content'); ?>
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
   background:#20b34e!important;
}
.process-steps li {
    width: 50%;
   
}
</style>
<!-- Seller Style -->
<link type="text/css" href="<?php echo e(asset('frontend/css/seller.css')); ?>" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" <?php if($mobileapp == 1): ?> style="margin-top:75px" <?php endif; ?>>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    <?php if(Auth::user()->user_type == 'seller'): ?>
                        <?php echo $__env->make('frontend.inc.seller_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php elseif(Auth::user()->user_type == 'customer'): ?>
                        <?php echo $__env->make('frontend.inc.customer_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="row">
                            <div class="col-12 px-0">
                                <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-12">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 p-0 text-white">
                                        <?php echo e(__('Return History')); ?>

                                    </h2>
                                </div>
                            </div>
                        </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <?php if(count($ReturnProduct) > 0): ?>
                                <?php $__currentLoopData = $ReturnProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $RP): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                        $order=\App\Order::find($RP->order_id);
                                        $Product=\App\Product::find($RP->product_id);
                                        $OrderDetail=\App\OrderDetail::where('order_id',$RP->order_id)->where('product_id',$RP->product_id)->first();
                                    ?>
                                    <?php $__currentLoopData = $order->orderDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $orderDetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <article class="card card-product-list">
                                        	<div class="row no-gutters align-items-center">
                                        		<div class="col-md-2 col-3">
                                        			<a href="<?php echo e(route('product', $orderDetail->product->slug)); ?>" >
                                    			    	<?php if($orderDetail->product->thumbnail_img!=""): ?>
                                        			    	<img src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" data-src="<?php echo e(asset($orderDetail->product->thumbnail_img)); ?>" alt="<?php echo e($orderDetail->product->name); ?>" class="img-fluid lazyload">
                                        			    <?php else: ?>
                                        			        <img src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" alt="<?php echo e($orderDetail->product->name); ?>" class="img-fluid lazyload">
                                        			    <?php endif; ?>
                                        			</a>
                                        		</div> 
                                        		<div class="col-md-7 col-9 ">
                                        			<div class="info-main ">
                                        			    <?php if($orderDetail->product != null): ?>
                                        			    	<a href="<?php echo e(route('product', $orderDetail->product->slug)); ?>" target="_blank" class="h5 title"><?php echo e($orderDetail->product->name); ?></a>
                                        		        <?php else: ?>
                                                            <strong><?php echo e(__('Product Unavailable')); ?></strong>
                                                        <?php endif; ?>
                                        		        <p class="info">Order 
                                        		            <a href="#<?php echo e($order->code); ?>" onclick="show_purchase_history_details(<?php echo e($order->id); ?>)" class="text-blue font-weight-bold"># <?php echo e($order->code); ?></a> 
                                        		            <span class="block-sm">Status
                                                                <?php if(str_replace('_', ' ', $RP->status)=="processing refund"): ?>
                                                                    <span class="badge badge-info"> <?php echo e(ucfirst(str_replace('_', ' ',$RP->status))); ?></span>
                                                                <?php else: ?>
                                                                    <span class="badge badge-success"> <?php echo e(ucfirst(str_replace('_', ' ',$RP->status))); ?></span>
                                                                <?php endif; ?>
                                        		            </span>
                                        		        </p>
                                        		        <p class="info">Request Date <b class="text-blue"><?php echo e($RP->created_at->format('d/m/Y')); ?> </b>
                                        		           <span class="block-sm">Payment Status 
                                        		             <?php if($order->payment_status == 'paid'): ?>
                                        		                <span class="badge badge-success"> <?php echo e(__('Paid')); ?></span>
                                                             <?php else: ?>
                                                                <span class="badge badge-warning"><?php echo e(__('Unpaid')); ?></span>
                                                             <?php endif; ?>
                                        		           </span>
                                        		        </p>
                                        		        <span class="price h5 d-block d-md-none">Product Price <?php echo e(single_price($orderDetail->price)); ?> </span>
                                        			</div>
                                        		</div> 
                                        		<div class="col-12 d-block d-md-none mb-2 px-2 mobilebtn">
                                        		    <a href="#" onclick="show_return_history_details(<?php echo e($RP->order_id); ?>,<?php echo e($OrderDetail->product_id); ?>)" class="btn  btn-light btn50 float-left bg-blue"> <?php echo e(__('Track Refund Status')); ?> </a>
                                        		</div>
                                        		<div class="col-md-3 d-none d-md-block">
                                        			<div class="info-aside">
                                        				<div class="price-wrap">
                                        					<span class="price h5"><span class="info">Product Price</span> <?php echo e(single_price($orderDetail->price)); ?> </span>	
                                        				</div> <br/>
                                        				<p class="d-none d-md-block">
                                        					<a href="#" onclick="show_return_history_details(<?php echo e($RP->order_id); ?>,<?php echo e($OrderDetail->product_id); ?>)" class="btn btn-light bg-blue btn-block"><?php echo e(__('Track Refund Status')); ?></a>
                                        				</p>
                                        			</div> 
                                        		</div> 
                                        	</div> 
                                        </article>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
    							<div class="card no-border mt-4">
                                    <div>
    									<table class="table table-sm table-hover table-responsive-md">
                                            <thead>
                                                <tr class="text-center">
                                                    <th><h3><?php echo e(__('No Return History Found')); ?></h3></th>
                                                </tr>
                                            </thead>
    									</table>
    								</div>
    							</div>
                            <?php endif; ?>
                        </div>
                        <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                <?php echo e($ReturnProduct->links()); ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="return_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative" style="border-color:#282563;">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="return-details-modal-body">

                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5"><?php echo e(__('Make Payment')); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="payment_modal_body"></div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        $('#order_details').on('hidden.bs.modal', function () {
            location.reload();
        })
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/return_history.blade.php ENDPATH**/ ?>