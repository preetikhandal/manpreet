<?php $__env->startSection('content'); ?>

   <style>
    .form-group {
    margin-left: 0 !important;
    margin-right: 0 !important;
}
</style>
<div class="row">
	<form class="form form-horizontal mar-top" action="<?php echo e(route('editSellerCommission')); ?>" method="POST" enctype="multipart/form-data" id="">
		<?php echo csrf_field(); ?>
		<input name="_method" type="hidden" >
		<input type="hidden" name="commissionId" value="<?php echo e($sellerComissionDetails->id); ?>">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Commission Panel</h3>
			</div>
				<div class="panel-body">
				    <div class="col-lg-8">
				        <div class="row">
				         <div class="col-lg-6 form-group">
				            <label>All Seller</label>
			                <select class="form-control demo-select2-placeholder" name="sellerId" >
                                  <option value="">All Sellers</option>
                                 <?php $__currentLoopData = App\Seller::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $seller): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($seller->user != null && $seller->user->shop != null): ?>
                                        <option value="<?php echo e($seller->user->id); ?>" <?php if($seller->user->id == $sellerComissionDetails->user_id): ?> selected <?php endif; ?> ><?php echo e($seller->user->shop->name); ?> (<?php echo e($seller->user->name); ?>)</option>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
    				    </div>
                        
    				  
    				    
    				    <div class="col-lg-6 form-group">
				            <label>Category</label>
			                <select class="form-control demo-select2-placeholder" name="category_id" id="category_id">
                                
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($category->id); ?>" <?php if($category->id == $sellerComissionDetails->category_id): ?> selected <?php endif; ?>><?php echo e(__($category->name)); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
    				    </div>
    				        <?php
    				          $subcatName = \App\SubCategory::findOrFail($sellerComissionDetails->subcategory_id);
    				          $SubsubcatName = \App\SubSubCategory::findOrFail($sellerComissionDetails->subsubcategory_id);
    				          
    				        ?>
    				    <div class="col-lg-6 form-group">
				            <label>Sub Category</label>
			                <select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id">
                               <?php if($sellerComissionDetails->subcategory_id): ?>
                                  
			                    <option value="<?php echo e($sellerComissionDetails->subcategory_id); ?>"><?php echo e($subcatName->name); ?></option>
			                    <?php endif; ?>
                            </select>
    				    </div>
    				    <div class="col-lg-6 form-group">
				            <label>Sub Category 2</label>
			                <select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id">
                                <?php if($sellerComissionDetails->subsubcategory_id): ?>
			                    <option value="<?php echo e($sellerComissionDetails->subsubcategory_id); ?>"><?php echo e($SubsubcatName->name); ?></option>
			                    <?php endif; ?>
                            </select>
    				    </div>
    				    
    				    <div class="col-lg-6 form-group">
				            <label>Commission Type</label>
			                <select class="form-control demo-select2-placeholder" name="commissionType" id="marg">
                                <option value="0" <?php if($sellerComissionDetails->commission_type ==0 ): ?> echo 'selected';<?php endif; ?></option>Inclusive</option>
                                <option value="1" <?php if($sellerComissionDetails->commission_type ==1 ): ?> echo 'selected';<?php endif; ?>>Exclusive</option>
                            </select>
    				    </div>
    				    
    				   
    				    
    				    <?php
    				  
    				    if($sellerComissionDetails->commision_choose == 'r'){
    				       
    				       $commiionRupease = $sellerComissionDetails->commission;
    				    }else{
    				       $commisionPercentage =  $sellerComissionDetails->commission;
    				    }
    				    
    				    ?>
    				     <div class="col-lg-6 form-group">
				            <label>Choose Commission</label>
			                <select class="form-control demo-select2-placeholder" name="commision_choose" id="commision_ch">
                                <option value="p" <?php if($sellerComissionDetails->commision_choose =='p' ): ?> echo 'selected';<?php endif; ?>>Percentage</option>
                                <option value="r" <?php if($sellerComissionDetails->commision_choose =='r' ): ?> echo 'selected';<?php endif; ?>>Rupease</option>
                            </select>
    				    </div>
    				    
    				    <div id="pe">
    				    <div class="col-lg-12 form-group">
				            <label>Commission in percentage</label>
			                <select class="form-control demo-select2-placeholder percent" name="commission" id="marg">
			                     <?php if($sellerComissionDetails->commission): ?>
			                    <option value="<?php echo e($sellerComissionDetails->commission); ?>"><?php echo e($sellerComissionDetails->commission); ?>%</option>
			                    <?php endif; ?>
                               
                            </select>
    				    </div>
    				    </div>
    				    <div id="re" style="display: none;">
    				    <div class="col-lg-12 form-group">
				            <label>Commission in Rupease %</label>
			                 <input type="text" class="form-control" name="commision_rupease" value="<?php echo e($sellerComissionDetails->commission); ?>">
    				    </div>
                        </div>
                        
                        
    				</div>
				    </div>
    				
				</div>
				<div class="panel-footer text-center">
					<button type="submit" name="button" class="btn btn-purple">Submit</button>
				</div>
		</div>
	</form>
</div>

<div class="row">
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all h-100">
            <h3 class="panel-title pull-left pad-no">Commission List</h3>
           
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Sub Category 2</th>
                    <th>Commission %</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $sellerCommission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sellerCommissionList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php
                             if($sellerCommissionList->commission_type ==0 ){
                               $sellerTYpe = 'Inclusive';
                             }else{
                                 $sellerTYpe = 'Exclusive';
                             }
                             $catName = '';
                             $subcatName = '';
                             $subsubcategoryName = '';
                             
                            
                             $catName =  \App\Category::where('id', $sellerCommissionList->category_id)->get();
                            
                             
                            
                             $subcatName =  \App\SubCategory::where('id', $sellerCommissionList->subcategory_id)->get();
                            
                             
                            
                             $subsubcategoryName =  \App\SubSubCategory::where('id', $sellerCommissionList->subsubcategory_id)->get();
                           
                             
                          ?>
                        <tr>
                            <td><?php echo e($sellerCommissionList->id); ?></td>
                            <td><?php echo e($sellerTYpe); ?></td>
                             
                            <td> <?php echo e($catName[0]->name ?? ''); ?></td>
                            
                            <td><?php echo e($subcatName[0]->name ?? ''); ?></td>
                            
                            <td><?php echo e($subsubcategoryName[0]->name ?? ''); ?></td>
                            
                            <td><?php echo e($sellerCommissionList->commission); ?></td>
                             <td><a href="<?php echo e(route('sellercomission.show', encrypt($sellerCommissionList->id))); ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style="color:red;" href="#" onclick="deleteCommission(<?php echo e($sellerCommissionList->id); ?>)"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                       
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            
        </div>
    </div>
</div>

<script>
    $(function(){
    var $select = $(".percent");
    for (i=1;i<=100;i++){
        $select.append($('<option></option>').val(i).html(i))
    }
});

$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

	$('#subsubcategory_id').on('change', function() {
	    // get_brands_by_subsubcategory();
		//get_attributes_by_subsubcategory();
	});
	
	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('<?php echo e(route('subcategories.get_subcategories_by_category')); ?>',{_token:'<?php echo e(csrf_token()); ?>', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('<?php echo e(route('subsubcategories.get_subsubcategories_by_subcategory')); ?>',{_token:'<?php echo e(csrf_token()); ?>', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
			$('#subsubcategory_id').append($('<option>', {
				value: null,
				text: null
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}
	
	//Delete commission
	function deleteCommission(id){
	    
	    var r = confirm("Do you want to delete Seller Configuration!");
        if (r == true) {
              $.post('<?php echo e(route('deleteSellerCommisionConfig')); ?>',{_token:'<?php echo e(csrf_token()); ?>', sellerId:id}, function(data){
    		   
    		 location.reload();  
    		});
        } else {
          return false;
        }
	    
	    
	    
	}
	
	 //Choose commision
     
     
     $( "#commision_ch" ).change(function() {
       var type = $(this).val();
       //alert(tyep);
       if(type == 'r'){
           $('#re').show();
           $('#pe').hide();
       }
       
       if(type == 'p'){
           $('#pe').show();
           $('#re').hide();
       }
     });


</script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/sellers/seller_commission_edit.blade.php ENDPATH**/ ?>