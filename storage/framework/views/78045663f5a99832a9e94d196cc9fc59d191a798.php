

<?php $__env->startSection('content'); ?>
<!-- Seller Style -->
<link type="text/css" href="<?php echo e(asset('frontend/css/seller.css')); ?>" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" <?php if($mobileapp == 1): ?> style="margin-top:75px" <?php endif; ?>>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    <?php if(Auth::user()->user_type == 'seller'): ?>
                        <?php echo $__env->make('frontend.inc.seller_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php elseif(Auth::user()->user_type == 'customer'): ?>
                        <?php echo $__env->make('frontend.inc.customer_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <?php if($errors->any()): ?>
							<div class="alert alert-danger">
								<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
								<ul>
									<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<li><?php echo e($error); ?></li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							</div>
						<?php endif; ?>
                        <!-- Page title -->
                        <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-12">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 text-white">
                                        <?php echo e(__('Manage Profile')); ?>

                                    </h2>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="<?php echo e(route('home')); ?>" class="text-white"><?php echo e(__('Home')); ?></a></li>
                                            <li><a href="<?php echo e(route('dashboard')); ?>" class="text-white"><?php echo e(__('Dashboard')); ?></a></li>
                                            <li class="active"><a href="<?php echo e(route('profile')); ?>" class="text-white"><?php echo e(__('Manage Profile')); ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form class="" action="<?php echo e(route('seller.profile.update')); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    <?php echo e(__('Basic info')); ?>

                                </div>
                                <div class="form-box-content p-3">
                                    <?php if(isset(Auth::user()->name)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Your Name')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Your Name')); ?>" name="name" value="<?php echo e(Auth::user()->name); ?>">
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Your Name')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Your Name')); ?>" name="name" value="<?php echo e(old('name')); ?>">
                                        </div>
                                    </div>
                                    
                                    <?php endif; ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Your Email')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="email" class="form-control mb-3" placeholder="<?php echo e(__('Your Email')); ?>" name="email" value="<?php echo e(Auth::user()->email); ?>" <?php if(isset(Auth::user()->email)): ?> readonly <?php endif; ?>>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Photo')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="photo" id="file-3" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-3" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    <?php echo e(__('Choose image')); ?>

                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                    
                                    <?php if(!isset(Auth::user()->password)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Your Password')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control mb-3" placeholder="<?php echo e(__('New Password')); ?>" name="new_password">
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Confirm Password')); ?> </label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control mb-3" placeholder="<?php echo e(__('Confirm Password')); ?>" name="confirm_password">
                                        </div>
                                    </div>
                                    
                                    <?php endif; ?>
                                </div>
                            </div>
                            
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    <?php echo e(__('Shipping info')); ?>

                                </div>
                                <div class="form-box-content p-3">
                                   <?php if(isset(Auth::user()->address)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Address')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea class="form-control textarea-autogrow mb-3" placeholder="<?php echo e(__('Your Address')); ?>" rows="1" value="<?php echo e((Auth::user()->address) ? Auth::user()->address : old('address')); ?>" name="address" ><?php echo e(Auth::user()->address); ?></textarea>
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Address')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea class="form-control textarea-autogrow mb-3" placeholder="<?php echo e(__('Your Address')); ?>" rows="1" value="<?php echo e(old('address')); ?>" name="address" ></textarea>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                   
                                    
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Country')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <select class="form-control mb-3 selectpicker" data-placeholder="<?php echo e(__('Select your country')); ?>" name="country">
                                                    <?php $__currentLoopData = \App\Country::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($country->name); ?>" <?php if(Auth::user()->country == $country->name) echo "selected";?> ><?php echo e($country->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php if(isset(Auth::user()->city)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('City')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Your City')); ?>" name="city"  value="<?php echo e(Auth::user()->city); ?>">
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('City')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Your City')); ?>" name="city"  value="<?php echo e(old('city')); ?>">
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    
                                    <?php if(isset(Auth::user()->postal_code)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Postal code')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="<?php echo e(__('Your Postal Code')); ?>" name="postal_code"  value="<?php echo e(Auth::user()->postal_code); ?>" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6">
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Postal code')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="<?php echo e(__('Your Postal Code')); ?>" name="postal_code"  value="<?php echo e(old('postal_code')); ?>" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6">
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    
                                    <?php if(isset(Auth::user()->phone)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Phone')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="<?php echo e(__('Your Phone Number')); ?>" name="phone" value="<?php echo e((Auth::user()->phone) ? Auth::user()->phone : old('phone')); ?>" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10" <?php if(isset(Auth::user()->phone)): ?> readonly <?php endif; ?>>
                                        </div>
                                    </div>
                                    <?php else: ?>
                                      <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Phone')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3" placeholder="<?php echo e(__('Your Phone Number')); ?>" name="phone" value="<?php echo e(old('phone')); ?>" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10">
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    <?php echo e(__('Payment Setting')); ?>

                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Cash Payment')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <label class="switch mb-3">
                                                <input value="1" name="cash_on_delivery_status" type="checkbox" <?php if(Auth::user()->seller->cash_on_delivery_status == 1): ?> checked <?php endif; ?>>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Bank Payment')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <label class="switch mb-3">
                                                <input value="1" name="bank_payment_status" type="checkbox" <?php if(Auth::user()->seller->bank_payment_status == 1): ?> checked <?php endif; ?>>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                  
                                <?php if(isset(Auth::user()->seller->bank_name)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Bank Name')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Bank Name')); ?>" value="<?php echo e(Auth::user()->seller->bank_name); ?>" name="bank_name">
                                        </div>
                                    </div>
                                <?php else: ?>
                                <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Bank Name')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Bank Name')); ?>" value="<?php echo e(old('bank_name')); ?>" name="bank_name">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                
                                <?php if(isset(Auth::user()->seller->bank_acc_name)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Bank Account Name')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Bank Account Name')); ?>" value="<?php echo e(Auth::user()->seller->bank_acc_name); ?>" name="bank_acc_name">
                                        </div>
                                    </div>
                                <?php else: ?>
                                <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Bank Account Name')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Bank Account Name')); ?>" value="<?php echo e(old('bank_acc_name')); ?>" name="bank_acc_name">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset(Auth::user()->seller->bank_acc_no)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Bank Account Number')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Bank Account Number')); ?>" value="<?php echo e(Auth::user()->seller->bank_acc_no); ?>" name="bank_acc_no">
                                        </div>
                                    </div>
                                <?php else: ?>
                                <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Bank Account Number')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Bank Account Number')); ?>" value="<?php echo e(old('bank_acc_no')); ?>" name="bank_acc_no">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                 <?php if(isset(Auth::user()->seller->bank_routing_no)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Bank Routing Number')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control mb-3" placeholder="<?php echo e(__('Bank Routing Number')); ?>" value="<?php echo e(Auth::user()->seller->bank_routing_no); ?>" name="bank_routing_no" >
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Bank Routing Number')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control mb-3" placeholder="<?php echo e(__('Bank Routing Number')); ?>" value="<?php echo e(old('bank_routing_no')); ?>" name="bank_routing_no" >
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset(Auth::user()->order_cancel_credit)): ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Order cancel credit limit')); ?></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="tel" class="form-control mb-3"  value="<?php echo e(Auth::user()->seller->order_cancel_credit); ?>" readonly>
                                        </div>
                                        
                                    </div>
                                </div>
                              
                                
                                <?php endif; ?>
                            </div>
                            <div class="text-right mt-4">
                                <input type="submit" value="Update Profile" class="btn btn-styled btn-base-1">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/seller/profile.blade.php ENDPATH**/ ?>