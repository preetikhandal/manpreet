<?php $__env->startSection('content'); ?>
<?php if($errors->any()): ?>
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e($error); ?></li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		<?php endif; ?>
<div class="col-lg-8 col-lg-offset-2">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo e(__('Brand Information')); ?></h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="<?php echo e(route('brands.store')); ?>" method="POST" enctype="multipart/form-data">
        	<?php echo csrf_field(); ?>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name"><?php echo e(__('Name')); ?></label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="<?php echo e(__('Name')); ?>" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="logo"><?php echo e(__('Logo')); ?> </label>
                    <div class="col-sm-10">
                        <input type="file" id="logo" name="logo" class="form-control">
						<small class="text-danger">Logo size 240px*120px</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="position"><?php echo e(__('Discount (%)')); ?></label>
                    <div class="col-sm-10">
                        <input type="text" min=0 value="0" class="form-control" data-toggle="tooltip" data-placement="top" title="Please Type ND in capital latter" name="brand_discount" placeholder="<?php echo e(__('Brand Discount')); ?>">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo e(__('Meta Title')); ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="meta_title" placeholder="<?php echo e(__('Meta Title')); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo e(__('Meta Description')); ?></label>
                    <div class="col-sm-10">
                        <textarea name="meta_description" rows="8" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo e(__('Meta Keywords')); ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="tags[]" placeholder="Type to add a tag" data-role="tagsinput">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit"><?php echo e(__('Save')); ?></button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<script type="text/javascript">
	window.URL = window.URL || window.webkitURL;
	$("form").submit( function( e ) {
		var form = this;
		e.preventDefault(); //Stop the submit for now
		var fileInput = $(this).find("input[type=file]")[0],
			file = fileInput.files && fileInput.files[0];

		if( file ) {
			var img = new Image();

			img.src = window.URL.createObjectURL( file );

			img.onload = function() {
				var width = img.naturalWidth,
					height = img.naturalHeight;

				window.URL.revokeObjectURL( img.src );

				if( width <= 240 && height <= 120 ) {
					form.submit();
				}
				else {
					alert("Please Upload Given Size of Logo.");
				}
			};
		}
		else { //No file was input or browser doesn't support client side reading
			form.submit();
		}

	});

</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/brands/create.blade.php ENDPATH**/ ?>