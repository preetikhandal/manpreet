 <?php $__env->startSection('content'); ?>
<style>
    .reviewListMain {    padding: 30px 0;}
    
    .reviewListMain .img img {
        width: 100%;
    }
    
    .reviewListMain h2 {font-size: 22px;
    font-weight: 500;
    color: #232f3e;
    text-align: center;}
</style>
<div class="reviewListMain">
    <div class="container">
        <h2>Review List</h2>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-12">
                <!-- <div class="img">
                    <img src="uploads/products/photos/systane-ultra-ophthalmic-solutionmain1598927456.png" alt="img">
                </div> -->
            </div>
            <div class="col-lg-8 col-md-8 col-12">
                <div class="items2">
                    <div class="card">
                        <div class="card-body text-center px-4">
                            <div class="list-group list-group-flush">
                                <div id="productReviews">
                                <?php
                                        $total = 0;
                                        $total += count($productReview);
                                        $commentable = true;
										
                                ?>
                                    <div class="prd-rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><span> Based on <?php echo e($total); ?> review</span></div>
                                    
                                    <?php if(count($productReview) <= 0): ?>
                                            <div class="text-center text14">
                                                <?php echo e(__('There have been no reviews for this product yet.')); ?>

                                            </div>
                                        <?php endif; ?>
                                        <?php $__currentLoopData = $productReview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<div class="review-item text-left">
												<h6 class="review-item_author"><?php echo e($review->user->name); ?></h6>
												<?php for($i=0; $i < $review->rating; $i++): ?>
												    <i class="fa fa-star"></i>
												<?php endfor; ?>
                                                <?php for($i=0; $i < 5-$review->rating; $i++): ?>
                                                    <i class="fa fa-star-o"></i>
                                                <?php endfor; ?>
												<p class="more"><?php echo e($review->comment); ?></p>
											</div>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   
                                    <div class="row">
                                    <?php if(Auth::check()): ?>
                									    <?php if($commentable): ?>
                									    <button type="button" class="btn btn-styled btn-xs-block btn-base-1" onclick="$('#review_modal').modal('show')"><?php echo e(__('Write a product review')); ?></button>
                									    <?php else: ?>
                									    <div class="text-center text14">
                                                            <?php echo e(__('You need to purchase the product to write review.')); ?>

                                                        </div>
                									    <?php endif; ?>
                									<?php else: ?>
                									<a href="<?php echo e(route('user.login')); ?>" class="btn btn-styled btn-xs-block btn-base-1"><?php echo e(__('Login to Write a review')); ?></a>
                									<?php endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if($commentable): ?>
    <div class="modal fade" id="review_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5"><?php echo e(__('Write a review')); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="" action="<?php echo e(route('reviews.store')); ?>" method="POST" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="product_id" value="<?php echo e($detailedProduct->id); ?>">
                    <div class="modal-body gry-bg px-3 pt-3">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="text-uppercase c-gray-light"><?php echo e(__('Your name')); ?></label>
                                    <input type="text" name="name" value="<?php echo e(Auth::user()->name); ?>" class="form-control" disabled required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="text-uppercase c-gray-light"><?php echo e(__('Email')); ?></label>
                                    <input type="text" name="email" value="<?php echo e(Auth::user()->email); ?>" class="form-control" required disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="c-rating mt-1 mb-1 clearfix d-inline-block">
                                    <input type="radio" id="star5" name="rating" value="5" required/>
                                    <label class="star" for="star5" title="Awesome" aria-hidden="true"></label>
                                    <input type="radio" id="star4" name="rating" value="4" required/>
                                    <label class="star" for="star4" title="Great" aria-hidden="true"></label>
                                    <input type="radio" id="star3" name="rating" value="3" required/>
                                    <label class="star" for="star3" title="Very good" aria-hidden="true"></label>
                                    <input type="radio" id="star2" name="rating" value="2" required/>
                                    <label class="star" for="star2" title="Good" aria-hidden="true"></label>
                                    <input type="radio" id="star1" name="rating" value="1" required/>
                                    <label class="star" for="star1" title="Bad" aria-hidden="true"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-12">
                                <textarea class="form-control" rows="4" name="comment" placeholder="<?php echo e(__('Your review')); ?>" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo e(__('Cancel')); ?></button>
                        <button type="submit" class="btn btn-base-1 btn-styled"><?php echo e(__('Send')); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif; ?>


<div class="clearfix"></div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/review-list.blade.php ENDPATH**/ ?>