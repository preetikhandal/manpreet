<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-sm-12">
        <a href="<?php echo e(route('subcategories.create')); ?>" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Subcategory')); ?></a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no"><?php echo e(__('Sub-Categories')); ?></h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_subcategories" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"<?php if(isset($sort_search)): ?> value="<?php echo e($sort_search); ?>" <?php endif; ?> placeholder=" Type name & Enter">
                    </div>
                </div>
                <span style="float: left;margin-top: 6px;">OR</span>
				<div style="margin-left:20px" class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                       <select class="form-control" name="categorySearchID">
							<option value="">SELECT CATEGORY</option>
							<?php $__currentLoopData = \App\Category::get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categories): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($categories->id); ?>" ><?php echo e(strtoupper($categories->name)); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					   </select>
                    </div>
                </div>
				<input type="submit" name="searchdata" class="btn btn-primary" value="Search">
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo e(__('Icon')); ?></th>
                    <th><?php echo e(__('Subcategory')); ?></th>
                    <th><?php echo e(__('Subcategory ID')); ?></th>
                    <th><?php echo e(__('Category')); ?></th>
                    <th><?php echo e(__('Category ID')); ?></th>
                    <th width="10%"><?php echo e(__('Options')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e(($key+1) + ($subcategories->currentPage() - 1)*$subcategories->perPage()); ?></td>
                        <td>
                        	<?php if(!empty($subcategory->icon)): ?>
                        		<img loading="lazy"  class="img-md" src="<?php echo e(asset($subcategory->icon)); ?>" alt="<?php echo e(__('Icon')); ?>">
                        	<?php else: ?>
                        		<img loading="lazy"  class="img-md" src="<?php echo e(asset('img/thumb.jpg')); ?>" alt="<?php echo e(__('Icon')); ?>">
                        	<?php endif; ?>
                        </td>
                        <td><?php echo e(__($subcategory->name)); ?> </td>
                        <td><?php echo e(__($subcategory->id)); ?></td>
                        <td><?php echo e($subcategory->category->name); ?></td>
                        <td><?php echo e($subcategory->category->id); ?></td>
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="<?php echo e(route('subcategories.edit', encrypt($subcategory->id))); ?>"><?php echo e(__('Edit')); ?></a></li>
                                    <li><a onclick="confirm_modal('<?php echo e(route('subcategories.destroy', $subcategory->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                <?php echo e($subcategories->appends(request()->input())->links()); ?>

            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/subcategories/index.blade.php ENDPATH**/ ?>