<?php 

if($order->user_id != null) {
    $CustomerName = $order->user->name;
} else {
     $shipping_address = json_decode($order->shipping_address);
     $CustomerName = $shipping_address->name;
}

$Pcount = $order->orderDetails->count();

$Price = $order->orderDetails->sum('price') + $order->orderDetails->sum('tax');
if($Pcount > 1) {
$Pcount  = $Pcount - 1;
$PName = \Illuminate\Support\Str::limit($order->orderDetails->first()->product->name, 20, '...')." and ". $Pcount ." more items";
} else {
$PName = \Illuminate\Support\Str::limit($order->orderDetails->first()->product->name, 40, '...');
}
?>

Order Placed: selleradmin, Your order for <?php echo e($PName); ?> with order ID# <?php echo e($order->code); ?> worth Rs. <?php echo e($order->grand_total); ?>   has been placed successfully & should get delivered within 5 working days. Thank You for you order from Alde Family. You can track and manage your orders from your account, For any other assistance contact: +919776977612.

<?php /**PATH /home2/aldebaza/staging_script/resources/views/sms/order_confirmation.blade.php ENDPATH**/ ?>