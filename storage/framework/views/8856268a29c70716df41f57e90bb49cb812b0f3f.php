<?php $__env->startSection('content'); ?>
<div class="row">
     <?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
    <?php endif; ?>
	<form class="form form-horizontal mar-top" action="<?php echo e(route('products.cat_export')); ?>" method="POST"  id="choice_form">
		<input name="_method" type="hidden" value="POST">
		
		<?php echo csrf_field(); ?>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo e(__('Product Information')); ?></h3>
			</div>
				<div class="panel-body">

						<div id="demo-stk-lft-tab-1" class="tab-pane fade active in">
							<div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Marg Code')); ?></label>
	                            <div class="col-lg-7">
	                                 <select class="form-control demo-select2-placeholder" name="marg" id="marg">
										
	                                	    <option value="0" ><?php echo e(__('All Product')); ?></option>
	                                	    <option value="1" ><?php echo e(__('In Marg')); ?></option>
	                                	    <option value="2" ><?php echo e(__('Not In Marg ')); ?></option>
	                                	
	                                </select>
	                            </div>
	                        </div>
							<div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Brand Name')); ?></label>
	                            <div class="col-lg-7">
	                                 <select class="form-control demo-select2-placeholder" name="brand_id" id="brand_id">
									 <option value="0" ><?php echo e(__('All Brands')); ?></option>
										<?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                                	    <option value="<?php echo e($brand->id); ?>" ><?php echo e(__($brand->name)); ?></option>
	                                	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                </select>
	                            </div>
	                        </div>
							<div class="form-group" id="category">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Category')); ?></label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
	                                	<option value="0" ><?php echo e(__('All Category')); ?></option>
	                                	<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                                	    <option value="<?php echo e($category->id); ?>" ><?php echo e(__($category->name)); ?></option>
	                                	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group" id="subcategory">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Subcategory')); ?></label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id" required>
										<option value="0" ><?php echo e(__('All Subcategory')); ?></option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group" id="subsubcategory">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Sub Subcategory')); ?></label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id" required>
										<option value="0" ><?php echo e(__('All Sub Subcategory')); ?></option>
	                                </select>
	                            </div>
	                        </div>
	                    </div>
				
				
				</div>
				<div class="panel-footer text-right">
					<button type="submit" name="button" class="btn btn-purple"><?php echo e(__('Export')); ?></button>
				</div>
		</div>
	</form>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<script type="text/javascript">

	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('<?php echo e(route('subcategories.get_subcategories_by_category')); ?>',{_token:'<?php echo e(csrf_token()); ?>', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
			
			$('#subcategory_id').append($('<option>', {
				value: '0',
				text: 'All Subcategory'
			}));
			
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		   /* $("#subcategory_id > option").each(function() {
		        
		            $("#subcategory_id").val(this.value).change();
		      
		    });
*/
		 //   $('.demo-select2').select2();

		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('<?php echo e(route('subsubcategories.get_subsubcategories_by_subcategory')); ?>',{_token:'<?php echo e(csrf_token()); ?>', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
			$('#subsubcategory_id').append($('<option>', {
				value: '0',
				text: 'All Sub Subcategory'
			}));
			
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
			/*
		    $("#subsubcategory_id > option").each(function() {
		      
		            $("#subsubcategory_id").val(this.value).change();
		       
		    });
*/
		 //   $('.demo-select2').select2();

		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}


	$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/products/categoryexport.blade.php ENDPATH**/ ?>