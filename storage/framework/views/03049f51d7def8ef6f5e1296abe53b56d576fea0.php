<?php $__env->startSection('content'); ?>
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
    background:#20b34e!important;
}
</style>
    <section class="gry-bg py-0 py-md-4 profile" <?php if($mobileapp == 1): ?> style="margin-top:80px" <?php endif; ?>>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <?php if(Auth::user()): ?>
                <div class="col-lg-3 d-none d-lg-block">
                        <?php if(Auth::user()->user_type == 'seller'): ?>
                            <?php echo $__env->make('frontend.inc.seller_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php elseif(Auth::user()->user_type == 'customer'): ?>
                            <?php echo $__env->make('frontend.inc.customer_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php endif; ?>
                </div>
                <?php endif; ?>
                <div class="<?php if(Auth::user()): ?> col-lg-9 <?php else: ?> col-lg-12 <?php endif; ?>">
                    <div class="main-content">
                        <div class="row mb-2">
                            <div class="col-12 p-0">
                                <div class="page-title bg-blue p-3" style="border-radius: 0.25rem; ">
                                    <div class="row align-items-center">
                                        <div class="col-md-6 col-12">
                                            <h2 class="heading heading-6 text-capitalize strong-600 mb-0 text-white p-0">
                                                <?php echo e(__('Track Order')); ?>

                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page title -->
                        <form class="" action="<?php echo e(route('orders.track')); ?>" method="GET" enctype="multipart/form-data">
                            <div class="form-box bg-white mt-4" style="border:1px solid #282563">
                                <div class="form-box-title px-3 py-2">
                                    <?php echo e(__('Order Info')); ?>

                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label><?php echo e(__('Order Code')); ?> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Order Code')); ?>" name="order_code" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right py-4">
                                <button type="submit" class="btn btn-styled btn-base-1 bg-blue"><?php echo e(__('Track Order')); ?></button>
                            </div>
                        </form>
                        <?php if(isset($order)): ?>
                            <div class="card mt-4" style="border:1px solid #282563">
                                <div class="card-header py-2 px-3 heading-6 strong-600 clearfix bg-blue">
                                    <div class="float-left text-white"><?php echo e(__('Order Summary')); ?></div>
                                </div>
                                <div class="card-body pb-0" >
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <table class="details-table table">
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Order Code')); ?>:</td>
                                                    <td><?php echo e($order->code); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Customer')); ?>:</td>
                                                    <td><?php echo e(json_decode($order->shipping_address)->name); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Email')); ?>:</td>
                                                    <?php if($order->user_id != null): ?>
                                                        <td><?php echo e($order->user->email); ?></td>
                                                    <?php endif; ?>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Shipping address')); ?>:</td>
                                                    <td><?php echo e(json_decode($order->shipping_address)->address); ?>, <?php echo e(json_decode($order->shipping_address)->city); ?>, <?php echo e(json_decode($order->shipping_address)->country); ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-lg-6">
                                            <table class="details-table table">
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Order date')); ?>:</td>
                                                    <td><?php echo e(date('d-m-Y H:m A', $order->date)); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Total order amount')); ?>:</td>
                                                    <td><?php echo e(single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax'))); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Shipping method')); ?>:</td>
                                                    <td><?php echo e(__('Flat shipping rate')); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Payment method')); ?>:</td>
                                                    <td><?php echo e(ucfirst(str_replace('_', ' ', $order->payment_type))); ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $__currentLoopData = $order->orderDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $orderDetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                    $status = $orderDetail->delivery_status;
                                ?>
                                <div class="card mt-4" style="border:1px solid #282563">
                                    <div class="card-header py-2 px-3 heading-6 strong-600 clearfix bg-blue">
                                        <ul class="process-steps clearfix">
                                            <li <?php if($status == 'pending'): ?> class="active" <?php else: ?> class="done" <?php endif; ?>>
                                                <div class="icon">1</div>
                                                <div class="title text-white"><?php echo e(__('Order placed')); ?></div>
                                            </li>
                                            <li <?php if($status == 'on_review'): ?> class="active" <?php elseif($status == 'on_delivery' || $status == 'delivered'): ?> class="done" <?php endif; ?>>
                                                <div class="icon">2</div>
                                                <div class="title text-white"><?php echo e(__('On review')); ?></div>
                                            </li>
                                            <li <?php if($status == 'on_delivery'): ?> class="active" <?php elseif($status == 'delivered'): ?> class="done" <?php endif; ?>>
                                                <div class="icon">3</div>
                                                <div class="title text-white"><?php echo e(__('On delivery')); ?></div>
                                            </li>
                                            <li <?php if($status == 'delivered'): ?> class="done" <?php endif; ?>>
                                                <div class="icon">4</div>
                                                <div class="title text-white"><?php echo e(__('Delivered')); ?></div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="card-body p-4">
                                        <div class="col-12">
                                            <table class="details-table table">
                                                <?php if($orderDetail->product != null): ?>
                                                    <tr>
                                                        <td class="w-50 strong-600"><?php echo e(__('Product Name')); ?>:</td>
                                                        <td><?php echo e($orderDetail->product->name); ?> <?php echo e($orderDetail->variation); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="w-50 strong-600"><?php echo e(__('Quantity')); ?>:</td>
                                                        <td><?php echo e($orderDetail->quantity); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="w-50 strong-600"><?php echo e(__('Shipped By')); ?>:</td>
                                                        <td><?php echo e($orderDetail->product->user->name); ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
                        <?php endif; ?>
                    </div>
                </div>
            </div>

           
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/track_order.blade.php ENDPATH**/ ?>