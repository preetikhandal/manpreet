<?php $__env->startSection('content'); ?>
<?php if($errors->any()): ?>
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e($error); ?></li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		<?php endif; ?>
<div class="col-sm-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo e(__('Flash Deal Information')); ?></h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" name="flashdeal" action="<?php echo e(route('flash_deals.store')); ?>" method="POST" onsubmit="return validate()" enctype="multipart/form-data">
        	<?php echo csrf_field(); ?>
        	<span class="err text-danger"></span>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="title"><?php echo e(__('Title')); ?></label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="<?php echo e(__('Title')); ?>" id="title" value="<?php echo e(old('title')); ?>" name="title" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="bg_img"><?php echo e(__('Background Image')); ?> </label>
                    <div class="col-sm-9">
                        <input type="file" id="bg_img" name="bg_img" class="form-control" accept="image/x-png,image/jpg,image/jpeg">
						<small class="text-danger">(222px * 250px)</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="background_color"><?php echo e(__('Background Color')); ?> <small>(Hexa-code)</small></label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="<?php echo e(__('#FFFFFF')); ?>" id="background_color" value="<?php echo e(old('background_color')); ?>" name="background_color" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="text_color"><?php echo e(__('Text Color')); ?></label>
                    <div class="col-lg-9">
                        <select name="text_color" id="text_color" class="form-control demo-select2" required>
                            <option value="">Select One</option>
                            <option value="white" <?php if(old('text_color')=='white'): ?> selected <?php endif; ?>><?php echo e(__('White')); ?></option>
                            <option value="dark" <?php if(old('text_color')=='dark'): ?> selected <?php endif; ?>><?php echo e(__('Dark')); ?></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="banner"><?php echo e(__('Banner')); ?> <small>(1920x500)</small></label>
                    <div class="col-sm-9">
                        <input type="file" id="banner" name="banner" class="form-control">
						<em class="text-danger" id="error"></em>
                    </div>
                </div>
                <div class="form-group mb-3">
                    <label class="col-sm-3 control-label" for="start_date"><?php echo e(__('Date')); ?></label>
                    <div class="col-sm-9">
                        <div id="demo-dp-range">
                            <div class="input-daterange input-group" id="datepicker">
                                <input type="text" class="form-control" name="start_date" value="<?php echo e(old('start_date')); ?>">
                                <span class="input-group-addon"><?php echo e(__('to')); ?></span>
                                <input type="text" class="form-control" name="end_date" value="<?php echo e(old('end_date')); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-3" id="category">
                    <label class="col-lg-3 control-label"><?php echo e(__('Category')); ?></label>
                    <div class="col-lg-9">
                        <select class="form-control demo-select2-placeholder" name="category_id" id="category_id">
                             <option>Select Category</option>
                        	<?php $__currentLoopData = \App\Category::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        	    <option value="<?php echo e($category->id); ?>" <?php if(old('category_id')==$category->id): ?> selected <?php endif; ?>><?php echo e(__($category->name)); ?></option>
                        	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
	            <div class="form-group mb-3" id="subcategory">
                    <label class="col-lg-3 control-label"><?php echo e(__('Subcategory')); ?></label>
                    <div class="col-lg-9">
                        <select class="form-control demo-select2-placeholder demo-select3" name="subcategory_id" id="subcategory_id">

                        </select>
                    </div>
                </div>
	            <div class="form-group mb-3" id="subsubcategory">
                    <label class="col-lg-3 control-label"><?php echo e(__('Sub Subcategory')); ?></label>
                    <div class="col-lg-9">
                        <select class="form-control demo-select2-placeholder demo-select3" name="subsubcategory_id" id="subsubcategory_id">

                        </select>
                    </div>
                </div>
                <div class="form-group mb-3">
                    <label class="col-sm-3 control-label" for="products" ><?php echo e(__('Products')); ?></label>
                    <div class="col-sm-9">
                        <select name="products[]" id="products"  class="form-control demo-select2" multiple required data-placeholder="Choose Products">
                            
                        </select>
                    </div>
                </div>
                <br>
                <div class="form-group" id="discount_table">

                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit" id="submitbtn"><?php echo e(__('Save')); ?></button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">
    function validate(){
         var count = $("#products :selected").length;
         var color_count= $("#text_color :selected").length; 
        if(flashdeal.title.value == '' || flashdeal.title.value.length <3){
            showAlert('warning', 'Title length can not be less than 3.');
            return false;
        }else if(flashdeal.background_color.value == '' || flashdeal.background_color.value.length<3){
            showAlert('warning', 'Background Color length can not be less than 3.');
             return false;
        }else if(color_count<=0){
            showAlert('warning', 'Please select text color.');
             return false;
        }else if(count<=0){
            showAlert('warning', 'Please select minimum one product.');
             return false;
        }else{
             return true;
        }
       
    }


	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('<?php echo e(route('subcategories.get_subcategories_by_category')); ?>',{_token:'<?php echo e(csrf_token()); ?>', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		    $("#subcategory_id > option").each(function() {
		        
		            $("#subcategory_id").val(this.value).change();
		      
		    });

		   $('.demo-select3').select2();

		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('<?php echo e(route('subsubcategories.get_subsubcategories_by_subcategory')); ?>',{_token:'<?php echo e(csrf_token()); ?>', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
			$('#subsubcategory_id').append($('<option>', {
			    value: "",
                text: 'Select Sub Categories'
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		/*	
		    $("#subsubcategory_id > option").each(function() {
		      
		            $("#subsubcategory_id").val(this.value).change();
		       
		    });
        */
		   // $('.demo-select3').select2();
           // get_products_by_subsubcategory();
		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}
        $('#subsubcategory_id').on('change', function() {
            get_products_by_subsubcategory();
        });
        
        
        function get_products_by_subsubcategory(){
            var count = $("#products :selected").length;
            var category_id = $('#category_id').val();
            var subcategory_id = $('#subcategory_id').val();
            var subsubcategory_id = $('#subsubcategory_id').val();
            $.post('<?php echo e(route('productsstock.get_product_by_subsubcategory')); ?>',{_token:'<?php echo e(csrf_token()); ?>',category_id:category_id,subcategory_id:subcategory_id, subsubcategory_id:subsubcategory_id}, function(data){
               if(count<=0){
               $('#products').html(null);
                 $('#products').append($('<option>', {
                        value: "",
                        text: 'Select Products'
                    }));
               }
                for (var i = 0; i < data.length; i++) {
                    $('#products').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                }
                $('#products').trigger("change");
            });
        }

	$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

</script>
    <script type="text/javascript">
        $(document).ready(function(){
			//Upload File
			var url = window.URL || window.webkitURL;
			$("#banner").change(function(e) {
				var chosen = this.files[0];
				var type=chosen.type;
				var image = new Image();
				image.onload = function() {
					var imagewidth=this.width;
					var imageheight=this.height;
					if (imagewidth>1920 || imageheight>500)
					{
						$("#error").html("Maximum image width and height should be 1920(W)*500(H)");
						$("#banner").val('');
					}
					else{ $("#error").html(""); }
					
				};
				if(type!="image/jpeg")
				{
					$("#error").html("Not a valid file type: Jpeg or jpg only");
					$("#banner").val('');
					return false;
				}
				else{ $("#error").html(""); }
				image.onerror = function() {
					$("#error").html("Not a valid file type:"+ chosen.type);
					$("#banner").val('');
				};
				image.src = url.createObjectURL(chosen);
			});
			//End Upload File
			
			
			
			
            $('#products').on('change', function(){
                var product_ids = $('#products').val();
                if(product_ids.length > 0){
                    $.post('<?php echo e(route('flash_deals.product_discount')); ?>', {_token:'<?php echo e(csrf_token()); ?>', product_ids:product_ids}, function(data){
                        $('#discount_table').html(data);
                        $('.demo-select2').select2();
                    });
                }
                else{
                    $('#discount_table').html(null);
                }
            });
        });
    </script>
    <script type="text/javascript">
    	$('INPUT[type="file"]').change(function () {
    		var ext = this.value.match(/\.(.+)$/)[1];
    	    switch (ext) {
    			case 'jpg':
    			case 'jpeg':
    			case 'png':
    			    $(".err").text('');
    				$('#submitbtn').attr('disabled', false);
    				break;
    			default:
    				$(".err").text('This is not an allowed file type.');
    				this.value = '';
    		}
    	});
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/flash_deals/create.blade.php ENDPATH**/ ?>