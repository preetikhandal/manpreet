<?php $__env->startSection('content'); ?>
<?php if($errors->any()): ?>
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e($error); ?></li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		<?php endif; ?>
<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo e(__('Customer Information')); ?></h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="<?php echo e(route('customers.update', $customer->id)); ?>" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
        	<?php echo csrf_field(); ?>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name"><?php echo e(__('Name')); ?></label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="<?php echo e(__('Name')); ?>" id="name" name="name" value="<?php echo e($customer->user->name); ?>" maxlegnth="190" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="email"><?php echo e(__('Email')); ?></label>
                    <div class="col-sm-9">
                        <input type="email" placeholder="<?php echo e(__('Email')); ?>" id="email" name="email" value="<?php echo e($customer->user->email); ?>" maxlegnth="30" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="mobile"><?php echo e(__('Phone')); ?></label>
                    <div class="col-sm-9">
                        <input type="tel" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10" placeholder="<?php echo e(__('Phone')); ?>" id="mobile" name="mobile" value="<?php echo e($customer->user->phone); ?>" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="password"><?php echo e(__('Password')); ?></label>
                    <div class="col-sm-9">
                        <input type="password" placeholder="<?php echo e(__('Password')); ?>" id="password" name="password" class="form-control">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Address"><?php echo e(__('Address')); ?></label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="<?php echo e(__('Address')); ?>" id="address" name="address" value="<?php echo e($customer->user->address); ?>" maxlegnth="300"  class="form-control">
                    </div>
                </div>
               
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Address"></label>
                    <div class="col-sm-3"> 
                        City <br />
                        <input type="text" placeholder="<?php echo e(__('City')); ?>" id="city" name="city" value="<?php echo e($customer->user->city); ?>" maxlegnth="30"  class="form-control">
                    </div>
                    <div class="col-sm-3"> 
                        State <br />
                        <input type="text" placeholder="<?php echo e(__('State')); ?>" id="state" name="state" value="<?php echo e($customer->user->state); ?>" maxlegnth="30" class="form-control">
                    </div>
                    <div class="col-sm-3"> 
                        Pincode <br />
                        <input type="text" placeholder="<?php echo e(__('Pincode')); ?>" id="postal_code" name="postal_code" minlength="6" maxlength="6" value="<?php echo e($customer->user->postal_code); ?>"  class="form-control" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">
                    </div>
                </div>
                
                
                <hr />
                
                
                
                
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo e(__('Current Wallet Balance')); ?></label>
                    <div class="col-sm-9">
                        <input type="text"  name="balance" <?php if($customer->user->balance == null): ?> value="0.00" <?php else: ?> value="<?php echo e($customer->user->balance); ?>" <?php endif; ?>  class="form-control" readonly disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="balance_type"><?php echo e(__('Wallet Balance')); ?></label>
                    <div class="col-sm-3">
                        <select name="balance_type" id="balance_type" class="form-control">
                            <option value="add">Credit (+)</option>
                            <option value="remove">Debit (-)</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" placeholder="<?php echo e(__('balance')); ?>" id="balance" name="balance" value="<?php echo e(old('balance')); ?>" max="10000" class="form-control">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="payment_details"><?php echo e(__('Balance Details')); ?></label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="<?php echo e(__('Balance Details')); ?>" id="payment_details" name="payment_details" value="<?php echo e(old('payment_details')); ?>" maxlegnth="500"  class="form-control">
                        <em>Will show to User Dashboard in Wallet History</em>
                    </div>
                </div>
                
               
                
            </div>
            
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit"><?php echo e(__('Save')); ?></button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/customers/edit.blade.php ENDPATH**/ ?>