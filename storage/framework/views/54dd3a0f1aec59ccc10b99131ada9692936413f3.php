<?php

$home_base_price = home_base_price($product->id);
$home_discounted_base_price = home_discounted_base_price($product->id);

if($home_base_price == "₹ 0.00") {
    $product->current_stock = 0;
    $qty = 0;
}

?>

<?php if($product->current_stock): ?>
 
<article class="list-product slide"  >
	<div class="img-block text-center">
	     
		<a href="<?php echo e(route('product', $product->slug)); ?>" class="thumbnail">
		    <?php if($product->thumbnail_img != null): ?>
		    
			<img class="first-img  img-fluid swiper-lazy" src="<?php echo e(asset($product->thumbnail_img)); ?>" data-src="<?php echo e(asset($product->thumbnail_img)); ?>" alt="<?php echo e(__($product->name)); ?>" />
			<?php else: ?>
			
		    <img class="first-img  img-fluid swiper-lazy" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" alt="<?php echo e(__($product->name)); ?>">
			<?php endif; ?>
		</a>
	</div>
	<?php if($product->current_stock != 0): ?>
    	<?php if($home_base_price != $home_discounted_base_price): ?>
    	<ul class="product-flag">
    		<li class="new discount-price bg-orange"><?php echo e(discount_calulate($product->id,$home_base_price, $home_discounted_base_price )); ?>% off</li>
    	</ul>
    	<?php endif; ?>
	<?php endif; ?>
	
	<div class="product-decs text-center">
		<h2><a href="<?php echo e(route('product', $product->slug)); ?>" class="product-link"> <?php echo Str::limit($product->name, 58, ' ...'); ?></a></h2>
		<div class="pricing-meta">
			<ul>
			    <?php if($product->current_stock == 0): ?>
			    <li class="current-price">&nbsp; &nbsp; &nbsp; &nbsp; </li>
			    <?php else: ?>
    			    <?php if($home_base_price != $home_discounted_base_price): ?>
    				<li class="old-price"><?php echo e($home_base_price); ?></li>
    				<?php endif; ?>
    				<li class="current-price"><?php echo e($home_discounted_base_price); ?></li>
				<?php endif; ?>
			</ul>
		</div>
	</div>

	<div class="add-to-cart-buttons">
	    <?php if($product->current_stock == 0): ?>
	    <button class="btn btn95" type="button" disabled> Out of Stock</button>
	    <?php else: ?>
	    <button class="btn btn25" onclick="addToCart(this, <?php echo e($product->id); ?>, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
	    <button class="btn btn70" onclick="buyNow(this, <?php echo e($product->id); ?>, 1, 'full')" type="button"> Buy Now</button>
	    <?php endif; ?>
	</div>
</article>

<?php endif; ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/partials/product_slider_block.blade.php ENDPATH**/ ?>