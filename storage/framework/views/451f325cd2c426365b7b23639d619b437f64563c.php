<?php $__env->startSection('content'); ?>

<?php if($type != 'Seller'): ?>
    <div class="row">
        <div class="col-lg-12 pull-right">
            <a href="<?php echo e(route('products.create')); ?>" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Product')); ?></a>
        </div>
    </div>
<?php endif; ?>

<br>

<div class="panel">
    <!--Panel heading-->
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no"><?php echo e(__($type.' Products')); ?></h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_products" action="" method="GET">
                <?php if($type == 'Seller'): ?>
                    <div class="box-inline pad-rgt pull-left">
                        <div class="select" style="min-width: 200px;">
                            <select class="form-control demo-select2" id="user_id" name="user_id" onchange="sort_products()">
                                <option value="">All Sellers</option>
                                <?php $__currentLoopData = App\Seller::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $seller): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($seller->user != null && $seller->user->shop != null): ?>
                                        <option value="<?php echo e($seller->user->id); ?>" <?php if($seller->user->id == $seller_id): ?> selected <?php endif; ?>><?php echo e($seller->user->shop->name); ?> (<?php echo e($seller->user->name); ?>)</option>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                <?php endif; ?>
                 <div class="box-inline pad-rgt pull-left">
                        <div class="select" style="min-width: 200px;">
                            <select class="form-control demo-select2" id="trashed" name="trashed" onchange="sort_products()">
                                <option value="0" <?php if($trashed == 0): ?> selected <?php endif; ?>>Without Trashed</option>
                                <option value="1" <?php if($trashed == 1): ?> selected <?php endif; ?>>Trashed Items</option>
                                
                            </select>
                        </div>
                    </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 200px;">
                        <select class="form-control demo-select2" name="type" id="type" onchange="sort_products()">
                            <option value="">Sort by</option>
                            <option value="name,asc" <?php if(isset($col_name , $query)): ?> <?php if($col_name == 'name' && $query == 'asc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Name Assending (123)')); ?></option>
                            <option value="name,desc" <?php if(isset($col_name , $query)): ?> <?php if($col_name == 'name' && $query == 'desc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Name Descending (321)')); ?></option>
                            <option value="product_id,asc" <?php if(isset($col_name , $query)): ?> <?php if($col_name == 'product_id' && $query == 'asc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('SKU Assending (123)')); ?></option>
                            <option value="product_id,desc" <?php if(isset($col_name , $query)): ?> <?php if($col_name == 'product_id' && $query == 'desc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('SKU Descending (321)')); ?></option>
                            <option value="current_stock,asc" <?php if(isset($col_name , $query)): ?> <?php if($col_name == 'current_stock' && $query == 'asc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Stock Assending (123)')); ?></option>
                            <option value="current_stock,desc" <?php if(isset($col_name , $query)): ?> <?php if($col_name == 'current_stock' && $query == 'desc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Stock Descending (321)')); ?></option>
                            <option value="rating,desc" <?php if(isset($col_name , $query)): ?> <?php if($col_name == 'rating' && $query == 'desc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Rating (High > Low)')); ?></option>
                            <option value="rating,asc" <?php if(isset($col_name , $query)): ?> <?php if($col_name == 'rating' && $query == 'asc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Rating (Low > High)')); ?></option>
                            <option value="num_of_sale,desc"<?php if(isset($col_name , $query)): ?> <?php if($col_name == 'num_of_sale' && $query == 'desc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Num of Sale (High > Low)')); ?></option>
                            <option value="num_of_sale,asc"<?php if(isset($col_name , $query)): ?> <?php if($col_name == 'num_of_sale' && $query == 'asc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Num of Sale (Low > High)')); ?></option>
                            <option value="unit_price,desc"<?php if(isset($col_name , $query)): ?> <?php if($col_name == 'unit_price' && $query == 'desc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Base Price (High > Low)')); ?></option>
                            <option value="unit_price,asc"<?php if(isset($col_name , $query)): ?> <?php if($col_name == 'unit_price' && $query == 'asc'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Base Price (Low > High)')); ?></option>
                        </select>
                    </div>
                </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"<?php if(isset($sort_search)): ?> value="<?php echo e($sort_search); ?>" <?php endif; ?> placeholder="Type & Enter">
                    </div>
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo e(__('Align Book Id')); ?></th>
                    <th width="20%">
                    <?php if(isset($col_name , $query)): ?>
                         <?php if($col_name == 'name' && $query == 'asc'): ?>
                        <a href="javascript:$('#type').val('name,desc');sort_products()"><?php echo e(__('Name')); ?> <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        <?php elseif($col_name == 'name' && $query == 'desc'): ?>
                        <a href="javascript:$('#type').val('name,asc');sort_products()"><?php echo e(__('Name')); ?> <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        <?php else: ?>
                        <a href="javascript:$('#type').val('name,asc');sort_products()"><?php echo e(__('Name')); ?></a>
                        <?php endif; ?>
                    <?php else: ?>
                    <a href="javascript:$('#type').val('name,asc');sort_products()"><?php echo e(__('Name')); ?></a>
                    <?php endif; ?>
                    
                    </th>
                    <?php if($type == 'Seller'): ?>
                        <th><?php echo e(__('Seller Name')); ?></th>
                    <?php endif; ?>
                    <th>
                    <?php if(isset($col_name , $query)): ?>
                         <?php if($col_name == 'product_id' && $query == 'asc'): ?>
                        <a href="javascript:$('#type').val('product_id,desc');sort_products()"><?php echo e(__('SKU')); ?> <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        <?php elseif($col_name == 'product_id' && $query == 'desc'): ?>
                        <a href="javascript:$('#type').val('product_id,asc');sort_products()"><?php echo e(__('SKU')); ?> <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        <?php else: ?>
                        <a href="javascript:$('#type').val('product_id,asc');sort_products()"><?php echo e(__('SKU')); ?></a>
                        <?php endif; ?>
                    <?php else: ?>
                    <a href="javascript:$('#type').val('product_id,asc');sort_products()"><?php echo e(__('SKU')); ?></a>
                    <?php endif; ?>
                    
                    </th>
                    <th>
                    <?php if(isset($col_name , $query)): ?>
                        <?php if($col_name == 'num_of_sale' && $query == 'asc'): ?>
                        <a href="javascript:$('#type').val('num_of_sale,desc');sort_products()"><?php echo e(__('Num of Sale')); ?> <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        <?php elseif($col_name == 'num_of_sale' && $query == 'desc'): ?>
                        <a href="javascript:$('#type').val('num_of_sale,asc');sort_products()"><?php echo e(__('Num of Sale')); ?> <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        <?php else: ?>
                        <a href="javascript:$('#type').val('num_of_sale,asc');sort_products()"><?php echo e(__('Num of Sale')); ?></a>
                        <?php endif; ?>
                    <?php else: ?>
                    <a href="javascript:$('#type').val('num_of_sale,asc');sort_products()"><?php echo e(__('Num of Sale')); ?></a>
                    <?php endif; ?>
                    </th>
                    
                    <th>
                    <?php if(isset($col_name , $query)): ?>
                        <?php if($col_name == 'current_stock' && $query == 'asc'): ?>
                        <a href="javascript:$('#type').val('current_stock,desc');sort_products()"><?php echo e(__('Total Stock')); ?> <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        <?php elseif($col_name == 'current_stock' && $query == 'desc'): ?>
                        <a href="javascript:$('#type').val('current_stock,asc');sort_products()"><?php echo e(__('Total Stock')); ?> <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        <?php else: ?>
                        <a href="javascript:$('#type').val('current_stock,asc');sort_products()"><?php echo e(__('Total Stock')); ?></a>
                        <?php endif; ?>
                    <?php else: ?>
                    <a href="javascript:$('#type').val('current_stock,asc');sort_products()"><?php echo e(__('Total Stock')); ?></a>
                    <?php endif; ?>
                    </th>
                    <th>
                    <?php if(isset($col_name , $query)): ?>
                        <?php if($col_name == 'unit_price' && $query == 'asc'): ?>
                        <a href="javascript:$('#type').val('unit_price,desc');sort_products()"><?php echo e(__('Base Price')); ?> <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        <?php elseif($col_name == 'unit_price' && $query == 'desc'): ?>
                        <a href="javascript:$('#type').val('unit_price,asc');sort_products()"><?php echo e(__('Base Price')); ?> <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        <?php else: ?>
                        <a href="javascript:$('#type').val('unit_price,asc');sort_products()"><?php echo e(__('Base Price')); ?></a>
                        <?php endif; ?>
                    <?php else: ?>
                    <a href="javascript:$('#type').val('unit_price,asc');sort_products()"><?php echo e(__('Base Price')); ?></a>
                    <?php endif; ?>
                    </th>
                    <!--<th><?php echo e(__('Todays Deal')); ?></th>-->
                    <th>
                    <?php if(isset($col_name , $query)): ?>
                        <?php if($col_name == 'rating' && $query == 'asc'): ?>
                        <a href="javascript:$('#type').val('rating,desc');sort_products()"><?php echo e(__('Rating')); ?> <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></a>
                        <?php elseif($col_name == 'rating' && $query == 'desc'): ?>
                        <a href="javascript:$('#type').val('rating,asc');sort_products()"><?php echo e(__('Rating')); ?> <i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a>
                        <?php else: ?>
                        <a href="javascript:$('#type').val('rating,asc');sort_products()"><?php echo e(__('Rating')); ?></a>
                        <?php endif; ?>
                    <?php else: ?>
                    <a href="javascript:$('#type').val('rating,asc');sort_products()"><?php echo e(__('Rating')); ?></a>
                    <?php endif; ?>
                    </th>
                    <th><?php echo e(__('Published')); ?></th>
                    <th><?php echo e(__('Featured')); ?></th>
                    <th><?php echo e(__('Options')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e(($key+1) + ($products->currentPage() - 1)*$products->perPage()); ?></td>
                        <td><?php echo e($product->alignbook_pro_id); ?></td>
                        
                        <td>
                             <?php if($product->trashed()): ?>
                             <a class="media-block">
                             <?php else: ?>
                            <a href="<?php echo e(route('product', $product->slug)); ?>" target="_blank" class="media-block">
                                <?php endif; ?>
                                <div class="media-left">
                                    <?php if(empty($product->thumbnail_img)): ?>
                                        <img loading="lazy"  class="img-md" src="<?php echo e(asset('img/thumb.jpg')); ?>" alt="Image">
                                    <?php else: ?>
                                        <img loading="lazy"  class="img-md" src="<?php echo e(asset($product->thumbnail_img)); ?>" alt="Image">
                                    <?php endif; ?>
                                </div>
                                <div class="media-body"><?php echo e(__($product->name)); ?></div>
                            </a>
                        </td>
                         <?php if($type == 'Seller'): ?>
                            <?php
                              $userData =  \App\User::where('id', $product->user_id)->get();
                            ?>
                           <td> <?php echo e($userData[0]->name); ?></td>
                        <?php endif; ?>
                        
                        <td><?php echo e($product->product_id ?? ' '); ?></td>
                       
                        <td><?php echo e($product->num_of_sale); ?> <?php echo e(__('times')); ?></td>
                        <td>
                            <?php
                                $qty = 0;
                                if($product->variant_product){
                                    foreach ($product->stocks as $key => $stock) {
                                        $qty += $stock->qty;
                                    }
                                }
                                else{
                                    $qty = $product->current_stock;
                                }
                                echo $qty;
                            ?>
                        </td>
                        <td><?php echo e(number_format($product->unit_price,2)); ?></td>
                        <!--<td><label class="switch">
                                <input onchange="update_todays_deal(this)" value="<?php echo e($product->id); ?>" type="checkbox" <?php if($product->todays_deal == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>-->
                        <td><?php echo e($product->rating); ?></td>
                        <?php if($product->trashed()): ?>
                             <td>N/A</td><td>N/A</td>
                        <?php else: ?>
                        <td><label class="switch">
                                <input onchange="update_published(this)" value="<?php echo e($product->id); ?>" type="checkbox" <?php if($product->published == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>
                        <td><label class="switch">
                                <input onchange="update_featured(this)" value="<?php echo e($product->id); ?>" type="checkbox" <?php if($product->featured == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>
                        <?php endif; ?>
                        <td>
                            <?php if($product->trashed()): ?>
                                <form action="<?php echo e(route('products.restore')); ?>" method="post" >
                                    <?php echo csrf_field(); ?>
                                    <input type="hidden" name="id" value="<?php echo e($product->id); ?>">
                                    <button class="btn btn-primary">Restore</button>
                                </form>
                                <form action="<?php echo e(route('products.forceDelete')); ?>" method="post" >
                                    <?php echo csrf_field(); ?>
                                    <input type="hidden" name="id" value="<?php echo e($product->id); ?>">
                                    <button class="btn btn-danger">Force Delete</button>
                                </form>
                            <?php else: ?>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <?php if($type == 'Seller'): ?>
                                        <li><a href="<?php echo e(route('products.seller.edit', encrypt($product->id))); ?>"><?php echo e(__('Edit')); ?></a></li>
                                    <?php else: ?>
                                        <li><a href="<?php echo e(route('products.admin.edit', encrypt($product->id))); ?>"><?php echo e(__('Edit')); ?></a></li>
                                    <?php endif; ?>
                                    <li><a href="<?php echo e(route('products.trsnfertoseller', $product->id)); ?>"><?php echo e(__('Transfer To Seller')); ?></a></li>
                                    <li><a onclick="confirm_modal('<?php echo e(route('products.destroy', $product->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                    <li><a href="<?php echo e(route('products.duplicate', $product->id)); ?>"><?php echo e(__('Duplicate')); ?></a></li>
                                </ul>
                            </div>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                <?php echo e($products->appends(request()->input())->links()); ?>

            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('script'); ?>
    <script type="text/javascript">

        $(document).ready(function(){
            //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
        });

        function update_todays_deal(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('<?php echo e(route('products.todays_deal')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Todays Deal updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function update_published(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('<?php echo e(route('products.published')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Published products updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function update_featured(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('<?php echo e(route('products.featured')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Featured products updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function sort_products(el){
            $('#sort_products').submit();
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/products/index.blade.php ENDPATH**/ ?>