<?php
	if (Cache::has('homeCategories')){
	   $homeCategories =  Cache::get('homeCategories');
	} else {
		 $homeCategories = \App\HomeCategory::where('status', 1)->orderBy('position')->get();
		Cache::forever('homeCategories', $homeCategories);
	}
	
	
?>

<?php $__currentLoopData = $homeCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $homekey => $homeCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  
	<?php if($homekey>=3) break; ?>
<section class="mb-2 aldeproductslider">
    <div class="container-fluid bg-white">
		<div class="row d-block d-lg-none">
			<div class="col-12">
				<!--<div class="section-title-1 clearfix pb-1">
					<h3 class="heading-5 strong-700 mb-0 float-left" style="color:#000;">
						<span class="mr-4"><?php echo e(__($homeCategory->category->name)); ?></span>
					</h3>
					<ul class="inline-links float-lg-right nav mt-3 mb-2 m-lg-0">	
						<li class="active">
							<a href="<?php echo e(route('products.category', $homeCategory->category->slug)); ?>" class="d-block active"><?php echo e(__('View All')); ?></a>
						</li>
					</ul>
				</div>-->
				<div class="section-title-1 clearfix pb-1">
					<div class="row">
						<div class="col-8">
							<h3 class="heading-6 strong-600 mb-0 float-left" style="color:#000;">
							   <?php if($homeCategory->display_title): ?>
							<span class="mr-4"><?php echo e(__($homeCategory->display_title)); ?> </span>
							<?php else: ?>
							  <span class="mr-4"><?php echo e(__($homeCategory->category->name)); ?> </span>
							<?php endif; ?>
							</h3>
						</div>
						<div class="col-4 float-left">
							<a href="<?php echo e(route('products.category', $homeCategory->category->slug)); ?>" class="btn text-white" style="background:#232F3E;"><?php echo e(__('View All')); ?></a>
						</div>
					</div>
	        	</div>
			</div>
		</div>
		<div class="row">
		    <?php if($homeCategory->category->background_img!=null): ?>
		        <div class="col-lg-2 p-0 d-none d-lg-block" style="z-index:10;">
    				<a href="<?php echo e(route('products.category', $homeCategory->category->slug)); ?>">
    				    <img src="<?php echo e($homeCategory->category->background_img); ?>" style="width: 100%;height: 100%;">
    				</a>
                </div>
		    <?php else: ?>
                <div class="col-lg-2 stickblock d-none d-lg-block" style="z-index:10; <?php if($homeCategory->category->background_color): ?> background: <?php echo e($homeCategory->category->background_color); ?> <?php else: ?> background: #282563 <?php endif; ?>">
    				
    				
    				 <?php if($homeCategory->display_title): ?>
							<h2 style="<?php if($homeCategory->category->text_color): ?> color: <?php echo e($homeCategory->category->text_color); ?> <?php else: ?> color: #fff <?php endif; ?>"><?php echo e(__($homeCategory->display_title)); ?> </h2>
						<?php else: ?>
							<h2 style="<?php if($homeCategory->category->text_color): ?> color: <?php echo e($homeCategory->category->text_color); ?> <?php else: ?> color: #fff <?php endif; ?>"><?php echo e(__($homeCategory->category->name)); ?> </h2>
						<?php endif; ?>
    				
    				
    				<?php
    				  $customName = str_replace(' ','-',$homeCategory->display_title);
    				?>
    				<a href="<?php echo e(route('products.custom', [$homeCategory->category->slug,$customName])); ?>">View All   &raquo;</a>
                </div>
            <?php endif; ?>
            <div class="col-lg-10 pt-1 px-0 pl-2">
                <div class="best-sell-slider-22">
                <div class="swiper-wrapper" id="<?php echo e($homeCategory->category->slug); ?>_slider" >
                    <?php
                    $home_category_products = Cache::rememberForever('HomeCategory:'.$homeCategory->category->slug, function () use ($homeCategory) {
                            return  filter_products(\App\Product::where('published', 1)->whereIn('id', json_decode($homeCategory->products)))->get();
                            
                    });
                    
                    ?>
                   <?php $__currentLoopData = $home_category_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                        if(\App\Product::select('id')->where('id', $product->id)->first() == null) {
                            continue;
                        }
                        ?>
                           <?php echo $__env->make('frontend.partials.product_slider_block', ['product' => $product], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    				</div>
				 <!-- If we need navigation buttons -->
                <!--<div class="<?php echo e($homeCategory->category->slug); ?>_slider_swiper-button-prev swiper-button-prev d-none d-md-block"></div>-->
                <!--<div class="<?php echo e($homeCategory->category->slug); ?>_slider_swiper-button-next swiper-button-next d-none d-md-block"></div>-->
    			</div>
            </div>
        </div>
	</div>
</section>
	<script>
$('#<?php echo e($homeCategory->category->slug); ?>_slider').slick({
  autoplay:true,
  autoplaySpeed:2000,
  arrows:true,
  prevArrow:'<button type="button" class="slick-prev"></button>',
  nextArrow:'<button type="button" class="slick-next"></button>',
  centerMode:true,
  slidesToShow:5,
  slidesToScroll:5,
  focusOnSelect: false,
   responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
  });
	</script>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/partials/home_categories_section.blade.php ENDPATH**/ ?>