<?php
	$data=\App\Policy::where('name', 'terms')->first();
?>

<?php $__env->startSection('meta_title'); ?><?php echo e($data->meta_title); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('meta_description'); ?><?php echo e($data->description); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<style>
    .offersCondition h2 {     font-size: 24px;
    font-weight: 500;}
    .offersCondition ol { }
    .offersCondition ol li {    font-size: 15px;
    line-height: 30px; }
    .offersCondition ol li a { text-decoration: underline;}
</style>
    <section class="gry-bg py-4 offersCondition">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="p-4 bg-white">
                       <h2>Terms & Conditions : Cashback</h2>
                       <ol>
                           <li>Cashback of 50% is applicable only on the products listed <a href="<?php echo e(url('category/city-haat-products')); ?>">HERE</a></li>
                           <li>The cashback is applicable only on the CityHaat category</li>
                           <li>There is a minimum transaction amount of 500/- to be eligible for cashback</li>
                           <li>There is no maximum limit for cashback</li>
                           <li>The offer is applicable on when the entire transaction is done and the product gets delivered sucessfully</li>
                           <li>Cashback will be credited in your Alde wallet within 24 hours after the items are delivered.</li>
                           <li>If the order is dispatched in multiple shipments, the cashback would be credited proportionately as each shipment is dispatched. For example, if you buy two products, A of Rs 1500 and B of Rs 1000 in the same order and you are eligible for cashback of Rs 2500 and the products get shipped separately, you will receive a cashback of Rs 1500 when the product A is dispatched to you and Rs 1000 when the product B is dispatched</li>
                       </ol>
                       
                       <h2>How to avail Cashback amount from Wallet ?</h2>
                       <ol>
                           <li>Cashback wallet amount can only be used on purchase of products from CityHaat Category.</li>
                           <li>Maximum 10% of your wallet amount can be used on next purchase from CityHaat.</li>
                           <li>Cashback wallet amount is only applicable on CityHaat Category.</li>
                       </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/policies/cashback.blade.php ENDPATH**/ ?>