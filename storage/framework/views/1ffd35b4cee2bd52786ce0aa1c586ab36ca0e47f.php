<?php $__env->startSection('content'); ?>
<div class="text-center">
    <p class="h4 text-uppercase text-bold"><h2><?php echo e($exception->getMessage()); ?></h2></p>
    <hr class="new-section-sm bord-no">
    <div class="pad-top"><a class="btn btn-primary" href="<?php echo e(env('APP_URL')); ?>"><?php echo e(__('Return Home')); ?></a></div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.blank', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/errors/401.blade.php ENDPATH**/ ?>