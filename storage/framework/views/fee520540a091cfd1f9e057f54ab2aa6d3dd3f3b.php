<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-sm-12">
        <!-- <a href="<?php echo e(route('sellers.create')); ?>" class="btn btn-info pull-right"><?php echo e(__('add_new')); ?></a> -->
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no"><?php echo e(__('Customers')); ?></h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_customers" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"<?php if(isset($sort_search)): ?> value="<?php echo e($sort_search); ?>" <?php endif; ?> placeholder=" Type email or name & Enter">
                    </div>
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                    
                    <div class="" style="min-width: 100px;display:inline-block;">
                        <a href="<?php echo e(route('customers.create')); ?>" class="btn btn-success">Add Customer</a>
                        <a href="<?php echo e(route('customers.export')); ?>" class="btn btn-success">Export Customer</a>
                        <a href="<?php echo e(route('userimoprt')); ?>" class="btn btn-success">Import Customer</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    
                    <th><?php echo e(__('Align Book Id')); ?></th>
                    <th><?php echo e(__('Name')); ?></th>
                    <th><?php echo e(__('Email Address')); ?></th>
                    <th><?php echo e(__('Phone')); ?></th>
                    <th><?php echo e(__('Date')); ?></th>
                    <th><?php echo e(__('Wallet Balance')); ?></th>
                    <th><?php echo e(__('Cancel order credit limit')); ?></th>
                    <th><?php echo e(__('No Of bookings')); ?></th>
                    
                    <th width="10%"><?php echo e(__('Options')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e(($key+1) + ($customers->currentPage() - 1)*$customers->perPage()); ?></td>
                        <?php if($customer->user != null): ?>
                        <td><?php echo e($customer->user->alignbook_customer_id); ?></td>
                        <td><?php echo e($customer->user->name); ?></td>
                        <td><?php echo e($customer->user->email); ?></td>
                        <td><?php echo e($customer->user->phone); ?></td>
                        <td> <?php echo e(date('d-M-y', strtotime($customer->user->created_at))); ?></td>
                        <td><?php echo e(single_price($customer->user->balance)); ?><a href="<?php echo e(route('wallet-history',encrypt($customer->id))); ?>">&nbsp;&nbsp;View History</a></td>
                        <td><?php echo e($customer->user->order_cancel_credit); ?></td>
                        <td><a target="_blank" href="<?php echo e(route('orders.index.admin',['key' => encrypt($customer->user_id)])); ?>"><?php echo e(customerBookingsCount($customer->user_id)); ?></a></td>
                        
                        <td>
                            <a class="btn btn-primary" href="<?php echo e(route('customers.edit', encrypt($customer->id))); ?>"><?php echo e(__('Edit')); ?></a>
                            <a class="btn btn-danger" onclick="confirm_modal('<?php echo e(route('customers.destroy', $customer->id)); ?>');"><?php echo e(__('Delete')); ?></a>
                        </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                <?php echo e($customers->appends(request()->input())->links()); ?>

            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        function sort_customers(el){
            $('#sort_customers').submit();
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/customers/index.blade.php ENDPATH**/ ?>