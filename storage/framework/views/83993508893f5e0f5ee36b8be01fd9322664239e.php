<?php $__env->startSection('content'); ?>

    <div class="tab-base">

        <!--Nav Tabs-->
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#demo-lft-tab-1" aria-expanded="true"><?php echo e(__('Home Menu')); ?></a>
            </li>
			<li class="">
                <a data-toggle="tab" href="#demo-lft-tab-2" aria-expanded="true"><?php echo e(__('Home slider')); ?></a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-7" aria-expanded="false"><?php echo e(__('Brand banner')); ?></a>
            </li>
			<li class="">
                <a data-toggle="tab" href="#demo-lft-tab-3" aria-expanded="false"><?php echo e(__('Home banner 1')); ?></a>
            </li>
			<li class="">
                <a data-toggle="tab" href="#demo-lft-tab-4" aria-expanded="false"><?php echo e(__('Home banner 2')); ?></a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-10" aria-expanded="false"><?php echo e(__('Home banner 3')); ?></a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-5" aria-expanded="false"><?php echo e(__('Home categories')); ?></a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-9" aria-expanded="false"><?php echo e(__('Best Selling')); ?></a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#demo-lft-tab-6" aria-expanded="false"><?php echo e(__('Top 20')); ?></a>
            </li>
			<li class="">
                <a data-toggle="tab" href="#demo-lft-tab-8" aria-expanded="false"><?php echo e(__('Offer')); ?></a>
            </li>
        </ul>

        <!--Tabs Content-->
        <div class="tab-content">
			<div id="demo-lft-tab-1" class="tab-pane fade active in">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_menu()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Menu')); ?></a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Home Menu')); ?></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(__('Menu')); ?></th>
                                    <th width="50%"><?php echo e(__('Link')); ?></th>
                                    <th><?php echo e(__('Position')); ?></th>
                                    <th><?php echo e(__('Published')); ?></th>
                                    <th width="10%"><?php echo e(__('Options')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = \App\Menu::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($menu->name); ?></td>
                                        <td><?php echo e($menu->link); ?></td>
										 <td><?php echo e($menu->position); ?></td>
                                        <td><label class="switch">
                                            <input onchange="update_menu_published(this)" value="<?php echo e($menu->id); ?>" type="checkbox" <?php if($menu->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_menu(<?php echo e($menu->id); ?>)"><?php echo e(__('Edit')); ?></a></li>
                                                    <li><a onclick="confirm_modal('<?php echo e(route('menu.destroy', $menu->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-2" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_slider()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Slider')); ?></a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Home slider')); ?></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(__('Photo')); ?></th>
                                    <th><?php echo e(__('Mobile Photo')); ?></th>
                                    <th width="50%"><?php echo e(__('Link')); ?></th>
                                    <th><?php echo e(__('Published')); ?></th>
                                    <th width="10%"><?php echo e(__('Options')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = \App\Slider::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><img loading="lazy"  class="img-md" src="<?php echo e(asset($slider->photo)); ?>" alt="Slider Image"></td>
                                        <td><img loading="lazy"  class="img-md" src="<?php echo e(asset($slider->mobile_photo)); ?>" alt="Slider Image"></td>
                                        <td><?php echo e($slider->link); ?></td>
                                        <td><label class="switch">
                                            <input onchange="update_slider_published(this)" value="<?php echo e($slider->id); ?>" type="checkbox" <?php if($slider->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_slider(<?php echo e($slider->id); ?>)"><?php echo e(__('Edit')); ?></a></li>
                                                    <li><a onclick="confirm_modal('<?php echo e(route('sliders.destroy', $slider->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-3" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_banner_1()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Banner')); ?></a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Home banner')); ?></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(__('Photo')); ?></th>
                                    <th><?php echo e(__('URL')); ?></th>
                                    <th><?php echo e(__('Published')); ?></th>
                                    <th><?php echo e(__('Display Position')); ?></th>
                                    <th width="10%"><?php echo e(__('Options')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = \App\Banner::where('position', 1)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><img loading="lazy"  class="img-md" src="<?php echo e(asset($banner->photo)); ?>" alt="banner Image"></td>
                                        <td><?php echo e($banner->url); ?></td>
                                        <td><label class="switch">
                                            <input onchange="update_banner_published(this)" value="<?php echo e($banner->id); ?>" type="checkbox" <?php if($banner->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td><?php echo e($banner->displayposition); ?></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_home_banner_1(<?php echo e($banner->id); ?>)"><?php echo e(__('Edit')); ?></a></li>
                                                    <li><a onclick="confirm_modal('<?php echo e(route('home_banners.destroy', $banner->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-4" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_banner_2()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Banner')); ?></a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Home banner')); ?></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(__('Photo')); ?></th>
                                    <th><?php echo e(__('URL')); ?></th>
                                    <th><?php echo e(__('Published')); ?></th>
                                    <th><?php echo e(__('Display Position')); ?></th>
                                    <th width="10%"><?php echo e(__('Options')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = \App\Banner::where('position', 2)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><img loading="lazy"  class="img-md" src="<?php echo e(asset($banner->photo)); ?>" alt="banner Image"></td>
                                        <td><?php echo e($banner->url); ?></td>
                                        <td><label class="switch">
                                            <input onchange="update_banner_published(this)" value="<?php echo e($banner->id); ?>" type="checkbox" <?php if($banner->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td><?php echo e($banner->displayposition); ?></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_home_banner_2(<?php echo e($banner->id); ?>)"><?php echo e(__('Edit')); ?></a></li>
                                                    <li><a onclick="confirm_modal('<?php echo e(route('home_banners.destroy', $banner->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-10" class="tab-pane fade">
                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_banner_3()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Banner')); ?></a>
                    </div>
                </div>
                <br>
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Home banner')); ?></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(__('Photo')); ?></th>
                                    <th><?php echo e(__('URL')); ?></th>
                                    <th><?php echo e(__('Published')); ?></th>
                                    <th><?php echo e(__('Display Position')); ?></th>
                                    <th width="10%"><?php echo e(__('Options')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = \App\Banner::where('position', 3)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><img loading="lazy"  class="img-md" src="<?php echo e(asset($banner->photo)); ?>" alt="banner Image"></td>
                                        <td><?php echo e($banner->url); ?></td>
                                        <td><label class="switch">
                                            <input onchange="update_banner_published(this)" value="<?php echo e($banner->id); ?>" type="checkbox" <?php if($banner->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td><?php echo e($banner->displayposition); ?></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_home_banner_3(<?php echo e($banner->id); ?>)"><?php echo e(__('Edit')); ?></a></li>
                                                    <li><a onclick="confirm_modal('<?php echo e(route('home_banners.destroy', $banner->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-5" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-6">
                        <a onclick="add_home_category()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Category')); ?></a>
                    </div>
                    
                    <div class="col-sm-6">
                        <a onclick="add_home_categoryConfig()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Home Category configuration with Position')); ?></a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Home Categories')); ?></h3>
                    </div>
                    <div class="panel-body">
<div class="table-responsive">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(__('Category Display Name')); ?></th>
                                    <th><?php echo e(__('Category')); ?></th>
                                    <th><?php echo e(__('Products')); ?></th>
                                    <th><?php echo e(__('Position')); ?></th>
                                    <th><?php echo e(__('Status')); ?></th>
                                    <th width="10%"><?php echo e(__('Options')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = \App\HomeCategory::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $home_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($home_category->display_title); ?></td>
                                        <td><?php echo e($home_category->category->name); ?></td>
                                        <td>
                                            <?php $__currentLoopData = json_decode($home_category->products); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(\App\Product::find($product_id) != null): ?>
                                                    <span class="badge badge-info"><?php echo e(\App\Product::find($product_id)->name); ?></span>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </td>
										<td><?php echo e($home_category->position); ?></td>
                                        <td><label class="switch">
                                            <input onchange="update_home_category_status(this)" value="<?php echo e($home_category->id); ?>" type="checkbox" <?php if($home_category->status == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                                </button>
                                                
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_home_category(<?php echo e($home_category->id); ?>)"><?php echo e(__('Edit')); ?></a></li>
                                                    <li><a onclick="confirm_modal('<?php echo e(route('home_categories.destroy', $home_category->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
</div>
                    </div>
                </div>
            </div>
            <div id="demo-lft-tab-6" class="tab-pane fade">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Top 20 Information')); ?></h3>
                    </div>

                    <!--Horizontal Form-->
                    <!--===================================================-->
                    <form class="form-horizontal" action="<?php echo e(route('top_10_settings.store')); ?>" method="POST" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3" for="url"><?php echo e(__('Top Categories (Max 20)')); ?></label>
                                <div class="col-sm-9">
                                    <select class="form-control demo-select2-max-10" name="top_categories[]" multiple required>
                                        <?php $__currentLoopData = \App\Category::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($category->id); ?>" <?php if($category->top == 1): ?> selected <?php endif; ?>><?php echo e($category->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                           <div class="form-group" style="display:none">
                                <label class="col-sm-3" for="url"><?php echo e(__('Top Brands (Max 20)')); ?></label>
                                <div class="col-sm-9">
                                    <select class="form-control demo-select2-max-10" name="top_brands[]" multiple required>
                                        <?php $__currentLoopData = \App\Brand::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($brand->id); ?>" <?php if($brand->top == 1): ?> selected <?php endif; ?>><?php echo e($brand->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-right">
                            <button class="btn btn-purple" type="submit"><?php echo e(__('Save')); ?></button>
                        </div>
                    </form>
                    <!--===================================================-->
                    <!--End Horizontal Form-->

                </div>
            </div>
			<div id="demo-lft-tab-7" class="tab-pane fade">

                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_brand_banner()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Brand Banner')); ?></a>
                    </div>
                </div>

                <br>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Home Brand Banner')); ?></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(__('Photo')); ?></th>
                                    <th width="50%"><?php echo e(__('Link')); ?></th>
                                    <th><?php echo e(__('Published')); ?></th>
                                    <th width="10%"><?php echo e(__('Options')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = \App\Brandbanner::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $Bbanner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><img loading="lazy"  class="img-md" src="<?php echo e(asset($Bbanner->photo)); ?>" alt="Slider Image"></td>
                                        <td><?php echo e($Bbanner->link); ?></td>
                                        <td><label class="switch">
                                            <input onchange="update_Bbanner_published(this)" value="<?php echo e($Bbanner->id); ?>" type="checkbox" <?php if($Bbanner->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
													<li><a onclick="edit_brandbanner(<?php echo e($Bbanner->id); ?>)"><?php echo e(__('Edit')); ?></a></li>
                                                    <li><a onclick="confirm_modal('<?php echo e(route('brandbanner.destroy', $Bbanner->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div id="demo-lft-tab-8" class="tab-pane fade">
				<div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_popup_offer()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add offer')); ?></a>
                    </div>
                </div>
                <br>
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Popup Offer')); ?></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(__('Photo')); ?></th>
                                    <th><?php echo e(__('URL')); ?></th>
                                    <th><?php echo e(__('Published')); ?></th>
                                    <th width="10%"><?php echo e(__('Options')); ?></th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php $__currentLoopData = \App\popup_offer::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><img loading="lazy"  class="img-md" src="<?php echo e(asset($offer->photo)); ?>" alt="Popup Offer Image"></td>
                                        <td><?php echo e($offer->url); ?></td>
                                        <td><label class="switch">
                                            <input onchange="update_popup_offer_published(this)" value="<?php echo e($offer->id); ?>" type="checkbox" <?php if($offer->published == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_popup_offer(<?php echo e($offer->id); ?>)"><?php echo e(__('Edit')); ?></a></li>
                                                    <li><a onclick="confirm_modal('<?php echo e(route('popup_offer.destroy', $offer->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            
            <div id="demo-lft-tab-9" class="tab-pane fade">
                <div class="row">
                    <div class="col-sm-12">
                        <a onclick="add_best_selling()" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New Product')); ?></a>
                    </div>
                </div>
                <br>
                
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(__('Best Selling Products')); ?></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo e(__('Category')); ?></th>
                                    <th><?php echo e(__('Products')); ?></th>
                                    <!--<th><?php echo e(__('Position')); ?></th>-->
                                    <th><?php echo e(__('Status')); ?></th>
                                    <th width="10%"><?php echo e(__('Options')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = \App\BestSelling::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $best_selling): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($best_selling->category->name); ?></td>
                                        <td>
                                            <?php $__currentLoopData = json_decode($best_selling->products); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(\App\Product::find($product_id) != null): ?>
                                                    <span class="badge badge-info"><?php echo e(\App\Product::find($product_id)->name); ?></span>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </td>
										<!--<td><?php echo e($best_selling->position); ?></td>-->
                                        <td><label class="switch">
                                            <input onchange="update_best_selling_status(this)" value="<?php echo e($best_selling->id); ?>" type="checkbox" <?php if($best_selling->status == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_best_selling(<?php echo e($best_selling->id); ?>)"><?php echo e(__('Edit')); ?></a></li>
                                                    <li><a onclick="confirm_modal('<?php echo e(route('best_selling.destroy', $best_selling->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
	   </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<script type="text/javascript">

    function updateSettings(el, type){
        if($(el).is(':checked')){
            var value = 1;
        }
        else{
            var value = 0;
        }
        $.post('<?php echo e(route('business_settings.update.activation')); ?>', {_token:'<?php echo e(csrf_token()); ?>', type:type, value:value}, function(data){
            if(data == 1){
                showAlert('success', 'Settings updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }
	
	function add_menu(){
        $.get('<?php echo e(route('menu.create')); ?>', {}, function(data){
            $('#demo-lft-tab-1').html(data);
        });
    }
	
	function edit_menu(id){
        var url = '<?php echo e(route("menu.edit", "menu_id")); ?>';
        url = url.replace('menu_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-1').html(data);
        });
    }


    function add_slider(){
        $.get('<?php echo e(route('sliders.create')); ?>', {}, function(data){
            $('#demo-lft-tab-2').html(data);
        });
    }
	function edit_slider(id){
        var url = '<?php echo e(route("sliders.edit", "home_slider_id")); ?>';
        url = url.replace('home_slider_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-2').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
    function update_slider_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
       // var url = '<?php echo e(route('sliders.update_status', 'slider_id')); ?>';
        //url = url.replace('slider_id', el.value);
		$.post('<?php echo e(route('sliders.update_status')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
			
        //$.post(url, {_token:'<?php echo e(csrf_token()); ?>', status:status, _method:'PATCH'}, function(data){
            if(data == 1){
                showAlert('success', 'Published sliders updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }
	function add_brand_banner(){
        $.get('<?php echo e(route('brandbanner.create')); ?>', {}, function(data){
            $('#demo-lft-tab-7').html(data);
        });
    }
	
	function edit_brandbanner(id){
        var url = '<?php echo e(route("brandbanner.edit", "Brandbanner")); ?>';
        url = url.replace('Brandbanner', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-7').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
	
	function update_Bbanner_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('<?php echo e(route('brandbanner.update_status')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Brand Banner status updated successfully');
            }
            else{
                showAlert('danger', 'There is some error occurred. Please try again.');
            }
        });
    }
	
	/*Start popup offer*/
	function add_popup_offer(){
        $.get('<?php echo e(route('popup_offer.create')); ?>', {}, function(data){
            $('#demo-lft-tab-8').html(data);
        });
    }
	function edit_popup_offer(id){
        var url = '<?php echo e(route("popup_offer.edit", "popup_offer_id")); ?>';
        url = url.replace('popup_offer_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-8').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
	function update_popup_offer_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('<?php echo e(route('popup_offer.update_status')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Popup Offer status updated successfully');
            }
            else{
                showAlert('danger', 'There is some error occurred. Please try again.');
            }
        });
    }
	/*End popup offer*/

	function add_banner_1(){
        $.get('<?php echo e(route('home_banners.create', 1)); ?>', {}, function(data){
            $('#demo-lft-tab-3').html(data);
        });
    }

    function add_banner_2(){
        $.get('<?php echo e(route('home_banners.create', 2)); ?>', {}, function(data){
            $('#demo-lft-tab-4').html(data);
        });
    }
    function add_banner_3(){
        $.get('<?php echo e(route('home_banners.create', 3)); ?>', {}, function(data){
            $('#demo-lft-tab-10').html(data);
        });
    }
   
    function edit_home_banner_1(id){
        var url = '<?php echo e(route("home_banners.edit", "home_banner_id")); ?>';
        url = url.replace('home_banner_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-3').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
	

    function edit_home_banner_2(id){
        var url = '<?php echo e(route("home_banners.edit", "home_banner_id")); ?>';
        url = url.replace('home_banner_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-4').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
    
    function edit_home_banner_3(id){
        var url = '<?php echo e(route("home_banners.edit", "home_banner_id")); ?>';
        url = url.replace('home_banner_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-10').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
    
    function add_home_category(){
        $.get('<?php echo e(route('home_categories.create')); ?>', {}, function(data){
            $('#demo-lft-tab-5').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
    
    function add_home_categoryConfig(){
        $.get('<?php echo e(route('home_categories.create')); ?>', {'config':true}, function(data){
            $('#demo-lft-tab-5').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

    function edit_home_category(id){
        var url = '<?php echo e(route("home_categories.edit", "home_category_id")); ?>';
        url = url.replace('home_category_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-5').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

    function update_home_category_status(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('<?php echo e(route('home_categories.update_status')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Home Page Category status updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }

    function update_banner_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('<?php echo e(route('home_banners.update_status')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Banner status updated successfully');
                location.reload();
            }
            else{
                showAlert('danger', 'Maximum 4 banners to be published.');
            }
        });
    }
	

    function update_menu_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('<?php echo e(route('menu.update_status')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Published Menu updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }
	
    
    function add_best_selling(){
        $.get('<?php echo e(route('best_selling.create')); ?>', {}, function(data){
            $('#demo-lft-tab-9').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }
	
	function edit_best_selling(id){
        var url = '<?php echo e(route("best_selling.edit", "best_selling_id")); ?>';
        url = url.replace('best_selling_id', id);
        $.get(url, {}, function(data){
            $('#demo-lft-tab-9').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

	function update_best_selling_status(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('<?php echo e(route('best_selling.update_status')); ?>', {_token:'<?php echo e(csrf_token()); ?>', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Best Selling Category status updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/home_settings/index.blade.php ENDPATH**/ ?>