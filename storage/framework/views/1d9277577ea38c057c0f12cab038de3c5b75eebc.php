<?php $__env->startSection('content'); ?>
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
    background:#20b34e!important;
}
    .panel-order .row {
    	border-bottom: 1px solid #ccc;
    }
    .panel-order .row:last-child {
    	border: 0px;
    }
    .panel-order .row .col-md-1  {
    	text-align: center;
    	padding-top: 15px;
    }
    .panel-order .row .col-md-1 img {
    	width: 50px;
    	max-height: 50px;
    }
    .panel-order .row .row {
    	border-bottom: 0;
    }
    .panel-order .row .col-md-11 {
    	border-left: 1px solid #ccc;
    }
    .panel-order .row .row .col-md-12 {
    	padding-top: 7px;
    	padding-bottom: 7px; 
    }
    .panel-order .row .row .col-md-12:last-child {
    	font-size: 11px; 
    	color: #555;  
    	background: #efefef;
    }
    .panel-order .btn-group {
    	margin: 0px;
    	padding: 0px;
    }
    .panel-order .panel-body {
    	padding-top: 0px;
    	padding-bottom: 0px;
    }
    .panel-order .panel-deading {
    	margin-bottom: 0;
    } 
    .prodname{
        font-weight:bold;
        font-size:14px;
    }
    .prodname::after{
        content:",";
    }
    .prodname:last-child::after{
        content:" ";
    }
    .total,.totalpricevalue{
        font-size:13px;
    }
    .totalpricevalue{font-weight:800;}
    .label {
      color: white;
      padding: 5px;
    }
</style>
<!-- Seller Style -->
<link type="text/css" href="<?php echo e(asset('frontend/css/seller.css')); ?>" rel="stylesheet" media="all">
    <section class="bg-white py-0 py-md-4 profile" <?php if($mobileapp == 1): ?> style="margin-top:75px" <?php endif; ?>>
        <div class="container-fluid d-none d-lg-block">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    <?php echo $__env->make('frontend.inc.customer_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="col-lg-9">
                    <!-- Page title -->
                    <div class="page-title bg-blue p-3" style="border-radius: 0.25rem; ">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0 text-white">
                                    <?php echo e(__('Dashboard')); ?>

                                </h2>
                            </div>
                        </div>
                    </div>

                    <!-- dashboard content -->
                    <div class="">
                        <div class="row">
                            <div class="col-md-4 col-6">
                                <div class="dashboard-widget text-center green-widget mt-4 c-pointer">
                                    <a href="<?php echo e(route('cart')); ?>" class="d-block">
                                        <i class="fa fa-shopping-cart"></i>
                                        <?php if(Session::has('cart')): ?>
                                            <span class="d-block title"><?php echo e(count(Session::get('cart'))); ?> <?php echo e(__('Product(s)')); ?></span>
                                        <?php else: ?>
                                            <span class="d-block title">0 <?php echo e(__('Product')); ?></span>
                                        <?php endif; ?>
                                        <span class="d-block sub-title"><?php echo e(__('in your cart')); ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-6">
                                <div class="dashboard-widget text-center red-widget mt-4 c-pointer">
                                    <a href="<?php echo e(route('wishlists.index')); ?>" class="d-block">
                                        <i class="fa fa-heart"></i>
                                        <span class="d-block title"><?php echo e(count(Auth::user()->wishlists)); ?> <?php echo e(__('Product(s)')); ?></span>
                                        <span class="d-block sub-title"><?php echo e(__('in your wishlist')); ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="dashboard-widget text-center bg-blue mt-4 c-pointer">
                                    <a href="<?php echo e(route('purchase_history.index')); ?>" class="d-block">
                                        <i class="fa fa-building"></i>
                                        <?php
                                          /*
                                            $orders = \App\Order::where('user_id', Auth::user()->id)->get();
                                            $total = 0;
                                            foreach ($orders as $key => $order) {
                                                $total += count($order->orderDetails);
                                            }
                                            */
                                        ?>
                                        <span class="d-block title"> <?php echo e(__('Your Orders')); ?></span>
                                        <span class="d-block sub-title text-white"><?php echo e(__('Track, return, or buy things again')); ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2 clearfix ">
                                        <h4 class="float-left"><?php echo e(__('Recent Orders')); ?></h4>
                                        <a href="<?php echo e(route('purchase_history.index')); ?>" class="btn bg-blue float-right"><?php echo e(__('View All')); ?></a>
                                    </div>
                                    <?php
                                        $orders = \App\Order::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->take(3)->get();
                                    ?>
									<?php if(count($orders)!=0): ?>
                                        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $order->orderDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $orderDetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <article class="card card-product-list">
                                                	<div class="row no-gutters align-items-center">
                                                		<div class="col-md-2 col-3">
                                                			<a href="<?php echo e(route('product', $orderDetail->product->slug)); ?>" >
                                            			    	<?php if($orderDetail->product->thumbnail_img!=""): ?>
                                                			    	<img src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" data-src="<?php echo e(asset($orderDetail->product->thumbnail_img)); ?>" alt="<?php echo e($orderDetail->product->name); ?>" class="img-fluid lazyload">
                                                			    <?php else: ?>
                                                			        <img src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" alt="<?php echo e($orderDetail->product->name); ?>" class="img-fluid lazyload">
                                                			    <?php endif; ?>
                                                			</a>
                                                		</div> 
                                                		<div class="col-md-7 col-9 ">
                                                			<div class="info-main ">
                                                			    <?php if($orderDetail->product != null): ?>
                                                			    	<a href="<?php echo e(route('product', $orderDetail->product->slug)); ?>" target="_blank" class="h5 title"><?php echo e($orderDetail->product->name); ?></a>
                                                		        <?php else: ?>
                                                                    <strong><?php echo e(__('Product Unavailable')); ?></strong>
                                                                <?php endif; ?>
                                                		        <p class="info">Order 
                                                		            <a href="#<?php echo e($order->code); ?>" onclick="show_purchase_history_details(<?php echo e($order->id); ?>)" class="text-blue font-weight-bold"># <?php echo e($order->code); ?></a> 
                                                		            <span class="block-sm">Delivery Status
                                                    		            <?php
                                                                            $status = $order->orderDetails->first()->delivery_status;
                                                                        ?>
                                                                        <?php if(str_replace('_', ' ', $status)=="pending"): ?>
                                                                            <span class="badge badge-info"> <?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></span>
                                                                        <?php else: ?>
                                                                            <span class="badge badge-success"> <?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></span>
                                                                        <?php endif; ?>
                                                		            </span>
                                                		        </p>
                                                		        <p class="info">Order Date <b class="text-blue"><?php echo e(date('d-m-Y', $order->date)); ?> </b>
                                                		           <span class="block-sm">Payment Status 
                                                		             <?php if($order->payment_status == 'paid'): ?>
                                                		                <span class="badge badge-success"> <?php echo e(__('Paid')); ?></span>
                                                                     <?php else: ?>
                                                                        <span class="badge badge-warning"><?php echo e(__('Unpaid')); ?></span>
                                                                     <?php endif; ?>
                                                		           </span>
                                                		        </p>
                                                		        <span class="price h5 d-block d-md-none">Product Price <?php echo e(single_price($orderDetail->price)); ?> </span>
                                                			</div>
                                                		</div> 
                                                		<div class="col-12 d-block d-md-none mb-2 px-2 mobilebtn">
                                                		    <a href="#<?php echo e($order->code); ?>" onclick="show_purchase_history_details(<?php echo e($order->id); ?>)" class="btn  btn-light btn50 float-left bg-blue"> Details </a>
                                        		            <a href="<?php echo e(route('customer.invoice.download', $order->id)); ?>" class="btn btn-light bg-red btn50">Download Invoice</a>
                                                		</div>
                                                		<div class="col-md-3 d-none d-md-block">
                                                			<div class="info-aside">
                                                				<div class="price-wrap">
                                                					<span class="price h5"><span class="info">Product Price</span> <?php echo e(single_price($orderDetail->price)); ?> </span>	
                                                				</div> <br/>
                                                				<p class="d-none d-md-block">
                                                					<a href="#<?php echo e($order->code); ?>" onclick="show_purchase_history_details(<?php echo e($order->id); ?>)" class="btn btn-light bg-blue btn-block">View Details </a>
                                                					<a href="<?php echo e(route('customer.invoice.download', $order->id)); ?>" class="btn btn-light btn-block bg-red">Download Invoice</a>
                                                				</p>
                                                			</div> 
                                                		</div> 
                                                	</div> 
                                                </article>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php else: ?>
										<div class="card no-border mt-4">
											<div>
												<table class="table table-sm table-hover table-responsive-md">
													<thead>
														<tr class="text-center">
															<th><h3><?php echo e(__('There are no recent orders to show.')); ?></h3></th>
														</tr>
													</thead>
												</table>
											</div>
										</div>
									<?php endif; ?>
                                </div>
                            </div>
                            <!--<div class="col-md-6">
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2 clearfix ">
                                        <?php echo e(__('Purchased Package')); ?>

                                    </div>
                                    <?php
                                        $customer_package = \App\CustomerPackage::find(Auth::user()->customer_package_id);
                                    ?>
                                    <div class="form-box-content p-3">
                                        <?php if($customer_package != null): ?>
                                            <div class="form-box-content p-2 category-widget text-center">
                                                <center><img alt="Package Logo" src="<?php echo e(asset($customer_package->logo)); ?>" style="height:100px; width:90px;"></center>
                                                <left> <strong><p><?php echo e(__('Product Upload')); ?>: <?php echo e($customer_package->product_upload); ?> <?php echo e(__('Times')); ?></p></strong></left>
                                                <strong><p><?php echo e(__('Product Upload Remaining')); ?>: <?php echo e(Auth::user()->remaining_uploads); ?> <?php echo e(__('Times')); ?></p></strong>
                                                <strong><p><div class="name mb-0"><?php echo e(__('Current Package')); ?>: <?php echo e($customer_package->name); ?> <span class="ml-2"><i class="fa fa-check-circle" style="color:green"></i></span></div></p></strong>
                                            </div>
                                        <?php else: ?>
                                            <div class="form-box-content p-2 category-widget text-center">
                                                <center><strong><p><?php echo e(__('Package Removed')); ?></p></strong></center>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>-->
                        
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="container-fluid d-block d-lg-none" style="max-width:100%!important">
            <div class="row">
                <div class="col-12">
                    <div class="page-title">
                        <div class="row align-items-center bg-blue py-2">
                            <div class="col-6">
                                <h2 class="heading heading-3 text-capitalize strong-600 mb-0 p-0 text-white">
                                    <?php echo e(__('My account')); ?>

                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mobileaccount">
                    <div class="widget-profile-menu py-3">
                        <ul class="categories categories--style-3">
                            <li>
                                <a href="<?php echo e(route('purchase_history.index')); ?>">
                                    <span class="category-name">
                                        <?php echo e(__('Your Orders')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('wishlists.index')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Wishlist')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('orders.track')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Track Order')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('customer_refund_track')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Track Refund Request')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('profile')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Manage Profile')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <?php if(\App\BusinessSetting::where('type', 'wallet_system')->first()->value == 1): ?>
                                <li>
                                    <a href="<?php echo e(route('wallet.index')); ?>" >
                                        <span class="category-name">
                                            <?php echo e(__('My Wallet')); ?>

                                        </span>
                                        <i class="fa fa-chevron-right float-right"></i>
                                    </a>

                                </li>
                            <?php endif; ?>
                            <li>
                                    <a href="<?php echo e(route('cashback.index')); ?>" >
                                        <span class="category-name">
                                            <?php echo e(__('My Cashback')); ?>

                                        </span>
                                        <i class="fa fa-chevron-right float-right"></i>
                                    </a>
                                    
                                </li>
                                <li>
                                    <a href="<?php echo e(route('commission')); ?>">
                                        <span class="category-name">
                                            <?php echo e(__('Category Commission')); ?>

                                        </span>
                                        <i class="fa fa-chevron-right float-right"></i>
                                    </a>
                                    
                                </li>
                            <li>
                                <a href="<?php echo e(route('support_ticket.index')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Support Ticket')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>  
    </section>

    <div class="modal fade p-0" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        $('#order_details').on('hidden.bs.modal', function () {
           // location.reload();
        })
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/customer/dashboard.blade.php ENDPATH**/ ?>