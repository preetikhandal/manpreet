<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
   	<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"/>
    <!--<![endif]-->
	<title>Alde Bazaar</title>
	<!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
	

	<style type="text/css" media="screen">
table{border:0px solid black;}
		/* Linked Styles */
		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none }
		a { color:#66b7f0; text-decoration:none }
		p { padding:0 !important; margin:0 !important } 
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }
		
		/* Mobile styles */
		@media  only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			.mobile-shell { width: 100% !important; min-width: 100% !important; }
			
			.m-center { text-align: center !important; }
			
			.center { margin: 0 auto !important; }
			
			.td { width: 100% !important; min-width: 100% !important; }

			.m-br-3 { height: 3px !important; }
			.m-br-4 { height: 4px !important; background: #f4f4f4 !important; }
			.m-br-15 { height: 15px !important; }
			.m-br-25 { height: 25px !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

			.column-top,
			.column { float: left !important; width: 100% !important; display: block !important; }

			.content-spacing { width: 15px !important; }

			.m-bg { display: block !important; width: 100% !important; height: auto !important; background-position: center center !important; }

			.h-auto { height: auto !important; }

			.plr-15 { padding-left: 15px !important; padding-right: 15px !important; }

			.w-2 { width: 2% !important; }
			.w-49 { width: 49% !important; }

			.pb-4 { padding-bottom: 4px !important; }
			.pb-15 { padding-bottom: 15px !important; }

			.pt-0 { padding-top: 0 !important; }

			.h1,
			.h1-white { font-size: 36px !important; line-height: 46px !important; }
			.h2 { font-size: 26px !important; line-height: 36px !important; }

			.text-btn-large,
			.text-btn-large-white { padding: 15px 25px !important; }
			.text-btn,
			.text-btn-1,
			.text-btn-1-white { padding: 12px 15px !important; }
		}
	</style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
			<td align="center" valign="top">
			
				<!-- Header -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						<td align="center" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="img-center" style="padding: 45px 15px; font-size:0pt; line-height:0pt; text-align:center;">
									<a href="<?php echo e(url('/')); ?>" target="_blank"><img src="<?php echo e(asset('email/logo_2.png')); ?>" width="225" border="0" alt="" /></a></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Header -->

				<!-- Nav -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						<td align="center" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="5" class="mobile-shell" bgcolor="#ffffff">
								<tr>
									<td class="text-nav fw-medium" style="padding: 20px 15px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:23px; line-height:21px; text-align:center; font-weight:500;">
										 <span class="link-2" style="color:#4a4a4a; text-decoration:none;">User Registration Confirmation</span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Nav -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="min-height:300px;">
					<tr>
						<td align="center" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell" bgcolor="#ffffff">
								<tr>
																		<td class="h1 fw-medium" style="padding-bottom: 10px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:15px; line-height:30px; text-align:left; font-weight:500;">
																			Dear <?php echo e($data['name']); ?>,
																		</td>
																	</tr>
																	<tr>
																		<td class="text-1" style="padding-bottom: 30px; color:#4a4a4a; font-family:'Poppins', Arial,sans-serif; font-size:16px; line-height:30px; text-align:justify;">
																		Thank you for registered with us as a User. <br />
																		<br />
																		With Regards, <br />
																		Team - Alde Bazaar
																		</td>
																	</tr>
																	
							</table>
						</td>
					</tr>
				</table>
				<!-- Footer -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center" valign="top">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="plr-15" style="padding: 40px 30px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<th class="column-top" valign="top" width="225" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
													<div class="img-center" style="font-size:0pt; line-height:0pt; text-align:center;"><a href="<?php echo e(url('/')); ?>" target="_blank"><img src="<?php echo e(asset('email/logo_2.png')); ?>" border="0" width="225" alt="" /></a></div>
												</th>
												<th class="column-top" valign="top" width="15" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;"><div style="font-size:0pt; line-height:0pt;" class="m-br-15"></div>
</th>
												<th class="column-top" valign="top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td align="right" style="padding: 15px 0 25px 0;">
																<!-- Socials -->
																<table border="0" cellspacing="0" cellpadding="0" class="center">
																	<tr>
																		<td class="img" width="11" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://www.facebook.com/Alde-Bazaar-100442855141856/" target="_blank"><img src="<?php echo e(asset('email/ico_facebook_light.png')); ?>" width="11" height="22" border="0" alt="" /></a></td>
																		<td class="img" width="40" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																		<td class="img" width="23" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://twitter.com/AldeBazaar" target="_blank"><img src="<?php echo e(asset('email/ico_twitter_light.png')); ?>" width="23" height="22" border="0" alt="" /></a></td>
																		<td class="img" width="36" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																		<td class="img" width="22" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="https://www.instagram.com/aldebazaar/" target="_blank"><img src="<?php echo e(asset('email/ico_instagram_light.png')); ?>" width="22" height="22" border="0" alt="" /></a></td>
																	</tr>
																</table>
																<!-- END Socials -->
															</td>
														</tr>
														
													</table>
												</th>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Footer -->

				
			</td>
		</tr>
	</table>
</body>
</html>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/emails/user_register.blade.php ENDPATH**/ ?>