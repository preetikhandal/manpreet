<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<input type="hidden" id="totalpages" value="<?php echo e($products->lastPage()); ?>">
    <?php
        $home_base_price = home_base_price($product->id);
        $home_discounted_base_price = home_discounted_base_price($product->id);
        if($home_base_price == "₹ 0.00") {
            $product->current_stock = 0;
            $qty = 0;
        }
    ?>
<div class="col-md-3 col-6" id="results">
    <article class="list-product">
		<div class="img-block">
			 <?php if($mobileapp == 0): ?>
				<a href="<?php echo e(route('product', $product->slug)); ?>" class="thumbnail">
				<?php else: ?>
				<a href="<?php echo e(route('product', ['slug' => $product->slug,  'android' => 1])); ?>" class="thumbnail">
				<?php endif; ?>
			    <?php if($product->thumbnail_img != null): ?>
				<img class="mx-auto d-block lazyload  img-fluid" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" data-src="<?php echo e(asset($product->thumbnail_img)); ?>" alt="<?php echo e(__($product->name)); ?>" />
				<?php else: ?>
			    <img class="mx-auto d-block lazyload  img-fluid" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" alt="<?php echo e(__($product->name)); ?>">
				<?php endif; ?>
			</a>
		</div>
		<?php if($product->current_stock != 0): ?>
			<?php if($home_base_price != $home_discounted_base_price): ?>
			<ul class="product-flag">
				<li class="new discount-price bg-orange"><?php echo e(discount_calulate($home_base_price, $home_discounted_base_price )); ?>% off</li>
			</ul>
			<?php endif; ?>
		<?php endif; ?>
		<div class="product-decs text-center">
			<?php if($mobileapp == 0): ?>
			<h2><a href="<?php echo e(route('product', $product->slug)); ?>" class="product-link"><?php echo e(__(ucwords(strtolower($product->name)))); ?></a></h2>
			<?php else: ?>
			<h2><a href="<?php echo e(route('product', ['slug' => $product->slug,  'android' => 1])); ?>" class="product-link"><?php echo e(__(ucwords(strtolower($product->name)))); ?></a></h2>
			<?php endif; ?>
			<div class="pricing-meta">
				<ul>
				    <?php if($product->current_stock == 0): ?>
    			    <li class="current-price">&nbsp; &nbsp; &nbsp; &nbsp; </li>
    			    <?php else: ?>
					    <?php if($home_base_price != $home_discounted_base_price): ?>
						<li class="old-price"><?php echo e($home_base_price); ?></li>
						<?php endif; ?>
						<li class="current-price"><?php echo e($home_discounted_base_price); ?></li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
		<div class="add-to-cart-buttons">
		    <?php if($product->current_stock == 0): ?>
		    <button class="btn btn95" type="button" disabled> Out of Stock</button>
		    <?php else: ?>
		    <button class="btn btn25" onclick="addToCart(this, <?php echo e($product->id); ?>, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
		    <button class="btn btn70" onclick="buyNow(this, <?php echo e($product->id); ?>, 1, 'full')" type="button"> Buy Now</button>
		    <?php endif; ?>
		    <?php if(env('BARGAIN_BTN',false)): ?>
			<?php $bargain_cat = explode(",",env('BARGAIN_CATEGORY','')); ?>
			    <?php if(in_array($product->category_id, $bargain_cat)): ?>
			    <a href="<?php echo e(env('BARGAIN_LINK','')); ?>" target="_blank" class="btn btn95" style="margin-top:5px;width:calc(90% + 4px);">
                <?php echo e(__('For best lowest price Click')); ?>

				</a>
		        <?php endif; ?>
		    <?php endif; ?>
		</div>
	</article>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<!--
<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div class="col-xxl-2 col-xl-3 col-lg-3 col-md-4 col-6" id="results">
		<div class="product-box-2 bg-white alt-box my-md-2">
			<div class="position-relative overflow-hidden">
				<a href="<?php echo e(route('product', $product->slug)); ?>" class="d-block product-image h-100 text-center" tabindex="0">
					<?php if($product->thumbnail_img!=NULL): ?>
						<img class="img-fit lazyload mx-auto" src="<?php echo e(asset('frontend/images/placeholder.jpg')); ?>" data-src="<?php echo e(asset($product->thumbnail_img)); ?>" alt="<?php echo e(__($product->name)); ?>">
					<?php else: ?>
						<img class="img-fit lazyload mx-auto" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" alt="<?php echo e(__($product->name)); ?>">
					<?php endif; ?>
				</a>
				<div class="product-btns clearfix">
                    <button class="btn add-wishlist" title="Add to Wishlist" onclick="addToWishList(<?php echo e($product->id); ?>)" type="button">
                        <i class="la la-heart-o"></i>
                    </button>
                    <button class="btn add-cart" title="Add to Wishlist" onclick="addToCart(this, <?php echo e($product->id); ?>,1)" type="button">
                        <i class="la la-cart-plus"></i>
                    </button>
                    
                   
                    <button class="btn quick-view" title="Quick view" onclick="showAddToCartModal(<?php echo e($product->id); ?>)" type="button">
                        <i class="la la-eye"></i>
                    </button>
                </div>
			</div>
			<div class="p-md-3 p-2">
				<input type="hidden" id="totalpages" value="<?php echo e($products->lastPage()); ?>">
				<div class="price-box">
				 <?php
			    $home_base_price = home_base_price($product->id);
			    $home_discounted_base_price = home_discounted_base_price($product->id);
			    ?>
				<?php if($home_base_price != $home_discounted_base_price): ?>
					<del class="old-product-price strong-400"><?php echo e($home_base_price); ?> </del>
				  <span class="product-price strong-600"><?php echo e($home_discounted_base_price); ?> <span style="color:red;font-size:14px;">(<?php echo e(discount_calulate($home_base_price, $home_discounted_base_price )); ?>% off)</span></span>
                <?php else: ?>
				<span class="product-price strong-600"><?php echo e($home_discounted_base_price); ?> </span>
				<?php endif; ?>
				</div>
				<div class="star-rating star-rating-sm mt-1">
					<?php echo e(renderStarRating($product->rating)); ?>

				</div>
				<h2 class="product-title p-0">
					<a href="<?php echo e(route('product', $product->slug)); ?>" class=" text-truncate"><?php echo e(__($product->name)); ?></a>
				</h2>
				<?php if(\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated): ?>
					<div class="club-point mt-2 bg-soft-base-1 border-light-base-1 border">
						<?php echo e(__('Club Point')); ?>:
						<span class="strong-700 float-right"><?php echo e($product->earn_point); ?></span>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
<?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/product_listing_ajax.blade.php ENDPATH**/ ?>