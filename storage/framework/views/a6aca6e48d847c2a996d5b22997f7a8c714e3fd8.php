
<section class="mb-2">
    <div class="container-fluid bg-white">
        <div class="py-2 px-md-2 py-md-1 p-md-1">
            <!--<div class="section-title-1 clearfix">
                <h3 class="heading-5 strong-700 mb-0 float-left">
                    <span class="mr-4"><?php echo e(__('Featured Products')); ?></span>
                </h3>
            </div>-->
            <div class="caorusel-box arrow-round gutters-5">
                <div class="slick-carousel brandbanner" data-slick-items="5" data-slick-xl-items="5" data-slick-lg-items="5"  data-slick-md-items="5" data-slick-sm-items="3" data-slick-xs-items="2"  >
					<?php
						if (Cache::has('Brandbanner')){
						   $Brandbanner =  Cache::get('Brandbanner');
						} else {
							 $Brandbanner = \App\Brandbanner::where('published', 1)->get();
							Cache::forever('Brandbanner', $Brandbanner);
						}
					?>
                    <?php $__currentLoopData = $Brandbanner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="caorusel-card" style="padding-left: 2px!important; padding-right: 2px!important;">   
						<div class="card-body p-0">
							<div class="card-image">
								<!--<a href="<?php echo e(route('product', $product->link)); ?>" class="d-block">-->
								<a href="<?php if(isset($product->link)): ?> <?php echo e(url($product->link)); ?> <?php else: ?> javascript:void(0) <?php endif; ?>" class="d-block">
									<img class="img-fit lazyload mx-auto" src="<?php echo e(asset('frontend/images/placeholder1.jpg')); ?>" data-src="<?php echo e(asset($product->photo)); ?>" alt="<?php echo e(__($product->id)); ?>" style="min-height:147px,max-height:147px">
								</a>
							</div>
						</div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/partials/brand_banner_section.blade.php ENDPATH**/ ?>