<?php $__env->startSection('content'); ?>
    <?php
        $status = $order->orderDetails->first()->delivery_status;
    ?>
<style>
    .product-item {
        padding: 15px;
        background: #f3f3f3;
        margin-top: 20px;
        position: relative;
    }
    .product-item:after {
        content: ".";
        display: block;
        height: 0;
        clear: both;
        visibility: hidden;
        font-size: 0;
        line-height:0;
    }
    
    .product-item h3 {
        font-size: 14px;
        font-weight: 600;
        padding-bottom: 4px;
    }
    .product-item h3 a:hover {
        color: #EB3038;
    }
    .pi-price {
        color: #000;
        font-size: 18px;
        float: left;
        padding-top: 1px;
    }
    .label{
        background:#282563;
        color:#fff;
        border-radius:4px;
    }
</style>
    <div id="page-content">
        <section class="processing slice-xs sct-color-2 border-bottom" <?php if($mobileapp == 1): ?> style="margin-top:80px" <?php endif; ?>>
            <div class="container-fluid">
                <div class="row cols-delimited justify-content-center">
                    <div class="process">
                        <div class="process-row">
                            <div class="process-step">
                                <a href="<?php echo e(route('cart')); ?>" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-2x text-white"></i></a>
                                <p>My Cart</p>
                            </div>
                            <div class="process-step">
                                <a href="<?php echo e(route('checkout.shipping_info')); ?>" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-map-o fa-2x text-white"></i></a>
                                <p>Shipping Info</p>
                            </div>
                            <div class="process-step">
                                <a href="javascript:;" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-credit-card fa-2x text-white"></i></a>
                                <p>Payment</p>
                            </div> 
                             <div class="process-step">
                                <a href="javascript:;" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-check-circle-o fa-2x text-white"></i></a>
                                <p>Confirmation</p>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-4">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-xl-8 mx-auto p-0 p-md-2">
                        <div class="card">
                            <div class="card-body p-1 p-md-2">
                                <div class="text-center py-4 border-bottom mb-4">
                                    <i class="la la-check-circle la-3x text-success mb-3"></i>
                                    <h1 class="h3 mb-3"><?php echo e(__('Thank You for Your Order!')); ?></h1>
                                    <h2 class="h5 strong-700"><?php echo e(__('Order Number:')); ?> <?php echo e($order->code); ?></h2>
                                    <?php if($order->user_id != null): ?>
                                        <p class="text-muted text-italic"><?php echo e(__('A copy or your order summary has been sent to')); ?> <?php echo e($order->user->email); ?></p>
                                    <?php endif; ?>
                                </div>
                                <div class="mb-4">
                                    <h5 class="strong-600 mb-3 border-bottom pb-2"><?php echo e(__('Order Summary')); ?></h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="details-table table">
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Order Code')); ?>:</td>
                                                    <td><?php echo e($order->code); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Name')); ?>:</td>
                                                    <td><?php echo e(json_decode($order->shipping_address)->name); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Email')); ?>:</td>
                                                    <?php if($order->user_id != null): ?>
                                                        <td><?php echo e($order->user->email); ?></td>
                                                    <?php endif; ?>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Shipping address')); ?>:</td>
                                                    <td><?php echo e(json_decode($order->shipping_address)->address); ?>, <?php echo e(json_decode($order->shipping_address)->city); ?>, <?php echo e(json_decode($order->shipping_address)->country); ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <table class="details-table table">
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Order date')); ?>:</td>
                                                    <td><?php echo e(date('d-m-Y H:m A', $order->date)); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Order status')); ?>:</td>
                                                    <td><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Total order amount')); ?>:</td>
                                                    <td><?php echo e(single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax'))); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Shipping')); ?>:</td>
                                                    <td><?php echo e(__('Flat shipping rate')); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="w-50 strong-600"><?php echo e(__('Payment method')); ?>:</td>
                                                    <td><?php echo e(ucfirst(str_replace('_', ' ', $order->payment_type))); ?></td>
                                                </tr>
                                                        <?php $total = $order->orderDetails->sum('price')+$order->orderDetails->sum('tax')?>
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <h5 class="strong-600 mb-3 border-bottom pb-2"><?php echo e(__('Order Details')); ?></h5>
                                    <div class="container">
                                        <div class="row">
                                    	<?php $__currentLoopData = $order->orderDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $orderDetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-12 ">
                                                <div class="product-item">
                                                   <?php if($orderDetail->product != null): ?>
                                                        <h3><a class="text-blue" href="<?php echo e(route('product', $orderDetail->product->slug)); ?>" target="_blank">
                                                            <?php echo e($orderDetail->product->name); ?> <?php echo e($orderDetail->variation); ?>

                                                        </a></h3>
                                                    <?php else: ?>
                                                        <strong><?php echo e(__('Product Unavailable')); ?></strong><br/>
                                                    <?php endif; ?>
                                                    <strong class="pb-2">Qty : <?php echo e($orderDetail->quantity); ?> </strong><br/>
                                                        <div class="pi-price">
            				        <?php if($orderDetail->discount != 0): ?>
            				        <del style="color:red"><?php echo e(single_price($orderDetail->price + $orderDetail->discount)); ?></del>  <br />
            				        <?php endif; ?>
            				        <?php echo e(single_price($orderDetail->price)); ?></div><br/><br/>
                                                        
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
									<hr/>
									<style>
										.details-table th {padding: .45rem!important;}
									</style>
                                    <?php
                                               
                                               $shipping = 0;
                                               $deliveryCharge = 0;
                                               $shipType = false;
                                               $shipData = Session::get('shippingData');
                                             
                                               if(isset($shipData['paymentOption'])){
                                                   if($shipData['paymentOption'] == 'COD'){
                           
                                                       if($shipData['totalAmount'] >= 1 && $shipData['totalAmount'] <= 499){
                                                           
                                                           $shipType = true;
                                                           $deliveryCharge = 50;
                                                       }else if($shipData['totalAmount']  >= 500 && $shipData['totalAmount'] <=  999){
                                                           $shipType = true;
                                                           $deliveryCharge = 30;
                                                       }else if($shipData['totalAmount'] >=  1000){
                                                           $shipType = true;
                                                           $deliveryCharge = 0;
                                                       }
                                                   }else if($shipData['paymentOption'] == 'razorpay'){
                                                    
                                                       if($shipData['totalAmount'] >= 1 && $shipData['totalAmount'] <= 499){
                                                           
                                                           $shipType = true;
                                                           $deliveryCharge = 30;
                                                       }else if($shipData['totalAmount']  >= 500 && $shipData['totalAmount'] <=  999){
                                                           $shipType = true;
                                                           $deliveryCharge = 0;
                                                       }else if($shipData['totalAmount'] >=  1000){
                                                           $shipType = true;
                                                           $deliveryCharge = 0;
                                                       }
                                                       
                           
                                                   }
                                                  
                                                
                                           }else{  
                                               if(isset($total)){
                                                   if($total < 1000){
                                                     $shipType = true;
                                                     $deliveryCharge = 30;
                                                   }
                                               }
                                                      
                                           }
                                                 $totalA = $order->orderDetails->sum('price');
                                                 $tax = $order->orderDetails->sum('tax');
                                                 $productShi = $order->orderDetails->sum('shipping_cost');
                                                 $subTotal = $totalA + $tax+$productShi+$deliveryCharge; 
                                                 
                                               ?>
                                    <div class="row">
                                        <div class="col-xl-5 col-md-6 ml-auto">
                                            <table class="table details-table">
                                                <tbody>
                                                    <tr>
                                                        <th><?php echo e(__('Subtotal')); ?></th>
                                                        <td class="text-right">
                                                            <span class="strong-600"><?php echo e(single_price($order->orderDetails->sum('price'))); ?></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th><?php echo e(__('Product Shipping')); ?></th>
                                                        <td class="text-right">
                                                            <span class="text-italic"><?php echo e(single_price($order->orderDetails->sum('shipping_cost'))); ?></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th><?php echo e(__('Tax')); ?></th>
                                                        <td class="text-right">
                                                            <span class="text-italic"><?php echo e(single_price($order->orderDetails->sum('tax'))); ?></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th><?php echo e(__('Coupon Discount')); ?></th>
                                                        <td class="text-right">
                                                            <span class="text-italic"><?php echo e(single_price($order->coupon_discount)); ?></span>
                                                        </td>
                                                    </tr>
                                                    <?php if($deliveryCharge > 0): ?>
                                                    <tr>
                                                        <th><?php echo e(__('Delivery Charges')); ?></th>
                                                        <td class="text-right">
                                                            <span class="text-italic"><?php echo e(single_price($deliveryCharge)); ?></span>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>
                                                    <?php if($order->wallet_credit > 0): ?>
                                                    <tr>
                                                        <th><?php echo e(__('Wallet Credit Use')); ?></th>
                                                        <td class="text-right">
                                                            <span class="text-italic"><?php echo e(single_price($order->wallet_credit)); ?></span>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>
                                                     
                                                    <tr>
                                                        <th><span class="strong-600"><?php echo e(__('Total')); ?></span></th>
                                                        <td class="text-right">
                                                            <strong><span><?php echo e(single_price($order->grand_total)); ?></span></strong>
                                                        </td>
                                                    </tr>
                                                        <?php 
                                                        $totalDiscount = $order->orderDetails->sum('discount');
                                                        ?>
                                                        
                                                        <?php if($totalDiscount > 0): ?>
                                                            <tr style="color:red">
                                                                <th><?php echo e(__('Total Saving on this Order')); ?></th>
                                                                <td class="text-right">
                                                                    <span class="text-italic"  style="color:red"><?php echo e(single_price($totalDiscount)); ?></span>
                                                                </td>
                                                            </tr>
                                                        <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="row">
					<div class="col-xxl-6 col-lg-8 col-md-10 mx-auto">
						<div class="text-center bg-gray py-4">
							<a href="<?php echo e(url('/')); ?>" class="btn btn-base-1 bg-blue">Continue Shopping</a>
						</div>
					</div>

                    <div class="col-xxl-6 col-lg-8 col-md-10 mx-auto">
						<div class="text-center bg-gray py-4">
							<a href="<?php echo e(url('feedback-form')); ?>" class="btn btn-base-1 bg-blue">Feedback</a>
						</div>
					</div>
				</div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/order_confirmed.blade.php ENDPATH**/ ?>