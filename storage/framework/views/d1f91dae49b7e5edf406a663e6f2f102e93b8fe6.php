<?php if(isset($subsubcategory_id)): ?>
    <?php
        $meta_title = \App\SubSubCategory::find($subsubcategory_id)->meta_title;
        $meta_description = \App\SubSubCategory::find($subsubcategory_id)->meta_description;
        $meta_keywords = \App\SubSubCategory::find($subsubcategory_id)->tags;
    ?>
<?php elseif(isset($subcategory_id)): ?>
    <?php
        $meta_title = \App\SubCategory::find($subcategory_id)->meta_title;
        $meta_description = \App\SubCategory::find($subcategory_id)->meta_description;
        $meta_keywords = \App\SubCategory::find($subcategory_id)->meta_keywords;
    ?>
<?php elseif(isset($category_id)): ?>
    <?php
        $meta_title = \App\Category::find($category_id)->meta_title;
        $meta_description = \App\Category::find($category_id)->meta_description;
        $meta_keywords = \App\Category::find($category_id)->meta_keywords;
    ?>
<?php elseif(isset($brand_id)): ?>
    <?php
        $meta_title = \App\Brand::find($brand_id)->meta_title;
        $meta_description = \App\Brand::find($brand_id)->meta_description;
        $meta_keywords = \App\Brand::find($brand_id)->tags;
    ?>
<?php else: ?>
    <?php
        $meta_title = \App\SeoSetting::first()->meta_title;
        $meta_description = \App\SeoSetting::first()->description;
        $meta_keywords = \App\SeoSetting::first()->keyword;
    ?>
<?php endif; ?>


<?php $__env->startSection('meta_title'); ?><?php echo e($meta_title); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_description'); ?><?php echo e($meta_description); ?><?php $__env->stopSection(); ?>
<?php $__env->startSection('meta_keywords'); ?><?php echo e($meta_keywords); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="<?php echo e($meta_title); ?>">
    <meta itemprop="description" content="<?php echo e($meta_description); ?>">

    <!-- Twitter Card data -->
    <meta name="twitter:title" content="<?php echo e($meta_title); ?>">
    <meta name="twitter:description" content="<?php echo e($meta_description); ?>">

    <!-- Open Graph data -->
    <meta property="og:title" content="<?php echo e($meta_title); ?>" />
    <meta property="og:description" content="<?php echo e($meta_description); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<style> .aldeproductslider .list-product{ margin: 0px 0px 5px 0px; }
.active_sub a:before {
    content: "\f105";
    font: normal normal normal 16px/1 FontAwesome;
    margin-right:5;
    text-rendering: optimizeLegibility;
    text-transform: none;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    font-smoothing: antialiased;
    opacity: 0.7;
    font-size: 90%;
    margin-right: 5px;
}

</style>
    <div class="breadcrumb-area" <?php if($mobileapp == 1): ?> style="margin-top:80px" <?php endif; ?>>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <?php if($mobileapp == 0): ?>
                    <ul class="breadcrumb">
                        <li><a href="<?php echo e(route('home')); ?>"><?php echo e(__('Home')); ?></a></li>
                        <li><a href="<?php echo e(route('categories.all')); ?>"><?php echo e(__('All Categories')); ?></a></li>
                        <?php if(isset($subsubcategory_id)): ?>
                            <?php
                            $SubSubCategory = \App\SubSubCategory::find($subsubcategory_id);
                            $SubCategory = \App\SubCategory::find($SubSubCategory->sub_category_id);
                            $Category = \App\Category::find($SubCategory->category_id);
                            ?>
                            <li ><a href="<?php echo e(route('products.category', $Category->slug)); ?>"><?php echo e($Category->name); ?></a></li>
                            <li ><a href="<?php echo e(route('products.subcategory', [$Category->slug,$SubCategory->slug])); ?>"><?php echo e($SubCategory->name); ?></a></li>
                            <li class="active"><a href="<?php echo e(route('products.subsubcategory', [$Category->slug, $SubCategory->slug,$SubSubCategory->slug])); ?>"><?php echo e($SubSubCategory->name); ?></a></li>
                        <?php elseif(isset($subcategory_id)): ?>
                            <li ><a href="<?php echo e(route('products.category', \App\SubCategory::find($subcategory_id)->category->slug)); ?>"><?php echo e(\App\SubCategory::find($subcategory_id)->category->name); ?></a></li>
                            <li class="active"><a href="<?php echo e(route('products.subcategory', [\App\SubCategory::find($subcategory_id)->category->slug, \App\SubCategory::find($subcategory_id)->slug])); ?>"><?php echo e(\App\SubCategory::find($subcategory_id)->name); ?></a></li>
                        <?php elseif(isset($category_id)): ?>
                            <li class="active"><a href="<?php echo e(route('products.category', \App\Category::find($category_id)->slug)); ?>"><?php echo e(\App\Category::find($category_id)->name); ?></a></li>
                        <?php endif; ?>
                    </ul>
                    <?php else: ?>
                    <ul class="breadcrumb">
                        <li><a href="<?php echo e(route('home', ['android' => 1])); ?>"><?php echo e(__('Home')); ?></a></li>
                        <li><a href="<?php echo e(route('categories.all', ['android' => 1])); ?>"><?php echo e(__('All Categories')); ?></a></li>
                        <?php if(isset($subsubcategory_id)): ?>
                            <?php
                            $SubSubCategory = \App\SubSubCategory::find($subsubcategory_id);
                            $SubCategory = \App\SubCategory::find($SubSubCategory->sub_category_id);
                            $Category = \App\Category::find($SubCategory->category_id);
                            ?>
                            <li ><a href="<?php echo e(route('products.category', ['category_slug'=>$Category->slug, 'android' => 1])); ?>"><?php echo e($Category->name); ?></a></li>
                            <li ><a href="<?php echo e(route('products.subcategory', ['category_slug'=>$Category->slug, 'subcategory_slug' => $SubCategory->slug, 'android' => 1])); ?>"><?php echo e($SubCategory->name); ?></a></li>
                            <li class="active"><a href="<?php echo e(route('products.subsubcategory', ['category_slug'=>$Category->slug, 'subcategory_slug' => $SubCategory->slug, 'subsubcategory_slug' => $SubSubCategory->slug, 'android' => 1])); ?>"><?php echo e($SubSubCategory->name); ?></a></li>
                        <?php elseif(isset($subcategory_id)): ?>
                            <li ><a href="<?php echo e(route('products.category', ['category_slug' => \App\SubCategory::find($subcategory_id)->category->slug, 'android' => 1])); ?>"><?php echo e(\App\SubCategory::find($subcategory_id)->category->name); ?></a></li>
                            <li class="active"><a href="<?php echo e(route('products.subcategory', ['category_slug' => \App\SubCategory::find($subcategory_id)->category->slug, 'subcategory_slug' => \App\SubCategory::find($subcategory_id)->slug, 'android' => 1])); ?>"><?php echo e(\App\SubCategory::find($subcategory_id)->name); ?></a></li>
                        <?php elseif(isset($category_id)): ?>
                            <li class="active"><a href="<?php echo e(route('products.category', ['category_slug'=>\App\Category::find($category_id)->slug, 'android' => 1])); ?>"><?php echo e(\App\Category::find($category_id)->name); ?></a></li>
                        <?php endif; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <section class="gry-bg py-md-2">
        <div class="container-fluid sm-px-0">
            <form class="" id="search-form" action="<?php echo e(url()->current()); ?>" method="GET">
                <?php if($brand_slug!=null): ?>
                    <input type="hidden" name="brand" id="brand_slug" value="<?php echo e($brand_slug); ?>" />
                <?php endif; ?>
                
            <?php if($mobileapp == 1): ?>
            <input type="hidden" name="android" value="1" />
            <?php endif; ?>
                <input type="hidden" name="sort_by" id="sort_by" value="<?php echo e($sort_by); ?>" />
                <div class="row productcustomrow">
                <div class="col-xl-3 side-filter d-xl-block">
                    <div class="filter-overlay filter-close"></div>
                    <div class="filter-wrapper c-scrollbar">
                        <div class="filter-title d-flex d-xl-none justify-content-between pb-3 align-items-center">
                            <h3 class="h6">Filters</h3>
                            <button type="button" class="close filter-close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="bg-white sidebar-box mb-3">
                            <div class="box-title text-center">
                                <?php echo e(__('Categories')); ?>

                            </div>
                            <div class="box-content">
                                <div class="category-filter">
                                    <ul>
                                        <?php if(!isset($category_id) && !isset($subcategory_id) && !isset($subsubcategory_id)): ?>
                                            <?php $__currentLoopData = \App\Category::orderBy('position','asc')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li class="font-weight-bold active"><a href="<?php echo e(route('products.category', $category->slug)); ?>"><?php echo e(__($category->name)); ?></a></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                            <li class="active d-none d-md-block"><a href="<?php echo e(route('categories.all')); ?>"><?php echo e(__('All Categories')); ?></a></li>
                                            <?php if($mobileapp == 0): ?>
                                                <li class="active d-block d-md-none"><a href="<?php echo e(route('categories.all.mobilegrid')); ?>"><?php echo e(__('All Categories')); ?></a></li>
                                            <?php else: ?>
                                                <li class="active d-block d-md-none"><a href="<?php echo e(route('categories.all.mobilegrid', ['android' => 1])); ?>"><?php echo e(__('All Categories')); ?></a></li>
                                            <?php endif; ?>
                                            <?php $__currentLoopData = \App\Category::orderBy('position','asc')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($category->display == 1): ?>
                                            <li class="font-weight-bold active"><a href="<?php echo e(route('products.category', $category->slug)); ?>"><?php echo e(__($category->name)); ?></a></li>
                                                <?php if(isset($category_id) && $category_id==$category->id): ?>
                                                    <?php
                                                        $Category = \App\Category::orderBy('position','asc')->find($category_id);
                                                    ?>
                                                    <ul class="ml-2">
                                                    <?php $__currentLoopData = $Category->subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2 => $subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($subcategory->display == 1): ?>
                                                         <li class="child active_sub"><a href="<?php echo e(route('products.subcategory', [$Category->slug, $subcategory->slug])); ?>"><?php echo e(__($subcategory->name)); ?></a></li>
                                                            <?php if(isset($subcategory_id) && $subcategory_id==$subcategory->id): ?>
                                                                <?php 
                                                                    $category_slug = \App\SubCategory::find($subcategory_id)->category->slug;
                                                                    $subcategory_slug = \App\SubCategory::find($subcategory_id)->slug;
                                                                ?>
                                                                <ul class="ml-5">
                                                                <?php $__currentLoopData = \App\SubCategory::find($subcategory_id)->subsubcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key3 => $subsubcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php if($subsubcategory->display == 1): ?>
                                                                    <li><a href="<?php echo e(route('products.subsubcategory', [$category_slug, $subcategory_slug, $subsubcategory->slug])); ?>"><?php echo e(__($subsubcategory->name)); ?></a></li>
                                                                    <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </ul>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white sidebar-box mb-3">
                            <div class="box-title text-center">
                                <?php echo e(__('Price range')); ?>

                            </div>
                            <div class="box-content">
                                <div class="range-slider-wrapper mt-3">
                                    <!-- Range slider container -->
                                    <div id="input-slider-range" data-range-value-min="<?php echo e($min); ?>" data-range-value-max="<?php echo e($max); ?>"></div>
                                    <!-- Range slider values -->
                                    <div class="row">
                                       <div class="col-6">
                                            <span class="range-slider-value value-low"
                                                <?php if(isset($min_price)): ?>
                                                    data-range-value-low="<?php echo e($min_price); ?>"
                                                <?php elseif($min > 0): ?>
                                                    data-range-value-low="<?php echo e($min); ?>"
                                                <?php else: ?>
                                                    data-range-value-low="0"
                                                <?php endif; ?>
                                                id="input-slider-range-value-low"></span>
                                        </div>

                                        <div class="col-6 text-right">
                                            <span class="range-slider-value value-high"
                                                <?php if(isset($max_price)): ?>
                                                    data-range-value-high="<?php echo e($max_price); ?>"
                                                <?php elseif($max > 0): ?>
                                                    data-range-value-high="<?php echo e($max); ?>"
                                                <?php else: ?>
                                                    data-range-value-high="0"
                                                <?php endif; ?>
                                                id="input-slider-range-value-high"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white sidebar-box mb-3 ">
                            <div class="box-title text-center">
                                <?php echo e(__('Sort by')); ?>

                            </div>
                            <div class="box-content">
                                <div class="category-filter">
                                    <div class="sort-by-box px-1">
										<div class="form-group">
											<select class="form-control sortSelect" data-minimum-results-for-search="Infinity" onchange="filter(this,this.value)">
												<option value="1" <?php if(isset($sort_by)): ?> <?php if($sort_by == '1'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Newest')); ?></option>
												<option value="2" <?php if(isset($sort_by)): ?> <?php if($sort_by == '2'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Oldest')); ?></option>
												<option value="3" <?php if(isset($sort_by)): ?> <?php if($sort_by == '3'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Price low to high')); ?></option>
												<option value="4" <?php if(isset($sort_by)): ?> <?php if($sort_by == '4'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Price high to low')); ?></option>
											</select>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                        
						<?php if(!empty($all_colors)): ?>
                        <div class="bg-white sidebar-box mb-3">
                            <div class="box-title text-center">
                                <?php echo e(__('Filter by color')); ?>

                            </div>
                            <div class="box-content">
                                <!-- Filter by color -->
                                <ul class="list-inline checkbox-color checkbox-color-circle mb-0">
                                    <?php $__currentLoopData = $all_colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <input type="radio" id="color-<?php echo e($key); ?>" name="color" value="<?php echo e($color); ?>" <?php if(isset($selected_color) && $selected_color == $color): ?> checked <?php endif; ?> onchange="filter()">
                                            <label style="background: <?php echo e(\App\Color::where('name', $color)->first()->code); ?>;" for="color-<?php echo e($key); ?>" data-toggle="tooltip" data-original-title="<?php echo e($color); ?>"></label>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php $__currentLoopData = $attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $attribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(\App\Attribute::find($attribute['id']) != null): ?>
                                <div class="bg-white sidebar-box mb-3">
                                    <div class="box-title text-center">
                                        Filter by <?php echo e(\App\Attribute::find($attribute['id'])->name); ?>

                                    </div>
                                    <div class="box-content">
                                        <!-- Filter by others -->
                                        <div class="filter-checkbox">
                                            <?php if(array_key_exists('values', $attribute)): ?>
                                                <?php $__currentLoopData = $attribute['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php
                                                        $flag = false;
                                                        if(isset($selected_attributes)){
                                                            foreach ($selected_attributes as $key => $selected_attribute) {
                                                                if($selected_attribute['id'] == $attribute['id']){
                                                                    if(in_array($value, $selected_attribute['values'])){
                                                                        $flag = true;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                    <div class="checkbox">
                                                        <input type="checkbox" id="attribute_<?php echo e($attribute['id']); ?>_value_<?php echo e($value); ?>" name="attribute_<?php echo e($attribute['id']); ?>[]" value="<?php echo e($value); ?>" <?php if($flag): ?> checked <?php endif; ?> onchange="filter()">
                                                        <label for="attribute_<?php echo e($attribute['id']); ?>_value_<?php echo e($value); ?>"><?php echo e($value); ?></label>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                        <div class="bg-white sidebar-box mb-3 ">
                            <div class="box-title text-center">
                                <?php echo e(__('Out of Stock')); ?>

                            </div>
                            <div class="box-content">
                                <div class="category-filter">
                                    <div class="sort-by-box px-1">
										<div class="form-group">
											<select class="form-control sortSelect" name="out_of_stock"  onchange="filter()">
												<option value="0" <?php if(isset($out_of_stock)): ?> <?php if($out_of_stock == '0'): ?> selected <?php endif; ?> <?php else: ?> selected <?php endif; ?>><?php echo e(__('Not Include')); ?></option>
												<option value="1" <?php if(isset($out_of_stock)): ?> <?php if($out_of_stock == '1'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Include')); ?></option>
											</select>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo e(url()->current()); ?>" class="btn btn-styled btn-block btn-base-4 bg-blue">Reset filter</a> 
                        
                    </div>
                </div>
                <div class="col-xl-9">
                    <!-- <div class="bg-white"> -->
                        <?php if(isset($category_id)): ?>
                            <input type="hidden" name="category" value="<?php echo e(\App\Category::find($category_id)->slug); ?>">
                        <?php endif; ?>
                        <?php if(isset($subcategory_id)): ?>
                            <input type="hidden" name="subcategory" value="<?php echo e(\App\SubCategory::find($subcategory_id)->slug); ?>">
                        <?php endif; ?>
                        <?php if(isset($subsubcategory_id)): ?>
                            <input type="hidden" name="subsubcategory" value="<?php echo e(\App\SubSubCategory::find($subsubcategory_id)->slug); ?>">
                        <?php endif; ?>
						<div class="d-block d-md-none">
							<div class="row bg-white mb-2 py-2">
								<div class="col-6" style="display: flex;align-items: center;justify-content: center;border-right:1px solid #f1f1f1">
									<button type="button" class="btn p-1 btn-sm" data-toggle="modal" href="#sortbymodal" style="background: none;">
                                        <i class="la la-sort" style="font-size:1.5em"></i> <span style="font-size:1.5em">Sort</span> 
                                    </button>
								</div>
								<div class="col-6" style="display: flex;align-items: center;justify-content: center;">
									<button type="button" class="btn p-1 btn-sm" id="side-filter" style="background: none;">
                                        <i class="la la-filter" style="font-size:1.5em"></i> <span style="font-size:1.5em">Categories</span>
                                    </button>
								</div>
							</div>
						</div>
                       <input type="hidden" name="min_price" value="">
                        <input type="hidden" name="max_price" value="">
                        <!-- <hr class=""> -->
                        <div class="products-box-bar p-3 bg-white aldeproductslider">
                            <div class="row sm-no-gutters gutters-5 results1">
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                        			    $home_base_price = home_base_price($product->id);
                        			    $home_discounted_base_price = home_discounted_base_price($product->id);
                        			    if($home_base_price == "₹ 0.00") {
                                            $product->current_stock = 0;
                                            $qty = 0;
                                        }
                    			    ?>
                    			    
                                    <div class="col-md-3 col-6">
                                        <article class="list-product">
                							<div class="img-block">
                							    <?php if($mobileapp == 0): ?>
                								<a href="<?php echo e(route('product', $product->slug)); ?>" class="thumbnail">
                								<?php else: ?>
                								<a href="<?php echo e(route('product', ['slug' => $product->slug,  'android' => 1])); ?>" class="thumbnail">
                								<?php endif; ?>
                								    <?php if($product->thumbnail_img != null): ?>
                									<img class="mx-auto d-block lazyload img-fluid" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" data-src="<?php echo e(asset($product->thumbnail_img)); ?>" alt="<?php echo e(__($product->name)); ?>" />
                									<?php else: ?>
                								    <img class="mx-auto d-block lazyload  img-fluid" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" alt="<?php echo e(__($product->name)); ?>">
                									<?php endif; ?>
                								</a>
                							</div>
                							<?php if($product->current_stock != 0): ?>
                    							<?php if($home_base_price != $home_discounted_base_price): ?>
                    							<ul class="product-flag">
                    								<li class="new discount-price bg-orange"><?php echo e(discount_calulate($product->id,$home_base_price, $home_discounted_base_price )); ?>% off</li>
                    							</ul>
                    							<?php endif; ?>
                    							
                							<?php endif; ?>
                							<div class="product-decs text-center">
                							    <?php if($mobileapp == 0): ?>
                								<h2><a href="<?php echo e(route('product', $product->slug)); ?>" class="product-link"><?php echo e(__(ucwords(strtolower($product->name)))); ?></a></h2>
                								<?php else: ?>
                								<h2><a href="<?php echo e(route('product', ['slug' => $product->slug,  'android' => 1])); ?>" class="product-link"><?php echo e(__(ucwords(strtolower($product->name)))); ?></a></h2>
                								<?php endif; ?>
                								<div class="pricing-meta">
                									<ul>
                    								    <?php if($product->current_stock == 0): ?>
                                        			    <li class="current-price">&nbsp; &nbsp; &nbsp; &nbsp; </li>
                                        			    <?php else: ?>
                    									    <?php if($home_base_price != $home_discounted_base_price): ?>
                    										<li class="old-price"><?php echo e($home_base_price); ?></li>
                    										<?php endif; ?>
                    										<li class="current-price"><?php echo e($home_discounted_base_price); ?></li>
                    									<?php endif; ?>
                									</ul>
                								</div>
                							</div>
                							<div class="add-to-cart-buttons">
                							    <?php if($product->current_stock == 0): ?>
                							    <button class="btn btn95" type="button" disabled> Out of Stock</button>
                							    <?php else: ?>
                							    <button class="btn btn25" onclick="addToCart(this, <?php echo e($product->id); ?>, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
                							    <button class="btn btn70" onclick="buyNow(this, <?php echo e($product->id); ?>, 1, 'full')" type="button"> Buy Now</button>
                							    <?php endif; ?>
                							    <?php if(env('BARGAIN_BTN',false)): ?>
                								<?php $bargain_cat = explode(",",env('BARGAIN_CATEGORY','')); ?>
                								    <?php if(in_array($product->category_id, $bargain_cat)): ?>
                    							    <a href="<?php echo e(env('BARGAIN_LINK','')); ?>" target="_blank" class="btn btn95" style="margin-top:5px;width:calc(90% + 4px);">
                                                    <?php echo e(__('For best lowest price Click')); ?>

                    								</a>
                							        <?php endif; ?>
                							    <?php endif; ?>
                							</div>
                						</article>
            						</div>
        						 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        					</div>
        					
        					<div class="row sm-no-gutters gutters-5">
								<div class="col-12 text-center pb-5 mb-5 mb-md-0 pb-md-0">
									<div class="ajax-loading text-center"><img src="<?php echo e(asset('frontend/images/loading.gif')); ?>" /></div>
								</div>
							</div>
    					</div>
                       

                    <!-- </div> -->
                </div>
            </div>
			<!--Sort Modal--->
			<div class="modal fade" id="sortbymodal" data-keyboard="false" data-backdrop="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Sort By</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<div class="sort-by-box px-1">
								<div class="form-group">
									<label><?php echo e(__('Sort by')); ?></label>
									<select class="form-control sortSelect" data-minimum-results-for-search="Infinity" onchange="filter(this,this.value)">
										<option value="1" <?php if(isset($sort_by)): ?> <?php if($sort_by == '1'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Newest')); ?></option>
										<option value="2" <?php if(isset($sort_by)): ?> <?php if($sort_by == '2'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Oldest')); ?></option>
										<option value="3" <?php if(isset($sort_by)): ?> <?php if($sort_by == '3'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Price low to high')); ?></option>
										<option value="4" <?php if(isset($sort_by)): ?> <?php if($sort_by == '4'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Price high to low')); ?></option>
									</select>
								</div>
							</div>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dalog -->
			</div><!-- /.modal -->
            </form>
        </div>
    </section>
	
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
       function filter(ele = null, val = null){
			if(val != null)  {
				$('#sort_by').val(val);
			}
            $('#search-form').submit();
        }
        function rangefilter(arg){
            $('input[name=min_price]').val(arg[0]);
            $('input[name=max_price]').val(arg[1]);
            filter();
        }
    </script>
    <script>
		$('.ajax-loading').hide();
		var page = 2; //track user scroll as page number, right now page number is 1
		var Totalpages=<?php echo e($products->lastPage()); ?>;
		var ajaxloading = 0;
		frame_width= window.innerWidth;

		$(window).scroll(function() { //detect page scroll
		  if(frame_width<=768)
		       footerheight=3000;
		    else
		       footerheight = $("#footer").outerHeight()+1000;
		    if($(window).scrollTop() + $(window).height() >= ($(document).height()-footerheight)) { //if user scrolled from top to bottom of the page
				//page number increment
				if(page <= Totalpages) {
					if(ajaxloading == 0) {
						ajaxloading=1;
					    load_more(page); //load content  
				    	page++; 
					}
				}					
			}
		});     
		function load_more(page){
			<?php 
			$query = request()->except('subsubcategory');
			$qq = array();
			foreach($query as $key=>$q) {
				$qq[] = "$key=$q";
			}
			$qq = implode("&",$qq);
				$url = "'".url()->current()."?page='+page";
				if($qq != "") {
					$url .="+'";
					if($qq != "") {
					$url .="&$qq";
					}
					$url .= "'";
				}
			?>
		  $.ajax(
				{
					url: <?php echo $url; ?>,
					type: "get",
					datatype: "html",
					beforeSend: function()
					{
						$('.ajax-loading').show();
					}
				})
				.done(function(data)
				{
					$('.ajax-loading').hide(); //hide loading animation once data is received
					$(".results1").append(data); //append data into #results element  
					ajaxloading = 0;	
					if(Totalpages == page ){
						//notify user if nothing to load
						$('.ajax-loading').html("No more records!");
						$('.ajax-loading').show();
					}					
				})
				.fail(function(jqXHR, ajaxOptions, thrownError)
				{
					 console.log('No response from server');
				});
		 }
	</script>

	<style>
		.product-box-2.alt-box .product-btns .btn {
        width: 33.33%;
        }
	</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/product_listing.blade.php ENDPATH**/ ?>