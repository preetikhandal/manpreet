<?php $__env->startSection('content'); ?>

<?php
    $generalsetting = \App\GeneralSetting::first();
?>
<style>
.magic-radio+label:before, .magic-checkbox+label:before {
    border: 1px solid #fff;
}
</style>
<div class="flex-row">
	<div class="container login-container" style="width:100%">
		<div class="row">
		    <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                     <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
			<div class="col-md-5 login-form-1">
				<br/><br/><br/>
				<?php if($generalsetting->admin_login_sidebar != null): ?>
					<img src="<?php echo e(asset($generalsetting->admin_login_sidebar)); ?>" class="img-fit" >
				 <?php else: ?>
					 <img src="<?php echo e(asset('img/bg-img/login-box.jpg')); ?>" class="img-fit" >
				<?php endif; ?>
			</div>
			<div class="col-md-7 col-12 login-form-2">
			
				<!--<div class="text-center">
					<br>
					<?php if($generalsetting->logo != null): ?>
						<img loading="lazy"  src="<?php echo e(asset($generalsetting->logo)); ?>" class="" height="44">
					<?php else: ?>
						<img loading="lazy"  src="<?php echo e(asset('frontend/images/logo/logo.png')); ?>" class="" height="44">
					<?php endif; ?>

					<br>
					<br>
					<br>
				</div>-->
				<form method="POST" role="form" action="<?php echo e(route('login')); ?>">
					<?php echo csrf_field(); ?>
					<div class="form-group">
						 <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required autofocus placeholder="Email">
					
					</div>
					<div class="form-group">
						<input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" placeholder="Password">
						
					</div>
					<div class="row">
						<div class="col-md-6 col-12">
							<div class="form-group">
								<div class="checkbox pad-btm text-left text-xs-center">
									<input id="demo-form-checkbox" class="magic-checkbox" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
									<label for="demo-form-checkbox" class="btn-link">
										<?php echo e(__('Remember Me')); ?>

									</label>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-12">
							<div class="form-group">
								<?php if(env('MAIL_USERNAME') != null && env('MAIL_PASSWORD') != null): ?>
									<div class="checkbox pad-btm text-right text-xs-center">
										<a href="<?php echo e(route('password.request')); ?>" class="btn-link"><?php echo e(__('Forgot password')); ?> ?</a>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-lg btn-block" style="background:#007bff">
							<?php echo e(__('Login')); ?>

						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/auth/login.blade.php ENDPATH**/ ?>