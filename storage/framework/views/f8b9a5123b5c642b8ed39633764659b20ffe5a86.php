<?php $__env->startSection('content'); ?>

<!-- Basic Data Tables -->
<!--===================================================-->
<style>
    .ordcode{
        color:#282563;
    }
    .ordcode:hover{
        color:#EB3038;
    }
</style>
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no"><?php echo e(__('Orders')); ?></h3>
        <div class="pull-right clearfix">
            <form class="" id="sort_orders" action="" method="GET">
                <!--<div class="box-inline pad-rgt pull-left">-->
                <!--    <div class="select" style="min-width: 300px;">-->
                <!--        <select class="form-control demo-select2" name="payment_type" id="payment_type" onchange="sort_orders()">-->
                <!--            <option value=""><?php echo e(__('Filter by Payment Status')); ?></option>-->
                <!--            <option value="paid"  <?php if(isset($payment_status)): ?> <?php if($payment_status == 'paid'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Paid')); ?></option>-->
                <!--            <option value="unpaid"  <?php if(isset($payment_status)): ?> <?php if($payment_status == 'unpaid'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Un-Paid')); ?></option>-->
                <!--        </select>-->
                <!--    </div>-->
                <!--</div>-->
                <!--<div class="box-inline pad-rgt pull-left">-->
                <!--    <div class="select" style="min-width: 300px;">-->
                <!--        <select class="form-control demo-select2" name="delivery_status" id="delivery_status" onchange="sort_orders()">-->
                <!--            <option value=""><?php echo e(__('Filter by Deliver Status')); ?></option>-->
                <!--            <option value="confirmation_pending"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'confirmation_pending'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Confirmation Pending')); ?></option>-->
                <!--            <option value="hold"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'hold'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Hold')); ?></option>-->
                <!--            <option value="pending"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'pending'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Pending')); ?></option>-->
                <!--            <option value="on_review"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'on_review'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('On review')); ?></option>-->
                <!--            <option value="ready_to_ship"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'ready_to_ship'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Ready to Ship')); ?></option>-->
                <!--            <option value="ready_to_delivery"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'ready_to_delivery'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Ready to Delivery')); ?></option>-->
                <!--            <option value="shipped"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'shipped'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Shipped')); ?></option>-->
                <!--            <option value="on_delivery"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'on_delivery'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('On delivery')); ?></option>-->
                <!--            <option value="delivered"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'delivered'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Delivered')); ?></option>-->
                <!--            <option value="processing_refund"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'processing_refund'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Processing Refund')); ?></option>-->
                <!--            <option value="refunded"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'refunded'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Refunded')); ?></option>-->
                <!--            <option value="cancel"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'cancel'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Cancel')); ?></option>-->
                <!--        </select>-->
                <!--    </div>-->
                <!--</div>-->
                <div class="box-inline pad-rgt pull-left">
                    <!--<div class="" style="min-width: 200px;display:inline-block;">-->
                    <!--    <input type="text" class="form-control" id="search" name="cust_search"<?php if(isset($cust_search)): ?> value="<?php echo e($cust_search); ?>" <?php endif; ?> placeholder="Cust Name">-->
                    <!--</div>-->
                    <a href="<?php echo e(route('order.export')); ?>" class="btn btn-success">Export Orders</a>
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"<?php if(isset($sort_search)): ?> value="<?php echo e($sort_search); ?>" <?php endif; ?> placeholder="Type & Enter">
                    </div>
                    
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo e(__('Order Code')); ?></th>
                    <th><?php echo e(__('Order Date')); ?></th>
                    <th><?php echo e(__('Num. of Products')); ?></th>
                    <th><?php echo e(__('Customer')); ?></th>
                    <th><?php echo e(__('Amount')); ?></th>
                    <th><?php echo e(__('Coupen Discount')); ?></th>
                    <th><?php echo e(__('Order Status')); ?></th>
                    <th><?php echo e(__('Pay Method')); ?></th>
                    <th><?php echo e(__('Pay Status')); ?></th>
                    <th><?php echo e(__('Options')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $order_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        $order = \App\Order::find($order_id->id);
                    ?>
                    <?php if($order != null): ?>
                    <?php
                    if ($order->orderDetails->where('seller_id',  $admin_user_id)->first()->payment_status == 'paid') {
                        $payment_status = "paid";
                    } else {
                        $payment_status = "unpaid";
                    }
                    ?>
                        <tr>
                            <td>
                                <?php echo e(($key+1) + ($orders->currentPage() - 1)*$orders->perPage()); ?> <input type="checkbox" data-ordercode="<?php echo e($order->code); ?>" data-payment="<?php echo e($payment_status); ?>" name="order_id[]" value="<?php echo e($order->id); ?>" id="check_order_id" class="check_order_id" />
                            </td>
                            <td>
                                <a href="<?php echo e(route('orders.show', encrypt($order->id))); ?>" class="ordcode"> <?php echo e($order->code); ?></a> <?php if($order->viewed == 0): ?> <span class="pull-right badge badge-info"><?php echo e(__('New')); ?></span> <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e(date('d-m-Y h:i A', $order->date)); ?>

                            </td>
                             <?php
								$return_product_id=\App\ReturnProduct::where('order_id',$order_id->id)->pluck('product_id');
								//dd(\Route::currentRouteName());
							?>
                            <td class="text-center">
                                <?php if(count($return_product_id)>0 && (\Route::currentRouteName() == 'orders.index.refunded.admin' || \Route::currentRouteName() == 'orders.index.processing_refund.admin')): ?>
								 Return Request of <?php echo e(count($return_product_id)); ?> out of <?php echo e(count($order->orderDetails->where('seller_id', $admin_user_id))); ?> 
							<?php else: ?>
                                <?php echo e(count($order->orderDetails->where('seller_id', $admin_user_id))); ?>

							<?php endif; ?>
                            </td>
                            <td>
                                
                                <?php if(\Route::currentRouteName() == 'orders.index.ready_to_delivery.admin'): ?>
                                
                                  <?php $shipping_address = json_decode($order->shipping_address,true); ?>
                                    <?php echo e($shipping_address['name']); ?>

                                    
                                <?php else: ?>
                                <?php if($order->user_id != null): ?>
                                      
                                    <?php echo e($order->user->name??' '); ?><?php if(customerBookingsCount($order->user_id) == 1): ?> <span class="pull-right badge badge-info"><?php echo e(__('New Customer')); ?></span> <?php endif; ?>
                                <?php else: ?>
                                    Guest (<?php echo e($order->guest_id); ?>)
                                <?php endif; ?>
                                <?php endif; ?>
                            </td>
                            <td>
                               
						
							<?php echo e(single_price($order->grand_total )); ?>

						
                            </td>
                            <td>
                               
								<?php echo e(single_price($order->coupon_discount )); ?>

						
                            </td>
                            <td>
                                <?php
                                    $status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->delivery_status;
                                ?>
                                <?php if($status == "Confirmation Pending"): ?>
                                <label class="label label-danger"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></label>
                                <?php elseif($status == "pending"): ?>
                                <label class="label label-warning"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></label>
                                <?php else: ?>
                                <label class="label label-warning"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></label>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e(ucfirst(str_replace('_', ' ', $order->payment_type))); ?>

                            </td>
                            <td>
                                <span class="badge badge--2 mr-4">
                                    <?php if($payment_status == 'paid'): ?>
                                        <i class="bg-green"></i> Paid
                                    <?php else: ?>
                                        <i class="bg-red"></i> Unpaid
                                    <?php endif; ?>
                                </span>
                            </td>
                            <td>
                                <?php if(count($order->orderDetails->where('seller_id', $admin_user_id)->where('delivery_status', 'confirmation_pending')) > 0): ?>
                                        <a  href="javascript:confirm_order(<?php echo e($order->id); ?>, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a>
                                        <?php if($order->prescription_file != null): ?> 
                                            <a href="<?php echo e(asset('prescription/'.$order->prescription_file)); ?>" class="btn btn-primary"><?php echo e(__('Download Prescription')); ?></a>
                                        <?php endif; ?>
                                        <?php if($payment_status == "paid"): ?>
                                            <a href="javascript:confirm_order(<?php echo e($order->id); ?>, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                        <?php else: ?>
                                            <a href="javascript:confirm_order(<?php echo e($order->id); ?>, 'cancel')" class="btn btn-primary">Cancel Order</a> 
                                            <a href="javascript:trash_order(<?php echo e($order->id); ?>, 'trash')" class="btn btn-primary">Trash Order</a>
                                            
                                           
                                        <?php endif; ?>
                                       
                                <?php else: ?>
                                    <?php if($status == "delivered"): ?>
                                     <?php if(count($return_product_id)>0 && (\Route::currentRouteName() == 'orders.index.refunded.admin' || \Route::currentRouteName() == 'orders.index.processing_refund.admin')): ?>
                                         <a href="<?php echo e(route('returnorders.show', encrypt($order->id))); ?>" class="btn btn-primary"><?php echo e(__('View Return')); ?></a>
                                     <?php endif; ?>
                                    <?php elseif($status == "pending"): ?>
                                        <a  href="javascript:confirm_order(<?php echo e($order->id); ?>, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a>
                                        <a href="javascript:confirm_order(<?php echo e($order->id); ?>, 'cancel')" class="btn btn-primary">Cancel Order</a>
                                        <a href="javascript:trash_order(<?php echo e($order->id); ?>, 'trash')" class="btn btn-primary">Trash Order</a>
                                         <a   href="javascript:denied_order(<?php echo e($order->id); ?>)" class="btn btn-primary">Denied Order</a>
                                        <?php if($order->prescription_file != null): ?> 
                                            <a href="<?php echo e(asset('prescription/'.$order->prescription_file)); ?>" class="btn btn-primary"><?php echo e(__('Download Prescription')); ?></a>
                                        <?php endif; ?>
                                        <!--a onclick="confirm_modal('<?php echo e(route('orders.destroy', $order->id)); ?>');" class="btn btn-primary"><?php echo e(__('Delete')); ?></a-->
                                        <?php if($payment_status == "paid"): ?>
                                            <a href="javascript:confirm_order(<?php echo e($order->id); ?>, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                        
                                        <?php endif; ?>
                                    <?php elseif($status == "ready_to_ship"): ?> 
                                        <a href="javascript:order_shipping_info(<?php echo e($order->id); ?>)"  class="btn btn-primary"><?php echo e(__('Add Shipping info')); ?></a>
                                        <a href="javascript:confirm_order(<?php echo e($order->id); ?>, 'ready_to_delivery')" class="btn btn-primary">Ready to Delivery</a>
                                        <a href="<?php echo e(route('orders.print_label', $order->id)); ?>" target="_blank" class="btn btn-primary"><?php echo e(__('Print Label')); ?></a>
                                    <?php elseif($status == "shipped" or $status == "on_delivery"): ?> 
                                        <?php if($payment_status == 'unpaid'): ?>
                                            <a href="javascript:confirm_payment(<?php echo e($order->id); ?>, 'paid')" class="btn btn-primary">Mark Paid</a>
                                            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#deniedModal">Denied Order</a>
                                            <span id="orderbtn_<?php echo e($order->id); ?>">
                                                <a href="javascript:void(0)" class="btn btn-primary" disabled><?php echo e(__('Mark Delivered')); ?></a></span>
                                        <?php else: ?>
                                        <span id="orderbtn_<?php echo e($order->id); ?>"><a href="javascript:confirm_order(<?php echo e($order->id); ?>, 'delivered')" class="btn btn-primary"><?php echo e(__('Mark Delivered')); ?></a></span>
                                        <?php endif; ?>
                                        
                                        <?php if($payment_status == "paid"): ?>
                                            <a href="javascript:confirm_order(<?php echo e($order->id); ?>, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                       
                                        <?php endif; ?>
                                        
                                    <?php elseif($status == "processing_refund"): ?> 
                                        <a href="javascript:confirm_order(<?php echo e($order->id); ?>, 'refunded')" class="btn btn-primary">Refunded to Wallet</a>
                                    <?php elseif($status == "ready_to_delivery"): ?>
                                        <a href="<?php echo e(route('orders.print_label', $order->id)); ?>" target="_blank" class="btn btn-primary"><?php echo e(__('Print Label')); ?></a>
                                        <button class="btn btn-primary" disabled><?php echo e(__('User below button to assign Agent')); ?></button>
                                    <?php elseif($status == "on_review" or $status == "hold"): ?>
                                        <a href="javascript:confirm_order(<?php echo e($order->id); ?>, 'pending')" class="btn btn-primary">Move to Pending</a>   
                                    <?php else: ?>
                                    <button class="btn btn-primary" disabled><?php echo e(__('No Action')); ?></button>
                                    <?php endif; ?>
                                <?php endif; ?>
                                
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="clearfix">
            <div class="col-md-12">
                <br />&nbsp;
                <?php if(\Route::currentRouteName() == 'orders.index.on_delivery.admin'): ?>
                <button class="btn btn-danger" onclick="mark_undelivered()">Unable to Delivered</button>
                <?php endif; ?>
                <?php if(\Route::currentRouteName() == 'orders.index.ready_to_delivery.admin'): ?>
                <button class="btn btn-primary" onclick="showmodal('assign_status_modal');">Assign Delivery Agent</button>
                <?php endif; ?>
                
                <?php if(\Route::currentRouteName() == 'orders.index.on_delivery.admin' || \Route::currentRouteName() == 'orders.index.shipped.admin' ): ?>
                <button class="btn btn-success" id="markdeliverybtn" onclick="showmodal('mark_delivery_modal')" disabled>Mark Selected to Delivered</button>
                <button class="btn btn-primary" id="markpaidbtn" onclick="showmodal('payment_status_modal');" disabled>Mark Selected to Paid</button>
                <?php endif; ?>
                
                <br />&nbsp;
                <br />&nbsp;
            </div>
        </div>
        <div class="clearfix">
            <div class="pull-right">
                <?php echo e($orders->appends(request()->input())->links()); ?>

            </div>
        </div>
    </div>
</div>

<!-- denied modal popup -->
<div class="modal fade" id="deniedPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__(' Request For Denied ')); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 pt-2">
                       <label> Request For Denied </label>
                       <textarea name="form_message" id="deniedMessage" class="form-control required" rows="5"></textarea>
                       <input type="hidden" id="deniedOrderId">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button class="btn btn-success"  onclick="getDenied()"  data-dismiss="modal">Submit</button>
            </div>
        </div>
    </div>
</div>


<script>
    function showmodal(modalid) {
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
        productArray.push($(v).val());
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        $('#'+modalid).modal();
    }
function mark_undelivered() {
    productArray = [];
    productCodeArray = [];
    html = '<table class="table table-bordred table-hover">';
    html +='<thead>';
    html +='    <tr>';
    html +='    <th class="text-center">Order No.</th>';
    html +='    <th class="text-center">Order Code</th>';
    html +='    <th class="text-center">Undelivery Note</th>';
    html +='    </tr>';
    html +='</thead>';
    html +='<tbody>';
    $('#check_order_id:checked').each(function(i,v) {
        productArray.push($(v).val());
        productCodeArray.push($(v).data('ordercode')); 
        html +='    <tr>';
        html +='        <td class="text-center">'+ $(v).val() +'<input type="hidden" name="order_ids[]" value="'+ $(v).val() +'"  class="form-control"></td>';
        html +='        <td class="text-center">'+ $(v).data('ordercode') +'<input type="hidden" name="order_code[]" value="'+ $(v).data('ordercode') +'" class="form-control"></td>';
        html +='        <td><input type="text" name="undelivery_notes[]" class="form-control"></td>';
        html +='    </tr>';
    });
    if(productCodeArray.length == 0) {
        alert("Kindly select order.");
        return false;
    }
    html +='</tbody>';
    html +='</table>';
    $('#undelivery_div').html(html);
   // console.log(productCodeArray);
    $('#undelivery_status_modal').modal();
}
</script>

<div class="modal fade" id="undelivery_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="undelivery_Form">
                <?php echo csrf_field(); ?>
                <?php if(isset($seller_id)): ?> 
                    <input type="hidden" name="seller_id" value="<?php echo e($seller_id); ?>">
                <?php endif; ?>
                    <input type="hidden" name="action" value="undelivery">
                    <input type="hidden" name="setFlash" value="Order set for ready for delivery status.">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Update Info')); ?></h4>
            </div>
            <div class="modal-body" id="undelivery_div">
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="delivery_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Delivery Status')); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Status &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control demo-select2"  id="modal_delivery_status">
                            <option value=""><?php echo e(__('Select Deliver Status')); ?></option>
                            <option value="confirmation_pending"><?php echo e(__('Confirmation Pending')); ?></option>
                            <option value="hold"><?php echo e(__('Hold')); ?></option>
                            <option value="pending"><?php echo e(__('Pending')); ?></option>
                            <option value="on_review"><?php echo e(__('On review')); ?></option>
                            <option value="ready_to_ship"><?php echo e(__('Ready to Ship')); ?></option>
                            <option value="ready_to_delivery"><?php echo e(__('Ready to Delivery')); ?></option>
                            <option value="shipped"><?php echo e(__('Shipped')); ?></option>
                            <option value="on_delivery"><?php echo e(__('On delivery')); ?></option>
                            <option value="delivered" ><?php echo e(__('Delivered')); ?></option>
                            <option value="processing_refund"><?php echo e(__('Processing Refund')); ?></option>
                            <option value="refunded"><?php echo e(__('Refunded')); ?></option>
                            <option value="cancel"><?php echo e(__('Cancel')); ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="change_status('delivery')"  data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="assign_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Assign to delivery')); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Status &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <select class="form-control demo-select2"  id="modal_delivery_agent_id">
                            <option value=""><?php echo e(__('Select Agent')); ?></option>
                            <?php $__currentLoopData = $delivery_agent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($d->id); ?>"><?php echo e($d->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="assign_delivery()"  data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="payment_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Changing of Payment Status')); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 text-right pt-2">
                       <center><strong>Do you want to continue ?</strong></center>
                    </div>
                        <input type="hidden" value="paid" id="modal_payment_status">
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="change_status('payment')"  data-dismiss="modal">Continue</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mark_delivery_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Changing of Delivery Status')); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 text-right pt-2">
                       <center><strong>Do you want to continue ?</strong></center>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="mark_delivered()"  data-dismiss="modal">Continue</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="shipping_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Shipping Info')); ?></h4>
            </div>
            <form action="<?php echo e(route('orders.shipping')); ?>" method="post">
            <?php echo csrf_field(); ?>
            <div class="modal-body">
            <input type="hidden" name="order_id" id="modal_order_id">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Courier &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name='courier' placeholder="Courier Company Name" >
                    </div>
                </div>
                <div class="form-group row" style="margin-top:5px;">
                    <div class="col-sm-3 text-right pt-2">
                        AWB/Ref/Tracking No. &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="awb" class="form-control" placeholder="AWB Number" >
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                  <button class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

    <script type="text/javascript">
        
        $('#undelivery_Form').submit(function(e){
            e.preventDefault();
            formdata =  $('#undelivery_Form').serialize();
            $('#undelivery_status_modal').modal('hide');
            $.ajax({
                type: 'POST',
                url: '<?php echo e(route('orders.update_status')); ?>',
                data:formdata,
                processData: false,
                success: function(result){
                 location.reload();
                }
            });
        });
    <?php if(\Route::currentRouteName() == 'orders.index.on_delivery.admin' || \Route::currentRouteName() == 'orders.index.shipped.admin' ): ?>
    
    function checkdeliverypayButton() {
        productunpaidArray = [];
        productpaidArray = [];
        $('#check_order_id:checked').each(function(i,v) {
            if($(v).data('payment') == 'unpaid') {
                productunpaidArray.push($(v).val());
            } else {
                productpaidArray.push($(v).val());
            }
        });
        console.log(productunpaidArray.length);
        console.log(productpaidArray.length);
        
        if(productunpaidArray.length == 0) {
            $('#markdeliverybtn').prop('disabled', false);
        } else {
            $('#markdeliverybtn').prop('disabled', 'disabled');
        }
        
        if(productpaidArray.length == 0) {
            $('#markpaidbtn').prop('disabled', false);
        } else {
            $('#markpaidbtn').prop('disabled', 'disabled');
        }
        if(productpaidArray.length == 0 && productunpaidArray.length == 0 ) {
            $('#markpaidbtn').prop('disabled', 'disabled');
            $('#markdeliverybtn').prop('disabled', 'disabled');
        }
    }
    
    $('.check_order_id').click(function() {
        checkdeliverypayButton();
    });
    
    <?php endif; ?>
    
    function mark_delivered() {
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "delivered";
        var setFlash = 'Order set for delivered status.';
        $.post('<?php echo e(route('orders.update_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', action:action, setFlash:setFlash, order_ids:productArray, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
             location.reload();
        });
    }
        
    function assign_delivery() {
        agent_id = $('#modal_delivery_agent_id').val();
        $('#modal_delivery_agent_id').val("");
        $('#modal_delivery_agent_id').trigger('change');
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "assign";
        var setFlash = 'Order set for out for delivery status.';
        $.post('<?php echo e(route('orders.update_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', action:action, setFlash:setFlash, order_ids:productArray, agent_id:agent_id, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
             location.reload();
        });
    }
        
    function change_status(action) {
        if(action == 'delivery') {
            status = $('#modal_delivery_status').val();
            $('#modal_delivery_status').val("");
            $('#modal_delivery_status').trigger('change');
        }
        if(action == 'payment') {
            status = $('#modal_payment_status').val();
            $('#modal_payment_status').val("");
            $('#modal_payment_status').trigger('change');
        }
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
        }); 
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        
        var setFlash = 'Order '+action+' status has been updated';
        $.post('<?php echo e(route('orders.update_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', action:action, setFlash:setFlash, order_ids:productArray, status:status, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
             location.reload();
        });
    }
        
    function order_shipping_info(order_id)
    {
        $('#modal_order_id').val(order_id);
        $('#shipping_info').modal();
    }
        function sort_orders(el){
            $('#sort_orders').submit();
        }
    
        function confirm_payment(order_id, status) {
            var setFlash = 'Payment status has been updated';
            $.post('<?php echo e(route('orders.update_payment_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', setFlash:setFlash, order_id:order_id,status:status, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
                location.reload();
            });
        }

        function confirm_order(order_id, status) {
            var setFlash = 'Order status has been updated';
            $.post('<?php echo e(route('orders.update_delivery_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', setFlash:setFlash, order_id:order_id,status:status, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
                 location.reload();
            });
        }

        function trash_order(order_id, status) {
            var setFlash = 'Order status has been updated';
            $.post('<?php echo e(route('orders.update_delivery_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', setFlash:setFlash, order_id:order_id,status:status, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
                 location.reload();
            });
        }
        
        
        
        function denied_order(order_id) {
            
            $('#deniedOrderId').val(order_id);
            $("#deniedPopup").modal('show');
            
        }
        
        
        //send denied order request
        
        function getDenied(){
            var orderId = $('#deniedOrderId').val();
            var deniedMessage = $('#deniedMessage').val();
            if(deniedMessage.length > 0){
                 $.ajax({
                 url:'<?php echo e(route('orders.denied')); ?>',
                 method:'POST',
                 data:{_token:'<?php echo e(@csrf_token()); ?>','deniedMessage':deniedMessage,'orderId':orderId},
                 async:false,
                 success:function(data){
                     location.reload();
                 }
             });
            }else{
                alert('Please enter reason for denied order');
            }
            
            
            
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/orders/index.blade.php ENDPATH**/ ?>