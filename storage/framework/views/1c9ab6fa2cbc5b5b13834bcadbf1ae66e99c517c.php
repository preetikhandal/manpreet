<style>
    .shipmsg{
	    color:red;
	    font-size:14px;
	}
</style>

<div class="card sticky-top">
    <div class="card-title py-2 bg-blue">
        <div class="row align-items-center">
            <div class="col-6">
                <h3 class="heading heading-3 strong-400 mb-0 text-white">
                    <span><?php echo e(__('Summary')); ?></span>
                </h3>
            </div>

            <div class="col-6 text-right">
                <span class="badge badge-md" style="background:orange"><?php echo e(count(Session::get('cart'))); ?> <?php echo e(__('Items')); ?></span>
            </div>
        </div>
    </div>
                                                    
    <div class="card-body">
        
        <table class="table-cart table-cart-review">
            <thead>
                <tr>
                    <th class="product-name"><?php echo e(__('Product')); ?></th>
                    <th class="product-total text-right"><?php echo e(__('Total')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $subtotal = 0;
                    $tax = 0;
                    $deliveryCharge = 0;
                    $shipping = 0;
                    $shipType = false;
                    $getPercent = 0;
                    $shipData = Session::get('shippingData');
                    
                    $applyCoupe = Session::get('applycoupen');
                   
                if(isset($shipData['paymentOption'])){
                        if($shipData['paymentOption'] == 'COD'){

                            if($shipData['totalAmount'] >= 1 && $shipData['totalAmount'] <= 499){
                                
                                $shipType = true;
                                $deliveryCharge = 0;
                            }else if($shipData['totalAmount']  >= 500 && $shipData['totalAmount'] <=  999){
                                $shipType = true;
                                $deliveryCharge = 0;
                            }else if($shipData['totalAmount'] >=  1000){
                                $shipType = true;
                                $deliveryCharge= 0;
                            }
                        }else if($shipData['paymentOption'] == 'razorpay'){
                         
                            if($shipData['totalAmount'] >= 1 && $shipData['totalAmount'] <= 499){
                                
                                $shipType = true;
                                $deliveryCharge = 0;
                            }else if($shipData['totalAmount']  >= 500 && $shipData['totalAmount'] <=  999){
                                $shipType = true;
                                $deliveryCharge = 0;
                            }else if($shipData['totalAmount'] >=  1000){
                                $shipType = true;
                                $deliveryCharge = 0;
                            }
                        }
                       
                }else{  
                    
                    if(isset($total)){
                        if($total <= 1000){
                          $shipType = true;
                          $deliveryCharge = 0;
                        }
                    }else{
                        $shipType = true; 
                    }
                           
                }
                  //echo '===>'.$deliveryCharge; 
                  //dd($deliveryCharge);
                ?>
                  
                <?php $__currentLoopData = Session::get('cart'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cartItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                    $product = \App\Product::find($cartItem['id']);
                    $categoryId = $product->category_id;
                    $subcategoryId = $product->subcategory_id;
                    $subsubcategory = $product->subsubcategory_id;
                    if(isset($product->user_id)){
                    	    $sellerId = $product->user_id;
                    	}else{
                    	    $sellerId = 0;
                    	}
                        if(!empty($sellerId)){
                                if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                                $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
                            }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                                    $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
                            }else if(!empty($categoryId) && !empty($sellerId)){
                                 $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
                            }    
                        }




                    if(isset($get_subcategory->commission) && $get_subcategory->commission_type==1){
                        $exculsivePercent = $get_subcategory->commission;
                    }else{
                        $exculsivePercent = 0; 
                    }
                   
                    if(!empty($exculsivePercent)){
                          $getPercent = ($product->unit_price*$exculsivePercent)/100;
                          $cartIteamPrice = $cartItem['price'];
                    }else{
                       $cartIteamPrice = $cartItem['price'];
                    }
                    $subtotal += $cartIteamPrice*$cartItem['quantity'];
                    $tax += $cartItem['tax']*$cartItem['quantity'];
                    $shipping += $cartItem['shipping']*$cartItem['quantity'];
                    if($shipType){
                        $deliveryCharge = $deliveryCharge;
                    }else{
                        //$deliveryCharge += $cartItem['shipping']*$cartItem['quantity'];
                    }
                   
                    $product_name_with_choice = $product->name;
                    if ($cartItem['variant'] != null) {
                        $product_name_with_choice = $product->name.' - '.$cartItem['variant'];
                    }
                    ?>
                    <tr class="cart_item">
                        <td class="product-name">
                            <?php echo e($product_name_with_choice); ?>

                            <strong class="product-quantity">× <?php echo e($cartItem['quantity']); ?></strong>
                        </td>
                        <td class="product-total text-right">
                            <span class="pl-4"><?php echo e(single_price($cartIteamPrice*$cartItem['quantity'])); ?></span>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                <?php
                    $total = $subtotal+$tax+$shipping;
                    if(Session::has('coupon_discount')){
                        $total -= Session::get('coupon_discount');
                    }
                    
            		if($total < env('FREE_CART_VALUE',0)) {
                        session(['cart_shipping' => env('BELOW_FREE_CART_CHARGE',0)]);
                    } else {
                        session(['cart_shipping' => 0]);
                    }
                    $total += session('cart_shipping');
                ?>
            </tbody>
        </table>
            <?php
                
                $shipDatacal = Session::get('shippingData');
                if(isset($shipDatacal['paymentOption'])){
                        if($shipDatacal['paymentOption'] == 'COD'){

                            if($total >= 1 && $total <= 499){
                                
                                $shipType = true;
                                $deliveryCharge = 0;
                            }else if($total  >= 500 && $total <=  999){
                                $shipType = true;
                                $deliveryCharge = 0;
                            }else if($total >=  1000){
                                $shipType = true;
                                $deliveryCharge= 0;
                            }
                        }else if($shipDatacal['paymentOption'] == 'razorpay'){
                         
                            if($total >= 1 && $total <= 499){
                                
                                $shipType = true;
                                $deliveryCharge = 0;
                            }else if($total  >= 500 && $total <=  999){
                                $shipType = true;
                                $deliveryCharge = 0;
                            }else if($total >=  1000){
                                $shipType = true;
                                $deliveryCharge = 0;
                            }
                        }
                       
                }else{  
                    
                    if(isset($total)){
                        if($total <= 1000){
                          $shipType = true;
                          $deliveryCharge = 0;
                        }
                    }else{
                        $shipType = true; 
                    }
                           
                }
            ?>
      

        <table class="table-cart table-cart-review">
            <tfoot>
                <tr class="cart-subtotal">
                    <th><?php echo e(__('Subtotal')); ?></th>
                    <td class="text-right">
                        <span class="strong-600"><?php echo e(single_price($subtotal)); ?></span>
                    </td>
                </tr>

                <tr class="cart-shipping">
                    <th><?php echo e(__('Tax')); ?></th>
                    <td class="text-right">
                        <span class="text-italic"><?php echo e(single_price($tax)); ?></span>
                    </td>
                </tr>

                <tr class="cart-shipping">
                    <th><?php echo e(__('Product Shipping')); ?></th>
                    <td class="text-right">
                        <span class="text-italic"><?php echo e(single_price($shipping)); ?></span>
                    </td>
                </tr>

                <?php if(Session::has('coupon_discount')): ?>
                    <tr class="cart-shipping">
                        <th><?php echo e(__('Coupon Discount')); ?></th>
                        <td class="text-right">
                            <span class="text-italic"><?php echo e(single_price(Session::get('coupon_discount'))); ?></span>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if($walletDisplay): ?>
                <tr>
                    <td>
                        
                            <?php if(Session::has('wallet_credit')): ?>
                            <input type="checkbox" checked class="btn btn-base-1 my-2" onclick="$('#remove_wallet_credit').submit()" >&nbsp;&nbsp;<span>Use Wallet Balance:- </span><?php echo e(single_price($totalWallet)); ?>

                            <?php else: ?>
                           
                            <input type="checkbox"  class="btn btn-base-1 my-2 " onclick="$('#apply_wallet_credit').submit()" style="width: 28px;">&nbsp;&nbsp;<span>Use Wallet Balance:-</span> <?php echo e(single_price($totalWallet)); ?>

                            <?php endif; ?>
                                                            <!-- <button type="button" class="btn btn-base-2 my-2" onclick="show_wallet_modal()">Recharge Wallet</button> -->
                                                       
                       
                    </td>
                </tr>
                <?php endif; ?>
                <?php if($deliveryCharge > 0): ?> 
                
                <tr class="cart-shipping">
                    <th><?php echo e(__('Delivery Charges')); ?></th>
                    <td class="text-right">
                        <span class="text-italic"><?php echo e(single_price($deliveryCharge)); ?> </span>
                    </td>
                </tr>
                <?php endif; ?>
                
                <?php if(Session::has('wallet_credit')): ?>
                <tr class="cart-shipping">
                    <th><?php echo e(__('Wallet Credit')); ?></th>
                    <td class="text-right">
                        <span class="text-italic"><?php echo e(single_price($crediteachOrder)); ?></span>
                    </td>
                    <?php 
                    $total -= $crediteachOrder;
                    ?>
                </tr>
                <?php endif; ?>
                

                <tr class="cart-total">
                    <th><span class="strong-600"><?php echo e(__('Total')); ?></span></th>
                    <td class="text-right">
                        <strong><span><?php echo e(single_price($total+$deliveryCharge)); ?></span></strong>
                    </td>
                </tr>
                <input type="hidden" id="totalPrice" value="<?php echo e($total); ?>">
            </tfoot>
        </table>
        <?php   
                if(isset($shipData['paymentOption'])){
                    if($shipData['paymentOption'] == 'COD'){
                        if($total >= 1 && $total <= 499){
                                
                                $freeship = 1000 - $subtotal;
                             ?>
                               <ul class="p-0">
                                  <li style="color:red; list-style-type: none;" class="mt-2">Pay online for free shipping and shop more with savings </li>
                                  <li style="color:red; list-style-type: none;" class="mt-2">Add item(s) worth Rs.( <?php echo $freeship;?>/-) for free shipping.</li>
                               </ul>
                             <?php 
                              
                        }else if($total  >= 500 && $total <=  999){
                                
                              $freeship = 1000 - $shipData['totalAmount'];
                              ?>
                               <ul class="p-0">
                                  <li style="color:red; list-style-type: none;" class="mt-2">Pay online for free shipping and shop more with savings</li>
                                 <li style="color:red; list-style-type: none;" class="mt-2"> Add item(s) worth Rs.( <?php echo $freeship;?>/-) for free shipping.</li>
                               </ul>
                             <?php 
                        }
                    }else if($shipData['paymentOption'] == 'razorpay'){
                        
                        if($total >= 1 && $total <= 499){
                           
                            $freeship = 500 - $subtotal;
                            ?>
                            <ul class="p-0">
                               <li style="color:red; list-style-type: none;" class="mt-2">Add item(s) worth Rs.( <?php echo $freeship;?>/-) for free shipping !!</li>
                            </ul>
                          <?php 
                           
                        }
                    }
                }else{
                    if($total <= 500){
                        $freeship = 500 - $subtotal;
                        ?>
                        <ul class="p-0">
                           <li style="color:red; list-style-type: none;" class="mt-2">Add item(s) worth Rs.( <?php echo $freeship;?>/-) for free shipping !!</li>
                        </ul>
                      <?php 
                    }
                }
                
                //Get city haat product cashback
                $casbackConfig = \App\UserCashbackConfig::first();
               
                $cityHaatTotal = Session::get('cart_cashback_Product_subtotal');
                if( $cityHaatTotal   <  $casbackConfig->casback_minimum_amount_limit){
                    $remainingAmount = $casbackConfig->casback_minimum_amount_limit - $cityHaatTotal;
                    ?>
                       <ul class="p-0">
                           <li style="color:red; list-style-type: none;" class="mt-2">Add more worth Rs.( <?php echo $remainingAmount;?> /-) city haat product item(s) for <?php echo $casbackConfig->casback_amount;?> % Cashback</li>
                        </ul>
                    <?php
                }
        ?>
         
        <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="
        ----------COD------------</br>
                            1-499 Shipping Charge Rs.50 </br>
        					500-999 Shipping charge Rs.30</br></br>
        					------Online Payment------</br>
        					1-499 Shipping Charge Rs.50</br>
        					500 above shipping free!!</br>-">
               <button class="btn btn-primary" style="pointer-events: none;" type="button" disabled>To save you shipping charges click here</button>
       </span>
        </div>

        
      
        <div class="clearfix">&nbsp;&nbsp;&nbsp;</div>
       


        <div>
        <?php if(Auth::check() && \App\BusinessSetting::where('type', 'coupon_system')->first()->value == 1): ?>
            <?php if(Session::has('coupon_discount')): ?>
                <div class="mt-3">
                    <form class="form-inline" name="removecouponform" id="removecouponform" action="<?php echo e(route('checkout.remove_coupon_code')); ?>" method="POST" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <?php if($applyCoupe): ?>
                        <div class="form-group flex-grow-1 mb-0">
                            <div class="form-control bg-gray w-100"><?php echo e(\App\Coupon::find(Session::get('coupon_id'))->code); ?></div>
                        </div>
                        <button type="submit" class="btn btn-base-1" style="background:#131921"><?php echo e(__('Change Coupon')); ?></button>
                    <?php endif; ?>
                    </form>
                </div>
            <?php else: ?>
                <div class="mt-3">
                    <form class="form-inline" name="applycpnform" id="applycpnform" action="<?php echo e(route('checkout.apply_coupon_code')); ?>" method="POST" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <?php if($applyCoupe): ?>
                            <div class="form-group flex-grow-1 mb-0">
                                <input type="text" class="form-control w-100" name="code" id="orignalcoupon" placeholder="<?php echo e(__('Have coupon code? Enter here')); ?>" required>
                            </div>
                            <button type="submit" class="btn btn-base-1" style="background:#131921"><?php echo e(__('Apply')); ?></button>
                        <?php endif; ?>
                        
                    </form>
                </div>
            <?php endif; ?>
        <?php endif; ?>
       </div>
       
    </div>
</div>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/partials/cart_summary.blade.php ENDPATH**/ ?>