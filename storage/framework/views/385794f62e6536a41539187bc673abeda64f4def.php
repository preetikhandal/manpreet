<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container" >
    <div id="mainnav">

        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap" >
            <div class="nano" >
                <div class="nano-content">

                    <!--Profile Widget-->
                    <!--================================-->
                    


                    <!--Shortcut buttons-->
                    <!--================================-->
                    <div id="mainnav-shortcut" class="hidden">
                        <ul class="list-unstyled shortcut-wrap">
                            <li class="col-xs-3" data-content="My Profile">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                    <i class="demo-pli-male"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Messages">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                    <i class="demo-pli-speech-bubble-3"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Activity">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                    <i class="demo-pli-thunder"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Lock Screen">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                    <i class="demo-pli-lock-2"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--================================-->
                    <!--End shortcut buttons-->


                    <ul id="mainnav-menu" class="list-group" style="overflow-y: scroll;height: 90vh;">

                        <!--Category name-->
                        

                        <!--Menu list item-->
                        <li class="<?php echo e(areActiveRoutes(['admin.dashboard'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('admin.dashboard')); ?>">
                                <i class="fa fa-home"></i>
                                <span class="menu-title"><?php echo e(__('Dashboard')); ?></span>
                            </a>
                        </li>
                        
                        <li class="city">
                            <a class="nav-link" href="<?php echo e(url('admin/city')); ?>">
                                <i class="fa fa-home"></i>
                                <span class="menu-title"><?php echo e(__('City')); ?></span>
                            </a>
                        </li>


                        <?php if(\App\Addon::where('unique_identifier', 'pos_system')->first() != null && \App\Addon::where('unique_identifier', 'pos_system')->first()->activated): ?>

                            <li>
                                <a href="#">
                                    <i class="fa fa-print"></i>
                                    <span class="menu-title"><?php echo e(__('POS Manager')); ?></span>
                                    <i class="arrow"></i>
                                </a>

                                <!--Submenu-->
                                <ul class="collapse">
                                    <li class="<?php echo e(areActiveRoutes(['poin-of-sales.index', 'poin-of-sales.create'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('poin-of-sales.index')); ?>"><?php echo e(__('POS Manager')); ?></a>
                                    </li>
                                    <li class="<?php echo e(areActiveRoutes(['poin-of-sales.activation'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('poin-of-sales.activation')); ?>"><?php echo e(__('POS Configuration')); ?></a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <!-- Product Menu -->
                        <?php if(Auth::user()->user_type == 'admin' || in_array('1', json_decode(Auth::user()->staff->role->permissions))): ?>
                            <li>
                                <a href="#">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="menu-title"><?php echo e(__('Products')); ?></span>
                                    <i class="arrow"></i>
                                </a>

                                <!--Submenu-->
                                <ul class="collapse">
                                    <li class="<?php echo e(areActiveRoutes(['brands.index', 'brands.create', 'brands.edit'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('brands.index')); ?>"><?php echo e(__('Brand')); ?></a>
                                    </li>
                                    <li class="<?php echo e(areActiveRoutes(['categories.index', 'categories.create', 'categories.edit'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('categories.index')); ?>"><?php echo e(__('Category')); ?></a>
                                    </li>
                                    <li class="<?php echo e(areActiveRoutes(['subcategories.index', 'subcategories.create', 'subcategories.edit'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('subcategories.index')); ?>"><?php echo e(__('Subcategory')); ?></a>
                                    </li>
                                    <li class="<?php echo e(areActiveRoutes(['subsubcategories.index', 'subsubcategories.create', 'subsubcategories.edit'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('subsubcategories.index')); ?>"><?php echo e(__('Sub Subcategory')); ?></a>
                                    </li>
                                    <li class="<?php echo e(areActiveRoutes(['products.admin', 'products.create', 'products.admin.edit'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('products.admin')); ?>"><?php echo e(__('In House Products')); ?></a>
                                    </li>
                                    <li class="<?php echo e(areActiveRoutes(['products.seller', 'products.seller.edit'])); ?>">
                                            <a class="nav-link" href="<?php echo e(route('products.seller')); ?>"><?php echo e(__('Seller Products')); ?></a>
                                    </li>
                                    <!--li class="<?php echo e(areActiveRoutes(['classified_products'])); ?>">
                                            <a class="nav-link" href="<?php echo e(route('classified_products')); ?>"><?php echo e(__('Classified Product')); ?></a>
                                    </li-->
                                    <li class="<?php echo e(areActiveRoutes(['product_bulk_upload.index'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('product_bulk_upload.index')); ?>"><?php echo e(__('Bulk Import')); ?></a>
                                    </li>
                                    <li class="<?php echo e(areActiveRoutes(['bulk_remove_view', 'bulk_remove'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('bulk_remove_view')); ?>"><?php echo e(__('Bulk Remover')); ?></a>
                                    </li>
                                     <!--li class="<?php echo e(areActiveRoutes(['product_bulk_export.export'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('product_bulk_export.index')); ?>"><?php echo e(__('All Bulk Export')); ?></a>
                                    </li-->
									<li class="<?php echo e(areActiveRoutes(['category.export'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('category.export')); ?>"><?php echo e(__('Product Export')); ?></a>
                                    </li>
                                    <?php
                                        $review_count = DB::table('reviews')
                                                    ->orderBy('code', 'desc')
                                                    ->join('products', 'products.id', '=', 'reviews.product_id')
                                                    ->where('products.user_id', Auth::user()->id)
                                                    ->where('reviews.viewed', 0)
                                                    ->select('reviews.id')
                                                    ->distinct()
                                                    ->count();
                                    ?>
                                    <li class="<?php echo e(areActiveRoutes(['reviews.index'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('reviews.index')); ?>"><?php echo e(__('Product Reviews')); ?><?php if($review_count > 0): ?><span class="pull-right badge badge-info"><?php echo e($review_count); ?></span><?php endif; ?></a>
                                    </li>
                                    <li class="<?php echo e(areActiveRoutes(['stock.index'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('stock.index')); ?>"><?php echo e(__('Stock Update')); ?></span></a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <!--<li>
                            <a href="#">
                                <i class="fa fa-list"></i>
                                <span class="menu-title"><?php echo e(__('Digital Products')); ?></span>
                                <i class="arrow"></i>
                            </a>
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['digitalproducts.index', 'digitalproducts.create', 'digitalproducts.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('digitalproducts.index')); ?>"><?php echo e(__('Products')); ?></a>
                                </li>
                            </ul>
                        </li>-->

                        <?php if(Auth::user()->user_type == 'admin' || in_array('2', json_decode(Auth::user()->staff->role->permissions))): ?>
                        <li class="<?php echo e(areActiveRoutes(['flash_deals.index', 'flash_deals.create', 'flash_deals.edit'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('flash_deals.index')); ?>">
                                <i class="fa fa-bolt"></i>
                                <span class="menu-title"><?php echo e(__('Deals')); ?></span>
                            </a>
                        </li>
                        
                        
                        
                        <?php endif; ?>

                        <?php if(Auth::user()->user_type == 'admin' || in_array('3', json_decode(Auth::user()->staff->role->permissions))): ?>
                            <?php
                                $admin_id = \App\User::where('user_type', 'admin')->first()->id;
                                
                                $otc_order = \App\Order::distinct('order_id')->join('order_details','order_details.order_id','orders.id')->where('order_details.delivery_status', 'pending')->groupBy('order_id')->count();
                                
                                $medicine_order = \App\Order::distinct('order_id')->join('order_details','order_details.order_id','orders.id')->where('order_details.delivery_status', 'confirmation_pending')->groupBy('order_id')->count();
                                   
                                $ready_to_ship = \App\Order::distinct('order_id')->join('order_details','order_details.order_id','orders.id')->where('order_details.delivery_status', 'ready_to_ship')->groupBy('order_id')->count();
                                   
                                $on_ship = \App\Order::distinct('order_id')->join('order_details','order_details.order_id','orders.id')->where('order_details.delivery_status', 'on_ship')->groupBy('order_id')->count();
                                   
                                $ready_to_delivery = \App\Order::distinct('order_id')->join('order_details','order_details.order_id','orders.id')->where('order_details.delivery_status', 'ready_to_delivery')->groupBy('order_id')->count();

                               $on_delivery = \App\Order::distinct('order_id')->join('order_details','order_details.order_id','orders.id')->where('order_details.delivery_status', 'on_delivery')->groupBy('order_id')->count();
                               
                               
                               $processing_refund = \App\Order::distinct('order_id')->join('order_details','order_details.order_id','orders.id')->where('order_details.delivery_status', 'processing_refund')->groupBy('order_id')->count();
                               $sellers_users_id = \App\User::where('user_type', 'seller')->select('id')->get()->pluck('id')->all();
                               $seller_order = \App\Order::distinct('order_id')->join('order_details','order_details.order_id','orders.id')->whereIn('order_details.seller_id', $sellers_users_id)->where('order_details.delivery_status', 'pending')->groupBy('order_id')->count();
                              
                            ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-shopping-basket"></i>
                                <span class="menu-title"><?php echo e(__('Orders')); ?></span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['orders.index.confirmation_pending.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.confirmation_pending.admin')); ?>"><?php echo e(__('Medicine Orders')); ?> <?php if($medicine_order > 0): ?><span class="pull-right badge badge-info"><?php echo e($medicine_order); ?></span><?php endif; ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.pending.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.pending.admin')); ?>"><?php echo e(__('OTC Orders')); ?> <?php if($otc_order > 0): ?><span class="pull-right badge badge-info"><?php echo e($otc_order); ?></span><?php endif; ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.ready_to_ship.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.ready_to_ship.admin')); ?>"><?php echo e(__('Ready to Ship')); ?> <?php if($ready_to_ship > 0): ?><span class="pull-right badge badge-info"><?php echo e($ready_to_ship); ?></span><?php endif; ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.on_ship.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.on_ship.admin')); ?>"><?php echo e(__('On Shipping')); ?> <?php if($on_ship > 0): ?><span class="pull-right badge badge-info"><?php echo e($on_ship); ?></span><?php endif; ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.ready_to_delivery.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.ready_to_delivery.admin')); ?>"><?php echo e(__('Ready to Delivery')); ?> <?php if($ready_to_delivery > 0): ?><span class="pull-right badge badge-info"><?php echo e($ready_to_delivery); ?></span><?php endif; ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.on_delivery.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.on_delivery.admin')); ?>"><?php echo e(__('On Delivery')); ?> <?php if($on_delivery > 0): ?><span class="pull-right badge badge-info"><?php echo e($on_delivery); ?></span><?php endif; ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.hold.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.hold.admin')); ?>"><?php echo e(__('Hold')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.processing_refund.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.processing_refund.admin')); ?>"><?php echo e(__('For Refund')); ?> <?php if($processing_refund > 0): ?><span class="pull-right badge badge-info"><?php echo e($processing_refund); ?></span><?php endif; ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.cancel.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.cancel.admin')); ?>"><?php echo e(__('Cancel')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.trash.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.trash.admin')); ?>"><?php echo e(__('Trash')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.delivered.admin'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.delivered.admin')); ?>"><?php echo e(__('Delivered')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.admin', 'orders.show'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.admin')); ?>"><?php echo e(__('All Order')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['orders.index.admin.seller'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.admin.seller')); ?>"><?php echo e(__('Seller Orders')); ?> <?php if($seller_order > 0): ?><span class="pull-right badge badge-info"><?php echo e($seller_order); ?></span><?php endif; ?></a></a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-shopping-basket"></i>
                                <span class="menu-title"><?php echo e(__('Denied Table')); ?></span>
                            </a>
                            
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-shopping-basket"></i>
                                <span class="menu-title"><?php echo e(__('Return Request')); ?></span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['return.index.admin', 'return.show_return'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('return.index.admin')); ?>"><?php echo e(__('All Return Order')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['return.index.return_request.admin', 'return.show_return'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('return.index.return_request.admin')); ?>"><?php echo e(__('Return Request')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['return.index.return_accepted.admin', 'return.show_return'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('return.index.return_accepted.admin')); ?>"><?php echo e(__('Return Accepted')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['return.index.pickup_completed.admin', 'return.show_return'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('return.index.pickup_completed.admin')); ?>"><?php echo e(__('Pickup Completed')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['return.index.refunded.admin', 'return.show_return'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('return.index.refunded.admin')); ?>"><?php echo e(__('Refunded')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['return.index.return_cancelled.admin', 'return.show_return'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('return.index.return_cancelled.admin')); ?>"><?php echo e(__('Cancel Request')); ?></a>
                                </li>
                            </ul>
                        </li>
                        <!--li class="<?php echo e(areActiveRoutes(['orders.admin.vendor'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('orders.admin.vendor')); ?>">
                                <i class="fa fa-shopping-basket"></i>
                                <span class="menu-title"><?php echo e(__('Vendors orders')); ?></span>
                            </a>
                        </li-->
                        <?php endif; ?>
                        
                        
                        
                        <?php if((Auth::user()->user_type == 'admin' || in_array('30', json_decode(Auth::user()->staff->role->permissions)))): ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-user-plus"></i>
                                <span class="menu-title"><?php echo e(__('Marg')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['marg.order.pending'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('marg.order.pending')); ?>"><?php echo e(__('Pending Order')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['marg.order.processed'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('marg.order.processed')); ?>"><?php echo e(__('Processed Order')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['unlistedProduct'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('unlistedProduct')); ?>"><?php echo e(__('Unlisted Product')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['unlistedCustomer'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('unlistedCustomer')); ?>"><?php echo e(__('Unlisted Customer')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['unmappedCustomer'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('unmappedCustomer')); ?>"><?php echo e(__('Unmapped Customer')); ?></a>
                                </li>
                            </ul>
                        </li>
                        <?php endif; ?>
                        
                        <?php if(Auth::user()->user_type == 'admin' || in_array('4', json_decode(Auth::user()->staff->role->permissions))): ?>
                        <li class="<?php echo e(areActiveRoutes(['sales.index', 'sales.show'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('sales.index')); ?>">
                                <i class="fa fa-money"></i>
                                <span class="menu-title"><?php echo e(__('Total sales')); ?></span>
                            </a>
                        </li>
                        <?php endif; ?>

                        
                        <?php if((Auth::user()->user_type == 'admin' || in_array('5', json_decode(Auth::user()->staff->role->permissions)))): ?>
                        <!--li>
                            <a href="#">
                                <i class="fa fa-user-plus"></i>
                                <span class="menu-title"><?php echo e(__('Vendors')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu>
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['vendors.index', 'vendors.create', 'vendors.edit', 'vendors.status', 'vendors.expenses', 'vendors.profile_modal'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('vendors.index')); ?>"><?php echo e(__('Vendor List')); ?> <?php /* @if($vendors > 0)<span class="pull-right badge badge-info">{{ $vendors }}</span> @endif */ ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['vendors.expenses'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('vendor.expenses')); ?>"><?php echo e(__('Vendor Expenses')); ?> </a>
                                </li>
                            </ul>
                        </li-->
                        <?php endif; ?>
                        
                        <?php if((Auth::user()->user_type == 'admin' || in_array('5', json_decode(Auth::user()->staff->role->permissions)))): ?>
                        <!--li>
                            <a href="#">
                                <i class="fa fa-user-plus"></i>
                                <span class="menu-title"><?php echo e(__('Expenses')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu>
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['Expenses'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('Expenses')); ?>"><?php echo e(__('Expenses')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['ExpenseCategory'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('ExpenseCategory')); ?>"><?php echo e(__('Expenses Category')); ?> </a>
                                </li>
                            </ul>
                        </li-->
                        <?php endif; ?>
                        
                        <?php if((Auth::user()->user_type == 'admin' || in_array('5', json_decode(Auth::user()->staff->role->permissions))) && \App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1): ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-user-plus"></i>
                                <span class="menu-title"><?php echo e(__('Sellers')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['sellers.index', 'sellers.create', 'sellers.edit', 'sellers.payment_history','sellers.approved','sellers.profile_modal'])); ?>">
                                    <?php
                                        $sellers = \App\Seller::where('verification_status', 0)->where('verification_info', '!=', null)->count();
                                        //$withdraw_req = \App\SellerWithdrawRequest::where('viewed', '0')->get();
                                    ?>
                                    <a class="nav-link" href="<?php echo e(route('sellers.index')); ?>"><?php echo e(__('Seller List')); ?> <?php if($sellers > 0): ?><span class="pull-right badge badge-info"><?php echo e($sellers); ?></span> <?php endif; ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['withdraw_requests_all'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('withdraw_requests_all')); ?>"><?php echo e(__('Seller Withdraw Requests')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['sellers.payment_histories'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('sellers.payment_histories')); ?>"><?php echo e(__('Seller Payments History')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['business_settings.vendor_commission'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('sellers.sellercommission')); ?>"><?php echo e(__('Seller Commission')); ?></a>
                                </li>

                                <li class="<?php echo e(areActiveRoutes(['products.seller', 'products.seller.edit'])); ?>">
                                            <a class="nav-link" href="<?php echo e(route('products.seller')); ?>"><?php echo e(__('Seller Products')); ?></a>
                                    </li>
                                <!--<li class="<?php echo e(areActiveRoutes(['seller_verification_form.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('seller_verification_form.index')); ?>"><?php echo e(__('Seller Verification Form')); ?></a>
                                </li>-->
                                 <li class="<?php echo e(areActiveRoutes(['orders.index.admin.seller'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('orders.index.admin.seller')); ?>"><?php echo e(__('Seller Orders')); ?> <?php if($seller_order > 0): ?><span class="pull-right badge badge-info"><?php echo e($seller_order); ?></span><?php endif; ?></a></a>
                                </li>
                            </ul>
                        </li>
                        <?php endif; ?>

                        <?php if(Auth::user()->user_type == 'admin' || in_array('6', json_decode(Auth::user()->staff->role->permissions))): ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-user-plus"></i>
                                <span class="menu-title"><?php echo e(__('Customers')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['customers.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('customers.index')); ?>"><?php echo e(__('Customer list')); ?></a>
                                </li>
                                <!--<li class="<?php echo e(areActiveRoutes(['customer_packages.index','customer_packages.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('customer_packages.index')); ?>"><?php echo e(__('Classified Packages')); ?></a>
                                </li>-->
                            </ul>
                        </li>
                        <?php endif; ?>
                        <?php
                            //$conversation = \App\Conversation::where('receiver_id', Auth::user()->id)->where('receiver_viewed', '1')->get();
                            $conversation=array();
                        ?>
                     <!--   <li class="<?php echo e(areActiveRoutes(['conversations.admin_index', 'conversations.admin_show'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('conversations.admin_index')); ?>">
                                <i class="fa fa-comment"></i>
                                <span class="menu-title"><?php echo e(__('Conversations')); ?></span>
                                <?php if(count($conversation) > 0): ?>
                                    <span class="pull-right badge badge-info"><?php echo e(count($conversation)); ?></span>
                                <?php endif; ?>
                            </a>
                            
                        </li>
                        -->
                        <li class="<?php echo e(areActiveRoutes(['productrequest.show'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('productrequest.show')); ?>">
                                <i class="fa fa-search"></i>
                                <span class="menu-title"><?php echo e(__('Request Products')); ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-file"></i>
                                <span class="menu-title"><?php echo e(__('Reports')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['report.sales'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('report.sales')); ?>"><?php echo e(__('Sales Report')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['report.expenses'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('report.expenses')); ?>"><?php echo e(__('Expenses Report')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['stock_report.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('stock_report.index')); ?>"><?php echo e(__('Stock Report')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['in_house_sale_report.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('in_house_sale_report.index')); ?>"><?php echo e(__('In House Sale Report')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['seller_report.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('seller_report.index')); ?>"><?php echo e(__('Seller Report')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['seller_sale_report.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('seller_sale_report.index')); ?>"><?php echo e(__('Seller Based Selling Report')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['wish_report.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('wish_report.index')); ?>"><?php echo e(__('Product Wish Report')); ?></a>
                                </li>
                            </ul>
                        </li>
<?php
/*
                        @if(Auth::user()->user_type == 'admin' || in_array('7', json_decode(Auth::user()->staff->role->permissions)))
 */
 ?>                       
                        <!--<li>
                            <a href="#">
                                <i class="fa fa-envelope"></i>
                                <span class="menu-title"><?php echo e(__('Messaging')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['newsletters.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('newsletters.index')); ?>"><?php echo e(__('Newsletters')); ?></a>
                                </li>

                                <?php if(\App\Addon::where('unique_identifier', 'otp_system')->first() != null): ?>
                                    <li class="<?php echo e(areActiveRoutes(['sms.index'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('sms.index')); ?>"><?php echo e(__('SMS')); ?></a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </li>-->
                    <?php
                    /*
                        @endif
                     */
                     ?> 
                        <?php if(Auth::user()->user_type == 'admin' || in_array('8', json_decode(Auth::user()->staff->role->permissions))): ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-briefcase"></i>
                                <span class="menu-title"><?php echo e(__('Business Settings')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['activation.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('activation.index')); ?>"><?php echo e(__('Activation')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['payment_method.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('payment_method.index')); ?>"><?php echo e(__('Payment method')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['smtp_settings.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('smtp_settings.index')); ?>"><?php echo e(__('SMTP Settings')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['google_analytics.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('google_analytics.index')); ?>"><?php echo e(__('Google Analytics')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['facebook_chat.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('facebook_chat.index')); ?>"><?php echo e(__('Facebook Pixel')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['social_login.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('social_login.index')); ?>"><?php echo e(__('Social Media Login')); ?></a>
                                </li>
                                
                                <li class="<?php echo e(areActiveRoutes(['currency.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('currency.index')); ?>"><?php echo e(__('Currency')); ?></a>
                                </li>
                                
                                 

                                <li class="<?php echo e(areActiveRoutes(['currency.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('user-cashback-config')); ?>"><?php echo e(__('User Cashback Configuration')); ?></a>
                                </li>
                         <!--    <li class="<?php echo e(areActiveRoutes(['languages.index', 'languages.create', 'languages.store', 'languages.show', 'languages.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('languages.index')); ?>"><?php echo e(__('Languages')); ?></a>
                                </li>
                                -->
                            </ul>
                        </li>
                        <?php endif; ?>

                        <?php if(Auth::user()->user_type == 'admin' || in_array('9', json_decode(Auth::user()->staff->role->permissions))): ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-desktop"></i>
                                <span class="menu-title"><?php echo e(__('Frontend Settings')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['home_settings.index', 'home_banners.index', 'sliders.index', 'home_categories.index', 'home_banners.create', 'home_categories.create','home_categories.create2', 'home_categories.edit', 'sliders.create'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('home_settings.index')); ?>"><?php echo e(__('Home')); ?></a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="menu-title"><?php echo e(__('Policy Pages')); ?></span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">

                                        <li class="<?php echo e(areActiveRoutes(['sellerpolicy.index'])); ?>">
                                            <a class="nav-link" href="<?php echo e(route('sellerpolicy.index', 'seller_policy')); ?>"><?php echo e(__('Seller Policy')); ?></a>
                                        </li>
                                        <li class="<?php echo e(areActiveRoutes(['returnpolicy.index'])); ?>">
                                            <a class="nav-link" href="<?php echo e(route('returnpolicy.index', 'return_policy')); ?>"><?php echo e(__('Return Policy')); ?></a>
                                        </li>
                                        <li class="<?php echo e(areActiveRoutes(['supportpolicy.index'])); ?>">
                                            <a class="nav-link" href="<?php echo e(route('supportpolicy.index', 'support_policy')); ?>"><?php echo e(__('Support Policy')); ?></a>
                                        </li>
                                        <li class="<?php echo e(areActiveRoutes(['terms.index'])); ?>">
                                            <a class="nav-link" href="<?php echo e(route('terms.index', 'terms')); ?>"><?php echo e(__('Terms & Conditions')); ?></a>
                                        </li>
                                        <li class="<?php echo e(areActiveRoutes(['privacypolicy.index'])); ?>">
                                            <a class="nav-link" href="<?php echo e(route('privacypolicy.index', 'privacy_policy')); ?>"><?php echo e(__('Privacy Policy')); ?></a>
                                        </li>
                                    </ul>

                                </li>
                                <!--<li class="<?php echo e(areActiveRoutes(['links.index', 'links.create', 'links.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('links.index')); ?>"><?php echo e(__('Useful Link')); ?></a>
                                </li>-->
                                <li class="<?php echo e(areActiveRoutes(['generalsettings.index'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('generalsettings.index')); ?>"><?php echo e(__('General Settings')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['generalsettings.logo'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('generalsettings.logo')); ?>"><?php echo e(__('Logo Settings')); ?></a>
                                </li>
                                <!--<li class="<?php echo e(areActiveRoutes(['generalsettings.color'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('generalsettings.color')); ?>"><?php echo e(__('Color Settings')); ?></a>
                                </li>-->
                            </ul>
                        </li>
                        <?php endif; ?>

                        <?php if(Auth::user()->user_type == 'admin' || in_array('12', json_decode(Auth::user()->staff->role->permissions))): ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-gear"></i>
                                <span class="menu-title"><?php echo e(__('E-commerce Setup')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['attributes.index','attributes.create','attributes.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('attributes.index')); ?>"><?php echo e(__('Attribute')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['coupon.index','coupon.create','coupon.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('coupon.index')); ?>"><?php echo e(__('Coupon')); ?></a>
                                </li>
                                <!--<li class="<?php echo e(areActiveRoutes(['pick_up_points.index','pick_up_points.create','pick_up_points.edit'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('pick_up_points.index')); ?>"><?php echo e(__('Pickup Point')); ?></a>
                                </li>-->
                            </ul>
                        </li>
                        <?php endif; ?>


                        <?php if(\App\Addon::where('unique_identifier', 'offline_payment')->first() != null): ?>
                            <li>
                                <a href="#">
                                    <i class="fa fa-bank"></i>
                                    <span class="menu-title"><?php echo e(__('Offline Payment System')); ?></span>
                                    <i class="arrow"></i>
                                </a>

                                <!--Submenu-->
                                <ul class="collapse">
                                    <li class="<?php echo e(areActiveRoutes(['manual_payment_methods.index', 'manual_payment_methods.create', 'manual_payment_methods.edit'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('manual_payment_methods.index')); ?>"><?php echo e(__('Manual Payment Methods')); ?></a>
                                    </li>
                                    <li class="<?php echo e(areActiveRoutes(['offline_wallet_recharge_request.index'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('offline_wallet_recharge_request.index')); ?>"><?php echo e(__('Offline Wallet Rechage')); ?></a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <?php if(\App\Addon::where('unique_identifier', 'paytm')->first() != null): ?>
                            <li>
                                <a href="#">
                                    <i class="fa fa-mobile"></i>
                                    <span class="menu-title"><?php echo e(__('Paytm Payment Gateway')); ?></span>
                                    <i class="arrow"></i>
                                </a>

                                <!--Submenu-->
                                <ul class="collapse">
                                    <li class="<?php echo e(areActiveRoutes(['paytm.index'])); ?>">
                                        <a class="nav-link" href="<?php echo e(route('paytm.index')); ?>"><?php echo e(__('Set Paytm Credentials')); ?></a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                        <?php if(Auth::user()->user_type == 'admin' || in_array('13', json_decode(Auth::user()->staff->role->permissions))): ?>
                            <?php
                                $support_ticket = DB::table('tickets')
                                            ->where('viewed', 0)
                                            ->select('id')
                                            ->count();
                            ?>
                        <li class="<?php echo e(areActiveRoutes(['support_ticket.admin_index', 'support_ticket.admin_show'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('support_ticket.admin_index')); ?>">
                                <i class="fa fa-support"></i>
                                <span class="menu-title"><?php echo e(__('Suppot Ticket')); ?> <?php if($support_ticket > 0): ?><span class="pull-right badge badge-info"><?php echo e($support_ticket); ?></span><?php endif; ?></span>
                            </a>
                        </li>
                        <?php endif; ?>

                        <?php if(Auth::user()->user_type == 'admin' || in_array('11', json_decode(Auth::user()->staff->role->permissions))): ?>
                        <li class="<?php echo e(areActiveRoutes(['sitemap.index', 'sitemap.edit', 'sitemap.create'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('sitemap.index')); ?>">
                                <i class="fa fa-search"></i>
                                <span class="menu-title"><?php echo e(__('Sitemap Setting')); ?></span>
                            </a>
                        </li>
                        <li class="<?php echo e(areActiveRoutes(['seosetting.index'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('seosetting.index')); ?>">
                                <i class="fa fa-search"></i>
                                <span class="menu-title"><?php echo e(__('SEO Setting')); ?></span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(Auth::user()->user_type == 'admin' || in_array('11', json_decode(Auth::user()->staff->role->permissions))): ?>
                        <li class="<?php echo e(areActiveRoutes(['Blog'])); ?>">
                            <a class="nav-link" href="<?php echo e(route('blog.index')); ?>">
                                <i class="fa fa-search"></i>
                                <span class="menu-title"><?php echo e(__('Blog')); ?></span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(Auth::user()->user_type == 'admin' || in_array('10', json_decode(Auth::user()->staff->role->permissions))): ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span class="menu-title"><?php echo e(__('Staffs')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['staffs.index', 'staffs.create', 'staffs.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('staffs.index')); ?>"><?php echo e(__('All staffs')); ?></a>
                                </li>
                                <li class="<?php echo e(areActiveRoutes(['roles.index', 'roles.create', 'roles.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('roles.index')); ?>"><?php echo e(__('Staff permissions')); ?></a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span class="menu-title"><?php echo e(__('Feedback')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['staffs.index', 'staffs.create', 'staffs.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('feedback')); ?>"><?php echo e(__('All Feedback')); ?></a>
                                </li> 
                            </ul>
                        </li>


                        <li>
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span class="menu-title"><?php echo e(__('Pharmacy Order')); ?></span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="<?php echo e(areActiveRoutes(['staffs.index', 'staffs.create', 'staffs.edit'])); ?>">
                                    <a class="nav-link" href="<?php echo e(route('pharmacy')); ?>"><?php echo e(__('All Prarmacy')); ?></a>
                                </li>
                            </ul>
                        </li>
                        <?php endif; ?>
                        <?php if(Auth::user()->user_type == 'admin' || in_array('15', json_decode(Auth::user()->staff->role->permissions))): ?>
                            <!--<li class="<?php echo e(areActiveRoutes(['addons.index', 'addons.create'])); ?>">
                                <a class="nav-link" href="<?php echo e(route('addons.index')); ?>">
                                    <i class="fa fa-wrench"></i>
                                    <span class="menu-title"><?php echo e(__('Addon Manager')); ?></span>
                                </a>
                            </li>-->
                        <?php endif; ?>

                    </ul>
                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->

    </div>
</nav>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/inc/admin_sidenav.blade.php ENDPATH**/ ?>