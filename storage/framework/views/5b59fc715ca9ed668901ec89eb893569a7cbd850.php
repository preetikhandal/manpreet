<?php $__env->startSection('content'); ?>
<style>
	.checkbox input[type="checkbox"]:checked + label::after{
		color:#273895;
		border:1px solid #273895;
	}
</style>
        <?php if($errors->any()): ?>
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e($error); ?></li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		<?php endif; ?>
	<section class="bg-white pt-1 pb-3 py-md-4" id="userlogin_section" <?php if($mobileapp == 1): ?> style="margin-top:90px" <?php endif; ?>>
		<div class="container">
			<div class="d-flex justify-content-center">
				<div class="user_card py-1">
					<div class="text-center px-3 my-2">
						<h1 class="heading heading-4 strong-500" id="create_title">
							<?php echo e(__('Create an account.')); ?>

						</h1>
					</div>
					<div class="px-5" id="socialmedialogin">
						<div class="">
							<?php if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1 || \App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1): ?>
								<div>
								     <a href="<?php echo e(route('social.login', ['provider' => 'apple'])); ?>" class="btn btn-styled btn-block btn-apple btn-icon--2 btn-icon-left px-4 mb-3">
											<span class="icon fa fa-apple" ></span> <?php echo e(__('Sign in with Apple')); ?>

									</a>
									<?php if(\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1): ?>
										<a href="<?php echo e(route('social.login', ['provider' => 'facebook'])); ?>" class="btn btn-styled btn-block btn-facebook btn-icon--2 btn-icon-left px-4 mb-3" style="color:#fff!important">
											<i class="icon fa fa-facebook text-white"></i> <?php echo e(__('Login with Facebook')); ?>

										</a>
									<?php endif; ?>
									<?php if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1): ?>
										<a href="<?php echo e(route('social.login', ['provider' => 'google'])); ?>" class="btn btn-styled btn-block btn-google btn-icon--2 btn-icon-left px-4">
											<i class="icon fa fa-google"></i> <?php echo e(__('Login with Google')); ?>

										</a>
									<?php endif; ?>
									<?php if(\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1): ?>
										<a href="<?php echo e(route('social.login', ['provider' => 'twitter'])); ?>" class="btn btn-styled btn-block btn-twitter btn-icon--2 btn-icon-left px-4">
											<i class="icon fa fa-twitter"></i> <?php echo e(__('Login with Twitter')); ?>

										</a>
									<?php endif; ?>
								</div>
								<div class="or or--1 mt-0 text-center">
									<span>or</span>
								</div>
							<?php endif; ?>
							<button class="btn btn-styled btn-block bg-blue btn-icon--2 btn-icon-left px-4 mb-3" id="signUpbtn">
								<?php echo e(__('Sign up with Phone or Email')); ?>

							</button>
						</div>
					</div>
					<div class="d-flex px-5 justify-content-center form_container">
						<form role="form" action="<?php echo e(route('register')); ?>" method="POST" id="loginform" style="display:none">
						    <input type="hidden" name="form_type" value="register"> 
							<?php echo csrf_field(); ?>
							<div class="input-group mb-3">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-user"></i></span>
								</div>
								<input type="text" class="input_user form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('name')); ?>" placeholder="<?php echo e(__('Name')); ?>" name="name" id="name" required >
								<br/>
								<?php if($errors->has('name')): ?>
									<span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('name')); ?></strong>
									</span>
								<?php endif; ?>
							</div>
							<?php if(\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated): ?>
								<div class="input-group mb-3">
									<div class="input-group-append">
										<span class="input-group-text"><i class="fa fa-phone"></i></span>
									</div>
									<input type="tel" id="phone-code" class="input_user border-right-0 h-100 w-100 form-control<?php echo e($errors->has('phone') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('phone')); ?>" placeholder="<?php echo e(__('Mobile Number')); ?>" name="phone" required>
									<br/><span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('phone')); ?></strong>
									</span>
								</div>
								<input type="hidden" name="country_code" value="India"/>
								
								<div class="input-group mb-3">
									<div class="input-group-append">
										<span class="input-group-text"><i class="fa fa-envelope"></i></span>
									</div>
									<input type="email" class="input_user form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('email')); ?>" placeholder="<?php echo e(__('Email')); ?>" name="email" required>
									<br/>
									<?php if($errors->has('email')): ?>
										<span class="invalid-feedback" role="alert">
											<strong><?php echo e($errors->first('email')); ?></strong>
										</span>
									<?php endif; ?>
								</div>
								
								<div class="input-group mb-3">
									<button class="btn btn-link p-0" type="button" onclick="toggleEmailPhone(this)">Use Email Instead</button>
								</div>
							<?php else: ?>
								<div class="input-group mb-3">
									<div class="input-group-append">
										<span class="input-group-text"><i class="fa fa-envelope"></i></span>
									</div>
									<input type="text" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('email')); ?>" placeholder="<?php echo e(__('Email or Phone')); ?>" name="email" id="email" required>
									<br/>
									<?php if($errors->has('email')): ?>
										<span class="invalid-feedback" role="alert">
											<strong><?php echo e($errors->first('email')); ?></strong>
										</span>
									<?php endif; ?>
								</div>
							<?php endif; ?>

							<div class="input-group mb-3" id="pass">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
								 <input type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" placeholder="<?php echo e(__('Password')); ?>" name="password" id="password" required>
								 <?php if($errors->has('password')): ?>
									<span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('password')); ?></strong>
									</span>
								<?php endif; ?>
							</div>
							<div class="input-group mb-3" id="conf_pass">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
								<input type="password" class="form-control" placeholder="<?php echo e(__('Confirm Password')); ?>" name="password_confirmation" id="password_confirmation" required>
							</div>
							<div class="input-group mb-3" id="OTPDiv" style="display:none;">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-lock"></i></span>
								</div>
									<input type="text" class="form-control input_user" value="<?php echo e(old('otp')); ?>" placeholder="<?php echo e(__('OTP')); ?>" name="otp" id="otp">
							</div>
							<div class="form-group mb-3">
								<div class="g-recaptcha" data-sitekey="<?php echo e(env('CAPTCHA_KEY')); ?>">
									<?php if($errors->has('g-recaptcha-response')): ?>
										<span class="invalid-feedback" style="display:block">
											<strong><?php echo e($errors->first('g-recaptcha-response')); ?></strong>
										</span>
									<?php endif; ?>
								</div>
							</div>
							
							<div class="checkbox pad-btm text-left">
								<input class="magic-checkbox" type="checkbox" name="checkbox_example_1" id="checkboxExample_1a" required>
								<label for="checkboxExample_1a" class="text-sm">By signing up you agree to our <a href="<?php echo e(route('terms')); ?>" class="text-blue" target="_blank">terms and conditions</a>.</label>
							</div>
							<div class="text-right" id="create_btn">
								<button type="button" class="btn btn-styled btn-base-1 w-100 btn-md bg-blue" onclick="create_acc()"><?php echo e(__('Create Account')); ?></button>
							</div>
							<div class="text-right" style="display:none" id="verify_btn">
								<button type="button" class="btn btn-styled btn-base-1 w-100 btn-md" onclick="verify_otp()"><?php echo e(__('Verify OTP')); ?></button>
							</div>
							<div class="col-12" id="mobileOtpretryDiv" style="display:none;">
                                <center id="mobileotpretrycontentdiv"></center>
                            </div>
						</form>
					</div>
					<div class="my-2">
						<div class="d-flex justify-content-center links">
							Already have an account?
							<a href="<?php echo e(route('user.login')); ?>" class="strong-600 ml-2 text-blue"><?php echo e(__('Log In')); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

    <script type="text/javascript">
    <?php if(old('form_type') == 'register'): ?> {
        $("#loginform").show();
        $("#socialmedialogin").hide();
    }
    <?php endif; ?>
    var otptime = 10;
    var timee;
    function verify_otp(){
        if($('#checkboxExample_1a').prop("checked") == true){
            $('#loginform').submit();
        }else{
           showFrontendAlert('error','Please check term & condition check box first.'); 
        }
    }
    function create_acc(){
        var name=$("#name").val();
        var mob=$("#email").val();
        var pass=$("#password").val();
        var conf_pass=$("#password_confirmation").val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if($('#checkboxExample_1a').prop("checked") == false){
            showFrontendAlert('error','Please check term & condition check box first.'); 
        }else if(name=='' || mob=='' || pass=='' || conf_pass==''){
            showFrontendAlert('error','All Fields are required.');
        }else if(isNaN(mob) && !filter.test(mob)){
            showFrontendAlert('error','Enter valid email id.');
        }else if(pass.length<8 || conf_pass.length<8){
            showFrontendAlert('error','The password & confirm password must be at least 8 characters.');
        }else if(pass != conf_pass ){
            showFrontendAlert('error','The password and Confirm password must be same.');
        }else if(isNaN(mob)){
            if(filter.test(mob)) {
                verify_otp();
            } else {
                showFrontendAlert('error','Enter valid email id.');
            }
        } else {
            var phoneno = /^\d{10}$/;
            if( mob.match(phoneno)){
                  $.post('<?php echo e(route('generate.otp.create')); ?>', { _token:'<?php echo e(csrf_token()); ?>', phone:mob}, function(data){
                    if(data.result == true) {
                            $('#email').attr('readonly','readonly');
                           $('#name').attr('readonly','readonly');
                           $('#pass').hide();
                           $('#conf_pass').hide();
                           $('#create_btn').hide();
                           $('#OTPDiv').show();
                           $('#verify_btn').show();
                           $('#create_title').html("Verify OTP");
                       // $('#loginDiv').show();
                        optcount();
                        timee = setInterval(function(){ optcount(); }, 1000);
                        msg = 'OTP has been generated successfully and send to your mobile number';
                        if(data.newuser == true) {
                            //msg += ' You will be registered as new user and agreed to our T & C.';
                            $('#SignupDiv').show();
                            $('#loginDiv').hide();
                        } else {
                            $('#SignupDiv').hide();
                            $('#loginDiv').show();
                        }
                        showFrontendAlert('success', msg);
                        
                    } else {
                        showFrontendAlert('warning', 'Enter a valid mobile number.');
                    }
                });
                  
                   
                   //showFrontendAlert('success','Valid phone number');
                }else{
                     showFrontendAlert('error','Enter valid Phone Number.');
                }
            // showFrontendAlert('warning','Enter valid Phone.');
            }
    }
    function optcount() {
        if(otptime > 0) {
            $('#mobileotpretrycontentdiv').html('Resend OTP in '+otptime+' seconds.');
            otptime = otptime - 1;
        } else {
            $('#mobileotpretrycontentdiv').html('<a href="javascript:create_acc()">Resend OTP</a>');
            clearInterval(timee);
            otptime = 10;
        }
        
    }
    
        var isPhoneShown = true;

        var input = document.querySelector("#phone-code");
        var iti = intlTelInput(input, {
            separateDialCode: true,
            preferredCountries: []
        });

        var countryCode = iti.getSelectedCountryData();


        input.addEventListener("countrychange", function() {
            var country = iti.getSelectedCountryData();
            $('input[name=country_code]').val(country.dialCode);
        });

        $(document).ready(function(){
            $('.email-form-group').hide();
        });

        function autoFillSeller(){
            $('#email').val('seller@example.com');
            $('#password').val('123456');
        }
        function autoFillCustomer(){
            $('#email').val('customer@example.com');
            $('#password').val('123456');
        }

        function toggleEmailPhone(el){
            if(isPhoneShown){
                $('.phone-form-group').hide();
                $('.email-form-group').show();
                isPhoneShown = false;
                $(el).html('Use Phone Instead');
            }
            else{
                $('.phone-form-group').show();
                $('.email-form-group').hide();
                isPhoneShown = true;
                $(el).html('Use Email Instead');
            }
        }
    </script>
    <script>
        $(document).ready(function () {
        	signUpbtn=$("#signUpbtn");
        	signUpbtn.click(function () {
        	   $("#loginform").show();
        	   $("#socialmedialogin").hide();
        	});
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/user_registration.blade.php ENDPATH**/ ?>