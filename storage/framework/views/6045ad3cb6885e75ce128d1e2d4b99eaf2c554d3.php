<?php $__env->startSection('content'); ?>

<?php
    if(isset($seller_id)) {
    $admin_user_id = $seller_id;
} else {
$admin_user_id = \App\User::where('user_type', 'admin')->first()->id;
}
?>
    <div class="panel">
    	<div class="panel-body">
    		<div class="invoice-masthead">
    			<div class="invoice-text">
    				<h3 class="h1 text-thin mar-no text-primary"><?php echo e(__('Order Details')); ?></h3>
    			</div>
    		</div>
            <div class="row">
                <?php
             // dd(Route::currentRouteAction()." ~ ".Route::currentRouteName() );
                    $otc_confirmation = count($order->orderDetails->where('seller_id', $admin_user_id)->where('delivery_status', 'confirmation_pending'));
                    $delivery_status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->delivery_status??'NA';
                    $payment_status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->payment_status??'NA';
                ?>
                <?php if($otc_confirmation == 0): ?>
                <div class="col-lg-3">
                    <h3>Payment Status: <?php if($payment_status == 'unpaid'): ?> <span style="color:red"><?php echo e(ucfirst($payment_status)); ?></span> <?php else: ?> <span style="color:green"><?php echo e(ucfirst($payment_status)); ?></span></h3><?php endif; ?>
                </div>
                    <?php if((Auth::user()->user_type == 'admin' || in_array('22', json_decode(Auth::user()->staff->role->permissions)))): ?>
                    <div class="col-lg-offset-3 col-lg-3">
                        <label for=update_payment_status""><?php echo e(__('Payment Status')); ?></label>
                        <select class="form-control demo-select2"  data-minimum-results-for-search="Infinity" id="update_payment_status">
                            <option value="paid" <?php if($payment_status == 'paid'): ?> selected <?php endif; ?>><?php echo e(__('Paid')); ?></option>
                            <option value="unpaid" <?php if($payment_status == 'unpaid'): ?> selected <?php endif; ?>><?php echo e(__('Unpaid')); ?></option>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <label for=update_delivery_status""><?php echo e(__('Delivery Status')); ?></label>
                        <select class="form-control demo-select2"  data-minimum-results-for-search="Infinity" id="update_delivery_status">
                            <option value="confirmation_pending" <?php if($delivery_status == 'confirmation_pending'): ?> selected <?php endif; ?>><?php echo e(__('Confirmation Pending')); ?></option>
                            <option value="hold" <?php if($delivery_status == 'hold'): ?> selected <?php endif; ?>><?php echo e(__('Hold')); ?></option>
                            <option value="pending" <?php if($delivery_status == 'pending'): ?> selected <?php endif; ?>><?php echo e(__('Pending')); ?></option>
                            <option value="on_review" <?php if($delivery_status == 'on_review'): ?> selected <?php endif; ?>><?php echo e(__('On review')); ?></option>
                            <option value="ready_to_ship" <?php if($delivery_status == 'ready_to_ship'): ?> selected <?php endif; ?>><?php echo e(__('Ready to Ship')); ?></option>
                            <option value="ready_to_delivery" <?php if($delivery_status == 'ready_to_delivery'): ?> selected <?php endif; ?>><?php echo e(__('Ready to Delivery')); ?></option>
                            <option value="shipped" <?php if($delivery_status == 'shipped'): ?> selected <?php endif; ?>><?php echo e(__('Shipped')); ?></option>
                            <option value="on_delivery" <?php if($delivery_status == 'on_delivery'): ?> selected <?php endif; ?>><?php echo e(__('On delivery')); ?></option>
                            <option value="delivered" <?php if($delivery_status == 'delivered'): ?> selected <?php endif; ?>><?php echo e(__('Delivered')); ?></option>
                            <option value="processing_refund" <?php if($delivery_status == 'processing_refund'): ?> selected <?php endif; ?>><?php echo e(__('Processing Refund')); ?></option>
                            <option value="refunded" <?php if($delivery_status == 'refunded'): ?> selected <?php endif; ?>><?php echo e(__('Refunded')); ?></option>
                            <option value="cancel" <?php if($delivery_status == 'cancel'): ?> selected <?php endif; ?>><?php echo e(__('Cancel')); ?></option>
                        </select>
                    </div>
                    <?php endif; ?>
                <?php else: ?>
                <div class="col-lg-3">
                    <h3>Payment Status: <?php if($payment_status == 'unpaid'): ?> <span style="color:red"><?php echo e(ucfirst($payment_status)); ?></span> <?php else: ?> <span style="color:green"><?php echo e(ucfirst($payment_status)); ?></span></h3><?php endif; ?>
                </div>
                <div clas="col-md-6">
                    <div class=" pull-right">
                    <?php if($payment_status == 'unpaid'): ?>
                        <button class="btn btn-primary" onclick="confirm_payment('paid')">Mark Paid</button>
                    <?php else: ?> 
                        <button class="btn btn-danger" onclick="confirm_payment('unpaid')">Mark Unpaid</button>
                    <?php endif; ?>
                    <button class="btn btn-primary" onclick="confirm_order('pending')">Confirm Order</button>
                    <button class="btn btn-primary" onclick="confirm_order('hold')">Mark Hold</button>
                        <?php if($order->prescription_file != null): ?><a  class="btn btn-primary" href="<?php echo e(asset('prescription/'.$order->prescription_file)); ?>" target="_blank"><?php echo e(__('Download Prescription')); ?></a> <?php endif; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <hr>
    		<div class="invoice-bill row">
    			<div class="col-sm-6 text-xs-center">
    				<address>
        				<?php echo e(json_decode($order->shipping_address)->name); ?><br>
						<?php if(!empty(json_decode($order->shipping_address)->email)): ?>
						<?php echo e(json_decode($order->shipping_address)->email); ?> <br>
						<?php endif; ?>
						<?php echo e(json_decode($order->shipping_address)->phone); ?><br>
						<?php echo e(json_decode($order->shipping_address)->address); ?>,
						<?php echo e(json_decode($order->shipping_address)->city); ?>,
					
						    <?php if(isset(json_decode($order->shipping_address)->state)): ?>
						       <?php echo e(json_decode($order->shipping_address)->state); ?>,
						    <?php else: ?>
						    NA
						    <?php endif; ?>
						<br>
						<?php echo e(json_decode($order->shipping_address)->postal_code); ?>

						  
                    </address>

                    <?php if($order->manual_payment && is_array(json_decode($order->manual_payment_data, true))): ?>
                        <br>
                        <strong class="text-main"><?php echo e(__('Payment Information')); ?></strong><br>
                        Name: <?php echo e(json_decode($order->manual_payment_data)->name); ?>, Amount: <?php echo e(single_price(json_decode($order->manual_payment_data)->amount)); ?>, TRX ID: <?php echo e(json_decode($order->manual_payment_data)->trx_id); ?>

                        <br>
                        <a href="<?php echo e(asset(json_decode($order->manual_payment_data)->photo)); ?>" target="_blank"><img src="<?php echo e(asset(json_decode($order->manual_payment_data)->photo)); ?>" alt="" height="100"></a>
                    <?php endif; ?>

    			</div>

    			<div class="col-sm-6 text-xs-center">
    				<table class="invoice-details">
    				<tbody>
    				<tr>
    					<td class="text-main text-bold">
    						<?php echo e(__('Order #')); ?>

    					</td>
    					<td class="text-right text-info text-bold">
    						<?php echo e($order->code); ?>

    					</td>
    				</tr>
    				<tr>
    					<td class="text-main text-bold">
    						<?php echo e(__('Order Status')); ?>

    					</td>
                        <?php
                            $status = $order->orderDetails->where('seller_id', $admin_user_id)->first()->delivery_status??'NA';
                        ?>
    					<td class="text-right">
                            <?php if($status == 'delivered'): ?>
                                <span class="badge badge-success"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></span>
                            <?php else: ?>
                                <span class="badge badge-info"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></span>
                            <?php endif; ?>
    					</td>
    				</tr>

    				<tr>
    					<td class="text-main text-bold">
    						<?php echo e(__('Order Date')); ?>

    					</td>
    					<td class="text-right">
    						<?php echo e(date('d-m-Y h:i A', $order->date)); ?> (UTC)
    					</td>
    				</tr>

                    <tr>
    					<td class="text-main text-bold">
    						<?php echo e(__('Total amount')); ?>

    					</td>
    					<td class="text-right">
    						<?php echo e(single_price($order->orderDetails->where('seller_id', $admin_user_id)->sum('price') + $order->orderDetails->where('seller_id', $admin_user_id)->sum('tax'))); ?>

    					</td>
    				</tr>
                    <tr>
    					<td class="text-main text-bold">
    						<?php echo e(__('Payment method')); ?>

    					</td>
    					<td class="text-right">
    						<?php echo e(ucfirst(str_replace('_', ' ', $order->payment_type))); ?>

    					</td>
    				</tr>
    				<?php if($order->payment_type == 'razorpay'): ?>
    				    <?php if($order->payment_details != null): ?>
    				    <tr>
        					<td class="text-main text-bold">
        						<?php echo e(__('Payment ID')); ?>

        					</td>
        					<td class="text-right">
        						<?php echo e(json_decode($order->payment_details)->id); ?>

        					</td>
    				    </tr>
						
    				    <?php endif; ?>
    				<?php endif; ?>
    				
    				<?php if($order->payment_type == 'paytm'): ?>
    				    <?php if($order->payment_details != null): ?>
    				    <tr>
        					<td class="text-main text-bold">
        						<?php echo e(__('Payment ID')); ?>

        					</td>
        					<td class="text-right">
        						<?php echo e(json_decode($order->payment_details)->TXNID); ?>

        					</td>
    				    </tr>
						
    				    <?php endif; ?>
    				<?php endif; ?>
    				
					<?php $total = $order->orderDetails->sum('price')+$order->orderDetails->sum('tax')?>
				
    				</tbody>
    				</table>
    			</div>
    		</div>

			<div class="row">
			<button type="button" class="btn btn-primary" id="showUpdate">Update order</button>
			</div>
			<div class="clearfix">&nbsp;&nbsp;</div>
			<div id="update_order" style="display:none;">
				<div class="row">
					<div class="col-md-2">
							<label>Search product</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" id="searchProduct" placeholder="Enter product name/sku-code">
					</div>
					<div class="col-md-2">
					
					<button type="button" class="btn btn-primary" id="searchProductDetails">Search Product</button>
					</div>	
				</div>
				
				<div class="clearfix">&nbsp;&nbsp;</div>
				<div class="row" id="show_product" style="display:none;">
					
					<div class="col-md-2">
						<div class="product-decs text-center">
							<p>Product Title:-<span id="productTitle"></span></p>
							
							<p>Product price:-<span id="producPrice"></span></p>
								
						</div>
					</div>
					<div class="col-md-2">
					<button type="button" class="btn btn-primary" id="addOrder">Add Product</button>
					</div>

					<div class="col-md-4">
						<span id="show_error" style="color:red;"></span>
					</div>
				</div>
				
			</div>
			<div class="col-md-4">
						<span id="show_error1" style="color:red;margin-left: 173px;"></span>
					</div>
			<input type="hidden" id="currentOrderId" value="<?php echo e($order->id); ?>">
			


    		<hr class="new-section-sm bord-no">
    		<form action="<?php echo e(route('orders.update',['id' => $order->id])); ?>" method="post">
    		    <?php echo csrf_field(); ?>
    		<div class="row">
    			<div class="col-lg-12 table-responsive">
    				<table class="table table-bordered invoice-summary">
        				<thead>
            				<tr class="bg-trans-dark">
                                <th class="min-col text-center">#</th>
                                <th width="10%">
            						<?php echo e(__('Photo')); ?>

            					</th>
            					<th class="text-uppercase">
            						<?php echo e(__('Description')); ?>

            					</th>
            					<th class="min-col text-center text-uppercase">
            						<?php echo e(__('Qty')); ?> <br />
            						A
            					</th>
            					<th class="min-col text-center text-uppercase">
            						<?php echo e(__('Unit Price')); ?><br /> B
            					</th>
            					<th class="min-col text-center text-uppercase">
            						<?php echo e(__('Unit Discount')); ?><br /> C
            					</th>
            					<th class="min-col text-center text-uppercase">
            						<?php echo e(__('Effective Unit Price')); ?><br /> 
            					</th>
            					<th class="min-col text-center text-uppercase">
            						<?php echo e(__('Total Price')); ?><br /> D = A * B
            					</th>
            					<th class="min-col text-center text-uppercase">
            						<?php echo e(__('Total Discount')); ?> <br /> F = A * C
            					</th>
            					<th class="min-col text-right text-uppercase">
            						<?php echo e(__('Total')); ?> <br /> E = D - F
            					</th>
            					<th class="min-col text-right text-uppercase">
            						<?php echo e(__('Exclusive ')); ?> 
            					</th>
            					<th class="min-col text-right text-uppercase">
            						<?php echo e(__('Inclusive  ')); ?> 
            					</th>
								
            				</tr>
        				</thead>
        				<tbody>
                            <?php $__currentLoopData = $order->orderDetails->where('seller_id', $admin_user_id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $orderDetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <input type="hidden" name="order_detail[<?php echo e($orderDetail->id); ?>]" value="<?php echo e($orderDetail->id); ?>">
                                    <td class="text-center"><?php echo e($key+1); ?></td>
                                    <td>
                                        <?php if($orderDetail->product != null): ?>
                    						<a href="<?php echo e(route('product', $orderDetail->product->slug)); ?>" target="_blank"><img height="50" src="<?php echo e(asset($orderDetail->product->thumbnail_img)); ?>"></a>
                                        <?php else: ?>
                                            <strong><?php echo e(__('N/A')); ?></strong>
                                        <?php endif; ?>
                                    </td>
                					<td>
                					    <?php
                					    $return_request=\App\ReturnProduct::where('order_id',$orderDetail->order_id)->where('product_id',$orderDetail->product_id)->first();
                					   // dd($return_request);
                					    ?>
                                        <?php if($orderDetail->product != null): ?>
                    						<strong><a href="<?php echo e(route('product', $orderDetail->product->slug)); ?>" target="_blank"><?php echo e($orderDetail->product->name); ?></a> <?php if($return_request!=null): ?> <span style="color:red"><?php echo e(__('Return Requested')); ?></span> <?php endif; ?></strong>
                                        <?php else: ?>
                                            <strong><?php echo e(__('Product Unavailable')); ?></strong>
                                        <?php endif; ?>
                					</td>
                					<td class="text-center">
                						<input type="text" name="quantity[<?php echo e($orderDetail->id); ?>]" value="<?php echo e($orderDetail->quantity); ?>" onkeyup="checkthis(<?php echo e($orderDetail->id); ?>, this)" class="form-control">
                					</td>
            					    <td class="text-center del_<?php echo e($orderDetail->id); ?>" colspan="5" style="display:none;">
            					        <em style="color:red">This product will remove on update.</em>
            					    </td>
                					<td class="text-center show_<?php echo e($orderDetail->id); ?>">
                						<input type="text" name="unit_price[<?php echo e($orderDetail->id); ?>]" value="<?php echo e($orderDetail->price/$orderDetail->quantity + $orderDetail->discount/$orderDetail->quantity); ?>" class="form-control">
                					</td>
                					<td class="text-center show_<?php echo e($orderDetail->id); ?>"><input type="text" name="discount[<?php echo e($orderDetail->id); ?>]" value="<?php echo e($orderDetail->discount/$orderDetail->quantity); ?>" class="form-control">
                					</td>
                					<td class="text-center show_<?php echo e($orderDetail->id); ?>"><?php echo e(single_price($orderDetail->price/$orderDetail->quantity)); ?></td>
                					<td class="text-center show_<?php echo e($orderDetail->id); ?>">
                						<?php echo e(single_price($orderDetail->price + $orderDetail->discount)); ?>

                					</td>
                					<td class="text-center show_<?php echo e($orderDetail->id); ?>" <?php if($orderDetail->discount > 0): ?> style="color:red" <?php endif; ?>> <?php echo e(single_price( $orderDetail->discount)); ?>

                					</td>
                                    <td class="text-center show_<?php echo e($orderDetail->id); ?>">
                						<?php echo e(single_price($orderDetail->price)); ?>

                					</td>
                					<?php
                               $getPrice = \App\OrderDetail::where('order_id', $order->id)->get(); 
                               if(isset($getPrice[0]->inclusive_price)){
                                   $comissionselleralde = single_price($order->seller_commission);
                                }else{
                                    $comissionselleralde = 0;
                                }
                               
                                if(isset($getPrice[0]->exclusive_price)){
                                   $comissionselleraldeex = single_price($order->seller_commission);
                                }else{
                                    $comissionselleraldeex = 0;
                                }
                            ?>
                					
                					 <td class="text-center show_<?php echo e($orderDetail->id); ?>">
                					       <?php echo e($comissionselleraldeex); ?>

                					</td>
                					
                					 <td class="text-center show_<?php echo e($orderDetail->id); ?>">
                					  <?php echo e($comissionselleralde); ?>

                					</td>
									
                				</tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        				</tbody>
    				</table>
    			<script>
    			function checkthis(id, ele) {
    			    v = $(ele).val();
    			    if(v == 0) {
    			        $('.show_'+id).hide();
    			        $('.del_'+id).show();
    			    } else {
    			        $('.show_'+id).show();
    			        $('.del_'+id).hide();
    			    }
    			}
    			</script>
    		<button class="btn btn-primary">Update Invoice Value</button>
    			</div>
    		</div>
    		</form>
    		<div class="clearfix">
    			<table class="table invoice-total">
    			<tbody>
    			<tr>
    				<td>
    					<strong><?php echo e(__('Sub Total')); ?> :</strong>
    				</td>
    				<td>
    					<?php echo e(single_price($order->orderDetails->where('seller_id', $admin_user_id)->sum('price'))); ?>

    				</td>
    			</tr>
    			<tr>
    				<td>
    					<strong><?php echo e(__('Tax')); ?> :</strong>
    				</td>
    				<td>
    					<?php echo e(single_price($order->orderDetails->where('seller_id', $admin_user_id)->sum('tax'))); ?>

    				</td>
    			</tr>
                <tr>
    				<td>
    					<strong><?php echo e(__('Shipping')); ?> :</strong>
    				</td>
    				<td>
    					<?php echo e(single_price($order->orderDetails->where('seller_id', $admin_user_id)->sum('shipping_cost'))); ?>

    				</td>
    			</tr>
    			
    			<?php if($order->cart_shipping > 0): ?>
    			<tr>
    				<td>
    					<strong><?php echo e(__('Delivery Charges')); ?> :</strong>
    				</td>
    				<td class="text-bold h4">
    					<?php echo e(single_price($order->cart_shipping)); ?>

    				</td>
    			</tr>
    			<?php endif; ?>
				<tr>
    				<td>
    					<strong><?php echo e(__('TOTAL')); ?> :</strong>
    				</td>
    				<td class="text-bold h4">
    					<?php echo e(single_price($order->grand_total)); ?>

    				</td>
    			</tr>
    			</tbody>
    			</table>
    		</div>
    		 <?php if((Auth::user()->user_type == 'admin' || in_array('22', json_decode(Auth::user()->staff->role->permissions)))): ?>
    		<div class="col-md-12">
    		    <form action="<?php echo e(route('orders.deliveryupdate')); ?>" method="post">
    		        <input name="order_id" type="hidden" value="<?php echo e($order->id); ?>">
        	            <?php echo csrf_field(); ?>
    		       <div class="col-md-1 text-right">
    		           Delivery Charges &nbsp; &nbsp;
    		       </div>
    		       <div class="col-md-3">
    		           <input type="text" class="form-control" name="delivery_charges" value="<?php echo e($order->cart_shipping); ?>" >
    		       </div>
		           <div class="col-md-2">
		               <button type="submit" class="btn btn-primary">Update</button>
		           </div>
		       </form>
    		</div>
    		<?php endif; ?>
    		<div class="col-md-12">
    		    <br />
    		  Total Order Value : <?php echo e(single_price($order->grand_total + $order->coupon_discount + $order->wallet_credit)); ?> &nbsp; &nbsp; &nbsp; &nbsp;
               Discount :  <?php echo e(single_price($order->coupon_discount)); ?> &nbsp; &nbsp; &nbsp; &nbsp;
               Use Wallet Credit :  <?php echo e(single_price($order->wallet_credit)); ?> &nbsp; &nbsp; &nbsp; &nbsp;
               Total Amount : <?php echo e(single_price($order->grand_total)); ?>

    		</div>
    		<div class="text-right no-print">
    			<a href="<?php echo e(route('customer.invoice.download', $order->id)); ?>" class="btn btn-default"><i class="demo-pli-printer icon-lg"></i></a>
    		</div>
    	</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        $('#update_delivery_status').on('change', function(){
            var order_id = <?php echo e($order->id); ?>;
            var status = $('#update_delivery_status').val();
            
            $.post('<?php echo e(route('orders.update_delivery_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>',order_id:order_id,status:status, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
                showAlert('success', 'Delivery status has been updated');
            });
        
        });
        function confirm_payment(status) {
            var order_id = <?php echo e($order->id); ?>;
            var setFlash = 'Payment status has been updated';
             $.post('<?php echo e(route('orders.update_payment_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', setFlash:setFlash, order_id:order_id,status:status, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
                location.reload();
            });
        }

        function confirm_order(status) {
            var order_id = <?php echo e($order->id); ?>;
            var setFlash = 'Order status has been updated';
            $.post('<?php echo e(route('orders.update_delivery_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', setFlash:setFlash, order_id:order_id,status:status, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
                  location.reload();
                });
        }

        $('#update_payment_status').on('change', function(){
            var order_id = <?php echo e($order->id); ?>;
            var status = $('#update_payment_status').val();
            $.post('<?php echo e(route('orders.update_payment_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>',order_id:order_id,status:status, <?php if(isset($seller_id)): ?> seller_id : <?php echo e($seller_id); ?> <?php endif; ?>}, function(data){
                showAlert('success', 'Payment status has been updated');
            });
		});
		

		//Select Category
		$(function(){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
		});

		$('#showUpdate').on('click',function(){
			
			$('#update_order').show();
		});

		$("#searchProductDetails").on('click',function(){

		var productId = $('#searchProduct').val(); 
		var orderId =  $('#currentOrderId').val();
		if(productId == ''){
			
			$('#show_error1').html('please Enter product id/Sku-code');
			return false;
		}
		$('#show_error1').html('');
			$.ajax({
				url:"<?php echo e(route('ajax.searchProduct')); ?>",
				type: 'POST',
				dataType:'json',
				data:{'productId':productId,'order_id':orderId},
				success: function(data) {
					console.log(data.product_name);
					if(data.status == 200){
						$('#show_product').show();
						$('#productTitle').html(data.product_name);
						$('#productDesc').html(data.description);
						$('#producPrice').html(data.price);
						$('#producphotos').html(data.photos);
					}else{
						$('#show_error1').html(data.product_name);
					}
					
				}
			});	
		});






		$("#addOrder").on('click',function(){

			var productId = $('#searchProduct').val(); 
			var orderId =  $('#currentOrderId').val();
			if(productId == ''){
				
				$('#show_error').html('please Enter product id/Sku-code');
				return false;
			}
			$('#show_error').html('');
				$.ajax({
					url:"<?php echo e(route('ajax.updatedOrder')); ?>",
					type: 'POST',
					dataType:'json',
					data:{'productId':productId,'order_id':orderId},
					success: function(data) {
						console.log(data.product_name);
						
						$('#show_error').html('product added sucessfully');
						$('#show_error').css("color", "green");
						setTimeout(function(){ 
							location.reload();
							}, 1000);
					}
				});	
		});
		
		

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/orders/show.blade.php ENDPATH**/ ?>