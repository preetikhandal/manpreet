<?php $__env->startSection('content'); ?>
<div class="container forget-password">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<img src="<?php echo e(asset('img/fogtpwKey.png')); ?>" alt="key" border="0">
						<h2 class="text-center"><?php echo e(__('Forgot Password?')); ?></h2>
						<p><?php echo e(__('Enter your email address or Phone no to recover your password.')); ?></p>
						<form id="register-form" role="form" autocomplete="off" class="form" method="POST" action="<?php echo e(route('password.email')); ?>">
						<?php echo csrf_field(); ?>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
									<?php if(\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated): ?>
										<input id="text" type="text" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required placeholder="Email or Phone">
									<?php else: ?>
										<input type="text" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('email')); ?>" placeholder="<?php echo e(__('Email or Phone')); ?>" name="email" required>
									<?php endif; ?>

								
								</div>
									<?php if($errors->has('email')): ?>
										<span class="invalid-feedback text-danger" role="alert">
											<strong><?php echo e($errors->first('email')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
							<div class="form-group">
								<button class="btn btn-lg btn-primary btn-block btnForget" type="submit">
									<?php echo e(__('Send Request')); ?>

								</button>
							</div>
						</form>
						 <div class="pad-top">
							<a href="<?php echo e(route('user.login')); ?>" class="btn-link text-bold text-main"><?php echo e(__('Back to Login')); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.blank', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/auth/passwords/email.blade.php ENDPATH**/ ?>