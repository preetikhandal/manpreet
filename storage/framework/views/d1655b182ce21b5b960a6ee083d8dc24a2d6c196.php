<?php if(count($combinations[0]) > 0): ?>
	<table class="table table-bordered">
		<thead>
			<tr>
				<td class="text-center">
					<label for="" class="control-label"><?php echo e(__('Variant')); ?></label>
				</td>
				<td class="text-center">
					<label for="" class="control-label"><?php echo e(__('Products')); ?></label>
				</td>
				<td class="text-center">
					<label for="" class="control-label"><?php echo e(__('Show/Hide')); ?></label>
				</td>
			</tr>
		</thead>
		<tbody>
<?php $__currentLoopData = $combinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $combination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php
		$str = implode("-",$combination);
        $variation_show_hide = 1;
	?>
	<?php if(strlen($str) > 0): ?>
		<tr>
			<td>
				<label for="" class="control-label"><?php echo e($str); ?></label>
                <input type="hidden" name="variation[]" value="<?php echo e($str); ?>">
			</td>
			<td>
                <select name="product_variation_select[<?php echo e($str); ?>]" class="form-control variationselect2">
                    <option value="0" <?php if(isset($product_variation[$str])): ?> <?php if($product_variation[$str] == 0): ?> selecte
                <?php endif; ?> <?php endif; ?>>No Product Selected</option>
                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <option value="<?php echo e($p->id); ?>" <?php if(isset($product_variation[$str])): ?> <?php if($product_variation[$str] == $p->id): ?> selected <?php
                        $variation_show_hide = $p->variation_show_hide; 
                ?> 
                <?php endif; ?> <?php endif; ?>><?php echo e($p->name); ?> - (SKU: <?php echo e($p->product_id); ?>)</option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
			</td>
			<td>
                <select name="product_variation_show[<?php echo e($str); ?>]" class="form-control"><option value="1" <?php if($variation_show_hide == 1): ?> selected <?php endif; ?>>Show</option><option value="0"  <?php if($variation_show_hide == 0): ?> selected <?php endif; ?>>Hide</option></select>
			</td>
		</tr>
	<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <script>
                $('.variationselect2').select2();
                </script>

	</tbody>
</table>
<?php endif; ?>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/partials/product_variation_edit.blade.php ENDPATH**/ ?>