<?php $__env->startSection('content'); ?>
<?php

 $stopProcessing = false;
 $lowStock = false;
 $outofstock = false;
    $all_product_ids = array();
    foreach (Session::get('cart') as $key => $cartItem){
        array_push($all_product_ids, $cartItem['id']);
        $product = \App\Product::find($cartItem['id']);
        $qty = 0;
        if($product->variant_product){
            foreach ($product->stocks as $key => $stock) {
                $qty += $stock->qty;
            }
        } else {
            if($product->current_stock > -1) {
            $qty = $product->current_stock;
            } else {
            $qty = 100;
            }
        }
        if($qty == 0) {
            $stopProcessing = true;
            $outofstock = true;
        } elseif($cartItem['quantity'] > $qty) {
		    $stopProcessing = true;
		    $lowStock = true;
        }
    }
    
$prescriptioncategory = env('PRESCRIPTION_CATEGORY');
$prescriptioncategory = explode(",",$prescriptioncategory);
$PrescriptionRequired = \App\Product::whereIn('id', $all_product_ids)->whereIn('category_id', $prescriptioncategory)->select('category_id')->count();
?>
    <div id="page-content">
        <section class="processing slice-xs sct-color-2 border-bottom" <?php if($mobileapp == 1): ?> style="margin-top:80px" <?php endif; ?>>
            <div class="container-fluid">
                <div class="row cols-delimited justify-content-center">
                    <div class="process">
                        <div class="process-row">
                            <div class="process-step">
                                <a href="<?php echo e(route('cart')); ?>" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-2x text-white"></i></a>
                                <p>My Cart</p>
                            </div>
                            <div class="process-step">
                                <a href="<?php echo e(route('checkout.shipping_info')); ?>" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-map-o fa-2x text-white"></i></a>
                                <p>Shipping Info</p>
                            </div>
                            <div class="process-step">
                                <a href="javascript:;" class="btn bg-blue btn-circle" disabled="disabled"><i class="fa fa-credit-card fa-2x text-white"></i></a>
                                <p>Payment</p>
                            </div> 
                             <div class="process-step">
                                <a href="javascript:;" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-check-circle-o fa-2x"></i></a>
                                <p>Confirmation</p>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-3 gry-bg">
            <div class="container-fluid">
                    <?php if($stopProcessing): ?>
                        <?php if($outofstock): ?>
                        <div class="row outofstock">
            	          <div class="col-xl-12">
            	              <div class="alert alert-danger">
            	                  Some of items in your cart is not in stock, Remove that item(s) to proceed further. <a href="<?php echo e(route('cart')); ?>">Click Here</a> to go to cart.
            	              </div>
            	          </div>
                	     </div>
                	     <?php else: ?>
                	      <div class="row lowstock">
                	          <div class="col-xl-12">
                	              <div class="alert alert-danger">
                	                  Required stock not available, Descrese the quantity to proceed further.  <a href="<?php echo e(route('cart')); ?>">Click Here</a> to go to cart.
                	              </div>
                	          </div>
                	     </div>
                	     <?php endif; ?>
            	     <?php endif; ?>
                <div class="row cols-xs-space cols-sm-space cols-md-space">
                    <div class="col-lg-8">
                        <form action="<?php echo e(route('payment.checkout')); ?>" class="form-default" data-toggle="validator" role="form" method="POST" id="checkout-form" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php if($PrescriptionRequired > 0): ?> 
                            <div class="card">
                                <div class="card-title px-4 py-3">
                                    <h3 class="heading heading-5 strong-500">
                                        <?php echo e(__('Prescription Required for this order')); ?>

                                    </h3>
                                </div>
                                <div class="card-body text-center">
                                    <div class="row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="file" id="prescriptionFile" class="form-control" name="prescriptionFile" accept="image/x-png,image/jpeg,image/jpg,.pdf" required>
                                                    <span class="text-danger py-2 allowedtype">* Jpg/png/pdf files only</span>
                                                    <span id="errorMsgnofile" class="text-danger" style="display:none;">Kindly select the prescription</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="card">
                                <div class="card-title px-4 py-2 bg-blue">
                                    <h3 class="heading heading-5 strong-500 text-white">
                                        <?php echo e(__('Select a payment option')); ?>

                                    </h3>
                                </div>
                                <div class="card-body text-center">
                                    <div class="row justify-content-center">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-12 text-left">
                                                <span id="errorMsgnopaymentop" class="text-danger font-weight-bold" style="display:none;">Please select the Payment Option</span>
                                               </div>
                                                <?php if(\App\BusinessSetting::where('type', 'razorpay')->first()->value == 1): ?>
                                                <?php
                                                $shipData = Session::get('shippingData');

                                                ?>
                                                
                                                <div class="col-12 text-left">
                                                    <label class="mb-2" style="border:1px solid #dcdcdc; padding:10px;border-radius:5px">
                                                        
                                                <?php if(isset($shipData['payoption'])): ?>   
                                                    <?php if($shipData['payoption'] == 'DBC'): ?>
                                                    
                                                        <input type="radio" id="" checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','DBC')">
                                                        <?php else: ?>
                                                         <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','DBC')">
                                                    <?php endif; ?>
                                               
                                                <?php else: ?>
                                                    <input type="radio" id="" checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','DBC')">
                                                <?php endif; ?>
                                               
                                                <span>
                                                           &nbsp; Debit  Card/Credit Card
                                                        </span>
                                                        <img src="<?php echo e(asset('frontend/images/icons/cards/visa.png')); ?>" class="img-fluid ml-2 mr-2">
                                                        <img src="<?php echo e(asset('frontend/images/icons/cards/mastercard.png')); ?>" class="img-fluid mr-2">
                                                        <img src="<?php echo e(asset('frontend/images/icons/cards/maestro.png')); ?>" class="img-fluid mr-2">
                                                        <img src="<?php echo e(asset('frontend/images/icons/cards/rupay.png')); ?>" class="img-fluid mr-2">
                                                    </label>
                                                </div>
                                                
                                                <div class="col-12 text-left">
                                                    <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px">
                                                <?php if(isset($shipData['payoption'])): ?>
                                                    <?php if($shipData['payoption'] == 'NB'): ?>
                                                        <input type="radio" id="" checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','NB')">
                                                    <?php else: ?>
                                                     <input type="radio" id="" name="payment_option" value="razorpay" onclick="paymentMode('razorpay','NB')">
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                 <input type="radio" id="" name="payment_option" value="razorpay" onclick="paymentMode('razorpay','NB')">
                                                <?php endif; ?>
                                                   
                                                    <span>
                                                          &nbsp; Net Banking
                                                        </span>
                                                    </label>
                                                </div>
                                                <div class="col-12 text-left">
                                                    <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px">
                                                <?php if(isset($shipData['payoption'])): ?> 
                                                    <?php if($shipData['payoption'] == 'UPI'): ?>
                                                        <input type="radio" id=""  checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','UPI')">
                                                    <?php else: ?>
                                                     <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','UPI')">
                                                    <?php endif; ?>
                                               <?php else: ?>
                                                    <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','UPI')">
                                                <?php endif; ?>
                                                <span>
                                                          &nbsp; UPI
                                                        </span>
                                                    </label>
                                                </div>
                                                <div class="col-12 text-left">
                                                    <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px">
                                                <?php if(isset($shipData['payoption'])): ?>
                                                    <?php if($shipData['payoption'] == 'WA'): ?>
                                                        <input type="radio" id="" checked name="payment_option" value="razorpay" onclick="paymentMode('razorpay','WA')">
                                                    <?php else: ?>
                                                     <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','WA')">
                                                   
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <input type="radio" id=""  name="payment_option" value="razorpay" onclick="paymentMode('razorpay','WA')">
                                                <?php endif; ?>
                                                    <span>
                                                           &nbsp; Wallets : Airtel Money, FreeCharge, PayZapp, MobiKwik, JioMoney
                                                        </span>
                                                    </label>
                                                </div>
                                                <?php endif; ?>
                                                
                                                <?php if(\App\BusinessSetting::where('type', 'paytm')->first()->value == 1): ?>
                                                <div class="col-12 text-left">
                                                    <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px">
                                                <?php if(isset($shipData['payoption'])): ?>
                                                    <?php if($shipData['payoption'] == 'PayTM'): ?>
                                                        <input type="radio" id="" checked name="payment_option" value="paytm" onclick="paymentMode('razorpay','PayTM')">
                                                    <?php else: ?>
                                                     <input type="radio" id="" name="payment_option" value="paytm" onclick="paymentMode('razorpay','PayTM')">
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                 <input type="radio" id="" name="payment_option" value="paytm" onclick="paymentMode('razorpay','PayTM')">
                                                <?php endif; ?>
                                                   
                                                    <span>
                                                          &nbsp; PayTM :: Wallet, UPI, NetBanking, Cards
                                                        </span>
                                                    </label>
                                                </div>
                                                <?php endif; ?>
                                                
                                                
                                                <?php if(\App\BusinessSetting::where('type', 'cash_payment')->first()->value == 1): ?>
                                                    <?php
                                                        $digital = 0;
                                                        foreach(Session::get('cart') as $cartItem){
                                                            if($cartItem['digital'] == 1){
                                                                $digital = 1;
                                                            }
                                                        }
                                                    ?>
                                                    <?php if($digital != 1): ?>
                                                    <div class="col-12 text-left">
                                                        <label class="mb-2"  style="border:1px solid #dcdcdc;padding:10px;border-radius:5px;">
                                                     <?php if(isset($shipData['payoption'])): ?>
                                                        <?php if($shipData['payoption'] == 'COD'): ?>
                                                            <input type="radio" id="" checked name="payment_option" value="cash_on_delivery" onclick="paymentMode('COD','COD')">
                                                        <?php else: ?>
                                                         <input type="radio" id="" name="payment_option" value="cash_on_delivery" onclick="paymentMode('COD','COD')">
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                    <input type="radio" id="" name="payment_option" value="cash_on_delivery" onclick="paymentMode('COD','COD')">
                                                    <?php endif; ?>
                                                        <span>
                                                               &nbsp; Cash On Delivery
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                
                                                <?php if(Auth::check()): ?>
                                                    <?php if(\App\Addon::where('unique_identifier', 'offline_payment')->first() != null && \App\Addon::where('unique_identifier', 'offline_payment')->first()->activated): ?>
                                                        <?php $__currentLoopData = \App\ManualPaymentMethod::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $method): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                          <div class="col-6">
                                                              <label class="payment_option mb-4" data-toggle="tooltip" data-title="<?php echo e($method->heading); ?>">
                                                                  <input type="radio" id="" name="payment_option" value="<?php echo e($method->heading); ?>">
                                                                  <span>
                                                                      <img loading="lazy"  src="<?php echo e(asset($method->photo)); ?>" class="img-fluid">
                                                                  </span>
                                                              </label>
                                                          </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if(Auth::check() && \App\BusinessSetting::where('type', 'wallet_system')->first()->value == 1): ?>
                                       
                                        
                                    <?php endif; ?>
                                </div>
                            </div>
                            
                            
                            <div class="row align-items-center pt-4 d-none d-lg-block d-lg-flex">
                                <div class="col-6">
                                    <a href="<?php echo e(route('home')); ?>" class="btn btn-styled btn-base-1" style="background:#131921">
                                        <i class="fa fa-reply" aria-hidden="true"></i>
                                        <?php echo e(__('Return to shop')); ?>

                                    </a>
                                </div>
                                <div class="col-6 text-right">
                                    <?php if($stopProcessing): ?>
                                    <button type="button" class="btn btn-styled btn-base-1 bg-blue" disabled><?php echo e(__('Complete Order')); ?></button>
                                    <?php else: ?>
                                    <button type="button" onclick="submitOrder(this)" class="btn btn-styled btn-base-1 bg-blue paymnybtn"><?php echo e(__('Complete Order')); ?></button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-4 ml-lg-auto">
                        <?php echo $__env->make('frontend.partials.cart_summary', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                </div>
                 <div class="row align-items-center pt-4 d-block d-lg-none">
                        <div class="col-md-12 text-center">
                            <?php if($stopProcessing): ?>
                            <button type="button" class="btn btn-styled btn-block btn-base-1 bg-blue" disabled><?php echo e(__('Complete Order')); ?></button>
                            <?php else: ?>        
                            <button type="button"  onclick="submitOrder(this)" class="btn btn-styled btn-block btn-base-1 bg-blue paymnybtn"><?php echo e(__('Complete Order')); ?></button>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-12 text-center pt-2">
                            <a href="<?php echo e(route('home')); ?>">
                                <i class="fa fa-reply" aria-hidden="true"></i>
                                <?php echo e(__('Return to shop')); ?>

                            </a>
                        </div>
                  </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="wallet_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <!-- <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5"><?php echo e(__('Recharge Wallet')); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> -->
                <form class="" action="<?php echo e(route('wallet.recharge')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body gry-bg px-3 pt-3">
                        <div class="row">
                            <div class="col-md-2">
                                <label><?php echo e(__('Amount')); ?> <span class="required-star">*</span></label>
                            </div>
                            <div class="col-md-10">
                                <input type="number" class="form-control mb-3" name="amount" placeholder="<?php echo e(__('Amount')); ?>" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label><?php echo e(__('Payment Method')); ?></label>
                            </div>
                            <div class="col-md-10">
                                <div class="mb-3" style="border:1px solid #e6e6e6">
                                    <select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="payment_option">
                                        <?php if(\App\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1): ?>
                                            <option value="paypal"><?php echo e(__('Paypal')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'stripe_payment')->first()->value == 1): ?>
                                            <option value="stripe"><?php echo e(__('Stripe')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'sslcommerz_payment')->first()->value == 1): ?>
                                            <option value="sslcommerz"><?php echo e(__('SSLCommerz')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'instamojo_payment')->first()->value == 1): ?>
                                            <option value="instamojo"><?php echo e(__('Instamojo')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'paystack')->first()->value == 1): ?>
                                            <option value="paystack"><?php echo e(__('Paystack')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'voguepay')->first()->value == 1): ?>
                                            <option value="voguepay"><?php echo e(__('VoguePay')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'razorpay')->first()->value == 1): ?>
                                            <option value="razorpay"><?php echo e(__('Razorpay')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\Addon::where('unique_identifier', 'paytm')->first() != null && \App\Addon::where('unique_identifier', 'paytm')->first()->activated): ?>
                                            <option value="paytm"><?php echo e(__('Paytm')); ?></option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-base-1"><?php echo e(__('Confirm')); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php if(Session::has('wallet_credit')): ?>
    <form id="remove_wallet_credit" action="<?php echo e(route('checkout.remove_wallet_credit')); ?>" method="post">
    <?php else: ?>
    <form id="apply_wallet_credit" action="<?php echo e(route('checkout.apply_wallet_credit')); ?>" method="post">
    <?php endif; ?>
    <?php echo csrf_field(); ?>
    </form>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        function use_wallet(ele){
            var html = $(ele).html();
            $(ele).prop('disabled', true);
            $(ele).html('<i class="fa fa-spin fa-spinner"></i> Loading...');
            
            $('input[name=payment_option]').val('wallet');
            $("input[name=payment_option][value='wallet']").prop("checked",true);
            <?php if($PrescriptionRequired > 0): ?> 
                if($('#prescriptionFile').val() == "") {
                    $('#errorMsgnofile').slideDown(100);
                    $("#prescriptionFile").focus(); 
                    $(ele).html(html);
                    $('html, body').animate({
                         scrollTop: ($('#prescriptionFile').offset().top - 300)
                    }, 2000);
                    $(ele).prop('disabled', false);
                }
                else {
                    $('#errorMsgnofile').slideUp(100);
                    $(ele).prop('disabled', true);
                    $(ele).html('<i class="fa fa-spin fa-spinner"></i>');
                    $('#checkout-form').submit();
                }
			<?php else: ?>
			    $(ele).prop('disabled', true);
                $(ele).html('<i class="fa fa-spin fa-spinner"></i> Loading...');
                $('#checkout-form').submit();
            <?php endif; ?>
        }
        function submitOrder(el){
            if($('input[name=payment_option]:checked').val()==undefined ){
                $('#errorMsgnopaymentop').slideDown(100);
                
                $('html, body').animate({
                     scrollTop: ($('#errorMsgnopaymentop').offset().top - 300)
                }, 2000);
                return false;
            }
            <?php if($PrescriptionRequired > 0): ?> 
                if($('#prescriptionFile').val() == "") {
                    $('#errorMsgnofile').slideDown(100);
                    $("#prescriptionFile").focus(); 
                    $(ele).html(html);
                    $('html, body').animate({
                         scrollTop: ($('#prescriptionFile').offset().top - 300)
                    }, 2000);
                    $(ele).prop('disabled', false);
                } else {
                    $('#errorMsgnofile').slideUp(100);
    			    $('.paymnybtn').prop('disabled', true);
                    $('.paymnybtn').html('<i class="fa fa-spin fa-spinner"></i> Loading...');
                    $('#checkout-form').submit();
                }
			<?php else: ?>
			    $('.paymnybtn').prop('disabled', true);
                $('.paymnybtn').html('<i class="fa fa-spin fa-spinner"></i> Loading...');
                $('#checkout-form').submit();
            <?php endif; ?>
        }
    </script>
    <script type="text/javascript">
          
      
        function show_wallet_modal(){
            $('#wallet_modal').modal('show');
        }

        function show_make_wallet_recharge_modal(){
            $.post('<?php echo e(route('offline_wallet_recharge_modal')); ?>', {_token:'<?php echo e(csrf_token()); ?>'}, function(data){
                $('#offline_wallet_recharge_modal_body').html(data);
                $('#offline_wallet_recharge_modal').modal('show');
            });
        }

        //Change shipping price according to payment mode
       

        function paymentMode($apymnetMode,$payoption){
         
            $totalAmount = $('#totalPrice').val();
            
                    $.ajax({
    				url:"<?php echo e(route('ajax.updatePaymentOption')); ?>",
    				type: 'POST',
    				dataType:'json',
    				data:{'paymentOption':$apymnetMode,'totalAmount':$totalAmount,'payoption':$payoption},
    				success: function(data) {
                        if(data){
                            location.reload();
                        }
    					
    				}
    			});
            
            
           
        }
    </script>
<script>
    $('INPUT[type="file"]').change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        ext= ext.toLowerCase();
		var FileSize = this.files[0].size / 1024 / 1024; // in MB
		if (FileSize > 4) {
		    $(".allowedtype").hide();
			$("#errorMsgnofile").text('Max file size: 4MB');
			this.value = '';
		}
		else{
		    $(".allowedtype").hide();
		    $("#errorMsgnofile").text('');
		}
        switch (ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'pdf':
                $('.paymnybtn').attr('disabled', false);
                break;
            default:
                $("#errorMsgnofile").html("This is not an allowed file type.");
                $("#errorMsgnofile").slideDown();
                $(".allowedtype").show();
                $('html, body').animate({
                     scrollTop: ($('#prescriptionFile').offset().top - 300)
                }, 2000);
                this.value = '';
        }
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/payment_select.blade.php ENDPATH**/ ?>