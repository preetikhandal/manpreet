<?php $__env->startSection('content'); ?>
<!-- Seller Style -->
<link type="text/css" href="<?php echo e(asset('frontend/css/seller.css')); ?>" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" <?php if($mobileapp == 1): ?> style="margin-top:75px" <?php endif; ?>>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    <?php if(Auth::user()->user_type == 'seller'): ?>
                        <?php echo $__env->make('frontend.inc.seller_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php elseif(Auth::user()->user_type == 'customer'): ?>
                        <?php echo $__env->make('frontend.inc.customer_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 p-0 text-white">
                                        <?php echo e(__('Wishlist')); ?>

                                    </h2>
                                </div>
                            </div>
                        </div>
                    <?php if(count($wishlists)!=0): ?>
                        <!-- Wishlist items -->
						<?php $product_id=""; ?>
					<form id="option-choice-form">
						<?php echo csrf_field(); ?>
						<input type="hidden" name="id" id="id">
                        <div class="row shop-default-wrapper shop-cards-wrapper shop-tech-wrapper mt-4">
                            <?php $__currentLoopData = $wishlists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $wishlist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($wishlist->product != null): ?>
                                    <div class="col-xl-6 col-12" id="wishlist_<?php echo e($wishlist->id); ?>">
                                        <div class="card card-product mb-3 product-card-2">
                                            <div class="card-body p-3">
                                            <div class="row">
                                                <div class="col-4">
                                                    <a href="<?php echo e(route('product', $wishlist->product->slug)); ?>" class="d-block" target="_blank">
														<?php if($wishlist->product->thumbnail_img!=null): ?>
															<img src="<?php echo e(asset($wishlist->product->thumbnail_img)); ?>" alt="<?php echo e($wishlist->product->name); ?>" class="img-fluid"/>
														<?php else: ?>
															<img src="<?php echo e(asset('frontend/images/product-thumb.jpg')); ?>" alt="<?php echo e($wishlist->product->name); ?>" class="img-fluid"/>
														<?php endif; ?>
                                                    </a>
                                                </div>
												<div class="col-8">
													<h2 class="heading heading-6 strong-600 mt-2 text-truncate-2">
														<a href="<?php echo e(route('product', $wishlist->product->slug)); ?>"target="_blank"><?php echo e($wishlist->product->name); ?></a>
													</h2>
													<div class="mt-2">
														<div class="price-box">
														<?php
															$home_base_price = home_base_price($wishlist->product->id);
															$home_discounted_base_price = home_discounted_base_price($wishlist->product->id);
														?>
															<?php if(home_base_price($wishlist->product->id) != home_discounted_base_price($wishlist->product->id)): ?>
																<del class="old-product-price strong-400"><?php echo e(home_base_price($wishlist->product->id)); ?></del>
																<span class="product-price strong-600"><?php echo e(home_discounted_base_price($wishlist->product->id)); ?></span>
																<strong>
																<span style="color:red;font-size:14px;">(<?php echo e(discount_calulate($home_base_price, $home_discounted_base_price )); ?>% off)</span></strong>
															<?php else: ?>
																<span class="product-price strong-600"><?php echo e(home_discounted_base_price($wishlist->product->id)); ?></span>
															<?php endif; ?>
														</div>
													</div>
												</div>
                                            </div>
                                            </div>
                                            <div class="card-footer p-3">
                                                <div class="product-buttons">
                                                    <div class="row align-items-center">
                                                        <div class="col-2">
                                                            <a href="#" class="link link--style-3 bg-red btn" data-toggle="tooltip" data-placement="top" title="Remove from wishlist" onclick="removeFromWishlist(<?php echo e($wishlist->id); ?>)">
                                                                <i class="fa fa-times mr-0"></i>
                                                            </a>
                                                        </div>
                                                        <div class="col-10">
														<?php
															$qty = 0;
															if($wishlist->product->variant_product){
																foreach ($wishlist->product->stocks as $key => $stock) {
																	$qty += $stock->qty;
																}
															}
															else{
																if($wishlist->product->current_stock > -1) {
																$qty = $wishlist->product->current_stock;
																} else {
																$qty = 100;
																}
																
															}
														?>
															 <?php if($qty > 0): ?>
																<button type="button" class="btn btn-block btn-base-1 btn-circle btn-icon-left" onclick="addWishtToCart(this,this.id,<?php echo e($wishlist->id); ?>)" id="<?php echo e($wishlist->product_id); ?>">
                                                                <i class="la la-shopping-cart mr-2"></i><?php echo e(__('Move to cart')); ?>

																</button>
															<?php else: ?>
																<button type="button" class="btn btn-block btn-base-3 btn-icon-left strong-700" disabled>
																<i class="la la-cart-arrow-down"></i> <?php echo e(__('Out of Stock')); ?>

																</button>
															<?php endif; ?>	
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
					</form>
                    <?php else: ?>
						<div class="card no-border mt-4">
							<div>
								<table class="table table-sm table-hover table-responsive-md">
									<thead>
										<tr class="text-center">
											<th><h3><?php echo e(__('No Wishlist Found')); ?></h3></th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					<?php endif; ?> 
                     
                     
                        <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                <?php echo e($wishlists->links()); ?>

                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        function addWishtToCart(el,id,wishid){
			//id= $("#id").val(id);
			var html = $(el).html();
            $(el).prop('disabled', true);
            $(el).html('<i class="fa fa-spin fa-spinner"></i> Loading');
            addToCart(el,id);
			removeFromWishlist(wishid);
		}
        function removeFromWishlist(id){
            $.post('<?php echo e(route('wishlists.remove')); ?>',{_token:'<?php echo e(csrf_token()); ?>', id:id}, function(data){
                $('#wishlist').html(data);
                $('#wishlist_'+id).hide();
                showFrontendAlert('success', 'Item has been renoved from wishlist');
            })
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/view_wishlist.blade.php ENDPATH**/ ?>