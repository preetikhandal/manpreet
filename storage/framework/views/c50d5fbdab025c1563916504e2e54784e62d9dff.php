<?php $__env->startSection('content'); ?>
<?php
    $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
?>

<!-- Basic Data Tables -->
<!--===================================================-->
<style>
    .ordcode{
        color:#282563;
    }
    .ordcode:hover{
        color:#EB3038;
    }
</style>
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <!-- <h3 class="panel-title pull-left pad-no"><?php echo e(__('Orders')); ?></h3> -->
        <div class="pull-right clearfix">
            <form class="" id="sort_orders" action="" method="GET">
                <!-- <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 300px;">
                        <select class="form-control demo-select2" name="payment_type" id="payment_type" onchange="sort_orders()">
                            <option value=""><?php echo e(__('Filter by Payment Status')); ?></option>
                            <option value="paid"  <?php if(isset($payment_status)): ?> <?php if($payment_status == 'paid'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Paid')); ?></option>
                            <option value="unpaid"  <?php if(isset($payment_status)): ?> <?php if($payment_status == 'unpaid'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Un-Paid')); ?></option>
                        </select>
                    </div>
                </div>
                <div class="box-inline pad-rgt pull-left">
                    <div class="select" style="min-width: 300px;">
                        <select class="form-control demo-select2" name="delivery_status" id="delivery_status" onchange="sort_orders()">
                            <option value=""><?php echo e(__('Filter by Deliver Status')); ?></option>
                            <option value="confirmation_pending"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'confirmation_pending'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Confirmation Pending')); ?></option>
                            <option value="hold"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'hold'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Hold')); ?></option>
                            <option value="pending"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'pending'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Pending')); ?></option>
                            <option value="on_review"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'on_review'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('On review')); ?></option>
                            <option value="ready_to_ship"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'ready_to_ship'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Ready to Ship')); ?></option>
                            <option value="ready_to_delivery"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'ready_to_delivery'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Ready to Delivery')); ?></option>
                            <option value="shipped"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'shipped'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Shipped')); ?></option>
                            <option value="on_delivery"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'on_delivery'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('On delivery')); ?></option>
                            <option value="delivered"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'delivered'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Delivered')); ?></option>
                            <option value="processing_refund"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'processing_refund'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Processing Refund')); ?></option>
                            <option value="refunded"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'refunded'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Refunded')); ?></option>
                            <option value="cancel"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'cancel'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Cancel')); ?></option>
                        </select>
                    </div>
                </div> -->
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;display:inline-block;">
                        <input type="text" class="form-control" id="search" name="search"<?php if(isset($sort_search)): ?> value="<?php echo e($sort_search); ?>" <?php endif; ?> placeholder="Type & Enter">
                    </div>
                    <div class="" style="min-width: 50px;display:inline-block;">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo e(__('Order Code')); ?></th>
                    <th><?php echo e(__('Num. of Products')); ?></th>
                    <th><?php echo e(__('Customer')); ?></th>
                    <th><?php echo e(__('Amount')); ?></th>
                    <th><?php echo e(__('Order Status')); ?></th>
                    <th><?php echo e(__('Pay Method')); ?></th>
                    <th><?php echo e(__('Pay Status')); ?></th>
                    <th><?php echo e(__('Seller Amount')); ?></th>
                    <th><?php echo e(__('Service Provider Comission')); ?></th>
                    <th><?php echo e(__('Inclusive Comission')); ?></th>
                    <th><?php echo e(__('Exclusive Comission')); ?></th>
                    <th><?php echo e(__('Credit to seller wallet')); ?></th>
                    <th><?php echo e(__('Options')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $order_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        $seller_id = $order_id->seller_id;
                        $order = \App\Order::find($order_id->id);
                         $orderDetails = \App\OrderDetail::find($order_id->id);
                    ?>
                    <?php if($order != null): ?>
                        <?php
                        if ($order->orderDetails->where('seller_id',  $seller_id)->first()->payment_status == 'paid') {
                            $payment_status = "paid";
                        } else {
                            $payment_status = "unpaid";
                        }
                        ?>
                        <tr>
                            <td>
                                <?php echo e(($key+1) + ($orders->currentPage() - 1)*$orders->perPage()); ?> <input type="checkbox" data-ordercode="<?php echo e($order->code); ?>" data-order-seller_id="<?php echo e($seller_id); ?>" data-payment="<?php echo e($payment_status); ?>" name="order_id[]" value="<?php echo e($order->id); ?>" id="check_order_id" class="check_order_id" />
                            </td>
                            <td>
                                <a href="<?php echo e(route('orders.seller.show', [$seller_id, encrypt($order->id)])); ?>" class="ordcode"> <?php echo e($order->code); ?></a><?php if($order->viewed == 0): ?> <span class="pull-right badge badge-info"><?php echo e(__('New')); ?></span> <?php endif; ?>
                            </td>
                            
                            <td><?php echo e(\App\Product::where('user_id', $seller_id)->count()); ?></td>
                            <td>
                                <?php if($order->user_id != null): ?>
                                    <?php echo e($order->user->name??' '); ?>

                                <?php else: ?>
                                    Guest (<?php echo e($order->guest_id); ?>)
                                <?php endif; ?>
                            </td>
                            <td>
								<?php echo e(single_price($order->orderDetails->where('seller_id', $seller_id)->sum('price') + $order->orderDetails->where('seller_id', $seller_id)->sum('tax'))); ?>

		
                            </td>
                            <td>
                                <?php
                                    $status = $order->orderDetails->where('seller_id', $seller_id)->first()->delivery_status;
                                ?>
                                <?php if($status == "Confirmation Pending"): ?>
                                <label class="label label-danger"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></label>
                                <?php elseif($status == "pending"): ?>
                                <label class="label label-warning"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></label>
                                <?php else: ?>
                                <label class="label label-warning"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></label>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e(ucfirst(str_replace('_', ' ', $order->payment_type))); ?>

                            </td>
                            <td>
                                <span class="badge badge--2 mr-4">
                                    <?php if($payment_status == 'paid'): ?>
                                        <i class="bg-green"></i> Paid
                                    <?php else: ?>
                                        <i class="bg-red"></i> Unpaid
                                    <?php endif; ?>
                                </span>
                            </td>
                            <td><?php echo e(single_price($order->service_provider)); ?></td>
                            <td><?php echo e($order->seller_commission); ?></td>
                            <?php
                               $getPrice = \App\OrderDetail::where('order_id', $order_id->id)->get(); 
                               if(isset($getPrice[0]->inclusive_price)){
                                   $comissionselleralde = single_price($order->seller_commission);
                                }else{
                                    $comissionselleralde = 0;
                                }
                               
                                if(isset($getPrice[0]->exclusive_price)){
                                   $comissionselleraldeex = single_price($order->seller_commission);
                                }else{
                                    $comissionselleraldeex = 0;
                                }
                            ?>
                           <td><?php echo e($comissionselleralde); ?></td>
                           
                            <td><?php echo e($comissionselleraldeex); ?></td>
                            <td><?php echo e($order->paid_to_seller_account); ?></td>
                            <td>
                                <?php if(count($order->orderDetails->where('seller_id', $seller_id)->where('delivery_status', 'confirmation_pending')) > 0): ?>
                                        <a  href="javascript:confirm_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a>
                                        <?php if($order->prescription_file != null): ?> 
                                            <a href="<?php echo e(asset('prescription/'.$order->prescription_file)); ?>" class="btn btn-primary"><?php echo e(__('Download Prescription')); ?></a>
                                        <?php endif; ?>
                                        <?php if($payment_status == "paid"): ?>
                                            <a href="javascript:confirm_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                        <?php else: ?>
                                            <a href="javascript:confirm_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'cancel')" class="btn btn-primary">Cancel Order</a>
                                        <?php endif; ?>
                                <?php else: ?>
                                    <?php if($status == "delivered"): ?>
                                    <?php elseif($status == "pending"): ?>
                                        <a  href="javascript:confirm_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'ready_to_ship')" class="btn btn-primary">Ready to Ship</a><a href="javascript:trash_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'trash')" class="btn btn-primary">Trash Order</a>
                                        <?php if($order->prescription_file != null): ?> 
                                            <a href="<?php echo e(asset('prescription/'.$order->prescription_file)); ?>" class="btn btn-primary"><?php echo e(__('Download Prescription')); ?></a>
                                        <?php endif; ?>
                                        <!--a onclick="confirm_modal('<?php echo e(route('orders.destroy', $order->id)); ?>');" class="btn btn-primary"><?php echo e(__('Delete')); ?></a-->
                                        <?php if($payment_status == "paid"): ?>
                                            <a href="javascript:confirm_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'cancel_refund')" class="btn btn-primary">Cancel & Process for Refund</a>
                                        <?php else: ?>
                                            <a href="javascript:confirm_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'cancel')" class="btn btn-primary">Cancel Order</a>
                                        <?php endif; ?>
                                    <?php elseif($status == "ready_to_ship"): ?> 
                                        <a href="javascript:order_shipping_info(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>)"  class="btn btn-primary"><?php echo e(__('Add Shipping info')); ?></a>
                                        <a href="<?php echo e(route('orders.printlabel.seller', [$seller_id, $order->id])); ?>" target="_blank" class="btn btn-primary"><?php echo e(__('Print Label')); ?></a>
                                    <?php elseif($status == "shipped"): ?> 
                                        <?php if($payment_status == 'unpaid'): ?>
                                            <a href="javascript:confirm_payment(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'paid')" class="btn btn-primary">Mark Paid</a>
                                            <span id="orderbtn_<?php echo e($order->id); ?>"><a href="javascript:void(0)" class="btn btn-primary" disabled><?php echo e(__('Mark Delivered')); ?></a></span>
                                        <?php else: ?>
                                        <span id="orderbtn_<?php echo e($order->id); ?>"><a href="javascript:confirm_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'delivered')" class="btn btn-primary"><?php echo e(__('Mark Delivered')); ?></a></span>
                                        <?php endif; ?>
                                    <?php elseif($status == "processing_refund"): ?> 
                                        <a href="javascript:confirm_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'refunded')" class="btn btn-primary">Refunded to Wallet</a>
                                    <?php elseif($status == "on_review" or $status == "hold"): ?>
                                        <a href="javascript:confirm_order(<?php echo e($order->id); ?>, <?php echo e($seller_id); ?>, 'pending')" class="btn btn-primary">Move to Pending</a>
                                    <?php else: ?>
                                    <button class="btn btn-primary" disabled><?php echo e(__('No Action')); ?></button>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                     <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="clearfix">
            <div class="col-md-12">
                <br />&nbsp;
                <?php if(\Route::currentRouteName() == 'orders.index.shipped.admin.seller' ): ?>
                <button class="btn btn-success" id="markdeliverybtn" onclick="showmodal('mark_delivery_modal')" disabled>Mark Selected to Delivered</button>
                <button class="btn btn-primary" id="markpaidbtn" onclick="showmodal('payment_status_modal');" disabled>Mark Selected to Paid</button>
                <?php endif; ?>
                
                <br />&nbsp;
                <br />&nbsp;
            </div>
        </div>
        <div class="clearfix">
            <div class="pull-right">
                <?php echo e($orders->appends(request()->input())->links()); ?>

            </div>
        </div>
    </div>
</div>
<script>
    function showmodal(modalid) {
        productArray = [];
        $('#check_order_id:checked').each(function(i,v) {
        productArray.push($(v).val());
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        $('#'+modalid).modal();
    }
</script>

<div class="modal fade" id="payment_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Changing of Payment Status')); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 text-right pt-2">
                       <center><strong>Do you want to continue ?</strong></center>
                    </div>
                        <input type="hidden" value="paid" id="modal_payment_status">
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="change_status('payment')"  data-dismiss="modal">Continue</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mark_delivery_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Changing of Delivery Status')); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 text-right pt-2">
                       <center><strong>Do you want to continue ?</strong></center>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <button class="btn btn-success"  onclick="mark_delivered()"  data-dismiss="modal">Continue</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="shipping_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Shipping Info')); ?></h4>
            </div>
            <form action="<?php echo e(route('orders.shipping')); ?>" method="post">
            <?php echo csrf_field(); ?>
            <div class="modal-body">
            <input type="hidden" name="order_id" id="modal_order_id">
            <input type="hidden" name="seller_id" id="modal_seller_id">
                <div class="form-group row">
                    <div class="col-sm-3 text-right pt-2">
                        Courier &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name='courier' placeholder="Courier Company Name" >
                    </div>
                </div>
                <div class="form-group row" style="margin-top:5px;">
                    <div class="col-sm-3 text-right pt-2">
                        AWB/Ref/Tracking No. &nbsp;
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="awb" class="form-control" placeholder="AWB Number" >
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                  <button class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

    <script type="text/javascript">
        
        $('#undelivery_Form').submit(function(e){
            e.preventDefault();
            formdata =  $('#undelivery_Form').serialize();
            $('#undelivery_status_modal').modal('hide');
            $.ajax({
                type: 'POST',
                url: '<?php echo e(route('orders.update_status')); ?>',
                data:formdata,
                processData: false,
                success: function(result){
                 location.reload();
                }
            });
        });
    <?php if(\Route::currentRouteName() == 'orders.index.shipped.admin' ): ?>
    
    function checkdeliverypayButton() {
        productunpaidArray = [];
        productpaidArray = [];
        $('#check_order_id:checked').each(function(i,v) {
            if($(v).data('payment') == 'unpaid') {
                productunpaidArray.push($(v).val());
            } else {
                productpaidArray.push($(v).val());
            }
        });
        console.log(productunpaidArray.length);
        console.log(productpaidArray.length);
        
        if(productunpaidArray.length == 0) {
            $('#markdeliverybtn').prop('disabled', false);
        } else {
            $('#markdeliverybtn').prop('disabled', 'disabled');
        }
        
        if(productpaidArray.length == 0) {
            $('#markpaidbtn').prop('disabled', false);
        } else {
            $('#markpaidbtn').prop('disabled', 'disabled');
        }
        if(productpaidArray.length == 0 && productunpaidArray.length == 0 ) {
            $('#markpaidbtn').prop('disabled', 'disabled');
            $('#markdeliverybtn').prop('disabled', 'disabled');
        }
    }
    
    $('.check_order_id').click(function() {
        checkdeliverypayButton();
    });
    
    <?php endif; ?>
    
    function mark_delivered() {
        productArray = [];
        seller_id = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
           seller_id.push($(v).data('order-seller_id'));
        });
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        action = "delivered";
        var setFlash = 'Order set for delivered status.';
        $.post('<?php echo e(route('orders.update_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', action:action, setFlash:setFlash, order_ids:productArray, seller_ids : seller_id }, function(data){
             location.reload();
        });
    }
        
    function change_status(action) {
        if(action == 'delivery') {
            status = $('#modal_delivery_status').val();
            $('#modal_delivery_status').val("");
            $('#modal_delivery_status').trigger('change');
        }
        if(action == 'payment') {
            status = $('#modal_payment_status').val();
            $('#modal_payment_status').val("");
            $('#modal_payment_status').trigger('change');
        }
        productArray = [];
        seller_id = [];
        $('#check_order_id:checked').each(function(i,v) {
           productArray.push($(v).val()); 
           seller_id.push($(v).data('order-seller_id'));
        }); 
        if(productArray.length == 0) {
            alert("Kindly select order.");
            return false;
        }
        
        var setFlash = 'Order '+action+' status has been updated';
        $.post('<?php echo e(route('orders.update_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', action:action, setFlash:setFlash, order_ids:productArray, status:status, seller_ids : seller_id}, function(data){
             location.reload();
        });
    }
        
    function order_shipping_info(order_id, seller_id)
    {
        $('#modal_order_id').val(order_id);
        $('#modal_seller_id').val(seller_id);
        $('#shipping_info').modal();
    }
        function sort_orders(el){
            $('#sort_orders').submit();
        }
    
        function confirm_payment(order_id, seller_id, status) {
            var setFlash = 'Payment status has been updated';
            $.post('<?php echo e(route('orders.update_payment_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', setFlash:setFlash, order_id:order_id, status:status, seller_id : seller_id }, function(data){
                location.reload();
            });
        }

        function confirm_order(order_id, seller_id, status) {
            var setFlash = 'Order status has been updated';
            $.post('<?php echo e(route('orders.update_delivery_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', setFlash:setFlash, order_id:order_id,status:status, seller_id : seller_id }, function(data){
                 location.reload();
            });
        }
        
         function trash_order(order_id, seller_id,status) {
            var setFlash = 'Order status has been updated';
             $.post('<?php echo e(route('orders.update_delivery_status')); ?>', {_token:'<?php echo e(@csrf_token()); ?>', setFlash:setFlash, order_id:order_id,status:status, seller_id : seller_id }, function(data){
                 location.reload();
            });
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/orders/seller/index.blade.php ENDPATH**/ ?>