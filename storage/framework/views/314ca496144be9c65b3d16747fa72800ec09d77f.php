<?php $__env->startSection('content'); ?>
<style>
    .ordcode{
        color:#282563;
    }
    .ordcode:hover{
        color:#EB3038;
    }
</style>
<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading bord-btm clearfix pad-all h-100">
        <h3 class="panel-title pull-left pad-no"><?php echo e(__('Orders')); ?></h3>

        <div class="pull-right clearfix">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"<?php if(isset($sort_search)): ?> value="<?php echo e($sort_search); ?>" <?php endif; ?> placeholder="Type code & Enter">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel-body">
        <form  id="sort_orders" method="GET">
        <div class="row">
            <div class="col-md-5">
               <div class="input-daterange input-group" id="datepicker_range">
                    <input type="text" class="input-sm form-control" name="startDate" id="startDate" value="<?php echo e($startDate); ?>" />
                    <span class="input-group-addon">to</span>
                    <input type="text" class="input-sm form-control" name="endDate" id="endDate" value="<?php echo e($endDate); ?>" />
                </div>
            </div>
            <div class="col-md-2">
                <select class="form-control demo-select2" name="payment_type" id="payment_type">
                    <option value="all" <?php if(isset($payment_status)): ?> <?php if($payment_status == 'all'): ?> selected <?php endif; ?> <?php endif; ?> >All</option>
                    <option value="paid"  <?php if(isset($payment_status)): ?> <?php if($payment_status == 'paid'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Paid')); ?></option>
                    <option value="unpaid"  <?php if(isset($payment_status)): ?> <?php if($payment_status == 'unpaid'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Un-Paid')); ?></option>
                </select>
            </div>
            <div class="col-md-3">
                <select name="delivery_status" id="delivery_status" class="form-control">
                    <option value="all" <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'all'): ?> selected <?php endif; ?> <?php endif; ?>>All</option>
                    <option value="confirmation_pending" <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'confirmation_pending'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Confirmation Pending')); ?></option>
                    <option value="hold"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'hold'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Hold')); ?></option>
                    <option value="pending"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'pending'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Pending')); ?></option>
                    <option value="on_review"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'on_review'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('On review')); ?></option>
                    <option value="ready_to_ship"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'ready_to_ship'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Ready to Ship')); ?></option>
                    <option value="ready_to_delivery"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'ready_to_delivery'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Ready to Delivery')); ?></option>
                    <option value="shipped"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'shipped'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Shipped')); ?></option>
                    <option value="on_delivery"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'on_delivery'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('On delivery')); ?></option>
                    <option value="delivered"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'delivered'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Delivered')); ?></option>
                    <option value="processing_refund"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'processing_refund'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Processing Refund')); ?></option>
                    <option value="refunded"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'refunded'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Refunded')); ?></option>
                    <option value="cancel"   <?php if(isset($delivery_status)): ?> <?php if($delivery_status == 'cancel'): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e(__('Cancel')); ?></option>
                </select>
            </div>
            <div class="col-md-1">
                <button class="btn btn-primary">Filter</button>
            </div>
            <div class="col-md-1">
                <button type="button" onclick="download()" class="btn btn-primary">Download</button>
            </div>
        </div>
        </form>
        
        <form action="<?php echo e(route('sales.Downlaod')); ?>" id="download_form" method="post">
            <?php echo csrf_field(); ?>
            <input type="hidden" name="startDate" id="startDateDownload">
            <input type="hidden" name="endDate" id="endDateDownload">
            <input type="hidden" name="payment_type" id="payment_typeDownload">
            <input type="hidden" name="delivery_status" id="delivery_statusDownload">
        </form>
        <script>
        function download() {
            startDate = $('#startDate').val();
            endDate = $('#endDate').val();
            payment_type = $('#payment_type').val();
            delivery_status = $('#delivery_status').val();
            $('#startDateDownload').val(startDate);
            $('#endDateDownload').val(endDate);
            $('#payment_typeDownload').val(payment_type);
            $('#delivery_statusDownload').val(delivery_status);
            $('#download_form').submit();
        }
        </script>
        <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Order Code</th>
                    <th>Num. of<br />Products</th>
                    <th>Customer</th>
                    <th>Total Order Value</th>
                    <th>Discount</th>
                    <th>Wallet<br />Credit Used</th>
                    <th>Amount</th>
                    <th>Delivery Status</th>
                    <th>Payment Status</th>
                    <th width="10%"><?php echo e(__('options')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>
                            <?php echo e(($key+1) + ($orders->currentPage() - 1)*$orders->perPage()); ?>

                        </td>
                        <td>
                            <a class="ordcode" href="<?php echo e(route('sales.show', encrypt($order->id))); ?>"><?php echo e($order->code); ?></a>
                        </td>
                        <td>
                            <?php echo e(count($order->orderDetails)); ?>

                        </td>
                        <td>
                            <?php if($order->user_id != null): ?>
                                <?php echo e($order->user->name); ?>

                            <?php else: ?>
                                Guest (<?php echo e($order->guest_id); ?>)
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php echo e(single_price($order->grand_total + $order->coupon_discount + $order->wallet_credit)); ?>

                        </td>
                        <td>
                            <?php echo e(single_price($order->coupon_discount)); ?>

                        </td>
                        <td>
                            <?php echo e(single_price($order->wallet_credit)); ?>

                        </td>
                        <td>
                            <?php echo e(single_price($order->grand_total)); ?>

                        </td>
                        <td>
                            <?php
                                $status = 'Delivered';
                                foreach ($order->orderDetails as $key => $orderDetail) {
                                    if($orderDetail->delivery_status != 'delivered'){
                                        $status = 'Pending';
                                    }
                                }
                            ?>
                            <?php echo e($status); ?>

                        </td>
                        <td>
                            <span class="badge badge--2 mr-4">
                                <?php if($order->payment_status == 'paid'): ?>
                                    <i class="bg-green"></i> Paid
                                <?php else: ?>
                                    <i class="bg-red"></i> Unpaid
                                <?php endif; ?>
                            </span>
                        </td>
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    <?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="<?php echo e(route('sales.show', encrypt($order->id))); ?>"><?php echo e(__('View')); ?></a></li>
                                    <li><a href="<?php echo e(route('customer.invoice.download', $order->id)); ?>"><?php echo e(__('Download Invoice')); ?></a></li>
                                    <li><a onclick="confirm_modal('<?php echo e(route('orders.destroy', $order->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        <div class="clearfix">
            <div class="pull-right">
                <?php echo e($orders->appends(request()->input())->links()); ?>

            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
$('.input-daterange').datepicker({
    format: "dd/mm/yyyy",
    endDate: "date()",
    autoclose: true
});
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/sales/index.blade.php ENDPATH**/ ?>