<div class="modal-header bg-blue">
    <h5 class="modal-title strong-600 heading-5"><?php echo e(__('Order id')); ?>: <?php echo e($order->code); ?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<?php
    $status = $order->orderDetails->first()->delivery_status;
    $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
?>

<div class="modal-body gry-bg px-1 px-md-3 pt-0">
    <div class="pt-4">
        <ul class="process-steps clearfix">
            <li <?php if($status == 'pending'): ?> class="active" <?php else: ?> class="done" <?php endif; ?>>
                <div class="icon">1</div>
                <div class="title"><?php echo e(__('Order placed')); ?></div>
            </li>
            <li <?php if($status == 'on_review'): ?> class="active" <?php elseif($status == 'on_delivery' || $status == 'delivered'): ?> class="done" <?php endif; ?>>
                <div class="icon">2</div>
                <div class="title"><?php echo e(__('On review')); ?></div>
            </li>
            <li <?php if($status == 'on_delivery'): ?> class="active" <?php elseif($status == 'delivered'): ?> class="done" <?php endif; ?>>
                <div class="icon">3</div>
                <div class="title"><?php echo e(__('On delivery')); ?></div>
            </li>
            <li <?php if($status == 'delivered'): ?> class="done" <?php endif; ?>>
                <div class="icon">4</div>
                <div class="title"><?php echo e(__('Delivered')); ?></div>
            </li>
        </ul>
    </div>
    <div class="card mt-4">
        <div class="card-header py-2 px-3 heading-6 strong-600 clearfix">
            <div class="float-left"><?php echo e(__('Order Summary')); ?></div>
        </div>
        <div class="card-body pb-0">
            <div class="row">
                <div class="col-lg-6 px-0">
                    <table class="details-table table">
                        <tr>
                            <td class="w-50 strong-600"><?php echo e(__('Order Code')); ?>:</td>
                            <td><?php echo e($order->code); ?></td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600"><?php echo e(__('Customer')); ?>:</td>
                            <td><?php echo e(json_decode($order->shipping_address)->name); ?></td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600"><?php echo e(__('Email')); ?>:</td>
                            <?php if($order->user_id != null): ?>
                                <td><?php echo e($order->user->email); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600"><?php echo e(__('Shipping address')); ?>:</td>
                            <td><?php echo e(json_decode($order->shipping_address)->address); ?>, <?php echo e(json_decode($order->shipping_address)->city); ?>, <?php echo e(json_decode($order->shipping_address)->postal_code); ?>, <?php echo e(json_decode($order->shipping_address)->country); ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-6 px-0">
                    <table class="details-table table">
                        <tr>
                            <td class="w-50 strong-600"><?php echo e(__('Order date')); ?>:</td>
                            <td><?php echo e(date('d-m-Y H:m A', $order->date)); ?></td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600"><?php echo e(__('Order status')); ?>:</td>
                            <td><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600"><?php echo e(__('Total order amount')); ?>:</td>
                            <td><?php echo e(single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax'))); ?></td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600"><?php echo e(__('Shipping method')); ?>:</td>
                            <td><?php echo e(__('Flat shipping rate')); ?></td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600"><?php echo e(__('Payment method')); ?>:</td>
                            <td><?php echo e(ucfirst(str_replace('_', ' ', $order->payment_type))); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <style>
    .detailsdiv .info-aside {
        width: 55%;
    }
    </style>
    <div class="row">
        <div class="col-lg-8">
            <div class="card my-3 detailsdiv">
                <div class="card-header py-2 px-3 heading-6 strong-600"><?php echo e(__('Order Details')); ?>

                    <div class="info-aside float-right">
                         <?php if($status=='delivered'): ?>
                            <form action="<?php echo e(route('return.details')); ?>" method="post">
                                <?php echo csrf_field(); ?>
                                <input type="hidden" value="<?php echo e($order->id); ?>" name="order_id"/>
                                <button type="submit" class="btn btn-light bg-blue float-right text-white"><?php echo e(__('Apply for Return')); ?></button>
                            </form>
                         <?php elseif($status == "pending_confirmation" || $status == "pending" || $status == "ready_to_ship"): ?>
                            
                                 
                                <form action="<?php echo e(route('purchase_history.order_cancel')); ?>" method="post">
                                    <?php echo csrf_field(); ?> <input type="hidden" value="<?php echo e($order->id); ?>" name="order_id"/>
                                    <button type="submit" class="btn btn-light bg-blue float-right text-white"><?php echo e(__('Cancel Order')); ?></button>
                                </form>
                                 
                               
                        <?php endif; ?>
    			    	<a href="<?php echo e(route('customer.invoice.download', $order->id)); ?>" class="btn btn-light bg-red float-right">Download Invoice</a>
    				</div>
				</div>
            </div>    
                <?php $__currentLoopData = $order->orderDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $orderDetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                	<article class="card card-product-list">
                    	<div class="row no-gutters align-items-center">
                    		<div class="col-md-2 col-3 ">
                    			<a href="<?php echo e(route('product', $orderDetail->product->slug)); ?>">
                			    	<?php if($orderDetail->product->thumbnail_img!=""): ?>
                    			    	<img src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" data-src="<?php echo e(asset($orderDetail->product->thumbnail_img)); ?>" alt="<?php echo e($orderDetail->product->name); ?>" class="img-fluid lazyload">
                    			    <?php else: ?>
                    			        <img src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" alt="<?php echo e($orderDetail->product->name); ?>" class="img-fluid lazyload">
                    			    <?php endif; ?>
                    			</a>
                    		</div> 
                    		<div class="col-md-7 col-9 ">
                    			<div class="info-main ">
                    			    <?php if($orderDetail->product != null): ?>
                    			    	<a href="<?php echo e(route('product', $orderDetail->product->slug)); ?>" target="_blank" class="h5 title"><?php echo e($orderDetail->product->name); ?></a>
                    		        <?php else: ?>
                                        <strong><?php echo e(__('Product Unavailable')); ?></strong>
                                    <?php endif; ?>
                                    <span class="qty">Qty <?php echo e($orderDetail->quantity); ?></span>
                    		        <span class="price h5 d-block d-md-none"><?php echo e(single_price($orderDetail->price)); ?> </span>
                    			</div>
                    		</div> 
                    		<div class="col-md-3 d-none d-md-block">
                    			<div class="info-aside" style="border-left: none">
                    				<div class="price-wrap">
                    					<span class="price h5"> <?php echo e(single_price($orderDetail->price)); ?> </span>	
                    				</div>
                    			</div> 
                    		</div> 
                    	</div> 
                    </article>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="col-lg-4">
            <div class="card mt-4">
                <div class="card-header py-2 px-3 heading-6 strong-600"><?php echo e(__('Order Ammount')); ?></div>
                <div class="card-body pb-0">
                    <table class="table details-table">
                        <tbody>
                            <tr>
                                <th><?php echo e(__('Subtotal')); ?></th>
                                <td class="text-right">
                                    <span class="strong-600"><?php echo e(single_price($order->orderDetails->sum('price'))); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo e(__('Shipping')); ?></th>
                                <td class="text-right">
                                    <span class="text-italic"><?php echo e(single_price($order->orderDetails->sum('shipping_cost'))); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo e(__('Tax')); ?></th>
                                <td class="text-right">
                                    <span class="text-italic"><?php echo e(single_price($order->orderDetails->sum('tax'))); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo e(__('Coupon Discount')); ?></th>
                                <td class="text-right">
                                    <span class="text-italic"><?php echo e(single_price($order->coupon_discount)); ?></span>
                                </td>
                            </tr>
                            <?php if($order->cart_shipping > 0): ?>
                            <tr>
                                <th><?php echo e(__('Delivery Charges')); ?></th>
                                <td class="text-right">
                                    <span class="text-italic"><?php echo e(single_price($order->cart_shipping)); ?></span>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <?php if($order->wallet_credit > 0): ?>
                            <tr>
                                <th><?php echo e(__('Wallet Credit Used')); ?></th>
                                <td class="text-right">
                                    <span class="text-italic"><?php echo e(single_price($order->wallet_credit)); ?></span>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <tr>
                                <th><span class="strong-600"><?php echo e(__('Total')); ?></span></th>
                                <td class="text-right">
                                    <strong><span><?php echo e(single_price($order->grand_total)); ?></span></strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php if($order->manual_payment && $order->manual_payment_data == null): ?>
                <button onclick="show_make_payment_modal(<?php echo e($order->id); ?>)" class="btn btn-block btn-base-1"><?php echo e(__('Make Payment')); ?></button>
            <?php endif; ?>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order cancel</h5>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          Warrning:- Do you want to cancel this order
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        
        <!--<button type="button" class="btn btn-primary">Confirm</button>-->
         <form action="<?php echo e(route('purchase_history.order_cancel')); ?>" method="post">
                <?php echo csrf_field(); ?> <input type="hidden" id="cancelOrderId" name="order_id"/>
            <button type="submit" class="btn btn-light bg-blue float-right text-white"><?php echo e(__('Confirm')); ?></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--END MODEL-->




<script type="text/javascript">
    function show_make_payment_modal(order_id){
        $.post('<?php echo e(route('checkout.make_payment')); ?>', {_token:'<?php echo e(csrf_token()); ?>', order_id : order_id}, function(data){
            $('#payment_modal_body').html(data);
            $('#payment_modal').modal('show');
            $('input[name=order_id]').val(order_id);
        });
    }
</script>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/partials/order_details_customer.blade.php ENDPATH**/ ?>