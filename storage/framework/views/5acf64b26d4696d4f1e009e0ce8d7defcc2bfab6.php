

<?php $__env->startSection('content'); ?>
<style>
.process-steps li.done .icon, .process-steps li.done:after, .process-steps li.active:after, .process-steps li.active .icon {
    background:#20b34e!important;
}
    .panel-order .row {
    	border-bottom: 1px solid #ccc;
    }
    .panel-order .row:last-child {
    	border: 0px;
    }
    .panel-order .row .col-md-1  {
    	text-align: center;
    	padding-top: 15px;
    }
    .panel-order .row .col-md-1 img {
    	width: 50px;
    	max-height: 50px;
    }
    .panel-order .row .row {
    	border-bottom: 0;
    }
    .panel-order .row .col-md-11 {
    	border-left: 1px solid #ccc;
    }
    .panel-order .row .row .col-md-12 {
    	padding-top: 7px;
    	padding-bottom: 7px; 
    }
    .panel-order .row .row .col-md-12:last-child {
    	font-size: 11px; 
    	color: #555;  
    	background: #efefef;
    }
    .panel-order .btn-group {
    	margin: 0px;
    	padding: 0px;
    }
    .panel-order .panel-body {
    	padding-top: 0px;
    	padding-bottom: 0px;
    }
    .panel-order .panel-deading {
    	margin-bottom: 0;
    } 
    .prodname{
        font-weight:bold;
        font-size:14px;
    }
    .prodname::after{
        content:",";
    }
    .prodname:last-child::after{
        content:" ";
    }
    .total,.totalpricevalue{
        font-size:13px;
    }
    .totalpricevalue{font-weight:800;}
    .label {
      color: white;
      padding: 5px;
    }
</style>
<!-- Seller Style -->
<link type="text/css" href="<?php echo e(asset('frontend/css/seller.css')); ?>" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" <?php if($mobileapp == 1): ?> style="margin-top:75px" <?php endif; ?>>
        <div class="container-fluid d-none d-lg-block">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    <?php echo $__env->make('frontend.inc.seller_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>

                <div class="col-lg-9">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    <?php echo e(__('Dashboard')); ?>

                                </h2>
                            </div>
                            <div class="col-md-6">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="<?php echo e(route('home')); ?>"><?php echo e(__('Home')); ?></a></li>
                                        <li class="active"><a href="<?php echo e(route('dashboard')); ?>"><?php echo e(__('Dashboard')); ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dashboard content -->
                    <div class="">
                        <div class="row">
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center green-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-upload"></i>
                                        <span class="d-block title heading-3 strong-400"><?php echo e(count(\App\Product::where('user_id', Auth::user()->id)->get())); ?></span>
                                        <span class="d-block sub-title"><?php echo e(__('Products')); ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center red-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-cart-plus"></i>
                                        <span class="d-block title heading-3 strong-400"><?php echo e(count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'delivered')->get())); ?></span>
                                        <span class="d-block sub-title"><?php echo e(__('Total sale')); ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center red-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-cart-plus"></i>
                                        <span class="d-block title heading-3 strong-400">
                                            <?php
                                           $seller =  \App\Seller::where('user_id',Auth::user()->id)->get();
                                         
                                           ?>
                                             <?php echo e($seller[0]->order_cancel_credit); ?>

                                        </span>
                                        <span class="d-block sub-title"><?php echo e(__('order Credit Limit')); ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center red-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-cart-plus"></i>
                                        <span class="d-block title heading-3 strong-400">
                                            <?php
                                            $wallets = \App\Wallet::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->paginate(9);
        
                                                $cashWallet = \App\Wallet::where('user_id', Auth::user()->id)->where('payment_method','cashback')->sum('amount');
                                                $paywallet = \App\Wallet::where('user_id', Auth::user()->id)->where('payment_method','Wallet Payment')->sum('amount');
                                                $totalWallet  =  $cashWallet-$paywallet;
                                            ?>
                                            <?php echo e($totalWallet); ?></span>
                                        <span class="d-block sub-title"><?php echo e(__('Wallet Amount')); ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center blue-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-rupee"></i>
                                        <?php
                                            $orderDetails = \App\OrderDetail::where('seller_id', Auth::user()->id)->get();
                                            $total = 0;
                                            foreach ($orderDetails as $key => $orderDetail) {
                                                if($orderDetail->order->payment_status == 'paid'){
                                                    $total += $orderDetail->price;
                                                }
                                            }
                                        ?>
                                        <span class="d-block title heading-3 strong-400"><?php echo e(single_price($total)); ?></span>
                                        <span class="d-block sub-title"><?php echo e(__('Total earnings')); ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div class="dashboard-widget text-center yellow-widget mt-4 c-pointer">
                                    <a href="javascript:;" class="d-block">
                                        <i class="fa fa-check-square-o"></i>
                                        <span class="d-block title heading-3 strong-400"><?php echo e(count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'delivered')->get())); ?></span>
                                        <span class="d-block sub-title"><?php echo e(__('Successful orders')); ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2 text-center">
                                        <?php echo e(__('Orders')); ?>

                                    </div>
                                    <div class="form-box-content p-3">
                                        <table class="table mb-0 table-bordered" style="font-size:14px;">
                                            <tr>
                                                <td><?php echo e(__('Total orders')); ?>:</td>
                                                <td><strong class="heading-6"><?php echo e(count(\App\OrderDetail::where('seller_id', Auth::user()->id)->get())); ?></strong></td>
                                            </tr>
                                            <tr >
                                                <td><?php echo e(__('Pending orders')); ?>:</td>
                                                <td><strong class="heading-6"><?php echo e(count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'pending')->get())); ?></strong></td>
                                            </tr>
                                            <tr >
                                                <td><?php echo e(__('Cancelled orders')); ?>:</td>
                                                <td><strong class="heading-6"><?php echo e(count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'cancelled')->get())); ?></strong></td>
                                            </tr>
                                            <tr >
                                                <td><?php echo e(__('Successful orders')); ?>:</td>
                                                <td><strong class="heading-6"><?php echo e(count(\App\OrderDetail::where('seller_id', Auth::user()->id)->where('delivery_status', 'delivered')->get())); ?></strong></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
								<div class="bg-white mt-4 p-4 text-center">
                                    <div class="heading-4 strong-700"><?php echo e(__('Shop')); ?></div>
                                    <p><?php echo e(__('Manage & organize your shop')); ?></p>
                                    <a href="<?php echo e(route('shops.index')); ?>" class="btn btn-styled bg-blue btn-sm"><?php echo e(__('Go to setting')); ?></a>
                                </div>
                                <div class="bg-white mt-4 p-4 text-center">
                                    <div class="heading-4 strong-700"><?php echo e(__('Payment')); ?></div>
                                    <p><?php echo e(__('Configure your payment method')); ?></p>
                                    <a href="<?php echo e(route('profile')); ?>" class="btn btn-styled bg-blue btn-sm"><?php echo e(__('Configure Now')); ?></a>
                                </div>
                                <!--<div class="bg-white mt-4 p-5 text-center">
                                    <div class="mb-3">
                                        <?php if(Auth::user()->seller->verification_status == 0): ?>
                                            <img loading="lazy"  src="<?php echo e(asset('frontend/images/icons/non_verified.png')); ?>" alt="" width="130">
                                        <?php else: ?>
                                            <img loading="lazy"  src="<?php echo e(asset('frontend/images/icons/verified.png')); ?>" alt="" width="130">
                                        <?php endif; ?>
                                    </div>
                                    <?php if(Auth::user()->seller->verification_status == 0): ?>
                                        <a href="<?php echo e(route('shop.verify')); ?>" class="btn btn-styled btn-base-1"><?php echo e(__('Verify Now')); ?></a>
                                    <?php endif; ?>
                                </div>-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2 text-center">
                                        <?php echo e(__('Products')); ?>

                                    </div>
                                    <div class="form-box-content p-3 category-widget">
                                        <ul class="clearfix">
                                            <?php $__currentLoopData = \App\Category::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(count($category->products->where('user_id', Auth::user()->id))>0): ?>
                                                    <li><a><?php echo e(__($category->name)); ?><span>(<?php echo e(count($category->products->where('user_id', Auth::user()->id))); ?>)</span></a></li>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                        <div class="text-center mt-2">
                                            <a href="<?php echo e(route('seller.products.upload')); ?>" class="btn bg-blue"><?php echo e(__('Add New Product')); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid d-block d-lg-none" style="max-width:100%!important">
            <div class="row">
                <div class="col-12">
                    <div class="page-title">
                        <div class="row align-items-center bg-blue py-2">
                            <div class="col-12">
                                <h2 class="heading heading-3 text-capitalize strong-600 mb-0 p-0 text-white">
                                    <?php echo e(__('Seller account')); ?>

                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mobileaccount">
                    <div class="widget-profile-menu py-3">
                        <ul class="categories categories--style-3">
                            <li>
                                <a href="<?php echo e(route('seller.products')); ?>">
                                    <span class="category-name">
                                        <?php echo e(__('Products')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('product_bulk_upload.index')); ?>">
                                    <span class="category-name">
                                        <?php echo e(__('Product Bulk Upload')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(url('ordersTabular')); ?>">
                                    <span class="category-name">
                                        <?php echo e(__('Orders')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('reviews.seller')); ?>">
                                    <span class="category-name">
                                        <?php echo e(__('Product Reviews')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('shops.index')); ?>">
                                    <span class="category-name">
                                        <?php echo e(__('Shop Setting')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('withdraw_requests.index')); ?>">
                                    <span class="category-name">
                                        <?php echo e(__('Money Withdraw')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12">
                    <div class="page-title">
                        <div class="row align-items-center bg-blue py-2">
                            <div class="col-12">
                                <h2 class="heading heading-3 text-capitalize strong-600 mb-0 p-0 text-white">
                                    <?php echo e(__('User account')); ?>

                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mobileaccount">
                    <div class="widget-profile-menu py-3">
                        <ul class="categories categories--style-3">
                            <li>
                                <a href="<?php echo e(route('purchase_history.index')); ?>">
                                    <span class="category-name">
                                        <?php echo e(__('Your Orders')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('wishlists.index')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Wishlist')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('orders.track')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Track Order')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('customer_refund_track')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Track Refund Request')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('profile')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Manage Profile')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                            <?php if(\App\BusinessSetting::where('type', 'wallet_system')->first()->value == 1): ?>
                                <li>
                                    <a href="<?php echo e(route('wallet.index')); ?>" >
                                        <span class="category-name">
                                            <?php echo e(__('My Wallet')); ?>

                                        </span>
                                        <i class="fa fa-chevron-right float-right"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a href="<?php echo e(route('support_ticket.index')); ?>" >
                                    <span class="category-name">
                                        <?php echo e(__('Support Ticket')); ?>

                                    </span>
                                    <i class="fa fa-chevron-right float-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>  
    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/seller/dashboard.blade.php ENDPATH**/ ?>