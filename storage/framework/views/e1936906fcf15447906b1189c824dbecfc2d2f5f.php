<div class="keyword">
    <?php if(sizeof($keywords) > 0): ?>
        <div class="title"><?php echo e(__('Popular Suggestions')); ?></div>
        <ul>
            <?php $__currentLoopData = $keywords; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $keyword): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><a href="<?php echo e(route('suggestion.search', $keyword)); ?>"><?php echo e($keyword); ?></a></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>
</div>
<div class="category">
    <?php if(count($subsubcategories) > 0): ?>
        <div class="title"><?php echo e(__('Category Suggestions')); ?></div>
        <ul>
            <?php $__currentLoopData = $subsubcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $subsubcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                if(env('CACHE_DRIVER') == 'redis') {
					$SubCategory = Cache::tags(['SubCategory'])->rememberForever('SubCategory:'.$subsubcategory->sub_category_id, function () use ($subsubcategory) {
                            return  \App\SubCategory::find($subsubcategory->sub_category_id);
                    });
					$Category = Cache::tags(['Category'])->rememberForever('Category:'.$SubCategory->category_id, function () use ($SubCategory) {
                            return  \App\Category::where('id', $SubCategory->category_id)->first();
                    });
                } else {
                    $SubCategory = Cache::rememberForever('SubCategory:'.$subsubcategory->sub_category_id, function () use ($subsubcategory) {
                            return  \App\SubCategory::find($subsubcategory->sub_category_id);
                    });
					$Category = Cache::rememberForever('Category:'.$SubCategory->category_id, function () use ($SubCategory) {
                            return  \App\Category::where('id', $SubCategory->category_id)->first();
                    });
                }
				?>
                <li><a href="<?php echo e(route('products.subsubcategory', [$Category->slug, $SubCategory->slug, $subsubcategory->slug])); ?>"><?php echo e(__(ucwords(strtolower($subsubcategory->name)))); ?> in <?php echo e(__(ucwords(strtolower($Category->name)))); ?></a></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>
</div>
<div class="product">
    <?php if(count($products) > 0): ?>
        <div class="title"><?php echo e(__('Products')); ?></div>
        <ul>
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <a href="<?php echo e(route('product', $product->slug)); ?>">
                        <div class="d-flex search-product align-items-center">
                            <?php if($product->thumbnail_img != null): ?>
                            <div class="image" style="background-image:url('<?php echo e(asset($product->thumbnail_img)); ?>');">
                            <?php else: ?>
							<div class="image" style="background-image:url('<?php echo e(asset('frontend/images/product-thumb.png')); ?>');">
							<?php endif; ?>
                            </div>
                            <div class="w-100 overflow--hidden">
                                <div class="product-name text-truncate">
                                    <?php echo e(__($product->name)); ?> 
                                    <?php if($product->salt != ""): ?>
                                     <br /><span style="font-size:12px"> (<?php echo e($product->salt); ?>) </span>
                                    <?php endif; ?>
                                </div>
                                <?php 
                                        $home_base_price = home_base_price($product->id);
                                        $home_discounted_base_price = home_discounted_base_price($product->id);
                                        if($home_base_price == "₹ 0.00") {
                                            $product->current_stock = 0;
                                            $qty = 0;
                                        } else {
                                            $qty = 100;
                                        }
                                        $discounted_per = discount_calulate($product->id,$home_base_price, $home_discounted_base_price);
                                        ?>
                                        
                                <div class="clearfix">
                                    <div class="price-box float-left">
                                        <?php if($qty == 0): ?>
                                        <span class="product-price strong-600"></span>
                                        <?php else: ?>
                                        <?php if($home_base_price != $home_discounted_base_price): ?>
                                            <del class="old-product-price strong-400"><?php echo e($home_base_price); ?></del>
                                        <?php endif; ?>
                                        <span class="product-price strong-600"><?php echo e($home_discounted_base_price); ?></span>
                                        
                                        <?php if($home_base_price != $home_discounted_base_price): ?>
                                            <span class="product-price strong-200" style="font-size:14px; color:red">&nbsp; <?php echo e($discounted_per); ?>% off</span>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>
</div>
<div class="category">
    <?php if(count($brands) > 0): ?>
        <div class="title "><?php echo e(__('Brand Suggestions')); ?></div>
        <ul>
            <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><a href="<?php echo e(route('categories.all', ['brand'=>$brand->slug])); ?>" ><?php echo e(__($brand->name)); ?></a></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>
</div>
<?php if(\App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1): ?>
    <div class="product">
        <?php if(count($shops) > 0): ?>
            <div class="title"><?php echo e(__('Shops')); ?></div>
            <ul>
                <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                        <a href="<?php echo e(route('shop.visit', $shop->slug)); ?>">
                            <div class="d-flex search-product align-items-center">
                                <div class="image" style="background-image:url('<?php echo e(asset($shop->logo)); ?>');">
                                </div>
                                <div class="w-100 overflow--hidden ">
                                    <div class="product-name text-truncate heading-6 strong-600">
                                        <?php echo e($shop->name); ?>


                                        <div class="stock-box d-inline-block">
                                            <?php if($shop->user->seller->verification_status == 1): ?>
                                                <span class="ml-2"><i class="fa fa-check-circle" style="color:green"></i></span>
                                            <?php else: ?>
                                                <span class="ml-2"><i class="fa fa-times-circle" style="color:red"></i></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="price-box alpha-6">
                                        <?php echo e($shop->address); ?>

                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        <?php endif; ?>
    </div>
<?php endif; ?>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/partials/search_content.blade.php ENDPATH**/ ?>