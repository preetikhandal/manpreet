<?php $__env->startSection('content'); ?>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo e(__('Product Bulk Remove')); ?></h3>
        </div>
        <div class="panel-body">
            <div class="alert" style="color: #004085;background-color: #cce5ff;border-color: #b8daff;margin-bottom:0;margin-top:10px;">
                <strong>Step 1:</strong>
                <p>1. Download the skeleton file and fill it with proper data.</p>
                <p>2. You can download the example file to understand how the data must be filled.</p>
                <p>3. Once you have downloaded and filled the skeleton file, upload it in the form below and submit.</p>
            </div>
            <br>
            <div class="">
                <a href="<?php echo e(asset('download/product_bulk_remove.xlsx')); ?>" download><button class="btn btn-primary">Download Bulk Remove CSV</button></a>
            </div>
            <br>
        </div>
    </div>

    <div class="panel">
        <div class="panel-heading">
            <h1 class="panel-title"><strong><?php echo e(__('Upload Product File')); ?></strong></h1>
        </div>
        <div class="panel-body">
           <form class="form-horizontal" id="MyForm" action="<?php echo e(route('bulk_remove_view')); ?>" method="POST" enctype="multipart/form-data" onsubmit="return showmodal()">
                <?php echo csrf_field(); ?>
                <div class="form-group">
                    <input type="file" class="form-control" name="bulk_file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <button class="btn btn-primary" type="submit"><?php echo e(__('Upload CSV')); ?></button>
                    </div>
                </div>
            </form>
			<div class="progressDiv">
			</div>
			<div class="ajax_success"></div>
			
			<div class="progress" style="display:none">
			  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div id="success"></div>
		</div>
    </div>
    
	<!-- The Modal -->
	<div class="modal" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<!-- Modal body -->
				<div class="modal-body">
				    <center>
					 <i class="fa fa-spin fa-spinner fa-4x"></i>
					 <br />
					 Processing...
					 </center>
				</div>
			</div>
		</div>
	</div>
	
	<script>
	function showmodal() {
	    $('#myModal').modal('show');
	    return true;
	}
	</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/products/bulk_remove.blade.php ENDPATH**/ ?>