<?php $__env->startSection('content'); ?>
 <style>
    .countdown .countdown-digit{
        background:#282563!important;
        color:#fff!important;
        font-weight:600;
    }
    .aldeproductslider .list-product{ margin: 0px 0px 5px 0px; }
</style>
<div <?php if($mobileapp == 1): ?> style="margin-top:75px" <?php endif; ?>>
    <?php if($flash_deal->status == 1 && strtotime(date('d-m-Y')) <= $flash_deal->end_date): ?>
        <div style="background-color:<?php echo e($flash_deal->background_color); ?>" >
            <?php if($flash_deal->banner != null): ?>
            <section class="text-center">
                <img src="<?php echo e(asset($flash_deal->banner)); ?>" alt="<?php echo e($flash_deal->title); ?>" class="img-fit w-100">
            </section>
            <?php endif; ?>
            <section class="pb-4">
                <div class="container ">
                    <div class="text-center text-<?php echo e($flash_deal->text_color); ?>">
                        <h1 class="h3 py-3"><?php echo e($flash_deal->title); ?> </h1>
                        <!--<?php echo e(date('m/d/Y', $flash_deal->end_date)); ?> -->
                        <div class="pb-3 countdown countdown-sm countdown--style-1" data-countdown-date="<?php echo e(date('m/d/Y', $flash_deal->end_date)); ?>" data-countdown-label="show"></div>
                    </div>
                    <div class="row sm-no-gutters gutters-5 aldeproductslider deepscustomrow">
                        <?php $__currentLoopData = $flash_deal->flash_deal_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $flash_deal_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php 
                                $product = \App\Product::find($flash_deal_product->product_id);
                                $home_base_price = home_base_price($product->id);
                			    $home_discounted_base_price = home_discounted_base_price($product->id);
                            ?>
                            <?php if($product->published != 0): ?>
                                <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                    <article class="list-product">
            							<div class="img-block">
            								<a href="<?php echo e(route('product', $product->slug)); ?>" class="thumbnail">
            								    <?php if($product->flash_deal_img != null): ?>
            									<img class="mx-auto d-block lazyload" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" data-src="<?php echo e(asset($product->flash_deal_img)); ?>" alt="<?php echo e(__($product->name)); ?>" />
            									<?php else: ?>
            								    <img class="mx-auto d-block lazyload" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" alt="<?php echo e(__($product->name)); ?>">
            									<?php endif; ?>
            								</a>
            							</div>
            							<?php if($home_base_price != $home_discounted_base_price): ?>
            							<ul class="product-flag">
            								<li class="new discount-price bg-orange" style="background-color: #282563!importan;"><?php echo e(discount_calulate($flash_deal_product->product_id,$home_base_price, $home_discounted_base_price )); ?>% off</li>
            							</ul>
            							<?php endif; ?>
            							<div class="product-decs text-center">
            								<h2><a href="<?php echo e(route('product', $product->slug)); ?>" class="product-link"><?php echo Str::limit($product->name, 58, ' ...'); ?></a></h2>
            								<div class="pricing-meta">
            									<ul>
            									    <?php if($home_base_price != $home_discounted_base_price): ?>
            									    	<li class="old-price"><?php echo e($home_base_price); ?></li>
            										<?php endif; ?>
            										    <li class="current-price"><?php echo e($home_discounted_base_price); ?></li>
            									</ul>
            								</div>
            							</div>
            							<div class="add-to-cart-buttons">
            							    <?php if($product->current_stock == 0): ?>
            							    <button class="btn btn95" type="button" disabled> Out of Stock</button>
            							    <?php else: ?>
            							    <button class="btn btn25" onclick="addToCart(this, <?php echo e($product->id); ?>, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
            							    <button class="btn btn70" onclick="buyNow(this, <?php echo e($product->id); ?>, 1, 'full')" type="button"> Buy Now</button>
            							    <?php endif; ?>
            							</div>
            						</article>
        						</div>
    						 <?php endif; ?>
    					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    				</div>
                    
                    
                    
                </div>
            </section>
        </div>
    <?php elseif($flash_deal->status == 1 && ($flash_deal->start_date==0 || $flash_deal->end_date==0)): ?>
	<div style="background-color:<?php echo e($flash_deal->background_color); ?>">
	    
        <?php if($flash_deal->banner != null): ?>
        <section class="text-center">
            <img src="<?php echo e(asset($flash_deal->banner)); ?>" alt="<?php echo e($flash_deal->title); ?>" class="img-fit w-100">
        </section>
        <?php endif; ?>
        <section class="pb-4">
            <div class="container">
                <div class="text-center my-4 text-<?php echo e($flash_deal->text_color); ?>">
                    <h1 class="h3"><?php echo e($flash_deal->title); ?></h1>
                </div>
                <div class="gutters-5 row aldeproductslider">
                    <?php $__currentLoopData = $flash_deal->flash_deal_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $flash_deal_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                            $product = \App\Product::find($flash_deal_product->product_id);
                            $home_base_price = home_base_price($product->id);
            			    $home_discounted_base_price = home_discounted_base_price($product->id);
                        ?> 
                        <?php if($product->published != 0): ?>
                            <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                <article class="list-product">
        							<div class="img-block">
        								<a href="<?php echo e(route('product', $product->slug)); ?>" class="thumbnail">
        								    <?php if($product->flash_deal_img != null): ?>
        									<img class="mx-auto d-block lazyload" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" data-src="<?php echo e(asset($product->flash_deal_img)); ?>" alt="<?php echo e(__($product->name)); ?>" />
        									<?php else: ?>
        								    <img class="mx-auto d-block lazyload" src="<?php echo e(asset('frontend/images/product-thumb.png')); ?>" alt="<?php echo e(__($product->name)); ?>">
        									<?php endif; ?>
        								</a>
        							</div>
        							<?php if($home_base_price != $home_discounted_base_price): ?>
        							<ul class="product-flag">
        								<li class="new discount-price bg-success"><?php echo e(discount_calulate($flash_deal_product->product_id,$home_base_price, $home_discounted_base_price )); ?>% off</li>
        							</ul>
        							<?php endif; ?>
        							<div class="product-decs text-center">
        								<h2><a href="<?php echo e(route('product', $product->slug)); ?>" class="product-link"><?php echo Str::limit($product->name, 58, ' ...'); ?></a></h2>
        								<div class="pricing-meta">
        									<ul>
        									    <?php if($home_base_price != $home_discounted_base_price): ?>
        									    	<li class="old-price"><?php echo e($home_base_price); ?></li>
        										<?php endif; ?>
        										    <li class="current-price"><?php echo e($home_discounted_base_price); ?></li>
        									</ul>
        								</div>
        							</div>
        							<div class="add-to-cart-buttons">
        							    <?php if($product->current_stock == 0): ?>
        							    <button class="btn btn95" type="button" disabled> Out of Stock</button>
        							    <?php else: ?>
        							    <button class="btn btn25" onclick="addToCart(this, <?php echo e($product->id); ?>, 1, 'sm')" type="button"><i class="fa fa-cart-plus"></i></button>
        							    <button class="btn btn70" onclick="buyNow(this, <?php echo e($product->id); ?>, 1, 'full')" type="button"> Buy Now</button>
        							    <?php endif; ?>
        							</div>
        						</article>
    						</div>
                            
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </section>
    </div>
    <?php else: ?>
        <div style="background-color:<?php echo e($flash_deal->background_color); ?>">
            <?php if($flash_deal->banner != null): ?>
                <section class="text-center pt-3">
                    <div class="container ">
                        <img src="<?php echo e(asset($flash_deal->banner)); ?>" alt="<?php echo e($flash_deal->title); ?>" class="img-fit">
                    </div>
                </section>
            <?php endif; ?>
            <section class="pb-4">
                <div class="container">
                    <div class="text-center text-<?php echo e($flash_deal->text_color); ?>">
                        <h1 class="h3 my-4"><?php echo e($flash_deal->title); ?></h1>
                        <p class="h4"><?php echo e(__('This offer has been expired.')); ?></p>
                    </div>
                </div>
            </section>
        </div>
    <?php endif; ?>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/flash_deal_details.blade.php ENDPATH**/ ?>