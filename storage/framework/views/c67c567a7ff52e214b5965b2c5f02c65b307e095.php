<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-sm-12">
            <a href="<?php echo e(route('city.create')); ?>" class="btn btn-rounded btn-info pull-right"><?php echo e(__('Add New City')); ?></a>
        </div>
    </div>

    <br>

    <!-- Basic Data Tables -->
    <!--===================================================-->
   <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all h-100">
            <h3 class="panel-title pull-left pad-no"><?php echo e(__('City')); ?></h3>
            <div class="pull-right clearfix">
                <form class="" id="sort_sellers" action="" method="GET">
                    <div class="box-inline pad-rgt pull-left">
                        <div class="" style="min-width: 200px;">
                            <input type="text" class="form-control" id="search" name="search"<?php if(isset($sort_search)): ?> value="<?php echo e($sort_search); ?>" <?php endif; ?> placeholder="Type City Name">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo e(__('Alignbook State Parent Id')); ?></th>
                    <th><?php echo e(__('State Name')); ?></th>
                    <th><?php echo e(__('City Name')); ?></th>
                    <th><?php echo e(__('Alignbook City Id')); ?></th>
                    <th><?php echo e(__('Code')); ?></th>
                    
                    
                </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $citys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $citysList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php
                          $statteName = App\State::find($citysList->state_id);
                          ?>
                        <tr>
                            <td><?php echo e($key+1); ?></td>
                            <td><?php echo e($citysList->parent_id); ?></td>
                            <td><?php echo e($statteName->name); ?></td>
                            <td><?php echo e($citysList->name); ?></td>
                            <td><?php echo e($citysList->city_id); ?></td>
                            <td><?php echo e($citysList->code); ?></td>
                        </td>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                
            </table>
            
        </div>
    </div>
        
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
  
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/City/index.blade.php ENDPATH**/ ?>