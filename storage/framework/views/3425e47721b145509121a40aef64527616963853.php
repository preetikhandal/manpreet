<?php $__env->startSection('content'); ?>
        <?php if($errors->any()): ?>
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e($error); ?></li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		<?php endif; ?>
<div class="col-lg-12 col-lg-offset-3" style="margin-left: 2px;">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo e(__('Seller Information')); ?></h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="<?php echo e(route('sellers.store')); ?>" method="POST" enctype="multipart/form-data">
        	<?php echo csrf_field(); ?>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name"><?php echo e(__('Name')); ?></label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="<?php echo e(__('Name')); ?>" id="name" value="<?php echo e(old('name')); ?>" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="email"><?php echo e(__('Email Address')); ?></label>
                    <div class="col-sm-9">
                        <input type="email" placeholder="<?php echo e(__('Email Address')); ?>" id="email" value="<?php echo e(old('email')); ?>" onkeyup="checkEmail()" name="email" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="password"><?php echo e(__('Password')); ?></label>
                    <div class="col-sm-9">
                        <input type="password" placeholder="<?php echo e(__('Password')); ?>" id="password" name="password" class="form-control" required>
                    </div>
                </div>
                
            </div>
            <script>
                
        function checkEmail(){
            var email = $('#email').val();
           alert(email);
            if(email.length > 3){
                 $.ajax({
                 url:'<?php echo e(route('checkemail')); ?>',
                 method:'POST',
                 data:{_token:'<?php echo e(@csrf_token()); ?>','email':email},
                 async:false,
                 success:function(data){
                    // location.reload();
                 }
             });
            }else{
                alert('Please enter reason for denied order');
            }
            
            
            
        }
            </script>
            
            <!--Seller shop information -->
                  <div class="panel-body">
                 <center><strong>Shop Info</strong></center><br />
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="password"><?php echo e(__('Shop Name ')); ?></label>
                    <div class="col-sm-9">
                       <input type="text" class="form-control mb-3" value="<?php echo e(old('Shop Name')); ?>" placeholder="<?php echo e(__('Shop Name')); ?>" name="name" >
                    </div>
                </div>
                <div class="form-group">
                <div class="col-md-2 text-right">
                    <label><?php echo e(__('Contact Perosn Name')); ?> <span class="required-star">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Contact Perosn Name')); ?>" name="contact_person_name" value="<?php echo e(old('contact_person_name')); ?>" >
                </div>
                <div class="col-md-2 text-right">
                    <label class="text-right"><?php echo e(__('Contact Perosn Phone')); ?> <span class="required-star">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="tel" class="form-control mb-3" placeholder="<?php echo e(__('Contact Perosn Phone')); ?>" name="contact_person_phone" value="<?php echo e(old('contact_person_phone')); ?>"  onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="10" maxlength="10">
                </div>
                </div>
                
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label><?php echo e(__('Address')); ?> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Address')); ?>" name="address" value="<?php echo e(old('address')); ?>">
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label><?php echo e(__('Landmark')); ?> <span class="required-star"></span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Landmark')); ?>" name="landmark"  value="<?php echo e(old('landmark')); ?>">
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label><?php echo e(__('City')); ?> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('City')); ?>" name="city" value="<?php echo e(old('city')); ?>" >
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label><?php echo e(__('State')); ?> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('State')); ?>" name="state" value="<?php echo e(old('state')); ?>">
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label><?php echo e(__('Pincode')); ?> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="tel" class="form-control mb-3" placeholder="<?php echo e(__('Pincode')); ?>" name="pincode" value="<?php echo e(old('pincode')); ?>" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" minlength="6" maxlength="6">
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label><?php echo e(__('Firm Type')); ?> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Firm Type')); ?>" name="firm_type" value="<?php echo e(old('firm_type')); ?>">
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label><?php echo e(__('GSTIN')); ?> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('GSTIN')); ?>" name="gstin" value="<?php echo e(old('gstin')); ?>">
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label><?php echo e(__('License')); ?> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Business License')); ?>" name="trade_license_no" value="<?php echo e(old('trade_license_no')); ?>">
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <label><?php echo e(__('Business Registered In')); ?> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mb-3" placeholder="<?php echo e(__('Business Registered In')); ?>" name="business_registration_in" value="<?php echo e(old('business_registration_in')); ?>">
                                        </div>
                                    </div>
                                    
                                    
                                   
            </div>
            
                <!--Een Seller shop information-->
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit"><?php echo e(__('Save')); ?></button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/sellers/create.blade.php ENDPATH**/ ?>