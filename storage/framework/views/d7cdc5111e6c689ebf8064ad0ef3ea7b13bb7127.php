<?php $__env->startSection('content'); ?>
<!-- Seller Style -->
<link type="text/css" href="<?php echo e(asset('frontend/css/seller.css')); ?>" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" <?php if($mobileapp == 1): ?> style="margin-top:75px" <?php endif; ?>>
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    <?php if(Auth::user()->user_type == 'seller'): ?>
                        <?php echo $__env->make('frontend.inc.seller_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php elseif(Auth::user()->user_type == 'customer'): ?>
                        <?php echo $__env->make('frontend.inc.customer_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title bg-blue p-3" style="border-radius: 0.25rem;">
                            <div class="row align-items-center">
                                <div class="col-12 d-flex align-items-center">
                                    <h2 class="heading heading-6 text-white p-0 text-capitalize strong-600 mb-0">
                                        <?php echo e(__('My Cashback')); ?>

                                    </h2>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="dashboard-widget text-center green-widget text-white mt-4 c-pointer">
                                    <i class="fa fa-rupee"></i>
                                    <span class="d-block title heading-3 strong-400"><?php echo e(single_price($totalCashback)); ?></span>
                                    <span class="d-block sub-title"><?php echo e(__('Cashback Amount')); ?></span>

                                </div>
                            </div>
                            

                        </div>
                        
                        <div class="card no-border mt-5" style="border:1px solid #282563!important">
                            <div class="card-header py-3 bg-blue">
                                <h4 class="mb-0 h6 text-white"><?php echo e(__('Cashback History')); ?></h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-responsive-md mb-0">
                                    <thead>
                                        <tr>
                                            
                                            <th><?php echo e(__('Date')); ?></th>
                                            <th><?php echo e(__('Amount')); ?></th>
                                            <th><?php echo e(__('Payment Method')); ?></th>
                                            <th><?php echo e(__('Status')); ?></th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(count($Cashbacks) > 0): ?>
                                            <?php $__currentLoopData = $Cashbacks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $Cashback): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    
                                                    <td><?php echo e(date('d-m-Y', strtotime($Cashback->created_at))); ?></td>
                                                    <td><?php echo e(single_price($Cashback->cashback_amount)); ?></td>
                                                    <td>Cashback</td>
                                                    <td><?php echo e($Cashback->status); ?></td>
                                                   
                                                    
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                            <tr>
                                                <td class="text-center pt-5 h4" colspan="100%">
                                                    <i class="la la-meh-o d-block heading-1 alpha-5"></i>
                                                <span class="d-block"><?php echo e(__('No history found.')); ?></span>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                       
                         
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="wallet_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5"><?php echo e(__('Recharge Wallet')); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="" action="<?php echo e(route('wallet.recharge')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body gry-bg px-3 pt-3">
                        <div class="row">
                            <div class="col-md-2">
                                <label><?php echo e(__('Amount')); ?> <span class="required-star">*</span></label>
                            </div>
                            <div class="col-md-10">
                                <input type="number" class="form-control mb-3" name="amount" placeholder="<?php echo e(__('Amount')); ?>" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label><?php echo e(__('Payment Method')); ?></label>
                            </div>
                            <div class="col-md-10">
                                <div class="mb-3" style="border:1px solid #e6e6e6">
                                    <select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="payment_option">
                                        <?php if(\App\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1): ?>
                                            <option value="paypal"><?php echo e(__('Paypal')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'stripe_payment')->first()->value == 1): ?>
                                            <option value="stripe"><?php echo e(__('Stripe')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'sslcommerz_payment')->first()->value == 1): ?>
                                            <option value="sslcommerz"><?php echo e(__('SSLCommerz')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'instamojo_payment')->first()->value == 1): ?>
                                            <option value="instamojo"><?php echo e(__('Instamojo')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'paystack')->first()->value == 1): ?>
                                            <option value="paystack"><?php echo e(__('Paystack')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'voguepay')->first()->value == 1): ?>
                                            <option value="voguepay"><?php echo e(__('VoguePay')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\BusinessSetting::where('type', 'razorpay')->first()->value == 1): ?>
                                            <option value="razorpay"><?php echo e(__('Razorpay')); ?></option>
                                        <?php endif; ?>
                                        <?php if(\App\Addon::where('unique_identifier', 'paytm')->first() != null && \App\Addon::where('unique_identifier', 'paytm')->first()->activated): ?>
                                            <option value="paytm"><?php echo e(__('Paytm')); ?></option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-base-1"><?php echo e(__('Confirm')); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="offline_wallet_recharge_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5"><?php echo e(__('Offline Recharge Wallet')); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="offline_wallet_recharge_modal_body"></div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        function show_wallet_modal(){
            $('#wallet_modal').modal('show');
        }

        function show_make_wallet_recharge_modal(){
            $.post('<?php echo e(route('offline_wallet_recharge_modal')); ?>', {_token:'<?php echo e(csrf_token()); ?>'}, function(data){
                $('#offline_wallet_recharge_modal_body').html(data);
                $('#offline_wallet_recharge_modal').modal('show');
            });
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/cashback.blade.php ENDPATH**/ ?>