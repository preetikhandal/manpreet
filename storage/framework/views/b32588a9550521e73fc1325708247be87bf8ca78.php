<?php
	$data=\App\Policy::where('name', 'privacy_policy')->first();
?>

<?php $__env->startSection('meta_title'); ?><?php echo e($data->meta_title); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('meta_description'); ?><?php echo e($data->description); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <section class="gry-bg py-4">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="p-4 bg-white">
                        <?php
                            echo $data->content;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/policies/privacypolicy.blade.php ENDPATH**/ ?>