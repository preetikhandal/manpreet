<!-- FOOTER -->

<footer id="footer" class="footer ">
    <div class="footer-top pb-0 pt-0">
        <div class="container-fluid">
            <div class="row mb-4 " style="border-bottom:1px solid #eee;padding:1rem 0">
				<div class="col">
					<h4 class="heading heading-xs strong-600 text-uppercase">
						<?php echo e(__('Shop by Category')); ?></h4>
					<ul class="list-inline">
						<?php $__currentLoopData = \App\Category::orderby('position')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
                            <?php $brands = array(); ?>
							<li class="list-inline-item pb-2">
								<a href="<?php echo e(route('products.category', $category->slug)); ?>" class="footercategories" >
                                    <span class="cat-name"><?php echo e(__($category->name)); ?></span>
                                </a>
							</li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</ul>
				</div>
			</div>
            
            <div class="row cols-xs-space cols-sm-space cols-md-space pb-2">
                <?php
                    $generalsetting = \App\GeneralSetting::first();
                ?>
                <div class="col-md-2 col-6 mb-0">
					<div class="col text-left ">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
                            <?php echo e(__('Contact Info')); ?>

                        </h4>
                        <ul class="footer-links contact-widget">
                            <li>
                               <span class="d-block heading strong-600"><?php echo e(__('Address')); ?>:</span>
                               <span class="d-block"><?php echo e($generalsetting->address); ?></span>
                            </li>
                            <li>
								<span class="d-block heading strong-600"><?php echo e(__('Phone')); ?>:</span>
                               <span class="d-block"><a href="tel:<?php echo e($generalsetting->phone); ?>"><?php echo e($generalsetting->phone); ?></a></span>
                            </li>
                            <li>
								<span class="d-block heading strong-600"><?php echo e(__('Email')); ?>:</span>
                               <span class="d-block">
                                   <a href="mailto:<?php echo e($generalsetting->email); ?>"><?php echo e($generalsetting->email); ?></a>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
				<div class="col-6 col-md-2 text-left mb-0">
                    <div class="col">
						<h4 class="heading heading-xs strong-600 text-uppercase mb-2">
                          <?php echo e(__('Important Links')); ?>

                       </h4>
						<ul class="footer-links">
						    <li><a href="<?php echo e(url('cashback-condition')); ?>" title=""><?php echo e(__('Cashback Condition')); ?></a></li>
							<li><a href="<?php echo e(route('terms')); ?>" title=""><?php echo e(__('Terms')); ?></a></li>
							<li><a href="<?php echo e(route('privacypolicy')); ?>" title=""><?php echo e(__('Privacy Policy')); ?></a></li>
							<li><a href="<?php echo e(route('sellerpolicy')); ?>" title=""><?php echo e(__('Seller Policy')); ?></a></li>
							<li><a href="<?php echo e(route('returnpolicy')); ?>" title=""><?php echo e(__('Return Policy')); ?></a></li>
							<li><a href="<?php echo e(route('supportpolicy')); ?>" title=""><?php echo e(__('Support Policy')); ?></a></li>
						</ul>
					</div>
					<div class="col text-left p-0 d-block d-md-none mt-4">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
						   <?php echo e(__('Company Overview')); ?>

                        </h4>
                        <ul class="footer-links rightSide">
							<li><a href="<?php echo e(route('blog')); ?>" title="Blog"><?php echo e(__('Blog')); ?></a></li>
							<li><a href="<?php echo e(route('feedback-form')); ?>" title="Blog" class="btn btn-base-1 feedbackBtn btn-icon-left bg-blue"><?php echo e(__('Feedback')); ?></a></li>
							<li><a href="<?php echo e(route('pharmacy-enquiry')); ?>" title="Blog" class="btn btn-base-1 feedbackBtn btn-icon-left bg-blue"><?php echo e(__('Pharmacy')); ?></a></li>
                        </ul>
                    </div>
                </div>
				<div class="col-6 col-md-2 d-none d-md-block">
                    <div class="col text-center text-md-left">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
						   <?php echo e(__('Company Overview')); ?>

                        </h4>
                        <ul class="footer-links">
							<!--<li><a href="<?php echo e(url('about-us')); ?>" title="About Us"><?php echo e(__('About Us')); ?></a></li>
							<li><a href="<?php echo e(url('career-opportunities')); ?>" title="We are Hiring"><?php echo e(__('We are Hiring')); ?></a></li>
							<li><a href="<?php echo e(url('contact-us')); ?>" title="Contact Us"><?php echo e(__('Contact Us')); ?></a></li> -->
							<li><a href="<?php echo e(route('blog')); ?>" title="Blog"><?php echo e(__('Blog')); ?></a></li>
							<li><a style="padding: 5px 10px;
    color: white;
    background: black;
    border: 1px solid black;" href="<?php echo e(route('feedback-form')); ?>" class="btn btn-base-1 btn-icon-left bg-blue" title="feedback"><?php echo e(__('Feedback')); ?></a></li>
	<li><a style="padding: 5px 10px;
    color: white;
    background: black;
    border: 1px solid black;margin-top: 9px;" href="<?php echo e(route('pharmacy-enquiry')); ?>" class="btn btn-base-1 btn-icon-left bg-blue" title="feedback"><?php echo e(__('Pharmacy')); ?></a></li>
                        </ul>
                    </div>
                </div>
				<div class="col-md-2 col-lg-2 col-4">
                    <div class="col text-left p-0">
                       <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
                          <?php echo e(__('My Account')); ?>

                       </h4>
                       <ul class="footer-links">
                            <?php if(Auth::check()): ?>
                                <li>
                                    <a href="<?php echo e(route('logout')); ?>" title="Logout">
                                        <?php echo e(__('Logout')); ?>

                                    </a>
                                </li>
                            <?php else: ?>
                                <li>
                                    <a href="<?php echo e(route('user.login')); ?>" title="Login">
                                        <?php echo e(__('Login')); ?>

                                    </a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a href="<?php echo e(route('purchase_history.index')); ?>" title="Order History">
                                    <?php echo e(__('Order History')); ?>

                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('wishlists.index')); ?>" title="My Wishlist">
                                    <?php echo e(__('My Wishlist')); ?>

                                </a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('orders.track')); ?>" title="Track Order">
                                    <?php echo e(__('Track Order')); ?>

                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php if(\App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1): ?>
                        <div class="col d-none d-md-block">
                            <div class="mt-4">
                                <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
                                    <?php echo e(__('Be a Seller')); ?>

                                </h4>
                                <a href="<?php echo e(route('shops.create')); ?>" class="btn btn-base-1 btn-icon-left bg-blue">
                                    <?php echo e(__('Apply Now')); ?>

                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
				<div class="col-md-4 col-lg-4 col-8">
					<div class="row">
						<div class="col-md-6">
							<div class="col text-center text-md-left p-0">
								<h4 class="heading heading-xs strong-600 text-uppercase text-left mb-2"><?php echo e(__('Connect')); ?></h4>
								<ul class="my-3 my-md-0 social-nav deepssocial model-2">
									<?php if($generalsetting->facebook != null): ?>
										<li>
											<a href="<?php echo e($generalsetting->facebook); ?>" target="_blank" class="fa fa-facebook"></a>
										</li>
									<?php endif; ?>
									<?php if($generalsetting->instagram != null): ?>
										<li>
											<a href="<?php echo e($generalsetting->instagram); ?>" target="_blank"><img  src="<?php echo e(asset('frontend/img/Insta icons.png')); ?>" alt="instragram"></a>
										</li>
									<?php endif; ?>
									<?php if($generalsetting->twitter != null): ?>
										<li>
											<a href="<?php echo e($generalsetting->twitter); ?>" target="_blank" class="fa fa-twitter"></a>
										</li>
									<?php endif; ?>
									<?php if($generalsetting->youtube != null): ?>
										<li>
											<a href="<?php echo e($generalsetting->youtube); ?>" class="fa fa-youtube" style="background: red" target="_blank"></a>
										</li>
									<?php endif; ?>
								</ul>
								<ul class="inline-links">
									<?php if(\App\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1): ?>
										<li>
											<img loading="lazy" alt="paypal" src="<?php echo e(asset('frontend/images/icons/cards/paypal.png')); ?>" height="20">
										</li>
									<?php endif; ?>
									<?php if(\App\BusinessSetting::where('type', 'stripe_payment')->first()->value == 1): ?>
										<li>
											<img loading="lazy" alt="stripe" src="<?php echo e(asset('frontend/images/icons/cards/stripe.png')); ?>" height="20">
										</li>
									<?php endif; ?>
									<?php if(\App\BusinessSetting::where('type', 'sslcommerz_payment')->first()->value == 1): ?>
										<li>
											<img loading="lazy" alt="sslcommerz" src="<?php echo e(asset('frontend/images/icons/cards/sslcommerz.png')); ?>" height="20">
										</li>
									<?php endif; ?>
									<?php if(\App\BusinessSetting::where('type', 'instamojo_payment')->first()->value == 1): ?>
										<li>
											<img loading="lazy" alt="instamojo" src="<?php echo e(asset('frontend/images/icons/cards/instamojo.png')); ?>" height="20">
										</li>
									<?php endif; ?>
									<?php if(\App\BusinessSetting::where('type', 'razorpay')->first()->value == 1): ?>
										<li>
											<img loading="lazy" alt="razorpay" src="<?php echo e(asset('frontend/images/icons/cards/rozarpay.png')); ?>" style="height: 35px">
										</li>
									<?php endif; ?>
									<?php if(\App\BusinessSetting::where('type', 'paystack')->first()->value == 1): ?>
										<li>
											<img loading="lazy" alt="paystack" src="<?php echo e(asset('frontend/images/icons/cards/paystack.png')); ?>" height="20">
										</li>
									<?php endif; ?>
									<?php if(\App\BusinessSetting::where('type', 'cash_payment')->first()->value == 1): ?>
										<li>
											<img loading="lazy" alt="cash on delivery" src="<?php echo e(asset('frontend/images/icons/cards/cod.png')); ?>" style="height: 35px">
										</li>
									<?php endif; ?>
									<?php if(\App\Addon::where('unique_identifier', 'offline_payment')->first() != null && \App\Addon::where('unique_identifier', 'offline_payment')->first()->activated): ?>
										<?php $__currentLoopData = \App\ManualPaymentMethod::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $method): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										  <li>
											<img loading="lazy" alt="<?php echo e($method->heading); ?>" src="<?php echo e(asset($method->photo)); ?>" height="20">
										</li>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php endif; ?>
								</ul>
							</div>
						</div>
						<div class="col-md-6" <?php if($mobileapp == 1): ?> style="display:none" <?php endif; ?>>
							<div class="col text-center">
								<h4 class="heading heading-xs strong-600 text-uppercase mb-2"><?php echo e(__('Download App')); ?></h4>
								 <ul class="footer-links">
									 <li>
										<a href="https://play.google.com/store/apps/details?id=com.aldeb.aldebazaar" target="_blank">
											<img src="<?php echo e(asset('frontend/images/icons/playstore.png')); ?>" class="img-fluid mx-auto" style="width:136px; height: 41px;">
										</a>
									</li>
									<li>
										<a href="https://apps.apple.com/app/id1533227336" target="_blank">
											<img src="<?php echo e(asset('frontend/images/icons/applestore.png')); ?>" class="img-fluid mx-auto" style="width:136px; height: 41px;">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
            </div>
			<!--<div class="line mt-4 mb-4" style="border-top: 1px solid #d4d3cd;"></div>-->
			<!--<div class="row cols-xs-space cols-sm-space cols-md-space text-center text-md-left deeps_auth-bar mt-4">
				<div class="col-md-4">
					<div class="row align-items-center no-gutters">
						<div class="col-md-4 text-center">
							<img src="<?php echo e(asset('frontend/images/icons/secure-rebrand_x6f8yq.svg')); ?>" class="img-fluid img lazyload">
						</div>
						<div class="info col-md-8">
							<h5 class="font18">Reliable</h5><p class="font12 text-gray">All products displayed on ALDE BAZAAR are procured from verified and licensed pharmacies. All labs listed on the platform are accredited</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row align-items-center no-gutters">
						<div class="col-md-4 text-center">
							<img src="<?php echo e(asset('frontend/images/icons/reliable-rebrand_rcpof3.svg')); ?>" class="img-fluid img lazyload">
						</div>
						<div class="info col-md-8">
							<h5 class="font18">Secure</h5><p class="font12 text-gray">ALDE BAZAAR uses Secure Sockets Layer (SSL) 128-bit encryption and is Payment Card Industry Data Security Standard (PCI DSS) compliant</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row align-items-center no-gutters">
						<div class="col-md-4 text-center">
							<img src="<?php echo e(asset('frontend/images/icons/affordable-rebrand_ivgidq.svg')); ?>" class="img-fluid img lazyload">
						</div>
						<div class="info col-md-8">
							<h5 class="font18">Affordable</h5><p class="font12 text-gray">Find affordable medicine substitutes, save up to 50% on health products, up to 80% off on lab tests and free doctor consultations.</p>
						</div>
					</div>
				</div>
			</div>-->
        </div>
    </div>

    <div class="footer-bottom py-2 sct-color-3">
        <div class="container-fluid">
            <div class="row row-cols-xs-spaced flex flex-items-xs-middle">
				<div class="col">
					<div class="copyright text-center">
                        <ul class="copy-links no-margin">
                            <li>
                                © <?php echo e(date('Y')); ?> <?php echo e($generalsetting->site_name); ?>

                            </li>
						</ul>
					</div>
				</div>
            </div>
        </div>
    </div>
</footer>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/inc/footer.blade.php ENDPATH**/ ?>