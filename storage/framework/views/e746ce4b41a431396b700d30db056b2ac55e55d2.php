
<div class="header bg-white d-none d-lg-block">
	<?php
		if (Cache::has('generalsetting')){
		   $generalsetting =  Cache::get('generalsetting');
		} else {
			$generalsetting = \App\GeneralSetting::first();
			Cache::forever('generalsetting', $generalsetting);
		}
	?>
    <div class="position-relative logo-bar-area">
        <div class="">
            <div class="container-fluid py-2" >
                <div class="row no-gutters align-items-center">
                    <div class="col-lg-2 col-8">
                        <div class="d-flex">
                            <!-- Brand/Logo -->
                            <a class="navbar-brand w-100 " href="<?php echo e(route('home')); ?>">
                                <?php if($generalsetting->logo != null): ?>
                                    <img src="<?php echo e(asset($generalsetting->logo)); ?>" alt="<?php echo e(env('APP_NAME')); ?>">
                                <?php else: ?>
                                    <img src="<?php echo e(asset('frontend/images/logo/logo.png')); ?>" alt="<?php echo e(env('APP_NAME')); ?>">
                                <?php endif; ?>
                            </a>
                            <?php if(Route::currentRouteName() != 'home' && Route::currentRouteName() != 'categories.all'): ?>
                                <!--<div class="d-none d-xl-block category-menu-icon-box">
                                    <div class="dropdown-toggle navbar-light category-menu-icon" id="category-menu-icon">
                                        <span class="navbar-toggler-icon"></span>
                                    </div>
                                </div>-->
                            <?php endif; ?>
                        </div>
                    </div>
                    <script>
                      
                        function searchSubmit() {
                            searchCategory = $('#searchCategory').val();
                           
                            if(searchCategory != "") {
                               formaction = $('#searchForm').prop('action');
                               formaction = formaction+"/"+searchCategory;
                               $('#searchForm').prop('action',formaction);
                               $('#searchCategory').prop('disabled','disabled');
                               return true;
                            } else {
                                return true;
                            }
                        }
                    </script>
                    <style>
                        .logo-bar-area .select2-container--default.select2-container--disabled .select2-selection--single {
                            background-color: #eee;
                            opacity: 1;
                            }
                    </style>
                    <div class="col-lg-10 col-4 position-static">
                        <div class="row d-flex w-100">
                            <div class="search-box flex-grow-1 px-3">
                                <form id="searchForm" action="<?php echo e(route('categories.all')); ?>" method="GET" onsubmit="return searchSubmit();">
                                    <div class="d-flex position-relative">
                                        <div class="d-lg-none search-box-back">
                                            <button class="" type="button"><i class="la la-long-arrow-left"></i></button>
                                        </div>
                                        <div class="w-100">
                                            <input type="text" aria-label="Search" id="search" name="q" class="w-100" placeholder="<?php echo e(__('I am shopping for...')); ?>" <?php if(isset($q)): ?> value="<?php echo e($q); ?>" <?php endif; ?> autocomplete="off">
                                        </div>
                                        <div class="form-group category-select d-none d-xl-block">
                                            <select class="form-control selectpicker" id="searchCategory" name="category">
                                                <option value=""><?php echo e(__('All Categories')); ?></option>
												<?php
													if (Cache::has('Categories')){
													   $Categories =  Cache::get('Categories');
													} else {
													$Categories = \App\Category::orderby('position')->get();
														Cache::forever('Categories', $Categories);
													}
												?>
                                                <?php $__currentLoopData = $Categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($category->display == 1): ?>
                                                    <option value="<?php echo e($category->slug); ?>"
                                                        <?php if(isset($category_id)): ?>
                                                            <?php if($category_id == $category->id): ?>
                                                                selected
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                        ><?php echo e(__($category->name)); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <button class="d-none d-lg-block bg-orange" type="submit" style="border:1px solid #ff0909">
                                            <i class="la la-search la-flip-horizontal"  style="color:#eee;;font-size: 19px;"></i>
                                        </button>
                                        <div class="typed-search-box d-none">
                                            <div class="search-preloader">
                                                <div class="loader"><div></div><div></div><div></div></div>
                                            </div>
                                            <div class="search-nothing d-none">

                                            </div>
                                            <div id="search-content" style="max-height:400px;overflow-y: scroll;">

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="logo-bar-icons d-inline-block ml-auto">
                                <div class="d-inline-block d-lg-none">
                                    <div class="nav-search-box">
                                        <a href="#" class="nav-box-link">
                                            <i class="la la-search la-flip-horizontal d-inline-block nav-box-icon"></i>
                                        </a>
                                    </div>
                                </div>
								<div class="d-none d-lg-inline-block">
                                    <div class="nav-wishlist-box">
                                        <a href="tel:<?php echo e($generalsetting->phone); ?>" class="nav-box-link">
                                            <i class="fa fa-phone-square d-inline-block nav-box-icon" ></i>
                                            <span class="nav-box-text d-none d-xl-inline-block" style="font-weight:600" > <?php echo e($generalsetting->phone); ?></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="d-none d-lg-inline-block">
                                    <div class="nav-wishlist-box" id="wishlist">
                                        <a href="<?php echo e(route('wishlists.index')); ?>" class="nav-box-link">
                                            <i class="la la-heart-o d-inline-block nav-box-icon" style="font-size:20px"></i>
                                            <span class="nav-box-text d-none d-xl-inline-block" style="font-weight:600"><?php echo e(__('Wishlist')); ?></span>
                                            <?php if(Auth::check()): ?>
                                                <span class="nav-box-number bg-orange" ><?php echo e(count(Auth::user()->wishlists)); ?></span>
                                            <?php else: ?>
                                                <span class="nav-box-number bg-orange">0</span>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                </div>
								<div class="d-none d-lg-inline-block" data-hover="dropdown">
                                    <div class="nav-wishlist-box dropdown" id="user_items">
										<?php if(auth()->guard()->check()): ?>
											<a href="<?php echo e(route('user.login')); ?>" class="nav-box-link dropdown-toggle" data-toggle="dropdown">
												<i class="la la-user d-inline-block nav-box-icon" style="font-size:20px"></i>
												<span class="nav-box-text d-none d-xl-inline-block"><?php echo e(__('Hello,')); ?> <?php if(isset(auth()->user()->name)): ?>
														<?php 
															$explodename= explode(" ",auth()->user()->name)
														?>
												<?php echo e($explodename[0]); ?> <?php endif; ?></span>
											</a>
											<ul class="dropdown-menu dropdown-menu-right px-0">
                                            <li>
                                                <div class="dropdown-cart px-0" style="min-width:150px">
													<div class="dropdown-cart-items c-scrollbar">
														<div class="dc-item" style="padding:5px 5px;">
															<div class="d-flex align-items-center">
																<div class="dc-content">
																	<span class="d-block dc-product-name text-capitalize mb-1">
																		<a href="<?php echo e(route('dashboard')); ?>" class="top-bar-item"><?php echo e(__('My Account')); ?></a>
																	</span>
																</div>
															</div>
														</div>
														<div class="dc-item" style="padding:5px 5px;">
															<div class="d-flex align-items-center">
																<div class="dc-content">
																	<span class="d-block dc-product-name text-capitalize mb-1">
																		<a href="<?php echo e(route('wishlists.index')); ?>" class="top-bar-item"><?php echo e(__('Wishlist')); ?></a>
																	</span>
																</div>
															</div>
														</div>
														<div class="dc-item" style="padding:5px 5px;">
															<div class="d-flex align-items-center">
																<div class="dc-content">
																	<span class="d-block dc-product-name text-capitalize mb-1">
																		<a href="<?php echo e(route('orders.track')); ?>" class="top-bar-item"><?php echo e(__('Track Order')); ?></a>
																	</span>
																</div>
															</div>
														</div>
														<div class="dc-item" style="padding:5px 5px;">
															<div class="d-flex align-items-center">
																<div class="dc-content">
																	<span class="d-block dc-product-name text-capitalize mb-1">
																		<a href="<?php echo e(route('logout')); ?>" class="top-bar-item"><?php echo e(__('Logout')); ?></a>
																	</span>
																</div>
															</div>
														</div>
														
													</div>
												</div>
											</li>
                                        </ul>
										<?php else: ?>
											<a href="<?php echo e(route('user.login')); ?>" class="nav-box-link">
												<i class="la la-user d-inline-block nav-box-icon" style="font-size:20px"></i>
												<span class="nav-box-text d-none d-xl-inline-block" style="font-weight:600"><?php echo e(__('Login')); ?></span>
											</a>
										 <?php endif; ?>
                                    </div>
                                </div>
                                <div class="d-inline-block" data-hover="dropdown">
                                    <div class="nav-cart-box dropdown cart_items" id="cart_items">
                                        <a href="" class="nav-box-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="la la-shopping-cart d-inline-block nav-box-icon" style="top:6px;"></i>
                                            <span class="nav-box-text d-none d-xl-inline-block" style="font-weight:600"><?php echo e(__('Cart')); ?></span>
                                            <?php if(Session::has('cart')): ?>
                                                <span class="nav-box-number" style="top:-4px;"><?php echo e(count(Session::get('cart'))); ?></span>
                                            <?php else: ?>
                                                <span class="nav-box-number bg-orange" style="top: -4px;">0</span>
                                            <?php endif; ?>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right px-0">
                                            <li>
                                                <div class="dropdown-cart px-0">
                                                    <?php if(Session::has('cart')): ?>
                                                        <?php if(count($cart = Session::get('cart')) > 0): ?>
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700"><?php echo e(__('Cart Items')); ?></h3>
                                                            </div>
                                                            <div class="dropdown-cart-items c-scrollbar">
                                                                <?php
                                                                    $total = 0;
                                                                    $cartIteamPrice=0;
                                                                    $getPercent = 0;
                                                                   
                                                                ?>
                                                                
                                                                <?php $__currentLoopData = $cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cartItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                
                                                                    <?php
                                                                        $product = \App\Product::find($cartItem['id']);
                                                                        if($product == null) {
                                                                            $cart1 = Session::get('cart', collect([]));
                                                                            $cart1->forget($key);
                                                                            Session::put('cart', $cart1);
                                                                            continue;
                                                                        }
                                                                   
                                                                    $categoryId = $product->category_id;
                                                            	    $subcategoryId = $product->subcategory_id;
                                                                    $subsubcategory = $product->subsubcategory_id;
                                                                                                                        if(isset($product->user_id)){
                                                                	    $sellerId = $product->user_id;
                                                                	}else{
                                                                	    $sellerId = 0;
                                                                	}
                                                                    if(!empty($sellerId)){
                                                                            if(!empty($categoryId) && !empty($subcategoryId) && !empty($subsubcategory) && !empty($sellerId)){
                                                                            $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'subsubcategory_id'=>$subsubcategory,'user_id'=>$sellerId])->first();
                                                                        }else if(!empty($categoryId) && !empty($subcategoryId) && !empty($sellerId)){
                                                                                $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'subcategory_id'=>$subcategoryId,'user_id'=>$sellerId])->first();
                                                                        }else if(!empty($categoryId) && !empty($sellerId)){
                                                                             $get_subcategory =  \App\SellerCommision::where(['category_id'=> $categoryId,'user_id'=>$sellerId])->first();
                                                                        }    
                                                                    }


                                                                    
                                                                    if(isset($get_subcategory->commission) && $get_subcategory->commission_type==1){
                                                                        $exculsivePercent = $get_subcategory->commission;
                                                                         
                                                                    }else{
                                                                        $exculsivePercent = 0; 
                                                                    }
                                                                     
                                                                     if(!empty($exculsivePercent)){
                                                                          $getPercent = ($product->unit_price*$exculsivePercent)/100;
                                                                          $cartIteamPrice = $cartItem['price'];
                                                                    }else{
                                                                       $cartIteamPrice = $cartItem['price'];
                                                                    }
                                                                    $total = $total + $cartIteamPrice*$cartItem['quantity'];
                                                                    ?>
                                                                    <div class="dc-item">
                                                                        <div class="d-flex align-items-center">
                                                                            <div class="dc-image">
                                                                                <a href="<?php echo e(route('product', $product->slug)); ?>">
                                                                                    <?php if(file_exists($product->thumbnail_img)): ?>
                                                                    					<img src="<?php echo e(asset('frontend/images/product-thumb.jpg')); ?>" data-src="<?php echo e(asset($product->thumbnail_img)); ?>" class="img-fluid lazyload" alt="<?php echo e(__($product->name)); ?>">
                                                                    				<?php else: ?>
                                                                    					<img src="<?php echo e(asset('frontend/images/product-thumb.jpg')); ?>" class="img-fluid"/>
                                                                    				<?php endif; ?>
                                                                                </a>
                                                                            </div>
                                                                            <div class="dc-content">
                                                                                <span class="d-block dc-product-name text-capitalize strong-600 mb-1">
                                                                                    <a href="<?php echo e(route('product', $product->slug)); ?>">
                                                                                        <?php echo e(__(ucwords(strtolower($product->name)))); ?>

                                                                                    </a>
                                                                                </span>

                                                                                <span class="dc-quantity">x<?php echo e($cartItem['quantity']); ?></span>
                                                                                <span class="dc-price"><?php echo e(single_price($cartIteamPrice*$cartItem['quantity'])); ?></span>
                                                                            </div>
                                                                            <div class="dc-actions">
                                                                                <button onclick="removeFromCart(<?php echo e($key); ?>)">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </div>
                                                            <div class="dc-item py-3 bg-gray">
                                                                <span class="subtotal-text"><?php echo e(__('Subtotal')); ?></span>
                                                                <span class="subtotal-amount"><?php echo e(single_price($total)); ?></span>
                                                            </div>
                                                            <div class="py-2 text-center dc-btn">
                                                                <ul class="inline-links inline-links--style-3">
                                                                    <li class="px-1">
                                                                        <a href="<?php echo e(route('cart')); ?>" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1">
                                                                            <i class="la la-shopping-cart"></i> <?php echo e(__('View cart')); ?>

                                                                        </a>
                                                                    </li>
                                                                    <?php if(Auth::check()): ?>
                                                                    <li class="px-1">
                                                                        <a href="<?php echo e(route('checkout.shipping_info')); ?>" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1 light-text">
                                                                            <i class="la la-mail-forward"></i> <?php echo e(__('Checkout')); ?>

                                                                        </a>
                                                                    </li>
                                                                    <?php endif; ?>
                                                                </ul>
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700"><?php echo e(__('Your Cart is empty')); ?></h3>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        <div class="dc-header">
                                                            <h3 class="heading heading-6 strong-700"><?php echo e(__('Your Cart is empty')); ?></h3>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hover-category-menu" id="hover-category-menu">
            <div class="container-fluid">
                <div class="row no-gutters position-relative">
                    <div class="col-lg-3 position-static">
                        <div class="category-sidebar" id="category-sidebar">
                            <div class="all-category">
                                <span><?php echo e(__('CATEGORIES')); ?></span>
                                <a href="<?php echo e(route('categories.all')); ?>" class="d-inline-block">See All ></a>
                            </div>
                            <ul class="categories">
                                <?php $__currentLoopData = \App\Category::all()->take(11); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($category->display == 1): ?>
                                    <?php
                                        $brands = array();
                                    ?>
                                    <li class="category-nav-element" data-id="<?php echo e($category->id); ?>">
                                        <a href="<?php echo e(route('products.category', $category->slug)); ?>">
											<?php if(!empty($category->icon)): ?>
												<img class="cat-image lazyload" src="<?php echo e(asset('frontend/images/placeholder.jpg')); ?>" data-src="<?php echo e(asset($category->icon)); ?>" width="30" alt="<?php echo e(__($category->name)); ?>">
											<?php endif; ?>
                                            <span class="cat-name"><?php echo e(__($category->name)); ?></span>
                                        </a>
                                        <?php if(count($category->subcategories)>0): ?>
                                            <div class="sub-cat-menu c-scrollbar">
                                                <div class="c-preloader">
                                                    <i class="fa fa-spin fa-spinner"></i>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </li>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Navbar -->

    <!--<div class="main-nav-area d-none d-lg-block">
        <nav class="navbar navbar-expand-lg navbar--bold navbar--style-2 navbar-light bg-default">
            <div class="container">
                <div class="collapse navbar-collapse align-items-center justify-content-center" id="navbar_main">
                    <ul class="navbar-nav">
                        <?php $__currentLoopData = \App\Search::orderBy('count', 'desc')->get()->take(5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $search): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo e(route('suggestion.search', $search->query)); ?>"><?php echo e($search->query); ?></a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div> -->
	<div class="main-nav-area d-none d-lg-block">
        <nav class="navbar navbar-expand-lg navbar--bold navbar--style-2 navbar-light">
            <div class="container-fluid">
                <div class="collapse navbar-collapse align-items-center justify-content-center" id="navbar_main">
                    <ul class="navbar-nav mx-auto">
						<li class="nav-item dropdown">
						
							<a class="nav-link d-none d-md-block"  href="<?php echo e(route('categories.all')); ?>" id="navbarDropdown" > <?php echo e(__('All Categories')); ?> <i class="fa fa-angle-down"></i> </a>
							<a class="nav-link d-block d-md-none" style="color: #fff;" href="<?php echo e(route('categories.all.mobile')); ?>" id="navbarDropdown" > <?php echo e(__('All Categories')); ?> <i class="fa fa-angle-down"></i> </a>
							
							<ul class="dropdown-menu " aria-labelledby="navbarDropdown">
								<?php $__currentLoopData = \App\Category::orderby('position')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								    <?php if($category->display == 1): ?>
									<?php $brands = array(); ?> 
									<li <?php if(count($category->subcategories)>0): ?> class='has-submenu'  <?php endif; ?>>
                                    <?php if( $category->name== 'Pharmacy'): ?>
										<a class="dropdown-item" href="<?php echo e(route('pharmacy-enquiry')); ?>">
                                        <?php else: ?>
                                        <a class="dropdown-item" href="<?php echo e(route('products.category', $category->slug)); ?>">
										<?php endif; ?>
                                        <?php echo e(__(strtolower($category->name))); ?>

										<?php if(count($category->subcategories)>0): ?> <i class="fa fa-angle-right"></i> <?php endif; ?> </a>
										<?php if(count($category->subcategories)>0): ?>
											<?php if(!empty($category->menu_banner)): ?>
											<div class="megasubmenu dropdown-menu" style="background:url('<?php echo e(asset($category->menu_banner)); ?>') right bottom no-repeat #fff; width:600px">
											<?php else: ?>
											<div class="megasubmenu dropdown-menu" style="background:#fff; width:600px">
											<?php endif; ?>
                                            <?php if( $category->name!= 'Pharmacy'): ?>
												<h5 class=""><?php echo e(__($category->name)); ?> </h5>
                                                 
												<ul class="list-unstyled">
													<?php $__currentLoopData = \App\SubCategory::where('category_id',$category->id)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													    <?php if($subcategory->display == 1): ?>
														<li><a class="nav-link"href="<?php echo e(route('products.subcategory', [$category->slug, $subcategory->slug])); ?>"><?php echo e(__(strtolower($subcategory->name))); ?> </a></li>
													    <?php endif; ?>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</ul>
                                            <?php endif; ?>

											</div>
										<?php endif; ?>
									</li>
									<?php endif; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo e(url('brands')); ?>"><?php echo e(__('Shop by Brands')); ?></a>
						</li>
					    <li class="nav-item">
                        	<?php
                        		$link=\App\Category::where('id',"35")->first();
                        		
                        	?>
                        	<a class="nav-link" href="<?php echo e(url('category/'.$link->slug)); ?>"><?php echo e(__('City Haat')); ?></a>
                        </li>
			    		<?php
							if (Cache::has('menu')){
							   $menu =  Cache::get('menu');
							} else {
								 $menu = \App\Menu::where('published',1)->orderBy('position')->get();
								Cache::forever('menu', $menu);
							}
						?>

						<?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
							<li class="nav-item">
								<a class="nav-link" href="<?php if($menu->link!='#'): ?> <?php echo e(url($menu->link)); ?> <?php else: ?> javascript:void(0) <?php endif; ?>"><?php echo e($menu->name); ?></a>
							</li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<!---Only For Small Devices--->
<div class="header bg-white d-block d-lg-none">
    <!-- mobile menu -->
    <div class="mobile-side-menu d-lg-none">
        <div class="side-menu-overlay opacity-0" onclick="sideMenuClose()"></div>
        <div class="side-menu-wrap opacity-0" style="z-index: 99999;">
            <div class="side-menu closed">
                <div class="side-menu-header " style="background:#131921">
                    <div class="side-menu-close py-1" onclick="sideMenuClose()">
                        <i class="la la-close"></i>
                    </div>
                    <?php if(auth()->guard()->check()): ?>
                        <div class="widget-profile-box px-2 py-4 d-flex align-items-center">
                            <?php if(Auth::user()->avatar_original != null): ?>
                                <!--<div class="image " style="background-image:url('<?php echo e(asset(Auth::user()->avatar_original)); ?>')"></div>-->
                            <?php else: ?>
                                <!--<div class="image " style="background-image:url('<?php echo e(asset('frontend/images/user.png')); ?>')"></div>-->
                            <?php endif; ?>
                            <div class="name"><i class="fa fa-user" aria-hidden="true" style="font-size:17px;"></i> Hello, <?php echo e(Auth::user()->name); ?>

                            <a href="<?php echo e(route('profile')); ?>" style="color:#fff;display: block;font-weight: 400;padding-top: 10px;padding-bottom: 0px;margin-bottom: 0;">Manage Profile</a>
                            </div>
                            
                        </div>
                        <!--<div class="side-login px-3 pb-3">
                            <a href="<?php echo e(route('logout')); ?>"><?php echo e(__('Sign Out')); ?></a>
                        </div>-->
                    <?php else: ?>
                        <!--<div class="widget-profile-box px-3 py-4 d-flex align-items-center">
                                <div class="image " style="background-image:url('<?php echo e(asset('frontend/images/icons/user-placeholder.jpg')); ?>')"></div>
                        </div>-->   
                        <div class="side-login px-3 py-3">
							<i class="fa fa-user" aria-hidden="true" style="font-size:17px;"></i> Hello,
                            <a href="<?php echo e(route('user.login')); ?>"><?php echo e(__('Login & Signup')); ?></a>
                            <!-- <a href="<?php echo e(route('user.registration')); ?>"><?php echo e(__('Register')); ?></a>-->
                        </div>
                    <?php endif; ?>
                </div>
                <div class="side-menu-list">
                    <ul class="side-user-menu">
                        <li>
                            <a href="<?php echo e(route('home')); ?>">
                                <!--<i class="la la-home"></i>-->
                                <span><?php echo e(__('Home')); ?></span>
                            </a>
                        </li>
						<div class="border-top my-1"></div>
						<li>
                            <a href="<?php echo e(url('categories')); ?>">
                                <span><?php echo e(__('Shop by Category')); ?></span>
                            </a>
                        </li>
                        <li>
                        	<a href="<?php echo e(url('brands')); ?>">
                        		<span><?php echo e(__('Shop by Brands')); ?></span>
                        	</a>
                        </li>
                        <li>
                        	<a href="<?php echo e(url('category/'.$link->slug)); ?>">
                        		<span><?php echo e(__('City Haat')); ?></span>
                        	</a>
                        </li>
						<?php $__currentLoopData = \App\Menu::where('published',1)->orderBy('position')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
							<li>
								<a href="<?php echo e($menu->link); ?>">
									<span><?php echo e($menu->name); ?></span>
								</a>
							</li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<div class="border-top my-1"></div>
					   <li>
                            <a href="<?php echo e(route('dashboard')); ?>">
                                <!--<i class="la la-dashboard"></i>-->
                                <span><?php echo e(__('My Account')); ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo e(route('purchase_history.index')); ?>">
                                <!--<i class="la la-file-text"></i>-->
                                <span><?php echo e(__('My Orders')); ?></span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="<?php echo e(route('cart')); ?>">
                                <!--<i class="la la-shopping-cart"></i>-->
                                <span><?php echo e(__('Cart')); ?></span>
                                <?php if(Session::has('cart')): ?>
                                    <span class="badge" id="cart_items_sidenav"><?php echo e(count(Session::get('cart'))); ?></span>
                                <?php else: ?>
                                    <span class="badge" id="cart_items_sidenav">0</span>
                                <?php endif; ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('wishlists.index')); ?>">
                                <!--<i class="la la-heart-o"></i>-->
                                <span><?php echo e(__('Wishlist')); ?></span>
                            </a>
                        </li>

                        <?php if(\App\BusinessSetting::where('type', 'wallet_system')->first()->value == 1): ?>
                            <li>
                                <a href="<?php echo e(route('wallet.index')); ?>">
                                    <!--<i class="la la-inr"></i>-->
                                    <span><?php echo e(__('My Wallet')); ?></span>
                                </a>
                            </li>
                        <?php endif; ?>
                         <li>
                        <a href="<?php echo e(route('cashback.index')); ?>" class="<?php echo e(areActiveRoutesHome(['cashback.index'])); ?>">
                            
                            <span class="category-name">
                                <?php echo e(__('My Cashback')); ?>

                            </span>
                        </a>
                    </li>
                     
                        <li>
                            <a href="<?php echo e(route('support_ticket.index')); ?>" class="<?php echo e(areActiveRoutesHome(['support_ticket.index', 'support_ticket.show'])); ?>">
                                <!--<i class="la la-support"></i>-->
                                <span class="category-name">
                                    <?php echo e(__('Support Ticket')); ?>

                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#shareitemmodal" data-toggle="modal">
                                <!--<i class="la la-support"></i>-->
                                <span class="category-name">
                                    <?php echo e(__('Share & Refer')); ?>

                                </span>
                            </a>
                        </li>
						<div class="border-top my-1"></div>
                    </ul>
                   
					<?php if(Auth::check() && Auth::user()->user_type == 'seller'): ?>
                        <div class="sidebar-widget-title py-0">
                            <span><?php echo e(__('Shop Options')); ?></span>
                        </div>
                        <ul class="side-seller-menu">
                            <li>
                                <a href="<?php echo e(route('seller.products')); ?>">
                                    <!--<i class="la la-diamond"></i>-->
                                    <span><?php echo e(__('Products')); ?></span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo e(url('ordersTabular')); ?>">
                                    <!--<i class="la la-file-text"></i>-->
                                    <span><?php echo e(__('Orders')); ?></span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo e(route('shops.index')); ?>">
                                    <!--<i class="la la-cog"></i>-->
                                    <span><?php echo e(__('Shop Setting')); ?></span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo e(route('withdraw_requests.index')); ?>">
                                    <!--<i class="la la-money"></i>-->
                                    <span>
                                        <?php echo e(__('Money Withdraw')); ?>

                                    </span>
                                </a>
                            </li>

                            <?php
                                $conversation = \App\Conversation::where('receiver_id', Auth::user()->id)->where('receiver_viewed', '1')->get();
                            ?>
                            <?php if(\App\BusinessSetting::where('type', 'conversation_system')->first()->value == 1): ?>
                                <li>
                                    <a href="<?php echo e(route('conversations.index')); ?>" class="<?php echo e(areActiveRoutesHome(['conversations.index', 'conversations.show'])); ?>">
                                        <!--<i class="la la-comment"></i>-->
                                        <span class="category-name">
                                            <?php echo e(__('Conversations')); ?>

                                            <?php if(count($conversation) > 0): ?>
                                                <span class="ml-2" style="color:green"><strong>(<?php echo e(count($conversation)); ?>)</strong></span>
                                            <?php endif; ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <li>
                                <a href="<?php echo e(route('payments.index')); ?>">
                                    <!--<i class="la la-cc-mastercard"></i>-->
                                    <span><?php echo e(__('Payment History')); ?></span>
                                </a>
                            </li>
                        </ul>
                        <div class="sidebar-widget-title py-0">
                            <span><?php echo e(__('Earnings')); ?></span>
                        </div>
                        <div class="widget-balance py-3">
                            <div class="text-center">
                                <div class="heading-4 strong-700 mb-4">
                                    <?php
                                        $orderDetails = \App\OrderDetail::where('seller_id', Auth::user()->id)->where('created_at', '>=', \Carbon\Carbon::parse()->subDays(30)->toDateTimeString())->get();
                                        $total = 0;
                                        foreach ($orderDetails as $key => $orderDetail) {
                                            if($orderDetail->order != null && $orderDetail->order != null && $orderDetail->order->payment_status == 'paid'){
                                                $total += $orderDetail->price;
                                            }
                                        }
                                    ?>
                                    <small class="d-block text-sm alpha-5 mb-2"><?php echo e(__('Your earnings (current month)')); ?></small>
                                    <span class="p-2 bg-base-1 rounded"><?php echo e(single_price($total)); ?></span>
                                </div>
                                <table class="text-left mb-0 table w-75 m-auto">
                                    <tbody>
                                        <tr>
                                            <?php
                                                $orderDetails = \App\OrderDetail::where('seller_id', Auth::user()->id)->get();
                                                $total = 0;
                                                foreach ($orderDetails as $key => $orderDetail) {
                                                    if($orderDetail->order != null && $orderDetail->order->payment_status == 'paid'){
                                                        $total += $orderDetail->price;
                                                    }
                                                }
                                            ?>
                                            <td class="p-1 text-sm">
                                                <?php echo e(__('Total earnings')); ?>:
                                            </td>
                                            <td class="p-1">
                                                <?php echo e(single_price($total)); ?>

                                            </td>
                                        </tr>
                                        <tr>
                                            <?php
                                                $orderDetails = \App\OrderDetail::where('seller_id', Auth::user()->id)->where('created_at', '>=', \Carbon\Carbon::parse()->subDays(60)->toDateTimeString())->where('created_at', '<=', \Carbon\Carbon::parse()->subDays(30)->toDateTimeString())->get();
                                                $total = 0;
                                                foreach ($orderDetails as $key => $orderDetail) {
                                                    if($orderDetail->order != null && $orderDetail->order->payment_status == 'paid'){
                                                        $total += $orderDetail->price;
                                                    }
                                                }
                                            ?>
                                            <td class="p-1 text-sm">
                                                <?php echo e(__('Last Month earnings')); ?>:
                                            </td>
                                            <td class="p-1">
                                                <?php echo e(single_price($total)); ?>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
					 <div class="sidebar-widget-title py-0">
						<span><?php echo e(__('Contact Us')); ?></span>
					</div>
					<div class="py-3 px-3">
						<span class="mail_txt"><span class="mail_txt">Customer Care : <a href="tel:<?php echo e($generalsetting->phone); ?>" target="_blank" rel="noopener"><?php echo e($generalsetting->phone); ?></a><br></span></span>
						<span class="mail_txt"><span class="mail_txt">Email : <a href="mailto:<?php echo e($generalsetting->email); ?>" target="_blank" rel="noopener"><?php echo e($generalsetting->email); ?></a><br></span></span>
						
						
						<!--<p style="margin: 0px; padding: 0px 0px 8px; font-size: 13px;">If you encounter any bugs, glitches, lack of functionality, delayed deliveries, billing errors or other problems on the beta website, please email us on <a href="mailto:<?php echo e($generalsetting->email); ?>" style="color: #008ecc;"><?php echo e($generalsetting->email); ?></a></p>-->
					</div>	
					<div class="sidebar-widget-title py-0">
						<span><?php echo e(__('Download App')); ?></span>
					</div>
					<div class="py-3 px-3">	
						<div class="row">
							<div class="col-6 p-2">
								<a href="https://play.google.com/store/apps/details?id=com.aldeb.aldebazaar" target="_blank" rel="noopener"><img src="<?php echo e(asset('frontend/images/icons/playstore.png')); ?>" alt="Download Alde App for Android from Play Store" class="img-fluid mx-auto" style="height: 41px;"></a>
							</div>
							<div class="col-6 p-2">
								<a href="https://apps.apple.com/app/id1533227336" target="_blank" rel="noopener"><img src="<?php echo e(asset('frontend/images/icons/applestore.png')); ?>" alt="Download Alde App for iOs from App Store" class="img-fluid mx-auto" style="height: 41px;"></a>
							</div>
						</div>
						
					</div>
		   </div>
        </div>
    </div>
    <!-- end mobile menu -->

    <div class="position-relative logo-bar-area  <?php if($mobileapp == 1): ?>  sm-fixed-top <?php endif; ?>">
        <div class="">
            <div class="container-fluid" style="max-width:100%!important;">
                <?php if($mobileapp != 1): ?>
                <div class="row no-gutters align-items-center" <?php if($mobileapp == 1): ?> style="display:none" <?php endif; ?>>
                    <div class="col-lg-2 col-8">
                        <div class="d-flex">
                            <div class="d-block d-lg-none mobile-menu-icon-box">
                                <!-- Navbar toggler  -->
                                <a href="" onclick="sideMenuOpen(this)">
                                    <div class="hamburger-icon">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                            </div>

							<a class="navbar-brand w-100 " href="<?php echo e(route('home')); ?>" <?php if($mobileapp == 1): ?> style="display:none" <?php endif; ?>>
                                <?php
                                    $generalsetting = \App\GeneralSetting::first();
                                ?>
                                <?php if($generalsetting->logo != null): ?>
                                    
									<img src="<?php echo e(asset($generalsetting->mobile_logo)); ?>" alt="<?php echo e(env('APP_NAME')); ?>">
                                <?php else: ?>
                                    <img src="<?php echo e(asset('frontend/images/logo/logo.png')); ?>" alt="<?php echo e(env('APP_NAME')); ?>">
                                <?php endif; ?>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-10 col-4 position-static" <?php if($mobileapp == 1): ?> style="display:none" <?php endif; ?>>
                        <div class="row d-flex w-100">
                            <div class="logo-bar-icons d-inline-block ml-auto">
								<div class="d-inline-block">
                                    <div class="nav-wishlist-box" id="wishlist">
										<?php if(auth()->guard()->check()): ?>
											<a href="<?php echo e(route('logout')); ?>" class="nav-box-link">
												<span class="nav-box-text" style="font-size:13px;color:#fff"><?php echo e(__('Logout')); ?></span>
											</a>
										<?php else: ?>
											<a href="<?php echo e(route('user.login')); ?>" class="nav-box-link">
												<span class="nav-box-text" style="font-size:13px;color:#fff"><?php echo e(__('Sign In')); ?></span>
											</a>
										<?php endif; ?>
                                    </div>
                                </div>
                                <div class="d-inline-block" data-hover="dropdown">
                                    <div class="nav-cart-box dropdown cart_items" id="cart_items">
                                        <a href="" class="nav-box-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="la la-shopping-cart d-inline-block nav-box-icon"></i>
                                            <span class="nav-box-text d-none d-xl-inline-block"><?php echo e(__('Cart')); ?></span>
                                            <?php if(Session::has('cart')): ?>
                                                <span class="nav-box-number bg-orange" style="color:#fff"><?php echo e(count(Session::get('cart'))); ?></span>
                                            <?php else: ?>
                                                <span class="nav-box-number bg-orange" style="color:#fff">0</span>
                                            <?php endif; ?>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right px-0">
                                            <li>
                                                <div class="dropdown-cart px-0">
                                                    <?php if(Session::has('cart')): ?>
                                                        <?php if(count($cart = Session::get('cart')) > 0): ?>
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700"><?php echo e(__('Cart Items')); ?></h3>
                                                            </div>
                                                            <div class="dropdown-cart-items c-scrollbar">
                                                                <?php
                                                                    $total = 0;
                                                                ?>
                                                                <?php $__currentLoopData = $cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cartItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php
                                                                        $product = \App\Product::find($cartItem['id']);
                                                                        $total = $total + $cartItem['price']*$cartItem['quantity'];
                                                                    ?>
                                                                    <div class="dc-item">
                                                                        <div class="d-flex align-items-center">
                                                                            <div class="dc-image">
                                                                                <a href="<?php echo e(route('product', $product->slug)); ?>">
                                                                                    <img src="<?php echo e(asset('frontend/images/placeholder.jpg')); ?>" data-src="<?php echo e(asset($product->thumbnail_img)); ?>" class="img-fluid lazyload" alt="<?php echo e(__($product->name)); ?>">
                                                                                </a>
                                                                            </div>
                                                                            <div class="dc-content">
                                                                                <span class="d-block dc-product-name text-capitalize strong-600 mb-1">
                                                                                    <a href="<?php echo e(route('product', $product->slug)); ?>">
                                                                                        <?php echo e(__($product->name)); ?>

                                                                                    </a>
                                                                                </span>

                                                                                <span class="dc-quantity">x<?php echo e($cartItem['quantity']); ?></span>
                                                                                <span class="dc-price"><?php echo e(single_price($cartItem['price']*$cartItem['quantity'])); ?></span>
                                                                            </div>
                                                                            <div class="dc-actions">
                                                                                <button onclick="removeFromCart(<?php echo e($key); ?>)">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </div>
                                                            <div class="dc-item py-3">
                                                                <span class="subtotal-text"><?php echo e(__('Subtotal')); ?></span>
                                                                <span class="subtotal-amount"><?php echo e(single_price($total)); ?></span>
                                                            </div>
                                                            <div class="py-2 text-center dc-btn">
                                                                <ul class="inline-links inline-links--style-3">
                                                                    <li class="px-1">
                                                                        <a href="<?php echo e(route('cart')); ?>" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1">
                                                                            <i class="la la-shopping-cart"></i> <?php echo e(__('View cart')); ?>

                                                                        </a>
                                                                    </li>
                                                                    <?php if(Auth::check()): ?>
                                                                    <li class="px-1">
                                                                        <a href="<?php echo e(route('checkout.shipping_info')); ?>" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1 light-text">
                                                                            <i class="la la-mail-forward"></i> <?php echo e(__('Checkout')); ?>

                                                                        </a>
                                                                    </li>
                                                                    <?php endif; ?>
                                                                </ul>
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700"><?php echo e(__('Your Cart is empty')); ?></h3>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        <div class="dc-header">
                                                            <h3 class="heading heading-6 strong-700"><?php echo e(__('Your Cart is empty')); ?></h3>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			    <?php else: ?>
					<div class="row no-gutters align-items-center">
						<div class="col-12">
							<br/>
						</div>
					</div>
				<?php endif; ?>
				<div class="row no-gutters align-items-center">
					 <div class="col-12">
						<form action="<?php echo e(route('categories.all')); ?>" method="GET">
							<div class="input-group mt-3 mb-1">
								<input type="text" id="search1" name="q"  class="form-control" placeholder="<?php echo e(__('I am shopping for...')); ?>" autocomplete="off" style="border-top-left-radius: 8px;border-bottom-left-radius: 8px;">
								<div class="input-group-append">
									<button class="btn bg-orange" style="border-top-right-radius: 8px;border-bottom-right-radius: 8px;" type="submit"><i class="la la-search la-flip-horizontal d-inline-block nav-box-icon mt-1 text-white" ></i></button>
								</div>
								<div class="typed-search-box d-none">
									<div class="search-preloader">
										<div class="loader"><div></div><div></div><div></div></div>
									</div>
									<div class="search-nothing d-none">

									</div>
									<div id="search-content1" style="max-height:400px;overflow-y: scroll;">

									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
        <div class="hover-category-menu" id="hover-category-menu">
            <div class="container">
                <div class="row no-gutters position-relative">
                    <div class="col-lg-3 position-static">
                        <div class="category-sidebar" id="category-sidebar">
                            <div class="all-category">
                                <span><?php echo e(__('CATEGORIES lll')); ?></span>
                                <a href="<?php echo e(route('categories.all')); ?>" class="d-inline-block">See All ></a>
                            </div>
                            <ul class="categories">
                                <?php $__currentLoopData = \App\Category::all()->take(11); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($category->display == 1): ?>
                                    <?php
                                        $brands = array();
                                    ?>
                                    <li class="category-nav-element" data-id="<?php echo e($category->id); ?>">
                                    <?php if( $category->name== 'Pharmacy'): ?>
				
                                        <a href="<?php echo e(route('pharmacy-enquiry')); ?>">
                                    <?php else: ?>
                                    <a href="<?php echo e(route('products.category', $category->slug)); ?>">
                                    <?php endif; ?>
                                            <img class="cat-image lazyload" src="<?php echo e(asset('frontend/images/placeholder.jpg')); ?>" data-src="<?php echo e(asset($category->icon)); ?>" width="30" alt="<?php echo e(__($category->name)); ?>">
                                            <span class="cat-name"><?php echo e(__($category->name)); ?></span>
                                        </a>
                                        <?php if(count($category->subcategories)>0): ?>
                                            <div class="sub-cat-menu c-scrollbar">
                                                <div class="c-preloader">
                                                    <i class="fa fa-spin fa-spinner"></i>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </li>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Navbar -->
</div>
	
	
<!-- The Modal -->
<div class="modal fade text-center py-5" id="pincodemodal">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header p-2" style="border-bottom: 1px solid transparent;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
			<div class="top-strip"></div>
			<h3 class="mb-0 font23">Where do you want the delivery?</h3>
			<p class="py-1 text-muted font15" id="delivery_location"><i class="fa fa-map-marker modal-icon"></i> We can't detect your location</p>
			<div class="popularRegionsBgBrd"><span class="js_pin_location_msg2">enter a pincode</span></div>
			<form class="mt-3" method="post">
				<?php echo csrf_field(); ?>

				<div class="input-group w-75 mx-auto">
				  <input type="text" class="form-control" name="pincode" id="pincode" placeholder="Enter Pincode" aria-label="Enter Pincode" aria-describedby="button-addon2" autocomplete="off" maxlength=6 pattern="[0-9]{6}" onkeypress="return isNumberKey(event);" required>
				  <div class="input-group-append">
					<button class="btn btn-primary" type="button" id="pincodeBtn">Apply</button>
				  </div>
				</div>
				<p class="text-danger pinerror text-center w-75 font-weight-bold"></p>
			</form>
			<p class="pb-1 text-muted"><small class="text-danger" id="pincodeErr"></small></p>
      </div>
	  
    </div>
  </div>
</div>	
<style>
    .btn-social{position:relative;padding-left:44px;text-align:left;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}
	.btn-social :first-child{ position:absolute;left:0;top:0;bottom:0;width:32px;line-height:34px;font-size:1.6em;text-align:center;border-right:1px solid rgba(0,0,0,0.2)}
	
	.fa-facebook{background:#3B5998!important;}
	.fa-twitter{background:#2BA9E1!important;}
	.fa-linkedin{background:#0077B5!important;}
	.fa-envelope{background:#3B5998!important;}
	.fa-whatsapp{background:#2DB742!important;}
	.btn-linkedin{ border-color:#0077B5!important; }
	.btn-email{ border-color: #517FA4!important; }
	.btn-whatsapp{ border-color: #2DB742!important; }
	.btn-facebook,.btn-twitter,.btn-linkedin,.btn-email,.btn-whatsapp{ color:#000!important;}
</style>
<!------ Share Modal ------>
<div class="modal fade" id="shareitemmodal" style="z-index:99999">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header p-2 bg-blue" style="">
				<h6 class="modal-title text-white">Share & Refer Aldebazaar.com with friends</h6>
				<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
			    <?php
			        $url=url('/');
			        $mesg="Hi, I recommend ".$url." website - Buy Indian delicacies, Groceries, Medicines, Appliances, daily essential products and many more With Fastest Delivery & Free Shipping with COD Available. Learn More : $url"; 
			        $msg=rawurlencode($mesg);
			        $msg1=urlencode($mesg); 
			    ?>
				<a href="https://www.facebook.com/sharer.php?u=<?php echo e(url('/')); ?>&text=<?php echo e($msg); ?>" target="_blank" class="btn btn-block btn-social btn-facebook bg-white">
					<span class="fa fa-facebook text-white"></span> Facebook
				</a>
				<a href="https://twitter.com/share?url=<?php echo e(url('/')); ?>&text=<?php echo e($msg); ?>" target="_blank" class="btn btn-block btn-social btn-twitter bg-white">
					<span class="fa fa-twitter text-white"></span> Twitter
				</a>
				<a href="https://www.linkedin.com/shareArticle?url=<?php echo e(url('/')); ?>&title=<?php echo e($msg); ?>" target="_blank" class="btn btn-block btn-social btn-linkedin bg-white">
					<span class="fa fa-linkedin text-white"></span> linkedin
				</a>
				<a href="mailto:?subject=I want to recommend Aldebazaar.com&body=<?php echo e($msg); ?>" target="_blank" class="btn btn-block btn-social btn-email bg-white">
					<span class="fa fa-envelope text-white"></span> Email
				</a>
				<a href="whatsapp://send?text=<?php echo e($msg1); ?>" data-action="share/whatsapp/share" target="_blank" class="btn btn-block btn-social btn-whatsapp bg-white">
					<span class="fa fa-whatsapp text-white"></span> Share via Whatsapp
				</a>
			</div>
		</div>
	</div>
</div>
<script>
    $('select').on('change', function() {
        
        if(this.value == 'Pharmacy'){
           window.location.href = 'http://staging.aldebazaar.com/pharmacy-enquiry'; 
        }
    });
</script>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/inc/nav.blade.php ENDPATH**/ ?>