<?php $__env->startSection('content'); ?>
<?php if($errors->any()): ?>
			<div class="alert alert-danger">
				<strong>Whoops! </strong> There were some problems with your input.<br/><br/>
				<ul>
					<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><?php echo e($error); ?></li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		<?php endif; ?>
<div class="col">
    <div class="panel">
        <div class="panel-title">
            <center><h3><?php echo e(__('Add City')); ?></h3></center>
        </div>
        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="<?php echo e(route('city.store')); ?>" method="POST" enctype="multipart/form-data" onsubmit="return validate()">
        	<?php echo csrf_field(); ?>
            <div class="panel-body">
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <?php echo e(__('Select State')); ?> <br />
                        <select class="form-control" name="stateid">
                            <option >Please select</option>
                            <?php $__currentLoopData = $state; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stateList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                               <option value="<?php echo e($stateList->id); ?>"><?php echo e($stateList->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
                
               
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <?php echo e(__('City Name')); ?> <br />
                        <input type="text" placeholder="<?php echo e(__('City Name')); ?>" id="BlogTitle" name="city" class="form-control" required value="<?php echo e(old('BlogTitle')); ?>">
                    </div>
                </div>
                
                
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <?php echo e(__('City code')); ?> <br />
                        <input type="text" placeholder="<?php echo e(__('City code')); ?>" id="BlogTitle" name="code" class="form-control" required value="<?php echo e(old('BlogTitle')); ?>">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit"><?php echo e(__('Save')); ?></button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<style>
    .error-input {
        border: 1px solid red;    
    }
    .success-input {
        border: 1px solid green;
    }
</style>
<!-- Bootstrap WYSIHTML5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="<?php echo e(asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')); ?>"></script>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')); ?>" />
<script src="//cdn.ckeditor.com/4.10.1/full/ckeditor.js"></script>
<script>
 /* $(function () {
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5(
	{
		"color": true,
	});
  });
  */
   CKEDITOR.replace( 'textarea', {
        filebrowserUploadUrl: "<?php echo e(route('EditorUploadImg', ['_token' => csrf_token() ])); ?>",
        filebrowserUploadMethod: 'form'
    });
   $(function () {
      $('#BlogPublishedOn').datetimepicker({
		format: 'YYYY-MM-DD HH:mm:ss'
    });
     });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/City/create.blade.php ENDPATH**/ ?>