<?php $__env->startSection('content'); ?>
<!-- Seller Style -->

<link type="text/css" href="<?php echo e(asset('frontend/css/seller.css')); ?>" rel="stylesheet" media="all">
    <section class="gry-bg py-4 profile" >
        <div class="container-fluid">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                       <?php echo $__env->make('frontend.inc.seller_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
                </div>
                <style>
                    .form-horizontal, .panel {     width: 100%; padding: 15px;background: #fff;}
                </style>
                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                       <div class="row">
	
</div>

<div class="row mt-4">
    <div class="panel">
        <div class="panel-heading bord-btm clearfix pad-all">
            <h3 class="panel-title pull-left pad-no" style="font-size: 22px;">Seller Order List</h3>
           
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo e(__('Order Code')); ?></th>
                    
                    <th><?php echo e(__('Customer')); ?></th>
                    <th><?php echo e(__('Amount')); ?></th>
                    <th><?php echo e(__('Discount Amount')); ?></th>
                    <th><?php echo e(__('Order Status')); ?></th>
                    <th><?php echo e(__('Pay Method')); ?></th>
                    <th><?php echo e(__('Pay Status')); ?></th>
                    <th><?php echo e(__('Seller Amount')); ?></th>
                    <th><?php echo e(__('Service Provider Comission')); ?></th>
                    <th><?php echo e(__('Product Discount')); ?></th>
                    <th><?php echo e(__('Inclusive Comission')); ?></th>
                    <th><?php echo e(__('Exclusive Comission')); ?></th>
                    <th><?php echo e(__('Credit to seller wallet')); ?></th>
                   
                </tr>
            </thead>
            <tbody>
               
                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $order_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                        $seller_id = $order_id->seller_id;
                        $order = \App\Order::find($order_id->id);
                       
                        $orderDetails = \App\OrderDetail::find($order_id->order_id);
                        
                    ?>
                    <?php if($order != null): ?>
                        <?php
                        if ($order->orderDetails->where('seller_id',  $seller_id)->first()->payment_status == 'paid') {
                            $payment_status = "paid";
                        } else {
                            $payment_status = "unpaid";
                        }
                        ?>
                        <tr>
                            <td>
                                <?php echo e(($key+1) + ($orders->currentPage() - 1)*$orders->perPage()); ?> 
                            </td>
                            <td>
                               <a href="#" class="ordcode"> <?php echo e($order->code); ?></a><?php if($order->viewed == 0): ?> <span class="pull-right badge badge-info"><?php echo e(__('New')); ?></span> <?php endif; ?>
                            </td>
                           
                            <td>
                                <?php if($order->user_id != null): ?>
                                    <?php echo e($order->user->name??' '); ?>

                                <?php else: ?>
                                    Guest (<?php echo e($order->guest_id); ?>)
                                <?php endif; ?>
                            </td>
                            <td>
								<?php echo e(single_price($order->orderDetails->where('seller_id', $seller_id)->sum('price') + $order->orderDetails->where('seller_id', $seller_id)->sum('tax'))); ?>

		
                            </td>
                            <td>Type discount</td>
                            <td>
                                <?php
                                    $status = $order->orderDetails->where('seller_id', $seller_id)->first()->delivery_status;
                                ?>
                                <?php if($status == "Confirmation Pending"): ?>
                                <label class="label label-danger"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></label>
                                <?php elseif($status == "pending"): ?>
                                <label class="label label-warning"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></label>
                                <?php else: ?>
                                <label class="label label-warning"><?php echo e(ucfirst(str_replace('_', ' ', $status))); ?></label>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e(ucfirst(str_replace('_', ' ', $order->payment_type))); ?>

                            </td>
                            <td>
                                <span class="badge badge--2 mr-4">
                                    <?php if($payment_status == 'paid'): ?>
                                        <i class="bg-green"></i> Paid
                                    <?php else: ?>
                                        <i class="bg-red"></i> Unpaid
                                    <?php endif; ?>
                                </span>
                            </td>
                            <td><?php echo e(single_price($order->service_provider)); ?></td>
                            <td><?php echo e($order->seller_commission); ?></td>
                            <?php
                               $getPrice = \App\OrderDetail::where('order_id', $order_id->id)->get(); 
                               
                              
                               if(isset($getPrice[0]->inclusive_price)){
                                   $comissionselleralde = single_price($order->seller_commission);
                                }else{
                                    $comissionselleralde = 0;
                                }
                               
                                if(isset($getPrice[0]->exclusive_price)){
                                   $comissionselleraldeex = single_price($order->seller_commission);
                                }else{
                                    $comissionselleraldeex = 0;
                                }
                            ?>
                            <td>
                                <?php
                                $discountAmount = $order->service_provider - $order->seller_commission ;
                                ?>
                                <?php echo e(single_price($getPrice[0]->discount)); ?>

                                
                            </td>
                            <td><?php echo e($comissionselleralde); ?></td>
                            <td><?php echo e($comissionselleraldeex); ?></td>
                            <td><?php echo e($order->paid_to_seller_account); ?></td>
                            <td> <button onclick="show_order_details(<?php echo e($order->id); ?>)" class="btn btn-light btn-block text-white" style="background:#131921;"><?php echo e(__('Order Details')); ?></button>&nbsp;</td>
                            
                        </tr>
                     <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
            </table>
            
            
        </div>
        <div class="clearfix">
        <!--<div class="pull-right">-->
        <!--    <?php echo e($orders->appends(request()->input())->links()); ?>-->
        <!--</div>-->
    </div>
    </div>
    
</div>

<div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>

<script>
    $(function(){
    var $select = $(".percent");
    for (i=1;i<=100;i++){
        $select.append($('<option></option>').val(i).html(i))
    }
});
</script>

                   
                    </div>
                </div>
            </div>
        </div>
    </section>

    
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/seller/order-list.blade.php ENDPATH**/ ?>