<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo e(__('Menu Information')); ?></h3>
    </div>

    <!--Horizontal Form-->
    <!--===================================================-->
    <form class="form-horizontal" action="<?php echo e(route('menu.update', $menu->id)); ?>" method="POST">
        <?php echo csrf_field(); ?>
        <input type="hidden" name="_method" value="PATCH">
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3" for="url"><?php echo e(__('URL')); ?></label>
                <div class="col-sm-9">
                    <input type="text" placeholder="<?php echo e(__('URL')); ?>" id="url" name="url" value="<?php echo e($menu->link); ?>" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label"><?php echo e(__('Name')); ?></label>
                </div>
                <div class="col-sm-9">
                   <input type="text" placeholder="<?php echo e(__('Name')); ?>" id="menu_name" name="menu_name" value="<?php echo e($menu->name); ?>" class="form-control" required>
                </div>
            </div>
			<?php
				$dbposition=\App\Menu::get();
				$allposiotns=array();
				foreach($dbposition as $value){
						array_push($allposiotns,$value->position);
				}						
			?>
			<div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label"><?php echo e(__('Position')); ?></label>
                </div>
                <div class="col-sm-9">
                    <select id="menu_position" name="menu_position" placeholder="Menu Position" class="form-control" required>
						<option value="<?php echo e($menu->position); ?>" selected ><?php echo e($menu->position); ?></option>
						<?php for($i=1;$i<=5;$i++): ?>
							<?php if(!in_array($i,$allposiotns)): ?>
								<option value="<?php echo e($i); ?>" ><?php echo e($i); ?></option>
							<?php endif; ?>
						<?php endfor; ?>
					<select>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-purple" type="submit"><?php echo e(__('Save')); ?></button>
        </div>
    </form>
    <!--===================================================-->
    <!--End Horizontal Form-->

</div>
<?php /**PATH /home2/aldebaza/staging_script/resources/views/menu/edit.blade.php ENDPATH**/ ?>