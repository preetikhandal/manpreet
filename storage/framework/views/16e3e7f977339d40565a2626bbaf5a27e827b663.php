<?php $__env->startSection('content'); ?>

<div class="py-4 gry-bg" <?php if($mobileapp == 1): ?> style="margin-top:75px" <?php endif; ?>>
    <div class="mt-4">
        <div class="container-fluid">
            <div class="bg-white px-3 pt-3">
                <div class="row gutters-10">
                    <?php $__currentLoopData = \App\Brand::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-xxl-2 col-lg-2 col-sm-4 text-center">
							<a href="<?php echo e(route('categories.all', ['brand'=>$brand->slug])); ?>" class="d-block p-3 mb-3 border rounded">
								<?php if($brand->logo!=null): ?>
									<img src="<?php echo e(asset($brand->logo)); ?>" class="lazyload img-fit" height="50" alt="<?php echo e(__($brand->name)); ?>">
								<?php else: ?>
									<h5 class="heading-5 strong-400"><?php echo e(ucwords(strtolower($brand->name))); ?></h5>
								<?php endif; ?>
                            </a>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/frontend/all_brand.blade.php ENDPATH**/ ?>