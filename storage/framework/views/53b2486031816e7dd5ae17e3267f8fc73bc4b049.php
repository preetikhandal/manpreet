<?php $__env->startSection('content'); ?>
<div class="row">
    <?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
	<form class="form form-horizontal mar-top" action="<?php echo e(route('products.update', $product->id)); ?>" method="POST" enctype="multipart/form-data" id="choice_form">
		<input name="_method" type="hidden" value="POST">
		<input type="hidden" name="id" value="<?php echo e($product->id); ?>">
		<?php echo csrf_field(); ?>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo e(__('Product Information')); ?></h3>
			</div>
			<div class="panel-body">
				<div class="tab-base tab-stacked-left">
				    <!--Nav tabs-->
				    <ul class="nav nav-tabs">
						<li class="active">
				            <a data-toggle="tab" href="#demo-stk-lft-tab-1" aria-expanded="true"><?php echo e(__('General')); ?></a>
				        </li>
				        <li class="">
				            <a data-toggle="tab" href="#demo-stk-lft-tab-2" aria-expanded="false"><?php echo e(__('Images')); ?></a>
				        </li>
						<!--li class="">
				            <a data-toggle="tab" href="#demo-stk-lft-tab-3" aria-expanded="false"><?php echo e(__('Videos')); ?></a>
				        </li-->
				        <li class="">
				            <a data-toggle="tab" href="#demo-stk-lft-tab-4" aria-expanded="false"><?php echo e(__('Meta Tags')); ?></a>
				        </li>
						<li class="">
				            <a data-toggle="tab" href="#demo-stk-lft-tab-55" aria-expanded="false"><?php echo e(__('Customer Choice')); ?></a>
				        </li>
						<li class="">
				            <a data-toggle="tab" href="#demo-stk-lft-tab-6" aria-expanded="false"><?php echo e(__('Price')); ?></a>
				        </li>
						<li class="">
				            <a data-toggle="tab" href="#demo-stk-lft-tab-7" aria-expanded="false"><?php echo e(__('Description')); ?></a>
				        </li>
						
						<li class="">
				            <a data-toggle="tab" href="#demo-stk-lft-tab-9" aria-expanded="false"><?php echo e(__('Shipping Info')); ?></a>
				        </li>
						<li class="">
				            <a data-toggle="tab" href="#demo-stk-lft-tab-10" aria-expanded="false"><?php echo e(__('PDF Specification')); ?></a>
				        </li>
				    </ul>

				    <!--Tabs Content-->
				    <div class="tab-content">
				        <div id="demo-stk-lft-tab-1" class="tab-pane fade active in">
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Product ID')); ?></label>
								<div class="col-lg-7">
									<input type="text" class="form-control" name="product_id" placeholder="<?php echo e(__('Product ID')); ?>" value="<?php echo e($product->product_id); ?>" required>
								</div>
							</div>
							<div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Product Name')); ?></label>
	                            <div class="col-lg-7">
	                                <input type="text" class="form-control" name="name" placeholder="<?php echo e(__('Product Name')); ?>" value="<?php echo e($product->name); ?>" required>
	                            </div>
	                        </div>
	                        <div class="form-group" id="category">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Category')); ?></label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
	                                	<option>Select an option</option>
	                                	<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                                	    <option value="<?php echo e($category->id); ?>" <?php if($product->category_id == $category->id) echo "selected"; ?> ><?php echo e(__($category->name)); ?></option>
	                                	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group" id="subcategory">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Subcategory')); ?></label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id" required>

	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group" id="subsubcategory">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Sub Subcategory')); ?></label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id" required>

	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group" id="brand">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Brand')); ?></label>
	                            <div class="col-lg-7">
	                                <select class="form-control demo-select2-placeholder" name="brand_id" id="brand_id" required>
										<option value=""><?php echo e(('Select Brand')); ?></option>
										<?php $__currentLoopData = \App\Brand::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($brand->id); ?>" <?php if($product->brand_id == $brand->id): ?> selected <?php endif; ?>><?php echo e($brand->name); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Unit')); ?></label>
	                            <div class="col-lg-7">
	                                <input type="text" class="form-control" name="unit" placeholder="Unit (e.g. KG, Pc etc)" value="<?php echo e($product->unit); ?>" required>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Tags')); ?></label>
	                            <div class="col-lg-7">
	                                <input type="text" class="form-control" name="tags[]" id="tags" value="<?php echo e($product->tags); ?>" placeholder="Type to add a tag" data-role="tagsinput">
	                            </div>
	                        </div>
				        </div>
				        <div id="demo-stk-lft-tab-2" class="tab-pane fade">
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Main Images')); ?> <small class="text-danger">(800px * 800px)</small></label>
								<div class="col-lg-7">
									<div id="photos">
										<?php if(is_array(json_decode($product->photos))): ?>
											<?php $__currentLoopData = json_decode($product->photos); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<div class="col-md-4 col-sm-4 col-xs-6">
													<div class="img-upload-preview">
														<img loading="lazy"  src="<?php echo e(asset($photo)); ?>" alt="" class="img-responsive">
														<input type="hidden" name="previous_photos[]" value="<?php echo e($photo); ?>">
														<button type="button" class="btn btn-danger close-btn remove-files-main-image"><i class="fa fa-times"></i></button>
													</div>
												</div>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Thumbnail Image')); ?> <small class="text-danger">(200px * 200px)</small></label>
								<div class="col-lg-7">
									<div id="thumbnail_img">
										<?php if($product->thumbnail_img != null): ?>
											<div class="col-md-4 col-sm-4 col-xs-6">
												<div class="img-upload-preview">
													<img loading="lazy"  src="<?php echo e(asset($product->thumbnail_img)); ?>" alt="" class="img-responsive">
													<input type="hidden" name="previous_thumbnail_img" value="<?php echo e($product->thumbnail_img); ?>">
													<button type="button" class="btn btn-danger close-btn remove-files-thumb"><i class="fa fa-times"></i></button>
												</div>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Featured')); ?> <small class="text-danger">(200px * 200px)</small></label>
								<div class="col-lg-7">
									<div id="featured_img">
										<?php if($product->featured_img != null): ?>
											<div class="col-md-4 col-sm-4 col-xs-6">
												<div class="img-upload-preview">
													<img loading="lazy"  src="<?php echo e(asset($product->featured_img)); ?>" alt="" class="img-responsive">
													<input type="hidden" name="previous_featured_img" value="<?php echo e($product->featured_img); ?>">
													<button type="button" class="btn btn-danger close-btn remove-files-featured"><i class="fa fa-times"></i></button>
												</div>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Flash Deal')); ?> <small class="text-danger">(200px * 200px)</small></label>
								<div class="col-lg-7">
									<div id="flash_deal_img">
										<?php if($product->flash_deal_img != null): ?>
											<div class="col-md-4 col-sm-4 col-xs-6">
												<div class="img-upload-preview">
													<img loading="lazy"  src="<?php echo e(asset($product->flash_deal_img)); ?>" alt="" class="img-responsive">
													<input type="hidden" name="previous_flash_deal_img" value="<?php echo e($product->flash_deal_img); ?>">
													<button type="button" class="btn btn-danger close-btn remove-files-flash-deal"><i class="fa fa-times"></i></button>
												</div>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
				        </div>
				        <div id="demo-stk-lft-tab-3" class="tab-pane fade">
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Video Provider')); ?></label>
								<div class="col-lg-7">
									<select class="form-control demo-select2-placeholder" name="video_provider" id="video_provider">
										<option value="youtube" <?php if($product->video_provider == 'youtube') echo "selected";?> ><?php echo e(__('Youtube')); ?></option>
										<option value="dailymotion" <?php if($product->video_provider == 'dailymotion') echo "selected";?> ><?php echo e(__('Dailymotion')); ?></option>
										<option value="vimeo" <?php if($product->video_provider == 'vimeo') echo "selected";?> ><?php echo e(__('Vimeo')); ?></option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Video Link')); ?></label>
								<div class="col-lg-7">
									<input type="text" class="form-control" name="video_link" value="<?php echo e($product->video_link); ?>" placeholder="Video Link">
								</div>
							</div>
				        </div>
						<div id="demo-stk-lft-tab-4" class="tab-pane fade">
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Meta Title')); ?></label>
								<div class="col-lg-7">
									<input type="text" class="form-control" name="meta_title" value="<?php echo e($product->meta_title); ?>" placeholder="<?php echo e(__('Meta Title')); ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Description')); ?></label>
								<div class="col-lg-7">
									<textarea name="meta_description" rows="8" class="form-control"><?php echo e($product->meta_description); ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Meta Image')); ?> <small class="text-danger">(200px * 200px)</small></label>
								<div class="col-lg-7">
									<div id="meta_photo">
										<?php if($product->meta_img != null): ?>
											<div class="col-md-4 col-sm-4 col-xs-6">
												<div class="img-upload-preview">
													<img loading="lazy"  src="<?php echo e(asset($product->meta_img)); ?>" alt="" class="img-responsive">
													<input type="hidden" name="previous_meta_img" value="<?php echo e($product->meta_img); ?>">
													<button type="button" class="btn btn-danger close-btn remove-files-meta"><i class="fa fa-times"></i></button>
												</div>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
				        </div>
						<div id="demo-stk-lft-tab-55" class="tab-pane fade">
							<div class="form-group">
								<div class="col-lg-2">
									<input type="text" class="form-control" value="<?php echo e(__('Colors')); ?>" disabled>
								</div>
								<div class="col-lg-7">
									<select class="form-control color-var-select" name="colors[]" id="colors" multiple>
										<?php $__currentLoopData = \App\Color::select('name')->orderBy('name', 'asc')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($color->name); ?>" <?php if(in_array($color->name, json_decode($product->colors))) echo 'selected'?> ><?php echo e($color->name); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</select>
								</div>
								<div class="col-lg-2">
									<label class="switch" style="margin-top:5px;">
										<input value="1" type="checkbox" name="colors_active" <?php if(count(json_decode($product->colors)) > 0) echo "checked";?> >
										<span class="slider round"></span>
									</label>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-2">
									<input type="text" class="form-control" value="<?php echo e(__('Attributes')); ?>" disabled>
								</div>
			                    <div class="col-lg-7">
                                    <?php
                                    $variation_order = json_decode($product->variation_order,true);
                                    if(in_array("Colors", $variation_order)) {
                                        unset($variation_order[array_search("Colors",$variation_order)]);
                                    }
                                    $variation = json_decode($product->variation,true);
                                    if(isset($variation["Colors"])) {
                                        unset($variation["Colors"]);
                                    }
                                    ?>
			                        <select name="choice_attributes[]" id="choice_attributes" class="form-control demo-select2" multiple data-placeholder="Choose Attributes">
										<?php $__currentLoopData = \App\Attribute::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $attribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($attribute->id); ?>" <?php if(in_array($attribute->name, $variation_order)): ?> selected <?php endif; ?>><?php echo e($attribute->name); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                        </select>
			                    </div>
			                </div>

							
							<div class="">
								<p>Choose the attributes of this product and then input values of each attribute</p>
								<br>
							</div>

							<div class="customer_choice_options" id="customer_choice_options">
                               <?php $__currentLoopData = $variation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$vo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $attribute = \App\Attribute::where('name',$key)->first();  ?>
									<div class="form-group">
										<div class="col-lg-2">
											<input type="hidden" name="choice_no[]" value="<?php echo e($attribute->id); ?>">
											<input type="text" class="form-control" name="choice[]" value="<?php echo e($attribute->name); ?>" placeholder="Choice Title" disabled>
										</div>
										<div class="col-lg-7">
											<input type="text" class="form-control" name="choice_options_<?php echo e($attribute->id); ?>" placeholder="Enter choice values" value="<?php echo e(implode(',', $vo)); ?>" data-role="tagsinput" onchange="product_variation()">
										</div>
										<div class="col-lg-2">
											<button onclick="delete_row(this)" class="btn btn-danger btn-icon"><i class="demo-psi-recycling icon-lg"></i></button>
										</div>
									</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>
                            
							<br>
							<div class="product_variation" id="product_variation">

							</div>
				        </div>
						<div id="demo-stk-lft-tab-6" class="tab-pane fade">
							<div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Unit price')); ?></label>
	                            <div class="col-lg-7">
	                                <input type="text" placeholder="<?php echo e(__('Unit price')); ?>" name="unit_price" class="form-control" value="<?php echo e($product->unit_price); ?>" required>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Purchase price')); ?></label>
	                            <div class="col-lg-7">
	                                <input type="number" min="0" step="0.01" placeholder="<?php echo e(__('Purchase price')); ?>" name="purchase_price" class="form-control" value="<?php echo e($product->purchase_price); ?>" required>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Tax')); ?></label>
	                            <div class="col-lg-7">
	                                <input type="number" min="0" step="0.01" placeholder="<?php echo e(__('tax')); ?>" name="tax" class="form-control" value="<?php echo e($product->tax); ?>" required>
	                            </div>
	                            <div class="col-lg-1">
	                                <select class="demo-select2" name="tax_type" required>
	                                	<option value="amount" <?php if($product->tax_type == 'amount') echo "selected";?> >₹</option>
	                                	<option value="percent" <?php if($product->tax_type == 'percent') echo "selected";?> >%</option>
	                                </select>
	                            </div>
	                        </div>
	                        
	                        <!--GST PERCENT-->
							<div class="form-group" id="gst">
								<label class="col-lg-2 control-label"><?php echo e(__('Gst Tax In Percent')); ?></label>
								<div class="col-lg-7">
									<select class="form-control demo-select2-placeholder" name="gstpercent" id="gstpercent" required>
									
											<option value="2.5" <?php if($product->gst_tax == 3) echo "selected";?>>2.5</option>
											<option value="5" <?php if($product->gst_tax == 5) echo "selected";?>>5</option>
											<option value="18" <?php if($product->gst_tax == 18) echo "selected";?>>18</option>
											<option value="28" <?php if($product->gst_tax == 28) echo "selected";?> >28</option>
										
									</select>
								</div>
							</div>
							<!--END GST PERCENT-->
	                        <div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Discount')); ?></label>
	                            <div class="col-lg-7">
	                                <input type="text"  placeholder="<?php echo e(__('Discount')); ?>" data-toggle="tooltip" data-placement="top" title="Please Type ND in capital latter" name="discount" class="form-control" value="<?php echo e($product->discount); ?>" required>
	                            </div>
	                            <div class="col-lg-1">
	                                <select class="demo-select2" name="discount_type" required>
	                                	<option value="amount" <?php if($product->discount_type == 'amount') echo "selected";?> >₹</option>
	                                	<option value="percent" <?php if($product->discount_type == 'percent') echo "selected";?> >%</option>
	                                </select>
	                            </div>
	                        </div>
							<div class="form-group" id="quantity">
								<label class="col-lg-2 control-label"><?php echo e(__('Quantity')); ?></label>
								<div class="col-lg-7">
									<input type="number" value="<?php echo e($product->current_stock); ?>" step="1" placeholder="<?php echo e(__('Quantity')); ?>" name="current_stock" class="form-control" required>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('Min Purchased Quantity')); ?></label>
								<div class="col-lg-7">
									<input type="number" min="1" step="1" placeholder="<?php echo e(__('Min Purchased Quantity')); ?>" value="<?php echo e($product->min_purchased_quantity); ?>"  name="min_purchased_quantity" class="form-control" required>
								</div>
							</div>
							<br>
							<div class="sku_combination" id="sku_combination">

							</div>
				        </div>
						<div id="demo-stk-lft-tab-7" class="tab-pane fade">
							<div class="form-group">
	                            <label class="col-lg-2 control-label"><?php echo e(__('Description')); ?></label>
	                            <div class="col-lg-9">
	                                <textarea class="editor" name="description"><?php echo e($product->description); ?></textarea>
	                            </div>
	                        </div>
				        </div>
						
						<div id="demo-stk-lft-tab-9" class="tab-pane fade">
							<div class="row bord-btm">
								<div class="col-md-2">
									<div class="panel-heading">
										<h3 class="panel-title"><?php echo e(__('Free Shipping')); ?></h3>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<label class="col-lg-2 control-label"><?php echo e(__('Status')); ?></label>
										<div class="col-lg-7">
											<label class="switch" style="margin-top:5px;">
												<input type="radio" name="shipping_type" value="free" <?php if($product->shipping_type == 'free'): ?> checked <?php endif; ?>>
												<span class="slider round"></span>
											</label>
										</div>
									</div>
								</div>
							</div>

							<div class="row bord-btm">
								<div class="col-md-2">
									<div class="panel-heading">
										<h3 class="panel-title"><?php echo e(__('Local Pickup')); ?></h3>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<label class="col-lg-2 control-label"><?php echo e(__('Status')); ?></label>
										<div class="col-lg-7">
											<label class="switch" style="margin-top:5px;">
												<input type="radio" name="shipping_type" value="local_pickup" <?php if($product->shipping_type == 'local_pickup'): ?> checked <?php endif; ?>>
												<span class="slider round"></span>
											</label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label"><?php echo e(__('Shipping cost')); ?></label>
										<div class="col-lg-7">
											<input type="number" min="0" step="0.01" placeholder="<?php echo e(__('Shipping cost')); ?>" name="local_pickup_shipping_cost" class="form-control" value="<?php echo e($product->shipping_cost); ?>" required>
										</div>
									</div>
								</div>
							</div>

							<div class="row bord-btm">
								<div class="col-md-2">
									<div class="panel-heading">
										<h3 class="panel-title"><?php echo e(__('Flat Rate')); ?></h3>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<label class="col-lg-2 control-label"><?php echo e(__('Status')); ?></label>
										<div class="col-lg-7">
											<label class="switch" style="margin-top:5px;">
												<input type="radio" name="shipping_type" value="flat_rate" <?php if($product->shipping_type == 'flat_rate'): ?> checked <?php endif; ?>>
												<span class="slider round"></span>
											</label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label"><?php echo e(__('Shipping cost')); ?></label>
										<div class="col-lg-7">
											<input type="number" min="0" step="0.01" placeholder="<?php echo e(__('Shipping cost')); ?>" name="flat_shipping_cost" class="form-control" value="<?php echo e($product->shipping_cost); ?>" required>
										</div>
									</div>
								</div>
							</div>

				        </div>
						<div id="demo-stk-lft-tab-10" class="tab-pane fade">
							<div class="form-group">
								<label class="col-lg-2 control-label"><?php echo e(__('PDF Specification')); ?></label>
								<div class="col-lg-7">
									<input type="file" class="form-control" placeholder="<?php echo e(__('PDF')); ?>" name="pdf" accept="application/pdf">
								</div>
							</div>
				        </div>
				    </div>
				</div>
			</div>
			<div class="panel-footer text-right">
				<button type="submit" name="button" class="btn btn-purple"><?php echo e(__('Save')); ?></button>
			</div>
		</div>
	</form>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<script type="text/javascript">

	// var i = $('input[name="choice_no[]"').last().val();
	// if(isNaN(i)){
	// 	i =0;
	// }

	function add_more_customer_choice_option(i, name){
		$('#customer_choice_options').append('<div class="form-group"><div class="col-lg-2"><input type="hidden" name="choice_no[]" value="'+i+'"><input type="text" class="form-control" name="choice[]" value="'+name+'" readonly></div><div class="col-lg-7"><input type="text" class="form-control" name="choice_options_'+i+'" placeholder="Enter choice values" data-role="tagsinput" onchange="product_variation()"></div><div class="col-lg-2"><button onclick="delete_row(this)" class="btn btn-danger btn-icon"><i class="demo-psi-recycling icon-lg"></i></button></div></div>');
		$("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
	}

	$('input[name="colors_active"]').on('change', function() {
	    if(!$('input[name="colors_active"]').is(':checked')){
			$('#colors').prop('disabled', true);
		}
		else{
			$('#colors').prop('disabled', false);
		}
		//update_sku();
		product_variation();
	});

	$('#colors').on('change', function() {
	    product_variation();
	    //update_sku();
	});

	// $('input[name="unit_price"]').on('keyup', function() {
	//     update_sku();
	// });

	function delete_row(em){
		$(em).closest('.form-group').remove();
		//update_sku();
		product_variation();
	}
    
    <?php /*
	function update_sku(){
		$.ajax({
		   type:"POST",
		   url:'{{ route('products.variation_edit') }}',
		   data:$('#choice_form').serialize(),
		   success: function(data){
			   $('#product_variation').html(data);
			   if (data.length > 1) {
				   $('#quantity').hide();
			   }
			   else {
					$('#quantity').show();
			   }
		   }
	   });
	} */
    ?>
    
	function product_variation(){
		$.ajax({
		   type:"POST",
		   url:'<?php echo e(route('products.variation_edit')); ?>',
		   data:$('#choice_form').serialize(),
		   success: function(data){
			   $('#product_variation').html(data);
			   /*if (data.length > 1) {
				   $('#quantity').hide();
			   }
			   else {
					$('#quantity').show();
			   } */
		   }
	   });
	}

	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('<?php echo e(route('subcategories.get_subcategories_by_category')); ?>',{_token:'<?php echo e(csrf_token()); ?>', category_id:category_id}, function(data){
		    $('#subcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		    $("#subcategory_id > option").each(function() {
		        if(this.value == '<?php echo e($product->subcategory_id); ?>'){
		            $("#subcategory_id").val(this.value).change();
		        }
		    });

		    $('.demo-select2').select2();

		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('<?php echo e(route('subsubcategories.get_subsubcategories_by_subcategory')); ?>',{_token:'<?php echo e(csrf_token()); ?>', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
			$('#subsubcategory_id').append($('<option>', {
				value: null,
				text: null
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		    $("#subsubcategory_id > option").each(function() {
		        if(this.value == '<?php echo e($product->subsubcategory_id); ?>'){
		            $("#subsubcategory_id").val(this.value).change();
		        }
		    });

		    $('.demo-select2').select2();

		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}

	// function get_brands_by_subsubcategory(){
	// 	var subsubcategory_id = $('#subsubcategory_id').val();
	// 	$.post('<?php echo e(route('subsubcategories.get_brands_by_subsubcategory')); ?>',{_token:'<?php echo e(csrf_token()); ?>', subsubcategory_id:subsubcategory_id}, function(data){
	// 	    $('#brand_id').html(null);
	// 	    for (var i = 0; i < data.length; i++) {
	// 	        $('#brand_id').append($('<option>', {
	// 	            value: data[i].id,
	// 	            text: data[i].name
	// 	        }));
	// 	    }
	// 	    $("#brand_id > option").each(function() {
	// 	        if(this.value == '<?php echo e($product->brand_id); ?>'){
	// 	            $("#brand_id").val(this.value).change();
	// 	        }
	// 	    });
	//
	// 	    $('.demo-select2').select2();
	//
	// 	});
	// }

	function get_attributes_by_subsubcategory(){
		var subsubcategory_id = $('#subsubcategory_id').val();
		$.post('<?php echo e(route('subsubcategories.get_attributes_by_subsubcategory')); ?>',{_token:'<?php echo e(csrf_token()); ?>', subsubcategory_id:subsubcategory_id}, function(data){
		    $('#choice_attributes').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#choice_attributes').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
			$("#choice_attributes > option").each(function() {
				var str = <?php echo $product->attributes ?>;
		        $("#choice_attributes").val(str).change();
		    });

			$('.demo-select2').select2();
		});
	}

	$(document).ready(function(){
		//$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
	    get_subcategories_by_category();
		$("#photos").spartanMultiImagePicker({
			fieldName:        'photos[]',
			maxCount:         10,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			allowedExt:'png|jpg|jpeg',
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#thumbnail_img").spartanMultiImagePicker({
			fieldName:        'thumbnail_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			allowedExt:'png|jpg|jpeg',
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#featured_img").spartanMultiImagePicker({
			fieldName:        'featured_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			allowedExt:'png|jpg|jpeg',
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#flash_deal_img").spartanMultiImagePicker({
			fieldName:        'flash_deal_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			allowedExt:'png|jpg|jpeg',
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#meta_photo").spartanMultiImagePicker({
			fieldName:        'meta_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			allowedExt: 'png|jpg|jpeg',
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});

		//update_sku();
        product_variation();

		$('.remove-files-main-image').on('click', function(){
		    $(this).parents(".col-md-4").append($(this).parents(".col-md-4").find('input[name="previous_photos[]"]').attr('name','delete_photos[]'));
		    $(this).parents(".col-md-4").find('input[name="previous_photos[]"]').remove();
            $(this).parents(".col-md-4").hide();
        });
        
		$('.remove-files-thumb').on('click', function(){
		    $(this).parents(".col-md-4").append($(this).parents(".col-md-4").find('input[name="previous_thumbnail_img"]').attr('name','delete_thumbnail_img'));
		    $(this).parents(".col-md-4").find('input[name="previous_thumbnail_img"]').remove();
            $(this).parents(".col-md-4").hide();
        });
        
		$('.remove-files-featured').on('click', function(){
		    $(this).parents(".col-md-4").append($(this).parents(".col-md-4").find('input[name="previous_featured_img"]').attr('name','delete_featured_img'));
		    $(this).parents(".col-md-4").find('input[name="previous_featured_img"]').remove();
            $(this).parents(".col-md-4").hide();
        });
        
		$('.remove-files-flash-deal').on('click', function(){
		    $(this).parents(".col-md-4").append($(this).parents(".col-md-4").find('input[name="previous_flash_deal_img"]').attr('name','delete_flash_deal_img'));
		    $(this).parents(".col-md-4").find('input[name="previous_flash_deal_img"]').remove();
            $(this).parents(".col-md-4").hide();
        });
		$('.remove-files-meta').on('click', function(){
		    $(this).parents(".col-md-4").append($(this).parents(".col-md-4").find('input[name="previous_meta_img"]').attr('name','delete_meta_img'));
		    $(this).parents(".col-md-4").find('input[name="previous_meta_img"]').remove();
            $(this).parents(".col-md-4").hide();
        });
	});

	$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

	$('#subsubcategory_id').on('change', function() {
	    //get_brands_by_subsubcategory();
		//get_attributes_by_subsubcategory();
	});

	$('#choice_attributes').on('change', function() {
		//$('#customer_choice_options').html(null);
		$.each($("#choice_attributes option:selected"), function(j, attribute){
			flag = false;
			$('input[name="choice_no[]"]').each(function(i, choice_no) {
				if($(attribute).val() == $(choice_no).val()){
					flag = true;
				}
			});
            if(!flag){
				add_more_customer_choice_option($(attribute).val(), $(attribute).text());
			}
        });

		var str = <?php echo $product->attributes ?>;

		$.each(str, function(index, value){
			flag = false;
			$.each($("#choice_attributes option:selected"), function(j, attribute){
				if(value == $(attribute).val()){
					flag = true;
				}
			});
            if(!flag){
				//console.log();
				$('input[name="choice_no[]"][value="'+value+'"]').parent().parent().remove();
			}
		});

		//update_sku();
        product_variation();
	});

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/products/edit.blade.php ENDPATH**/ ?>