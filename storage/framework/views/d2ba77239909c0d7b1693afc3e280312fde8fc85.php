 <?php $__env->startSection('content'); ?>

<style>
.btn-file {
        position: relative;
        overflow: hidden;
    }
    
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 82%;
        min-height: 82%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    
    #img-upload {
        width: 82%;
    }
    
    .input-group {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -ms-flex-align: stretch;
        align-items: stretch;
        width: 82% !important;
    }
    
    .browse {
        background: black;
        color: #fff;
    }
}
</style>
<div class="pharmacy clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-12 offset-lg-2 offset-md-2 items">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
                <?php endif; ?>
                <h2 class="text-center">Pharmacy Inquiry</h2>
                <form class="form-horizontal" method="post" action="<?php echo e(route('add-pharmacy-form')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                        <div class="row">
                        <div class="col-lg-6 col-12">
                            <div class="form-group">
        <label class="control-label" for="email">Patient name:</label>
        <div class="">
        <input type="text" class="form-control" value="<?php echo e(old('name')); ?>" name="name" placeholder="Enter Name" required>
        </div>
    </div>
                        </div>
                            <div class="col-lg-6 col-12">
                            <div class="form-group">
                                <label class="control-label " for="email">Doctor name:</label>
                                <div class="">
                                    <input type="text" class="form-control" value="<?php echo e(old('docter_name')); ?>" name="docter_name" placeholder="Enter Doctor name">
                                </div>
                            </div>
                        </div>
                   
                    </div>
                    <div class="row">
                    
                        <div class="col-lg-6 col-12">
                            <div class="form-group">
                                <label class="control-label" for="pwd">Phone:</label>
                                <div class="">
                                    <input type="text" class="form-control" value="<?php echo e(old('phone')); ?>" name="phone" placeholder="Enter Phone" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="form-group">
                                <label class="control-label" for="pwd">Email:</label>
                                <div class="">
                                    <input type="email" class="form-control" value="<?php echo e(old('email')); ?>" name="email" placeholder="Enter email" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-lg-6 col-12">
                            <div class="form-group">
                            <label class="control-label " for="pwd">Address:</label>
                            <div class="">
                                <textarea class="form-control" name="address" maxlength="1000"><?php echo e(old('address')); ?></textarea>
                            </div>
                        </div>
                            
                        </div>
                         <div class="col-lg-6 col-12">
                            <div class="form-group">
                                <label class="control-label " for="pwd">Order medicines:</label>
                                <div class="">
                                    <textarea class="form-control" name="comment" maxlength="1000"><?php echo e(old('comment')); ?></textarea>
                                </div>
                            </div>
                        </div>
                        
                       </div>
                        <div class="row">
                             <div class="col-lg-6 col-12">
                          <div class="form-group">
                                <label>Upload Prescription </label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                <span class="btn btn-default btn-file browse">
                    Browse… <input type="file" id="imgInp" class="w-100"  name="prescriptionFile" accept="image/x-png,image/jpeg,image/jpg,.pdf" required>
                </span>
                                    </span>
                                    <input type="text" class="form-control" readonly>
                                </div>
                               
                            </div>
                        </div>
                         <div class="col-lg-12 col-12">
                             <img id='img-upload'  class="w-100" />
                        </div>
                        </div>
                    
                   
                    <div class="controlBtn text-center mt-3">
                        <button type="submit" class="btn btn-default browse">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/pharmacy/index.blade.php ENDPATH**/ ?>