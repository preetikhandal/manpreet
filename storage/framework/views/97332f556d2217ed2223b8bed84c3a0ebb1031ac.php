<?php $__env->startSection('content'); ?>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo e(__('Request Products')); ?></h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th><?php echo e(__('Product Name')); ?></th>
                        <th><?php echo e(__('Customer Name')); ?></th>
                        <th><?php echo e(__('Phone')); ?></th>
                        <th><?php echo e(__('Email')); ?></th>
                        <th><?php echo e(__('Message')); ?></th>
                        <th><?php echo e(__('Status')); ?></th>
                        <th width="10%"><?php echo e(__('Action')); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $ProductRequest; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $PR): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($key+1); ?></td>
                            <td><a href="<?php echo e(url('product/'.$PR->product)); ?>" target="_blank">
									<?php echo e(\App\Product::where('slug',$PR->product)->first()->name ?? ' '); ?>

								</a>
							</td>
                            <td><?php echo e($PR->name); ?></td>
                            <td><?php echo e($PR->phone); ?></td>
                            <td><?php echo e($PR->email); ?></td>
                            <td><?php echo e($PR->message); ?></td>
							<td><?php echo e($PR->status); ?></td>
                            <td>
								<div class="btn-group dropdown">
									<button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
										<?php echo e(__('Actions')); ?> <i class="dropdown-caret"></i>
									</button>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="<?php echo e(route('productrequest.process', $PR->id.'~In Process')); ?>"><?php echo e(__('In Process')); ?></a></li>
										<li><a href="<?php echo e(route('productrequest.process',$PR->id.'~Email')); ?>"><?php echo e(__('Precure')); ?></a></li>
										<li><a onclick="confirm_modal('<?php echo e(route('productrequest.destroy', $PR->id)); ?>');"><?php echo e(__('Delete')); ?></a></li>
									</ul>
								</div>
							</td>
                            
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/ProductRequest/index.blade.php ENDPATH**/ ?>