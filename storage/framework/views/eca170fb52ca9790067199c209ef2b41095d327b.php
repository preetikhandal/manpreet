176217

<?php $__env->startSection('content'); ?>
<div class="row">
  
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title text-center"><?php echo e(__('USER CASHBACK CONFIGURATION')); ?></h3>
            </div>

            <?php if(count($cashBackConfig) >= 1): ?>
           
            <div class="panel-body">
                <form class="form-horizontal" action="<?php echo e(route('update-cashback-config')); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" value="<?php echo e($cashBackConfig[0]->id); ?>" name="updateid">
                    <div id="smtp">
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name"><?php echo e(__('Category')); ?></label>
                            <div class="col-sm-6">
                                <select name="category_id" required class="form-control demo-select2-placeholder">
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($category->id); ?>" <?php if($cashBackConfig[0]->category_id == $category->id) echo "selected";?> ><?php echo e(__($category->name)); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                
                        <div class="form-group">
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('CASH BACK AMOUNT PERCENTAGE')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="CASHBACK_AMOUNT" value="<?php echo e($cashBackConfig[0]->casback_amount); ?>" placeholder="ENTER CASHBACK AMOUNT" require>
                            </div>%
                        </div>
                        
                        <div class="form-group">
                        
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('MINIMUM SHOPPING AMOUNT')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="MINIMUM_SHOPPING_AMOUNT" value="<?php echo e($cashBackConfig[0]->casback_minimum_amount_limit); ?>" placeholder="ENTER MINIMUM SHOPPING AMOUNT" require>
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('SHOPPING USER PERCENTAGE(Next Shopping)')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="SHOPPING_percentage" value="<?php echo e($cashBackConfig[0]->casback_use_percentage); ?>" placeholder="Shopping Order percentage" require>
                            </div>%
                        </div>

                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('NEXT MINIMUM SHOPPING AMOUNT')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="CASHBACK_CREDIT_AMT" value="<?php echo e($cashBackConfig[0]->cashback_credit_amount); ?>" placeholder="ENTER CASHBACK CREDIT AMOUNT" require>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('USER PRODUCT CANCELLATION CHARGE')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="user_cancel_charge" value="<?php echo e($cashBackConfig[0]->user_cancel_charge); ?>" placeholder="ENTER USER PRODUCT CANCELLATION CHARGE">
                            </div>%
                        </div>
                       
                       
                    </div>
                   
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-purple" type="submit"><?php echo e(__('update')); ?></button>
                        </div>
                    </div>
                </form>
            </div>
                   <?php else: ?>
                   <div class="panel-body">
                <form class="form-horizontal" action="<?php echo e(route('add-cashback-config')); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                    <div id="smtp">
                        <div class="form-group">
                           
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('CASH BACK AMOUNT')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="CASHBACK_AMOUNT" value="" placeholder="ENTER CASHBACK AMOUNT" require>
                            </div>
                        </div>
                        <div class="form-group">
                        
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('MINIMUM SHOPPING AMOUNT')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="MINIMUM_SHOPPING_AMOUNT" value="" placeholder="ENTER MINIMUM SHOPPING AMOUNT" require>
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('SHOPPING USER PERCENTAGE(per order)')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="SHOPPING_percentage" value="" placeholder="Shopping Order percentage" require>
                            </div>%
                        </div>

                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('CASHBACK CREDIT AMOUNT')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="CASHBACK_CREDIT_AMT" value="" placeholder="ENTER CASHBACK CREDIT AMOUNT" require>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            
                            <div class="col-lg-3">
                                <label class="control-label"><?php echo e(__('OFFER DAYS')); ?></label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="OFFER_DAYS" value="" placeholder="MAIL ENCRYPTION">
                            </div>
                        </div>
                       
                       
                    </div>
                   
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-purple" type="submit"><?php echo e(__('Save')); ?></button>
                        </div>
                    </div>
                </form>
            </div>
            <?php endif; ?>

            

        </div>
    
    
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

    <script type="text/javascript">
        $(document).ready(function(){
            checkMailDriver();
        });
        function checkMailDriver(){
            if($('select[name=MAIL_DRIVER]').val() == 'mailgun'){
                $('#mailgun').show();
                $('#smtp').hide();
            }
            else{
                $('#mailgun').hide();
                $('#smtp').show();
            }
        }
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/aldebaza/staging_script/resources/views/business_settings/cashback-user-setting.blade.php ENDPATH**/ ?>